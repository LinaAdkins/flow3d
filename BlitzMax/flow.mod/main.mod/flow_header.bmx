'Provides primary FLOW functionality
Include "flow_constants.bmx"
Include "flow_dllfunctions.bmx"
Include "flow_classes.bmx"
Include "HelperFunctions.bmx"

'Add-ons
Include "CEGUI.bmx"
Include "OFUSION.bmx"
Include "OgreMax.bmx"
Include "FG.bmx"