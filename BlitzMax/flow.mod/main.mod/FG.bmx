Include "FG2D.bmx"

Rem
	bbdoc: A static container interface for initializing and manipulating Ogre.
ENDREM
Type fG
	Global _ogre:TOgre = Null
	Global _scene:TSceneManager = Null
	Global _renderwindow:TRenderWindow = Null
	Global _rootnode:TSceneNode = Null
	Global _viewport:TViewport = Null
	Global _camera:TCamera = Null
	Global _light:TLight = Null
	Global LineManager:TLineManager = Null
	
	'Data for 2D drawing:
	Global _2DManualObject:TManualObject
	Global _2DSceneNode:TSceneNode
	Global _2DMaterial:String
	
	'Width and height stored for 2D use- If you pass a window size like 480,480 to Init,
	'Ogre's width and height may not match these figures.
	'Ogre may instead still render at 1024x800 for instance.
	Global _2DWidth:Int
	Global _2DHeight:Int
	
	REM
		bbdoc: List used to handle .SetOption(...).
	ENDREM
	Global optionList:TNameValuePairList
	
	Global _terminated:Byte = False
	
	'RenderSystem constants
	Const RS_OPENGL:Int = 0
	Const RS_D3D:Int = 1
	
	'Cache the icon path so we can specify it before or after window creation
	Global _iconPath:String = ""
	
	
	Rem
		bbdoc: Initializes the Flow Graphics window. If useConfigWindow is true, then all other settings will be ignored and the config window will be used for configuration. All other parameters should be self explanatory. For the rendersystem use fG.RS_OPENGL and fG.RS_D3D constants
	ENDREM
	Function init(useConfigWindow:Byte = True, width:Int = 800, height:Int = 600, depth:Int = 16, fullscreen:Byte = 0, rendersystem:Int = fG.RS_OPENGL, WindowHandle:Int = 0)

		'Check to see if we're going to be using options setup with SetOptions
		Local usingOptionList:Byte = False
		If optionList <> Null Then
			usingOptionList = True
		End If
		
		'SetOptions - logging
		Local logging:Byte = True
		If usingOptionList Then
			Local value:String = _findOptionValue("logging")
			If value <> Null And value = "false" Then
				logging = False
			End If
		End If
		
		'Create new default log with specified logging params
		TLogManager.Create()
		TLogManager.createLog("Conf/log.txt", True, False, Not logging)
		
		'Create Ogre Root - With default log, log param is ignored
		_ogre = TOgre.Create("Conf/Plugins.cfg", "Conf/ogre.cfg", "Conf/log.txt")
		
		'SetOptions - useResourceCFG
		Local useResourceCFG:Byte = True
		
		'Check for an entry and see if it is false
		If usingOptionList Then
			Local value:String = _findOptionValue("useResourceCFG")
			If value <> Null And value = "false" Then
				useResourceCFG = False
			End If
		End If
		
		'If we use Resource.CFG then load it.
		If useResourceCFG Then
			TResourceGroupManager.LoadResourceFile("Conf/Resources.cfg")
		End If

		'Local opts var
		Local opts:TNameValuePairList = New TNameValuePairList.Create()
		
		'The user has opted for the configuration dialog, so we will ignore all other variables and
		'use the varaibles that are saved when the config window is accepted.
		If useConfigWindow = True Then
			'Get config options from the user and initialise the ogre renderer-
			If _ogre.showConfigDialog() = False Then End
			_ogre.initialise(False) 
					
			_2dwidth:Int = _ogre.getWidth()
			_2dHeight:Int = _Ogre.getHeight()
			
			'Create the win32 window-
			If windowHandle = 0 Then windowHandle = createWin32Window(_ogre.getwidth(), _ogre.getheight(), _ogre.getbitDepth(), _ogre.getFullScreen())
			
			'Give Ogre the window handle and create the render window-
			opts.insert("externalWindowHandle", String(windowHandle)) 
			opts.insert("depth", String(_ogre.getbitDepth()))
			
			'If using an option list, try to find and add "vsync" value
			If usingOptionList Then
				Local value:String = _findOptionValue("vsync")
				If value <> Null Then
					opts.insert("vsync", value)
				End If
			
			'If not using options, set the normal way
			Else
				opts.insert("vsync", _ogre.getVsyncEnabled())
			End If
			
			
			
			'Setup full screen anti-aliasing
			'Direct3D is being used-
				If _ogre.getRenderSystem().getName().Find("Direct3D") <> - 1 Then
					opts.insert("Anti aliasing", _ogre.getAntiAliasingSettings())
					opts.insert("useNVPerfHUD", _ogre.getNVPerfHudSetting())
				End If
				
				If _ogre.getRenderSystem().getName().Find("OpenGL") <> - 1 Then
					opts.insert("FSAA", _ogre.getAntiAliasingSettings()) 
				End If
			
			_renderwindow = _ogre.createRenderWindow("renderwindow", _ogre.getwidth(), _ogre.getheight(), 0, opts) 
		
		'If the user has chosen to NOT use the config dialog-
		Else
		
			'Choose the rendering system based on the rendersystem parameter
			If rendersystem = fG.RS_OPENGL
				_ogre.unloadPlugin("RenderSystem_Direct3D9")
				_ogre.loadPlugin("RenderSystem_GL")				 
				_ogre.setRenderSystem(_ogre.getRenderSystemByName("OpenGL Rendering Subsystem"))
			Else
				_ogre.unloadPlugin("RenderSystem_GL")
				_ogre.loadPlugin("RenderSystem_Direct3D9")
				_ogre.setRenderSystem(_ogre.getRenderSystemByName("Direct3D9 Rendering Subsystem"))
			End If
		
			'If using an option list, try to find and add "vsync" value
			If usingOptionList Then
				Local value:String = _findOptionValue("vsync")
				If value <> Null Then
					opts.insert("vsync", value)
				End If			
			End If

			_ogre.initialise(False) 
			
			'2D Variables			
			_2dwidth:Int = width
			_2dHeight:Int = height	
			
			'Create our own win32 window-
			If windowHandle = 0 Then windowHandle = createWin32Window(width, height, depth, fullscreen) 
			'Give Ogre the window handle and create the render window-
			opts.insert("externalWindowHandle", String(windowHandle))
			opts.insert("depth", String(depth))
			_renderwindow = _ogre.createRenderWindow("renderwindow", _ogre.getwidth(), _ogre.getheight(), 0, opts)
			
		End If
		
		'Initialise resources-
			TResourceGroupManager.initialiseAllResourceGroups() 
		
		'TSceneManager Setup
			
			'Get option for scene manager from option list, if it exists
			If usingOptionList Then
				Local value:String = _findOptionValue("scenetype")
				If value <> Null Then
					_scene = _ogre.createSceneManager(Int(value) , "MainSceneManager")
				
				'If we're using an option list, but scenetype is not set, then set generic
				Else
					_scene = _ogre.createSceneManager(ST_GENERIC, "MainSceneManager")
				End If
			
			'If no option was set, set it to the generic
			Else
				_scene = _ogre.createSceneManager(ST_GENERIC, "MainSceneManager")
			End If

			_scene.setAmbientLight(TColourValue.Create(.5, .5, .5, 1.0))
			_ogre._setCurrentSceneManager(_scene)
			_rootnode = _scene.getRootSceneNode()
		'Main Camera Setup
			_camera = _scene.createCamera("_camera") 
			_camera.setNearClipDistance(0.1) 
			_camera.setAspectRatio(Float(_ogre.getWidth()) / Float(_ogre.getheight()))
			'CameraNode
			Local cameraNode:TSceneNode = _rootnode.createChildSceneNode("_camera", 0, 0, 0)
			cameraNode.attachObject(_camera)
			cameraNode.setPosition(0, 10, - 10)
			cameraNode.lookAt(Vec3() , TS_WORLD, TVector3.NEGATIVE_UNIT_Z())
			cameraNode.roll(180)
		'Default Light Setup
			_light = Self.createLight("_light", TLight.LT_POINT) 
			Local lightNode:TSceneNode = _rootnode.createChildSceneNode("_light", 0, 0, 0)
			lightNode.attachObject(_light)
			lightNode.setPosition(0, 10, 10)
 		'Viewport Setup
			_viewport = _renderwindow.addViewPort(_camera) 
			_viewport.setBackgroundColour(TColourValue.Create(0.266, 0.549, 0.79, 1.0))
                		
	End Function
	
	Rem
		bbdoc: Attaches an existing Ogre instance ( with corresponding RenderWindow ) to the fG class. A valid Ogre and RenderWindow pointer must be passed in.
	ENDREM
	Function attach(ExternalOgre:TOgre, ExternalRenderWindow:TRenderWindow, ExternalCamera:TCamera = Null)
	    'Input validation
		'Assert(ExternalOgre = Null Or Not ExternalOgre._inited)
		_ogre = ExternalOgre
		_renderWindow = ExternalRenderWindow
		If(_ogre = Null Or Not _ogre._inited)
			fG.init()
			Return
		EndIf
 
		'Has a scene manager been created yet?
		_scene = _ogre._getCurrentSceneManager()
		If(_scene.Pointer = Null)
			_scene = _ogre.createSceneManager(ST_GENERIC, "MainScene")
			_scene.setAmbientLight(TColourValue.Create(.5, .5, .5, 1.0)) 
		EndIf
		_rootnode = _scene.getRootSceneNode()

		'Main Camera
		If(ExternalCamera = Null)
			'If no camera was specified, handle the first camera as the current
			'_camera = _scene.getCamera(_scene.getCameraIterator().ToString()) 'Needs to be fixed
			'If no camera exists in the scene, create a default one
			If(_camera = Null)
					'Main Camera Setup 
					_camera = _scene.createCamera("_camera")
					_camera.setNearClipDistance(0.1) 
					_camera.setAspectRatio(Float(_ogre.getWidth()) / Float(_ogre.getheight()))
					'CameraNode
					Local cameraNode:TSceneNode = _rootnode.createChildSceneNode("_camera", 0, 0, 0)
					cameraNode.attachObject(_camera)
					cameraNode.setPosition(0, 10, - 10)
					cameraNode.lookAt(Vec3() , TS_WORLD, TVector3.NEGATIVE_UNIT_Z())
					cameraNode.roll(180)
					Local Viewport:TViewport = _renderWindow.addViewPort(_camera)
			EndIf
		Else
		  	_camera = ExternalCamera
		EndIf
		
		'Do we have a _light already? We need _Light to exist for soft shadows not to crash.				  
		If(_light = Null)
			'Default Light Setup
			_light = Self.createLight("_light", TLight.LT_POINT)
			Local lightNode:TSceneNode = _rootnode.createChildSceneNode("_light", 0, 0, 0)
			lightNode.attachObject(_light)
			lightNode.setPosition(0, 10, 10)		
		EndIf
		
		_viewport = _camera.GetViewport()
		
	End Function
	
	REM
		bbdoc: This setting allows you to set options for the render that are not available in fG init.
	ENDREM
	Function SetOption(name:String, value:String)
		
		'If the list isn't created, create it
		If optionList = Null Then
			optionList = TNameValuePairList.Create()
		End If
		
		optionList.insert(name, value)
	End Function
	
	REM
		bbdoc: Search internal option list for a name and return the value as a string. If no entry is found, returns NULL.
	ENDREM
	Function _findOptionValue:String(name:String)
		
		'Find our option by iterating through the list
		Local optionListIter:TNameValuePairListIter = optionList.begin()
		While optionListIter.isNotEqualTo(optionList.ending())
			Local listElementName:String = optionListIter.first()
			
			'If we have a match then set the return value
			If listElementName = name Then
				Return optionListIter.second()
			End If
			
			optionListIter.increment()
		Wend
		
		Return Null
		
	End Function

	'==================
	'Scene Properties
	'==================
		
		Rem
			bbdoc: Clear the world of all entities, except for cameras. This does not clear textures, "Brushes" etc.
		ENDREM
		Function ClearWorld() 
			_scene.clearScene()
		End Function
		
		Rem
			bbdoc: Set the ambient lighting for the scene. Default is .5 , .5 , .5. 
		ENDREM
		Function AmbientLight(r:Float, g:Float, b:Float) 
			_scene.setAmbientLight(TColourValue.Create(r, g, b, 1.0)) 
		End Function
		
		Rem
			bbdoc: Gets the default TCamera that is created with the initialisation of fG.
		ENDREM
		Function getDefaultCamera:TCamera() 
			Return fG._camera
		End Function
		
		Rem
			bbdoc: Render one frame of the current scene.
		ENDREM
		Function renderWorld() 
			_ogre.renderOneFrame() 
		End Function

		Rem
			bbdoc: Get the number of triangles rendered for the entire window. (_renderWindow:TRenderWindow )
		ENDREM
		Function TrisRendered:Int() 
			Return _renderwindow.getTriangleCount() 
		End Function
		
		Rem
			bbdoc: Get the FPS of the primary fG window.<br/>isAveraged:Byte = False - Set this to true if you would like the FPS to be averaged for a smoother readout.
		ENDREM
		Function getFPS:Float(isAveraged:Byte = False)
			If isAveraged Then
				Return _renderwindow.getAverageFPS()
			Else
				Return _renderWindow.getLastFPS()
			End If
		End Function

	'==================
	'OSM
	'==================
	
		Rem
			bbdoc: Load an OSM's contents into the current scene by specifying the OSM's name. Please note that the OSM must be available in a resource directory.
		ENDREM
		Function LoadOSM:TOSMScene(fileName:String) 
			Local OfusionScene:TOSMScene = TOSMScene.Create(fG._scene, fG._renderwindow) 
			OfusionScene.initialise(fileName) 
			OfusionScene.createScene() 
			Return OfusionScene
		End Function
	
		Rem
			bbdoc: Write your scene's contents to an OSM. If you need to filter objects then you will have to use a callback object.
		ENDREM
		Function SaveOSM:TOSMSerializer(fileName:String = "Scene.OSM", callbacks:TOSMSerializerCallbacks = Null) 
			Local serializer:TOSMSerializer = TOSMSerializer.Create(_scene, _renderwindow) 
			serializer.writeScene(fileName, Callbacks) 
		End Function
		
		REM
			bbdoc: Load a dotscene scene file into your current scene. Please note that the dotscene file and all of its resources must be in a directory specified in the resources.cfg or elsewhere in code.
		ENDREM
		Function LoadDotScene:TOgreMaxScene(fileName:String, loadOptions:Int = TSceneLoadOptions.NO_OPTIONS, itemPrefix:String = Null, rootNode:TSceneNode = Null, callback:TOgreMaxSceneCallback = Null)
			Local scene:TOgreMaxScene = TOgreMaxScene.Create()
			
			'If the user specified a prefix, set it for all  loaded objects
			If itemPrefix <> Null Then
				scene.SetNamePrefix( itemPrefix , TWhichNamePrefix.ALL_NAME_PREFIXES )
			End If
			
			scene.Load(filename, fG._renderwindow, loadOptions, fG._scene, rootNode, callback)
			Return scene
		
		End Function
		
	
	'==================
	'Camera
	'==================
	
		Rem
			bbdoc: Set the camera's background colour. In Ogre cameras are tied to viewports, and the viewport tied to this camera is what actually gets changed.
		ENDREM
		Function CameraClsColor(camera:TCamera, RED:Float, green:Float, blue:Float, alpha:Float = 1.0) 
			camera.GetViewport().setBackgroundColour(TColourValue.Create(RED, green, blue, alpha)) 
		End Function
	

		Rem
			bbdoc: Sets the viewing window for the specified camera, ie. Dimensions and placement. These coordinates are in relative format, being relative to the size of the renderwindow.<br>x:Float and y:Float (0.0-1.0) Percentage of offset from the left and right of the render window.<br>width:Float and height:Float (0.0-1.0) Percentage of the render window width and height.
		ENDREM
		Function CameraViewport(camera:TCamera, x:Float, y:Float, width:Float, Height:Float) 
			camera.GetViewport().setDimensions(x, y, width, height) 
		End Function
			
		Rem
			bbdoc: Pick with mouse coordinates and the camera.<br/>viewportx:Float and viewporty:Float = -1 - Mouse position in relative coordinates (0-1) in terms of the viewport. ( To transform to relative coords with fG you would use something like Float(MouseX()) / Float(fG._ogre.getWidth()).<br/>camera:TCamera = Null - Default is to use the fG camera, otherwise this is the camera attached to a viewport that you will cast the picking ray from.<br/>Please note that while cameraPick can be very useful, it uses 1:1 collision meshes which are not optimal. If you are needing higher speed results, it's recommended that you use an option like OgreNewt's basic ray cast.<br/>RESULTS ARE STORED IN TCameraPickResult!
		ENDREM
		Function cameraPick(viewportx:Float, viewporty:Float, camera:TCamera = Null)
		  
			'If camera defaults, set to the fG camera-
			If camera = Null Then
				camera = fG._camera
			End If
			
			'Initialise our TCameraPickResult structure if it hasn't been-
			If TCameraPickResult._inited = False Then
				TCameraPickResult.init()
			End If
			
			'Create the query and ray and set them up-
			Local cameraRay:TRay = TRay.CreateEmpty()
			Local cameraQuery:TRaySceneQuery = fG._scene.createRayQuery(cameraRay)
			camera.getCameraToViewportRay(viewportx, viewporty, cameraRay)
			cameraQuery.setRay(cameraRay)
				
			'We want one result sorted by distance-
			cameraQuery.setSortByDistance(True, 1)
			
			'Save the results to TCameraPickResult
			TCameraPickResult._entity = fG._scene.polyPick(cameraQuery, TCameraPickResult._collisionPos, TCameraPickResult._collisionNormal)	
			
			'Destroy the query
			fG._scene.destroyQuery(cameraQuery)
		EndFunction
		
		Rem
			bbdoc: Quickly enable or disable a given compositor by its name. Note that you can "stack" multiple compositors!<br/>The zorder specifies where in the line of compositors this one sits. The -1 default means that it automatically goes to the end of the compositor chain.<br/> Returns the compositor instance if enabled, null if disabled.
		ENDREM
		Function enableCompositor:TCompositorInstance(enabled:Byte, name:String, zorder:Int = -1)
		
			'RETURNS: If enabled, adds the compositor and returns the instance.
			If enabled = True Then
				Local instance:TCompositorInstance = TCompositorManager.addCompositor(_viewport, name, zorder)
				TCompositorManager.setCompositorEnabled(_viewport, name, True)
				Return instance
				
			'If not enabled, remove the compositor
			Else
				TCompositorManager.setCompositorEnabled(_viewport, name, False)
				TCompositorManager.removeCompositor(_viewport, name)
				
			End If
			
			'RETURNS: Null as a catch all.
			Return Null
			
		End Function
		
		Rem
			bbdoc: Easily remove all compositors from this fG/Ogre instance.
		ENDREM
		Function removeAllCompositors()
			TCompositorManager.removeAll()
		End Function
	
	'==================
	'Entity Movement
	'==================
		Rem
			bbdoc: Sets the position of a TEntity/TMovableObject.
		ENDREM
		Function PositionEntity(entity:TMovableObject, x:Float, y:Float, z:Float) 
			entity.getParentSceneNode().setPosition(x, y, z) 
		End Function
	
		Rem
			bbdoc: Moves an entity relative to it's current position using the x/y/z axes.<br>relativeTo:Int = TS_LOCAL Default is local coordinate space. You may also translate in TS_GLOBAL/Global coordinate space and TS_PARENT/Parent coordinate space.
		ENDREM
		Function TranslateEntity(entity:TMovableObject, x:Float, y:Float, z:Float, relativeTo:Int = TS_LOCAL)
			entity.getParentSceneNode().translate(x, y, z, relativeTo) 
		End Function
		
		Rem
			bbdoc: Points one entity at another entity, with an option to set the local direction vector of the entity being pointed which defaults to -z.
		ENDREM
		Function PointEntity(entity:TMovableObject, target:TMovableObject, localDirectionVector:TVector3 = Null) 
			If localDirectionVector = Null Then localDirectionVector = TVector3.Create(0.0, 0.0, - 1.0) 
			entity.getParentSceneNode().lookAt(target.getParentSceneNode()._getDerivedPosition() , TS_WORLD, localDirectionVector)
		End Function

		Rem
			bbdoc: Points an entity at a specified world coordinate, with an option to set the local direction vector of the entity being pointed which defaults to -z.
		ENDREM
		Function EntityLookAt(entity:TMovableObject, TargetX:Float, TargetY:Float, TargetZ:Float, localDirectionVector:TVector3 = Null)
			If localDirectionVector = Null Then localDirectionVector = TVector3.Create(0.0, 0.0, - 1.0)
			entity.getParentSceneNode().lookAt(TVector3.Create(targetx, targety, targetx), TS_WORLD, localDirectionVector)
		End Function
		
		Rem
			bbdoc: Set the scale of a given entity. Default scale is usually 1,1,1.
		ENDREM
		Function ScaleEntity(entity:TMovableObject, x_scale:Float, y_scale:Float, z_scale:Float)
			entity.getParentSceneNode().SetScale(x_scale, y_scale, z_scale) 
		End Function
		
		Rem
			bbdoc: Turn an entity relative to it's current position using degrees.<br>relativeTo:Int = TS_LOCAL Default is local coordinate space. You may also rotate in TS_GLOBAL/Global coordinate space and TS_PARENT/Parent coordinate space.<br>Order of rotation: Pitch, Yaw, then Roll.
		ENDREM
		Function TurnEntity(entity:TMovableObject, pitch:Float, yaw:Float, roll:Float, relativeTo:Int = TS_LOCAL)
			Local sn:TSceneNode = entity.getParentSceneNode() 
			sn.pitch(pitch, relativeTo) ; sn.yaw(yaw, relativeTo) ; sn.roll(roll, relativeTo) 
		End Function
		
		Rem
			bbdoc: Orient an entity using Euler angles in degrees.<br>Order of rotation: Pitch, Yaw, then Roll.
		ENDREM
		Function SetRotation(entity:TMovableObject, pitch:Float, yaw:Float, roll:Float)
			Local sn:TSceneNode = entity.getParentSceneNode()
			sn.setOrientationwithquaternion(TQuaternion.IDENTITY())
			sn.pitch(pitch, TS_WORLD) ; sn.yaw(yaw, TS_WORLD) ; sn.roll(roll, TS_WORLD)
		End Function
				
	'==================
	'Entity Control
	'==================
	
		Rem
			bbdoc: Get an entity in the default fG scene by specifying its name. Returns Null if an entity by that name is not found.
		ENDREM
		Function getEntity:TEntity(name:String)
			If _scene.hasEntity(name) Then
				Return _scene.getEntity(name)
			Else
				Return Null
			End If
		End Function
		
		Rem
			bbdoc: Set whether or not you should show a bounding box for the specified entity. Note that if this entity is attached to a scene node with other entities attached, it will turn bounding boxes on for all of them.
		ENDREM
		Function showBoundingBox(entity:TMovableObject, show:Byte)
			entity.getParentSceneNode().showBoundingBox(show)
		End Function
		
		Rem
			bbdoc: Get whether or not the bounding box is visible for this entity. 
		ENDREM
		Function getShowBoundingBox:Byte(entity:TMovableObject)
			Return entity.getParentSceneNode().getShowBoundingBox()
		End Function
	
		Rem
			bbdoc: Load a .mesh file into the scene as a TEntity at 0,0,0. You may specify a parent:TEntity, which will cause this TEntity's scene node to be a child of the given TEntity's scene node.
		END REM
		Function LoadMesh:TEntity(Name:String, filename:String, parent:TEntity = Null) 
			Local mesh:TEntity = _scene.createEntity(Name, filename) 
			If parent <> Null Then
				If parent.isAttached() Then mesh.getParentSceneNode().attachObject(mesh) 
			Else
				Local meshNode:TSceneNode = _rootnode.createChildSceneNode(Name + "node", 0, 0, 0) 
				meshNode.attachObject(mesh) 
			End If
			Return mesh
		End Function
		
		Rem
			bbdoc: Create a generic cube entity at point 0,0,0. Note that the TSceneNode the cube is attached to's name is name+"node". Note that you must make sure that you give this cube a unique name among TEntities. The cube object is returned as a scene TEntity.
		ENDREM
		Function createCube:TEntity(Name:String, scale:Float = 1)
			Local cube:TEntity = _scene.createEntityWithPrefab(Name, _scene.PT_CUBE)
 			Local cubeNode:TSceneNode = _rootnode.createChildSceneNode(Name + "node", 0, 0, 0)
			cubenode.attachObject(cube)
			cubeNode.SetScale(.01 * scale, .01 * scale, .01 * scale)
			Return cube
		End Function
	
		Rem
			bbdoc: Create a generic sphere entity at point 0,0,0. Note that the TSceneNode the sphere is attached to's name is name+"node". Note that you must make sure that you give this sphere a unique name among TEntities. The sphere object is returned as a scene TEntity.
		ENDREM
		Function CreateSphere:TEntity(Name:String, scale:Float = 1)
			Local sphere:TEntity = _scene.createEntityWithPrefab(Name, _scene.PT_SPHERE) 
			Local sphereNode:TSceneNode = _rootnode.createChildSceneNode(Name + "node", 0, 0, 0) 
			sphereNode.attachObject(sphere)
			sphereNode.SetScale(.01 * scale, .01 * scale, .01 * scale)
			Return sphere
		End Function
		
		Rem
			bbdoc: Create a generic plane at 0,0,0. Unlike blitz3d this plane is not infinite, but you may specify both subdivisions and size. Also, please make sure that you give your plane entity a unique name.<br/>The mesh generated will be given yourname_Mesh and the scene node generated will be given yourname_SceneNode.
		ENDREM
		Function createPlane:TEntity(Name:String, scale:Float = 1, sub_divs:Int = 3, parent:TSceneNode = Null)
			TMeshManager.createPlane(Name + "_Mesh", "General", Float(10) * scale, Float(10) * scale, sub_divs, sub_divs)
			Local planeEntity:TEntity = _scene.createEntity(Name, Name + "_Mesh")
			Local planeNode:TSceneNode = _rootnode.createChildSceneNode(Name + "_SceneNode", 0, 0, 0)
			planeNode.attachObject(planeEntity)
			planeNode.pitch(- 90)
			Return planeEntity
		End Function
	
		Rem
			bbdoc: Remove and destroy a TEntity in your scene. All child nodes have TEntites detached but only nodes and destroyed. Entities will be detached from the scenegraph, but will still need to be destroyed from the scene manager.
		ENDREM
		Function FreeEntity(entity:TMovableObject)
			Local sn:TSceneNode = entity.getParentSceneNode() 
			sn.removeAndDestroyAllChildren()
			_scene.destroyEntity(entity.toEntity())
			_scene.destroySceneNode(sn.getName()) 
		End Function
		
		Rem
			bbdoc: Get an entities attached scene node. Under the hood, nodes control all transforms, not entities, so if you need to do anything more than fG.TurnEntity , fG.Translate , etc - you may need to drill down into entity functionality. Note that if there is no entity attached, this function will return null.
		ENDREM
		Function getNode:TSceneNode(entity:TMovableObject)
			If entity.isAttached() Then
				Return entity.getParentSceneNode()
			Else
				Return Null
			End If
		End Function
		
		Rem
			bbdoc: Get the position of an entity. If global is set to true, then you will get world space instead of space relative to the parent.
		ENDREM
		Function getPosition:TVector3(entity:TMovableObject, world:Byte = False)
			If world Then
				Return entity.getParentSceneNode()._getDerivedPosition()
			Else
				Return entity.getParentSceneNode().getPosition()
			End If
		End Function
		
		Rem
			BBDOC: Sets a fixed yaw axis. Useful for cameras where you do not want the object to roll.
		Endrem
		Function SetFixedYawAxis(Entity:TMovableObject, Use:Int = True, YawAxisX:Byte = 0, YawAxisY:Byte = 1, yawaxisz:Byte = 0)
			Entity.Getparentscenenode().SetFixedYawAxis(use, yawaxisX, yawaxisy, YawaxisZ)
		End Function
		
		
	'==================
	'Light Control
	'==================
			
		Rem
			bbdoc: Create a new dynamic light in the scene by giving a unique name.<br>Light types are: TLight.LT_DIRECTIONAL, TLight.LT_SPOT  and TLight.LT_POINT. Keep in mind that because of how omni lights are projected they can be cpu intensive in numbers.
		ENDREM
		Function createLight:TLight(Name:String, lightType:Int, parent:TSceneNode = Null) 
			Local Obj:TLight = _scene.createLight(Name) 
			If parent <> Null Then parent.attachObject(Obj) 
			Return Obj
		End Function
	
		Rem
			bbdoc: Set the attenuation range for a given light.
		END REM
		Function LightRange(light:TLight, range:Float = 1000) 
			light.setAttenuation(range, range, range, range) 
		End Function
		
		Rem
			bbdoc: Enable shadows using one of Ogre's specified shadow types. If you want to turn shadows off, simple set SHADOWTYPE_NONE. If you want a different kind of shadow, then look at the SHADOWTYPE_ constants, or look into the shader based texture shadows.
		ENDREM
		Function enableShadows(shadowType:Int = SHADOWTYPE_STENCIL_MODULATIVE)
			_scene.setShadowTechnique(shadowType)
		End Function
		
		Rem
			bbdoc: Enables soft shadows for your scene. Note that you MUST have shadow texture enabled materials on your objects to enable them to work. There are a lot of parameters in this method, but in general the defaults should work properly.<br/>shadowType:Int - Specify whether you want shadow blending to be modulative or additive. Default is modulative.<br/>shadowTextureCount:Int = 4 - Number of shadow textures to use. More shadow textures generally means lower performance and better shadows.<br/>shadowTextureSize:Int = 1024 - The size of the texture shadows. The higher the number the slower the performance and the clearer the textures.<br/>material:String = "shadow_caster" - Default shadow caster material. Don't like how these texture shadows work? You can set a custom receiver shader material here.<br/>pixelFormat:Int = PF_FLOAT16_RGB - The pixel format of the shader textures.<br/>Note: This function will change the fG default light to a spotlight and change its position! Shadow textures only work with directional and spot lights! Disable soft shadows to reset the fG light type and position.
		ENDREM
		Function enableSoftShadows(enabled:Byte, shadowType:Int = SHADOWTYPE_TEXTURE_MODULATIVE_INTEGRATED, shadowTextureCount:Int = 4, shadowTextureSize:Int = 1024, material:String = "shadow_caster", pixelFormat:Int = PF_FLOAT16_RGB)
		
			If enabled = True Then	
				'Change main fG light to a spot light.	
				Local light:TLight = fG._light
				light.setType(TLight.LT_SPOTLIGHT)
				fG.PositionEntity(light, 0, 10, 12.5)
				light.setDirection(0, - 1, - 1)
			
				'Set up soft size by setting shadow texture count, size, the caster material used, etc.
				fG._scene.setShadowTextureSelfShadow(True)
				fG._scene.setShadowTextureCasterMaterial(material)
				fG._scene.setShadowTextureCount(shadowTextureCount)
				fG._scene.setShadowTextureSize(shadowTextureSize)
				fG._scene.setShadowTexturePixelFormat(pixelFormat)
				fG._scene.setShadowCasterRenderBackFaces(False) 
				
				'Set the background colour and clear every frame attributes for the shadow textures.
				Local numShadowRTTs:Int = fG._scene.getShadowTextureCount()
				For Local i:Int = 0 To numShadowRTTs - 1
					Local tex:TTexture = fG._scene.getShadowTexture(i) 
					Local vp:TViewport = tex.getBuffer().getRenderTarget().GetViewport(0) 
					vp.setBackgroundColour(TColourValue.Create(1, 1, 1, 1)) 
					vp.setClearEveryFrame(True)
				Next
			
				'Enable the shadow texture technique	
				fG._scene.setShadowTechnique(shadowType)
			
			'Set fG light back to default and turn off shadows.
			Else
				fG._light.setType(TLight.LT_POINT)
				fG.PositionEntity(fG._light, 0, 10, 10)
				fG._scene.setShadowTechnique(SHADOWTYPE_NONE)
			End If
				
		
		End Function
		
	'==================
	'Graphics
	'==================
		
		Rem
			bbdoc: Swaps the back buffer to the front. By default all Ogre render windows are double buffered, so you do not have to set anything up for double buffering first.<br>vwait:Int = True By default VSync is on, but here you can specify whether or not you want the buffer swap to wait until the monitor is finished rastering.
		ENDREM
		Function Flip(vwait:Int = True) 
			_renderwindow.SwapBuffers(vwait) 
		End Function
		
	'==================
	'LineDrawing
	'==================
		
		Rem
			bbdoc: Initialise line drawing within fG, returning the LineManager. You may also access the line manager by using fG.LineManager.
		ENDREM
		Function InitLineDrawing:TLineManager()
			LineManager = TLineManager.Create(fG._scene)
			Return LineManager
		End Function
		
		REM
			bbdoc: Free all resources and uninitialise the line manager in fG. This will remove any lines initialised and you will have to call InitLineDrawing() if you want to use the line manager.
		ENDREM
		Function DeInitLineDrawing()
			LineManager.Destroy()
		End Function
		
	'==================
	'2D Drawing   - Max2D Equivalent functions
	'==================
		REM
			bbdoc: Initialise 2D Drawing. Also inits line drawing.
		ENDREM
		Function Init2D()
			If _2DManualObject = Null Then
				_2DManualObject = fG._scene.createManualObject("2DManualObject")
				_2DManualObject.setCastShadows( false )
				_2DSceneNode:TSceneNode = fG._rootnode.createChildSceneNode("2DManualNode", 0, 0, 0)
				_2DSceneNode.attachObject(_2DManualObject)
	
				' Use identity view/projection matrices
				_2DManualObject.setUseIdentityProjection(True) ;
				_2DManualObject.setUseIdentityView(True) ;
				_2DMaterial = "BaseWhiteNoLighting"
			Else
				RuntimeError "2D Drawing already initialised"
			EndIf
		End Function
		
		REM
			bbdoc: Cleans up after 2D drawing. you will need to re-init to continue drawing.
		ENDREM
		Function DeInit2D()
			_2DManualObject.detatchFromParent()
			fG._scene.destroyManualObject(_2DManualObject)
			fG._rootnode.removeChild(_2DSceneNode)
			fG._scene.destroySceneNode("2DManualNode")
			_2DManualObject = Null
		End Function
		REM
			bbdoc: Clear 2D Drawing
		ENDREM
		Function Cls()
			_2DManualObject.clear()
		End Function
		REM
			bbdoc: Sets the material used in 2D operations.
		ENDREM
		Function SetMaterial(material:String)
			_2DMaterial = material
		End Function
		REM
			bbdoc: Draw a rectangle. Coordinates are in pixels.
		ENDREM
		Function DrawRect(x1:Float, y1:Float, WIDTH:Float, HEIGHT:Float, fill:Int = False)
			Local renderop:Int
			If fill Then renderop = TRenderOperation.OT_TRIANGLE_LIST Else renderop = TRenderOperation.OT_LINE_STRIP
			_2DManualObject.begin(_2DMaterial, Renderop) ;
			Local x3:Float = ((x1 / _2dwidth) - 0.5)
			Local y3:Float = ((y1 / _2dHeight) - 0.5)
			Local x4:Float = (((WIDTH + X1) / _2dwidth) - 0.5)
			Local y4:Float = (((HEIGHT + Y1) / _2dHeight) - 0.5)
			_2DManualObject.position(x3 * 2, y3 * -2, 0.0) ;
			_2dmanualobject.textureCoord(0, 1)
			_2DManualObject.normal(0, 1, 0)
			_2DManualObject.position(x4 * 2, y3 * -2, 0.0) ;
			_2dmanualobject.textureCoord(1, 1)
			_2DManualObject.normal(0, 1, 0)
			_2DManualObject.position(x3 * 2, y4 * -2, 0.0) ;
			_2dmanualobject.textureCoord(0, 0)
			_2DManualObject.normal(0, 1, 0)
			_2DManualObject.position(x4 * 2, y4 * -2, 0.0) ;
			_2dmanualobject.textureCoord(1, 0)
			_2DManualObject.normal(0, 1, 0)
			If fill = False Then
				_2DManualObject.index(0)
				_2DManualObject.index(1)
				_2DManualObject.index(3)
				_2DManualObject.index(2)
				_2DManualObject.index(0)
			Else
				_2dmanualobject.triangle(0, 2, 1)
				_2dmanualobject.triangle(1, 2, 3)
			EndIf
			
			_2DManualObject.setBoundingBox(TAxisAlignedBox.BOX_INFINITE())
			_2DManualObject.ending()
		End Function
		
		REM
			bbdoc: Draw a line. Coordinates are in pixels.
		ENDREM
		Function DrawLine(x1:Float, y1:Float, x2:Float, y2:Float)
			_2DManualObject.begin(_2DMaterial, TRenderOperation.OT_LINE_STRIP) ;
			Local x3:Float = ((x1 / _2dwidth) - 0.5)
			Local y3:Float = ((y1 / _2dHeight) - 0.5)
			Local x4:Float = ((x2 / _2dwidth) - 0.5)
			Local y4:Float = ((y2 / _2dHeight) - 0.5)
			
			_2DManualObject.position(x3 * 2, y3 * -2, 0.0) ;
			_2DManualObject.normal(0, 1, 0)
			_2dmanualobject.textureCoord(0, 0)

			_2DManualObject.position(x4 * 2, y4 * -2, 0.0) ;
			_2DManualObject.normal(0, 1, 0)
			_2dmanualobject.textureCoord(1, 1)
			
			_2DManualObject.index(0) ;
			_2DManualObject.index(1) ;
			_2DManualObject.setBoundingBox(TAxisAlignedBox.BOX_INFINITE())
			_2DManualObject.ending()
		End Function
		
		REM
			bbdoc: Draw an oval. Coordinates are in pixels.  This can be used to draw many shapes like Triangles (Edges=3.)
		ENDREM
		Function DrawOval(x:Float, y:Float, width:Float, height:Float, Edges:Int = 32, fill:Int = False)
			Local renderop:Int
			If fill Then renderop = TRenderOperation.OT_TRIANGLE_LIST Else renderop = TRenderOperation.OT_LINE_STRIP
			_2DManualObject.begin(_2DMaterial, renderop) ;
			Local x3:Float = (x / Float(_2dwidth))
			Local y3:Float = (y / Float(_2dHeight))
			Local point_index:Int = 0
			Local index:Int = 0
			rem
			
			if fill=false then
				_2DManualObject.position((x3 - 0.5) * 2, - (y3 - 0.5) * 2, 0)
				_2DManualObject.normal(0, 1, 0)
				_2DManualObject.index(0)
				point_index = 1
			EndIf
			endrem
			
			While index < 360
				Local x4:Float = ((Sin(index) * width / 2.0) / _2dwidth) - 0.5
				Local y4:Float = ((Cos(index) * height / 2.0) / _2dHeight) - 0.5
			
				_2DManualObject.position((x3 + x4) * 2, - (y3 + y4) * 2, 0)
				_2DManualObject.normal(0, 1, 0)
				_2dmanualobject.textureCoord((sin(index)+1)/2,(cos(index)+1)/2)

				If fill = False Then _2DManualObject.index(point_index)
				
				point_index = point_index + 1
				index = index + (360.0 / Edges)
			Wend
			If fill = False Then _2DManualObject.index(0)
			
			'fill it?
			If fill Then
				For Local i:Int = 1 To point_index - 2 Step 1
					_2dmanualobject.triangle(i, i + 1, 0)
				Next
			End If
			
			_2DManualObject.setBoundingBox(TAxisAlignedBox.BOX_INFINITE())
			_2DManualObject.ending()
		End Function
			
		
	'==================
	'Particle Systems
	'==================
	
		Rem
			bbdoc: Load a particle system from a particle system template name in a .particle file. You may load multiple systems using the same template file.<br/>The name specified must be unique.<br/>Optionally, if you wish to attach this particle system to a parent scene node then you may specify it.<br/>If you do not specify a parent Scene Node the node will be automatically created with name + "_PSNODE" at 0,0,0<br/>Returns an instance of the particle system created from the template specified.
		ENDREM
		Function loadParticleSystem:TParticleSystem(name:String, templateName:String, parent:TSceneNode = Null)
			
			'Create the Particle System
			Local tempPS:TParticleSystem = _scene.createParticleSystemWithTemplate(name, templateName)
			
			'If the user specified a parent object and its reference isn't Null then use the parent.
			If parent <> Null And parent.isNull() <> True Then
				parent.attachObject(tempPS)
			
			'No parent specified, so create the node	
			Else
				Local tempNode:TSceneNode = _rootnode.createChildSceneNode(name + "_PSNODE", 0, 0, 0)
				tempNode.attachObject(tempPS)	
			End If
			
			Return tempPS
		End Function
		
		Rem
			bbdoc: Create an empty particle system to specify the attributes of manually. <br/>The name specified must be unique.<br/>quota:Int Represents how many particles can be rendered at a time with this Particle System.<br/>Optionally, if you wish to attach this particle system to a parent scene node then you may specify it.<br/>If you do not specify a parent Scene Node the node will be automatically created with name + "_PSNODE" at 0,0,0<br/>Returns an instance of the particle system created.
		ENDREM
		Function createEmptyParticleSystem:TParticleSystem(name:String, quota:Int = 500, parent:TSceneNode = Null)
			
			'Create the Particle System
			Local tempPS:TParticleSystem = _scene.createParticleSystem(name, quota)
			
			'If the user specified a parent object and its reference isn't Null then use the parent.
			If parent <> Null And parent.isNull() <> True Then
				parent.attachObject(tempPS)
			
			'No parent specified, so create the node	
			Else
				Local tempNode:TSceneNode = _rootnode.createChildSceneNode(name + "_PSNODE", 0, 0, 0)
				tempNode.attachObject(tempPS)	
			End If
			
			tempPS._Update(.0001)
			
			Return tempPS
		End Function
		
		Rem
			bbdoc: This allows you to spawn particles in a specified particle system.<br/>particleSystem:TParticleSystem is the particle system to spawn the particle into. You must make sure that the particle system has been initialized ( a render call has been made after its creation ) and that the particle will not excede the particle quota of the system.<br/>dir:TVector3 A 3d Vector Specifying the direction that the particle will go. The magnitude of the vector also describes the velocity of the particle.<br/>pos:TVector3 (Optional) Specifies the world position where the particle will spawn.<br/>Returns the TParticle instance, which you can make further modifications to.
		ENDREM
		Function createParticle:TParticle(particleSystem:TParticleSystem, dir:TVector3, size:Float = 1.0, pos:TVector3 = Null)
		
			'Create the particle
			Local p:TParticle = particleSystem.createParticle()
			
			'If the particle was created then set the attributes.
			If p.isNotNull() Then
				
				'Set the default for the position vector3.
				If pos = Null Then pos = TVector3.createEmpty()
				
				'Set Attributes
				p.setDirection(dir)
				p.setPosition(pos)
				p.setDimensions(size, size)
				p.setTimeToLive(5)
				p.setTotalTimeToLive(5)
				p.setColour(TColourValue.Create(1, 1, 1, 1))
					
			End If
			
			Return p
			
		End Function
		
		'==================
		'Utility Functions
		'==================
		
		Rem
			bbdoc: Give the main fG window focus.
		ENDREM
		Function setFocused()
			Local windowHandle:Long
			_renderWindow.getCustomAttribute("WINDOW", windowHandle)
			SetFocus(windowHandle)
		End Function
		
		Rem
			bbdoc: See whether or not the close button has been pressed. Be aware that once you check for the press it will be reset until the program is closed again.
		ENDREM
		Function appTerminated:Byte()
			Local result:Byte = _terminated
			_terminated = False
			Return result
		End Function
		
		Rem
			bbdoc: Set an application icon ( Icon in the top left corner ) for your icon by specifying the file name and path relative to the .exe.<br>You may call this function before or after fG.init()!<br>Right now this only works for win32.
		ENDREM
		Function setApplicationIcon(path:String)
			
			'If we have not called 
			If _ogre = Null Then
				_iconPath = path
			
			'If fG.init() has been called, load the icon immediately!
			Else
				Local windowHandle:Long
				_renderWindow.getCustomAttribute("WINDOW", windowHandle)
				Local iconHandle:Int = LoadIcon(windowHandle, path, 0)
				SetClassLong(windowHandle, - 14, iconHandle)
			End If
			
		End Function
		
		Rem
			bbdoc: Change the application's title text after it has been created. Before this, you can simply use APPTITLE to set the name on fG.init().
		ENDREM
		Function changeAppTitle(title:String)
			'Get the window handle
			Local windowHandle:Long
			_renderWIndow.getCustomAttribute("WINDOW", windowHandle)
			'Set the text (win32 only )
			SetWindowText(windowHandle, title)
		End Function
		
		Rem
			bbdoc: Get whether or not fG.init() has been called successfully.
		ENDREM
		Function isInitialised:Byte()
			
			'RETURN TRUE: If our ogre instance is not NULL.
			If _ogre <> Null Then
				Return True
			End If
			
			'RETURN FALSE: If Ogre has not been initialised.
			Return False
		End Function
		
		Rem
			bbdoc: Get the application icon path specified by the user ( as a string ).
		ENDREM
		Function getApplicationIconPath:String()
			Return _iconPath
		End Function
		
	
		'==========================
		'Math Functions
		'==========================
		
		Rem
			bbdoc: Transform a given vector from one entity's local space into anothers, or to and from world space.<br/>Leave FromEntity null if you wish to transform from world space, leave ToEntity null if you wish to transform into world space.
		ENDREM
		Function TformPoint:TVector3(vector:TVector3, FromEntity:TMovableObject = Null, ToEntity:TMovableObject = Null)
		
			Local fromspace:TSceneNode
			Local tospace:TSceneNode
			
			'Lets get the scene nodes the given entities are attached to.
			If FromEntity <> Null Then fromspace = FromEntity.getParentSceneNode()
			If ToEntity <> Null Then Tospace = ToEntity.getParentSceneNode()
		
			'Global - it's only created once. For speed.
			Global TFormPointTransMatrix:TMatrix4 = TMatrix4.createEmpty()
		
			'If we have a valid space to convert FROM then get the matrix and multiply it against our point.
			'RETURNS: The vector if our space to convert TO is Null.
			If fromspace <> Null Then
				fromspace.getWorldTransforms(TFormPointTransMatrix) 
				vector:TVector3 = TFormPointTransMatrix.mulWithVector3(vector) 
				If tospace = Null Then Return vector
			EndIf
			
			'If we have a valid space to convert TO then multiply the space transforms against the already transformed vector.
			'RETURNS: The transformed vector.
			If tospace <> Null Then
				tospace.getWorldTransforms(TFormPointTransMatrix)
				Return TFormPointTransMatrix.inverse().mulWithVector3(vector)
			EndIf
			
			'RETURN: Transformed vector whether tospace or fromspace was valid.
			Return vector
		
	End Function

		

	
End Type

'WndProc to handle the message pump from the win32 window to the BlitzMax control system. This is an internal function and you shouldn't need it for anything.
Function ClassWndProc:Int(hwnd:Int, MSG:Int, wp:Int, lp:Int) "win32"

	'Handle ALT keys so that the system menu will not be invoked and pause the app-
	Select MSG
		Case WM_SYSKEYDOWN
			Select wp
				'If the ALT key is pressed, do not give it to the window-
				Case 18
					bbSystemEmitOSEvent(hwnd, MSG, wp, lp, Null) 
					Return 0
			End Select
		
		Case WM_CLOSE
			fG._terminated = True
			Return 0
	End Select
	
	bbSystemEmitOSEvent(hwnd, MSG, wp, lp, Null) 
	Return DefWindowProcW( hwnd,msg,wp,lp )
End Function

'Create a correctly sized win32 window with the specified parameters.
Function createWin32Window:Int(width:Int, height:Int, depth:Int, fullscreen:Byte = 0) 
	Global _wc:WNDCLASSW
	Global ClassAtom:Int
	Local Styles:Int = 0
	
	If fullscreen = True Then
		FG_enableFullScreen(width, height, depth) 
		Styles = WS_POPUP
	Else
		Styles = WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX
	End If
		
	
	_wc = New WNDCLASSW
	_wc.style = CS_OWNDC | CS_HREDRAW | CS_VREDRAW
	_wc.lpfnWndProc = ClassWndProc
	_wc.hInstance = GetModuleHandleW(Null) 
	_wc.lpszClassName = "MyClass".ToWString() 
	_wc.cbWndExtra = DLGWINDOWEXTRA
	
	ClassAtom = RegisterClassW(_wc) 
	Local hwnd:Int = CreateWindowExW(0, "MyClass", AppTitle, Styles, 0, 0, width, height, 0, 0, GetModuleHandleW(Null), Null) 
	
	If fullscreen = False Then
		'When we size windows with createwindow, the entire window winds up being width x height, when
		'we really want the client area to be that large, meaning a larger window size in general. To approximate
		'this we make use of the win32 api through blitz and calculate new size for our window.
	
		Local rcClient:Int[4]     'RECT Structure : 0 = left , 1 = top , 2 = right , 3 = bottom
		Local rcWindow:Int[4]     'RECT
		Local ptDiff:Int[2]    	 'POINT Structure : 0 = x , 1 = y

		'Get the client and window rectangles-
		GetClientRect(hwnd, rcClient) 
		GetWindowRect(hwnd, rcWindow) 
		'DifferenceX = ( WindowRight - WindowLeft ) - ClientRight
		ptDiff[0] = (rcWindow[2] - rcWindow[0] ) - rcClient[2] 
		'DifferenceY = ( WindowBottom - WindowTop ) - ClientBottom
		ptDiff[1] = (rcWindow[3] - rcWindow[1] ) - rcClient[3] 
		'Add DifferenceX and Difference Y to the x/y window height.
		MoveWindow(hwnd, rcWindow[0], rcWindow[1], width + ptDiff[0], height + ptDiff[1], True)
		
		'If fG has not ben initialised and the icon path isn't Null then set the icon!
		Local iconPath:String = fG.getApplicationIconPath()
		If fG.isInitialised() And iconPath <> "" Then
			Local iconHandle:Int = LoadIcon(hwnd, iconPath, 0)
			SetClassLong(hwnd, - 14, iconHandle)
		End If
		
	
		
	End If
	
	'Show the window and return it's handle
	ShowWindow(hwnd, SW_SHOW) 
	Return hwnd
	
End Function

Rem
	bbdoc: Class to hold the the fG.cameraPick(...) results.
ENDREM
Type TCameraPickResult

	Rem
		bbdoc: Get whether or not TCameraPickResult has been initialised.
	ENDREM
	Global _inited:Byte = False
	
	Rem
		bbdoc: Global TVector3 to hold collision position info.
	ENDREM
	Global _collisionPos:TVector3
	
	Rem
		bbdoc: Global TVector4 to hold collision normal info.
	ENDREM
	Global _collisionNormal:TVector4
	
	Rem
		bbdoc: Global entity object to hold entity info.
	ENDREM
	Global _entity:TEntity
	
	Rem
		bbdoc: Initialise the collisionPos and collisionNormal vars.
	ENDREM
	Function init()
		_collisionPos = TVector3.createEmpty()
		_collisionNormal = TVector4.createEmpty()	
	End Function
	
	Rem
		bbdoc: Get whether or not the entity selected is null.
	ENDREM
	Function isNotNull:Byte()
		Return _entity.isNull() = False
	End Function
	
	Rem
		bbdoc: Get the position of the actual collision as a TVector3.
	ENDREM
	Function getPos:TVector3()
		Local Obj:TVector3 = New TVector3
		Obj.Pointer = _collisionPos.Pointer
		Obj.managed = True
		Return Obj
	End Function
	
	Rem
		bbdoc: Get the normal of the triangle you collided with.
	ENDREM
	Function getNormal:TVector4()
		Local Obj:TVector4 = New TVector4
		Obj.Pointer = _collisionNormal.Pointer
		Obj.managed = True
		Return Obj
	End Function
	
	Rem
		bbdoc: Get the entity collided with.  You should check to make sure TCameraPickResult.isNotNull() before proceeding.
	ENDREM
	Function getEntity:TEntity()
		Return _entity
	End Function
	
End Type

REM
	bbdoc: A line manager that can draw and modify sets of 2d and 3d lines. If you need several sets of 2d or 3d lines then you may instiate multiple versions of this type.
ENDREM
Type TLineManager

	REM
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	REM
		bbdoc: Initialise a line manager instance. You must pass in the scene manager that you want the lines to appear in.
	ENDREM
	Function Create:TLineManager(s:TSceneManager)
		Local Obj:TLineManager = New TLineManager
		Obj.Pointer = LINEMANAGER_create(s.Pointer)
		Return Obj
	End Function
	
	REM
		bbdoc: Destructor for Garbage Collection.
	ENDREM
	Method Delete()
		LINEMANAGER_delete(Self.Pointer)
	End Method
	
	REM
		bbdoc: Manual destruction method.
	ENDREM
	Method Destroy()
		LINEMANAGER_delete(Self.Pointer)
	End Method
	
	REM
		bbdoc: Draw a 2d line with relative coordinates in screen space.
	ENDREM
	Method Draw2DLine(x1:Float, y1:Float, x2:Float, y2:Float)
		LINEMANAGER_Draw2DLine(x1, y1, x2, y2, Self.Pointer)
	End Method
	
	REM
		bbdoc: Set the material to use for 2d line drawing. You may use a different material each draw call. Note that this will not change previously created line materials.
	ENDREM
	Method Set2DMaterialName(name:String)
		LINEMANAGER_Set2DMaterialName( name.ToCString() , Self.Pointer )
	End Method
	
	REM
		bbdoc: Clear all 2d lines drawn.
	ENDREM
	Method Clear2DLines()
		LINEMANAGER_Clear2DLines(Self.Pointer)
	End Method
	
	REM
		bbdoc: Set the material to use for 3d line drawing. You may use a different material each draw call. Note that this will not change previously created line materials.
	ENDREM
	Method Set3DMaterialName(name:String)
		LINEMANAGER_Set3DMaterialName(name.ToCString() , Self.Pointer)
	End Method
	
	REM
		bbdoc: Draw a 3d line relative to 0,0,0.
	ENDREM
	Method Draw3DLine(x1:Float, y1:Float, z1:Float, x2:Float, y2:Float, z2:Float)
		LINEMANAGER_Draw3DLine(x1, y1, z1, x2, y2, z2, Self.Pointer)
	End Method
	
	REM
		bbdoc: Clear all 3d lines drawn.
	ENDREM
	Method Clear3DLines()
		LINEMANAGER_Clear3DLines(Self.Pointer)
	End Method
	
	
End Type
