SuperStrict

REM
	bbdoc: The main Flow3D Module.
END REM
Module flow.main

ModuleInfo "Name: Main Flow3D Module"
ModuleInfo "Description: This is the main Flow module that will allow you to use the Ogre engine in your application."
ModuleInfo "License: Private"
ModuleInfo "Authors: Lina Adkins, Damien Sturdy and Adrian Tysoe"

Import brl.system
Import brl.linkedlist
Import pub.win32
Import brl.math

'This is the win32 import code required to load application icons
?Win32

	Import "-lshell32"
	
	Extern "Win32"
		Function LoadIcon:Int(hWnd:Int, file$z, index:Int) = "ExtractIconA@12"
		Function SetClassLong:Int(hWnd:Int, nIndex:Int, dwNewLong:Int) = "SetClassLongA@12"
		Function SetWindowText:Int(hWnd:Int, lpString$z) = "SetWindowTextA@8"
	End Extern
?

Include "flow_header.bmx"
