Rem
	bbdoc: GUI Renderer that is responsible for rendering the quads on screen that represent the GUI correctly.
ENDREM
Type TCEGUIRenderer

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create a new gui renderer by specifying the render window it will appear in.<br/>queue_id - The render queue ID your gui will use.<br/>post_queue - Post queue ID<br/>max_quads - Maximum number of quads allowed for this gui system.<br/>ogreSceneManager:TSceneManager - The scene manager this GUI should reside in.
	ENDREM
	Function Create:TCEGUIRenderer( ogreRenderWindow:TRenderWindow , queue_id:Int = 100 , post_queue:Int = 0, max_quads:Int=0 , ogreSceneManager:TSceneManager )
		Local Obj:TCEGUIRenderer = New TCEGUIRenderer
		Obj.Pointer = CEGUIRENDERER_create( ogreRenderWindow.Pointer , queue_id , post_queue , max_quads ,  ogreSceneManager.Pointer )
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		CEGUIRENDERER_delete( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the target scene manager that this GUI renderer will use to render the GUI.
	ENDREM
	Method setTargetSceneManager( ogreSceneManager:TSceneManager )
		CEGUIRENDERER_setTargetSceneManager( ogreSceneManager.Pointer ,  Self.Pointer )
	End Method
	
End Type

Rem
	bbdoc: Primary manager for CEGUI system functionality.
ENDREM
Type TCEGUISystem

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create a new TCEGUISystem by specifying the created TCEGUIRenderer.<br/>resourceProvider:Byte Ptr - A pointer to an external resource provider.<br/>xmlParser:Byte Ptr - A pointer to an external xml parser<br/>scriptModule:Byte Ptr - A pointer to an external script module<br/>configFile:String - The filename of an external config file.
	ENDREM
	Function Create:TCEGUISystem( renderer:TCEGUIRenderer , resourceProvider:Byte Ptr = Null , xmlParser:Byte Ptr = Null , scriptModule:Byte Ptr = Null , configFile:String = Null )
		Local Obj:TCEGUISystem = New TCEGUISystem
		Obj.Pointer = CEGUISYSTEM_create( renderer.Pointer ,  resourceProvider ,  xmlParser ,  scriptModule , configFile.ToCString()  )
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		CEGUISystem_delete( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the default mouse cursor by specifying its image set and image name.
	ENDREM
	Method setDefaultMouseCursor( imageset$ , image_name$ )
		CEGUISYSTEM_setDefaultMouseCursor( imageset.tocstring() , image_name.tocstring() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the default font by specifying its name.
	ENDREM
	Method setDefaultFont( Name$ )
		CEGUISYSTEM_setDefaultFont( Name.tocstring() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the GUI Sheet.The gui sheet is like a root window on which all other GUI windows reside.
	ENDREM
	Method setGUISheet( sheet:TCEGUIWindow )
		CEGUISYSTEM_setGUISheet( sheet.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the default tooltip type that will be used for all tool tips.
	ENDREM
	Method getDefaultTooltip:TCEGUITooltip()
		Local Obj:TCEGUITooltip = New TCEGUITooltip
		Obj.Pointer = CEGUISYSTEM_getDefaultTooltip( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Inject mouse movement. The mouse movement is based on a delta of how much the mouse has moved on the x and y axes.
	ENDREM
	Method injectMouseMove( x:Float , y:Float )
		CEGUISYSTEM_injectMouseMove( x , y , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Inject a mouse button being pressed. 1 = Left Mouse 2 = Right Mouse 3 = Middle Mouse.
	ENDREM
	Method injectMouseButtonDown( mouseButton:Int )
		CEGUISYSTEM_injectMouseButtonDown( mouseButton , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Inject a mouse button being up. 1 = Left Mouse 2 = Right Mouse 3 = Middle Mouse.
	ENDREM
	Method injectMouseButtonUp( mouseButton:Int )
		CEGUISYSTEM_injectMouseButtonUp( mouseButton , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Inject the time pulse. CEGUI does not keep track of time, therefor you must pass it the time to govern time based functionality like tooltips. You should pass in the time, in seconds, since the last injectTimePulse was called.
	ENDREM
	Method injectTimePulse:Int( timeElapsed:Float )
		Return CEGUISYSTEM_injectTimePulse( timeElapsed , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Inject a keyboard key up. Uses standard blitzmax values.
	ENDREM
	Method injectKeyUp:Int( key_code:Int )
		CEGUISYSTEM_injectKeyUp( key_code , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Inject a keyboard key down. Uses standard blitzmax values.
	ENDREM
	Method injectKeyDown:Int( key_code:Int )
		CEGUISYSTEM_injectKeyDown( key_code , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Inject a character using its utf32 code point.
	ENDREM
	Method injectChar( code_point:Int )
		CEGUISYSTEM_injectChar( code_point , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set default tool tip for this system by name.
	ENDREM
	Method setDefaultTooltip( tooltipType:String )
		CEGUISYSTEM_setDefaultTooltip( tooltipType.ToCString() , Self.Pointer )
	End Method
	
	
End Type


Rem
	bbdoc: Base window class for describing nondescript windows. Generally you will want to use a TCEGUIFrameWindow instead of this. 
ENDREM
Type TCEGUIWindow

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Return this window type's name as a string.
	ENDREM
	Method getType:String()
		Return String.FromCString(CEGUIWINDOW_getType(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Return this window's name as a string.
	ENDREM
	Method getName:String()
		Return String.FromCString(CEGUIWINDOW_getName(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Get whether or not this window is active and can take input.
	ENDREM
	Method isActive:Byte()
		Return CEGUIWINDOW_isActive(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Add an already created window to this one as a child.
	ENDREM
	Method addChildWindow( childWindow:TCEGUIWindow )
		CEGUIWINDOW_addChildWindow( childWindow.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the position of this window on screen with a TCEGUIUVector2.
	ENDREM
	Method setPosition(pos:TCEGUIUVector2) 
		  CEGUIWINDOW_setPosition( pos.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the position of this window on screen as a TCEGUIUVector2.
	ENDREM
	Method getPosition:TCEGUIUVector2() 
		Return TCEGUIUVector2.FromPtr(CEGUIWINDOW_getPosition(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Set the window height and width of this window with TCEGUIUDims.
	ENDREM
	Method setSize(windowWidth:TCEGUIUDim, windowHeight:TCEGUIUDim) 
		CEGUIWINDOW_setSize( windowWidth.Pointer , windowHeight.Pointer, Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the text for this window. What this means depends on the window type, but it generally means whatever the primary text for the window is.
	ENDREM
	Method SetText( text$ )
		CEGUIWINDOW_setText( text.tocstring() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether or not this window is visible. Even though this window is invisible, it can still be clickable unless you used setEnabled( False ).
	ENDREM
	Method setVisible( visible:Int )
		CEGUIWINDOW_setVisible( visible  , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns any window within this window that has active input focus. This method may also return this window if there are no active children within it.
	ENDREM
	Method getActiveChild:TCEGUIWindow()
		Local Obj:TCEGUIWindow = New TCEGUIWindow
		Obj.Pointer = CEGUIWINDOW_getActiveChild(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the transparency of this window. (0.0-1.0)
	ENDREM
	Method SetAlpha( alpha:Float )
		CEGUIWINDOW_setAlpha( alpha , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the primary text of this window.
	ENDREM
	Method GetText:String()
		Return String.fromcstring(CEGUIWINDOW_getText(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Get whether or not this window has active input focus.
	ENDREM
	Method isCapturedByThis:Byte()
		Return CEGUIWINDOW_isCapturedByThis(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not an ancestor ( parent ) of this window has active input focus.
	ENDREM
	Method isCapturedByAncestor:Byte()
		Return CEGUIWINDOW_isCapturedByAncestor(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not a child window of this window has active input focus.
	ENDREM
	Method isCapturedByChild:Byte()
		Return CEGUIWINDOW_isCapturedByChild(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set whether or not this window is enabled and can take input.
	ENDREM
	Method SetEnabled( enabled:Int )
		CEGUIWINDOW_setEnabled( enabled, Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the popup tooltip text for this window.
	ENDREM
	Method setTooltipText( tooltipText$ )
		CEGUIWINDOW_setTooltipText( tooltipText.tocstring() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the tooltip object that this window will use for its tooltip.
	ENDREM
	Method SetToolTip( tooltip:TCEGUITooltip )
		CEGUIWINDOW_setTooltip( tooltip.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Specify whether this window is hit or not by a given position (TCEGUIUVector2 ).
	ENDREM
	Method isHit:Int(Position:TCEGUIVector2) 
		Return CEGUIWINDOW_isHit( Position.Pointer , Self.Pointer )		
	End Method
	
	Rem
		bbdoc: Determine whether or not this TCEGUIWindow will be processed for rendering. Window obstruction is not counted in this check.<br>localOnly:Int Determine visibility through local properties instead of inherited properties.
	ENDREM
	Method isVisible:Int(localOnly:Int = False) 
		Return CEGUIWINDOW_isVisible( localOnly , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Displays this TCEGUIWindow.
	ENDREM
	Method show() 
		CEGUIWINDOW_show(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Hides this TCEGUIWindow.
	ENDREM
	Method hide() 
		CEGUIWINDOW_hide(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Bring this window to the top of other windows that have the same always on top setting and give it input focus.
	ENDREM
	Method activate()
		CEGUIWINDOW_activate(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Remove all input focus from this window until it is activated or re-selected in the GUI.
	ENDREM
	Method deactivate()
		CEGUIWINDOW_deactivate(Self.Pointer)
	End Method
	
	'--- TPropertySet Methods Here---'
	
	Rem
		bbdoc: Set a custom property for this window.
	ENDREM
	Method setProperty(name:String, value:String) 
		CEGUIPROPERTYSET_setProperty( name.ToCString() , value.ToCString() , Self.Pointer )
	End Method
	
End Type

Rem
	bbdoc: Describes a framed window with a title bar and potentially a close button, etc. This is the standard type of container window used in CEGUI.
ENDREM
Type TCEGUIFrameWindow Extends TCEGUIWindow

	Rem
		bbdoc: Set whether or not this window is resizeable.
	ENDREM
	Method setSizingEnabled( setting:Int )
		CEGUIFW_setSizingEnabled( setting , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether or not the frame is enabled for this window.
	ENDREM
	Method setFrameEnabled( setting:Int )
		CEGUIFW_setFrameEnabled( setting , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether or not the title bar is enabled for this window.
	ENDREM
	Method setTitleBarEnabled( setting:Int )
		CEGUIFW_setTitleBarEnabled( setting , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether or not the close button is enabled for this window.
	ENDREM
	Method setCloseButtonEnabled( setting:Int )
		CEGUIFW_setCloseButtonEnabled( setting , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether you can double-click on the title bar to "roll up" this window.
	ENDREM
	Method setRollupEnabled( setting:Int )
		CEGUIFW_setRollupEnabled( setting , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the window so that it hides when the close button is clicked.
	ENDREM
	Method hideOnClose()
		CEGUIFW_hideOnClose( Self.Pointer )
	End Method
	
End Type


Rem
	bbdoc: Base button class for all button types.
ENDREM
Type TCEGUIButtonBase Extends TCEGUIWindow

	Rem
		bbdoc: Is this button being hovered over?
	ENDREM
	Method isHovering:Int()
		Return CEGUIBB_isHovering( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Is this button pushed?
	ENDREM
	Method isPushed:Int()
		Return CEGUIBB_isPushed( Self.Pointer )
	End Method
End Type

Rem
	bbdoc: Defines the basic push button. No specialisations are set here, as you can use .isPushed easily.
ENDREM
Type TCEGUIPushButton Extends TCEGUIButtonBase
	
End Type

Rem
	bbdoc: Defines the radio botton window control.
ENDREM
Type TCEGUIRadioButton Extends TCEGUIButtonBase

	Rem
		bbdoc: Get whether or not this radio button is selected.
	ENDREM
	Method isSelected:Int()
		Return CEGUIRADIO_isSelected( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get this radio button's group ID.
	ENDREM
	Method getGroupID:Long()
		Return CEGUIRADIO_getGroupID( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get selected button that is in the same group as this button.
	ENDREM
	Method getSelectedButtonInGroup:TCEGUIRadioButton()
		Local Obj:TCEGUIRadioButton = New TCEGUIRadioButton
		Obj.Pointer = CEGUIRADIO_getSelectedButtonInGroup( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Set whether or not this button is selected.
	ENDREM
	Method SetSelected( selected:Int)
		CEGUIRADIO_setSelected( selected , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the group ID for this button.
	ENDREM
	Method setGroupID( group:Long )
		CEGUIRADIO_setGroupID( group , Self.Pointer )
	End Method

End Type

Rem
	bbdoc: Describes the CEGUI checkbox implementation.
ENDREM
Type TCEGUICheckBox Extends TCEGUIButtonBase

	Rem
		bbdoc: Get whether or not this checkbox is selected.
	ENDREM
	Method isSelected:Int()
		Return CEGUICHECKBOX_isSelected( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether or not this checkbox is selected.
	ENDREM
	Method SetSelected( selected:Int )
		CEGUICHECKBOX_setSelected( selected , Self.Pointer )
	End Method
End Type


Rem
	bbdoc: Base tab control interface.
ENDREM
Type TTabControl Extends TCEGUIWindow
	
	Rem
		bbdoc: Set the tab height for this tab control.
	ENDREM
	Method setTabHeight(height:TCEGUIUDim)
		CEGUITABCONTROL_setTabHeight(height.Pointer, Self.Pointer)
	End Method
End Type

Rem
	bbdoc:
ENDREM
Type TCEGUITabButton Extends TCEGUIButtonBase
	Method SetSelected( selected:Int )
		CEGUITAB_setSelected( selected , Self.Pointer )
	End Method
	
	Method isSelected:Int()
		Return CEGUITAB_isSelected( Self.Pointer )
	End Method
	
	Method setTargetWindow( Window:TCEGUIWindow )
		CEGUITAB_setTargetWindow( Window.Pointer , Self.Pointer )
	End Method
	
	Method getTargetWindow:TCEGUIWindow()
		Local Obj:TCEGUIWindow = New TCEGUIWindow
		Obj.Pointer = CEGUITAB_getTargetWindow( Self.Pointer )
		Return Obj
	End Method
End Type

'---------------------------


Rem
	bbdoc: This type interfaces with combo boxes and allows you to get and set selected items, etc.
ENDREM
Type TCEGUICombobox Extends TCEGUIWindow

	Rem
		bbdoc: This method simply removes all items from the combo box list.
	ENDREM
	Method resetList()
		CEGUICOMBO_resetList( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the number of items currently in this combo box.
	ENDREM
	Method getItemCount:Int()
		Return CEGUICOMBO_getItemCount(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the currently selected item in this combo box as a TCEGUIListboxItem.
	ENDREM
	Method getSelectedItem:TCEGUIListboxItem()
		Local Obj:TCEGUIListboxItem = New TCEGUIListboxItem
		Obj.Pointer = CEGUICOMBO_getSelectedItem( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get an item in this combo box by specifying its index. The item will be returned as a TCEGUIListboxItem.
	ENDREM
	Method getListboxItemFromIndex:TCEGUIListboxItem(index:Int)
		Local Obj:TCEGUIListboxItem = New TCEGUIListboxItem
		Obj.Pointer = CEGUICOMBO_getListboxItemFromIndex(index, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the index of a given item in the combo box by specifying an instance of the item as a TCEGUIListboxItem.
	ENDREM
	Method getItemIndex:Int(item:TCEGUIListboxItem)
		Return CEGUICOMBO_getItemIndex(item.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Add a list box item to this combo box.
	ENDREM
	Method addItem( item:TCEGUIListboxTextItem )
		CEGUICOMBO_addItem( item.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: If you must select an item or group of items in your combo box procedurally, this is the recommended method for selection or de-selection.<br>item:TCEGUIListboxTextItem - An instance of the list box item that you want to set as selected or not selected.<br>state:Int - The state that you want to specify for the item. True is selected and False is not selected.
	ENDREM
	Method setItemSelectState( item:TCEGUIListboxTextItem , state:Int )
		CEGUICOMBO_setItemSelectState( item.Pointer , state , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Search through this combobox for an item containing the given text. You may specify what item in the list you wish to start your search from, but make sure that it exists or your application will throw an exception!
	ENDREM
	Method findItemWithText:TCEGUIListboxTextItem( text:String , start_item:TCEGUIListboxTextItem )
		Local Obj:TCEGUIListboxTextItem = New TCEGUIListboxTextItem
		
		If start_item = Null Then
			Obj.Pointer = CEGUICOMBO_findItemWithText( text.ToCString() , Null , Self.Pointer )
		Else
			Obj.Pointer = CEGUICOMBO_findItemWithText( text.ToCString() , start_item.Pointer , Self.Pointer )
		End If
		
		Return Obj
	End Method
	
	Rem
		bbdoc: You may use this to quickly set a single selected item in your combo box. If you need multiple item selection, you must use setItemSelectState instead.
	ENDREM
	Method setSelectedItem( item:TCEGUIListboxTextItem )
		CEGUICOMBO_setSelectedItem( item.Pointer , Self.Pointer )
	End Method
	
End Type

Rem
	bbdoc: Slider implementation in CEGUI.
ENDREM
Type TCEGUISlider Extends TCEGUIWindow

	Rem
		bbdoc: Get the current value of this slider.
	ENDREM
	Method getCurrentValue:Float()
		Return CEGUISLIDER_getCurrentValue( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the max value of this slider.
	ENDREM
	Method getMaxValue:Float()
		Return CEGUISLIDER_getMaxValue( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the value step per click for this slider.
	ENDREM
	Method getClickStep:Float()
		Return CEGUISLIDER_getClickStep( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the maximum value for this slider.
	ENDREM
	Method setMaxValue( maxVal:Float )
		CEGUISLIDER_setMaxValue( maxVal , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the current value for this slider.
	ENDREM
	Method setCurrentValue( value:Float )
		CEGUISLIDER_setCurrentValue( value , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the current value step per click for this slider.
	ENDREM
	Method setClickStep( clickStep:Float )
		CEGUISLIDER_setClickStep( clickStep , Self.Pointer )
	End Method
	
End Type

Rem
	bbdoc: Describes the CEGUI Edit Box control, or the text box control.
ENDREM
Type TCEGUIEditbox Extends TCEGUIWindow

	Rem
		bbdoc: Tells us whether or not our edit box is currently selected for input.
	ENDREM
	Method hasInputFocus:Int()
		Return CEGUIEDITBOX_hasInputFocus( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get whether or not this edit box is read only.
	ENDREM
	Method isReadOnly:Int()
		Return CEGUIEDITBOX_isReadOnly( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get whether or not there is text currently masked in this edit box.
	ENDREM
	Method isTextMasked:Int()
		Return CEGUIEDITBOX_isTextMasked( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Is text in this edit box valid for its type?
	ENDREM
	Method isTextValid:Int()
		Return CEGUIEDITBOX_isTextValid( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Since text is validated via regex, you get the regex string for how the text is validated.
	ENDREM
	Method getValidationString:String()
		Local temp$
		Return temp.fromcstring( CEGUIEDITBOX_getValidationString( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Get the current position of the carat or text-cursor.
	ENDREM
	Method getCaratIndex:Int()
		Return CEGUIEDITBOX_getCaratIndex( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get where the selection in this edit box starts.
	ENDREM
	Method getSelectionStartIndex:Int()
		Return CEGUIEDITBOX_getSelectionStartIndex( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get where the selection in this edit box ends.
	ENDREM
	Method getSelectionEndIndex:Int()
		Return CEGUIEDITBOX_getSelectionEndIndex( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the overall length of the current selection.
	ENDREM
	Method GetSelectionLength:Int()
		Return CEGUIEDITBOX_getSelectionLength( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the utf32 code point used when rendering masked text.
	ENDREM
	Method getMaskCodePoint:Int()
		Return CEGUIEDITBOX_getMaskCodePoint( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the maximum text length for this edit box.
	ENDREM
	Method getMaxTextLength:Int()
		Return CEGUIEDITBOX_getMaxTextLength( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether or not this text box is read-only.
	ENDREM
	Method setReadOnly( setting:Int )
		CEGUIEDITBOX_setReadOnly( setting , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether or not text in this editbox will be rendered masked.
	ENDREM
	Method setTextMasked( setting:Int )
		CEGUIEDITBOX_setTextMasked( setting , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the regex validation string that determines whether or not we have valid input.
	ENDREM
	Method setValidationString( validation_string$ )
		CEGUIEDITBOX_setValidationString( validation_string.tocstring() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the position of the carat or text cursor in the edit box.
	ENDREM
	Method setCaratIndex( carat_pos:Int )
		CEGUIEDITBOX_setCaratIndex( carat_pos , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the current selection by specifying the start and end position of this edit box.
	ENDREM
	Method SetSelection( start_pos:Int , end_pos:Int )
		CEGUIEDITBOX_setSelection( start_pos , end_pos , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the utf32 code point to use when the text is rendered masked.
	ENDREM
	Method setMaskCodePoint( code_point:Int )
		CEGUIEDITBOX_setMaskCodePoint( code_point , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the maximum text length for this edit box.
	ENDREM
	Method setMaxTestLength( max_len:Int )
		CEGUIEDITBOX_setMaxTextLength( max_len , Self.Pointer )
	End Method
		
End Type

Rem
	bbdoc: Interface for the spinner widget. The spinner widget is a value box with up an down arrows to change the value.
ENDREM
Type TCEGUISpinner Extends TCEGUIWindow

	Rem
		bbdoc: Text Input Mode : Floating Point
	ENDREM
	Const IM_FloatingPoint:Int = 0
	
	Rem
		bbdoc: Text Input Mode : Integer Decimal
	ENDREM
	Const IM_IntegerDecimal:Int = 1
	
	Rem
		bbdoc: Text Input Mode : Hexadecimal
	ENDREM
	Const IM_Hexadecimal:Int = 2
	
	Rem
		bbdoc: Text Input Mode : Octal
	ENDREM
	Const IM_Octal:Int = 3

	Rem
		bbdoc: Get the spinner's current value.
	ENDREM	
	Method getCurrentValue:Float()
		Return CEGUISPINNER_getCurrentValue( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the step size value for this spinner.
	ENDREM
	Method getStepSize:Float()
		Return CEGUISPINNER_getStepSize( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the maximum value for this spinner.
	ENDREM
	Method getMaximumValue:Float()
		Return CEGUISPINNER_getMaximumValue( Self.Pointer )
	End Method
		
	Rem
		bbdoc: Get the minimum value required for this spinner.
	ENDREM
	Method getMinimumValue:Float()
		Return CEGUISPINNER_getMinimumValue( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the text input mode for this spinner. Check out the TCEGUISpinner.IM_ constants for more info.
	ENDREM
	Method getTextInputMode:Int()
		Return CEGUISPINNER_getTextInputMode( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the current spinner value.
	ENDREM
	Method setCurrentValue( value:Float )
		CEGUISPINNER_setCurrentValue( value, Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the current step size for this spinner.
	ENDREM
	Method setStepSize( stepSize:Float )
		CEGUISPINNER_setStepSize( stepSize , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the maximum value for this spinner.
	ENDREM
	Method setMaximumValue( maxValue:Float )
		CEGUISPINNER_setMaximumValue( maxValue , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the minimum required value for this spinner.
	ENDREM
	Method setMinimumValue( minValue:Float )
		CEGUISPINNER_setMinimumValue( minValue , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the text input mode for this spinner. Check out the TCEGUISpinner.IM_ constants for more info.
	ENDREM
	Method setTextInputMode( mode:Int )
		CEGUISPINNER_setTextInputMode( mode , Self.Pointer )
	End Method
	
End Type

Rem
	bbdoc: Interface type for the listbox widget that allows you to have a list of entries for selection.
ENDREM
Type TCEGUIListBox Extends TCEGUIWindow

	Rem
		bbdoc: Get the item that was selected first ( if you are not supporting multiple selections then this is how you will get your selected item ).
	ENDREM
	Method getFirstSelectedItem:TCEGUIListboxItem()
		Local Obj:TCEGUIListboxItem = New TCEGUIListboxItem
		Obj.Pointer = CEGUILB_getFirstSelectedItem(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Remove all the entries from the list.
	ENDREM
	Method resetList()
		CEGUILB_resetList( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Add a list item by specifying a new TCEGUIListboxItem.
	ENDREM
	Method addItem( item:TCEGUIListboxTextItem )
		CEGUILB_addItem( item.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Remove an item by specifying its TCEGUIListboxItem instance.
	ENDREM
	Method removeItem( item:TCEGUIListboxTextItem )
		CEGUILB_removeItem( item.Pointer , Self.Pointer )
	End Method
End Type

Rem
	bbdoc: This is the base class for interfacing with list box items. This class is not meant to be instantiated but, instead, extended.
ENDREM
Type TCEGUIListboxItem

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get the text that this list box item contains.
	ENDREM
	Method getText:String()
		Return String.FromCString(CEGUILBI_getText(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Set the text that this list box item contains.
	ENDREM
	Method setText(text:String)
		CEGUILBI_setText(text.ToCString() , Self.Pointer)
	End Method
End Type

Rem
	bbdoc: This class is for representing list box items that contain text only.
ENDREM
Type TCEGUIListboxTextItem Extends TCEGUIListboxItem

	Rem
		bbdoc: Constructor for list box text item that allows you to set the text.
	ENDREM	
	Function Create:TCEGUIListboxTextItem( text:String )
		Local Obj:TCEGUIListboxTextItem = New TCEGUIListboxTextItem
		Obj.Pointer = CEGUILBTI_create( text.ToCString() )
		Return Obj
	End Function
	
End Type

Rem
	bbdoc: Base class from which Menubars , PopupMenus and ItemList boxes are derived.
ENDREM
Type TCEGUIItemListBase Extends TCEGUIWindow

	Rem
		bbdoc: Sort ascending.
	ENDREM	
	Const Ascending:Int = 0
	
	Rem
		bbdoc: Sort descending.
	ENDREM
	Const Descending:Int = 1
	
	Rem
		bbdoc: No sorting, let the user do the sorting.
	ENDREM
	Const UserSort:Int = 2
	
	Rem
		bbdoc: Returns the number of items in the list as an Int.
	ENDREM
	Method getItemCount:Int()
		CEGUIILB_getItemCount( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns the  object TCEGUIItemEntry instance at the index specified in the list.
	ENDREM
	Method getItemFromIndex:TCEGUIItemEntry( index:Int )
		Local Obj:TCEGUIItemEntry = New TCEGUIItemEntry
		Obj.Pointer = CEGUIILB_getItemFromIndex( index , Self.Pointer ) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Returns and Int that is the list index for the given TCEGUIItemEntry Instance
	ENDREM
	Method getItemIndex:Int( item:TCEGUIItemEntry )
		Return CEGUIILB_getItemIndex( item.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Searches the list from the given start item with the given search string, return an object TCEGUIItemEntry if found.<br>text:String The search string <br>start_item:TCEGUIItemEntry The list item from where the search is to begin
	ENDREM
	Method findItemWithText:TCEGUIItemEntry( text:String , start_item:TCEGUIItemEntry ) 
		Local Obj:TCEGUIItemEntry = New TCEGUIItemEntry
		Obj.Pointer = CEGUIILB_findItemWithText( text.ToCString() , start_item.Pointer , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Seaches the list for list item and if found, returns true
	ENDREM
	Method isItemInList:Int( item:TCEGUIItemEntry )
		Return CEGUIILB_isItemInList( item.Pointer , Self.Pointer )		
	End Method
	
	Rem
		bbdoc: Returns true if this window is resized to fit it's content
	ENDREM
	Method isAutoResizeEnabled:Int()
		Return CEGUIILB_isAutoResizeEnabled( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns true if sorting is enabled for this list.
	ENDREM
	Method isSortEnabled:Int()
		Return CEGUIILB_isSortEnabled( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns the mode of sorting enabled for this list as an integer if the list is sorted. Constants for these modes are mapped in the TCEGUIItemListBase SortingMode constants.
	ENDREM
	Method getSortMode:Int() 
		Return CEGUIILB_getSortMode( Self.Pointer )
	End Method
	
	Rem
		bbdoc: If you have created a custom callback function for sorting, its address will be returned here. Otherwise it will return zero. This function requires you to have extended the C++ source code with a function callback for handling sorting.
	ENDREM
	Method getSortCallback:Byte Ptr() 
		Return CEGUIILB_getSortCallback( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Initialise this window  object so it can be used.Note that if you've used the TCEGUIWindowManager to create this object, it has already been initialised. 
	ENDREM
	Method initialiseComponents() 
		CEGUIILB_initialiseComponents( Self.Pointer )
	End Method
		
	Rem
		bbdoc: Clear the list.
	ENDREM
	Method resetList()
		CEGUIILB_resetList( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Add an item to the list.
	ENDREM
	Method addItem( item:TCEGUIItemEntry )
		CEGUIILB_addItem( item.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Insert an item to the list behind another specified item.<br>item:TCEGUIItemEntry The item to be inserted into the list.<br>position:TCEGUIItemEntry The TCEGUIItemEntry that we will insert item after in the list.
	ENDREM
	Method InsertItem( item:TCEGUIItemEntry , Position:TCEGUIItemEntry )
		CEGUIILB_insertItem( item.Pointer , Position.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Removes the TCEGUIItemEntry specified from the list.
	ENDREM
	Method RemoveItem( item:TCEGUIItemEntry )
		CEGUIILB_removeItem( item.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Updates the list entries and possible resorts the list when TCEGUIItemEntrys in the list have changed.<br>resort:Int Flag that states whether or not there should be an internal resort when the update is called.
	ENDREM
	Method handleUpdatedItemData( resort:Int = False )
		CEGUIILB_handleUpdatedItemData( resort , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets whether or not this TCEGUIItemListBase resizes to fit the content it contains.
	ENDREM
	Method setAutoResizeEnabled( setting:Int )
		CEGUIILB_setAutoResizeEnabled( setting , Self.Pointer )
	End Method
	
	Rem
		bbdoc: "Shrink Wrap" the TCEGUIItemListBase to fit the content it contains.
	ENDREM
	Method sizeToContent()
		CEGUIILB_sizeToContent( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Tells CEGUI Event handles that the list contents have changed.
	ENDREM
	Method endInitialisation()
		CEGUIILB_endInitialisation( Self.Pointer )
	End Method
	
	Rem
		bbdoc: "method called to perform extended laying out of attached child windows. " from CEGUI documentation.
	ENDREM
	Method performChildWindowLayout()
		CEGUIILB_performChildWindowLayout( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns a TCEGUIRect with the dimensions of the TCEGUIItemListBase.
	ENDREM
	Method getItemRenderArea:TCEGUIRect()
		Local Obj:TCEGUIRect = New TCEGUIRect
		Obj.Pointer = CEGUIILB_getItemRenderArea( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Returns a window that all objects in this TCEGUIItemListBase are attached to.
	ENDREM
	Method getContentPane:TCEGUIWindow()
		Local Obj:TCEGUIWindow = New TCEGUIWindow
		Obj.Pointer = CEGUIILB_getContentPane( Self.Pointer )
		Return Obj
	End Method

	Rem
		bbdoc: This is generally used only as an internal CEGUI Function.<br>Notify li:TCEGUIItemEntry that it has been clicked. 
	ENDREM
	Method notifyItemClicked( li:TCEGUIItemEntry )
		CEGUIILB_notifyItemClicked( li.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Notify li:TCEGUIItemEntry that its selection state has changed. This is generally used only as an internal CEGUI Function.<br> li:TCEGUIItemEntry Item to be notified.state:Int Flag whether the selection state for this item has changed.
	ENDREM
	Method notifyItemSelectState(li:TCEGUIItemEntry, State:Int) 
		CEGUIILB_notifyItemSelectState( li.Pointer , State , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Enables or disables sorting for the current TCEGUIItemListBase
	ENDREM
	Method setSortEnabled( setting:Int )
		CEGUIILB_setSortEnabled( setting , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the sorting mode.The Sorting Mode constants are available in the TCEGUIItemListBase type. 
	ENDREM
	Method setSortMode(mode:Int) 
		CEGUIILB_setSortMode( mode , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Allows the user to specify a callback function to handle sorting.To use this function you will have to create a callback function in C(++).<br>cb:Byte Ptr Address to the callback function to use.
	ENDREM
	Method setSortCallback(cb:Byte Ptr) 
		CEGUIILB_setSortCallback( cb , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Resorts the list.<br>relayout:Int Specifies if the list layout should be recalculated on sort.
	ENDREM
	Method SortList( relayout:Int=True )
		CEGUIILB_sortList(relayout, Self.Pointer) 
	End Method

End Type

Rem
	bbdoc:Base class from which Menubars And PopupMenus are derived.
ENDREM
Type TCEGUIMenuBase Extends TCEGUIItemListBase
	
	Rem
		bbdoc: Returns item spacing for the current menu.
	ENDREM
	Method getItemSpacing:Float()
		Return CEGUIMENUBASE_getItemSpacing( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns if the current menu is allowed multiple popups.
	ENDREM
	Method isMultiplePopupsAllowed:Int()
		Return CEGUIMENUBASE_isMultiplePopupsAllowed( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns the currently selected menu item. 
	ENDREM
	Method getPopupMenuItem:TCEGUIMenuItem()
		Local Obj:TCEGUIMenuItem = New TCEGUIMenuItem
		Obj.Pointer = CEGUIMENUBASE_getPopupMenuItem( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Changes the spacing magnitude of menu items.
	ENDREM
	Method setItemSpacing( spacing:Float )
		CEGUIMENUBASE_setItemSpacing( spacing , Self.Pointer )
	End Method
	

	Rem
		bbdoc: Changes the menu item that is currently open.
	ENDREM
	Method changePopupMenuItem( item:TCEGUIMenuItem )
		CEGUIMENUBASE_changePopupMenuItem( item.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the current menu to allow/deny multiple pop-ups open at once.
	ENDREM
	Method setAllowMultiplePopups( setting:Int )
		CEGUIMENUBASE_setAllowMultiplePopups( setting , Self.Pointer )
	End Method
	
End Type

Rem
	bbdoc: Type that defines menu bars.
ENDREM
Type TCEGUIMenuBar Extends TCEGUIMenuBase
End Type

Rem
	bbdoc: Class that defines Popup Menus
ENDREM
Type TCEGUIPopupMenu Extends TCEGUIMenuBase
	
	Rem
		bbdoc: Get fade in time.
	ENDREM
	Method getFadeInTime:Float()
		Return CEGUIPOPUP_getFadeInTime( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get fade out time.
	ENDREM
	Method getFadeOutTime:Float()
		Return CEGUIPOPUP_getFadeOutTime( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns true if the pop-up menu is open.
	ENDREM
	Method isPopupMenuOpen:Int()
		Return CEGUIPOPUP_isPopupMenuOpen( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set fade-in time in seconds.
	ENDREM
	Method setFadeInTime( fadetime:Float )
		CEGUIPOPUP_setFadeInTime( fadeTime , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set fade-out time in seconds.
	ENDREM
	Method setFadeOutTime( fadetime:Float )
		CEGUIPOPUP_setFadeOutTime( fadeTime , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Open pop-up menu.<br>Notify:Int Flag to set which method is used. ( Notify Parent=True  or Direct=False )
	ENDREM
	Method openPopupMenu( Notify:Int )
		CEGUIPOPUP_openPopupMenu( Notify , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Close pop-up menu.<br>Notify:Int Flag to set which method is used. ( Notify Parent=True  or Direct=False )
	ENDREM
	Method closePopupMenu( Notify:Int )
		CEGUIPOPUP_closePopupMenu( Notify , Self.Pointer )
	End Method
	
End Type

Rem
	bbdoc: Type that defines the scrolled item list base class.
ENDREM
Type TCEGUIScrolledItemListBase Extends TCEGUIItemListBase
	
	Rem
		bbdoc: Get whether or not the vertical scroll bar is always shown.
	ENDREM
	Method isVertScrollbarAlwaysShown:Int()
		Return CEGUISILB_isVertScrollbarAlwaysShown( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get whether or not the horizontal scroll bar is always shown.
	ENDREM
	Method isHorzScrollbarAlwaysShown:Int()
		Return CEGUISILB_isHorzScrollbarAlwaysShown( Self.Pointer )
	End Method

	Rem
		bbdoc: Get the vertical scroll bar instance.
	ENDREM
	Method getVertScrollbar:TCEGUIScrollbar()
		Local Obj:TCEGUIScrollbar = New TCEGUIScrollbar
		Obj.Pointer = CEGUISILB_getVertScrollbar( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the horizontal scroll bar instance.
	ENDREM
	Method getHorzScrollbar:TCEGUIScrollbar()
		Local Obj:TCEGUIScrollbar = New TCEGUIScrollbar
		Obj.Pointer = CEGUISILB_getHorzScrollbar( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Set whether or not to show the vertical scroll bar.
	ENDREM
	Method setShowVertScrollbar( mode:Int )
		CEGUISILB_setShowVertScrollbar( mode , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether or not to show the horizontal scroll bar.
	ENDREM
	Method setShowHorzScrollbar( mode:Int )
		CEGUISILB_setShowHorzScrollbar( mode , Self.Pointer )
	End Method
	
End Type

Rem
	bbdoc: Type for Item List Boxes.
ENDREM
Type TCEGUIItemListBox Extends TCEGUIScrolledItemListBase

	Rem
		bbdoc: Get the number of selected items in this item list box.
	ENDREM
	Method getSelectedCount:Int()
		Return CEGUIILBOX_getSelectedCount( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the last selected item in the item list box. If you are not allowing multiple selections this is probably all you need to use.
	ENDREM
	Method getLastSelectedItem:TCEGUIItemEntry()
		Local Obj:TCEGUIItemEntry = New TCEGUIItemEntry
		Obj.Pointer = CEGUIILBOX_getLastSelectedItem( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the first selected item in a group of items.
	ENDREM
	Method getFirstSelectedItem:TCEGUIItemEntry( start_index:Int )
		Local Obj:TCEGUIItemEntry = New TCEGUIItemEntry
		Obj.Pointer = CEGUIILBOX_getFirstSelectedItem( start_index , Self.Pointer )
		Return Obj	
	End Method
	
	Rem
		bbdoc: Get the next selected item. Starts at the first selected item and goes to the last.
	ENDREM
	Method getNextSelectedItem:TCEGUIItemEntry()
		Local Obj:TCEGUIItemEntry = New TCEGUIItemEntry
		Obj.Pointer = CEGUIILBOX_getNextSelectedItem( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the next selected item after a specified selected item. 
	ENDREM
	Method getNextSelectedItemAfter:TCEGUIItemEntry( start_item:TCEGUIItemEntry )
		Local Obj:TCEGUIItemEntry = New TCEGUIItemEntry
		Obj.Pointer = CEGUIILBOX_getNextSelectedItemAfter( start_item.Pointer , Self.Pointer )
		Return Obj	
	End Method
	
	Rem
		bbdoc: Are multiple selections allowed with this item list box?
	ENDREM
	Method isMultiSelectEnabled:Int()
		Return CEGUIILBOX_isMultiSelectEnabled( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Is an item selected in this item list box?
	ENDREM
	Method isItemSelected:Int( index:Int )
		Return CEGUIILBOX_isItemSelected( index , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether or not multiple selections are allowed.
	ENDREM
	Method setMultiSelectEnabled( state:Int )
		CEGUIILBOX_setMultiSelectEnabled( state , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Clear all selections in this item list box.
	ENDREM
	Method clearAllSelections()
		CEGUIILBOX_clearAllSelections( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Select a range of items in this item list box based on start and stop indexes.
	ENDREM
	Method selectRange( a:Int, z:Int )
		CEGUIILBOX_selectRange( a , z , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Select all items in this item list box.
	ENDREM
	Method selectAllItems()
		CEGUIILBOX_selectAllItems( Self.Pointer )
	End Method
	
End Type


Rem
	bbdoc: Type that defines a scroll bar instance. This type is not implemented fully.
ENDREM
Type TCEGUIScrollbar
	
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type


Type TCEGUIMouseCursor

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Decides whether or not this object is cleaned up by garbage collection.
	ENDREM
	Field managed:Byte
	
	Rem
		bbdoc: Create a new mouse cursor.
	ENDREM
	Function Create:TCEGUIMouseCursor()
		Local Obj:TCEGUIMouseCursor = New TCEGUIMouseCursor
		Obj.Pointer = CEGUIMC_create()
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		If Self.managed = False Then CEGUIMC_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the image that will be used for this mouse cursor.
	ENDREM
	Method setImage( image:TCEGUIImage )
		CEGUIMC_setImage( image.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the current position of mouse as a TCEGUIVector2.
	ENDREM
	Function getPosition:TCEGUIVector2() 
		Return TCEGUIVector2.FromPtr(CEGUIMC_getPosition()) 
	End Function
	
	Rem
		bbdoc: Hide this mouse cursor from view!
	ENDREM
	Method hide() 
		CEGUIMC_hide(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Show this mouse cursor!
	ENDREM
	Method show() 
		CEGUIMC_show(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set whether or not this mouse cursor will be visible.
	ENDREM
	Method setVisible(visible:Byte) 
		CEGUIMC_setVisible(visible, Self.Pointer) 
	End Method

	Rem
		bbdoc: Check whether or not this mouse cursor is visible currently.
	ENDREM
	Method isVisible:Byte() 
		Return CEGUIMC_isVisible(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the main mouse cursor instance used in the current GUI system.
	ENDREM
	Function getSingleton:TCEGUIMouseCursor() 
		Local Obj:TCEGUIMouseCursor = New TCEGUIMouseCursor
		Obj.Pointer = CEGUIMC_getSingleton() 
		Obj.managed = True
		Return Obj
	End Function
	
End Type

Rem
	bbdoc: Defines a CEGUI image. This type is currently not fully supported.
ENDREM
Type TCEGUIImage
	
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Defines a tool tip instance that appears when tooltips are enabled.
ENDREM
Type TCEGUITooltip

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create a new tool tip and specify the type and the name.
	ENDREM
	Function Create:TCEGUITooltip( tooltipType$ , Name$ )
		Local Obj:TCEGUITooltip = New TCEGUITooltip
		Obj.Pointer = CEGUITT_create( tooltipType.tocstring() , Name.tocstring() )
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		CEGUITT_delete( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set time required to hover before the tool tip appears.
	ENDREM
	Method setHoverTime( hoverTime:Float )
		CEGUITT_setHoverTime( hoverTime , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the time this tooltip is displayed before dissappearing.
	ENDREM
	Method setDisplayTime( displayTime:Float )
		CEGUITT_setDisplayTime( displayTime , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the amount of time this tooltip will fade in or out.
	ENDREM
	Method setFadeTime( fadeTime:Float )
		CEGUITT_setFadeTime( fadeTime , Self.Pointer )
	End Method
	
	
End Type


Rem
	bbdoc: A set of images in CEGUI.
ENDREM
Type TCEGUIImageset

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
End Type



Rem
	bbdoc: This represents unified dimensions using relative offset and scale metrics.
ENDREM
Type TCEGUIUDim

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Specifies whether or not this object will get cleaned up by garbage collection.
	ENDREM
	Field managed:Byte
	
	Rem
		bbdoc:  Create a TCEGUIUdim from a Byte Ptr. If you set isManaged to TRUE, then this TCEGUIUDim will not be cleaned up by Garbage Collection. Do not specify that this TCEGUIUdim is managed to true unless you are certain that it will be cleaned up.
	ENDREM
	Function FromPtr:TCEGUIUDim(Pointer:Byte Ptr, isManaged:Byte = False) 
		Local Obj:TCEGUIUDim = New TCEGUIUDim
		Obj.Pointer = Pointer
		Obj.managed = isManaged
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a new TCEGUIUDim with the specified scale and offset relative values.
	ENDREM
	Function Create:TCEGUIUDim(scale:Float, Offset:Float) 
		Return TCEGUIUDim.FromPtr(CEGUIUDIM_create(scale, Offset)) 
	End Function
	
	Rem
		bbdoc: Destructor for unmanaged instances of TCEGUIUDim.
	ENDREM
	Method Delete()
		If Self.managed = False Then CEGUIUDIM_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the scale component of this TCEGUIUDim.
	ENDREM
	Method GetScale:Float() 
		Return CEGUIUDIM_getScale(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the scale component of this TCEGUIUDim.
	ENDREM
	Method SetScale(d_scale:Float) 
		CEGUIUDIM_setScale(d_scale, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the offset component of this TCEGUIUDim.
	ENDREM
	Method GetOffset:Float() 
		Return CEGUIUDIM_getOffset(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the offset component of this TCEGUIUDim.
	ENDREM
	Method SetOffset(d_offset:Float) 
		CEGUIUDIM_setOffset(d_offset, Self.Pointer) 
	End Method
	
	Rem
		bbdoc:  Set the scale and offset components of this TCEGUIUDim.
	ENDREM
	Method setAll(d_scale:Float, d_offset:Float) 
		CEGUIUDIM_setAll(d_scale, d_offset, Self.Pointer) 
	End Method
	
End Type

Rem
	bbdoc: Type used for 2-dimensional vectors.
ENDREM
Type TCEGUIVector2

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Specifies whether or not this object will get cleaned up by garbage collection.
	ENDREM
	Field managed:Byte
	
	Rem
		bbdoc:  Create a TCEGUIVector2 from a Byte Ptr. If you set isManaged to TRUE, then this TCEGUIVector2 will not be cleaned up by Garbage Collection. Do not specify that this TCEGUIVector2 is managed to true unless you are certain that it will be cleaned up.
	ENDREM
	Function FromPtr:TCEGUIVector2(Pointer:Byte Ptr, isManaged:Byte = False) 
		Local Obj:TCEGUIVector2 = New TCEGUIVector2
		Obj.Pointer = Byte Ptr(Pointer) 
		Obj.managed = isManaged
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a new TCEGUIVector2 with the specified x and y components.
	ENDREM
	Function Create:TCEGUIVector2(d_x:Float, d_y:Float) 
		Return TCEGUIVector2.FromPtr(CEGUIVECTOR2_create(d_x, d_y)) 
	End Function
	
	Rem
		bbdoc: Destructor for unmanaged instances of TCEGUIVector2.
	ENDREM
	Method Delete()
		If managed = False Then CEGUIVECTOR2_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the X component for this TCEGUIVector2.
	ENDREM
	Method getX:Float() 
		Return CEGUIVECTOR2_getX(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the X component for this TCEGUIVector2.
	ENDREM
	Method setX:Float(d_x:Float) 
		Return CEGUIVECTOR2_setX(d_x, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the Y component for this TCEGUIVector2.
	ENDREM
	Method getY:Float() 
		Return CEGUIVECTOR2_getY(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the Y component for this TCEGUIVector2.
	ENDREM
	Method setY:Float(d_y:Float) 
		Return CEGUIVECTOR2_setY(d_y, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set both the X and Y components for this TCEGUIVector2.
	ENDREM
	Method setAll(d_x:Float, d_y:Float) 
		CEGUIVECTOR2_setAll(d_x, d_y, Self.Pointer) 
	End Method
	
	
End Type


Rem	
	bbdoc: This type represents a 2-dimensional vector that represents both size and position in unified dimensions.
ENDREM
Type TCEGUIUVector2

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Specifies whether or not this object will get cleaned up by garbage collection.
	ENDREM
	Field managed:Byte
	
	Rem
		bbdoc:  Create a TCEGUIUVector2 from a Byte Ptr. If you set isManaged to TRUE, then this TCEGUIUVector2 will not be cleaned up by Garbage Collection. Do not specify that this TCEGUIUVector2 is managed to true unless you are certain that it will be cleaned up.
	ENDREM
	Function FromPtr:TCEGUIUVector2(Pointer:Byte Ptr, isManaged:Byte = False) 
		Local Obj:TCEGUIUVector2 = New TCEGUIUVector2
		Obj.Pointer = Pointer
		Obj.managed = isManaged
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a new TCEGUIUVector2 with the specified parameters.
	ENDREM
	Function Create:TCEGUIUVector2(d_x:TCEGUIUDim, d_y:TCEGUIUDim) 
		Return TCEGUIUVector2.FromPtr(CEGUIUVECTOR2_create(d_x.Pointer, d_y.Pointer)) 
	End Function
	
	Rem
		bbdoc: Destructor for unmanaged TCEGUIVector2 instances.
	ENDREM
	Method Delete() 
		If managed = False Then CEGUIUVECTOR2_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the X component of this TCEGUIUVector2.
	ENDREM
	Method getX:TCEGUIUDim() 
		Return TCEGUIUDim.FromPtr(CEGUIUVECTOR2_getX(Self.Pointer) , True) 
	End Method
	
	Rem
		bbdoc: Set the X component of this TCEGUIVector. Notice that the values from the TCEGUIUDim are copied by value, not by reference.
	ENDREM
	Method setX(d_x:TCEGUIUDim) 
		CEGUIUVECTOR2_setX(d_x.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the Y component of this TCEGUIUVector2.
	ENDREM
	Method getY:TCEGUIUDim() 
		Return TCEGUIUDim.FromPtr(CEGUIUVECTOR2_getY(Self.Pointer) , True) 
	End Method
	
	Rem
		bbdoc: Set the Y component of this TCEGUIUVector2. Notice that the values from the TCEGUIUDim are copied by value, not by reference.
	ENDREM
	Method setY(d_y:TCEGUIUDim) 
		CEGUIUVECTOR2_setY(d_y.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set both the X and Y components of this TCEGUIUVector2. Notice that the values from the TCEGUIUDims are copied by value, not by reference.
	ENDREM
	Method setAll(d_x:TCEGUIUDim, d_y:TCEGUIUDim) 
		CEGUIUVECTOR2_setAll(d_x.Pointer, d_y.Pointer, Self.Pointer) 
	End Method

		
End Type


Rem
	bbdoc: Describes a base item in an item list.
ENDREM
Type TCEGUIItemEntry Extends TCEGUIWindow
End Type

Rem
	bbdoc: Describes a menu item.
ENDREM
Type TCEGUIMenuItem Extends TCEGUIItemEntry

	Rem
		bbdoc: Is this menu item being hovered over?
	ENDREM
	Method isHovering:Int()
		Return CEGUIMI_isHovering( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Is this menu item pushed?
	ENDREM
	Method isPushed:Int()
		Return CEGUIMI_isPushed( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Is this menu item opened?
	ENDREM
	Method isOpened:Int()
		Return CEGUIMI_isOpened( Self.Pointer )
	End Method
	
	
End Type

Rem
	bbdoc: Defines a CEGUI rectangle. This type is not yet fully implemented.
ENDREM
Type TCEGUIRect

	Rem
		bbdoc: REference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type



'Singleton Types---------------------------------------------

Rem
	bbdoc: Type that manages GUI scheme objects.
ENDREM
Type TCEGUISchemeManager

	Rem
		bbdoc: Load a scheme by its file name.
	ENDREM
	Function loadScheme( scheme$ )
		CEGUISM_loadScheme( scheme.tocstring() )
	End Function

End Type

Rem
	bbdoc: Type to manage all window instances in CEGUI.
ENDREM
Type TCEGUIWindowManager

	Rem
		bbdoc: Create a new, basic window by specifying the type and the name.
	ENDREM
	Function createWnd:TCEGUIWindow( windowType$ , windowName$ )
		Local Obj:TCEGUIWindow = New TCEGUIWindow
		Obj.Pointer = CEGUIWM_createWindow( windowType.tocstring() , windowName.tocstring() )
		Return Obj
	End Function
	
	Rem
		bbdoc: Destroy a window by giving the reference to it.
	ENDREM
	Function DestroyWindow( Window:Byte Ptr )
		CEGUIWM_destroyWindow( Window )
	End Function
	
	Rem
		bbdoc: Create a frame window by specifying the type and name.
	ENDREM
	Function CreateFrameWnd:TCEGUIFrameWindow( windowType$ , Name$ )
		Local Obj:TCEGUIFrameWindow = New TCEGUIFrameWindow
		Obj.Pointer = CEGUIWM_createFrameWindow( windowType.tocstring() , Name.tocstring() )
		Return Obj
	End Function
	
	Rem
		bbdoc: Load a window layout from a .layout file.
	ENDREM
	Function loadWindowLayout:TCEGUIWindow( layoutFile$ )
		Local Obj:TCEGUIWindow = New TCEGUIWindow
		Obj.Pointer = CEGUIWM_loadWindowLayout( layoutfile.tocstring() )
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a push button window by specifying the window type and name.
	ENDREM
	Function createPushButton:TCEGUIPushButton( windowType$ , windowName$ )
		Local Obj:TCEGUIPushButton = New TCEGUIPushButton
		Obj.Pointer = CEGUIWM_createPushButton( windowType.tocstring() , windowName.tocstring() )
		Return Obj
	End Function		 
	
	Rem
		bbdoc: Create a combobox window by specifying the window type and name. 
	ENDREM
	Function CreateComboBox:TCEGUICombobox( windowType$ , windowName$ )
		Local Obj:TCEGUICombobox = New TCEGUICombobox
		Obj.Pointer = CEGUIWM_createCombobox( windowType.tocstring() , windowName.tocstring() )
		Return Obj
	End Function 
	
	Rem
		bbdoc:Create a radio button window by specifying the window type and name. 
	ENDREM
	Function createRadioButton:TCEGUIRadioButton( windowType$ , windowName$ )
		Local Obj:TCEGUIRadioButton = New TCEGUIRadioButton
		Obj.Pointer = CEGUIWM_createRadioButton( windowType.tocstring() , windowName.tocstring() )
		Return Obj
	End Function
	
	Rem
		bbdoc:Create a check box window by specifying the window type and name. 
	ENDREM
	Function createCheckbox:TCEGUICheckBox( windowType$ , windowName$ )
		Local Obj:TCEGUICheckBox = New TCEGUICheckBox
		Obj.Pointer = CEGUIWM_createCheckbox( windowType.tocstring() , windowName.tocstring() )
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a tab button window by specifying the window type and name. 
	ENDREM
	Function createTabButton:TCEGUITabButton( windowType$ , windowName$ )
		Local Obj:TCEGUITabButton = New TCEGUITabButton
		Obj.Pointer = CEGUIWM_createTabButton( windowType.tocstring() , windowName.tocstring() )
		Return Obj
	End Function
	
	Rem
		bbdoc:Create a slider window by specifying the window type and name. 
	ENDREM
	Function CreateSlider:TCEGUISlider( windowType$ , windowName$ )
		Local Obj:TCEGUISlider = New TCEGUISlider
		Obj.Pointer = CEGUIWM_createSlider( windowType.tocstring() , windowName.tocstring() )
		Return Obj
	End Function
	
	Rem
		bbdoc: Create an edit box window by specifying the window type and name. 
	ENDREM
	Function createEditbox:TCEGUIEditbox( windowType$ , windowName$ )
		Local Obj:TCEGUIEditbox = New TCEGUIEditbox
		Obj.Pointer = CEGUIWM_createEditbox( windowType.tocstring() , windowName.tocstring() )
		Return Obj
	End Function
	
	REM
		bbdoc: Create a listbox that stores a set of text list items.
	ENDREM
	Function CreateListBox:TCEGUIListBox(windowType:String, windowName:String)
		Local Obj:TCEGUIListBox = New TCEGUIListBox
		Obj.Pointer = CEGUIWM_createListbox(windowType.ToCString() , windowName.ToCString())
		Return obj
	End Function
	
	Rem
		bbdoc: Create a spinner window by specifying the window type and name.
	ENDREM
	Function createSpinner:TCEGUISpinner( windowType$ , windowName$ )
		Local Obj:TCEGUISpinner = New TCEGUISpinner
		Obj.Pointer = CEGUIWM_createSpinner( windowType.tocstring() , windowName.tocstring() )
		Return Obj
	End Function
	
	Rem
		bbdoc: Create an item list base window by specifying the window type and name.
	ENDREM
	Function createItemListBase:TCEGUIItemListBase( windowType$ , windowName$ )
		Local Obj:TCEGUIItemListBase = New TCEGUIItemListBase
		Obj.Pointer = CEGUIWM_createItemListBase( windowType.ToCString() , windowName.ToCString() )
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a menu base window by specifying the window type and name.
	ENDREM
	Function createMenuBase:TCEGUIMenuBase( windowType$ , windowName$ )
		Local Obj:TCEGUIMenuBase = New TCEGUIMenuBase
		Obj.Pointer = CEGUIWM_createMenuBase( windowType.ToCString() , windowName.ToCString() )
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a menu bar window by specifying the window type and name. 
	ENDREM
	Function createMenuBar:TCEGUIMenuBar( windowType$ , windowName$ )
		Local Obj:TCEGUIMenuBar = New TCEGUIMenuBar
		Obj.Pointer = CEGUIWM_createMenuBar( windowType.ToCString() , windowName.ToCString() )
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a popup menu window by specifying the window type and name.
	ENDREM
	Function CreatePopupMenu:TCEGUIPopupMenu( windowType$ , windowName$ )
		Local Obj:TCEGUIPopupMenu = New TCEGUIPopupMenu
		Obj.Pointer = CEGUIWM_createPopupMenu( windowType.ToCString() , windowName.ToCString() )
		Return Obj
	End Function
	
	'Get functions---------
	
	Rem
		bbdoc: Get a push button from the window manager by name.
	ENDREM
	Function getPushButton:TCEGUIPushButton( Name:String )
		Local Obj:TCEGUIPushButton = New TCEGUIPushButton
		Obj.Pointer = CEGUIWM_getPushButton(Name.ToCString()) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Get a popup menu from the window manager by name. 
	ENDREM
	Function getPopupMenu:TCEGUIPopupMenu( Name:String )
		Local Obj:TCEGUIPopupMenu = New TCEGUIPopupMenu
		Obj.Pointer = CEGUIWM_getPopupMenu(Name.ToCString()) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Get a frame window from the window manager by name. 
	ENDREM
	Function getWindowFrameWnd:TCEGUIFrameWindow(Name:String)
		Local Obj:TCEGUIFrameWindow = New TCEGUIFrameWindow
		Obj.Pointer = CEGUIWM_getFrameWindow(Name.ToCString()) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Get a basic window from the window manager by name.
	ENDREM
	Function getWindow:TCEGUIWindow( Name:String )
		Local Obj:TCEGUIWindow = New TCEGUIWindow
		Obj.Pointer = CEGUIWM_getWindow( Name.ToCString() )
		Return Obj
	End Function
	
	Rem
		bbdoc: Get a slider from the window manager by name. 
	ENDREM
	Function getWindowSlider:TCEGUISlider( Name:String )
		Local Obj:TCEGUISlider = New TCEGUISlider
		Obj.Pointer = CEGUIWM_getSlider(Name.ToCString()) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Get a spinner from the window manager by name.
	ENDREM
	Function getWindowSpinner:TCEGUISpinner( Name:String )
		Local Obj:TCEGUISpinner = New TCEGUISpinner
		Obj.Pointer = CEGUIWM_getSpinner(Name.ToCString()) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Get a combobox from the window manager by name.
	ENDREM
	Function getWindowCombobox:TCEGUICombobox(Name:String) 
		Local Obj:TCEGUICombobox = New TCEGUICombobox
		Obj.Pointer = CEGUIWM_getCombobox(Name.ToCString()) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Get a radio button from the window manager by name.
	ENDREM
	Function getWindowRadioButton:TCEGUIRadioButton( Name:String )
		Local Obj:TCEGUIRadioButton = New TCEGUIRadioButton
		Obj.Pointer = CEGUIWM_getRadioButton(Name.ToCString()) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Get a check box from the window manager by name.
	ENDREM
	Function getWindowCheckBox:TCEGUICheckBox( Name:String )
		Local Obj:TCEGUICheckBox = New TCEGUICheckBox
		Obj.Pointer = CEGUIWM_getCheckbox(Name.ToCString()) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Get an edit box from the window manager by name.
	ENDREM
	Function getWindowEditbox:TCEGUIEditbox( Name:String )
		Local Obj:TCEGUIEditbox = New TCEGUIEditbox
		Obj.Pointer = CEGUIWM_getEditbox(Name.ToCString()) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Get an item list box from the window manager by name. 
	ENDREM
	Function getItemListBox:TCEGUIItemListBox( Name:String )
		Local Obj:TCEGUIItemListBox = New TCEGUIItemListBox
		Obj.Pointer = CEGUIWM_getItemListbox(Name.ToCString()) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Get a list box from the window manager by name.
	ENDREM
	Function getListBox:TCEGUIListBox( Name:String )
		Local Obj:TCEGUIListBox = New TCEGUIListBox
		Obj.Pointer = CEGUIWM_getListBox( Name.ToCString() )
		Return Obj
	End Function
	
	Rem
		bbdoc:
	ENDREM
	Function getMenubar:TCEGUIMenuBar(Name:String) 
		Local Obj:TCEGUIMenuBar = New TCEGUIMenuBar
		Obj.Pointer = CEGUIWM_getMenubar(Name.ToCString()) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Get a menu item from the window manager by name.
	ENDREM
	Function getMenuItem:TCEGUIMenuItem(Name:String) 
		Local Obj:TCEGUIMenuItem = New TCEGUIMenuItem
		Obj.Pointer = CEGUIWM_getMenuItem(Name.ToCString()) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Get a tab control from the window manager by name.
	ENDREM
	Function getTabControl:TTabControl(Name:String)
		Local Obj:TTabControl = New TTabControl
		Obj.Pointer = CEGUIWM_getTabControl(Name.ToCString())
		Return Obj
	End Function
	
End Type

Rem
	bbdoc: Manager that handles the cegui log file.
ENDREM
Type TCEGUILogger

	Rem
		bbdoc: Log on errors only.
	ENDREM
	Const Errors:Int = 0
	
	Rem
		bbdoc: Standard logging.
	ENDREM
	Const Standard:Int = 1

	Rem
		bbdoc: Somewhat verbose logging.
	ENDREM
	Const Informative:Int = 2
	
	Rem
		bbdoc: Very verbose logging.
	ENDREM
	Const Insane:Int = 3
	
	Rem
		bbdoc: Set the logging level. See TCEGUILogger constants for values and descriptions.
	ENDREM
	Function setLoggingLevel( level:Int )
		CEGUILOGGER_setLoggingLevel( level )
	End Function
	
	Rem
		bbdoc: Get the logging level. See TCEGUILogger constants for values and descriptions.  
	ENDREM
	Function getLoggingLevel:Int()
		Return CEGUILOGGER_getLoggingLevel()
	End Function
	
	Rem
		bbdoc: Log an event by specifying the message and level. See TCEGUILogger constants for values and descriptions.   
	ENDREM
	Function logEvent( message$ , level:Int )
		CEGUILOGGER_logEvent( message.tocstring() , level )
	End Function
	
	Rem
		bbdoc: Set the log file name.<br/>append:Int - Sets whether or not you should create a new file or append to the existing one.
	ENDREM
	Function setLogFileName( filename$ , append:Int )
		CEGUILOGGER_setLogFileName( filename.tocstring() , append )
	End Function
End Type

Rem
	bbdoc: Manages and governs fonts in the CEGUI system.
ENDREM
Type TCEGUIFontManager

	Rem
		bbdoc: Create a new font.
	ENDREM
	Function CreateFont( font$ )
		CEGUIFONTMANAGER_createFont( font.tocstring() )
	End Function
End Type

Rem
	bbdoc: Manages and governs imagesets in the CEGUI system.
ENDREM
Type TCEGUIImagesetManager

	Rem
		bbdoc: Create a new image set and give it a name.
	ENDREM
	Function createImageset:TCEGUIImageset( Name$ )
		Local Obj:TCEGUIImageset = New TCEGUIImageset
		Obj.Pointer = CEGUIISM_createImageset( Name.tocstring() )
		Return Obj
	End Function
	
End Type



'Helper Functions-

Rem
	bbdoc: Create a TCEGUIUDim with the relative value specified.
ENDREM
Function cegui_reldim:TCEGUIUDim(x:Float) 
	Return TCEGUIUDim.Create(x, 0) 
End Function

Rem
	bbdoc: Create a TCEGUIUDIM with the absolute value specified.
ENDREM
Function cegui_absdim:TCEGUIUDim(x:Float) 
	Return TCEGUIUDim.Create(0, x) 
End Function


