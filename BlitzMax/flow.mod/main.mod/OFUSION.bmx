REM
	bbdoc: Our interface for loading oFusion .osm scenes.
END REM
Type TOSMScene

	REM
		bbdoc: A list of all the OSM scenes created.
	ENDREM
	Global OSM_scenes:TList = New TList
	
	REM
		bbdoc: A pointer to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	REM
		bbdoc: Pointer to the callback.
	ENDREM
	Field CallbackPtr:Byte Ptr

	REM
		bbdoc: List of nodes loaded by this osm scene.
	ENDREM
	Field nodelist:TList = New TList
	
	REM
		bbdoc: List of entities loaded by this osm scene.
	ENDREM
	Field entitylist:TList = New TList
	
	REM
		bbdoc: List of lights loaded by this osm scene.
	ENDREM
	Field lightlist:TList = New TList
	
	REM
		bbdoc: List of cameras loaded by this osm scene.
	ENDREM
	Field cameralist:TList = New TList
	
	REM
		bbdoc: List of helpers loaded by this osm scene.
	ENDREM
	Field helperlist:TList = New TList
	
	REM
		bbdoc: List of shapes loaded by this osm scene.
	ENDREM
	Field shapelist:TList = New TList
	
	REM
		bbdoc: List of static geometry loaded by this osm scene.
	ENDREM
	Field staticgeometrylist:TList= New Tlist
	
	REM
		bbdoc: Create a new TOSMScene by specifying the current scene manager and render window. You can allow these to be auto-created if you like.
	ENDREM
	Function Create:TOSMScene( ogreSceneManager:TSceneManager=Null , ogreRenderWindow:TRenderWindow=Null  )
		Local Obj:TOSMScene = New TOSMScene
		' Ensure that types are initialized
		Local SM:Byte Ptr = Null
		Local RW:Byte Ptr = Null
		If ogreSceneManager<>Null Then SM = ogreSceneManager.Pointer
		If ogreRenderWindow<>Null Then RW = ogreRenderWindow.Pointer
		
		Obj.Pointer = OSMSCENE_Create( SM , RW )
		Obj.CallbackPtr = OSMSCENE_createCallback()
		osm_scenes.AddLast(obj)
		Return Obj
	End Function
	
	REM
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		OSMSCENE_Destroy( Self.Pointer )
		OSMSCENE_deleteCallback( Self.CallbackPtr )
	End Method
	
	REM
		bbdoc: Initialise this TOSMScene by specifying the filename of the OSM scene you want to load. Make sure that it is in a resource directory that Ogre can see!
	ENDREM
	Method initialise( filename$ )
		If Self.CallbackPtr = Null Then Notify("OSMScene.initialise(...) - Callback Pointer is Null!") 
		OSMSCENE_registerFunctions( nodeCallBack , entityCallBack , cameraCallBack , lightCallBack , nodeCallBack  , shapeCallBack , staticGeometryCallBack , CallbackPtr )
		OSMSCENE_initialise( filename.ToCString() , Self.CallbackPtr , Self.Pointer )
	End Method
	
	REM
		bbdoc: Create the initialised scene. Optionally you may specify a scene node to attach all of the created nodes to.
	ENDREM
	Method createScene( ogreSceneNode:TSceneNode = Null )
		'Ensure object is initialised
		Local SN:Byte Ptr = Null
		If ogreSceneNode <> Null Then SN = ogreSceneNode.Pointer
		
		'Initialise Global lists ready for scene creation.
		OSM_nodelist = New TList
		OSM_entitylist = New TList
		OSM_lightlist = New TList
		OSM_cameralist = New TList
		OSM_helperlist = New TList
		OSM_shapelist = New TList
		OSM_staticgeometrylist = New TList	
		
		OSMSCENE_createScene( Self.Pointer , SN )
		
		'Fill the objects lists with what was loaded
		Self.nodelist=OSM_nodelist
		Self.entitylist=OSM_entitylist
		Self.lightlist=OSM_lightlist
		Self.cameralist=OSM_cameralist
		Self.helperlist=OSM_helperlist
		Self.shapelist=OSM_shapelist
		Self.staticgeometrylist=OSM_staticgeometrylist
		
	End Method
	
	REM
		bbdoc: Get the scene manager that this TOSMScene is using.
	ENDREM
	Method getSceneManager:TSceneManager()
		Local Obj:TSceneManager = New TSceneManager
		Obj.Pointer = OSMSCENE_getSceneManager( Self.Pointer ) 
		Return Obj
	End Method
	
	REM
		bbdoc: Write the current scene to an OSM file.
	ENDREM
	Function writeScene( name:String )
		OSMSCENE_writeScene( name.ToCString() )
	End Function
	
	REM
		bbdoc: Clears the user properties out of all lights, cameras and entities that would've been placed due to having user properties loaded from the OSM scene file. WARNING: This will destroy all user property data for any TMovableObjects loaded through the OSM, regardless of whether you set them yourself or a scene loader method set them.
	ENDREM
	Method clearUserProperties() 
			'Our empty userObject-
			Local emptyObject:TUserDefinedObject = TUserDefinedObject.Create() 
			emptyObject.setCustomObject(emptyObject) 
			
			'Loop through each ent in the list and determine if it's userprops are valid
			For Local e:TEntity = EachIn Self.entityList
			
				Local userObject:TUserDefinedObject = e.getUserObject() 

				If userObject.isNull() = False Then
					'If we have a user object, then set the current userobject to this entity to Null
					'userObject.Destroy() <-- This line bugs out, making it impossible to remove created objects, thus leak-
					e.setUserObject(emptyObject) 
					
				End If
			
			Next
	End Method

End Type

Private
Global OSM_nodelist:TList
Global OSM_entitylist:TList
Global OSM_lightlist:TList
Global OSM_cameralist:TList
Global OSM_helperlist:TList
Global OSM_shapelist:TList
Global OSM_staticgeometrylist:TList

Public
	REM
		bbdoc: Standard callback for nodes on OSM scene load.
	ENDREM
	Function nodeCallBack( nodePointer:Byte Ptr )
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = nodePointer
		OSM_nodelist.AddLast(obj) 
	End Function
	
	REM
		bbdoc: Standard callback for entites on OSM scene load.
	ENDREM
	Function entityCallBack( entityPointer:Byte Ptr )
		Local Obj:TEntity = New TEntity
		Obj.Pointer = entityPointer
		OSM_entitylist.AddLast(obj) 
	End Function
	
	REM
		bbdoc: Standard callback for cameras on OSM scene load.
	ENDREM
	Function cameraCallBack( cameraPointer:Byte Ptr )
		Local Obj:TCamera = New TCamera
		Obj.Pointer = cameraPointer
		OSM_cameralist.AddLast(obj) 
	End Function

	REM
		bbdoc: Standard callback for lights on OSM scene load.
	ENDREM	
	Function lightCallBack( lightPointer:Byte Ptr )
		Local Obj:TLight = New TLight
		Obj.Pointer = lightPointer
		OSM_lightlist.AddLast(obj) 
	End Function
	
	REM
		bbdoc: Standard callback for helpers on OSM scene load.
	ENDREM
	Function helperCallBack( helperPointer:Byte Ptr )
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = helperPointer
		OSM_helperlist.AddLast(obj) 
	End Function
	
	REM
		bbdoc: Standard callback for shapes on OSM scene load.
	ENDREM
	Function shapeCallBack( shapePointer:Byte Ptr )
		Local Obj:TSimpleSpline = New TSimpleSpline
		Obj.Pointer = shapePointer
		OSM_shapelist.AddLast(obj) 
	End Function
	
	REM
		bbdoc: Standard callback for static geometry on OSM scene load.
	ENDREM
	Function staticGeometryCallBack( staticGeometryPointer:Byte Ptr )
		Local Obj:TStaticGeometry = New TStaticGeometry
		Obj.Pointer = staticGeometryPointer
	End Function
	
	
REM
	bbdoc: This type defines the OSM Serializer implementation, which allows you to save out your Ogre .OSM scenes thru Flow.	
ENDREM
Type TOSMSerializer

	REM
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	REM
		bbdoc: This creates a TOSMSerializer instance to be used.
	ENDREM
	Function Create:TOSMSerializer(sm:TSceneManager, rw:TRenderWindow) 
		Local Obj:TOSMSerializer = New TOSMSerializer
		Obj.Pointer = OSMS_create(sm.Pointer, rw.Pointer) 
		Return Obj
		'Initialize our callback functions-
		
	End Function
	
	REM
		bbdoc: Write your current Ogre scene to a .osm scene file.
	ENDREM
	Method writeScene:Int(filename:String, pCallbacks:TOSMSerializerCallbacks = Null, precision:Int = 6) 
		If pCallbacks = Null Then
			Return OSMS_writeScene(filename.ToCString() , Null, precision, Self.Pointer) 
		Else
			Return OSMS_writeScene(filename.ToCString() , pCallbacks.Pointer, precision, Self.Pointer) 
		End If
	End Method

	REM
		bbdoc: Destructor.
	END REM
	Method Delete() 
		OSMS_delete(Self.Pointer) 
	End Method
	

End Type

REM
	bbdoc: Serializer Callback type. This holds and initializes callbacks for the TOSMSerializer class. If you need further assistance with the use of this class, see "OSM Saving and Callbacks.bmx".
ENDREM
Type TOSMSerializerCallbacks

	REM
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	REM
		bbdoc: Constructor
	ENDREM
	Function Create:TOSMSerializerCallbacks() 
		Local s:TOSMSerializerCallbacks = New TOSMSerializerCallbacks
		s.Pointer = OSMSCallbacks_create() 
		Return s
	End Function
	
	REM
		bbdoc: Initialize all of your callback functions!
	ENDREM
	Method initialize(nodeFunctionPtr:Byte Ptr, meshFunctionPtr:Byte Ptr, camFunctionPtr:Byte Ptr, entityFunctionPtr:Byte Ptr, lightFunctionPtr:Byte Ptr, skeletonFunctionPtr:Byte Ptr, helperFunctionPtr:Byte Ptr) 
		OSMSCallbacks_initialize(nodeFunctionPtr, meshFunctionPtr, camFunctionPtr, entityFunctionPtr, lightFunctionPtr, skeletonFunctionPtr, helperFunctionPtr, Self.Pointer) 
	End Method
	
	REM
		bbdoc: Use this in your callbacks to add UserProperties to a given object that supports them.
	ENDREM
	Method addProperty(name:String, value:String) 
		OSMSCallbacks_addProperty(name.ToCString() , value.ToCString() , Self.Pointer) 
	End Method
	
	REM
		bbdoc: Destructor
	ENDREM
	Method Delete() 
		OSMSCallbacks_delete(Self.Pointer) 
	End Method
	
End Type
	


	

