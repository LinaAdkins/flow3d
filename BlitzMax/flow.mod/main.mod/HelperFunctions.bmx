REM
** HelperFunctions.bmx for Flow3D
** Description: HelperFunctions.bmx provides a safe place to keep track of helper functions that reside in the global
** namespace. Functions here should ONLY be functions that ease very common tasks ( such as TVector3 creation ).
**
** While it's not considered good OOP practice, some of the functions here will increase usability far more
** than they will increase confusion.
**
** Author: Lina Adkins
** Date Started: December 18th, 2008
**
** Change History:
**		(o) December 18th, 2008 : Created and added Vec3(...) , Vec4(...) and Quat(..).
ENDREM

REM
	bbdoc: Helper function to create a TVector3 quickly. You call it without any parameters to get a zero vector.<br/>The analog to this function would be TVector3.Create(x,y,z).
ENDREM
Function Vec3:TVector3(x:Float = 0.0, y:Float = 0.0, z:Float = 0.0)
	Local Obj:TVector3 = New TVector3
	Obj.Pointer = VECTOR3_create(x, y, z)
	Obj.managed = False
	Return Obj
End Function

REM
	bbdoc: Create a TRadian object quickly by specifying the value in radians. If you need to convert to radians you may use Radians( deg2rad( 180 ) ). Returns a TRadian object.
ENDREM
Function Radians:TRadian(radians:Float)
	Local Obj:TRadian = New TRadian
	Obj.Pointer = RAD_create(radians)
	Return Obj
End Function

REM
	bbdoc: Create a TDegree objection quickly by specifying the value in degrees. If you need to convert from radians you may use Degrees( rad2Deg( 1.2 ) ). Returns a TDegree object.
ENDREM
Function Degrees:TDegree(degrees:Float)
	Local Obj:TDegree = New TDegree
	Obj.Pointer = DEGREE_create(Degrees)
	Return Obj
End Function

REM
	bbdoc: Convert radians to degrees.
ENDREM
Function Rad2Deg:Float(rads:Float)
	Return rads * 57.2957795
End Function

REM
	bbdoc: Convert degrees to radians.
ENDREM
Function Deg2Rad:Float(degrees:Float)
	Return degrees * 0.0174532925
End Function