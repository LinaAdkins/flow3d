Rem
	bbdoc: Base Ogre class for handling and initialising base objects and configurations.
END REM
Type TOgre Final
	
	Rem
		bbdoc: Pointer
	END REM
	Global Pointer:Byte Ptr
	
	Rem
		bbdoc: Internal static flag to keep the user from spawning several TOgre instances. You probably shouldn't use this for anything.
	END REM
	Global _inited:Byte

	Rem
		bbdoc: Constructs the root Ogre object and allows you to specify filenames for the following:<br>The plugins cfg file is a plaintext file where you can specify plugins.<br>The Ogre config file is where you can specify your Ogre renderer configuration, and where that configuration is stored.<br>The log file is where Ogre logs it's exceptions and general log messages.
	END REM
	Function Create:TOgre(pluginFileName:String = "plugins.cfg", configFileName:String = "ogre.cfg", logFileName:String = "Ogre.log") 
		Local Obj:TOgre = New TOgre
		Obj.Pointer = OGRE_create(pluginFileName.ToCString() , configFileName.ToCString() , logFileName.ToCString()) 
		_inited = True
		Return Obj
	End Function
	
	Rem
		bbdoc: Overloaded to prevent New() operator from being used.
	END REM
	Method New() 
		If _inited = True Then Notify("Error: TOgre is a single-instance type and two instances are not supported.", True) End
	End Method
	
	Rem
		bbdoc: Destructor.
	END REM
	Method Delete() 
		_inited = False
		OGRE_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Save Ogre's current configuration to the config file.
	END REM
	Method saveConfig()
		OGRE_saveConfig(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Restores Ogre's current configuration from the config file specified in Create. If one is not found then "false" will be returned.
	END REM
	Method restoreConfig:Int() 
		Return OGRE_restoreConfig(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Displays the stock Ogre engine configuration dialog box for editing the configuration graphically. This method also propogates basic engine settings throughout the blitz environment.
	END REM
	Method showConfigDialog:Byte() 
		Return OGRE_showConfigDialog(Self.Pointer) 	
	End Method
	
	Rem
		bbdoc: Method intended to let plug-in writers and users load new render systems that are subclassed the primary render system class. This will usually be called from an extension plug-in.
	END REM
	Method addRenderSystem( newRend:TRenderSystem )
		OGRE_addRenderSystem(newRend.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Retreives a list of available render systems. This is usually used to roll your own setup dialog.
	END REM
	Method getAvailableRenderers:TRenderSystemList()
		Local Obj:TRenderSystemList = New TRenderSystemList
		Obj.Pointer = OGRE_getAvailableRenderers(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Allows you to retreive a specified render system by name. If the render system specified is not found then yourrendersystem.isNull() will be true.
	END REM
	Method getRenderSystemByName:TRenderSystem( name:String )
		Local Obj:TRenderSystem = New TRenderSystem
		Obj.Pointer = OGRE_getRenderSystemByName(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Specify which render system you wish to use for rendering. When you use showConfigDialog this is automatically called for you. Usually you will use this if you are rolling your own configuration dialog or not using one altogether. Right after calling this you should use TOgre.initialise(...).
	END REM
	Method setRenderSystem(system:TRenderSystem) 
		OGRE_setRenderSystem(system.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the render system currently being used.
	END REM
	Method getRenderSystem:TRenderSystem()
		Local Obj:TRenderSystem = New TRenderSystem
		Obj.Pointer = OGRE_getRenderSystem(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Initialise the current renderer for rendering and return a handle to the render window.<br>If you specify autoCreateWindow = True, then a render window will be automatically created with the renderers current settings. Keep in mind that you will have no way to control this window without hooking into it, or using a library. It is reccommended that you use fG or hand Ogre a Blitzmax window handle during TOgre.createRenderWindow(...) using the opts:NameValuePair settings and specify autoCreateWindow = False.
	END REM
	Method initialise(autoCreateWindow:Byte, windowTitle:String = "Ogre Render Window") 
		OGRE_initialise(autoCreateWindow, windowTitle.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Check to see if the Ogre Renderer is initialised.
	END REM
	Method isInitialised:Byte() 
		Return OGRE_isInitialised(Self.Pointer) 
	End Method
		 
	Rem
		bbdoc: Unsupported at this time.
	ENDREM
	Method addSceneManagerFactory(fact:TSceneManagerFactory)
		
	End Method

	Rem
		bbdoc:Unsupported at this time.
	EndRem
	Method removeSceneManagerFactory(fact:TSceneManagerFactory)
		
	End Method

	Rem
		bbdoc: Get meta-data on the type of scene manager specified. If you don't have the type name, you may have to iterate throught he meta data to find what you are looking for using getSceneManagerMetaDataIterator.
	END REM
	Method getSceneManagerMetaData:TSceneManagerMetaData(typeName:String) 
		Local Obj:TSceneManagerMetaData = New TSceneManagerMetaData
		Obj.Pointer = OGRE_getSceneManagerMetaData(typeName.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a meta-data iterator to iterate through all of the scene manager's meta-data. TSceneManagerMetaDataIterator is not yet implemented.
	END REM
	Method getSceneManagerMetaDataIterator:TSceneManagerMetaDataIterator() 
		Local Obj:TSceneManagerMetaDataIterator = New TSceneManagerMetaDataIterator
		Obj.Pointer = OGRE_getSceneManagerMetaDataIterator(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Creates an Ogre Scene Manager to manage objects within your scene. The default scene manager constants you can use are:<br>ST_GENERIC<br>ST_EXTERIOR_CLOSE<br>ST_EXTERIOR_FAR<br>ST_EXTERIOR_REAL_FAR<br>ST_INTERIOR<br>instanceName:String is the name you wish to give to the created scene manager instance.
	END REM
	Method createSceneManager:TSceneManager(typeMask:Int, instanceName:String = "") 
		Local Obj:TSceneManager = New TSceneManager
		Obj.Pointer = OGRE_createSceneManager(typeMask, instanceName.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Creates a SceneManager that is of the type given for the typeName.<br>instanceName:String is the name you wish to give to the created scene manager instance.
	END REM 
	Method createSceneManagerWithTypeName:TSceneManager(typeName:String, instanceName:String) 
		Local Obj:TSceneManager = New TSceneManager
		Obj.Pointer = OGRE_createSceneManagerWIthTypeName(typeName.ToCString() , instanceName.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Destroys an instance of a Scene Manager.
	END REM
	Method destroySceneManager(sm:TSceneManager) 
		OGRE_destroySceneManager(sm.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Returns the scene manager with the name specified. If you attempt to get a scene manager that does not exist an exception will be thrown.
	END REM
	Method getSceneManager:TSceneManager(Name:String) 
		Local Obj:TSceneManager = New TSceneManager
		Obj.Pointer = OGRE_getSceneManager(Name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Simply returns the TTextureManager singleton object for ease of use-
	ENDREM
	Method getTextureManager:TTextureManager() 
		Local Obj:TTextureManager
		Return Obj
	End Method
	
	Rem
		bbdoc: Simply returns the TMeshManager object for ease of use-
	ENDREM
	Method getMeshManager:TMeshManager() 
		Local Obj:TMeshManager
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets more information about an error code.<br>errorNumber:Long The error number that you would like a description for.
	ENDREM
	Method getErrorDescription:String(errorNumber:Long) 
		Local tempString:String
		Return tempString.FromCString( ROOT_getErrorDescription( errorNumber , Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Add's a listener that is called on every frame.
	ENDREM
	Method addFrameListener( ogreFrameListener:TFrameListener )
		ROOT_addFrameListener( ogreFrameListener.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Removes a frame listener from the rendering queue.
	ENDREM
	Method removeFrameListener( ogreFrameListener:TFrameListener )
		ROOT_removeFrameListener( ogreFrameListener.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Puts an item in the Queue that instructs the Ogre renderer to stop rendering.
	ENDREM
	Method queueEndRendering()
		ROOT_queueEndRendering( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Start the render queue.
	ENDREM
	Method startRendering()
		ROOT_startRendering( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Render a single frame in queue.
	ENDREM
	Method renderOneFrame()
		ROOT_renderOneFrame( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Shut down the Ogre rendering system manually. Usually fulfilled by Ogre automatically.
	ENDREM
	Method shutdown()
		ROOT_shutdown( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Adds a new location where resources are stored and allows Ogre to use them.<br>name:String The name of the resource to be added.<br>locType:String The type of location the resource is contained in ( zip , filesystem ,etc ).<br>groupName:String The resource group that this specific resource is contained in.<br>recursive:Int True if you want to have recursive directory lookup of all resources in location.
	ENDREM
	Method addResourceLocation( name:String , locType:String , groupName:String , recursive:Int )
		ROOT_addResourceLocation( name.ToCString() , locType.ToCString() , groupName.ToCString() , recursive , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Remove a resource location from Ogre's list of resource locations.
	ENDREM
	Method removeResourceLocation( name:String , groupName:String )
		ROOT_removeResourceLocation( name.ToCString()  , groupName.ToCString() , Self.Pointer )
	End Method

	Rem
		bbdoc: Converts the Color Value given to one that is usable with the currrent render system.<br>pDest:Int Var Unsigned 32-bit integer where your converted value will be stored.
	ENDREM
	Method convertColourValue( colour:TColourValue , pDest:Int Var )
		ROOT_convertColourValue( colour.Pointer , Varptr( pDest ) , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns a render window that was automaticallly created.
	ENDREM
	Method getAutoCreatedWindow:TRenderWindow() 
		Local Obj:TRenderWindow = New TRenderWindow
		Obj.Pointer = ROOT_getAutoCreatedWindow( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Manually creates and/or binds the Ogre renderer to a valid native OS window.<br/>name:String The name/caption of the TRenderWindow created.<br/>width The width of the TRenderWindow in pixels.<br/>height The height of the TRenderWindow in pixels.<br/>fullScreen:Byte Flag specifying whether the TRenderWindow is a full screen window or not.<br/>opts:TNameValuePairList=0 A name value pair list that handles specific options for the renderer, such as the resolution, PerfHUD toggle, and external window handles.
	ENDREM
	Method createRenderWindow:TRenderWindow(Name:String, width:Int, Height:Int, fullScreen:Byte, opts:TNameValuePairList) 
		 Local Obj:TRenderWindow = New TRenderWindow
		 Obj.Pointer = ROOT_createRenderWindow(name.ToCString() , width, height, fullScreen, opts.Pointer, Self.Pointer)
		 Return Obj
	End Method
	
	Rem
		bbdoc: Destroys a TRenderTarget.
	ENDREM
	Method detachRenderTarget( ogreRenderTarget:TRenderTarget )
		ROOT_detachRenderTarget( ogreRenderTarget.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Destroys a render target using the name as the specifier.
	ENDREM
	Method detachRenderTargetByName( name:String )
		ROOT_detachRenderTargetByName( name.ToCString() , Self.Pointer )
	End Method
	

	Rem
		bbdoc: Returns a TRenderTarget using it's name as the specifier.
	ENDREM
	Method getRenderTarget:TRenderTarget(Name:String) 
		Local Obj:TRenderTarget = New TRenderTarget
		Obj.Pointer = ROOT_getRenderTarget( name.ToCString() , Self.Pointer )
		Return Obj
	End Method
	

	Rem
		bbdoc: Loads an available Ogre Plugin for use.
	ENDREM
	Method loadPlugin( name:String )
		ROOT_loadPlugin( name.ToCString() , Self.Pointer )
	End Method
	

	Rem
		bbdoc: Unloads an Ogre Plugin from our Ogre instance.
	ENDREM
	Method unloadPlugin( name:String )
		ROOT_unloadPlugin( name.ToCString() , Self.Pointer )
	End Method
	

	Rem
		bbdoc: Installs/Registers DLL and static-linked plugins.
	ENDREM
	Method installPlugin( ogrePlugin:TPlugin )
		ROOT_installPlugin( ogrePlugin.Pointer , Self.Pointer )
	End Method
	

	Rem
		bbdoc: Unregisters a DLL or static-linked plugin.
	ENDREM
	Method uninstallPlugin( ogrePlugin:TPlugin )
		ROOT_uninstallPlugin( ogrePlugin.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retrieve the main Ogre timer for this instance.
	ENDREM
	Method getTimer:TOgreTimer() 
		Local Obj:TOgreTimer = New TOgreTimer
		Obj.Pointer = ROOT_getTimer(Self.Pointer) 
		Obj.Managed = True
		Return Obj
	End Method
	
	Rem
		bbdoc: Internal method, can use to construct your own rendering loops if desired.<br>evt:TFrameEvent Custom TFrameEvent to be used for timing.<br>Returns false if a Frame Listener wants to terminate frame.
	ENDREM
	Method _fireFrameStartedWithEvent:Int(evt:TFrameEvent) 
		Return ROOT__fireFrameStartedWithEvent( evt.Pointer , Self.Pointer )
	End Method
	

	Rem
		bbdoc: Internal method, can use to construct your own rendering loops if desired.Custom TFrameEvent to be used for timing.<br>Returns false if a Frame Listener wants to terminate frame.
	ENDREM
	Method _fireFrameEndedWithEvent:Int(evt:TFrameEvent) 
		Return ROOT__fireFrameEndedWithEvent( evt.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Internal method, can use to construct your own rendering loops if desired. Returns false if a frame listener wants to terminate this frame.
	ENDREM
	Method _fireFrameStarted:Int() 
		Return ROOT__fireFrameStarted( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Internal method, can use to construct your own rendering loops if desired.  Returns false if a frame listener wants to terminate this frame.
	ENDREM
	Method _fireFrameEnded:Int() 
		Return ROOT__fireFrameEnded( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retreive next frame's number.
	ENDREM
	Method getNextFrameNumber:Long()
		Return ROOT_getNextFrameNumber(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Gets the current scene manager in use with this instance of Ogre.
	ENDREM
	Method _getCurrentSceneManager:TSceneManager() 
		Local Obj:TSceneManager = New TSceneManager
		Obj.Pointer = ROOT__getCurrentSceneManager( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the current TSceneManager for this instance of Ogre.
	ENDREM
	Method _setCurrentSceneManager( sm:TSceneManager )
		ROOT__setCurrentSceneManager( sm.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Updates TRenderTargets.
	ENDREM
	Method _updatAllRenderTargets()
		ROOT__updateAllRenderTargets( Self.Pointer )	
	End Method
	
	Rem
		bbdoc: Create a new TRenderQueue invocation sequence.
	ENDREM
	Method createRenderQueueInvocationSequence:TRenderQueueInvocationSequence(Name:String) 
		Local Obj:TRenderQueueInvocationSequence = New TRenderQueueInvocationSequence
		Obj.Pointer = ROOT_createRenderQueueInvocationSequence( name.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Find an existing invocation sequence.
	ENDREM
	Method getRenderQueueInvocationSequence:TRenderQueueInvocationSequence(Name:String) 
		Local Obj:TRenderQueueInvocationSequence = New TRenderQueueInvocationSequence
		Obj.Pointer = ROOT_getRenderQueueInvocationSequence( name.ToCString() , Self.Pointer )
		Return Obj
	End Method
	

	Rem
		bbdoc: Destroys an invocation sequence.
	ENDREM
	Method destroyRenderQueueInvocationSequence( name:String )
		ROOT_destroyRenderQueueInvocationSequence(Name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Destroys all invocation sequences.
	ENDREM
	Method destroyAllRenderQueueInvocationSequences()
		ROOT_destroyAllRenderQueueInvocationSequences( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Clears all past event itmes.
	ENDREM
	Method clearEventTimes()
		ROOT_clearEventTimes( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Smoothes out the framerate timings by including more frames averaged per sample.<br>period:Float Smoothing period in seconds.
	ENDREM
	Method setFrameSmoothingPeriod( period:Float )
		ROOT_setFrameSmoothingPeriod( period , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retrieve the frame smoothing period is a float.<br>period:Float Smoothing period in seconds.
	ENDREM
	Method getFrameSmoothingPeriod:Float() 
		Return ROOT_getFrameSmoothingPeriod( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Allows you to include a custom MovableObjectFactory for spawning many custom objects in a scene.<br>factory:TMovableObjectFactory Add a factory to the scene for creating many custom MovableObjects.<br>overrideExisting:Int Override current MovableObjectFactory, true or false?
	ENDREM
	Method addMovableObjectFactory( factory:TMovableObjectFactory , overrideExisting:Int )
		ROOT_addMovableObjectFactory( factory.Pointer , overrideExisting , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Destroy a movableobjectfactory.
	ENDREM
	Method removeMovableObjectFactory( factory:TMovableObjectFactory )
		ROOT_removeMovableObjectFactory( factory.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Checks to see if a type of MovableObject has a corresponding MovableObject factory. True or False.
	ENDREM
	Method hasMovableObjectFactory:Int( typeName:String )
		Return ROOT_hasMovableObjectFactory( typeName.ToCString() , Self.Pointer )
	End Method
	

	Rem
		bbdoc: Gets a TMovableObjectFactory for a corresponding TMovableObject type.
	ENDREM
	Method getMovableObjectFactory:TMovableObjectFactory(typeName:String) 
		Local Obj:TMovableObjectFactory = New TMovableObjectFactory
		Obj.Pointer = ROOT_getMovableObjectFactory( typeName.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: " Allocate the Next MovableObject Type flag." - Ogre Manual
	ENDREM
	Method _allocatedNextMovableObjectTypeFlag:Int() 
		Return ROOT__allocateNextMovableObjectTypeFlag( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Helper method that returns whether or not TOgre's current renderer is in FullScreen mode.
	ENDREM
	Method getFullScreen:Byte() 
		Return OGRE_getFullScreen(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Helper method that returns TOgre's current renderer's bit-depth as an integer. Returns 0 if there was an error detecting the renders ystem.
	ENDREM
	Method getBitDepth:Int() 
		Return OGRE_getBitDepth(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Helper method that returns TOgre's current renderer's width in pixels. 
	ENDREM
	Method getWidth:Int() 
		Return OGRE_getWidth(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Helper method that returns TOgre's current renderer's height in pixels. Returns 0 if there was an error detecting the render system.
	ENDREM
	Method getHeight:Int() 
		Return OGRE_getHeight(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Helper method that returns TOgre's current renderer's anti-aliasing settings. Returns NULL if there was an error detecting the render system.
	ENDREM
	Method getAntiAliasingSettings:String() 
		Return String.FromCString( OGRE_getAntiAliasingSettings( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Helper method to get the current NVPerfHud setting. Returns "yes" if NVPerfHUD is allowed and "no" if it isn't.
	ENDREM
	Method getNVPerfHudSetting:String()
		Return String.FromCString(OGRE_getNVPerfHudSetting(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Helper method to get the current VSync setting. Returns "yes" if enabled and "no" if it isn't.
	ENDREM
	Method getVsyncEnabled:String()
		Return String.FromCString(OGRE_getVsyncEnabled(Self.Pointer))
	End Method
	
	
End Type

Rem
	bbdoc: TRenderSystems classify the behavior and structure of a Rendering API within Ogre.
ENDREM
Type TRenderSystem
	
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get the name of this TRenderSystem.
	ENDREM
	Method getName:String() 
		Local tempString:String
		Return tempString.FromCString( RS_getName( Self.Pointer ) )	
	End Method
	
	Rem
		bbdoc: Sets a configuration option name and value for the TRenderSystem.
	ENDREM
	Method setConfigOption(Name:String, Value:String) 
		RS_setConfigOption( name.ToCString() , value.ToCString() , Self.Pointer )	
	End Method
	
	Rem
		bbdoc: Create an object that allows you to perform THardwareOcclusionQueries for this TRenderSystem.
	ENDREM
	Method createHardwareOcclusionQuery:THardwareOcclusionQuery() 
		Local obj:THardwareOcclusionQuery = New THardwareOcclusionQuery
		Obj.Pointer = RS_createHardwareOcclusionQuery( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Destroy a THardwareOcclusionQuery object.
	ENDREM
	Method destroyHardwareOcclusionQuery(ogreHOQ:THardwareOcclusionQuery) 
		RS_destroyHardwareOcclusionQuery( ogreHOQ.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Validate the Config Options selected for this TRenderingSystem. If there are any problems, return them in the string. If the string is empty there were no problems.
	ENDREM
	Method validateConfigOptions:String() 
		Local tempString:String
		Return tempString.FromCString( RS_ValidateConfigOptions( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Initialises the currently selected TRenderSystem for use.<br>autoCreateWindow:Int True if you wish to have the window automatically created after it is initialised.<br>windowTitle:String The title of the TRenderWindow.<br>The TRenderWindow created by the parameters of the initialise function.
	ENDREM
	Method _initialise:TRenderWindow(autoCreateWindow:Int, windowTitle:String = "OGRE Render Window")
		Local Obj:TRenderWindow = New TRenderWindow
		Obj.Pointer = RS__initialise(autoCreateWindow, windowTitle.ToCString() , Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Set how the vertices are wind. This is generally based on the direction that the vertices are wound, and the default is CULL_CLOCKWISE. You may choose from CULL_CLOCKWISE , CULL_ANTICLOCKWISE , CULL_BACKFACES , CULL_FRONTFACES and CULL_NONE.
	ENDREM
	Method _setCullingMode( mode:Int )
		RS__setCullingMode( mode , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Check to see whether or not this TRenderSystem is a null-reference.
	ENDREM
	Method isNull:Byte() 
		If Self.Pointer = Null Then
			Return True
		Else
			Return False
		End If
	End Method
	
	
End Type

Rem
	bbdoc: Interface that describes a surface on which rendering can occur.
ENDREM
Type TRenderTarget

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get the name of this TRenderTarget.
	ENDREM
	Method getName:String()
		Local tempString:String
		Return tempString.FromCString( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Stores many characteristics of the TRenderTarget to the variables provided in the parametrs.<br>width:Int Var Variable used to store the width of the TRenderTarget.<br>height:Int Var Variable used to store the height of the TRenderTarget.<br>colourDepth:Int Var Variable used to store the colour depth  of the TRenderTarget.
	ENDREM
	Method getMetrics(width:Int Var, Height:Int Var, colourDepth:Int Var) 
		RT_getMetrics( Varptr( width )  , Varptr( height ) , Varptr( colourDepth ) , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retreives the width of the TRenderTarget.
	ENDREM
	Method getWidth:Int() 
		Return RT_getWidth( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retreives the height  of the TRenderTarget.
	ENDREM
	Method getHeight:Int() 
		Return RT_getHeight( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retreives the colour depth  of the TRenderTarget.
	ENDREM
	Method getColourDepth:Int() 
		Return RT_getColourDepth( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Inform the TRenderTarget that it needs to update it's contents. This is called automatically for you usually, but you can use this to selectively update render targets as needed.
	ENDREM
	Method update()
		RT_update( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Used to get platform-specific window attributes. Only use this if you know what you're doing.<br/>name:String The name of the attribute you want to get.<br/>pData:Long Var Pre-initialized Long used for storing the value you get from calling getCustomAttribute. 
	ENDREM
	Method getCustomAttribute( name:String , pData:Long Var )
		RT_getCustomAttribute( name , Varptr( pData ) , Self.Pointer )
	End Method
	

	Rem
		bbdoc: Add a viewport to the TRenderTarget.<br/>cam:TCamera The camera that will be projected onto the created viewport.<br/>ZOrder:Int (Optional) Specifies the "depth" of the viewport on the render target. Viewports with higher ZOrders will be displayed on top of ones with lower ZOrders.<br/>left:Float (Optional) Relative position of the left of the viewport from the render target. (0.0-1.0)<br/>top:Float (Optional) Relative position of the viewport from the top of the render target. (0.0-1.0)<br/>The relative width of the viewport to the render target. (0.0-1.0)<br/>The relative height of the viewport to the render target(0.0-1.0).
	ENDREM	
	Method addViewPort:TViewport(cam:TCamera, ZOrder:Int = 0, offsetLeft:Float = 0.0, offsetTop:Float = 0.0, vp_width:Float = 1.0, vp_height:Float = 1.0)
		Local Obj:TViewport = New TViewport
		Obj.Pointer = RT_addViewport(cam.Pointer, ZOrder, offsetLeft, offsetTop, vp_width, vp_height, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Retreives a viewport on this render target by the given index ( starts at 0 ).
	ENDREM
	Method GetViewport:TViewport(index:Int) 
		Local Obj:TViewport = New TViewport
		Obj.Pointer = RT_getViewport(index, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Writes the contents of the render target to a time stamped file with <prefix><time><suffix>. You can use this to save out screenshots of the render target's contents by specifying a .jpg or other file format suffix. 
	ENDREM
	Method writeContentsToTimeStampedFile:String( fileName:String , fileExtension:String )
		Local temp:String
		Return temp.FromCString( RT_writeContentsToTimeStampedFile( fileName , fileExtension , Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Writes the contents of the render target to a file of a specified name. You can use this to take screenshots by specifying a file name like filename.jpg.
	ENDREM
	Method writeContentsToFile( filename:String )
		RT_writeContentsToFile( filename.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the number of triangles being rendered in this render target.
	ENDREM
	Method getTriangleCount:Int() 
		Return RT_getTriangleCount( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the last FPS since this target was updated. Note that if you want a stable FPS readout you may want to try .getAverageFPS() instead.
	ENDREM
	Method getLastFPS:Float()
		Return RT_getLastFPS(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the average FPS of this render target
	ENDREM
	Method getAverageFPS:Float()
		Return RT_getAverageFPS( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the pixel colour as a TColourValue for this render target at a specified 2D coordinate.
	ENDREM
	Method getPixelColour:TColourValue(x:Int, y:Int)
		Return TColourValue.FromPtr(RT_getPixelColour(x, y, Self.Pointer))
	End Method
	
End Type

Rem
	bbdoc: This type manages render windows. A specialisation of TRenderTarget.
ENDREM
Type TRenderWindow Extends TRenderTarget


	
	Rem
		bbdoc: Every render window is double buffered, thus this function allows you to swap the buffers when you prefer to, rather than when another frame is rendered.<br>waitForVSync:Int Wait for the current monitor raster operation to stop before swapping. This will slow down your rendering and limit your fps to the refresh rate, but will prevent graphical artifacting, such as tearing.
	ENDREM
	Method swapBuffers(waitForVSync:Byte) 
		RW_swapBuffers(waitForVSync, Self.Pointer) 
	End Method

End Type
  
Rem
	bbdoc: NOT SUPPORTED: This class/type is not implemented yet.
ENDREM
Type TRenderTexture Extends TRenderTarget
	
End Type

Rem
	bbdoc: NOT SUPPORTED: This class/type is not implemented yet.
ENDREM
Type TMultiRenderTarget Extends TRenderTarget
End Type

Rem
	bbdoc: The generic Scene Manager implementation. Organizes object heirarchy within a scene, working in conjuction with TRenderQueues.
ENDREM
Type TSceneManager

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Prefab Type : Plane
	ENDREM
	Const PT_PLANE:Int = 0
	
	Rem
		bbdoc: Prefab Type : Cube
	ENDREM
	Const PT_CUBE:Int = 1
	
	Rem
		bbdoc: Prefab Type : Sphere
	ENDREM
	Const PT_SPHERE:Int = 2	
	
	Rem
		bbdoc: Special Case Render Queue Mode : Render only the queues in the SCRQM.
	ENDREM
	Const SCRQM_INCLUDE:Int = 0
	
	Rem
		bbdoc: Special Case Render Queue Mode : Render only the queues NOT in the SCRQM.
	ENDREM
	Const SCRQM_EXCLUDE:Int = 1
	
	Rem
		bbdoc: Special Illumination Render Stage : No special illumination occuring.
	ENDREM
	Const IRS_NONE:Int = 0
	
	Rem
		bbdoc: Special Illumination Render Stage : Render to texture for texture based shadows.
	ENDREM
	Const IRS_RENDER_TO_TEXTURE:Int = 1
	
	Rem
		bbdoc: Special Illumination Render Stage : Render shadows to shadow receivers.
	ENDREM
	Const IRS_RENDER_RECEIVER_PASS:Int = 2
	
	
	Rem
		bbdoc: Gets the name of this TSceneManager instance.
	ENDREM
	Method getName:String() 
		Local name:String
		Return name.FromCString( SM_getName( Self.Pointer ))
	End Method

	Rem
		bbdoc: Gets this TSceneManager's type as a name.
	ENDREM
	Method getTypeName:String() 
		Local name:String
		Return name.FromCString( SM_getTypeName( Self.Pointer ))
	End Method
	
	Rem
		bbdoc: Creates a camera managed by this TSceneManager from a given name.
	ENDREM
	Method createCamera:TCamera(Name:String) 
		Local Obj:TCamera = New TCamera
		Obj.Pointer = SM_createCamera( name.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the TCamera object of a given name.
	ENDREM
	Method getCamera:TCamera(Name:String) 
		Local Obj:TCamera = New TCamera
		Obj.Pointer = SM_getCamera(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Check to see whether a camera of the given name exists.
	ENDREM
	Method hasCamera:Int(Name:String) 
		Return SM_hasCamera(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Destroy a TCamera by passing a the TCamera object you wish to destroy.
	ENDREM
	Method destroyCamera(cam:TCamera) 
		SM_destroyCamera( cam.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Destroy a TCamera by passing the name of the TCamera you wish to destroy.
	ENDREM
	Method destroyCameraWithName(Name:String) 
		SM_destroyCameraWithName(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Destroy all cameras in this TSceneManager.
	ENDREM
	Method destroyAllCameras() 
		SM_destroyAllCameras( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Creates a light that is managed by this TSceneManager. You may choose and type and adjust this light, after creation, directly.
	ENDREM
	Method createLight:TLight(Name:String) 
		Local Obj:TLight = New TLight
		Obj.Pointer = SM_createLight(name.toCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets a light from the specified name.
	ENDREM
	Method getLight:TLight(Name:String) 
		Local Obj:TLight = New TLight
		Obj.Pointer = SM_getLight(name.toCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Checks whether or not a light of the specified name exists.
	ENDREM
	Method hasLight:Int(Name:String) 
		Return SM_hasLight(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove the TLight object from the scene and destroy it.
	ENDREM
	Method destroyLight(light:TLight) 
		SM_destroyLight( light.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Specify a TLight to remove and destroy by name.
	ENDREM
	Method destroyLightWithName(Name:String) 
		SM_destroyLightWithName( name.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Remove and destroy all lights from this scene.
	ENDREM
	Method destroyAllLights() 
		SM_destroyAllLights(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Increase the lights dirty counter manually, this is an advanced method so only use it if you know what you're doing. You would use this if you need to manually update light positions, etc.
	ENDREM
	Method _notifyLightsDirty() 
		SM__notifyLightsDirty(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the lights dirty counter, which specifies when to repopulate the used light list, or to use a cached one. The lights dirty counter will increase when lights position, or other properties are changed. 
	ENDREM
	Method _getLightsDirtyCounter:Int() 
		Return SM__getLightsDirtyCounter(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED : Get a TLightList of lights that are affecting the current frustum.
	ENDREM
	Method _getLightsAffectingFrustum:TLightList() 
		Local Obj:TLightList = New TLightList
		Obj.Pointer = SM__getLightsAffectingFrustum(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Populate a given lightlist with a set of lights in the scene when given a point and a radius to select them from.<br>position:TVector3 The position to project the radius out from for the selection.<br>radius:Float The outer-radius used to determine whether lights are in the selection range or not.<br>destList:TLightList The TLightList where the lights will be stored.
	ENDREM
	Method _populateLightList(position:TVector3, radius:Float, destList:TLightList) 
		SM__populateLightList(position.Pointer, radius, destList.Pointer) 
	End Method
	
	Rem
		bbdoc: Create a scenenode inside this TSceneManager. The node is not attached to the scene, and has a generated name.
	ENDREM
	Method createSceneNode:TSceneNode() 
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = SM_createSceneNode(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Create an unattached TSceneNode with a given name.
	ENDREM
	Method createSceneNodeWithName:TSceneNode(Name:String) 
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = SM_createSceneNodeWithName(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Destroy a TSceneNode by specifying it's name.
	ENDREM
	Method destroySceneNode(Name:String) 
		SM_destroySceneNode( name.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the root node that is at the base of the scene.
	ENDREM
	Method getRootSceneNode:TSceneNode() 
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = SM_getRootSceneNode( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets a TSceneNode from this scene by name.
	ENDREM
	Method getSceneNode:TSceneNode(Name:String) 
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = SM_getSceneNode( name.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Checks to see whether a TSceneNode with the specified name exists.
	ENDREM
	Method hasSceneNode:Int(Name:String) 
		Return SM_hasSceneNode(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Create an entity, which is basically a mesh instance. The TEntity will not be attached to a TSceneNode until you do yournode.attachObject( yourEntity ).<br>entityMesh:String A handle to the mesh resource.
	ENDREM
	Method createEntity:TEntity(entityName:String, entityMesh:String) 
		Local Obj:TEntity = New TEntity
		Obj.Pointer = SM_createEntity(entityName.toCString() , entityMesh.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Create an entity with a prefabricated mesh shape, rather than a custom mesh resource. The TEntity will not be attached to a TSceneNode until you do yournode.attachObject( yourEntity ).<br>ptype:Int The Prefab type you wish to use for this TEntity. The list of prefab constants is below:<br/>TSceneManager.PT_PLANE<br/>TSceneManager.PT_CUBE<br/>TSceneManager.PT_SPHERE
	ENDREM
	Method createEntityWithPrefab:TEntity(entityName:String, ptype:Int) 
		Local Obj:TEntity = New TEntity
		Obj.Pointer = SM_createEntityWithPrefab(entityName.toCString() , ptype, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a specific entity in this TSceneManager specified by name.
	ENDREM
	Method getEntity:TEntity(Name:String) 
		Local Obj:TEntity = New TEntity
		Obj.Pointer = SM_getEntity(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Checks to see whether or not a specific entity exists by name.
	ENDREM
	Method hasEntity:Int(Name:String) 
		Return SM_hasEntity(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Removes and destroys a specific TEntity from the scene, specified by object.
	ENDREM
	Method destroyEntity(ent:TEntity) 
		SM_destroyEntity(ent.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Removes and destroys a specific TEntity from the scene, specified by name.
	ENDREM
	Method destroyEntityWithName(Name:String) 
		SM_destroyEntityWithName(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Removes And destroys all TEntities in the scene.	
	ENDREM
	Method destroyAllEntities() 
		SM_destroyAllEntities( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Note: Manual Objects are not yet supported! Creates a TManualObject within this TSceneManager. A manual object allows you to add and specify geometry manually.
	ENDREM
	Method createManualObject:TManualObject(Name:String) 
		Local Obj:TManualObject = New TManualObject
		Obj.Pointer = SM_createManualObject(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Note: Manual Objects are not yet supported! Gets a TManualObject by name.
	ENDREM
	Method getManualObject:TManualObject(Name:String) 
		Local Obj:TManualObject = New TManualObject
		Obj.Pointer = SM_getManualObject(name.ToCString() , Self.Pointer) 
		Return Obj	
	End Method
	
	Rem
		bbdoc: Check whether or not a specified TManualObject exists, by name.
	ENDREM
	Method hasManualObject:Int(Name:String) 
		Return SM_hasManualObject(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Removes and destroys a specific TManualObject from the scene, specified by object.
	ENDREM
	Method destroyManualObject(obj:TManualObject) 
		SM_destroyManualObject(obj.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Removes and destroys a specific TManualObject from the scene, specified by name.
	ENDREM
	Method destroyManualObjectWithName(Name:String) 
		SM_destroyManualObjectWithName(name.ToCString(), Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy all TManualObjects in this scene.
	ENDREM
	Method destroyAllManualObjects() 
		SM_destroyAllManualObjects(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Creates a TBillboard chain in this scene. You can use this to link many TBillboards together.
	ENDREM
	Method createBillboardChain:TBillboardChain(Name:String) 
		Local Obj:TBillboardChain = New TBillboardChain
		Obj.Pointer = SM_createBillboardChain(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a TBillboardChain by specifying the name. If a billboard chain of the specified name does not exist, an exception will be thrown. Use yourSM.hasBillboardChain().
	ENDREM
	Method getBillboardChain:TBillboardChain(Name:String) 
		Local Obj:TBillboardChain = New TBillboardChain
		Obj.Pointer = SM_getBillboardChain(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Check to see if a TBillboardChain with a specified name exists.
	ENDREM
	Method hasBillboardChain:Int(Name:String) 
		Return SM_hasBillboardChain(name.ToCString(), Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Removes and destroys the TBillboardChain given.
	ENDREM
	Method destroyBillboardChain(obj:TBillboardChain) 
		SM_destroyBillboardChain(obj.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Removes and destroys the TBillboard chain specified by name.
	ENDREM
	Method destroyBillboardChainWithName(Name:String) 
		SM_destroyBillboardChainWithName(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy all TBillboardChains in this TSceneManager.
	ENDREM
	Method destroyAllBillboardChains() 
		SM_destroyAllBillboardChains(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Note: Ribbon trails are not yet supported. Create a TRibbonTrail in this scene. A TRibbonTrail is a ribbon of connected billboards that follow a node or nodes.
	ENDREM
	Method createRibbonTrail:TRibbonTrail(Name:String) 
		Local Obj:TRibbonTrail = New TRibbonTrail
		Obj.Pointer = SM_createRibbonTrail(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Note: Ribbon trails are not yet supported. Gets a TRibbonTrail specified by name.
	ENDREM
	Method getRibbonTrail:TRibbonTrail(Name:String) 
		Local Obj:TRibbonTrail = New TRibbonTrail
		Obj.Pointer = SM_getRibbonTrail(name.ToCString() , Self.Pointer) 
		Return Obj	
	End Method
	
	Rem
		bbdoc: Checks whether or not a specified TRibbonTrail exists.
	ENDREM
	Method hasRibbonTrail:Int(Name:String) 
		Return SM_hasRibbonTrail(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy a TRibbonTrail by reference.
	ENDREM
	Method destroyRibbonTrail(obj:TRibbonTrail) 
		SM_destroyRibbonTrail(obj.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy a TRibbonTrail by name.
	ENDREM
	Method destroyRibbonTrailWithName(Name:String) 
		SM_destroyRibbonTrailWithName(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy all TRibbonTrails in this scene.
	ENDREM
	Method destroyAllRibbonTrails() 
		SM_destroyAllRibbonTrails( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Create an empty particle system that will be set up manually. If you are looking for a simple visuals based particle system, you probably want to use createParticleSystemWithTemplate.<br>quota:Int The upper-limit for the number of particles in the particle system.
	ENDREM
	Method createParticleSystem:TParticleSystem(Name:String, quota:Int = 500, resourceGroup:String = "") 
		Local Obj:TParticleSystem = New TParticleSystem
		Obj.Pointer = SM_createParticleSystem( name.ToCString() , quota , resourceGroup.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Create a TParticle system based on a template, usually these exists as a .particle files in a resource directory that's currently active.
	ENDREM
	Method createParticleSystemWithTemplate:TParticleSystem(Name:String, templateName:String) 
		Local Obj:TParticleSystem = New TParticleSystem
		Obj.Pointer = SM_createParticleSystemWithTemplate(name.ToCString() , templateName.tocstring() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a TParticle instance by name.
	ENDREM
	Method getParticleSystem:TParticleSystem(Name:String) 
		Local Obj:TParticleSystem = New TParticleSystem
		Obj.Pointer = SM_getParticleSystem(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Check to see if this TSceneManager has a particle system of a specified name.
	ENDREM
	Method hasParticleSystem:Int(Name:String) 
		Return SM_hasParticleSystem(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy a TParticleSystem from this TSceneManager instance.
	ENDREM
	Method destroyParticleSystem(obj:TParticleSystem) 
		SM_destroyParticleSystem(obj.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy a TParticleSystem from this TSceneManager instance by specifying it's name.
	ENDREM
	Method destroyParticleSystemWithName(Name:String) 
		SM_destroyParticleSystemWithName(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy all TParticleSystems from this TSceneManager instance.
	ENDREM
	Method destroyAllParticleSystems() 
		SM_destroyAllParticleSystems(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Clears the entire scene except for cameras.
	ENDREM
	Method clearScene() 
		SM_clearScene(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Sets a scene-wide ambient light for this TSceneManager.
	ENDREM
	Method setAmbientLight(colour:TColourValue) 
		SM_setAmbientLight(colour.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the properties of the scene-wide ambient light for this TSceneManager.
	ENDREM
	Method getAmbientLight:TColourValue() 
		Return TColourValue.FromPtr(SM_getAmbientLight(Self.Pointer)) 
	End Method
	
		
	Rem
		bbdoc: Load a type of world geometry. The type accepted depends on the scene manager plugin that you are using. 
	ENDREM
	Method setWorldGeometry(filename:String) 
		SM_setWorldGeometry(filename.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Load a type of world geometry from a TOgreDataStream. The types accepted depend on the scene manager plugint hat you are using. If only one type is accepted then you do not need to specify type.
	ENDREM
	Method setWorldGeometryWithDataStream(stream:TOgreDataStream, typeName:String = "") 
		SM_setWorldGeometryWithDataStream(stream.Pointer, typeName.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Estimate the number of loading stages it will take to load the specified world geometry.
	ENDREM
	Method estimateWorldGeometry:Int(filename:String) 
		SM_estimateWorldGeometry(filename.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Estimate the number of loading stages it will take to load the specified world geometry using a TOgreDataStream.
	ENDREM
	Method estimateWorldGeometryWithDataStream:Int(stream:TOgreDataStream, typeName:String = "") 
		SM_estimateWorldGeometryWithDataStream(stream.Pointer, typeName.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: A suggested TViewPoint for viewing the scene will be returned if setWorldGeometry has been used and there are suggested start points in the world geometry, otherwise the origin is returned.<br>random:Int If there is more than one viewpoint found, select one randomly.
	ENDREM
	Method getSuggestedViewpoint:TViewPoint(random:Byte = False) 
		Local Obj:TViewPoint = New TViewPoint
		Obj.Pointer = SM_getSuggestedViewpoint(random, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: This method allows you to set options for specific types of scene manager implementations.<br>pValue:Byte Ptr The pointer to the value being used. For flow/ogre structures, use the .Pointer value. For BlitzMax types such as integers, VarPtr(yourvar) may be used.<br>Returns true if setting the option was successful.
	ENDREM
	Method setOption:Int(strKey:String, pValue:Byte Ptr) 
		Return SM_setOption(strKey.ToCString() , pValue, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the value of an option by specifying it's name. The names/keys available depend on the type of TSceneManager you are using.<br>pDestValue:Byte Ptr The pointer to a place to store the gotten value.  You will need to create the ogre type first if it is an ogre type. Example: Global aabb:TAxisAlingedBoundingBox = New TAxisAlignedBoundingBox; yoursm.getOption( "Size" , aabb.Pointer ). Otherwise you may just create a BlitzMax type and use VarPtr(yourvar).
	ENDREM
	Method getOption(strKey:String, pDestValue:Byte Ptr) 
		SM_getOption(strKey.ToCString() , pDestValue, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Check to see if this TSceneManager has a specific option avaialble to it.
	ENDREM
	Method hasOption:Int(strKey:String) 
		Return SM_hasOption(strKey.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get all possible values for a specific option.<br>strKey:String The option to get a list of values for.<br>refValueList:TStringVector The list to store the options to.<br>Returns true if getting the option values was a success, false if it was not.
	ENDREM
	Method getOptionValues:Int(strKey:String, refValueList:TStringVector) 
	    Return SM_getOptionValues(strKey.ToCString() , refValueList.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get implementation-specific TSceneManager options.<br>refKeys:TStringVector The TStringVector to store all of the options in.<br>If this method fails, it will return false.
	ENDREM
	Method getOptionKeys:Int(refKeys:TStringVector) 
		Return SM_getOptionKeys(refKeys.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Internal method that updates the TSceneNode tree structure for this TSceneManager.<br>cam:TCamera The camera from which the update is determined.
	ENDREM
	Method _updateSceneGraph(cam:TCamera) 
		SM__updateSceneGraph(cam.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Internal method which checks the scene graph for objects that are visible to render.<br>cam:TCamera The camera to use for this visibility test.<br>visibleBounds:TVisibleObjectBoundsInfo The visibility bounds for which to determine visibility.<br>onlyShadowCasters:Int Check for shadow casters only?
	ENDREM
	Method _findVisibleObjects(cam:TCamera, visibleBounds:TVisibleObjectsBoundsInfo, onlyShadowCasters:Byte) 
		SM__findVisibleObjects(cam.Pointer, visibleBounds.Pointer, onlyShadowCasters, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Internal method that applies animations to TSceneNodes.
	ENDREM
	Method _applySceneAnimations() 
		SM__applySceneAnimations(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Internal method to specify processing objects obtained by _findVisibleObjects to the engine to be rendered.
	ENDREM
	Method _renderVisibleObjects() 
		SM__renderVisibleObjects(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Alerts this TSceneManager that it should send it's contents to the renderer.<br>camera:TCamera Which camera to render contents from.<br>vp:TViewport Which viewport to render to.<br>includeOverlays:Int Do we send overlays also?
	ENDREM
	Method _renderScene(camera:TCamera, vp:TViewport, includeOverlays:Byte) 
		SM__renderScene(camera.Pointer, vp.Pointer, includeOverlays, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Internal method to set up a defined sky system in the TRenderQueue.<br>cam:TCamera The camera that the sky system will be rendered from.
	ENDREM
	Method _queueSkiesForRendering(cam:TCamera) 
		SM__queueSkiesForRendering(cam.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Method used to notify this TSceneManager of the TRenderSytem being used.
	ENDREM
	Method _setDestinationRenderSystem(sys:TRenderSystem) 
		SM__setDestinationRenderSystem(sys.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set a skyplane that is always the same distance from any TCamera.<br>enable:Int Is this skybox to be enabled upon creation?<br>plane:TPlane The TPlane that you wish to use for the sky.<br>materialName:String The name of the material to use for the sky.<br>scale:Float Scaling applied to the sky plane.<br>tiling:Float  This setting affects how often the texture should tile across the sky.<br>drawFirst:Int Draw the sky first then draw everything else over the top of it? If set to true, ensures that objects will never intersect with the sky plane. If set to false, you get a mild performance gain but must ensure objects do not intersect the sky plane.<br>xsegments:Int Describes how tesselated the sky plane will be. This is important for vertex lighting schemes and also if the plane is to be bowed.<br>ysegments:Int Describes how tesselated the sky plane will be. This is important for vertex lighting schemes and also if the plane is to be bowed.<br>groupName:String The resource group name of this sky plane.
	ENDREM

	Method setSkyPlane(enable:Int, plane:TPlane, materialName:String, scale:Float = 1000, tiling:Float = 10, drawFirst:Int = True, bow:Float = 0, xsegments:Int = 1, ysegments:Int = 1, groupName:String = DEFAULT_RESOURCE_GROUP_NAME) 
		SM_setSkyPlane(enable, plane.Pointer, materialName.tocstring() , Self.Pointer, scale, tiling, drawFirst, bow, xsegments, ysegments, groupName.ToCString()) 
	End Method
	
	Rem
		bbdoc: Check to see whether or not a sky plane is enabled for this TSceneManager.
	ENDREM
	Method isSkyPlaneEnabled:Int() 
		Return SM_isSkyPlaneEnabled(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the TSceneNode that the sky plane node is attached to.
	ENDREM
	Method getSkyPlaneNode:TSceneNode() 
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = SM_getSkyPlaneNode(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get sky plane parameters.
	ENDREM
	Method getSkyPlaneGenParameters:TSkyPlaneGenParameters() 
		Local Obj:TSkyPlaneGenParameters = New TSkyPlaneGenParameters
		Obj.Pointer = SM_getSkyPlaneGenParameters(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Create a textured cube that is always the same distance from any camera to represent the sky. For materials, you must use either a six-sided cube texture or a layered texture. <br>enable:Int Enable the skybox after it's creation?<brmaterialName:String The material to use for this skybox. Must be a six-sided or layered texture.<br>distance:Float The distance of the skybox from the camera.<br>drawFirst:Int Draw the sky first then draw everything else over the top of it? If set to true, ensures that objects will never intersect with the sky box. If set to false, you get a mild performance gain but must ensure objects do not intersect the sky box.<br>orientation:TQuaternion The Orientation of the skybox. Default is TQuaternion.IDENTITY().<br>groupName:String The resource group that the skybox belongs to.
	ENDREM
	Method setSkyBox(enable:Int, materialName:String, distance:Float = 5000, drawFirst:Int = True, orientation:TQuaternion = Null, groupName:String = DEFAULT_RESOURCE_GROUP_NAME) 
		If orientation = Null Then orientation = TQuaternion.IDENTITY() 
		SM_setSkybox(enable, materialName.tocstring() , Self.Pointer, distance, drawFirst, orientation.Pointer, groupName.ToCString()) 
	End Method
	
	Rem
		bbdoc: Check to see whether or not a skybox is enabled for this TSceneManager.
	ENDREM
	Method isSkyBoxEnabled:Int() 
		Return SM_isSkyBoxEnabled(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the TSceneNode that the sky box node is attached to.
	ENDREM
	Method getSkyBoxNode:TSceneNode() 
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = SM_getSkyBoxNode(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get sky box parameters.
	ENDREM
	Method getSkyBoxGenParameters:TSkyBoxGenParameters() 
		Local Obj:TSkyBoxGenParameters = New TSkyBoxGenParameters
		Obj.Pointer = SM_getSkyBoxGenParameters(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Create a textured dome that is always the same distance away from any camera to represent the sky. <br>enable:Int Enable the skydonme after it's creation?<br>materialName:String The material to use for the skydome.<br>curvature:Float Values between 2 and 65, 65 being smoothest.<br>tiling:Float How many times you want the texture to tile.<br>distance:Float The distance from the camera to the dome.<br>drawFirst:Int Draw the sky first then draw everything else over the top of it? If set to true, ensures that objects will never intersect with the sky box. If set to false, you get a mild performance gain but must ensure objects do not intersect the sky box.<br>orientation:TQuaternion The orientation of the skydome. By default it is TQuaternion.IDENTITY().<br>xsegments:Int Describes how tesselated the sky dome will be. This is important For vertex lighting schemes and also the curvature of the dome.<br>ysegments:Int Describes how tesselated the sky dome will be. This is important for vertex lighting schemes and also the curvature of the dome.<br>ysegments_keep:Int No documentation.<br>groupName:String The resource group that the sky dome belongs to.
	ENDREM

	Method setSkyDome(enable:Int, materialName:String, curvature:Float = 10, tiling:Float = 8, distance:Float = 4000, drawfirst:Int = True, orientation:TQuaternion = Null, xsegments:Int = 16, ysegments:Int = 16, ysegments_keep:Int = -1, groupName:String = DEFAULT_RESOURCE_GROUP_NAME) 
		If orientation = Null Then orientation = TQuaternion.IDENTITY() 
		SM_setSkyDome(enable, materialName.tocstring() , Self.Pointer, curvature, tiling, distance, drawfirst, orientation.Pointer, xsegments, ysegments, ysegments_keep, groupName.ToCString()) 
	End Method
	
	Rem
		bbdoc: Check to see whether or not a sky dome is enabled for this TSceneManager.
	ENDREM
	Method isSkyDomeEnabled:Int() 
		Return SM_isSkyDomeEnabled(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the TSceneNode that the sky dome  is attached to.
	ENDREM
	Method getSkyDomeNode:TSceneNode() 
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = SM_getSkyDomeNode(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get sky dome parameters.
	ENDREM
	Method getSkyDomeGenParameters:TSkyDomeGenParameters() 
		Local Obj:TSkyDomeGenParameters = New TSkyDomeGenParameters
		Obj.Pointer = SM_getSkyDomeGenParameters(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the type of fog to be displayed in this scene. <br>mode:Int Fog types: <br/> FOG_NONE - No fog <br/> FOG_EXP - Exponential fog <br/> FOG_EXP2 - Exponential thickness ^2 <br/>  FOG_LINEAR - Linear fog thickness.<br>colour:TColourValue The colour of the fog, the default value is white.<br>expDensity:Float The density of the fog as set by the exponential density modes.<br>linearStart:Float For FOG_Linear, distance at which fog begins to have density.<br>linearEnd:Float For FOG_Linear, distance at which fog falls off in density.
	ENDREM
	Method setFog(mode:Int = FOG_NONE, colour:TColourValue = Null, expDensity:Float = 0.001, linearStart:Float = 0.0, linearEnd:Float = 1.0) 
		If colour = Null Then colour = TColourValue.Create(1, 1, 1, 1) 
		SM_setFog(mode, colour.Pointer, expDensity, linearStart, linearEnd, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets the fog mode currently set for this scene:  <br/> FOG_NONE - No fog <br/> FOG_EXP - Exponential fog <br/> FOG_EXP2 - Exponential thickness ^2 <br/>  FOG_LINEAR - Linear fog thickness.
	ENDREM
	Method getFogMode:Int() 
		Return SM_getFogMode(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the colour of the fog used in this scene.
	ENDREM
	Method getFogColour:TColourValue() 
		Return TColourValue.FromPtr(SM_getFogColour(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the linear starting point of the fog used in this scene (world units).
	ENDREM
	Method getFogStart:Float() 
		Return SM_getFogStart(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the linear end point of the fog used in this scene (world units).
	ENDREM
	Method getFogEnd:Float() 
		Return SM_getFogEnd(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the exponential density of the fog used in this scene.
	ENDREM
	Method getFogDensity:Float() 
		Return SM_getFogDensity(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Create a TBillboardset for use in this scene.<br>poolSize:Int The initial size of the amount of allowed billboards for this TBillboardSet.
	ENDREM
	Method createBillboardSet:TBillboardSet(Name:String, poolSize:Int = 20) 
		Local Obj:TBillboardSet = New TBillboardSet
		Obj.Pointer = SM_createBillboardSet(name.ToCString() , poolSize, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a TBillboardSet by name.
	ENDREM
	Method getBillboardSet:TBillboardSet(Name:String) 
		Local Obj:TBillboardSet = New TBillboardSet
		Obj.Pointer = SM_getBillboardSet(name.ToCString() , Self.Pointer) 
		Return Obj	
	End Method
	
	Rem
		bbdoc: Check to see whether or not a given TBillboardSet exists.
	ENDREM
	Method hasBillboardSet:Int(Name:String) 
		Return SM_hasBillboardSet(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Detach and destroy a TBillboardSet by reference.
	ENDREM
	Method destroyBillboardSet(Set:TBillboardSet) 
		SM_destroyBillboardSet(set.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Detach and destroy a TBIllboardSet by name.
	ENDREM
	Method destroyBillboardSetWithName(Name:String) 
		SM_destroyBillboardSetWithName(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Detach and destroy all TBillboardSets in this scene.
	ENDREM
	Method destroyAllBillboardSets() 
		SM_destroyAllBillboardSets(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Debugging purposes, allows you to easilly see the current orientation of a given node, as all TSceneNodes will be rendered if this is set to true, with a 3 dimensional set of axes to show orientation. Please note that you must have OgreCore.zip accessible to use this, as it requires core materials.
	ENDREM
	Method setDisplaySceneNodes(display:Byte) 
		SM_setDisplaySceneNodes(display, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not the actual TSceneNode in a scene is visible as a set of axes.
	ENDREM
	Method getDisplaySceneNodes:Int() 
		Return SM_getDisplaySceneNodes(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Create a blank TAnimation object which can be used to animate objects in this scene. Basically this type of animation allows for complex movement using tracks.
	ENDREM
	Method createAnimation:TAnimation(Name:String, length:Float) 
		Local Obj:TAnimation = New TAnimation
		Obj.Pointer = SM_createAnimation(name.ToCString() , length, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a TAnimation by specifying it's name.
	ENDREM
	Method getAnimation:TAnimation(Name:String) 
		Local Obj:TAnimation = New TAnimation
		Obj.Pointer = SM_getAnimation(name.ToCString() , Self.Pointer) 
		Return Obj	
	End Method
	
	Rem
		bbdoc: Check to see whether or not a specific TAnimation exists in this scene.
	ENDREM
	Method hasAnimation:Int(Name:String) 
		Return SM_hasAnimation(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy a TAnimation from this scene by specifying it's name.
	ENDREM
	Method destroyAnimation(Name:String) 
		SM_destroyAnimation(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy all TAnimations from this scene.
	ENDREM
	Method destroyAllAnimations() 
		SM_destroyAllAnimations(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Creates a TAnimationState, which allows you to track and use TAnimations easier by keeping track of state information and letting you update every frame.
	ENDREM
	Method createAnimationState:TAnimationState(animName:String) 
		Local Obj:TAnimationState = New TAnimationState 
		Obj.Pointer = SM_createAnimationState( animName.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a specific animation state by specifying the name of the TAnimation it is linked to.
	ENDREM
	Method getAnimationState:TAnimationState(animName:String) 
		Local Obj:TAnimationState = New TAnimationState
		Obj.Pointer = SM_getAnimationState(animName.ToCString() , Self.Pointer) 
		Return Obj		
	End Method
	
	Rem
		bbdoc: Check to see whether or not a specified TAnimationState exists.
	ENDREM
	Method hasAnimationState:Int(Name:String) 
		Return SM_hasAnimationState(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy a TAnimationState from this scene by name.
	ENDREM
	Method destroyAnimationState(Name:String) 
		SM_destroyAnimationState(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy all TAnimationStates associated with this scene.
	ENDREM
	Method destroyAllAnimationStates() 
		SM_destroyAllAnimationStates(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Manually send rendering commands through the rendering pipeline. This method is for advanced users and should be used conservatively.<br>rend:TRenderOperation TRenderOperation that describes the rendering operation we will do with this call.br>pass:TPass The rendering pass to use.<br>vp:TViewPort The viewport to render to.<br>worldMatrix:TMatrix4 The world transform to apply.<br>viewMatrix:TMatrix4 The world to view space transform.<br>projMatrix:TMatrix4  The view to screen space transform.<br>If true, call BeginFrame() and EndFrame() with this. Best left false unless you are calling it within your rendering loop.
	ENDREM
	Method manualRender(rend:TRenderOperation, pass:TPass, vp:TViewport, worldMatrix:TMatrix4, viewMatrix:TMatrix4, projMatrix:TMatrix4, doBeginEndFrame:Byte = False) 
		SM_manualRender(rend.Pointer, pass.Pointer, vp.Pointer, worldMatrix.Pointer, viewMatrix.Pointer, projMatrix.Pointer, doBeginEndFrame, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Advanced method for getting the TRenderQueue for this scene to manipulate certain rendering properties. Use this only if you know what you are doing.
	ENDREM
	Method getRenderQueue:TRenderQueue() 
		Local Obj:TRenderQueue = New TRenderQueue
		Obj.Pointer = SM_getRenderQueue( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Registers a TRenderQueueListener which is alerted whenever the render queue is processed.
	ENDREM
	Method addRenderQueueListener(newListener:TRenderQueueListener) 
		SM_addRenderQueueListener(newListener.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Removes a TRenderQueueListener from the render queue.
	ENDREM
	Method removeRenderQueueListener(delListener:TRenderQueueListener) 
		SM_removeRenderQueueListener(delListener.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Add a render queue to a special case renderqueue to vary the rendering behavior without using a TRenderQueueListener. How this special case list is handled can be altered by setting the special cast list's mode.<br>qid:Int The render queue id of the queue you want to add to the special case list.
	ENDREM
	Method addSpecialCaseRenderQueue(qid:Int) 
		SM_addSpecialCaseRenderQueue(qid, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Removes a render queue from the special case renderqueue to vary the rendering behavior without using a TRenderQueueListener. How this special case list is handled can be altered by setting the special cast list's mode.<br>qid:Int The render queue id of the queue you want to remove from the special case list.
	ENDREM 
	Method removeSpecialCaseRenderQueue(qid:Int) 
		SM_removeSpecialCaseRenderQueue(qid, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Clear all render queues from the special case render queue.
	ENDREM
	Method clearSpecialCaseRenderQueues() 
		SM_clearSpecialCaseRenderQueues(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Sets the mode for this scene's special case render queue. SCRQM_INCLUDE - Only render the queues in the special case list. SCRQM_EXCLUDE - Only render the queues that are not in the special case list.
	ENDREM
	Method setSpecialCaseRenderQueueMode(mode:Int) 
		SM_setSpecialCaseRenderQueueMode(mode, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets the mode for this scene's special case render queue. SCRQM_INCLUDE - Only render the queues in the special case list. SCRQM_EXCLUDE - Only render the queues that are not in the special case list.
	ENDREM
	Method getSpecialCaseRenderQueueMode:Int() 
		Return SM_getSpecialCaseRenderQueueMode(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Determine whether or not a specified render queue will be processed in the current render queue special case list with it's current mode.
	ENDREM
	Method isRenderQueueToBeProcessed:Int(qid:Int) 
		Return SM_isRenderQueueToBeProcessed(qid, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the render queue that world geometry will be rendered with for this scene.<br>qid:Int The render queue to use for world geometry.
	ENDREM
	Method setWorldGeometryRenderQueue(qid:Int) 
		SM_setWorldGeometryRenderQueue(qid, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the render queue that world geometry will be rendered with for this scene.
	ENDREM
	Method getWorldGeometryRenderQueue:Int() 
		Return SM_getWorldGeometryRenderQueue(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Turn on or off bounding boxes for all TSceneNodes.
	ENDREM
	Method showBoundingBoxes(bShow:Byte) 
		SM_showBoundingBoxes(bShow, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether showing bounding boxes for all TSceneNodes is on or off for this scene.
	ENDREM
	Method getShowBoundingBoxes:Int() 
		Return SM_getShowBoundingBoxes(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Internal method to notify the TSceneManager that a TSceneNode is autotracking.<br>autoTrack:Byte Is this TSceneNode autotracking?
	ENDREM
	Method _notifyAutotrackingSceneNode(NODE:TSceneNode, autoTrack:Byte) 
		SM__notifyAutotrackingSceneNode(node.Pointer, autoTrack, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Create an TAxisAlignedBoxSceneQuery to make aabb queries in this scene. You must manually destroyQuery when you are done with this. <br>box:TAxisAlignedBox The box that is the basis for this region query.<br>mask:Int The query mask to apply to this query. Used for filtering different objects out of the query.
	ENDREM
	Method createAABBQuery:TAxisAlignedBoxSceneQuery(box:TAxisAlignedBox, mask:Int = -1) 
		Local Obj:TAxisAlignedBoxSceneQuery = New TAxisAlignedBoxSceneQuery
		Obj.Pointer = SM_createAABBQuery(box.Pointer, mask, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Create a TSphereSceneQuery to make TSphere based queries in this scene. You must use destroyQuery when you are done with this object.<br>sphere:TSphere The sphere that is the basis for this region query.<br>mask:Int The query mask to apply to this query. Used for filtering different objects out of the query.
	ENDREM
	Method createSphereQuery:TSphereSceneQuery(sphere:TSphere, mask:Int = -1) 
		Local Obj:TSphereSceneQuery = New TSphereSceneQuery
		Obj.Pointer = SM_createSphereQuery(sphere.Pointer, mask, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Create a TPlaneBoundedVolumeListSceneQuery to make TPlaneBoundedVolumeList based queries in this scene. You must use destroyQuery when you are done with this object. <br>volumes:TPlaneBoundedVolumeList The volume list that will form the basis for this region query.<br>mask:Int The query mask to apply to this query. Used for filtering different objects out of the query.
	ENDREM
	Method createPlaneBoundedVolumeQuery:TPlaneBoundedVolumeListSceneQuery(volumes:TPlaneBoundedVolumeList, mask:Int = -1) 
		Local Obj:TPlaneBoundedVolumeListSceneQuery = New TPlaneBoundedVolumeListSceneQuery
		Obj.Pointer = SM_createPlaneBoundedVolumeQuery(volumes.Pointer, mask, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Create a TRaySceneQuery to make TRay based queries in this scene. You must use destroyQuery when you are done with this object. <br>ray:TRay The ray that will form the basis for this scene query.<br>mask:Int The query mask to apply to this query. Used for filtering different objects out of the query.
	ENDREM
	Method createRayQuery:TRaySceneQuery(ray:TRay, mask:Int = -1) 
		Local Obj:TRaySceneQuery = New TRaySceneQuery
		Obj.Pointer = SM_createRayQuery(ray.Pointer, mask, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Create a TIntersectionSceneQuery to make intersection based queries in this scene. You must use destroyQuery when you are done with this object.<br>mask:Int The query mask to apply to this query. Used for filtering different objects out of the query.
	ENDREM
	Method createIntersectionQuery:TIntersectionSceneQuery(mask:Int = -1) 
		Local Obj:TIntersectionSceneQuery = New TIntersectionSceneQuery
		Obj.Pointer = SM_createIntersectionQuery(mask, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Destroy a scenequery which is required as part of clean-up.
	ENDREM
	Method destroyQuery(Query:TSceneQuery) 
		SM_destroyQuery(query.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get an iterator that allows you to iterate through all cameras in this scene.
	ENDREM
	Method getCameraIterator:TCameraIterator() 
		Local Obj:TCameraIterator = New TCameraIterator
		Obj.Pointer = SM_getCameraIterator(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get an iterator that allows you to iterate through all animations in this scene.
	ENDREM
	Method getAnimationIterator:TAnimationIterator() 
		Local Obj:TAnimationIterator = New TAnimationIterator
		Obj.Pointer = SM_getAnimationIterator(Self.Pointer) 
		Return Obj	
	End Method
	
	Rem
		bbdoc: Get an iterator that allows you to iterate through all animationstates in this scene.
	ENDREM
	Method getAnimationStateIterator:TAnimationStateIterator() 
		Local Obj:TAnimationStateIterator = New TAnimationStateIterator
		Obj.Pointer = SM_getAnimationStateIterator(Self.Pointer) 
		Return Obj	
	End Method

	
	Rem
		bbdoc: Sets the shadow technique to be used for shadow casters in the scene. <br>ShadowTechnique:Int Options are: SHADOWTYPE_NONE , SHADOWTYPE_STENCIL_ADDITIVE , SHADOWTYPE_STENCIL_MODULATIVE , SHADOWTYPE_TEXTURE_MODULATIVE , SHADOWTYPE_TEXTURE_ADDITIVE , SHADOWTYPE_TEXTURE_ADDITIVE_INTEGRATED , SHADOWTYPE_TEXTURE_MODULATIVE_INTEGRATED.
	ENDREM
	Method setShadowTechnique(ShadowTechnique:Int) 
		SM_setShadowTechnique( ShadowTechnique , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the shadow technique being used for shadow casters in the scene.<br>Options are: SHADOWTYPE_NONE , SHADOWTYPE_STENCIL_ADDITIVE , SHADOWTYPE_STENCIL_MODULATIVE , SHADOWTYPE_TEXTURE_MODULATIVE , SHADOWTYPE_TEXTURE_ADDITIVE , SHADOWTYPE_TEXTURE_ADDITIVE_INTEGRATED , SHADOWTYPE_TEXTURE_MODULATIVE_INTEGRATED.
	ENDREM
	Method getShadowTechnique:Int() 
		Return SM_getShadowTechnique( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Turn on or off the rendering of shadow debug information.
	ENDREM
	Method setShowDebugShadows(debug:Int) 
		SM_setShowDebugShadows( debug , Self.Pointer )
	End Method

	Rem
		bbdoc: Check to see if shadow debug information is displaying or not.
	ENDREM
	Method getShowDebugShadows:Int() 
		Return SM_getShowDebugShadows(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the modulative colour value of the shadow for when areas are darkened.
	ENDREM
	Method setShadowColour(colour:TColourValue) 
		SM_setShadowColour( colour.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the modulative colour value of the shadow for when areas are darkened.
	ENDREM
	Method getShadowColour:TColourValue() 
		Return TColourValue.FromPtr(SM_getShadowColour(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Sets and allows you to limit or expand the extrusion distance of a directional light. This can be good for optimization and compatibility reasons.  The default value is 10,000 world units and this has no application on point or spot lights.
	ENDREM
	Method setShadowDirectionalLightExtrusionDistance(dist:Float) 
		SM_setShadowDirectionalLightExtrusionDistance(dist, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets the extrusion distance allowed for directional lights in world units.
	ENDREM
	Method getShadowDirectionalLightExtrusionDistance:Float() 
		Return SM_getShadowDirectionalLightExtrusionDistance(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the maximum distance from the camera that shadows are visible.
	ENDREM
	Method setShadowFarDistance(distance:Float) 
		SM_setShadowFarDistance(distance, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the maximum distance from the camera that shadows are visible.
	ENDREM
	Method getShadowFarDistance:Float() 
		Return SM_getShadowFarDistance(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the maximum size of the index buffer used to render shadow primitives.<br>size:Int  The maximum number of indexes. This is 3 times the number of triangles in your shadow primitives.
	ENDREM
	Method setShadowIndexBufferSize(Size:Int) 
		SM_setShadowIndexBufferSize(size, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the maximum size of the index buffer used to render shadow primitives.
	ENDREM
	Method getShadowIndexBufferSize:Int() 
		Return SM_getShadowIndexBufferSize(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Simple version of setShadowTextureConfig. This allows you to specify the size of the shadow texture your texture shadows reside upon. The default is 512 ( Always a power of 2 ) and the larger you make it, the more clarity your shadows will have, at the cost of memory.<BR>Shadow texture size MUST be in a power of two.
	ENDREM
	Method setShadowTextureSize(Size:Int) 
		SM_setShadowTextureSize(size, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Configure this scene's shadow texture index, size and format. <br>shadowIndex:Int The index of the shadow texture you are configuring.<br>width:Int The shadow texture width.<br>height:Int The shadow texture height.<br>format:Int The shadow texture pixel format. Constants are: <br>PF_UNKNOWN<br>PF_L8<br>PF_BYTE_L<br>PF_L16<br>PF_SHORT_L<br>PF_A8<br>PF_BYTE_A<br>PF_A4L4<br>PF_BYTE_LA<br>PF_R5G6B5<br>PF_B5G6R5<br>PF_R3G3B2<br>PF_A4R4G4B4<br>PF_A1R5G5B5<br>PF_R8G8B8<br>PF_B8G8R8<br>PF_A8R8G8B8<br>PF_A8B8G8R8<br>PF_B8G8R8A8<br>PF_R8G8B8A8<br>PF_X8R8G8B8<br>PF_X8B8G8R8
	ENDREM
	Method setShadowTextureConfig(shadowIndex:Int, width:Int, Height:Int, format:Int) 
		SM_setShadowTextureConfig(shadowIndex, width, height, format, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Configure this scene's shadow texture using an index to specify the texture and a TShadowTextureConfig object to configure.<br>shadowIndex:Int The index of the shadow to set up.<br>config:TShadowTextureConfig Shadow texture configuration structure containing shadow texture config values.
	ENDREM
	Method setShadowTextureConfigWithConfig(shadowIndex:Int, config:TShadowTextureConfig) 
		SM_setShadowTextureConfigWithConfig(shadowIndex, config.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get an iterator that allows you to iterate through all shadow texture configurations.
	ENDREM
	Method getShadowTextureConfigIterator:TConstShadowTextureConfigIterator() 
		Local Obj:TConstShadowTextureConfigIterator = New TConstShadowTextureConfigIterator
		Obj.Pointer = SM_getShadowTextureConfigIterator(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Sets the format of shadow textures for use in texture shadows. The default value is PF_X8R8G8B8. Constants are: <br>PF_UNKNOWN<br>PF_L8<br>PF_BYTE_L<br>PF_L16<br>PF_SHORT_L<br>PF_A8<br>PF_BYTE_A<br>PF_A4L4<br>PF_BYTE_LA<br>PF_R5G6B5<br>PF_B5G6R5<br>PF_R3G3B2<br>PF_A4R4G4B4<br>PF_A1R5G5B5<br>PF_R8G8B8<br>PF_B8G8R8<br>PF_A8R8G8B8<br>PF_A8B8G8R8<br>PF_B8G8R8A8<br>PF_R8G8B8A8<br>PF_X8R8G8B8<br>PF_X8B8G8R8
	ENDREM
	Method setShadowTexturePixelFormat(fmt:Int) 
		SM_setShadowTexturePixelFormat(fmt, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the number of textures specified for texture-based shadows.
	ENDREM
	Method setShadowTextureCount(Count:Int) 
		SM_setShadowTextureCount(count, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the number of textures specified for texture-based shadows.
	ENDREM
	Method getShadowTextureCount:Int() 
		Return SM_getShadowTextureCount(Self.Pointer) 
	End Method
	
	Rem
		bbdoc:Set the Size, Count And pixel format of the textures used For shadow - based textures.<br> Size:Int The Size of the shadow textures.<br> Count:Int The number of shadow textures.<br> fmt:Int The pixel format.Constants are:<br> PF_UNKNOWN <br> PF_L8 <br> PF_BYTE_L <br> PF_L16 <br> PF_SHORT_L <br> PF_A8 <br> PF_BYTE_A <br> PF_A4L4 <br> PF_BYTE_LA <br> PF_R5G6B5 <br> PF_B5G6R5 <br> PF_R3G3B2 <br> PF_A4R4G4B4 <br> PF_A1R5G5B5 <br> PF_R8G8B8 <br> PF_B8G8R8 <br> PF_A8R8G8B8 <br> PF_A8B8G8R8 <br> PF_B8G8R8A8 <br> PF_R8G8B8A8 <br> PF_X8R8G8B8 <br> PF_X8B8G8R8
	ENDREM
	Method setShadowTextureSettings(Size:Int, Count:Int, fmt:Int = PF_X8R8G8B8) 
		SM_setShadowTextureSettings(size, count, fmt, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get a shadow-texture texture as specified by the index.
	ENDREM
	Method getShadowTexture:TTexture(shadowIndex:Int) 
		Local Obj:TTexture = New TTexture
		Obj.Pointer = SM_getShadowTexture(shadowIndex, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Offset the shadow texture projection, which is normally centered over the camera, to optimize your shadow texture space usage.<br>offset:Float The offset, default is 0.6.
	ENDREM
	Method setShadowDirLightTextureOffset(offset:Float) 
		SM_setShadowDirLightTextureOffset(offset, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the offset of the shadow texture projection which is normally centered over the camera.
	ENDREM
	Method getShadowDirLightTextureOffset:Float() 
		Return SM_getShadowDirLightTextureOffset(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the value at which ogre will fade out the shadow into the distance.  Default is 0.7.
	ENDREM
	Method setShadowTextureFadeStart(fadeStart:Float) 
		SM_setShadowTextureFadeStart(fadeStart, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the value at with ogre will fade out the shadow into the distance. Default is 0.9.
	ENDREM
	Method setShadowTextureFadeEnd(fadeEnd:Float) 
		SM_setShadowTextureFadeEnd(fadeEnd, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Turn on or off shadow-texture self shadowing.
	ENDREM
	Method setShadowTextureSelfShadow(selfShadow:Byte) 
		SM_setShadowTextureSelfShadow(selfShadow, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not shadow-texture self-shadowing is turned on.
	ENDREM
	Method getShadowTextureSelfShadow:Int() 
		Return SM_getShadowTextureSelfShadow(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Override basic projective texture shadow caster with your own material to provide advanced shadow caster handling.
	ENDREM
	Method setShadowTextureCasterMaterial(Name:String) 
		SM_setShadowTextureCasterMaterial( name.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Override basic projective texture shadow receiver with your own material to provide advanced shadow receiver handling.
	ENDREM
	Method setShadowTextureReceiverMaterial(Name:String) 
		SM_setShadowTextureReceiverMaterial( name.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether shadow casters should be rendered into shadow textures using their front or back faces. True for back-faces, otherwise front-faces will be used.
	ENDREM
	Method setShadowCasterRenderBackFaces(bf:Byte) 
		SM_setShadowCasterRenderBackFaces(bf, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not shadow casters should be rendered into shadow textures using their front or back faces.
	ENDREM
	Method getShadowCasterRenderBackFaces:Int() 
		Return SM_getShadowCasterRenderBackFaces(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set up shadow camera for any lights that don't have their own shadow camera setup.
	ENDREM
	Method setShadowCameraSetup(shadowSetup:TShadowCameraSetup) 
		SM_setShadowCameraSetup(shadowSetup.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the shadow camera setup for any lights that don't have their own shadow camera setup.
	ENDREM
	Method getShadowCameraSetup:TShadowCameraSetup() 
		Local Obj:TShadowCameraSetup = New TShadowCameraSetup
		Obj.Pointer = SM_getShadowCameraSetup(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Set stencil shadows to use infinite far planes? Some hardware/configurations do not support this, but most newer hardware should.
	ENDREM
	Method setShadowUseInfiniteFarPlane(enable:Byte) 
		SM_setShadowUseInfiniteFarPlane(enable, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Is the shadow technique in use stencil based?
	ENDREM
	Method isShadowTechniqueStencilBased:Int() 
		Return SM_isShadowTechniqueStencilBased(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Is the shadow technique in use texture based?
	ENDREM
	Method isShadowTechniqueTextureBased:Int() 
		Return SM_isShadowTechniqueTextureBased(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Is the shadow technique in use modulative?
	ENDREM
	Method isShadowTechniqueModulative:Int() 
		Return SM_isShadowTechniqueModulative(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Is the shadow technique in use additive?
	ENDREM
	Method isShadowTechniqueAdditive:Int() 
		Return SM_isShadowTechniqueAdditive(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Is the shadow technique in use integrated?
	ENDREM
	Method isShadowTechniqueIntegrated:Int() 
		Return SM_isShadowTechniqueIntegrated(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Is the shadow technique in use?
	ENDREM
	Method isShadowTechniqueInUse:Int() 
		Return SM_isShadowTechniqueInUse(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Add a scene manager listener to this scene manager. This method is not fully supported.
	ENDREM
	Method addListener(s:TSceneManagerListener)
		SM_addListener(s.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Remove a scene manager listener from this scene manager. This method is not fully supported
	ENDREM
	Method removeListener(s:TSceneManagerListener)
		SM_removeListener(s.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Create a static geometry instance. Static geometry is batched geometry that is faster to render, but you cannot move it.
	ENDREM
	Method createStaticGeometry:TStaticGeometry(Name:String) 
		Local Obj:TStaticGeometry = New TStaticGeometry
		Obj.Pointer = SM_createStaticGeometry( name.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Retreive a TStaticGeometry by specifying it's name.
	ENDREM
	Method getStaticGeometry:TStaticGeometry(Name:String) 
		Local Obj:TStaticGeometry = New TStaticGeometry
		Obj.Pointer = SM_getStaticGeometry(name.ToCString() , Self.Pointer) 
		Return Obj	
	End Method
	
	Rem
		bbdoc: Check whether or not a TStaticGeometry instance with a given name exists.
	ENDREM
	Method hasStaticGeometry:Int(Name:String) 
		Return SM_hasStaticGeometry(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy the TStaticGeometry instance specified.
	ENDREM
	Method destroyStaticGeometry(geom:TStaticGeometry) 
		SM_destroyStaticGeometry(geom.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy the TStaticGeometry of the specified name.
	ENDREM
	Method destroyStaticGeometryWithname(Name:String) 
		SM_destroyStaticGeometryWithName(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy all TStaticGeometry in this scene.
	ENDREM
	Method destroyAllStaticGeometry() 
		SM_destroyAllStaticGeometry(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Note: Instanced Geometry is not yet supported. Create an instanced geometry object. Instanced geometry uses your graphics hardware/shaders to instance hardware on the GPU, allowing for batched, movable objects that can be very efficient if used properly.
	ENDREM
	Method createInstancedGeometry:TInstancedGeometry(Name:String) 
		Local Obj:TInstancedGeometry = New TInstancedGeometry
		Obj.Pointer = SM_createInstancedGeometry(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Note: Instanced Geometry is not yet supported. Get an instanced geometry object by name.
	ENDREM
	Method getInstancedGeometry:TInstancedGeometry(Name:String) 
		Local Obj:TInstancedGeometry = New TInstancedGeometry
		Obj.Pointer = SM_getInstancedGeometry(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Remove and destroy a TInstancedGeometry object from this scene.
	ENDREM
	Method destroyInstancedGeometry(geom:TInstancedGeometry) 
		SM_destroyInstancedGeometry(geom.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy a TInstancedGeometry object, by name, from this scene.
	ENDREM
	Method destroyInstancedGeometryWithName(Name:String) 
		SM_destroyInstancedGeometryWithName(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove and destroy all TInstanced geometry from this scene.
	ENDREM
	Method destroyAllInstancedGeometry() 
		SM_destroyAllInstancedGeometry(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Generalized TMovableObject creation method. This method also allows you to create new types of TMovableObjects that might be registered through plugins. <br>params:TNameValuePairList Any extra initialisation parameters for this TMovableObject.
	ENDREM
	Method createMovableObject:TMovableObject(Name:String, typeName:String, params:TNameValuePairList = Null) 
		Local Obj:TMovableObject = New TMovableObject
		If params = Null Then
			Obj.Pointer = SM_createMovableObject(name.ToCString() , typeName.ToCString() , Null, Self.Pointer) 
		Else
			Obj.Pointer = SM_createMovableObject(name.ToCString() , typeName.ToCString() , params.Pointer, Self.Pointer) 
		End If
		Return Obj
	End Method
	
	Rem
		bbdoc: Generalized TMovableObject destruction method, with name.
	ENDREM
	Method destroyMovableObjectWithName(Name:String, typeName:String) 
		SM_destroyMovableObjectWithName(name.ToCString() , typeName.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Generalized TMovableObject destruction method.
	ENDREM
	Method destroyMovableObject(m:TMovableObject) 
		SM_destroyMovableObject(m.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Method to destroy all TMovableObjects of a certain type.
	ENDREM
	Method destroyAllMovableObjectsByType(typeName:String) 
		SM_destroyAllMovableObjectsByType(typeName.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Method to destroy all TMovableObjects in this scene.
	ENDREM
	Method destroyAllMovableObjects() 
		SM_destroyAllMovableObjects(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Generalized method to get a TMovableObject by name and type.
	ENDREM
	Method getMovableObject:TMovableObject(Name:String, typeName:String) 
		Local Obj:TMovableObject = New TMovableObject
		Obj.Pointer = SM_getMovableObject(name.ToCString() , typeName.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Generalized method to test and see if this scene has a specified TMovableObject or not.
	ENDREM
	Method hasMovableObject:Int(Name:String, typeName:String) 
		Return SM_hasMovableObject(name.ToCString() , typeName.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get an Iterator that allows you to iterate through all TMovableObjects in the scene of a specific type.
	ENDREM
	Method getMovableObjectIterator:TMovableObjectIterator(typeName:String) 
		Local Obj:TMovableObjectIterator = New TMovableObjectIterator
		Obj.Pointer = SM_getMovableObjectIterator(typeName.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Inject a TMovableObject into the scene manager, foregoing a factory, etc. It will be up to you to make sure that you have a unique name and proper type name upon injection.
	ENDREM
	Method injectMovableObject(m:TMovableObject) 
		SM_injectMovableObject(m.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Extract a previously injected TMovableObject from the scene manager.
	ENDREM
	Method extractMovableObject(m:TMovableObject) 
		SM_extractMovableObject(m.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Extract a previously injected TMovableObject from the scene manager by name.
	ENDREM
	Method extractMovableObjectWithName(Name:String, typeName:String) 
		SM_extractMovableObjectWithName(name.ToCString() , typeName.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Extract all previously injected TMovableObjects based on type.
	ENDREM
	Method extractAllMovableObjectsByType(typeName:String) 
		SM_extractAllMovableObjectsByType(typeName.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set a visibility mask which is ANDed with other object's visibility masks to determine visibility for this scene. This is combined with TViewport visibility masks as well.
	ENDREM
	Method setVisibilityMask(vmask:Int) 
		SM_setVisibilityMask(vmask, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the visibility mask for this scene.
	ENDREM
	Method getVisibilityMask:Int() 
		Return SM_getVisibilityMask(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Internal method for determining the scene visibiility mask added to the viewport visibility mask.
	ENDREM
	Method _getCombinedVisibilityMask:Int() 
		Return SM__getCombinedVisibilityMask(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set whether or not the scenemanager should automatically search for visible objects. The alternative being that you are handling them manually.
	ENDREM
	Method setFindVisibleObjects(find:Byte) 
		SM_setFindVisibleObjects(find, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not the scene manager should automatically search for visible objects.
	ENDREM
	Method getFindVisibleObjects:Int() 
		Return SM_getFindVisibleObjects(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Render a TRenderable without having to put it through the queue.<br>pass:TPass The pass to use for the rendering.<br>rend:TRenderable The TRenderable you wish to render.<br>shadowDerivation:Byte Use shadow caster/receiver passes?
	ENDREM
	Method _injectRenderWithPass(pass:TPass, rend:TRenderable, shadowDerivation:Byte = True) 
		SM__injectRenderWithPass(pass.Pointer, rend.Pointer, shadowDerivation, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set whether or not we allow the scenemanager to change the rendersystem parameters while rendering.
	ENDREM
	Method _suppressRenderStateChanges(suppress:Byte) 
		SM__suppressRenderStateChanges(suppress, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not we allow the scenemanager to change the rendersystem parameters while rendering.
	ENDREM
	Method _areRenderStateChangesSuppressed:Int() 
		Return SM__areRenderStateChangesSuppressed(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Internal method for setting up the renderstate for a pass.<br>pass:TPass The pass to use.<br>evenIfSuppressed:Byte Tammper with the rendersystem even if it is suppressed?<br>shadowDerivation:Byte Set pass up for shadow receivers/casters.
	ENDREM

	Method _setPass:TPass(pass:TPass, evenIfSuppressed:Byte = False, shadowDerivation:Byte = True) 
		Local Obj:TPass = New TPass
		Obj.Pointer = SM__setPass(pass.Pointer, evenIfSuppressed, shadowDerivation, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Set whether or not the scene manager should suppress the current shadow technique for it's rendering.
	ENDREM
	Method _suppressShadows(suppress:Byte) 
		SM__suppressShadows(suppress, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not the scene manager should suppress the current shadow technique for it's rendering.
	ENDREM
	Method _areShadowsSuppressed:Int() 
		Return SM__areShadowsSuppressed(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Internal method used to render objects in a specific queue group. You will probably never need to touch this, as it is generally only used for TRenderQueueInvocation implementations.<br>group:TRenderQueueGroup The queue group to render.<br>om:Int The organisation mode for the TRenderQueue Group. Set this with the TQueuedRenderableCollection.OM_* constants.
	ENDREM
	Method _renderQueueGroupObjects(group:TRenderQueueGroup, om:Int) 
		SM__renderQueueGroupObjects(group.Pointer, om, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the TRenderSystem that this scenemanager outputs to.
	ENDREM
	Method getDestinationRenderSystem:TRenderSystem() 
		Local Obj:TRenderSystem = New TRenderSystem
		Obj.Pointer = SM_getDestinationRenderSystem(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets the current viewport being rendered to. This is considered an advanced method only valid during viewport update.
	ENDREM
	Method getCurrentViewport:TViewport() 
		Local Obj:TViewport = New TViewport
		Obj.Pointer = SM_getCurrentViewport(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Returns visibility bounds information for a specific camera.
	ENDREM
	Method getVisibleObjectsBoundsInfo:TVisibleObjectsBoundsInfo(cam:TCamera) 
		Local Obj:TVisibleObjectsBoundsInfo = New TVisibleObjectsBoundsInfo
		Obj.Pointer = SM_getVisibleObjectsBoundsInfo(cam.Pointer, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Returns bounds information for a shadow caster given a specific light.
	ENDREM
	Method getShadowCasterBoundsInfo:TVisibleObjectsBoundsInfo(light:TLight) 
		Local Obj:TVisibleObjectsBoundsInfo = New TVisibleObjectsBoundsInfo
		Obj.Pointer = SM_getShadowCasterBoundsInfo(Light.Pointer, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the WORLD_GEOMETRY_TYPE_MASK mask.
	ENDREM
	Function WORLD_GEOMETRY_TYPE_MASK:Int() 
		Return SM_WORLD_GEOMETRY_TYPE_MASK() 
	End Function
	
	Rem
		bbdoc: Get the ENTITY_TYPE_MASK mask.
	ENDREM
	Function ENTITY_TYPE_MASK:Int() 
		Return SM_ENTITY_TYPE_MASK() 
	End Function
	
	Rem
		bbdoc: Get the FX_TYPE_MASK mask.
	ENDREM
	Function FX_TYPE_MASK:Int() 
		Return SM_FX_TYPE_MASK() 
	End Function
	
	Rem
		bbdoc: Get the STATICGEOMETRY_TYPE_MASK mask.
	ENDREM
	Function STATICGEOMETRY_TYPE_MASK:Int() 
		Return STATICGEOMETRY_TYPE_MASK() 
	End Function
	
	Rem
		bbdoc: Get the LIGHT_TYPE_MASK mask.
	ENDREM
	Function LIGHT_TYPE_MASK:Int() 
		Return LIGHT_TYPE_MASK() 
	End Function
	
	Rem
		bbdoc: Get the USER_TYPE_MASK_LIMIT mask.
	ENDREM
	Function USER_TYPE_MASK_LIMIT:Int() 
		Return SM_USER_TYPE_MASK_LIMIT() 
	End Function
	
	Rem
		bbdoc: This is a flow helper class that lets you do polygon level ray picking easilly. It can be rather heavy, as it generates 1:1 collision meshes through ogre and you should only be using this conservatively, and move on to a physics library if more accuracy/speed is required. <br>rsq:TRaySceneQuery The base TRaySceneQuery to use for this polygon pick. You must set one up to use with this.<br>result:TVector3 The point of collision in world space.<br>normal:TVector4 The normal of the collision.<BR>Returns: The entity determined to have been picked.
	ENDREM
	Method polyPick:TEntity(rsq:TRaySceneQuery, result:TVector3, normal:TVector4) 
		Local Obj:TEntity = New TEntity
		Obj.Pointer = POLYRSQ_run( rsq.Pointer  , result.Pointer , normal.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: This method allows you to get the terrain scene node, if you have terrain in your scene.
	ENDREM
	Method getTerrainRootNode:TSceneNode() 
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = SM_getTerrainRootNode( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Check to see whether or not this TRenderSystem is a null-reference.
	ENDREM
	Method isNull:Byte() 
		If Self.Pointer = Null Then
			Return True
		Else
			Return False
		End If
	End Method
End Type


Rem
	bbdoc: A small class that holds some information about a scene manager.
ENDREM
Type TSceneManagerMetaData
	
	Rem
		bbdoc: Pointer Reference.
	END REM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get a unique string identifying the scene manager.
	END REM
	Method getTypeName:String() 
		Return String.FromCString(SMMD_getTypeName(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get a description of the scene manager.
	END REM
	Method getDescription:String() 
		Return String.FromCString(SMMD_getDescription(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get a mask that describes what sort of scenes this scene manager can handle.
	END REM
	Method getSceneTypeMask:Int() 
		Return SMMD_getSceneTypeMask(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not this scene manager supports world geometry.
	END REM
	Method isWorldGeometrySupported:Byte() 
		Return SMMD_isWorldGeometrySupported(Self.Pointer) 
	End Method
	
End Type

Rem
	bbdoc: NOT SUPPORTED: Class that allows you to iterate through all scene manager meta data.
ENDREM
Type TSceneManagerMetaDataIterator
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: This type represents the current state of its animation and the strength of the influence it has over a mesh. Animation states should never be created directly and are managed by the scene manager.
ENDREM
Type TAnimationState

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get the name of the animation that this state utilises.
	ENDREM
	Method getAnimationName:String()
		Return String.FromCString(AS_getAnimationName(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Set whether or not this animation state is enabled.
	ENDREM
	Method setEnabled( enabled:Int )
		AS_setEnabled( enabled, Self.Pointer )
	End Method
	
	Rem
		bbdoc: Modifies the current time in this animation state. If you go over the length of this state and looping is enabled, the animation state will loop.
	ENDREM
	Method addTime( offset:Float )
		AS_addTime( offset, Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whethor not this animation loops when it has been played longer than its length forwards or backwards.
	ENDREM
	Method setLoop( Loop:Int )
		AS_setLoop( Loop , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get whethor not this animation loops when it has been played longer than its length forwards or backwards.
	ENDREM 
	Method getLoop:Byte()
		Return AS_getLoop( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the current time of this animation state.
	ENDREM
	Method setTimePosition( timePos:Float )
		AS_setTimePosition( timePos , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the current time of this animation state.
	ENDREM
	Method getTimePosition:Float()
		Return AS_getTimePosition( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the length of this animation state.
	ENDREM
	Method getLength:Float()
		Return AS_getLength( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the weight, or influence, of this animation state.
	ENDREM
	Method getWeight:Float()
		Return AS_getWeight(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the weight, or influence, of this animation state.
	ENDREM
	Method setWeight(weight:Float)
		AS_setWeight(weight, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Create a new blend mask for this animation state that will allow you to assign weight, or influence, per bone.<br/>blendMaskSizeHint:Hint - The number of bones in this animation state skeleton.<br/>initialWeight:Float = 1.0 - The initial weight of all the bones in the mask. Specify a negative value to leave the blend mask uninitialised.
	ENDREM
	Method createBlendMask(blendMaskSizeHint:Int, initialWeight:Float = 1.0)
		AS_createBlendMask(blendMaskSizeHint, initialWeight, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Destroy this animation state's current blend mask.
	ENDREM
	Method destroyBlendMask()
		AS_destroyBlendMask(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not this animation state has a blend mask.
	ENDREM
	Method hasBlendMask:Byte()
		Return AS_hasBlendMask(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Define a weight for a given bone in this animation state's blendmask by a numeric handle to the bone.
	ENDREM
	Method setBlendMaskEntry(boneHandle:Int, weight:Float)
		AS_setBlendMaskEntry(boneHandle, weight, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the weight of a specified bone in this animation state's blendmask by specifying its numeric bone handle.
	ENDREM
	Method getBlendMaskEntry:Float(boneHandle:Int)
		Return AS_getBlendMaskEntry(boneHandle, Self.Pointer)
	End Method
	
	
End Type

Rem
	bbdoc: Type that holds a set of TAnimationState objects.
ENDREM
Type TAnimationStateSet

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Specifies whether or not this object is cleaned up by garbage collection.
	ENDREM
	Field managed:Byte
	
	Rem
		bbdoc: Create an empty Animation State Set.
	ENDREM
	Function Create:TAnimationStateSet()
		Local Obj:TAnimationStateSet = New TAnimationStateSet
		Obj.Pointer = ANIMSTATESET_create()
		Obj.managed = False
		Return Obj
	End Function
	
	Rem
		bbdoc: Create an Animation State Set by using the values in another Animation State Set.
	ENDREM
	Function CreateWithAnimationStateSet:TAnimationStateSet(rhs:TAnimationStateSet)
		Local Obj:TAnimationStateSet = New TAnimationStateSet
		Obj.Pointer = ANIMSTATESET_createWithAnimationStateSet(rhs.Pointer)
		Obj.managed = False
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		If Not managed
			ANIMSTATESET_delete(Self.Pointer)
		End If
	End Method
	
	Rem
		bbdoc: Add an animation state to this TAnimationStateSet.
	ENDREM
	Method createAnimationState:TAnimationState(animName:String, timePos:Float, length:Float, weight:Float, enabled:Byte)
		Local Obj:TAnimationState = New TAnimationState
		Obj.Pointer = ANIMSTATESET_createAnimationState(animName.ToCString() , timePos, length, weight, enabled, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get an animation state from this Animation State Set ( by name ).
	ENDREM
	Method getAnimationState:TAnimationState(name:String)
		Local Obj:TAnimationState = New TAnimationState
		Obj.Pointer = ANIMSTATESET_getAnimationState(name.ToCString() , Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Check to see whether an animation state exists within this Animation State Set.
	ENDREM
	Method hasAnimationState:Byte(name:String)
		Return ANIMSTATESET_hasAnimationState(name.ToCString() , Self.Pointer)
	End Method
	
	Rem
		bbdoc: Remove an animation state from this TAnimationStateSet.
	ENDREM
	Method removeAnimationState(name:String)
		ANIMSTATESET_removeAnimationState(name.ToCString() , Self.Pointer)
	End Method
	
	Rem
		bbdoc: Remove all animation states from this TAnimationStateSet.
	ENDREM
	Method removeAllAnimationStates()
		ANIMSTATESET_removeAllAnimationStates(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get an iterator for this Animation State Set.
	ENDREM
	Method getAnimationStateIterator:TAnimationStateIterator()
		Local Obj:TAnimationStateIterator = New TAnimationStateIterator
		Obj.Pointer = ANIMSTATESET_getAnimationStateIterator(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Copy any matching animation states from the target TAnimationStateSet to this one.
	ENDREM
	Method copyMatchingState(target:TAnimationStateSet)
		ANIMSTATESET_copyMatchingState(target.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Specify that an animation state in this set has been changed and should be considered dirty.
	ENDREM
	Method _notifyDirty()
		ANIMSTATESET__notifyDirty(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the frame number of the dirty animation state.
	ENDREM
	Method getDirtyFrameNumber:Long()
		Return ANIMSTATESET_getDirtyFrameNumber(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Internal method to handle notification for enabling or disabling an animation state in this set.
	ENDREM
	Method _notifyAnimationStateEnabled(target:TAnimationState, enabled:Byte)
		ANIMSTATESET__notifyAnimationStateEnabled(target.Pointer, enabled, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Checks to see whether or not this animation state has any enabled animation states.
	ENDREM
	Method hasEnabledAnimationState:Byte()
		Return ANIMSTATESET_hasEnabledAnimationState(Self.Pointer)
	End Method
End Type

Rem
	bbdoc: This class describes animation sequences. Animation sequences are made up of tracks of movement, defined by nodes, vertices or splines.
ENDREM
Type TAnimation
	
	Rem
		bbdoc: Pointer Reference.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Linear animation interpolation constant.
	ENDREM
	Const IM_LINEAR:Int = 0
	
	Rem
		bbdoc: Spine based animation interpolation constant.
	ENDREM
	Const IM_SPLINE:Int = 1
	
	Rem
		bbdoc: Linear rotation interpolation, less costly and less accurate.
	ENDREM
	Const RIM_LINEAR:Int = 0
	
	Rem
		bbdoc: Spherical rotation interpolation, more costly and more accurate.
	ENDREM
	Const RIM_SPHERICAL:Int = 1
	
	Rem
		bbdoc: Get the name of this TAnimation as a string.
	ENDREM
	Method getName:String() 
		Return String.FromCString(ANIMATION_getName(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the length of the complete animation as a float.
	ENDREM
	Method getLength:Float() 
		Return ANIMATION_getLength(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Create a new TNodeAnimationTrack in this TAnimation, specifying a handle and a node for it to affect.
	ENDREM
	Method createNodeTrack:TNodeAnimationTrack(handle:Int, node:TOgreNode)
		Local Obj:TNodeAnimationTrack = New TNodeAnimationTrack
		Obj.Pointer = ANIMATION_createNodeTrack(handle, node.Pointer, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Create new TNodeAniamtionTrack in this TAnimation, specifying a handle and a bone for it to affect.
	ENDREM
	Method createNodeTrackWithBone:TNodeAnimationTrack(handle:Int, node:TBone)
		Local Obj:TNodeAnimationTrack = New TNodeAnimationTrack
		Obj.Pointer = ANIMATION_createNodeTrackWithBone(handle, node.Pointer, Self.Pointer)
		Return Obj	
	End Method
	
	Rem
		bbdoc: Get the number of TNodeAnimationTrack s  stored in this object.
	ENDREM
	Method getNumNodeTracks:Int() 
		Return ANIMATION_getNumNodeTracks(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the node track at the specified index. If there is no node-track your program will throw an exception! Use hasNodeTrack or getNumNodeTracks to determine what you have to work with.<br> Track handles/indexes start at 1.
	ENDREM
	Method getNodeTrack:TNodeAnimationTrack(Handle:Int) 
		Return TNodeAnimationTrack.FromPtr(ANIMATION_getNodeTrack(Handle, Self.Pointer)) 
	End Method
		
	Rem
		bbdoc: Get the number of TNumericAnimationTrack s  stored in this object.
	ENDREM
	Method getNumNumericTracks:Int() 
		Return ANIMATION_getNumNumericTracks(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the number of TVertexAnimationTrack s  stored in this object.
	ENDREM
	Method getNumVertexTracks:Int() 
		Return ANIMATION_getNumVertexTracks(Self.Pointer) 
	End Method

	Rem
		bbdoc: Apply an animation with specified time and weight, with an optional scaling factor.<br>timePos:Float - The time position of the animation you want to apply.<br>weight:Float - The weight of the animation. At 1.0 this animation will have full influence. At lesser values this animation will blend into others.<br>scale:Float = 1.0f - The scaling factor for this animation. Good for varying size targets.
	ENDREM
	Method apply(timePos:Float, weight:Float = 1.0, scale:Float = 1.0) 
		ANIMATION_apply(timePos, weight, scale, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the standard way to interpolate between key frames. Your options are:<br>TAnimation.IM_LINEAR - For linear interpolation.<br>TAnimation.IM_SPLINE - For spline based interpolation.
	ENDREM
	Method setInterpolationMode(im:Int)
		ANIMATION_setInterpolationMode(im, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the standard way to interpolate rotations between key frames. Your options are:<br/>TAnimation.RIM_LINEAR (default) - For using linear interpolation. Less costly and less accurate.<br/>TAnimation.RIM_SPHERICAL - For using spherical rotation interpolation. More costly and more accurate.
	ENDREM
	Method setRotationInterpolationMode(im:Int)
		ANIMATION_setRotationInterpolationMode(im, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the internal list of node animation tracks that this TAnimation holds.
	ENDREM
	Method _getNodeTrackList:TNodeTrackList()
		Local Obj:TNodeTrackList = New TNodeTrackList
		Obj.Pointer = ANIMATION__getNodeTrackList(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the default interpolation mode for any animation created after this function is used. Your options are:<br>TAnimation.IM_LINEAR - For linear interpolation.<br>TAnimation.IM_SPLINE - For spline based interpolation.
	ENDREM
	Function setDefaultInterpolationMode( im:Int )
		ANIMATION_setDefaultInterpolationMode( im )
	End Function
End Type

Rem
	bbdoc: Describes a list of node track animations
ENDREM
Type TNodeTrackList
	
	Rem
		bbdoc: Pointer to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		NODETRACKLIST_delete(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get an iterator that starts at the beginning of the track list.
	ENDREM
	Method begin:TNodeTrackListIter()
		Local Obj:TNodeTrackListIter = New TNodeTrackListIter
		Obj.Pointer = NODETRACKLIST_begin(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get an iterator that starts at the end of the track list.
	ENDREM
	Method ending:TNodeTrackListIter()
		Local Obj:TNodeTrackListIter = New TNodeTrackListIter
		Obj.Pointer = NODETRACKLIST_end(Self.Pointer)
		Return Obj
	End Method
	
	
End Type

Rem
	bbdoc: Iterator for iterating through TNodeTrackLists .
ENDREM
Type TNodeTrackListIter
	
	Rem
		bbdoc: Pointer to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr

	Rem
		bbdoc: Destructor.
	EndRem
	Method Delete()
		NODETRACKLISTITER_delete(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Check to see wether or not another iterator's position is not equal to this iterator's.
	ENDREM
	Method isNotEqualTo:Byte(i2:TNodeTrackListIter)
		Return NODETRACKLISTITER_isNotEqualTo(i2.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Move the position of this iterator up 1 element.
	ENDREM
	Method moveNext()
		NODETRACKLISTITER_moveNext(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the node animation track that this iterator currently points to.
	ENDREM
	Method getAnimationTrack:TNodeAnimationTrack()
		Local Obj:TNodeAnimationTrack = New TNodeAnimationTrack
		Obj.Pointer = NODETRACKLISTITER_getAnimationTrack(Self.Pointer)
		Return Obj
	End Method
	
	
End Type

Rem
	bbdoc: Interface for animation tracks that use node transforms for their sequences.
ENDREM
Type TNodeAnimationTrack
	
	Rem
		bbdoc: Pointer Reference.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Adds a keyframe to this animation track. You should declare these in order if possible to avoid additional processing. Also note that a 0.0 keyframe is always created for you.
	ENDREM
	Method createNodeKeyFrame:TTransformKeyFrame(timePos:Float) 
		Local Obj:TTransformKeyFrame = New TTransformKeyFrame
		Obj.Pointer = NODEANIMTRACK_createNodeKeyFrame(timePos, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the associated node to which this animation track will be applied to.
	ENDREM
	Method getAssociatedNode:TOgreNode() 
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = NODEANIMTRACK_getAssociatedNode(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Apply this track to it's associated node.<br>timeIndex:Float - The time position to apply to the animation.<br>weight:Float = 1.0 - The amount of influence this track has. 1.0 will give full influence while lower numbers will blend the track with other animations.<br>scale:Float = 1.0 - The scale to apply translations and scaling to, this will help you scale up or down to a specified target size.
	EndRem
	Method apply(timeIndex:Float, weight:Float = 1.0, scale:Float = 1.0)
		NODEANIMTRACK_apply(timeIndex, weight, scale, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Apply this track to it's associated node using TTimeIndex.<br>timeIndex:TTimeIndex - The time position to apply to the animation.<br>weight:Float = 1.0 - The amount of influence this track has. 1.0 will give full influence while lower numbers will blend the track with other animations.<br>scale:Float = 1.0 - The scale to apply translations and scaling to, this will help you scale up or down to a specified target size.
	ENDREM
	Method applyWithTimeIndex(timeIndex:TTimeIndex, weight:Float = 1.0, scale:Float = 1.0)
		NODEANIMTRACK_applyWithTimeIndex(timeIndex.Pointer, weight, scale, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Function that creates a TNodeAnimationTrack object from a Byte Ptr.
	ENDREM
	Function FromPtr:TNodeAnimationTrack(Pointer:Byte Ptr) 
		Local Obj:TNodeAnimationTrack = New TNodeAnimationTrack
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
End Type

Rem
	bbdoc: A generic Ogre keyframe for Ogre animations. For specific implementations look through the children implementations.
ENDREM
Type TOgreKeyFrame

	Rem
		bbdoc: Reference to this object in memory.
	EndRem
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get the time spot for this keyframe.
	ENDREM
	Method getTime:Float()
		Return OGREKEYFRAME_getTime(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Clone this keyframe through a specified parent instance. Only use this if you know what you're doing.
	ENDREM
	Method _clone:TOgreKeyFrame(newParent:TAnimationTrack)
		Local obj:TOgreKeyFrame = New TOgreKeyFrame
		Obj.Pointer = OGREKEYFRAME__clone(newParent.Pointer, Self.Pointer)
		Return Obj
	End Method
	
		

End Type

Rem
	bbdoc: A keyframe that uses basic transformations to manipulate the scene.
ENDREM
Type TTransformKeyFrame Extends TOgreKeyFrame

	Rem
		bbdoc: Set where you want your object to translate for this keyframe.
	ENDREM
	Method setTranslate(trans:TVector3)
		TFORMKEYFRAME_setTranslate(trans.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc:Get where this Object will translate For this keyframe.
	ENDREM		
	Method getTranslate:TVector3()
		Local Obj:TVector3 = New TVector3
		Obj.Pointer = TFORMKEYFRAME_getTranslate(Self.Pointer)
		Obj.managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the scale of the object for this keyframe.
	ENDREM
	Method SetScale(scale:TVector3)
		TFORMKEYFRAME_setScale(scale.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the scale of the object in this keyframe.
	ENDREM		
	Method GetScale:TVector3()
		Local Obj:TVector3 = New TVector3
		Obj.Pointer = TFORMKEYFRAME_getScale(Self.Pointer)
		Obj.managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the rotation of the object for this frame via quaternion.
	ENDREM
	Method SetRotation(rot:TQuaternion)
		TFORMKEYFRAME_setRotation(rot.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the rotation of the object in this keyframe.
	ENDREM		
	Method GetRotation:TQuaternion()
		Local Obj:TQuaternion = New TQuaternion
		Obj.Pointer = TFORMKEYFRAME_getRotation(Self.Pointer)
		Obj.managed = False
		Return Obj
	End Method
	
End Type

Rem
	bbdoc: NOT SUPPORTED: An Ogre animation track instance.
ENDREM
Type TAnimationTrack
	
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
End Type

Rem
	bbdoc: Time index for searching a keyframe at the given position.
ENDREM
Type TTimeIndex
	
	Rem
		bbdoc: Pointer Reference.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Defines whether or not this object is deleted by GC or some other means.
	ENDREM
	Field Managed:Byte
	
	Rem
		bbdoc: Create a TTimeIndex with it's index at a given time position.
	ENDREM
	Function Create:TTimeIndex(timePos:Float) 
		Return TTimeIndex.FromPtr(TIMEINDEX_create(timePos)) 
	End Function
	
	Rem
		bbdoc: Create a TTimeIndex object from a time position and a keyframe index.
	ENDREM
	Function CreateWithIndex:TTimeIndex(timePos:Float, keyIndex:Int) 
		Return TTimeIndex.FromPtr(TIMEINDEX_createWithIndex(timePos, keyIndex)) 
	End Function
	
	Rem
		bbdoc: Check to see whether or not this TTimeIndex has a key index associated with it.
	ENDREM
	Method hasKeyIndex:Byte() 
		Return TIMEINDEX_hasKeyIndex(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the time position data contained in this TTimeIndex object.
	ENDREM
	Method getTimePos:Float() 
		Return TIMEINDEX_getTimePos(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the key frame index associated with this TTimeIndex. Returns -1 if no key index exists.
	EndRem
	Method getKeyIndex:Int() 
		Return TIMEINDEX_getKeyIndex(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		If Managed <> True And Pointer <> Null Then
			TIMEINDEX_delete(Self.Pointer)
			Pointer = Null
		End If
	End Method
	
	Rem
		bbdoc: Manual destructor. This won't delete managed instances, but will allow for quick cleanup of unmanaged instances.
	ENDREM
	Method Destroy()
		If Managed <> True And Pointer <> Null Then
			TIMEINDEX_delete(Self.Pointer)
			Pointer = Null
		End If
	End Method
	
	Rem
		bbdoc: Function that creates a TTimeIndex object from a Byte Ptr.
	ENDREM
	Function FromPtr:TTimeIndex(Pointer:Byte Ptr, Managed:Byte = False)
		Local Obj:TTimeIndex = New TTimeIndex
		Obj.Pointer = Pointer
		Obj.Managed = Managed
		Return Obj
	End Function
	

	
	
	
End Type

Rem
	bbdoc: A simple timer class.
ENDREM
Type TOgreTimer
	Field Pointer:Byte Ptr
	Field Managed:Byte
	
	Rem
		bbdoc: Constructor. Creates a TOgreTimer instance that starts counting after creation.
	ENDREM
	Function Create:TOgreTimer() 
		Local Obj:TOgreTimer = New TOgreTimer
		Obj.Managed = False
		Obj.Pointer = OGRETIMER_create() 
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor
	ENDREM
	Method Delete() 
		If Self.Managed = False Then OGRETIMER_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Reset this timer.
	ENDREM
	Method reset() 
		OGRETIMER_reset(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the time ( in milliseconds ) as a float.
	ENDREM
	Method getTime:Float() 
		Return OGRETIMER_getTime(Self.Pointer) 
	End Method
	
End Type

Rem
	bbdoc: NOT SUPPORTED: Iterates through a camera list.
ENDREM
Type TCameraIterator
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Iterates through an animation list.
ENDREM
Type TAnimationIterator

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		ANIMITER_delete(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not there are more elements in this iterator from its current position.
	ENDREM
	Method hasMoreElements:Byte()
		Return ANIMITER_hasMoreElements(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the next TAnimation contained in this animation iterator.
	ENDREM
	Method getNext:TAnimation()
		Local Obj:TAnimation = New TAnimation
		Obj.Pointer = ANIMITER_getNext(Self.Pointer)
		Return Obj
	End Method
End Type

Rem
	bbdoc: Iterates through an animation state list.
ENDREM
Type TAnimationStateIterator

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		ANIMSTATEITER_delete(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not there are more elements in this iterator from its current position.
	ENDREM
	Method hasMoreElements:Byte()
		Return ANIMSTATEITER_hasMoreElements(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the next TAnimationState contained in this animation state iterator.
	ENDREM
	Method getNext:TAnimationState()
		Local Obj:TAnimationState = New TAnimationState
		Obj.Pointer = ANIMSTATEITER_getNext(Self.Pointer)
		Return Obj
	End Method
	
End Type

Rem
	bbdoc: NOT SUPPORTED: Contains individual shadow texture information.
ENDREM
Type TShadowTextureConfig
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: NOT SUPPORTED: Iterates through shadow texture configurations.
ENDREM
Type TConstShadowTextureConfigIterator
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: NOT SUPPORTED: contains individual shadow camera setup information.
ENDREM
Type TShadowCameraSetup
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: NOT SUPPORTED: Listener for scene manager.
ENDREM
Type TSceneManagerListener
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: NOT SUPPORTED: GPU Instanced geometry implementation.
ENDREM
Type TInstancedGeometry
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: NOT SUPPORTED: Iterates through movable object lists.
ENDREM
Type TMovableObjectIterator
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Low level collection of renderables. This class only contains constants at the moment, and none of its impelementation details.
ENDREM
Type TQueuedRenderableCollection

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Organisational Mode : Group by pass.
	ENDREM
	Const OM_PASS_GROUP:Int = 1
	
	Rem
		bbdoc: Organisational Mode : Sort descending with distance from camera.
	ENDREM
	Const OM_SORT_DESCENDING:Int = 2
	
	Rem
		bbdoc: Organisational Mode : Sort ascending with distance from camera.
	ENDREM
	Const OM_SORT_ASCENDING:Int = 6
End Type


Rem
	bbdoc: Interface that allows you to manipulate Hardware Occlusion operations of a particular TRenderSystem
ENDREM
Type THardwareOcclusionQuery

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Destructor for this type.
	ENDREM
	Method Delete()
		HOQ_Delete( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Begin occlusion query test.
	ENDREM
	Method beginOcclusionQuery()
		HOQ_beginOcclusionQuery( Self.Pointer )
	End Method
	
	Rem
		bbdoc: End occlusion query test.
	ENDREM
	Method endOcclusionQuery()
		HOQ_endOcclusionQuery( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Obtains the query results once they are available.<br>NumOfFragments:Int The number of fragments resulting from the query will be stored in this variable.( use VarPtr ). Returns true if this method is successful.
	ENDREM
	Method pullOcclusionQuery:Int(NumOfFragments:Int Var) 
		Return HOQ_pullOcclusionQuery( Varptr( NumOfFragments )  , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the last completed query's fragment count.
	ENDREM
	Method getLastQuerysPixelcount:Int() 
		Return HOQ_getLastQuerysPixelcount( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Tests whether the occlusion test is still running.
	ENDREM
	Method isStillOutstanding:Int() 
		Return HOQ_isStilloutstanding( Self.Pointer )
	End Method
	
	
	
End Type

Rem
	bbdoc: This class provides a simplified interface to drawing custom geometry. Normally one would have to go into the vertex buffer and create triangles. Here the interface is akin to OpenGL immediate mode.<br/>You start .begin() and then you define vertices by calling .position(...). Then you can call .normal(...), etc to set vertice information. When you are done you call .end(...) to finish defining the manual object. To display the object you will have to attach it to a scene node.
ENDREM
Type TManualObject Extends TMovableObject
	Rem
		bbdoc: Override new to provide the proper vartype.
	ENDREM
	Method New() 
		Self.vartype = 6
	End Method

	
	Rem
		bbdoc: This destorys the entire manual object buffer and you have to recreate it again by calling begin(...). Doing this constantly is not good for performance, and you should probably use beginUpdate(...) if you are going to update this object often.
	ENDREM
	Method clear()
		MANUALOBJECT_clear(Self.Pointer)
	End Method
	
	Rem
		bbdoc: This method allows you to specify the number of vertexes ahead of time. While dynamic allocation is supported, it is much more expedient to allocate all of the buffer space at once.
	ENDREM
	Method estimateVertexCount(vcount:Int)
		MANUALOBJECT_estimateVertexCount(vcount, Self.Pointer)
	End Method
	
	Rem
		bbdoc: This method allows you to specify the number of indexes ahead of time. While dynamic allocation is supported, it is much more expedient to allocate all of the buffer space at once.
	ENDREM
	Method estimateIndexCount(icount:Int)
		MANUALOBJECT_estimateVertexCount(icount, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Allocate buffer space and start a new section of this manual object. You can use seperate materials and rendreing types for each section.<br/>materialName: The name of the material to use for this manual object.<br/>opType:Int = TRenderOperation.OT_TRIANGLE_LIST - The rendering operation type. With this you can render lines, etc. Check TRenderOperation constants for more information.
	ENDREM
	Method begin(materialName:String, opType:Int = TRenderOperation.OT_TRIANGLE_LIST)
		MANUALOBJECT_begin(materialName.ToCString() , opType, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set whether or not this manual object is going to get updated often. There are performance benefits to structuring the manual object one way or the other.
	ENDREM
	Method setDynamic(dyn:Byte)
		MANUALOBJECT_setDynamic(dyn, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not this manual object is structured to be updated often.
	ENDREM
	Method getDynamic:Byte()
		Return MANUALOBJECT_getDynamic(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Start an update block for this manual object at the specified index. This will update a section of your manual object, though the render operation must stay the same. If you are updating your object frequently, it is best to use this method. Note that if the updates are causing your object to grow or shrink, it's best to call estimateVertexCount(...) to keep your manual object performance friendly.
	ENDREM
	Method beginUpdate(sectionIndex:Int)
		MANUALOBJECT_beginUpdate(sectionIndex, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Start a new vertex at the specified world position. All methods past the initial position(...) call that are not another position(...) call are assumed to be modifying the current current created vertice.
	ENDREM
	Method positionWithVector3(pos:TVector3)
		MANUALOBJECT_positionWithVector3(pos.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Start a new vertex at the specified position. All methods past the initial position(...) call that are not another position(...) call are assumed to be modifying the current current created vertice.
	ENDREM
	Method position(x:Float, y:Float, z:Float)
		MANUALOBJECT_position(x, y, z, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Define a  vertex normal for the last vertex created by a .position(...) call. The vector specified should be a normalised ( magnitude of 1 ).
	ENDREM
	Method normalWithVector3(norm:TVector3)
		MANUALOBJECT_normalWithVector3(norm.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Define a  vertex normal for the last vertex created by a .position(...) call. The vector specified should be a normalised ( magnitude of 1 ).
	ENDREM
	Method normal(x:Float, y:Float, z:Float)
		MANUALOBJECT_normal(x, y, z, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Give the current vertex a 1d texture coordinate.	You can use this as many times as you like to have multiple texture coordinates per vertice.
	ENDREM
	Method textureCoord1D(u:Float)
		MANUALOBJECT_textureCoord1D(u, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Give the current vertex a uv texture coordinate. You can use this as many times as you like to have multiple texture coordinates per vertice.
	ENDREM
	Method textureCoord(u:Float, v:Float)
		MANUALOBJECT_textureCoord(u, v, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Give the current vertex a uvw texture coordinate. You can use this as many times as you like to have multiple texture coordinates per vertice.
	ENDREM
	Method textureCoord3D(u:Float, v:Float, w:Float)
		MANUALOBJECT_textureCoord3D(u, v, w, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Give the current vertex a xyzw texture coordinate. You can use this as many times as you like to have multiple texture coordinates per vertice.
	ENDREM
	Method textureCoord4D(x:Float, y:Float, z:Float, w:Float)
		MANUALOBJECT_textureCoord4D(x, y, z, w, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Give the current vertex a uv texture coordinate. You can use this as many times as you like to have multiple texture coordinates per vertice.
	ENDREM
	Method textureCoordWithVector2(uv:TVector2)
		MANUALOBJECT_textureCoordWithVector2(uv.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Give the current vertex a uvw texture coordinate. You can use this as many times as you like to have multiple texture coordinates per vertice.
	ENDREM
	Method textureCoordWithVector3(uvw:TVector3)
		MANUALOBJECT_textureCoordWithVector3(uvw.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Give the current vertex a xyzw texture coordinate. You can use this as many times as you like to have multiple texture coordinates per vertice.
	ENDREM
	Method textureCoordWithVector4(xyzw:TVector4)
		MANUALOBJECT_textureCoordWithVector4(xyzw.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Add a vertex colour to the current vertex using a TColourValue.
	ENDREM
	Method colourWithColourValue(col:TColourValue)
		MANUALOBJECT_colourWithColourValue(col.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Add a vertex colour to the current vertex using rgba with relative values (0.0-1.0).
	ENDREM
	Method colour(r:Float, g:Float, b:Float, a:Float = 1.0)
		MANUALOBJECT_colour(r, g, b, a, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Add a single vertex index to use in the construction of your specified render operation type.
	ENDREM
	Method index(idx:Int)
		MANUALOBJECT_index(idx, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Add three vertex indexes to this manual object. This could be used to create a single face, depending on your render operation type.
	ENDREM
	Method triangle(i1:Float, i2:Float, i3:Float)
		MANUALOBJECT_triangle(i1, i2, i3, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Add four vertex indexes to this manual object. This could be used to create a quad depending on your render operation type.
	ENDREM
	Method quad(i1:Float, i2:Float, i3:Float, i4:Float)
		MANUALOBJECT_quad(i1, i2, i3, i4, Self.Pointer)
	End Method
	
	Rem
		bbdoc: This completes the definition for a section of your manual object starting with the last called .begin(...).<br/>Returns the created manual object section for later manipulation. If it has the potential of containing no vertices,you may want to check that .isNotNull() = True.
	ENDREM
	Method ending:TManualObjectSection()
		Local Obj:TManualObjectSection = New TManualObjectSection
		Obj.Pointer = MANUALOBJECT_end(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: If you want to change the material for a subsection of this manual object, you may do so here by specifying the subsection index and the new material name.
	ENDREM
	Method setMaterialName(subindex:Int, name:String)
		MANUALOBJECT_setMaterialName(subindex, name.ToCString() , Self.Pointer)
	End Method
	
	Rem
		bbdoc: If you have indexed this manual object properly, you may use this method to convert it to a mesh, specifying its name and resource group. There are two reasons for doign this:<br/>1. You want to export this object and save it as a mesh.<br/>2. You want to use multiple instances of this object in your scene, since manual objects can only be attached to one scene node per instance.
	ENDREM
	Method convertToMesh:TMesh(meshName:String, groupName:String = DEFAULT_RESOURCE_GROUP_NAME)
		Return TMesh.FromPtr(MANUALOBJECT_convertToMesh(meshName.ToCString() , groupName.ToCString() , Self.Pointer))
	End Method
	
	Rem
		bbdoc: Enable this if you want your manual object to effectively act as a 2D overlay, as setting this to "true" causes the manual object to completely ignore the current camera's projection matrix. This is false by default.
	ENDREM
	Method setUseIdentityProjection(useIdentityProjection:Byte)
		MANUALOBJECT_setUseIdentityProjection(useIdentityProjection, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not this manual object uses identity projection. With identity projection turned on, this manual object will ignore active camera projection and effectively become a 2d overlay.
	ENDREM
	Method getUseIdentityProjection:Byte()
		Return MANUALOBJECT_getUseIdentityProjection(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set whether or not this manual object uses an identity view matrix. By default manual objects use the active camera's view matrix - if you want to give your manual object relative coordinates useful for 2D overlay rendering, set this to true.
	ENDREM
	Method setUseIdentityView(useIdentityView:Byte)
		MANUALOBJECT_setUseIdentityView(useIdentityView, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not this manual object uses an identity view matrix. By default manual objects use the active camera's view matrix - if you want to give your manual object relative coordinates useful for 2D overlay rendering, set this to true.
	ENDREM
	Method getUseIdentityView:Byte()
		Return MANUALOBJECT_getUseIdentityView(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the axis aligned bounding box for this manual object.
	ENDREM
	Method setBoundingBox(box:TAxisAlignedBox)
		MANUALOBJECT_setBoundingBox(box.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get a subsection of this TManualObject as specified by the index provided.
	ENDREM
	Method getSection:TManualObjectSection(index:Int)
		Local Obj:TManualObjectSection = New TManualObjectSection
		Obj.Pointer = MANUALOBJECT_getSection(index, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the number of subsections in this manual object.
	ENDREM
	Method getNumSections:Int()
		Return MANUALOBJECT_getNumSections(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set whether or not to keep the original declaration order. This allows you to override render queue behavior and set the desired rendering order. Might hurt performance a little.
	ENDREM
	Method setKeepDeclarationOrder(keepOrder:Byte)
		MANUALOBJECT_setKeepDeclarationOrder(keepOrder, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not to keep the original declration order.
	ENDREM
	Method getKeepDeclarationOrder:Byte()
		Return MANUALOBJECT_getKeepDeclarationOrder(Self.Pointer)
	End Method	

End Type

Rem
	bbdoc: A section of a manual object.
ENDREM
Type TManualObjectSection Extends TRenderable

	Rem
		bbdoc: Get the render operation used on this Manual Object Section. Note that this is different from getRenderOperation in that it returns the referenced render operation for you to modify.
	ENDREM
	Method getRenderOperationRef:TRenderOperation()
		Local Obj:TRenderOperation = New TRenderOperation
		Obj.Pointer = MOS_getRenderOperationRef(Self.Pointer)
		Obj.managed = True
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the material name used for this manual object section.
	ENDREM
	Method getMaterialName:String()
		Return String.FromCString(MOS_getMaterialName(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Set the material name used for this manual object section.
	ENDREM
	Method setMaterialName(name:String)
		MOS_setMaterialName(name.ToCString() , Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set whether or not this section uses 32-bit indices.
	ENDREM
	Method set32BitIndices(n32:Byte)
		MOS_set32BitIndices(n32, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not this section uses 32-bit indices.
	ENDREM
	Method get32BitIndices:Byte()
		Return MOS_get32BitIndices(Self.Pointer)
	End Method

End Type

Rem
	bbdoc: NOT SUPPORTED: Frame listener type.
ENDREM
Type TFrameListener
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: NOT SUPPORTED: Ogre plug-in implementation .
ENDREM
Type TPlugin
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: NOT SUPPORTED: Frame Event implementation.
ENDREM
Type TFrameEvent
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: NOT SUPPORTED: Movable Object Factory implementation.
ENDREM
Type TMovableObjectFactory
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: NOT SUPPORTED: Visible object bounding information.
ENDREM
Type TVisibleObjectsBoundsInfo
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: The Ogre Interface that shadow casting objects  use.
ENDREM
Type TShadowCaster
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get whether or not this object is set to cast shadows.
	ENDREM
	Method getCastShadows:Int()
		Return SHADOWCASTER_getCastShadows( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the edgelist for this shadowcaster in TEdgeData vector form.
	ENDREM
	Method getEdgeList:TEdgeList()
		Local Obj:TEdgeList = New TEdgeList
		Obj.Pointer = SHADOWCASTER_getEdgeList(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Retreive the flag that specifies if the object has an edge list or not.
	ENDREM
	Method hasEdgeList:Int()
		Return SHADOWCASTER_hasEdgeList( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the world bounding box of the TShadowCaster.
	ENDREM
	Method getWorldBoundingBox:TAxisAlignedBox(derive:Int = False)
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = SHADOWCASTER_getWorldBoundingBox(derive, Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Retreive the world TAxisAlignedBox for the light cap.<br/>Returns:A TAxisAlignedBox that contains the metrics of the light cap axis-aligned box for the TShadowCaster.  
	ENDREM
	Method getLightCapBounds:TAxisAlignedBox()
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = SHADOWCASTER_getLightCapBounds(Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Retreive the world TAxisAlignedBox for the dark cap, using the light for extrusion.<br/>dirLightExtrusionDist:Float The distance of the extrusion.<br/>l:TLight The TLight to be used for extrusion calculations.<br/>A TAxisAlignedBox that contains the metrics of the dark cap  axis-aligned box for the TShadowCaster.
	ENDREM
	Method getDarkCapBounds:TAxisAlignedBox(l:TLight, dirLightExtrusionDist:Float)
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = SHADOWCASTER_getDarkCapBounds(l.Pointer, dirLightExtrusionDist, Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Retreive the distance the shadow volume must be extruded from a specified TLight.
	ENDREM
	Method getPointExtrusionDistance:Float(l:TLight)
		Return SHADOWCASTER_getPointExtrusionDistance( l.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED: Static function to exture vertices in a vertex buffer based on light calculations.<br>vertexBuffer: Must be originalVertexCount*2*3 floats long to account for doubling of the vertices for the extrude.<br>originalVertexCount:Long The original number of vertices before the doubling.<br>lightPos:TVector4  The light position in 4d space, the w will be populated if the light is directional.<br>extrudeDist:Float The distance to extrude the vertices.
	ENDREM
	Function extrudeVertices(vertexBuffer:THardwareVertexBufferSharedPtr, originalVertexCount:Long, lightPos:TVector4, extrudeDist:Float) 
		SHADOWCASTER_extrudeVertices(vertexBuffer.Pointer, originalVertexCount, lightPos.pointer, extrudeDist) 
	End Function
	
	
	
End Type

 
Rem
	bbdoc: Interface that describes and manipulates a region of  TRenderTarget that is usually attached to a single TCamera.
ENDREM
Type TViewport

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Function that creates a TViewport manually with specified parameters. While this option has finer control, it's generally adviseable, if you do not need this control, use TRenderWindow.addViewport(...) instead. <br>camera:TCamera The single camera that this TViewport will look through.<br>target:RenderTarget The TRenderTarget that this TViewport will be located on.<br>left:Float Horizontal location of the TViewport on the TRenderTarget in relative coordinates (0.0-1.0).<br>top:Float Vertical location of the TViewport on the TRenderTarget in relative coordinates(0.0-1.0).<br>width:Float The width of the TViewport on the TRenderTarget in relative coordinates(0.0-1.0).<br>height:Float The height  of the TViewport on the TRenderTarget in relative coordinates(0.0-1.0).<br>ZOrder:Int The index that determins the depth on TViewports when any may overap. The lower the index, the "higher" the TViewport will be above the others.
	ENDREM
	Function Create:TViewport(camera:TCamera, target:TRenderTarget, Left:Float, top:Float, width:Float, Height:Float, ZOrder:Int) 
		Local obj:TViewport = New TViewport
		Obj.Pointer = VP_create( camera.Pointer , target.Pointer , Left , top , width , height , ZOrder )
		Return Obj
	End Function
	
	Rem
		bbdoc: Used internal to notify the TViewport that it needs to update it's dimensions. Usually because the TRenderTarget's dimensions have changed and it must change relative to them. This is generally used as an internal function only.
	ENDREM
	Method _updateDimensions()
		VP__updateDimensions( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Notifies the TViewport that it needs to update it's contents.
	ENDREM
	Method update()
		VP_update( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the TRenderTarget that the TViewport currently resides on.
	ENDREM
	Method getTarget:TRenderTarget() 
		Local Obj:TRenderTarget = New TRenderTarget
		Obj.Pointer = VP_getTarget( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the single camera that the TViewport is using displaying.
	ENDREM
	Method getCamera:TCamera() 
		Local Obj:TCamera = New TCamera
		Obj.Pointer = VP_getCamera( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the TCamera that the TViewport will "look" through.
	ENDREM
	Method setCamera(ogreCamera:TCamera) 
		VP_setCamera( ogreCamera.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the ZOrder sorting index of the TViewport.
	ENDREM
	Method getZOrder:Int() 
		Return VP_getZOrder( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the horizontal positioning of the TViewport on the TRenderTarget (0.0-1.0).
	ENDREM
	Method getLeft:Float() 
		Return VP_getLeft(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the vertical  positioning of the TViewport on the TRenderTarget (0.0-1.0).
	ENDREM
	Method getTop:Float() 
		Return VP_getTop( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the current width of the TViewport in relative coordinates (0.0-1.0).
	ENDREM
	Method getWidth:Float() 
		Return VP_getWidth( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the current height of the TViewport in relative coordinates. (0.0-1.0)
	ENDREM
	Method getHeight:Float() 
		Return VP_getHeight( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the value of the TViewports offset from the left of the TRenderTarget IN PIXELS.
	ENDREM
	Method getActualLeft:Int() 
		Return VP_getActualLeft( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the value of the TViewports offset from the top  of the TRenderTarget IN PIXELS.
	ENDREM
	Method getActualTop:Int() 
		Return VP_getActualTop( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the width of the TViewport IN PIXELS.
	ENDREM
	Method getActualWidth:Int() 
		Return VP_getActualWidth( Self.Pointer )
	End Method
		
	Rem
		bbdoc: Get the width of the TViewport IN PIXELS.
	ENDREM
	Method getActualHeight:Int() 
		Return VP_getActualHeight( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the dimensions of the TViewport in relative coordinates (0.0-1.0). <br>offsetLeft:Float Specify the offset from the left of the TRenderTarget that you want your TViewport to be placed (0.0-1.0).<br>offsetTop:Float Specify the offset from the top of the TRenderTarget that you want your TViewport to be placed (0.0-1.0). <br>width:Float Specify the width of the TViewport in relative coordinates (0.0-1.0).<br>height:Float Specify the height of the TViewport in relative coordinates(0.0-1.0)
	ENDREM
	Method setDimensions(offsetLeft:Float, offsetTop:Float, width:Float, Height:Float) 
		VP_setDimensions( offsetLeft ,offsetTop , width , height , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the unrendered portions of the TViewport to a specific color.
	ENDREM
	Method setBackgroundColour(colour:TColourValue) 
		VP_setBackgroundColour( colour.Pointer , Self.Pointer)
	End Method
	
	Rem
		bbdoc: Retrieve the background colour that the unrendered portions of the TViewport are designated to be.
	ENDREM
	Method getBackgroundColour:TColourValue() 
		Return TColourValue.FromPtr(VP_getBackgroundColour(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Sets how the TViewport is cleared for every frame rendered. <br>clear:Int True if you want any buffers to be cleared on every frame. False if you do not.<br>buffers:Int The value that specifies which buffers to clear or not clear.  Your options are FBT_COLOUR , FBT_DEPTH and FBT_STENCIL. To use them together, simply use the XOR operator like so: FBT_COLOR | FBT_DEPTH | FBT_STENCIL.
	ENDREM
	Method setClearEveryFrame(Clear:Int, buffers:Int = FBT_COLOUR | FBT_DEPTH) 
		VP_setClearEveryFrame( clear , buffers , Self.Pointer )
	End Method
	
	Rem
		bbdoc: True or false, tells you if the TViewport is to be cleared every frame.
	ENDREM
	Method getClearEveryFrame:Int() 
		Return VP_getClearEveryFrame( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns buffers to be cleared every frame in the format of XORing all FBT values into a single Int.
	ENDREM
	Method getClearBuffers:Int() 
		Return VP_getClearBuffers( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the TMaterialScheme that you want to use for rendering textures through this TViewport.
	ENDREM
	Method setMaterialScheme(schemeName:String) 
		VP_setMaterialScheme( schemeName.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the material scheme that this TViewport is using to render textures.
	ENDREM
	Method getMaterialScheme:String() 
		Return String.FromCString(VP_getMaterialScheme(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Gets the dimensional data for the TViewport IN PIXELS and stores it to the variables specified in the parameters.
	ENDREM
	Method getActualDimensions(Left:Int Var, top:Int Var, width:Int Var, Height:Int Var) 
		VP_getActualDimensions( Varptr(Left) , Varptr(top) , Varptr(width) , Varptr(height) , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns a flag that specifies whether the TViewport is up to date or not.
	ENDREM
	Method _isUpdated:Int() 
		Return VP__isUpdated( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the flag that tells Ogre if the TViewport is updated to FALSE.
	ENDREM
	Method _clearUpdatedFlag()
		VP__clearUpdatedFlag( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the number of rendered faces in the last update of the TViewport.
	ENDREM
	Method _getNumRenderedFaces:Int() 
		Return VP__getNumRenderedFaces( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the number of rendered batches in the last update of the TViewport.
	ENDREM
	Method _getNumRenderedBatches:Int() 
		Return VP__getNumRenderedBatches( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Tells the renderer whether this TViewport should display TOverlay objects.
	ENDREM
	Method setOverlaysEnabled(enabled:Int) 
		VP_setOverlaysEnabled( enabled , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the parameter the governs wheter TOverlay objects will be displayed in this TViewport.
	ENDREM
	Method getOverlaysEnabled:Int() 
		Return VP_getOverlaysEnabled( Self.Pointer )
	End Method   
	
	Rem
		bbdoc: Specify if skyboxes will be rendered.
	ENDREM
	Method setSkiesEnabled(enabled:Int) 
		VP_setSkiesEnabled( enabled ,Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retreive the flag that specifies whether skyboxes are enabled.
	ENDREM
	Method getSkiesEnabled:Int() 
		Return VP_getSkiesEnabled( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set if shadows will be cast and rendered in this TViewport.
	ENDREM
	Method setShadowsEnabled(enabled:Int) 
		VP_setShadowsEnabled( enabled, Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retrieve the flag the defines whether shadows will be rendered or not.
	ENDREM
	Method getShadowsEnabled:Int() 
		Return VP_getShadowsEnabled( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the visibility mask for this viewport. These correspond to visibility flags on movable objects to determine what gets drawn inside this viewport.
	ENDREM
	Method setVisibilityMask(mask:Int)
		VP_setVisibilityMask(mask, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the visibility mask for this viewport.
	ENDREM
	Method getVisibilityMask:Int()
		Return VP_getVisibilityMask(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Use a custom TRenderQueueInvocation sequence for this TViewport's TRenderTarget. These allow for more custom render ordering and state operations.
	ENDREM
	Method setRenderQueueInvocationSequenceName(sequenceName:String) 
		VP_setRenderQueueInvocationSequenceName( sequenceName.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the name of the TRenderQueueInvocationSequence being used on the TRenderTarget of the TViewport.
	ENDREM
	Method getRenderQueueInvocationSequenceName:String() 
		Local tempStr:String
		tempStr = tempStr.FromCString( VP_getRenderQueueInvocationSequenceName( Self.Pointer )  )
		Return tempStr
	End Method
	
	Rem
		bbdoc: Gets the TRenderQueueInvocationSequence being used on the TRenderTarget of the TViewport.
	ENDREM
	Method _getRenderQueueInvocationSequence:TRenderQueueInvocationSequence() 
		Local Obj:TRenderQueueInvocationSequence = New TRenderQueueInvocationSequence
		Obj.Pointer = VP__getRenderQueueInvocationSequence( Self.Pointer )
		Return Obj
	End Method
	
End Type


Rem
	bbdoc: An abstract interface that describes items that are going to be put into the rendering queue.
ENDREM
Type TRenderable

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Object to hold user data. Note that this userdata will not persist past the life of this variable.
	ENDREM
	Field userData:Object
	
	Rem
		bbdoc: Gets a reference to the TMaterial object that this TRenderable uses.
	ENDREM
	Method getMaterial:TMaterial() 
		Local Obj:TMaterial = New TMaterial
		Obj.Pointer = RENDERABLE_getMaterial( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets the TTechnique object that is being used to render the TMaterial for this TRenderable.
	ENDREM
	Method getTechnique:TTechnique() 
		Local Obj:TTechnique = New TTechnique
		Obj.Pointer = RENDERABLE_getTechnique( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets the TRenderOperation that is required to send this TRenderable to the frame buffer.<BR> op:TRenderOperation The TRenderOperation will be stored in this variable.
	ENDREM
	Method getRenderOperation(op:TRenderOperation) 
		RENDERABLE_getRenderOperation( op.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets any world transform matrices for this TRenderable.<BR>xform:TMatrix4 Stores the world transform matrices.
	ENDREM
	Method getWorldTransforms(xform:TMatrix4) 
		RENDERABLE_getWorldTransforms( xform.Pointer , Self.Pointer )
	End Method	
	
	Rem
		bbdoc: Gets the number of world transforms this TRenderable has applied to it.
	ENDREM
	Method getNumWorldTransforms:Int() 
		Return RENDERABLE_getNumWorldTransforms( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets whether an identity projection will be used or not.
	ENDREM
	Method setUseIdentityProjection(useIdentityProjection:Int) 
		RENDERABLE_setUseIdentityProjection( useIdentityProjection , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retreive whether or not this TRenderable uses an identity projection.
	ENDREM
	Method getUseIdentityProjection:Int() 
		Return RENDERABLE_getUseIdentityProjection( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets whether or not to use an identity view on this TRenderable.
	ENDREM
	Method setUseIdentityView(useIdentityView:Int) 
		RENDERABLE_setUseIdentityView( useIdentityView , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retreive whether or not this TRenderable uses an identity view.
	ENDREM
	Method getUseIdentityView:Int() 
		Return RENDERABLE_getUseIdentityView( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retreives a camera centric view depth of this TRenderable.
	ENDREM
	Method getSquaredViewDepth:Float(camera:TCamera) 
		Return RENDERABLE_getSquaredViewDepth( camera.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns a list of lights that affect this TRenderable, sorting them by nearest.
	ENDREM
	Method getLights:TLightList() 
		Local Obj:TLightList = New TLightList
		Obj.Pointer = RENDERABLE_getLights(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Returns a flag specifying whether this TRenderable casts a shadow or not.
	ENDREM
	Method getCastsShadows:Int() 
		Return RENDERABLE_getCastsShadows( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set custom rendering parameters for this TRenderable that can be used as quanta for affecting different rendering actions.<br> index:Long An index To associate the Value with, somewhat like a Key.<br>value:TVector4 The value of the custom parameter.
	ENDREM
	Method setCustomParameter(index:Int, Value:TVector4) 
		RENDERABLE_setCustomParameter( index , value.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retreive a custom parameter set for this TRenderable object by index.
	ENDREM
	Method getCustomParameter:TVector4(index:Int) 
		Return TVector4.FromPtr(RENDERABLE_getCustomParameter(index, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Use this to update custom Gpu Parameters for this TRenderable.<br>constantEntry:TAutoConstantEntry Entry referring to the parameter being updated.<br>params:TGpuProgramParameters The parameters object that the TAutoConstantEntry parameter object should call to update the paramseters.
	ENDREM
	Method _updateCustomGpuParameter(constantEntry:TAutoConstantEntry, params:TGpuProgramParameters) 
		RENDERABLE__updateCustomGpuParameter( constantEntry.Pointer , params.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets whether or not this camera's detail level can be overridden by the camera settings.
	ENDREM
	Method setPolygonModeOverrideable(overrideable:Int) 
		RENDERABLE_setPolygonModeOverrideable( overrideable , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Return whether or not this TRenderable can have it's detail level overridden by camera settings.
	ENDREM
	Method getPolygonModeOverrideable:Int() 
		Return RENDERABLE_getPolygonModeOverrideable( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Cast this TRenderable to a TSceneNode.
	ENDREM
	Method getSceneNode:TSceneNode()
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = Self.Pointer
		Return Obj
	End Method
	
End Type

Rem
	bbdoc: A class that defines sub-parts of a TEntity. These sub parts include sub-parts of the TMesh as well as the materials linked to.
ENDREM
Type TSubEntity Extends TRenderable

End Type

Rem
	bbdoc: General class for handling nodes in an Ogre scene graph. Please note that you are not meant to create this class directly, but are instead directed to use derived classes such as TSceneNode or TBone.
ENDREM
Type TOgreNode Extends TRenderable
	
	Rem
		bbdoc: Local transform space.
	ENDREM
	Const TS_LOCAL:Int = 0
	Rem
		bbdoc: Parent transform space.
	ENDREM
	Const TS_PARENT:Int = 1
	Rem
		bbdoc: World transform space.
	ENDREM
	Const TS_WORLD:Int = 2
	
	Rem
		bbdoc: Get the name of this node.	
	ENDREM
	Method getName:String() 
		Return String.FromCString(NODE_getName(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get this node's parent as a TOgreNode.  This will return NULL if there is no parent.
	ENDREM
	Method getParent:TOgreNode()
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = NODE_getParent( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Return a TQuaternion that contains this node's current orientation.
	ENDREM
	Method getOrientation:TQuaternion() 
		Return TQuaternion.FromPtr(NODE_getOrientation(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Sets the orientation/rotation of this node via a TQuaternion.
	ENDREM
	Method setOrientationWithQuaternion(ogreQuaternion:TQuaternion) 
		NODE_setOrientationWithQuaternion(ogreQuaternion.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Sets the orientation/rotation of this node via a TQuaternion defined as a series of floats.
	ENDREM
	Method setOrientation(w:Float, x:Float, y:Float, z:Float) 
		NODE_setOrientation(w, x, y, z, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Resets the node to it's local axes position with no orientation in world space.
	ENDREM
	Method resetOrientation() 
		NODE_resetOrientation(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the position of this node relative to it's parent node with a TVector3.
	ENDREM
	Method setPositionWithVector3(pos:TVector3) 
		NODE_setPositionWithVector3( pos.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the position of this node relative to it's parent node with 3 floats.
	ENDREM
	Method setPosition(x:Float, y:Float, z:Float) 
		NODE_setPosition( x , y , z ,Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the position of this node relative to it's parent node as a TVector3.
	ENDREM
	Method getPosition:TVector3() 
		Return TVector3.FromPtr(NODE_getPosition(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Set the scaling of this node with a TVector3.
	ENDREM
	Method setScaleWithVector3(scale:TVector3) 
		NODE_setScaleWithVector3( scale.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the scaling of this node with three axes as floats.
	ENDREM
	Method SetScale(x:Float, y:Float, z:Float) 
		NODE_setScale( x , y , z , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the scaling of this node.
	ENDREM
	Method GetScale:TVector3() 
		Return TVector3.FromPtr(NODE_getScale(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Set whether or not this node inherits orientation/rotation from it's parent.
	ENDREM
	Method setInheritOrientation(inherit:Byte) 
		NODE_setInheritOrientation( inherit , Self.Pointer ) 
	End Method
	
	Rem
		bbdoc: Get whether or not this node inherits orientation/rotation from it's parent.
	ENDREM
	Method getInheritOrientation:Byte() 
		Return NODE_getInheritOrientation(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set whether or not this node inherits scale/sizing from it's parent.
	ENDREM
	Method setInheritScale(inherit:Byte) 
		NODE_setInheritScale(inherit, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not this node inherits scale/sizing from it's parent.
	ENDREM
	Method getInheritScale:Byte() 
		Return NODE_getInheritScale(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Scales this node from it's current scale via the passed TVector3.
	ENDREM
	Method scaleWithVector3(scale:TVector3) 
		NODE_scaleWithVector3(scale.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Scales this node from it's current scale via three floats as axes.
	ENDREM
	Method scale(x:Float, y:Float, z:Float) 
		NODE_scale( x , y , z , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Translate this node along cartesian axes using a TVector3. Note that you may specify what space you wish to translate in. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space.
	ENDREM
	Method translateWithVector3(d:TVector3, relativeTo:Int = TOgreNode.TS_PARENT) 
		NODE_translateWithVector3(d.Pointer, relativeTo, Self.Pointer) 
	End Method
		
	Rem
		bbdoc: Translate this node along cartesian axes with three axes as floats. Note that you may specify what space you wish to translate in. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space.
	ENDREM
	Method translate(x:Float, y:Float, z:Float, relativeTo:Int = TOgreNode.TS_PARENT) 
		NODE_translate(x, y, z, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Translate this node along arbitrary axes defined by a TMatrix3. Once the axes are defined, you may translate/move using the move:TVector3 Variable. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space.
	ENDREM
	Method translateAlongAxesWithVector3(axes:TMatrix3, move:TVector3, relativeTo:Int = TOgreNode.TS_PARENT) 
		NODE_translateAlongAxesWithVector3(axes.Pointer, move.Pointer, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Translate this node along arbitrary axes defined by a TMatrix3. Once the axes are defined, you may translate/move along them using the x,y,z floats provided. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space.
	ENDREM
	Method translateAlongAxes(axes:TMatrix3, x:Float, y:Float, z:Float, relativeTo:Int = TOgreNode.TS_PARENT) 
		NODE_translateAlongAxes(axes.Pointer, x, y, z, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate around the Z-Axis(in degrees) of the specified transform space. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space. 	
	ENDREM
	Method roll(Degrees:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		NODE_roll(Degrees, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate around the Z-Axis(in radians) of the specified transform space. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space. 	
	ENDREM
	Method rollWithRadians(Radians:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		NODE_rollWithRadians(Radians, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate around the X-Axis(in degrees) of the specified transform space. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space. 	
	ENDREM
	Method pitch(Degrees:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		NODE_pitch(Degrees, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate around the X-Axis(in radians) of the specified transform space. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space. 	
	ENDREM
	Method pitchWithRadians(Radians:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		NODE_pitchWithRadians(Radians, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate around the Y-Axis(in degrees) of the specified transform space. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space. 	
	ENDREM
	Method yaw(Degrees:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		NODE_yaw(Degrees, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate around the Y-Axis(in radians) of the specified transform space. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space. 	
	ENDREM
	Method yawWithRadians(Radians:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		NODE_yawWithRadians(Radians, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate this node( in degrees ) around an arbitrary axis.
	ENDREM
	Method rotate(axis:TVector3, Degrees:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		NODE_rotate(axis.Pointer, Degrees, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate this node( in radians ) around an arbitrary axis.
	ENDREM
	Method rotateWithRadians(axis:TVector3, Radians:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		NODE_rotateWithRadians(axis.Pointer, Radians, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate this node around an arbitrary axis using a TQuaternion.
	ENDREM
	Method rotateWithQuaternion(q:TQuaternion, relativeTo:Int = TOgreNode.TS_LOCAL) 
		NODE_rotateWithQuaternion(q.Pointer, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the local space axes for this node as a TMatrix3.
	ENDREM
	Method getLocalAxes:TMatrix3() 
		Local Obj:TMatrix3 = New TMatrix3
		Obj.Pointer = NODE_getLocalAxes(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Creates a child node of this node without a name.
	ENDREM
	Method createChild:TOgreNode(translate:TVector3 = Null, rotate:TQuaternion = Null) 
		'Workaround to add custom type default paramters-
		If translate = Null Then translate = New TVector3; translate.Pointer = TVector3.ZERO().Pointer; translate.managed = True;
		If rotate = Null Then rotate = New TQuaternion; rotate.Pointer = TQuaternion.IDENTITY().Pointer; rotate.managed = True;
		
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = NODE_createChild(translate.Pointer, rotate.Pointer, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Creates a child node of this node with a name.
	ENDREM
	Method createChildWithName:TOgreNode(name:String, translate:TVector3 = Null, rotate:TQuaternion = Null) 
		'Workaround to add custom type default paramters-
		If translate = Null Then translate = New TVector3; translate.Pointer = TVector3.ZERO().Pointer; translate.managed = True;
		If rotate = Null Then rotate = New TQuaternion; rotate.Pointer = TQuaternion.IDENTITY().Pointer; rotate.managed = True;
		
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = NODE_createChildWithName(name.ToCString(), translate.Pointer, rotate.Pointer, Self.Pointer) 
		Return Obj
	End Method

	Rem
		bbdoc: Add a pre-created node to this node as a child.
	ENDREM
	Method addChild(child:TOgreNode) 
		NODE_addChild(child.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Return the number of children attached to this node. Note that this is not the recursive number of children, only the number directly attached to this node.
	ENDREM
	Method numChildren:Int() 
		Return NODE_numChildren( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get a handle to a child node via the child node's index value. Index goes from 0-n. If no node is found under the given index, the TOgreNode.Pointer will be null, and you may check quickly by using the .isNull() function.
	ENDREM
	Method getChild:TOgreNode(index:Int) 
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = NODE_getChild( index , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a handle to a child node via the child node's name. Unlike plain .getChild(...) which retrives the child by index, if .getChildWithName(...) fails your program will throw an exception! 
	ENDREM
	Method getChildWithName:TOgreNode(name:String) 
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = NODE_getChildWithName( name.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets an iterator that can be used for looping through all of the children of this node.
	ENDREM
	Method getChildIterator:TChildNodeIterator() 
		Local Obj:TChildNodeIterator = New TChildNodeIterator
		Obj.Pointer = NODE_getChildIterator(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Detaches a given child from this TOgreNode by index. Note that the child object is not destroyed and is returned to be potentially reattached elsewhere.
	ENDREM
	Method removeChildWithIndex:TOgreNode(index:Int) 
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = NODE_removeChildWithIndex(index, Self.Pointer) 
		Return Obj
	End Method

	Rem
		bbdoc: Detaches a given child from this TOgreNode by object reference. Note that the child object is not destroyed and is returned to be potentially reattached elsewhere.
	ENDREM
	Method removeChild:TOgreNode(child:TOgreNode) 
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = NODE_removeChild(child.Pointer, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Detaches a given child from this TOgreNode by name. Note that the child object is not destroyed and is returned to be potentially reattached elsewhere.
	ENDREM
	Method removeChildWithName:TOgreNode(name:String) 
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = NODE_removeChildWithName(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Removes all children from this node. Note that this does not destroy the child objects and they may be reattached elsewhere.
	ENDREM
	Method removeAllChildren()
		NODE_removeAllChildren( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the orienation of this TOgreNode as defined from all of it's parents. Note that nodes can avoid inherting orientation from parents by using setInheritOrientation( False ).
	ENDREM
	Method _getDerivedOrientation:TQuaternion() 
		Return TQuaternion.FromPtr(NODE__getDerivedOrientation(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the position of this TOgreNode as defined from all of it's parents.
	ENDREM
	Method _getDerivedPosition:TVector3() 
		Return TVector3.FromPtr(NODE__getDerivedPosition(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the scale of this TOgreNode as defiend from all it's parents. Note that nodes can avoid inheriting scale from their parents by using setInheritScale( False ).
	ENDREM
	Method _getDerivedScale:TVector3() 
		Return TVector3.FromPtr(NODE__getDerivedScale(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the full transform matrix for this TOgreNode.
	ENDREM
	Method _getFullTransform:TMatrix4() 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = NODE__getFullTransform(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Internal method that updates this TOgreNode and any children specified, as well as possibly taking parent transforms into account. Specify True for updateChildren if you wish to update the children also. Specify True for parentHasChanged if you wish to take into account changing parent transforms. You should probably never need to use this as it is usually for scene manager implementations.
	ENDREM
	Method _update(updateChildren:Byte, parentHasChanged:Byte) 
		NODE__update(updateChildren, parentHasChanged, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set up a listener for this TOgreNode. Note that only one listener per TOgreNode is allowed. ( Listeners are not implemented yet. )
	ENDREM
	Method setListener(listener:TOgreNodeListener) 
		NODE_setListener(listener.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets the curernt listener attached to this TOgreNode. ( Listeners are not implemented yet. )
	ENDREM
	Method getListener:TOgreNodeListener() 
		Local Obj:TOgreNodeListener = New TOgreNodeListener
		Obj.Pointer = NODE_getListener(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the initial state of this node to it's current transforms. Generally these values are used for keyframe animation. If you do not use this and are not animating the node, then the transforms will equal an identity matrix, which will do nothing.
	ENDREM
	Method setInitialState() 
		NODE_setInitialState(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Reset this node to it's initial state, which will apply all transforms from the initial state to the node.
	ENDREM
	Method resetToInitialState() 
		NODE_resetToInitialState(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Helper function that allows you to determine whether or not this Node is Null.
	ENDREM
	Method isNull:Byte() 
		If Self.Pointer <> Null Then
			Return False
		Else
			Return True
		End If
	End Method
	
	Rem
		bbdoc: Helper function to cast/return this TOgreNode as a TSceneNode.
	ENDREM
	Method ToSceneNode:TSceneNode() 
		Return TSceneNode.FromPtr( Self.Pointer )
	End Method
	
End Type

Rem
	bbdoc: NOT SUPPORTED: Listener for child nodes.
ENDREM
Type TChildNodeIterator
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: NOT SUPPORTED: Listener for nodes.
ENDREM
Type TOgreNodeListener
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Type that represents a node in the scene graph.
ENDREM
Type TSceneNode Extends TOgreNode
	
	Rem
		bbdoc: Attach an instance of a TMovableObject to the scene node. Any class in the TMovableObject heirarchy can be attached to a scene node. This includes TCamera , TParticleSystem , TEntity , TLight, etc.
	ENDREM
	Method attachObject( mo:TMovableObject )
		If(mo.vartype < 5) Then
			SNODE_attachObject(mo.Pointer, Self.Pointer)
			Return
		End If
		
		If mo.vartype = 5 Then
			SNODE_attachParticleSystem(mo.Pointer, Self.Pointer)
		End If
		
		If mo.vartype = 6 Then
			SNODE_attachManualObject(mo.Pointer, Self.Pointer)
		End If
	End Method
	
	Rem
		bbdoc: Detach an instance of a TMovableObject from this scene node by specifying its an instance of the object to be detached.
	ENDREM	
	Method detachObject( mo:TMovableObject  )
		If(mo.vartype < 5) Then
			SNODE_detachObject(mo.Pointer, Self.Pointer)
			Return
		End If
		
		If mo.vartype = 5 Then
			SNODE_detachParticleSystem( mo.Pointer , Self.Pointer )
		End If
		
		If mo.vartype = 6 Then
			SNODE_detachManualObject(mo.Pointer, Self.Pointer)
		End If
	End Method
	
	Rem
		bbdoc: Create a named scene node and specify its offset ( relative to its parent ).
	ENDREM	
	Method createChildSceneNode:TSceneNode( sceneNodeName$ , x:Float , y:Float , z:Float )
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = SNODE_createChildSceneNode( sceneNodeName.tocstring() , x , y , z , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Remove and destroy all children from this scene node.
	ENDREM
	Method removeAndDestroyAllChildren()
		SNODE_removeAndDestroyAllChildren( Pointer )
	End Method

	Rem
		bbdoc: Set whether or not the bounding box is visible for the objects attached to this scene node.
	ENDREM
	Method showBoundingBox( Visible:Int )
		SNODE_showBoundingBox( Visible , Pointer )
	End Method
	
	Rem
		bbdoc: Get this scene node's parent.
	ENDREM
	Method getParentSceneNode:TSceneNode()
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = SNODE_getParentSceneNode( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Set whether or not the objects attached to this scene node are visible.<br/>cascade:Int = True Specify whether or not this setting applies to all childen of this scene node as well.
	ENDREM
	Method setVisible( visible:Int , cascade:Int = True )
		SNODE_setVisible( visible , Self.Pointer , cascade )
	End Method
	
	Rem
		bbdoc: Set whether or not this node should yaw around its own local yaw axis or a custom specified yaw axis.
	ENDREM
	Method setFixedYawAxis( setUsed:Int , x:Float , y:Float , z:Float )
		SNODE_setFixedYawAxis( setUsed , x , y , z , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get whether or not bounding boxes are being shown for the objects attached to this scene node.
	ENDREM
	Method getShowBoundingBox:Int()
		Return SNODE_getShowBoundingBox( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Point this scene node at a target point.<br/>relativeTo:Int - The space that the point in. This would be TS_WORLD , TS_LOCAL or TS_PARENT.<br/>Set the local direction vector. This is the direction that the entity will point from, and is typically negative unit z.
	ENDREM
	Method lookAt( targetPoint:TVector3 , relativeTo:Int , localDirectionVector:TVector3 )
		SNODE_lookAt( targetPoint.Pointer , relativeTo , localDirectionVector.Pointer ,  Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether or not this scene node auto-tracks another scene node with an optional direction vector and offset to specify.
	ENDREM
	Method setAutoTracking(enabled:Byte, target:TSceneNode = Null, localDirectionVector:TVector3 = Null, offset:TVector3 = Null)
	
		'Set our defaults
		If target = Null Then
			target = New TSceneNode
			target.Pointer = Null
		End If
		
		If localDirectionVector = Null Then localDirectionVector = TVector3.NEGATIVE_UNIT_Z()
		If offset = Null Then offset = TVector3.ZERO()
		
		'Set auto tracking
		SNODE_setAutoTracking(enabled, target.Pointer, localDirectionVector.Pointer, offset.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the number of objects attached to this scene node with attachObject.
	ENDREM
	Method numAttachedObjects:Short()
		Return SNODE_numAttachedObjects( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get an attached object by specifying the index. Starts at 0.
	ENDREM
	Method getAttachedObject:TMovableObject( index:Short )
		Local Obj:TMovableObject = New TMovableObject
		Obj.Pointer = SNODE_getAttachedObject( index , Self.Pointer ) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get an object attached to this scene node by specifying its name.
	ENDREM
	Method getAttachedObjectWithName:TMovableObject( name:String )
		Local Obj:TMovableObject = New TMovableObject
		Obj.Pointer = SNODE_getAttachedObjectWithName( name.ToCString() , Self.Pointer ) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Helper function to create a TSceneNode from a reference/pointer.
	ENDREM
	Function FromPtr:TSceneNode(Pointer:Byte Ptr) 
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
End Type


Rem
	bbdoc: Type that represents a particular way that a material can be rendered.
ENDREM
Type TTechnique

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Set the diffuse or reflected colour for every pass in this technique.
	ENDREM
	Method setDiffuse(diffuse:TColourValue) 
		TECHNIQUE_setDiffuse(diffuse.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the kind of blending used in every pass in this technique.
	ENDREM
	Method setSceneBlending(sbt:Int) 
		TECHNIQUE_setSceneBlending(sbt, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set whether or not each pass in this technique renders using depth-buffer checks.
	ENDREM
	Method setDepthCheckEnabled(enabled:Byte) 
		TECHNIQUE_setDepthCheckEnabled(enabled, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the shadow receiver material for this particular technique.
	ENDREM
	Method setShadowReceiverMaterial(name:String)
		TECHNIQUE_setShadowReceiverMaterial( name.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get a pass in this technique by index. (0-n)
	ENDREM
	Method getPassWithIndex:TPass(index:Int)
		Local Obj:TPass = New TPass
		Obj.Pointer = TECHNIQUE_getPassWithIndex(index, Self.Pointer)
		Return Obj
	End Method
End Type

Rem
	bbdoc: Type that specifies a single pass of a technique, which is a certain way to render a given material.
ENDREM
Type TPass

	Rem
		bbdoc: Pointer to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Set what kind of blending this material will do with the rest of the scene. See TSceneBlendType for constants to use.
	ENDREM
	Method setSceneBlending(sbt:Int)
		PASS_setSceneBlending(sbt, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the way this pass will reject and not render pixels.<br/>func:Int - Function to use. Check out the CMPF_ functions in flow_constants for more information.<br/>value:Int - 0-255 value that alpha values will be tested against.<br/>alphaToCoverageEnabled:Byte = False - Enable alpha-to-coverage support.
	ENDREM
	Method setAlphaRejectSettings(func:Int, value:Int, alphaToCoverageEnabled:Byte = False)
		PASS_setAlphaRejectSettings(func, value, alphaToCoverageEnabled, Self.Pointer)
	End Method
	
	
	Rem
		bbdoc: Set the culling mode for this pass based on the direction that vertices are wound. Your choices are: CULL_NONE , CULL_CLOCKWISE , CULL_ANTICLOCKWISE , CULL_BACKFACES , CULL_FRONTFACES.
	ENDREM
	Method setCullingMode(mode:Int)
		PASS_setCullingMode(mode, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the manual culling mode. With manual culling, the geometry is culled by the cpu before being sent to the hardware for culling.
	ENDREM
	Method setManualCullingMode(mode:Int)
		PASS_setManualCullingMode(mode, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Creates a new texture unit state for this pass.<br/>
	ENDREM
	Method createTextureUnitState:TTextureUnitState(textureName:String, texCoordSet:Int = 0)
		Local Obj:TTextureUnitState = New TTextureUnitState'
		Obj.Pointer = PASS_createTextureUnitState(textureName.ToCString() , texCoordSet, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the vertex program to be used with this pass by name.
	ENDREM
	Method setVertexProgram(name:String, resetParams:Byte = True)
		PASS_setVertexProgram(name.ToCString() , resetParams, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get a handle to the instance of the gpu parameters used by the vertex program in this pass.
	ENDREM
	Method getVertexProgramParameters:TGpuProgramParameters()
		Local Obj:TGpuProgramParameters = New TGpuProgramParameters
		Obj.Pointer = PASS_getVertexProgramParameters(Self.Pointer)
		Return Obj
	End Method
	
End Type

Rem
	bbdoc: Texture Unit State interface. Not yet fully implemented.
ENDREM
Type TTextureUnitState
	
	Rem
		bbdoc: Pointer to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: A common interface for defining an object with a reflection-style , self-defining parameter set.
ENDREM
Type TStringInterface
	
	Rem
		bbdoc: Pointer to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: NOT SUPPORTED: Retreives the list of available parameters for this interface.
	ENDREM
	Method getParamDictionary:TParamDictionary() 
		Local Obj:TParamDictionary = New TParamDictionary
		Obj.Pointer = STRINT_getParamDictionary( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED: Gets a list of useable parameters for this object.
	ENDREM
	Method getParameters:TParameterList()
		Local Obj:TParameterList = New TParameterList
		Obj.Pointer = STRINT_getParameters( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: General method for setting parameters for this object. Returns true if setting the parameter was successful.
	ENDREM
	Method setParameter:Int(Name:String, Value:String) 
		Return STRINT_setParameter( name.ToCString() , value.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Generic method for setting multiple parameters for this object.
	ENDREM
	Method setParameterList(paramList:TNameValuePairList) 
		STRINT_setParameterList(paramList.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: General method for retreiving the value of a parameter specified.
	ENDREM
	Method getParameter:String(Name:String) 
		Local tempString:String
		Return tempString.FromCString( STRINT_getParameter( name.ToCString() , Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Method for copying this TStringInterface's parameters to another TStringInterface.<br/>Note: Unfortunately, TStringInterface is a method that is often used with multiple inheritance, which BlitzMax does not support. If you know that your object is derived from StringInterface in Ogre but it is not in Flow3D, you may need to do something like this:  originalObject.copyParametersTo( yourobject.ToTStringInterface() ).
	ENDREM
	Method copyParametersTo(dest:TStringInterface) 
		STRINT_copyParametersTo( dest.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Cleans up so that it's containers are not left with invalid pointers. Required if you wish to reset Ogre.
	ENDREM
	Function cleanupDictionary()
		STRINT_cleanupDictionary() 
	End Function
	
End Type

Rem
	bbdoc: Abstract interface for resources that can be loaded.
ENDREM
Type TResource Extends TStringInterface
	
	Rem
		bbdoc: Load State : Unloaded.
	ENDREM
	Const LOADSTATE_UNLOADED:Int = 0
	
	Rem
		bbdoc: Load State : Is loading.
	ENDREM
	Const LOADSTATE_LOADING:Int = 1
	
	Rem
		bbdoc: Load State : Is Loaded.
	ENDREM
	Const LOADSTATE_LOADED:Int = 2
	
	Rem
		bbdoc: Load State : Is Unloading.
	ENDREM
	Const LOADSTATE_UNLOADING:Int = 3
	
	Rem
		bbdoc: Loads the resource unless it is already loaded.<br> backgroundThread:Int Specifices whether or not to use the background loading thread.
	ENDREM
	Method Load(backgroundThread:Int = False) 
		RESOURCE_load( backgroundThread , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Reloads the resource if the resource is already loaded.
	ENDREM
	Method reload()
		RESOURCE_reload( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns whether or not this resource can be reloaded.
	ENDREM
	Method isReloadable:Int()
		Return RESOURCE_isReloadable( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns whether or not this resource is manually loaded.
	ENDREM
	Method isManuallyLoaded:Int()
		Return RESOURCE_isManuallyLoaded( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Unloads a resource, which can be reloaded again if desired.
	ENDREM
	Method unload()
		RESOURCE_unload( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the size of the resource.
	ENDREM
	Method getSize:Int()
		Return RESOURCE_getSize( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Indicate that the resource has been used.
	ENDREM
	Method touch()
		RESOURCE_touch( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the resource name.
	ENDREM
	Method getName:String()
		Local tempString:String
		Return tempString.FromCString( RESOURCE_getName( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Get the handle of this resource.
	ENDREM
	Method GetHandle:Long()
		Return RESOURCE_getHandle( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Is the current resource loaded?
	ENDREM
	Method isLoaded:Int()
		Return RESOURCE_isLoaded( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Is the TResource currently loading?
	ENDREM
	Method isLoading:Int()
		Return RESOURCE_isLoading( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the current loading state of this resource.<br/>An integer pertaining to a set of constant states.  { TResource.LOADSTATE_LOADED , TResource.LOADSTATE_UNLOADED , TResource.LOADSTATE_LOADING , TResource.LOADSTATE_UNLOADING }
	ENDREM
	Method getLoadingState:Int()
		Return RESOURCE_getLoadingState( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Is this resource going to be loaded from the background loading thread?
	ENDREM
	Method isBackgroundLoaded:Int()
		Return RESOURCE_isBackgroundLoaded( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Specify whether or not this resource is to be loaded through the background loading thread.
	ENDREM
	Method setBackgroundLoaded(bl:Int)
		RESOURCE_setBackgroundLoaded( bl , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Escalates the loading of a background loaded resource.
	ENDREM
	Method escalateLoading()
		RESOURCE_escalateLoading( Self.Pointer )
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED: Add a listener to the resource.
	ENDREM
	Method addListener(lis:TResourceListener)
		RESOURCE_addListener( lis.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Remove a listener from the resource.
	ENDREM
	Method removeListener(lis:TResourceListener)
		RESOURCE_removeListener( lis.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the group name of the TResource.
	ENDREM
	Method getGroup:String()
		Local tempString:String
		Return tempString.FromCString( RESOURCE_getGroup( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Change the TResource group.
	ENDREM
	Method changeGroupOwnership(newGroup:String)
		RESOURCE_changeGroupOwnership(newGroup.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets the creator of this TResource.
	ENDREM
	Method getCreator:TResourceManager()
		Local Obj:TResourceManager = New TResourceManager
		Obj.Pointer = RESOURCE_getCreator( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the origin of this TResource. This could be a resource script, etc.
	ENDREM
	Method GetOrigin:String()
		Local tempString:String
		Return tempString.FromCString( RESOURCE_getOrigin( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Notifies this resource of it's origin.
	ENDREM
	Method _notifyOrigin(origin:String)
		RESOURCE__notifyOrigin( origin.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Helper method that determins whether or not this TResource is null.
	ENDREM
	Method isNull:Byte() 
		If Self.Pointer = Null Then
			Return True
		Else
			Return False
		End If
	End Method
	
End Type

Rem
	bbdoc: The type that reprsents texture resources. This class is not fully implemented yet.
ENDREM
Type TTexture Extends TResource

	Const TEX_TYPE_1D:Int = 1
	Const TEX_TYPE_2D:Int = 2
	Const TEX_TYPE_3D:Int = 3
	Const TEX_TYPE_CUBE_MAP:Int = 4

	Rem
		bbdoc: Return the hardware buffer for a given surface. 
	ENDREM
	Method getBuffer:THardwarePixelBuffer(face:Int = 0, mipmap:Int = 0) 
		Local Obj:THardwarePixelBuffer = New THardwarePixelBuffer
		Obj.Pointer = TEXTURE_getBuffer(face, mipmap, Self.Pointer) 
		Return Obj
	End Method

End Type

Rem
	bbdoc: This is an interface for the extension of the hardware buffer for pixesl buffers.
ENDREM
Type THardwarePixelBuffer Extends THardwareBuffer
	
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get a slice of the render target for this pixel buffer. Returned as a TRenderTexture. 
	ENDREM
	Method getRenderTarget:TRenderTexture(slice:Int = 0) 
		Local Obj:TRenderTexture = New TRenderTexture
		Obj.Pointer = HPB_getRenderTarget(slice, Self.Pointer) 
		Return Obj
	End Method

End Type

Rem
	bbdoc: Interface describing the renderable properties of an object. The material class is not fully implemented yet.
ENDREM
Type TMaterial Extends TResource
	
	Rem
		bbdoc: TMaterial's constructor. You should be using the resource manager's texture creation method instead of this.
	ENDREM
	Function Create:TMaterial(  creator:TResourceManager , name:String , handle:Long , group:String , isManual:Int , loader:TManualResourceLoader )
		Local Obj:TMaterial = New TMaterial
		Obj.Pointer = MATERIAL_create( creator.Pointer , name.ToCString() , handle , group.ToCString() , isManual , loader.Pointer )
		Return Obj
	End Function
	
	Rem
		bbdoc: Get a techinque in this TMaterial by index (0-n).
	ENDREM
	Method getTechniqueWithIndex:TTechnique(index:Int)
		Local Obj:TTechnique = New TTechnique
		Obj.Pointer = MATERIAL_getTechniqueWithIndex(index, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Equals operator for copying values in between materials. materiala.eq(materialb ) is the same as materiala = materialb.
	ENDREM
	Method Eq(a:TMaterial) 
		MATERIAL_eq( a.Pointer, Self.Pointer )
	End Method
	
End Type


Rem
	bbdoc: The interface describing mesh data information. For Ogre, however, a mesh can also encompass several sub-parts, or sub meshes as well as skeleton and texturing data. You do not construct meshes from this class manually, instead you must use TResourceManager or load mesh resources.<br> Note: If you are following Ogre documentation, please note that the attribute sharedVertexData has been replaced with the methods getSharedVertexData and setSharedVertexData.
ENDREM
Type TMesh Extends TResource

	Rem
		bbdoc: Specifies whether this object is managed or not. If the object is managed, it will not be freed with garbage collection.
	ENDREM
	Field managed:Byte
	
	Rem
		bbdoc:  Create a TMesh from a Byte Ptr. If you set isManaged to TRUE, then this TMesh will not be cleaned up by Garbage Collection. Do not specify that this TMesh is managed to true unless you are certain that it will be cleaned up.
	ENDREM
	Function FromPtr:TMesh(Pointer:Byte Ptr, isManaged:Byte = False) 
		Local Obj:TMesh = New TMesh
		Obj.Pointer = Pointer
		Obj.managed = isManaged
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor for unmanaged TMeshes.
	ENDREM
	Method Delete() 
		If Self.managed = False Then MESH_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: This method will allow you to manually create geometry for this TMesh as a sub-part of this TMesh. Use this if you know what you are doing and have set up the geometry properly.
	ENDREM
	Method createSubMesh:TSubMesh() 
		Return TSubMesh.FromPtr(MESH_createSubMesh(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: This method will allow you to manually create geometry for this TMesh as a sub-part of this TMesh and give it a name. Use this if you know what you are doing and have set up the geometry properly.
	ENDREM
	Method createSubMeshWithName:TSubMesh(Name:String) 
		Return TSubMesh.FromPtr(MESH_createSubMeshWithName(Name.ToCString() , Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Give a name to a TSubMesh instance on this TMesh. You specify which TSubMesh by giving it's index.
	ENDREM
	Method nameSubMesh(Name:String, index:Int) 
		MESH_nameSubMesh(name.ToCString() , index, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the index of a TSubMesh if you know it's name. This will generate an ItemIdentityException if a TSubMesh with the given name does not exist.
	ENDREM
	Method _getSubMeshIndex:Int(Name:String) 
		Return MESH__getSubMeshIndex(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the number of TSubMeshes attached to this TMesh.
	ENDREM
	Method getNumSubMeshes:Int() 
		Return MESH_getNumSubMeshes(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get a TSubMesh instance by specifying the index of the TSubMesh. If you specify an index that has no TSubMesh in it you will get an InvalidParametersException out of bounds exception.
	ENDREM
	Method getSubMeshWithIndex:TSubMesh(index:Int) 
		Return TSubMesh.FromPtr(MESH_getSubMeshWithIndex(index, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get a TSubMesh instance by specifying the name of the TSubMesh. If a TSubMesh with the given name is not found a ItemIdentityException exception will occur.
	ENDREM
	Method getSubMesh:TSubMesh(Name:String) 
		Return TSubMesh.FromPtr(MESH_getSubMesh(Name.ToCString() , Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get an iterator which allows you to iterate through all of the TSubMeshes of this TMesh.
	ENDREM
	Method getSubMeshIterator:TSubMeshIterator() 
		Local Obj:TSubMeshIterator = New TSubMeshIterator
		Obj.Pointer = MESH_getSubMeshIterator(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Create a clone of this TMesh with a new new and, optionally, a new resource group name.
	ENDREM
	Method clone:TMesh(newName:String, newGroup:String = "") 
		Return TMesh.FromPtr(MESH_clone(newName.ToCString() , newGroup.ToCString() , Self.Pointer) , 1) 
	End Method
	
	Rem
		bbdoc: Retreive the axis-aligned bounding box for this TMesh.
	ENDREM
	Method getBounds:TAxisAlignedBox() 
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = MESH_getBounds(Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Retreives the radius of the bounding sphere for this TMesh as a float.
	ENDREM
	Method getBoundingSphereRadius:Float() 
		Return MESH_getBoundingSphereRadius(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the AABB manually for this TMesh. When building manual meshes this is required.<br>pad:Int Add padding to seperate the AABB from the TMesh.
	ENDREM
	Method _setBounds(bounds:TAxisAlignedBox, pad:Byte = True) 
		MESH__setBounds(bounds.Pointer, pad, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the bounding radius for this TMesh. When building manual meshes this is required.
	ENDREM
	Method _setBoundingSphereRadius(radius:Float) 
		MESH__setBoundingSphereRadius(radius, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Sets the name of the TSkeleton that this mesh uses for animation.
	ENDREM
	Method setSkeletonName(skelName:String) 
		MESH_setSkeletonName(skelName.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Checks to see whether or not this TMesh has a skeleton set for it.
	ENDREM
	Method hasSkeleton:Int() 
		Return MESH_hasSkeleton(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Checks to see whether or not this TMesh has any vertex animations.
	ENDREM
	Method hasVertexAnimation:Int() 
		Return MESH_hasVertexAnimation(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the TSkeleton attached to this TMesh.
	ENDREM
	Method getSkeleton:TSkeleton() 
		Local Obj:TSkeleton = New TSkeleton
		Obj.Pointer = MESH_getSkeleton(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the name of the TSkeleton currently attached to this TMesh.
	ENDREM
	Method getSkeletonName:String() 
		Return String.FromCString(MESH_getSkeletonName(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: This will initialise an animation set for use with this TMesh. You probably should not use this unless you know what you're doing.
	ENDREM
	Method _initAnimationState(animSet:TAnimationStateSet) 
		MESH__initAnimationState(animSet.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: This will refresh an animation set for use with this TMesh. You probably should not use this unless you know what you're doing.
	ENDREM
	Method _refreshAnimationState(animSet:TAnimationStateSet) 
		MESH__refreshAnimationState(animSet.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Assign a vertex to a bone with weight.  This method is for assigning vertices to bones in the shared vertex data of this TMesh, if you need to use this on TSubMesh data, you will need to call the TSubMesh method.
	ENDREM
	Method addBoneAssignment(vertBoneAssign:TVertexBoneAssignment) 
		MESH_addBoneAssignment(vertBoneAssign.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove all bone assignments from this TMesh.
	ENDREM
	Method clearBoneAssignments() 
		MESH_clearBoneAssignments(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Internal method to notify this TMesh which skeleton it is to use. This does not load the TSkeleton.
	ENDREM
	Method _notifySkeleton(pSkel:TSkeleton) 
		MESH__notifySkeleton(pSkel.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Retreives an iterator that allows you to access all of the bone assignments currently on this TMesh.
	ENDREM
	Method getBoneAssignmentIterator:TBoneAssignmentIterator() 
		Local Obj:TBoneAssignmentIterator = New TBoneAssignmentIterator
		Obj.Pointer = MESH_getBoneAssignmentIterator(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: This method is one of the primary methods for TMesh lod generation. Ogre generates what the mesh should look like at many different distances automatically. It is reccommended that this method is called when you are exporting the mesh, and not at runtime. If you already have seperate meshes generated, then you should use createManualLodLevel instead.<br>lodDistances:TLodDistanceList The list of depths where new LOD meshes should be generated.<br>reductionMethod:Int You may either use TProgressiveMesh.VRQ_CONSTANT or TProgressiveMesh.VRQ_PROPORTIONAL. <br>reductionValue:Float The proportional or fixed number of vertices to collapse. Depends on the reductionMethod.
	ENDREM
	Method generateLodLevels(lodDistances:TLodDistanceList, reductionMethod:Int, reductionValue:Float) 
		MESH_generateLodLevels(lodDistances.Pointer, reductionMethod, reductionValue, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the number of lod levels for this TMesh.
	ENDREM.
	Method getNumLodLevels:Int() 
		Return MESH_getNumLodLevels(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: This is not functional yet.
	ENDREM
	Method getLodLevel:TMeshLodUsage(index:Int) 
		Return New TMeshLodUsage
	End Method
	
	Rem
		bbdoc: Create a manual Lod level for your TMesh with another TMesh. This is the method you will want to use if you have a very complicated mesh, as you can scale down the number of bones, etc. along with the number of triangles. This gives you absolute control over the LOD degradation, and is the alternative to generateLodLevels. Note: You must do this before attaching the mesh to a TEntity. <br>fromDepth:Float The depth from the active TCamera at which this LOD will be effective. This is in Ogre units.<br>meshName:String The name of the TMesh you wish to use for this LOD level.
	ENDREM
	Method createManualLodLevel(fromDepth:Float, meshName:String) 
		MESH_createManualLodLevel(fromDepth, meshName.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Changes the manual LOD mesh at a given index that already exists. As the stack of LODs shrinks or grows, you may need to use getLodIndex to get accurate index info.<br>index:Int The index of the LOD you wish to update.<br>meshName:String The name of the mesh resource you wish to use as a manual LOD for this index.
	ENDREM
	Method updateManualLodLevel(index:Int, meshName:String) 
		MESH_updateManualLodLevel(index, meshName, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the index of the LOD at the given depth from the camera.<br>depth:Float The depth from the camera that you wish to retreive the LOD index for.
	ENDREM
	Method getLodIndex:Int(depth:Float) 
		Return MESH_getLodIndex(depth, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the index of the LOD at the given squared depth from the camera. Ogre handles the depth internally as a square for performance reasons.
	ENDREM
	Method getLodIndexSquaredDepth:Int(squaredDepth:Float) 
		Return MESH_getLodIndexSquaredDepth(squaredDepth, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Check to see if a TMesh is using manual LOD settings or not.
	ENDREM
	Method isLodManual:Int() 
		Return MESH_isLodManual(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Internal method for loading LOD parameters. You probably shouldn't use this.<br>numLevels:Int The number of LOD levels to specify for this TMesh.
	ENDREM
	Method _setLodInfo(numLevels:Int, isManual:Byte) 
		MESH__setLodInfo(numLevels, isManual, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: An internal method for setting LOD usage. You should probably not be using this.<br>level:Int The LOD level to set the usage for.<br>usage:TMeshLodUsage The LOD usage for this level as set by the TMeshLodUsage structure.
	ENDREM 
	Method _setLodUsage(level:Int, usage:TMeshLodUsage) 
		MESH__setLodUsage(level, usage.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Internal method to setup the facelist of a LOD submesh from a given Index. You should probably not be using this. <br>subIdx:Int The index of the Submesh you wish to set the face list for.<br>The Lod level to get set the face list for.<br>facedata:TIndexData The face data to set for this LOD Submesh.
	ENDREM
	Method _setSubMeshLodFaceList(subIdx:Int, level:Int, facedata:TIndexData) 
		MESH__setSubMeshLodFaceList(subIdx, level, facedata.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Removes all LOD data from this TMesh.
	ENDREM
	Method removeLodLevels() 
		MESH_removeLodLevels(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Sets access policies, etc for the vertex buffers for loading this TMesh.<br>usage:Int Different constants describing buffer usage are listed below: <br> THardwareBuffer.HBU_STATIC - A buffer that is not modified very often after creation. <br> THardwareBuffer.HBU_DYNAMIC - A buffer that will be updated often. <br> THardwareBuffer.HBU_WRITE_ONLY A buffer that will be written to, but the application will never read the data itself. <br> THardwareBuffer.HBU_DISCARDABLE Type of buffer that is often given a new memory address, and it does not matter if the contents are destroyed and have to be rewritten again. <br> THardwareBuffer.HBU_STATIC_WRITE_ONLY - A combination of HBU_STATIC and HBU_WRITE_ONLY <br> THardwareBuffer.HBU_DYNAMIC_WRITE_ONLY - A commbination of HBU_DYNAMIC and HBU_WRITE_ONLY. <br> THardwareBuffer.HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE - A combination of HBU_DYNAMIC , HBU_WRITE_ONLY and HBU_DISCARDABLE. The default is HBU_STATIC_WRITE_ONLY.<br>shadowBuffer:Int Set the vertex buffer so that it allocates space for the shadow buffer.
	ENDREM
	Method setVertexBufferPolicy(usage:Int, shadowBuffer:Byte = False) 
		MESH_setVertexBufferPolicy(Usage, shadowBuffer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Sets access policies, etc for the index buffers for loading this TMesh.<br>usage:Int Different constants describing buffer usage are listed below: <br> THardwareBuffer.HBU_STATIC - A buffer that is not modified very often after creation. <br> THardwareBuffer.HBU_DYNAMIC - A buffer that will be updated often. <br> THardwareBuffer.HBU_WRITE_ONLY A buffer that will be written to, but the application will never read the data itself. <br> THardwareBuffer.HBU_DISCARDABLE Type of buffer that is often given a new memory address, and it does not matter if the contents are destroyed and have to be rewritten again. <br> THardwareBuffer.HBU_STATIC_WRITE_ONLY - A combination of HBU_STATIC and HBU_WRITE_ONLY <br> THardwareBuffer.HBU_DYNAMIC_WRITE_ONLY - A commbination of HBU_DYNAMIC and HBU_WRITE_ONLY. <br> THardwareBuffer.HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE - A combination of HBU_DYNAMIC , HBU_WRITE_ONLY and HBU_DISCARDABLE. The default is HBU_STATIC_WRITE_ONLY.<br>shadowBuffer:Int Set the index buffer so that it allocates space for the shadow buffer.
	ENDREM
	Method setIndexBufferPolicy(usage:Int, shadowBuffer:Byte = False) 
		MESH_setIndexBufferPolicy(Usage, shadowBuffer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the usage settings for this TMesh's vertex buffer.<br>The vertex buffer usage as an integer. THardwareBuffer.HBU_STATIC = 1 <br> THardwareBuffer.HBU_DYNAMIC = 2 <br> THardwareBuffer.HBU_WRITE_ONLY = 4 <br> THardwareBuffer.HBU_DISCARDABLE = 8 <br> THardwareBuffer.HBU_STATIC_WRITE_ONLY = 5 <br> THardwaveBuffer.HBU_DYNAMIC_WRITE_ONLY = 6 <br> THardwareBuffer.HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE = 14
	ENDREM
	Method getVertexBufferUsage:Int() 
		Return MESH_getVertexBufferUsage(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the usage settings for this TMesh's index buffer.<br>The index buffer usage as an integer. THardwareBuffer.HBU_STATIC = 1 <br> THardwareBuffer.HBU_DYNAMIC = 2 <br> THardwareBuffer.HBU_WRITE_ONLY = 4 <br> THardwareBuffer.HBU_DISCARDABLE = 8 <br> THardwareBuffer.HBU_STATIC_WRITE_ONLY = 5 <br> THardwaveBuffer.HBU_DYNAMIC_WRITE_ONLY = 6 <br> THardwareBuffer.HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE = 14
	ENDREM
	Method getIndexBufferUsage:Int() 
		Return MESH_getIndexBufferUsage(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not this TMesh's vertex buffer has shadow information.
	ENDREM
	Method isVertexBufferShadowed:Int() 
		Return MESH_isVertexBufferShadowed(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not this TMesh's index buffer has shadow information.
	ENDREM
	Method isIndexBufferShadowed:Int() 
		Return MESH_isIndexBufferShadowed(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Decides on which bones in a bone assignment list will be used for this TMesh. As ogre only supports four different bones influencing a vertex at once, only the four highest weights will persist after rationalisation.<br>vertexCount:Int The vertex count of the vertexes in the TVertexBoneAssignmenList.<br>assigment:TVertexBoneAssigmentList The list to modify in accordance with this TMesh.<br>The maximum number of bone assignments found per vertex.
	ENDREM
	Method _rationaliseBoneAssignments:Int(vertexCount:Int, assignments:TVertexBoneAssignmentList) 
		Return MESH__rationaliseBoneAssignments(vertexCount, assignments.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Internal method to compile all bone assignments into the geometry buffer. Bone assignments are compiled automatically by Ogre for real time use using this method.
	ENDREM
	Method _compileBoneAssignments() 
		MESH__compileBoneAssignments(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Update bone assignments into the geometry buffer after they have already been compiled and placed there once.
	ENDREM
	Method _updateCompiledBoneAssignments() 
		MESH__updateCompiledBoneAssignments(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Build tangent vectors for this mesh into a 3d texture coordinate buffer. Every submesh of this must MUST have vertex normals and 2d texture coordinates in order for this to work.<br>targetSemantic:Int The vertex semantic to store tangents into. On newer cards, the tangent semantic is available, but on older cards it is not. See TVertexElementSemantic for details on usage.<br>sourceTexCoordSet:Int  The texture index that should be used in tangent vector generation.<br>index:Int The element index that should be used to store the 3d texture coordinates.
	ENDREM
	Method buildTangentVectors(targetSemantic:Int = TVertexElementSemantic.VES_TANGENT, sourceTexCoordSet:Int = 0, index:Int = 0, splitMirrored:Byte = False, splitRotated:Byte = False, storeParityInW:Byte = False)
		MESH_buildTangentVectors(targetSemantic, sourceTexCoordSet, index, splitMirrored , splitRotated , storeParityInW , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: This method will help you find good parameters for the buildTangentVectors method. You specify the semantic and it gives you the source and destination texture coordinate sets. The method will populate the variables given as the last two parameters. <br>targetSemantic:Int The target vertex semantic from TVertexElementSemantic structure that you want the suggested settings for.<br>outSourceCoordSet:Int Here you place a variable to be populated by the suggestTangentVectorBuildParams function. This will suggest the output coordinate set.<br>outIndex:Int Here you will place a variable to be populated. This will be the element index that is suggested to use for storage.<br>Returns True if there are already 3d mesh coordinates with this mesh.
	ENDREM
	Method suggestTangentVectorBuildParams:Int(targetSemantic:Int, outSourceCoordSet:Int, outIndex:Int) 
		MESH_suggestTangentVectorBuildParams(targetSemantic, Varptr(outSourceCoordSet), Varptr(outIndex), Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Builds an edge list for this TMesh. This is used for shadow casting.
	ENDREM
	Method buildEdgeList() 
		MESH_buildEdgeList(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Destroys this TMesh's build edge lists.
	ENDREM
	Method freeEdgeList() 
		MESH_freeEdgeList(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Prepare this TMesh for shadow volume data. This method will put this TMesh in a standalone vertex buffer and it's vertex data will double in size to store an extruded version of the TMesh.
	ENDREM
	Method prepareForShadowVolume() 
		MESH_prepareForShadowVolume(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Retreive the edge list data for this TMesh. If there is no edge data it will be built, so ensure that this TMesh is ready for shadow volumes by using prepareForShadowVolume().<br>lodIndex:Int The LOD index of the edge list you wish to retreive. 0 is the highest, and the default.
	ENDREM
	Method getEdgeList:TEdgeData(lodIndex:Int = 0) 
		Local Obj:TEdgeData = New TEdgeData
		Obj.Pointer = MESH_getEdgeList(lodIndex, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Specifies whether or not this TMesh has been prepared for the use of shadow volumes.
	ENDREM
	Method isPreparedForShadowVolumes:Int() 
		Return MESH_isPreparedForShadowVolumes(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Specifies whether or not this TMesh has edge list data.
	ENDREM
	Method isEdgeListBuilt:Int() 
		Return MESH_isEdgeListBuilt(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Method to get a value-key pair/hash map of the name assignments of this TMesh's TSubmeshes. 
	ENDREM
	Method getSubMeshNameMap:TSubMeshNameMap() 
		Local Obj:TSubMeshNameMap = New TSubMeshNameMap
		Obj.Pointer = MESH_getSubMeshNameMap(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: This method allows you to specify whether or not you want this TMesh to build edge lists when asked for them or not. You can use this method to avoid ever building edgelists for things you do not need them for, or to ensure that a given TMesh will always have an edge list.
	ENDREM
	Method setAutoBuildEdgeLists(autobuild:Byte) 
		MESH_setAutoBuildEdgeLists(autobuild, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not edge lists will be built for this TMesh when they are requested from it.
	ENDREM
	Method getAutoBuildEdgeLists:Int() 
		Return MESH_getAutoBuildEdgeLists(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets the type of vertex animation supported by the shared vertex data of this TMesh. See TVertexAnimationType for an explanation of the types.
	ENDREM
	Method getSharedVertexDataAnimationType:Int() 
		Return MESH_getSharedVertexDataAnimationType(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Create a new animation for animating this TMesh via vertex animation.<br>name:String The name of the animation.<br>length:Float The length of the animation.
	ENDREM
	Method createAnimation:TAnimation(Name:String, length:Float) 
		Local Obj:TAnimation = New TAnimation
		Obj.Pointer = MESH_createAnimation(name.ToCString() , length, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a TAnimation attached to this TMesh by name.
	ENDREM
	Method getAnimation:TAnimation(Name:String) 
		Local Obj:TAnimation = New TAnimation
		Obj.Pointer = MESH_getAnimation(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Internal method that accesses this TAnimation internally by name.
	ENDREM
	Method _getAnimationImpl:TAnimation(Name:String) 
		Local Obj:TAnimation = New TAnimation
		Obj.Pointer = MESH__getAnimationImpl(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets whether or not this TMesh contains the specified vertex animation.
	ENDREM
	Method hasAnimation:Int(Name:String) 
		Return MESH_hasAnimation(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Removes a specified TAnimation from this TMesh.
	ENDREM
	Method removeAnimation(Name:String) 
		MESH_removeAnimation(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets the number of vertex morph animations in this TMesh.
	ENDREM
	Method getNumAnimations:Int() 
		Return MESH_getNumAnimations(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get a TAnimation attached to this TMesh by index.<br>index:Int The index of the morph animation on this mesh.
	ENDREM
	Method getAnimationWithIndex:TAnimation(index:Int) 
		Local Obj:TAnimation = New TAnimation
		Obj.Pointer = MESH_getAnimationWithIndex(index, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Remove all vertex morph animations from this TMesh.
	ENDREM
	Method removeAllAnimations() 
		MESH_removeAllAnimations(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get a handle to the TVertexData of this TMesh by track handle.<br>handle:Int With the track handle you can use 0 to specify shared vertex data, or index + 1 to specify TSubmesh vertex data.
	ENDREM
	Method getVertexDataByTrackHandle:TVertexData(Handle:Int) 
		Return TVertexData.FromPtr(MESH_getVertexDataByTrackHandle(Handle, Self.Pointer) , 1) 
	End Method
	
	Rem
		bbdoc: Goes through all the TSubMeshes of this TMesh and requests them to use the texutre aliases with the material they are using. This is usually called for you.
	ENDREM
	Method updateMaterialForAllSubMeshes() 
		MESH_updateMaterialForAllSubmeshes(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Internal method to scan the different types of vertex animation and figure out their types. Vertex animations can not be of 2 different types.
	ENDREM
	Method _determineAnimationTypes() 
		MESH__determineAnimationTypes(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Internal method to check to see whether or not animation types are out of date.
	ENDREM
	Method _getAnimationTypesDirty:Int() 
		Return MESH__getAnimationTypesDirty(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Create a TPose for this TMesh or one of it's TSubMeshes.<br>target:Int The target for this TPose. Use 0 for the shared vertex buffer, or index + 1 to access TSubMesh data.<br>name:String The optional name for this TPose.
	ENDREM
	Method createPose:TPose(target:Int, Name:String = "") 
		Local Obj:TPose = New TPose
		Obj.Pointer = MESH_createPose(target, name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the number of TPoses used for this TMesh.
	ENDREM
	Method getPoseCount:Int() 
		Return MESH_getPoseCount(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the specified TPose by Index.
	ENDREM
	Method getPoseWithIndex:TPose(index:Int) 
		Local Obj:TPose = New TPose
		Obj.Pointer = MESH_getPoseWithIndex(index, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the specified TPose by name.
	ENDREM
	Method getPose:TPose(Name:String) 
		Local Obj:TPose = New TPose
		Obj.Pointer = MESH_getPose(name.ToCString(), Self.Pointer) 
		Return Obj	
	End Method
	
	Rem
		bbdoc: Remove the specified TPose by index.
	ENDREM
	Method removePoseWithIndex(index:Int) 
		MESH_removePoseWithIndex(index, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove the specified TPose by name.
	ENDREM
	Method removePose(Name:String) 
		MESH_removePose(name.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove all TPoses attached to this TMesh.
	ENDREM
	Method removeAllPoses() 
		MESH_removeAllPoses(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get a TPoseIterator which will allow you to iterate through all of the TPoses currently on this TMesh.
	ENDREM
	Method getPoseIterator:TPoseIterator() 
		Local Obj:TPoseIterator = New TPoseIterator
		Obj.Pointer = MESH_getPoseIterator(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a list of TPoses associated with this TMesh.
	ENDREM
	Method getPoseList:TPoseList() 
		Local Obj:TPoseList = New TPoseList
		Obj.Pointer = MESH_getPoseList(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Sets sharedVertexData to a given TVertexData, allowing you to initialize sharedVertexData. Instead of using TMesh attribute sharedVertexData = , you should use this method to initialize sharedVertexData.
	ENDREM
	Method setSharedVertexData(v:TVertexData) 
		v.managed = True
		MESH_setSharedVertexData(v.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets sharedVertexData for this TMesh, allowing you to access shared vertex data. This is put in place of using sharedVertexData directly as an attribute of TMesh.
	ENDREM
	Method getSharedVertexData:TVertexData() 
		Return TVertexData.FromPtr(MESH_getSharedVertexData(Self.Pointer) , 1) 
	End Method
	
	Rem
		bbdoc: Get the shared index map which will allow you to translate from blend index to bone index. This map may be shared amongst submeshes.
	ENDREM
	Method getSharedBlendIndexToBoneIndexMap:TIndexMap() 
		Local Obj:TIndexMap = New TIndexMap
		Obj.Pointer = MESH_getSharedBlendIndexToBoneIndexMap(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc:  Set the shared index map that defines translations from the blend index to the bone index. This map may be shared amongst submeshes.
	ENDREM
	Method setSharedBlendIndexToBoneIndexMap(map:TIndexMap) 
		MESH_setSharedBlendIndexToBoneIndexMap(map.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Method to retreive vertex and index data from this mesh.
	ENDREM
	
	Rem
	void MESH_getVertexInformation(
                        size_t &vertex_count,
                        Ogre::Vector3* &vertices,
                        size_t &index_count,
                        unsigned long* &indices,
                        const Ogre::Vector3 &position,
                        const Ogre::Quaternion &orient,
                        const Ogre::Vector3 &scale,
						const Ogre::Mesh* const mesh)
	ENDREM
	Method getVertexInformation(vertex_count:Int Var, verticeList:TVertexList, index_count:Int Var, indexList:TIndexList)

	
		MESH_getVertexInformation(Varptr(vertex_count), verticeList.Pointer, Varptr(index_count), indexList.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Gets the triangle count of this TMesh
	ENDREM
	Method getTriangleCount:Int()
		Return MESH_getTriangleCount(Self.Pointer)
	End Method
		
	Rem
		bbdoc: Performs software vertex morph calculations by doing linear interpolation of two buffers and storing the result in a third buffer.<br>t:Float Parametric distance between beginning and end buffer positions.<br>b1: Buffer containing starting positions.<br>b2: Buffer containing ending positions.<br>targetVertexData:TVertexData Where the result of the vertex morph is stored.
	ENDREM
	Function softwareVertexMorph(t:Float, b1:THardwareVertexBuffer, b2:THardwareVertexBuffer, targetVertexData:TVertexData) 
		MESH_softwareVertexMorph(t, b1.Pointer, b2.Pointer, targetVertexData.Pointer) 
	End Function
	
	Rem
		bbdoc: Performs software vertex pose blend calculations.<br>weight:Float Parametric weight for offsets.<br>vertexOffsetMap:TVertexOffsetMap Vertex index offset map.<br>targetVertexData:TvertexData Destination for the vertex pose blended operation.
	ENDREM
	Function softwareVertexPoseBlend(weight:Float, vertexOffsetMap:TVertexOffsetMap, targetVertexData:TVertexData) 
		MESH_softwareVertexPoseBlend(weight, vertexOffsetMap.Pointer, targetVertexData.Pointer) 
	End Function
	

	
	
End Type

Rem
	bbdoc: A structure of auto updated values you can use in your GPU programs.
ENDREM
Type TAutoConstantType
	Const ACT_WORLD_MATRIX:Int = 0
	Const ACT_INVERSE_WORLD_MATRIX:Int = 1
	Const ACT_TRANSPOSE_WORLD_MATRIX:Int = 2
	Const ACT_INVERSE_TRANSPOSE_WORLD_MATRIX:Int = 3
	Const ACT_WORLD_MATRIX_ARRAY_3x4:Int = 4
	Const ACT_WORLD_MATRIX_ARRAY:Int = 5
	Const ACT_VIEW_MATRIX:Int = 6
	Const ACT_INVERSE_VIEW_MATRIX:Int = 7
	Const ACT_TRANSPOSE_VIEW_MATRIX:Int = 8
	Const ACT_INVERSE_TRANSPOSE_VIEW_MATRIX:Int = 9
	Const ACT_PROJECTION_MATRIX:Int = 10
	Const ACT_INVERSE_PROJECTION_MATRIX:Int = 11
	Const ACT_TRANSPOSE_PROJECTION_MATRIX:Int = 12
	Const ACT_INVERSE_TRANSPOSE_PROJECTION_MATRIX:Int = 13
	Const ACT_VIEWPROJ_MATRIX:Int = 14
	Const ACT_INVERSE_VIEWPROJ_MATRIX:Int = 15
	Const ACT_TRANSPOSE_VIEWPROJ_MATRIX:Int = 16
	Const ACT_INVERSE_TRANSPOSE_VIEWPROJ_MATRIX:Int = 17
	Const ACT_WORLDVIEW_MATRIX:Int = 18
	Const ACT_INVERSE_WORLDVIEW_MATRIX:Int = 19
	Const ACT_TRANSPOSE_WORLDVIEW_MATRIX:Int = 20
	Const ACT_INVERSE_TRANSPOSE_WORLDVIEW_MATRIX:Int = 21
	Const ACT_WORLDVIEWPROJ_MATRIX:Int = 22
	Const ACT_INVERSE_WORLDVIEWPROJ_MATRIX:Int = 23
	Const ACT_TRANSPOSE_WORLDVIEWPROJ_MATRIX:Int = 24
	Const ACT_INVERSE_TRANSPOSE_WORLDVIEWPROJ_MATRIX:Int = 25
	Const ACT_RENDER_TARGET_FLIPPING:Int = 26
	Const ACT_FOG_COLOUR:Int = 27
	Const ACT_FOG_PARAMS:Int = 28
	Const ACT_SURFACE_AMBIENT_COLOUR:Int = 29
	Const ACT_SURFACE_DIFFUSE_COLOUR:Int = 30
	Const ACT_SURFACE_SPECULAR_COLOUR:Int = 31
	Const ACT_SURFACE_EMISSIVE_COLOUR:Int = 32
	Const ACT_SURFACE_SHININESS:Int = 33
	Const ACT_LIGHT_COUNT:Int = 34
	Const ACT_AMBIENT_LIGHT_COLOUR:Int = 35
	Const ACT_LIGHT_DIFFUSE_COLOUR:Int = 36
	Const ACT_LIGHT_SPECULAR_COLOUR:Int = 37
	Const ACT_LIGHT_ATTENUATION:Int = 38
	Const ACT_SPOTLIGHT_PARAMS:Int = 39
	Const ACT_LIGHT_POSITION:Int = 40
	Const ACT_LIGHT_POSITION_OBJECT_SPACE:Int = 41
	Const ACT_LIGHT_POSITION_VIEW_SPACE:Int = 42
	Const ACT_LIGHT_DIRECTION:Int = 43
	Const ACT_LIGHT_DIRECTION_OBJECT_SPACE:Int = 44
	Const ACT_LIGHT_DIRECTION_VIEW_SPACE:Int = 45
	Const ACT_LIGHT_DISTANCE_OBJECT_SPACE:Int = 46
	Const ACT_LIGHT_POWER_SCALE:Int = 47
	Const ACT_LIGHT_DIFFUSE_COLOUR_POWER_SCALED:Int = 48
	Const ACT_LIGHT_SPECULAR_COLOUR_POWER_SCALED:Int = 49
	Const ACT_LIGHT_DIFFUSE_COLOUR_ARRAY:Int = 50
	Const ACT_LIGHT_SPECULAR_COLOUR_ARRAY:Int = 51
	Const ACT_LIGHT_DIFFUSE_COLOUR_POWER_SCALED_ARRAY:Int = 52
	Const ACT_LIGHT_SPECULAR_COLOUR_POWER_SCALED_ARRAY:Int = 53
	Const ACT_LIGHT_ATTENUATION_ARRAY:Int = 54
	Const ACT_LIGHT_POSITION_ARRAY:Int = 55
	Const ACT_LIGHT_POSITION_OBJECT_SPACE_ARRAY:Int = 56
	Const ACT_LIGHT_POSITION_VIEW_SPACE_ARRAY:Int = 57
	Const ACT_LIGHT_DIRECTION_ARRAY:Int = 58
	Const ACT_LIGHT_DIRECTION_OBJECT_SPACE_ARRAY:Int = 59
	Const ACT_LIGHT_DIRECTION_VIEW_SPACE_ARRAY:Int = 60
	Const ACT_LIGHT_DISTANCE_OBJECT_SPACE_ARRAY:Int = 61
	Const ACT_LIGHT_POWER_SCALE_ARRAY:Int = 62
	Const ACT_SPOTLIGHT_PARAMS_ARRAY:Int = 63
	Const ACT_DERIVED_AMBIENT_LIGHT_COLOUR:Int = 64
	Const ACT_DERIVED_SCENE_COLOUR:Int = 65
	Const ACT_DERIVED_LIGHT_DIFFUSE_COLOUR:Int = 66
	Const ACT_DERIVED_LIGHT_SPECULAR_COLOUR:Int = 67
	Const ACT_DERIVED_LIGHT_DIFFUSE_COLOUR_ARRAY:Int = 68
	Const ACT_DERIVED_LIGHT_SPECULAR_COLOUR_ARRAY:Int = 69
	Const ACT_LIGHT_NUMBER:Int = 70
	Const ACT_LIGHT_CASTS_SHADOWS:Int = 71
	Const ACT_SHADOW_EXTRUSION_DISTANCE:Int = 72
	Const ACT_CAMERA_POSITION:Int = 73
	Const ACT_CAMERA_POSITION_OBJECT_SPACE:Int = 74
	Const ACT_TEXTURE_VIEWPROJ_MATRIX:Int = 75
	Const ACT_TEXTURE_VIEWPROJ_MATRIX_ARRAY:Int = 76
	Const ACT_TEXTURE_WORLDVIEWPROJ_MATRIX:Int = 77
	Const ACT_TEXTURE_WORLDVIEWPROJ_MATRIX_ARRAY:Int = 78
	Const ACT_SPOTLIGHT_VIEWPROJ_MATRIX:Int = 79
	Const ACT_SPOTLIGHT_WORLDVIEWPROJ_MATRIX:Int = 80
	Const ACT_CUSTOM:Int = 81
	Const ACT_TIME:Int = 82
	Const ACT_TIME_0_X:Int = 83
	Const ACT_COSTIME_0_X:Int = 84
	Const ACT_SINTIME_0_X:Int = 85
	Const ACT_TANTIME_0_X:Int = 86
	Const ACT_TIME_0_X_PACKED:Int = 87
	Const ACT_TIME_0_1:Int = 88
	Const ACT_COSTIME_0_1:Int = 89
	Const ACT_SINTIME_0_1:Int = 90
	Const ACT_TANTIME_0_1:Int = 91
	Const ACT_TIME_0_1_PACKED:Int = 92
	Const ACT_TIME_0_2PI:Int = 93
	Const ACT_COSTIME_0_2PI:Int = 94
	Const ACT_SINTIME_0_2PI:Int = 95
	Const ACT_TANTIME_0_2PI:Int = 96
	Const ACT_TIME_0_2PI_PACKED:Int = 97
	Const ACT_FRAME_TIME:Int = 98
	Const ACT_FPS:Int = 99
	Const ACT_VIEWPORT_WIDTH:Int = 100
	Const ACT_VIEWPORT_HEIGHT:Int = 101
	Const ACT_INVERSE_VIEWPORT_WIDTH:Int = 102
	Const ACT_INVERSE_VIEWPORT_HEIGHT:Int = 103
	Const ACT_VIEWPORT_SIZE:Int = 104
	Const ACT_VIEW_DIRECTION:Int = 105
	Const ACT_VIEW_SIDE_VECTOR:Int = 106
	Const ACT_VIEW_UP_VECTOR:Int = 107
	Const ACT_FOV:Int = 108
	Const ACT_NEAR_CLIP_DISTANCE:Int = 109
	Const ACT_FAR_CLIP_DISTANCE:Int = 110
	Const ACT_PASS_NUMBER:Int = 111
	Const ACT_PASS_ITERATION_NUMBER:Int = 112
	Const ACT_ANIMATION_PARAMETRIC:Int = 113
	Const ACT_TEXEL_OFFSETS:Int = 114
	Const ACT_SCENE_DEPTH_RANGE:Int = 115
	Const ACT_SHADOW_SCENE_DEPTH_RANGE:Int = 116
	Const ACT_SHADOW_COLOUR:Int = 117
	Const ACT_TEXTURE_SIZE:Int = 118
	Const ACT_INVERSE_TEXTURE_SIZE:Int = 119
	Const ACT_PACKED_TEXTURE_SIZE:Int = 120
	Const ACT_TEXTURE_MATRIX:Int = 121
	Const ACT_LOD_CAMERA_POSITION:Int = 122
	Const ACT_LOD_CAMERA_POSITION_OBJECT_SPACE:Int = 123	
End Type

Rem
	bbdoc: Type that defines a part of a mesh. This piece may use different materials, vertex formats, etc.
ENDREM
Type TSubMesh

	Rem
		bbdoc: A pointer to the object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc:  Create a TSubMesh from a Byte Ptr. If you set isManaged to TRUE, then this TSubMesh will not be cleaned up by Garbage Collection. Do not specify that this TSubMesh is managed to true unless you are certain that it will be cleaned up.
	ENDREM
	Function FromPtr:TSubMesh(Pointer:Byte Ptr) 
		Local Obj:TSubMesh = New TSubMesh
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
	Rem
		bbdoc: Add a vertex bone assignment to this submesh using a TVertexBoneAssignment type.
	ENDREM
	Method addBoneAssignment(vertBoneAssign:TVertexBoneAssignment)
		SUBMESH_addBoneAssignment(vertBoneAssign.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Clear all bone assignments from this submesh.
	ENDREM
	Method clearBoneAssignments()
		SUBMESH_clearBoneAssignments(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set whether or not this TSubMesh uses shared vertex data contained in it's containing mesh. This method replaces the TSubMesh attribute "useSharedVertices".
	ENDREM
	Method setUseSharedVertices(useSharedVertices:Byte) 
		SUBMESH_setUseSharedVertices(useSharedVertices, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not this TSubMesh uses shared vertex data contained in it's containing mesh. This method replaces the TSubMesh attribute "useSharedVertcies".
	ENDREM
	Method getUseSharedVertices:Byte() 
		Return SUBMESH_getUseSharedVertices(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the render operation type used to render this TSubMesh.<br>Options: TRenderOperation.OT_POINT_LIST - A list of points using 1 vertex per point.<br>TRenderOperation.OT_LINE_LIST - A list of lines using 2 vertices per line.<br>TRenderOperation.OT_LINE_STRIP - A strip of lines with one vertice per line and one starting vertice.<br>TRenderOperation.OT_TRIANGLE_LIST - A triangle list with 3 vertices per triangle.<br>TRenderOperation.OT_TRIANGLE_STRIP - A triangle strip with 3 vertices per triangle, 1 starting vertex and 1 vertex for each triangle prior.<br>TRenderOperation.OT_TRIANGLE_FAN - 	A triangle fan with 3 vertices for the first triangle and a single vertice for each triangle prior.
	ENDREM
	Method setOperationType(operationType:Int) 
		SUBMESH_setOperationType(operationType, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the render operation type used to render this TSubMesh. Options: TRenderOperation.OT_POINT_LIST - A list of points using 1 vertex per point.<br>TRenderOperation.OT_LINE_LIST - A list of lines using 2 vertices per line.<br>TRenderOperation.OT_LINE_STRIP - A strip of lines with one vertice per line and one starting vertice.<br>TRenderOperation.OT_TRIANGLE_LIST - A triangle list with 3 vertices per triangle.<br>TRenderOperation.OT_TRIANGLE_STRIP - A triangle strip with 3 vertices per triangle, 1 starting vertex and 1 vertex for each triangle prior.<br>TRenderOperation.OT_TRIANGLE_FAN - 	A triangle fan with 3 vertices for the first triangle and a single vertice for each triangle prior.
	ENDREM
	Method getOperationType:Int() 
		Return SUBMESH_getOperationType(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the vertex data for this TSubMesh. This method takes the place of the vertexData attribute for TSubMesh.
	ENDREM
	Method setVertexData(v:TVertexData) 
		v.managed = True
		SUBMESH_setVertexData(v.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the vertex data for this TSubMesh. This method takes the place of the vertexData attribute for TSubMesh. <br> Note: If there is no non-shared vertex data for this TSubMesh, all values for this TVertexData object will be Null. If you try to dereference null pointers you will get an error, so you must check to see that you have a valid TVertexData object using isNull. Also, if you are going to be replacing an existing TVertexData object and the previous object will not be used, you should set it to unmanaged or clear it.
	ENDREM
	Method getVertexData:TVertexData() 
		Return TVertexData.FromPtr(SUBMESH_getVertexData(Self.Pointer) , 1) 
	End Method
	

	Rem
		bbdoc: Get the face index data for this TSubMesh.
	ENDREM
	Method getIndexData:TIndexData() 
		Local Obj:TIndexData = New TIndexData
		Obj.Pointer = SUBMESH_getIndexData(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the face index data for this TSubMesh.
	ENDREM
	Method setIndexData(indexData:TIndexData) 
		SUBMESH_setIndexData(indexData.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the blend index to bone index map data.
	ENDREM
	Method getBlendIndexToBoneIndexMap:TIndexMap() 
		Local Obj:TIndexMap = New TIndexMap
		Obj.Pointer = SUBMESH_getBlendIndexToBoneIndexMap(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the blend index to bone index map data. Note that doing this only copies the values of one TIndexMap over to the other, so you will not have to keep the TIndexMap supplied valid once this method is used.
	ENDREM
	Method setBlendIndexToBoneIndexMap(blendIndexToBoneIndexMap:TIndexMap) 
		SUBMESH_setBlendIndexToBoneIndexMap(blendIndexToBoneIndexMap.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the LOD face list for this TSubMesh.
	ENDREM
	Method getLodFaceList:TLODFaceList() 
		Local Obj:TLODFaceList = New TLODFaceList
		Obj.Pointer = SUBMESH_getLodFaceList(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the LOD face list for this TSubMesh. Note that doing this only copies the values of one TLodFaceList over tothe ohter, so you will not have to keep the TLodFaceLIst supplied valid once this method is used.
	ENDREM
	Method setLodFaceList(mLodFaceList:TLODFaceList) 
		SUBMESH_setLodFaceList(mLodFaceList.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get a list of extremity points for this TSubmesh. This particular property is not required to be set and is primarily used in semi-transparent meshes for depth sorting where otherwise the z-buffer would be sufficient for culling.
	ENDREM
	Method getExtremityPoints:TExtremityPointsList() 
		Local Obj:TExtremityPointsList = New TExtremityPointsList
		Obj.Pointer = SUBMESH_getExtremityPoints(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Set a list of extremity points for this TSubmesh. This particular property is not required to be set and is primarily used in semi-transparent meshes for depth sorting where otherwise the z-buffer would be sufficient for culling.
	ENDREM
	Method setExtremityPoints(extremityPoints:TExtremityPointsList) 
		SUBMESH_setExtremityPoints(extremityPoints.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get a reference to this TSubMesh's parent mesh.
	ENDREM
	Method getParent:TMesh() 
		Return TMesh.FromPtr(SUBMESH_getParent(Self.Pointer) , True) 
	End Method
	
	Rem
		bbdoc: Set a reference to this TSubMesh's parent mesh.
	ENDREM
	Method setParent(parent:TMesh) 
		SUBMESH_setParent(parent.Pointer, Self.Pointer) 
	End Method
		
End Type

Rem
	bbdoc: NOT SUPPORTED: A hash map that is used to store submesh names.
ENDREM
Type TSubMeshNameMap
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Holds constants for the different types of blending you can do with different textures and materials.
ENDREM
Type TSceneBlendType
	Const SBT_TRANSPARENT_ALPHA:Int = 0
	Const SBT_TRANSPARENT_COLOUR:Int = 1
	Const SBT_ADD:Int = 2
	Const SBT_MODULATE:Int = 3
	Const SBT_REPLACE:Int = 4
End Type

Rem
	bbdoc: NOT SUPPORTED: Holds a list of extremity points for a mesh, submesh, etc.
ENDREM
Type TExtremityPointsList
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Struct for holding the types of vertex animation constants.
ENDREM
Type TVertexAnimationType
	
	Rem
		bbdoc: No vertex animation.
	ENDREM
	Const VAT_NONE:Int = 0
	
	Rem
		bbdoc: Vertex morph animation supported. Vertex morph animation is a set of coherent vertex positions that can be interpolated to create movement. These are easier to set-up and better for performance in general. The limitation with morph vertex animation is that it cannot be blended into different motions based on keyframes. This is what poses are used for.
	ENDREM
	Const VAT_MORPH:Int = 1
	
	Rem
		bbdoc: Vertex pos animation supported. Vertex pose animation is a set of "pose" keyframes, rather than full animation. From these pose keyframes animation is interpolated and blended based on which poses you want at a given time. These are harder to set up and somewhat heavier performance wise, but allow your animations to blend multiple actions together, such as a character model bending upwards to aim while strafing to the side.
	ENDREM
	Const VAT_POSE:Int = 2
End Type

Rem
	bbdoc: Basic static data container for RayTriangle queries from execRayTriangleQuery.
ENDREM
Type TTriangleQueryResults

	Rem
		bbdoc: Has TTriangleQueryResults been initialised yet?
	ENDREM
	Global inited:Int = 0
	
	Rem
		bbdoc: The point at which the collision occured ( in world space ).
	ENDREM
	Global collisionPoint:TVector3 = New TVector3
	
	Rem
		bbdoc: The normal of the surface collided with.
	ENDREM
	Global normal:TVector4 = New TVector4
	
	Rem
		bbdoc: The entity that was hit.
	ENDREM
	Global entity:TEntity = New TEntity
	
	Rem
		bbdoc: Point 1 of the triangle hit.
	ENDREM
	Global triangle1:TVector3 = New TVector3
	
	Rem
		bbdoc: Point 2 of the triangle hit.
	ENDREM
	Global triangle2:TVector3 = New TVector3
	
	Rem
		bbdoc: Point 3 of the triangle hit.
	ENDREM
	Global triangle3:TVector3 = New TVector3
End Type

Rem
	bbdoc: Tags for identifying the different types of vertex buffer contents.
ENDREM
Type TVertexElementSemantic
	Rem
		bbdoc: This describes 3d position as 3 floating point numbers for each vertex.
	ENDREM
	Const VES_POSITION:Int = 1
	
	Rem
		bbdoc: This describes blending weight vertex data.
	ENDREM
	Const VES_BLEND_WEIGHTS:Int = 2
	
	Rem
		bbdoc: This describes blending indice vertex data.
	ENDREM
	Const VES_BLEND_INDICES:Int = 3
	
	Rem
		bbdoc: This describes normal vertex data. Normal vertex data is comprised of three floats.
	ENDREM
	Const VES_NORMAL:Int = 4
	
	Rem
		bbdoc: This describes diffuse colour vertex data.
	ENDREM
	Const VES_DIFFUSE:Int = 5
	
	Rem
		bbdoc: This describes specular colour vertex data.
	ENDREM
	Const VES_SPECULAR:Int = 6
	
	Rem
		bbdoc: This describes texture coordinate vertex data.
	ENDREM
	Const VES_TEXTURE_COORDINATES:Int = 7
	
	Rem
		bbdoc: This describes binormal vertex data.
	ENDREM
	Const VES_BINORMAL:Int = 8
	
	Rem
		bbdoc: This describes tangent vertex data.
	ENDREM
	Const VES_TANGENT:Int = 9
End Type

Rem
	bbdoc: Vertex type, used to identify vertex contents.
ENDREM
Type TVertexElementType
	Const VET_FLOAT1:Int = 0
	Const VET_FLOAT2:Int = 1
	Const VET_FLOAT3:Int = 2
	Const VET_FLOAT4:Int = 3
	Const VET_COLOUR:Int = 4
	Const VET_SHORT1:Int = 5
	Const VET_SHORT2:Int = 6
	Const VET_SHORT3:Int = 7
	Const VET_SHORT4:Int = 8
	Const VET_UBYTE4:Int = 9
	Const VET_COLOUR_ARGB:Int = 10
	Const VET_COLOUR_ABGR:Int = 11
End Type

Rem
	bbdoc: A list of vertex offsets. This type is not fully supported yet.
ENDREM
Type TVertexOffsetMap

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Represents a set of vertex offsets that are applied to vertex data. This type is not fully supported yet.
ENDREM
Type TPose

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Iterate through sets of vertex offsets. This type is not fully supported yet.
ENDREM
Type TPoseIterator

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: A list of poses. This type is not fully supported yet.
ENDREM
Type TPoseList

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: A listing of level of detail distances. This type is not fully supported yet.
ENDREM
Type TLodDistanceList

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: This type represents an object that reduces the geometry given to it progressively.
ENDREM
Type TProgressiveMesh
	
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr

	Rem
		bbdoc: Vertice Reduction Quota : A set number of vertices are removed in the reduction.
	ENDREM
	Const VRQ_CONSTANT:Int = 0
	
	Rem
		bbdoc: Vertice Reduction Quota : A relative number of vertices are removed in reduction based on the complexity of the geometry.
	ENDREM
	Const VRQ_PROPORTIONAL:Int = 1
End Type

Rem
	bbdoc: A list of vertex bone assignments. This type is not fully implemented.
ENDREM
Type TVertexBoneAssignmentList

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Type that keeps track of automatic parameters in GPU program parameters. This type is not fully implemented. 
ENDREM
Type TAutoConstantEntry

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Iterator for vertex bone assignments. This type is not fully implemented. 
ENDREM
Type TBoneAssignmentIterator

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: A list of level of detail faces. This type is not fully implemented.
ENDREM
Type TLODFaceList

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: An iterator for TSubMeshes. This type is not fully implemented.
ENDREM
Type TSubMeshIterator

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type


Rem
	bbdoc: An data structure that contains individual LOD detail information for a specific LOD. This type is not fully implemented.
ENDREM
Type TMeshLodUsage

	Rem
		bbdoc: The pointer to this structure in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: If this LOD is manually created, this will contain the .mesh file to be used.
	ENDREM
	Field manualName:String
	
	Rem
		bbdoc: Link to the TMesh used in this LOD. Note that while you may modify the current mesh, modifying which mesh has no effect on the actual data structure as they are not linked by reference.
	ENDREM
	Field manualMesh:TMesh
	
	Rem
		bbdoc: Link to the TEdgeData for this LOD usage level.
	ENDREM
	Field edgeData:TEdgeData
	
	Rem
		bbdoc: Method for initializing data structures on creation of this TMeshLodUsage.
	ENDREM
	Method New() 
		Self.manualMesh = New TMesh
		Self.edgeData = New TEdgeData
	End Method
End Type

Rem
	bbdoc: A class that describes a connection of edges across a set of triangles/vertices/indexes. TEdgeData is built for each seperate TSubMesh of a TMesh, as well.
ENDREM
Type TEdgeData

	Rem
		bbdoc: The pointer to this structure in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: All of the triangles of this list stored in a TTriangleList array.
	ENDREM
	Field triangles:TTriangleList
	
	Rem
		bbdoc: All of the triangle face normals for this TEdgeData.
	ENDREM
	Field triangleFaceNormals:TTriangleFaceNormalList
	
	Rem
		bbdoc: The light facing states of each triangle of this TEdgeData.
	ENDREM
	Field triangleLightFacings:TTriangleLightFacingList
	
	Rem
		bbdoc: All of the edge groups in this TEdgeList.
	ENDREM
	Field edgeGroups:TEdgeGroupList
	
	Rem
		bbdoc: Flag indicating whether this edge list is closed/manifold.
	ENDREM
	Field isClosed:Int Ptr
End Type

Rem
	bbdoc: Type representing a vertex bone assignment.
ENDREM
Type TVertexBoneAssignment

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create an empty TVertexBoneAssignment object for holding bone assignments.
	ENDREM
	Function Create:TVertexBoneAssignment()
		Local Obj:TVertexBoneAssignment = New TVertexBoneAssignment
		Obj.Pointer = VBA_create()
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		VBA_delete(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the vertex index of this TVertexBoneAssignment.
	ENDREM
	Method getVertexIndex:Int()
		Return VBA_getVertexIndex(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the vertex index of this TVertexBoneAssignment.
	ENDREM
	Method setVertexIndex(vertexIndex:Int)
		VBA_setVertexIndex(vertexIndex, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the bone index of this TVertexBoneAssignment.
	ENDREM
	Method getBoneIndex:Int()
		Return VBA_getBoneIndex(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the bone index of this TVertexBoneAssignment.
	ENDREM
	Method setBoneIndex(boneIndex:Int)
		VBA_setBoneIndex(boneIndex, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the weight of this assignment.
	ENDREM
	Method getWeight:Float()
		Return VBA_getWeight(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the weight of this assignment.
	ENDREM
	Method setWeight(weight:Float)
		VBA_setWeight(weight, Self.Pointer)
	End Method
	
End Type

Rem
	bbdoc: Represents a list of triangles. This type is not fully implemented.
ENDREM
Type TTriangleList

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Represents a normal list for triangle faces. This type is not fully implemented.  
ENDREM
Type TTriangleFaceNormalList

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Triangle Light Facing List. This type is not fully implemented.
ENDREM
Type TTriangleLightFacingList

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: A list of edge groups. This type is not fully implemented.
ENDREM
Type TEdgeGroupList

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Represents GPU program parameters.
ENDREM
Type TGpuProgramParameters

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr

	Rem
		bbdoc: Get the number of auto-constant params used in these parameters.
	ENDREM
	Method getAutoConstantCount:Int()
		Return GPUPP_getAutoConstantCount(Self.Pointer)
	End Method

	Rem
		bbdoc: Set a named constant as a floating point value for this pass's gpu program.
	ENDREM
	Method setNamedConstantWithFloat(name:String, val:Float)
		GPUPP_setNamedConstantWithFloat(name.ToCString() , val, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set a named constant as an integer value for this pass's gpu program.
	ENDREM
	Method setNamedConstantWithInt(name:String, val:Int)
		GPUPP_setNamedConstantWithInt(name.ToCString() , val, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set a named constant as a 4d vector value for this pass's gpu program.
	ENDREM
	Method setNamedConstantWithVec4(name:String, val:TVector4)
		GPUPP_setNamedConstantWithVec4(name.ToCString() , val.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set a named constant as a 3d vector value for this pass's gpu program.
	ENDREM
	Method setNamedConstantWithVec3(name:String, val:TVector3)
		GPUPP_setNamedConstantWithVec3(name.ToCString() , val.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set a named constant as a 4x4 Matrix value for this pass's gpu program.
	ENDREM
	Method setNamedConstantWithMatrix4(name:String, val:TMatrix4)
		GPUPP_setNamedConstantWithMatrix4(name.ToCString() , val.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set a named constant as a colour value for this pass's gpu program.
	ENDREM
	Method setNamedConstantWithColourValue(name:String, val:TColourValue)
		GPUPP_setNamedConstantWithColourValue(name.ToCString() , val.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set named auto constant by the named constant's type and value. For acType see TAutoConstantType for constants.
	ENDREM
	Method setNamedAutoConstant(name:String, acType:Int, extraInfo:Int)
		GPUPP_setNamedAutoConstant(name.ToCString() , acType, extraInfo, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set named auto constant with a floating point value and auto constant type. For acType see TAutoConstantType for constants.
	ENDREM
	Method setNamedAutoConstantReal(name:String, acType:Int, extraInfo:Int)
		GPUPP_setNamedAutoConstant(name.ToCString() , acType, extraInfo, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set a named constant with a time factor.
	ENDREM
	Method setNamedConstantFromTime(name:String, factor:Float)
		GPUPP_setNamedConstantFromTime(name.ToCString() , factor, Self.Pointer)
	End Method
	
End Type

Rem
	bbdoc: Singleton that manages drawing text boxes to the screen.
ENDREM
Type TTextManager
	
	Rem
		bbdoc: Here we initialise the resources used by the text manager to draw text boxes.
	EndRem
	Function init()
		TEXTMANAGER_init()
	End Function
	
	Rem
		bbdoc: Here we free the resources used by the text manager.
	ENDREM
	Function uninit()
		TEXTMANAGER_uninit()
	End Function
	
	Rem
		bbdoc: Add a text box onto the screen using relative coordinates. Default colour is white.
	ENDREM
	Function addTextBox(ID:String, text:String, x:Float, y:Float, width:Float, height:Float, colour:TColourValue = Null)
	
		'Appease defaults
		If colour = Null Then
			colour = TColourValue.Create(1.0, 1.0, 1.0, 1.0)
		End If
	
		TEXTMANAGER_addTextBox(id.ToCString() , text.ToCString() , x, y, width, height, colour.Pointer)
	End Function
	
	Rem
		bbdoc: Remove a text box by its ID.
	ENDREM
	Function removeTextBox(ID:String)
		TEXTMANAGER_removeTextBox(id.ToCString())
	End Function
	
	Rem
		bbdoc: Set the text in a text box by ID.
	ENDREM
	Function setText(ID:String, Text:String)
		TEXTMANAGER_setText(id.ToCString() , Text.ToCString())
	End Function
	
	Rem
		bbdoc: Remove and destroy all managed text boxes.
	ENDREM
	Function clear()
		TEXTMANAGER_clear()
	End Function
	
	Rem
		bbdoc: Set the colour of the text specified by the ID.
	ENDREM
	Function setTextColour(ID:String, colour:TColourValue)
		TEXTMANAGER_setTextColour(id.ToCString() , colour.Pointer)
	End Function
	
	Rem
		bbdoc: Get the colour of the text specified by the ID.
	ENDREM
	Function getTextColour:TColourValue(ID:String)
		Local Obj:TColourValue = New TColourValue
		Obj.Pointer = TEXTMANAGER_getTextColour(id)
		Obj.managed = False
		Return Obj
	End Function
	
	Rem
		bbdoc: Set the default text colour used when addTextBox(...) is called.
	ENDREM
	Function setDefaultTextColour(colour:TColourValue)
		TEXTMANAGER_setDefaultTextColour(colour.Pointer)
	End Function
	
	Rem
		bbdoc: Get the default text colour used when addTextBox(...) is called.
	ENDREM
	Function getDefaultTextColour:TColourValue()
		Return TColourValue.FromPtr(TEXTMANAGER_getDefaultTextColour())
	End Function
	
	Rem
		bbdoc: Set the text font to a font resource name. Note that this is not just a TrueType font, it must be registered as a font resource either manually or with a .font file.
	ENDREM
	Function setTextFont(ID:String, FontName:String)
		TEXTMANAGER_setTextFont(id.ToCString() , FontName.ToCString())
	End Function
	
	Rem
		bbdoc: Get the text's font resource name.
	ENDREM
	Function getTextFont:String(ID:String)
		Return String.FromCString(TEXTMANAGER_getTextFont(id.ToCString()))
	End Function
	
	Rem
		bbdoc: Sets the default font used everytime addTextBox is used.
	ENDREM
	Function setDefaultFont(FontName:String)
		TEXTMANAGER_setDefaultFont(FontName.ToCString())
	End Function
	
	Rem
		bbdoc: Get the default font used everytime addTextBox is used.
	ENDREM
	Function getDefaultFont:String()
		Return String.FromCString(TEXTMANAGER_getDefaultFont())
	End Function
	
	Rem
		bbdoc: Set the text colour for the TOP portion of the given text.
	ENDREM
	Function setTextColourTop(ID:String, col:TColourValue)
		TEXTMANAGER_setTextColourTop(id.ToCString() , col.Pointer)
	End Function
	
	Rem
		bbdoc: Get the text colour for the top portion of the given text.
	ENDREM
	Function getTextColourTop:TColourValue(ID:String)
		Return TColourValue.FromPtr(TEXTMANAGER_getTextColourTop(id))
	End Function
	
	Rem
		bbdoc: Set the text colour for the TOP portion of the given text.
	ENDREM
	Function setTextColourBottom(ID:String, col:TColourValue)
		TEXTMANAGER_setTextColourBottom(id.ToCString() , col.Pointer)
	End Function
	
	Rem
		bbdoc: Get the text colour for the top portion of the given text.
	ENDREM
	Function getTextColourBottom:TColourValue(ID:String)
		Return TColourValue.FromPtr(TEXTMANAGER_getTextColourBottom(id))
	End Function
	
	Rem
		bbdoc: Set the metrics mode ( pixel/relative/etc ) for all text boxes created after this is called. Use the struct in TGuiMetricsMode to find the proper metrics mode constant.
	ENDREM
	Function setDefaultMetricsMode(mm:Int)
	  TEXTMANAGER_setDefaultMetricsMode(mm)
	End Function
	
	Rem
		bbdoc: Get the metrics mode ( pixel/relative/etc ) for all text boxes created after this is called. Use the struct in TGuiMetricsMode to find the proper metrics mode constant. 
	ENDREM
	Function getDefaultMetricsMode:Int()
		Return TEXTMANAGER_getDefaultMetricsMode()
	End Function
	
	Rem
		bbdoc: Set the material name to use for the specified text.
	ENDREM
	Function setTextMaterial(ID:String, materialName:String)
		TEXTMANAGER_setTextMaterial(id.ToCString() , materialName.ToCString())
	End Function
	
	Rem
		bbdoc: Get the material name used for the specified text.
	ENDREM
	Function getTextMaterial:String(ID:String)
		Return String.FromCString(TEXTMANAGER_getTextMaterial(id.ToCString()))
	End Function
	
	Rem
		bbdoc: Rotate all the text managed by this TTextManager a given number of degrees.
	EndRem
	Function rotateAllText( Degrees:Float )
		TEXTMANAGER_rotateAllText( Degrees )	
	End Function
	
	Rem
		bbdoc: Get the angle of the text in this TTextManager.
	EndRem
	Function getTextAngle:Float()
		Return TEXTMANAGER_getTextAngle()
	End Function
		
	Rem
		bbdoc: Set the default character height when you create a text box.
	EndRem
	Function setDefaultCharacterHeight( height:Int )
		TEXTMANAGER_setDefaultCharacterHeight( height )
	End Function
	
	Rem
		bbdoc: Get the default character height when you create a text box.
	EndRem
	Function getDefaultCharacterHeight:Int()
		Return TEXTMANAGER_getDefaultCharacterHeight()
	End Function
	
End Type

Rem
	bbdoc: Type representing a render queue invocation sequence. This type is not fully implemented.
EndRem
Type TRenderQueueInvocationSequence

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Type represents a render queue listener object. This type is not fully implemented.
ENDREM
Type TRenderQueueListener

	Rem
		bbdoc: Reference to this object in memory. This type is not fully implemented.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Type represents a manual resource loader. This type is not fully implemented.
ENDREM
Type TManualResourceLoader

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Type that represents a resource listener. This type is not fully implemented.
ENDREM
Type TResourceListener

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Type that represents a hardware vertex buffer shared pointer. This type is being phased out and is not supported.
ENDREM
Type THardwareVertexBufferSharedPtr

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type


Rem
	bbdoc: Interface that describes instances of objects that have key-frame animation information. This may be a single key-frame, or many.
ENDREM
Type TAnimableObject

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: A generic user object for user storage. Please note that this userData object will not survive the destruction of this instance.
	ENDREM
	Field userData:Object
	
	Rem
		bbdoc: Gets a TStringVector containing all of the AnimableValue names for this TAniambleObject.
	ENDREM
	Method getAnimableValueNames:TStringVector() 
		Local Obj:TStringVector = New TStringVector
		Obj.Pointer = AO_getAnimableValueNames( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Use this to  create an animable value for a TAnimableObject.
	ENDREM
	Method createAnimableValue:TAnimableValue(valueName:String) 
		Local Obj:TAnimableValue = New TAnimableValue
		Obj.Pointer = AO_createAnimableValue(valueName.ToCString() , Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Helper function to determine whether or not the reference to this user-defined object is null.
	ENDREM
	Method isNull:Byte()
		If Self.Pointer <> Null Then
			Return False
		Else
			Return True
		End If
	End Method
	
	
End Type

Rem
	bbdoc: Interface that describes objects that have a movable position that is set by attaching the TMovableObject to a TSceneNode.
ENDREM
Type TMovableObject Extends TAnimableObject

	Rem
		bbdoc: Field that internally keeps track of which type of TMovableObject we have.
	ENDREM
	Field vartype:Int = 0
	
	Rem
		bbdoc: INTERNAL - Notify this TMovableObject of the factory interface it was created by.
	ENDREM
	'@params: fact:TMovableObject The TMovableObjectFactory that this object is to be told it was created by.
	Method _notifyCreator( fact:TMovableObjectFactory )
		MO__notifyCreator( fact.Pointer , Self.Pointer )
	End Method

	Rem
		bbdoc: INTERNAL - Get the factory interface that created this TMovableObject.
	ENDREM
	Method _getCreator:TMovableObjectFactory() 
		Local Obj:TMovableObjectFactory = New TMovableObjectFactory
		Obj.Pointer = MO__getCreator( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: INTERNAL - Notify this TMovableObject of the TSceneManager that created it.
	ENDREM
	Method _notifyManager(man:TSceneManager) 
		MO__notifyManager( man.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: INTERNAL - Get the TSceneManager that governs this object.
	ENDREM
	Method _getManager:TSceneManager() 
		Local Obj:TSceneManager = New TSceneManager
		Obj.Pointer = MO__getManager( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the name of this object.
	ENDREM
	Method getName:String() 
		Local temp:String
		Return temp.FromCString( MO_getName( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Get the TMovableObject child class type for this object and return it as a string.
	ENDREM
	Method getMovableType:String() 
		Return String.fromcstring(MO_getMovableType(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Get the TOgreNode that this object is attached to.
	ENDREM
	Method getParentNode:TOgreNode() 
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = MO_getParentNode( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the TSceneNode that this object is attached to. 
	ENDREM
	Method getParentSceneNode:TSceneNode() 
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = MO_getParentSceneNode( Self.Pointer ) 
		Return Obj
	End Method
	
	Rem
		bbdoc: INTERNAL - Tell this TMovableObject that it has been attached to a TOgreNode.<br>parent:TOgreNode The TOgreNode that this object is told it is attached to.<br>Specifies whether or not parent:TOgreNode is a TTagPoint.
	ENDREM
	Method _notifyAttached(parent:TOgreNode, isTagPoint:Int = False) 
		MO__notifyAttached( parent.Pointer , isTagPoint , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get whether or not this object is attached to a TOgreNode or TTagPoint
	ENDREM
	Method isAttached:Byte()
		Return MO_isAttached( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Detatch this Movable Object from its node or tag point, if currently attached.
	ENDREM
	Method detatchFromParent()
		MO_detatchFromParent(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not this object is attached to a TSceneNode or TTagPoint that is  active within the current TSceneManager. Ie. Is it in the current scene?
	ENDREM
	Method isInScene:Byte()
		Return MO_isInScene( Self.Pointer )
	End Method
	
	Rem
		bbdoc: INTERNAL - Tell this TMovableObject that it's position has changed in some way.
	ENDREM
	Method _notifyMoved()
		MO__notifyMoved( Self.Pointer )
	End Method
	
	Rem
		bbdoc: INTERNAL - Tell object which TCamera will be used for the next rendering operation.
	ENDREM
	Method _notifyCurrentCamera( cam:TCamera )
		MO__notifyCurrentCamera( cam.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the local bounding box for this object.
	ENDREM
	Method getBoundingBox:TAxisAlignedBox() 
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = MO_getBoundingBox(Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the centered bounding radius for this object.
	ENDREM
	Method getBoundingRadius:Float() 
		Return MO_getBoundingRadius( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the world bounding sphere for this object.
	ENDREM
	Method getWorldBoundingSphere:TSphere(derive:Int = False) 
		Local Obj:TSphere = New TSphere
		Obj.Pointer = MO_getWorldBoundingSphere( derive , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: INTERNAL - Internal call that takes place when this object is to be rendered. This causes the object to create TRenderableOnstances on the TRenderQueue given.
	ENDREM
	Method _updateRenderQueue(queue:TRenderQueue) 
		MO__updateRenderQueue( queue.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set this object to visible or invisible if it can be.
	ENDREM
	Method setVisible(visible:Byte)
		MO_setVisible( visible , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get whether this object is visible or not.
	ENDREM
	Method getVisible:Byte()
		Return MO_getVisible(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets if this object is supposed to be visible or not. This method also considers visibility flags and rendering distance.
	ENDREM
	Method isVisible:Byte()
		Return MO_isVisible( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the viewing distance/radius where the object will be culled out of visibility.
	ENDREM
	Method setRenderingDistance(dist:Float) 
		MO_setRenderingDistance( dist , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the viewing distance where the object will be culled out of visibility.
	ENDREM
	Method getRenderingDistance:Float() 
		Return MO_getRenderingDistance( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Through use of the TUserDefinedObject interface, allows you to specify any C++ Native Types/Objects  and inject them into this TMovableObject.
	ENDREM
	Method setUserObject(obj:TUserDefinedObject) 
		MO_setUserObject(obj.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Retreive the TUserDefinedObject used and the information within it from this object.
	ENDREM
	Method getUserObject:TUserDefinedObject() 
		Local Obj:TUserDefinedObject = New TUserDefinedObject
		Obj.Pointer = MO_getUserObject(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Sets any user specified numeric value type on this object.
	ENDREM
	Method setUserAny(anything:TAny) 
		MO_setUserAny( anything.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets any user specified numeric value on this object.
	ENDREM
	Method getUserAny:TAny() 
		Local Obj:TAny = New TAny
		Obj.Pointer = MO_getUserAny( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Sets a renderqueue group for this object, allowing it to be rendered earlier or later than other objects.<br>Different render queues:<br></br> Const RENDER_QUEUE_BACKGROUND:Int = 0 </br>Const RENDER_QUEUE_SKIES_EARLY:Int = 5 </br>Const RENDER_QUEUE_1:Int = 10 </br>Const RENDER_QUEUE_2:Int = 20 </br>Const RENDER_QUEUE_WORLD_GEOMETRY_1:Int = 25 </br>Const RENDER_QUEUE_3:Int = 30 </br>Const RENDER_QUEUE_4:Int = 40 </br>Const RENDER_QUEUE_MAIN:Int = 50 </br>Const RENDER_QUEUE_6:Int = 60 </br>Const RENDER_QUEUE_7:Int = 70 </br>Const RENDER_QUEUE_WORLD_GEOMETRY_2:Int = 75 </br>Const RENDER_QUEUE_8:Int = 80 </br>Const RENDER_QUEUE_9:Int = 90 </br>Const RENDER_QUEUE_SKIES_LATE:Int = 95 </br>Const RENDER_QUEUE_OVERLAY:Int = 100 </br></br> Where 50 is the default render queue for most TMovableObject derivatives. 0 is Drawn first, 100 is drawn last
	ENDREM
	Method setRenderQueueGroup(queueID:Int) 
		MO_setRenderQueueGroup( queueID , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the renderqueue group for this object, specifying whether it will be earlier or later than other objects. <br>Different render queues:<br></br> Const RENDER_QUEUE_BACKGROUND:Int = 0 </br>Const RENDER_QUEUE_SKIES_EARLY:Int = 5 </br>Const RENDER_QUEUE_1:Int = 10 </br>Const RENDER_QUEUE_2:Int = 20 </br>Const RENDER_QUEUE_WORLD_GEOMETRY_1:Int = 25 </br>Const RENDER_QUEUE_3:Int = 30 </br>Const RENDER_QUEUE_4:Int = 40 </br>Const RENDER_QUEUE_MAIN:Int = 50 </br>Const RENDER_QUEUE_6:Int = 60 </br>Const RENDER_QUEUE_7:Int = 70 </br>Const RENDER_QUEUE_WORLD_GEOMETRY_2:Int = 75 </br>Const RENDER_QUEUE_8:Int = 80 </br>Const RENDER_QUEUE_9:Int = 90 </br>Const RENDER_QUEUE_SKIES_LATE:Int = 95 </br>Const RENDER_QUEUE_OVERLAY:Int = 100 </br></br> Where 50 is the default render queue for most TMovableObject derivatives. 0 is Drawn first, 100 is drawn last
	ENDREM
	Method getRenderQueueGroup:Int() 
		Return MO_getRenderQueueGroup( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the full transform of the parent node.
	ENDREM
	Method _getParentNodeFullTransform:TMatrix4() 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MO__getParentNodeFullTransform( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Sets a query flag for this object as a specifier for when scene queries such as TRaySceneQuery are done. This value is a bitwise mask value that can and should be manipulated by bitwise operators if multiple values are used.
	ENDREM
	Method setQueryFlags(flags:Int) 
		MO_setQueryFlags( flags , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Adds flags/bits to the query flag that is used in scene queries, such as TRaySceneQuery, to include or exclude objects. This value is a bitwise mask, and the values for the flags given are appended to the current query mask.
	ENDREM
	Method addQueryFlags(flags:Int) 
		MO_addQueryFlags( flags , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Removes flags/bits to the query flag that is used in scene queries, such as TRaySceneQuery, to include or exclude objects. This value is a bitwise mask, and the values for the flags given are removed from the current query mask.
	ENDREM
	Method removeQueryFlags(flags:Int) 
		MO_removeQueryFlags( flags , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get this object's query flags. The value is a bitwise mask.
	ENDREM
	Method getQueryFlags:Int() 
		Return MO_getQueryFlags( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets a bitwise mask of flags called visibility flags. Visibility flags comprise the visibility mask, which is used with a scene manager to determine what object are visible at any given time. This can be used dynamically for group based occlusions, etc.
	ENDREM
	Method setVisibilityFlags(flags:Int) 
		MO_setVisibilityFlags( flags , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Adds flags/bits to the current visibility flags by appending them. These comprise the visibility mask, which is used by the scene manager to determine if object are visible based on bitwise flags. This can be used dynamically for grouping, etc.
	ENDREM
	Method addVisibilityFlags(flags:Int) 
		MO_addVisibilityFlags( flags , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Removes flags/bits from the current visbility flags. This comprises the visibility mask, which is used by the scene manager to determine if object are visible based on bitwise flags. This can be used dynamically for grouping, etc.
	ENDREM
	Method removeVisibilityFlags(flags:Int) 
		MO_removeVisibilityFlags( flags , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get this object's visibility flags. This comprises the visibility mask, which is used by the scene manager to determine if object are visible based on bitwise flags. This can be used dynamically for grouping, etc.
	ENDREM
	Method getVisibilityFlags:Int() 
		Return MO_getQueryFlags( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set a listener for this object so you you listen in and react to events for this object, such as attach and detachments, etc.
	ENDREM
	Method setListener(listener:TMovableObjectListener) 
		MO_setListener( listener.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the listener currently attached to this object.
	ENDREM
	Method getListener:TMovableObjectListener() 
		Local Obj:TMovableObjectListener = New TMovableObjectListener
		Obj.Pointer = MO_getListener( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets a list of lights that affect this object, ordered from closest to furthest.
	ENDREM
	Method queryLights:TLightList() 
		Local Obj:TLightList = New TLightList
		Obj.Pointer = MO_queryLights( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Sets whether or not this object will cast shadows when a scene manager shadow technique is set with TSceneManager.setShadowTechnique(...).
	ENDREM
	Method setCastShadows(enabled:Int) 
		MO_setCastShadows( enabled , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get this TMovableObject type's specific bitpattern for use in inclusion/exclusion in scenequeries.
	ENDREM
	Method getTypeFlags:Int() 
		Return MO_getTypeFlags( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Method to allow the iteration of each renderable in this MO. Visitors are not currently supported.
	ENDREM
	Method visitRenderables(visitor:TRenderableVisitor, debugRenderables:Byte = False)
		MO_visitRenderables(visitor.Pointer, debugRenderables, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set whether or not you want Ogre to display visible debug geometry for this movable object.
	ENDREM
	Method setDebugDisplayEnabled(enabled:Byte)
		MO_setDebugDisplayEnabled(enabled, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not you want Ogre to display visible debug geometry for this movable object.
	ENDREM
	Method isDebugDisplayEnabled:Byte()
		Return MO_isDebugDisplayEnabled(Self.Pointer)
	End Method

	Rem
		bbdoc: Cast this TMovableObject to an entity.
	ENDREM
	Method toEntity:TEntity()
		Local Obj:TEntity = New TEntity
		Obj.Pointer = Self.Pointer
		Obj.vartype = 1
		Return Obj
	End Method
	
	Rem
		bbdoc: Cast this TMovableObject to a light.
	ENDREM
	Method toLight:TLight()
		Local Obj:TLight = New TLight
		Obj.Pointer = Self.Pointer
		Obj.vartype = 0
		Return Obj
	End Method
	
	Rem
		bbdoc: Cast this TMovableObject to a camera.
	ENDREM
	Method toCamera:TCamera()
		Local Obj:TCamera = New TCamera
		Obj.Pointer = Self.Pointer
		Obj.vartype = 2
		Return Obj
	End Method
	
	Rem
		bbdoc: Cast this TMovableObject to a particle system.
	ENDREM
	Method toParticleSystem:TParticleSystem()
		Local Obj:TParticleSystem = New TParticleSystem
		Obj.Pointer = Self.Pointer
		Obj.vartype = 5
		Return Obj
	End Method
	
	Rem
		bbdoc: Cast this TMovableObject to a manual object.
	ENDREM
	Method toManualObject:TManualObject()
		Local Obj:TManualObject = New TManualObject
		Obj.Pointer = Self.Pointer
		Obj.vartype = 6
		Return Obj
	End Method
	
	Rem
		bbdoc:	Set the default query flags	for all future TMovableObjects created.  This value is a bitwise mask value that can and should be manipulated by bitwise operators if multiple values are used.
	ENDREM
	Function setDefaultQueryFlags(flags:Int)
		MO_setDefaultQueryFlags(flags)
	End Function
	
	Rem
		bbdoc:	Get the default query flags	for all future TMovableObjects created.  This value is a bitwise mask value that can and should be manipulated by bitwise operators if multiple values are used.
	ENDREM
	Function getDefaultQueryFlags:Int()
		Return MO_getDefaultQueryFlags()
	End Function
	
	Rem
		bbdoc:	Set the default visibility flags for all future TMovableObjects created.  This value is a bitwise mask value that can and should be manipulated by bitwise operators if multiple values are used.
	ENDREM
	Function setDefaultVisibilityFlags(flags:Int)
		MO_setDefaultVisibilityFlags(flags)
	End Function
	
	Rem
		bbdoc:	Get the default visibility flags for all future TMovableObjects created.  This value is a bitwise mask value that can and should be manipulated by bitwise operators if multiple values are used.
	ENDREM
	Function getDefaultVisibilityFlags:Int()
		Return MO_getDefaultVisibilityFlags()
	End Function
	
	Rem
		bbdoc: Helper function that lets you create a TMovableObject from a Byte Ptr.
	ENDREM
	Function FromPtr:TMovableObject(pointer:Byte Ptr)
		Local Obj:TMovableObject = New TMovableObject
		Obj.Pointer = pointer
		Return Obj
	End Function
	
	'----------------------
	'From TShadowCaster
	'----------------------
	
	Rem
		bbdoc: Retreive the flag that specifies if the object casts a shadow or not.
	ENDREM
	Method getCastShadows:Int() 
		Return SHADOWCASTER_getCastShadows( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the edgelist for this shadowcaster in TEdgeData vector form.
	ENDREM
	Method getEdgeList:TEdgeList() 
		Local Obj:TEdgeList = New TEdgeList
		Obj.Pointer = SHADOWCASTER_getEdgeList(Self.Pointer) 
		Return Obj
	End Method
	
	
	Rem
		bbdoc: Retreive the flag that specifies if the object has an edge list or not.
	ENDREM
	'@returns : An Int that is TRUE if there is an edge list, and FALSE if there is not.
	Method hasEdgeList:Int()
		Return SHADOWCASTER_hasEdgeList( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the world bounding box of the TShadowCaster.
	ENDREM
	Method getWorldBoundingBox:TAxisAlignedBox(derive:Int = False) 
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = SHADOWCASTER_getWorldBoundingBox(derive, Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Retreive the world TAxisAlignedBox for the light cap.
	ENDREM
	Method getLightCapBounds:TAxisAlignedBox() 
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = SHADOWCASTER_getLightCapBounds(Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Retreive the world TAxisAlignedBox for the dark cap, using the light for extrusion.<br>dirLightExtrusionDist:Float The distance of the extrusion.<br>Returns a TAxisAlignedBox that contains the metrics of the dark cap  axis-aligned box for the TShadowCaster. 
	ENDREM
	Method getDarkCapBounds:TAxisAlignedBox(l:TLight, dirLightExtrusionDist:Float) 
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = SHADOWCASTER_getDarkCapBounds(l.Pointer, dirLightExtrusionDist, Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Retreive the distance the shadow volume must be extruded from a specified TLight.
	ENDREM
	Method getPointExtrusionDistance:Float(l:TLight) 
		Return SHADOWCASTER_getPointExtrusionDistance( l.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED: Static function to exture vertices in a vertex buffer based on light calculations.<br>vertexBuffer: Must be originalVertexCount*2*3 floats long to account for doubling of the vertices for the extrude.<br>originalVertexCount:Long The original number of vertices before the doubling.<br>lightPos:TVector4  The light position in 4d space, the w will be populated if the light is directional.<br>extrudeDist:Float The distance to extrude the vertices.
	ENDREM
	Function extrudeVertices(vertexBuffer:THardwareVertexBufferSharedPtr, originalVertexCount:Long, lightPos:TVector4, extrudeDist:Float) 
		SHADOWCASTER_extrudeVertices(vertexBuffer.Pointer, originalVertexCount, lightPos.pointer, extrudeDist)
	End Function
	
End Type

Rem
	bbdoc: Object describing a TMovableObject based on a TMesh.
ENDREM
Type TEntity Extends TMovableObject
	
	Rem
		bbdoc: Override new to provide the proper vartype.
	ENDREM
	Method New() 
		Self.vartype = 1
	End Method
	
	Rem
		bbdoc: Vertex data we're sending to the renderer : Original.
	ENDREM
	Const BIND_ORIGINAL:Int = 0
	
	Rem
		bbdoc: Vertex data we're sending to the renderer : Skeletal.
	ENDREM
	Const BIND_SOFTWARE_SKELETAL:Int = 1
	
	Rem
		bbdoc: Vertex data we're sending to the renderer : Software Morph.
	ENDREM
	Const BIND_SOFTWARE_MORPH:Int = 2
	
	Rem
		bbdoc: Vertex data we're sending to the renderer : Hardware Morph.
	ENDREM
	Const BIND_HARDWARE_MORPH:Int = 3
	
	Rem
		bbdoc: Get the TMesh object attached to this entity. This is a hard reference, and if it is destroyed then the mesh attached to this entity will be destroyed and unavailable to this entity.
	ENDREM
	Method getMesh:TMesh() 
		Return TMesh.FromPtr(ENT_getMesh(Self.Pointer) , 1) 
	End Method
	
	Rem
		bbdoc: Get a sub-part of this TEntity by specifying the sub entity's index.
	ENDREM
	Method getSubEntity:TSubEntity(index:Int) 
		Local Obj:TSubEntity = New TSubEntity
		Obj.Pointer = ENT_getSubEntity( index , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a sub-part of this TEntity by it's name.
	ENDREM
	Method getSubEntityWithName:TSubEntity(Name:String) 
		Local Obj:TSubEntity = New TSubEntity
		Obj.Pointer = ENT_getSubEntityWithName( name.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets the number of sub-entities attached to this TEntity.
	ENDREM
	Method getNumSubEntities:Int() 
		Return ENT_getNumSubEntities( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Clones this TEntity, returning the clone.
	ENDREM
	Method clone:TEntity(cloneName:String) 
		Local Obj:TEntity = New TEntity
		Obj.Pointer = ENT_clone( cloneName.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Sets a material for this TEntity and all of it's sub-entities.
	ENDREM
	Method setMaterialName(materialName:String) 
		ENT_setMaterialName( materialName.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Internal method used to let an object know which camera it will be rendered from. Overridden from TMovableObject.
	ENDREM
	Method _notifyCurrentCamera(cam:TCamera) 
		ENT__notifyCurrentCamera( cam.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets a renderqueue group for this object, allowing it to be rendered earlier or later than other objects. Overrides TMovableObject.
	ENDREM
	'@params: Set the render queue group that you wish this object to be in, a list is detailed below: </br> Const RENDER_QUEUE_BACKGROUND:Int = 0 </br>Const RENDER_QUEUE_SKIES_EARLY:Int = 5 </br>Const RENDER_QUEUE_1:Int = 10 </br>Const RENDER_QUEUE_2:Int = 20 </br>Const RENDER_QUEUE_WORLD_GEOMETRY_1:Int = 25 </br>Const RENDER_QUEUE_3:Int = 30 </br>Const RENDER_QUEUE_4:Int = 40 </br>Const RENDER_QUEUE_MAIN:Int = 50 </br>Const RENDER_QUEUE_6:Int = 60 </br>Const RENDER_QUEUE_7:Int = 70 </br>Const RENDER_QUEUE_WORLD_GEOMETRY_2:Int = 75 </br>Const RENDER_QUEUE_8:Int = 80 </br>Const RENDER_QUEUE_9:Int = 90 </br>Const RENDER_QUEUE_SKIES_LATE:Int = 95 </br>Const RENDER_QUEUE_OVERLAY:Int = 100 </br></br> Where 50 is the default render queue for most TMovableObject derivatives. 0 is Drawn first, 100 is drawn last.
	Method setRenderQueueGroup( queueID:Int )
		ENT_setRenderQueueGroup( queueID , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the local Axis Aligned Bounding Box for this object.
	ENDREM
	Method getBoundingBox:TAxisAlignedBox() 
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = ENT_getBoundingBox(Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Return the bounds of all the children objects of this TEntity as a TAxisAlignedBox.
	ENDREM
	Method getChildObjectsBoundingBox:TAxisAlignedBox() 
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = ENT_getChildObjectsBoundingBox(Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Internal method that adds a TRenderable instance of this class to the specified TRenderQueue. Overridden from TMovableObject.
	ENDREM
	Method _updateRenderQueue(queue:TRenderQueue) 
		ENT__updateRenderQueue( queue.Pointer , Self.Pointer )	
	End Method
	
	Rem
		bbdoc: Gets this TMovableObject derived type as a string.
	ENDREM
	Method getMovableType:String() 
		Local temp:String
		Return temp.FromCString( ENT_getMovableType( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Get a named TAnimationState for a mesh. Animation States are a named grouping of frames that contain a certain animation for animated meshes.
	ENDREM
	Method getAnimationState:TAnimationState(Name:String) 
		Local Obj:TAnimationState = New TAnimationState
		Obj.Pointer = ENT_getAnimationState( name.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the set of animation states possible for this TEntity.
	ENDREM
	Method getAllAnimationStates:TAnimationStateSet() 
		Local Obj:TAnimationStateSet = New TAnimationStateSet
		Obj.Pointer = ENT_getAllAnimationStates(Self.Pointer)
		Obj.managed = True
		Return Obj
	End Method
	
	Rem
		bbdoc: If this TEntity has a skeleton, set it to display or not.
	ENDREM
	Method setDisplaySkeleton(display:Int) 
		ENT_setDisplaySkeleton( display  , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the flag specifying whether or not this TEntity's skeleton is visible.
	ENDREM
	Method getDisplaySkeleton:Int() 
		Return ENT_getDisplaySkeleton( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the desired entity For the manual LOD level specified.
	ENDREM
	Method getManualLodLevel:TEntity(index:Int) 
		Local Obj:TEntity = New TEntity
		Obj.Pointer = ENT_getManualLodLevel( index , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets the number of manual LOD levels for this TEntity.
	ENDREM
	Method getNumManualLodLevels:Int() 
		Return ENT_getNumManualLodLevels(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the current LOD bias for this TEntity's mesh. <br>factor:Float The distance at which LOD should start affecting the mesh. This value is proportional: "1.0" would mean no change, "2.0" would mean double and "0.5" would mean half the distance.<br>maxDetailIndex:Int The maximum detail permitted for this TEntity's mesh.<br>minDetailIndex:Int The minimum detail permitted for this TEntity's mesh.
	ENDREM
	Method setMeshLodBias(factor:Float, maxDetailIndex:Int = 0, minDetailIndex:Int = 99) 
		ENT_setMeshLodBias(factor, maxDetailIndex, minDetailIndex, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the current LOD bias for this TEntity's material.<br>factor:Float The distance at which LOD should start affecting the material. This value is proportional: "1.0" would mean no change, "2.0" would mean double and "0.5" would mean half the distance.<br>maxDetailIndex:Int The maximum detail permitted for this TEntity's material.<br>minDetailIndex:Int The minimum detail permitted for this TEntity's material.
	ENDREM
	Method setMaterialLodBias(factor:Float, maxDetailIndex:Int = 0, minDetailIndex:Int = 99) 
		ENT_setMaterialLodBias(factor, maxDetailIndex, minDetailIndex, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set this TEntity's polygon mode so that a TCamera's detail settings may override it's own.
	ENDREM
	Method setPolygonModeOverrideable(PolygonModeOverrideable:Int) 
		ENT_setPolygonModeOverrideable(PolygonModeOverrideable, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Attach another TMovableObject to one of the bones associated with this TEntity, by name. You may also specify rotation and orientation offsets.<br>boneName:String The name of the bone you wish to attach to.<br>pMovable:TmovableObject The TMovable object you wish to attach to the bone.<br>offsetOrientation:TQuaternion The orientation offset of the TMovableObject on the bone. The default is { 1.0 , 0.0 , 0.0 , 0.0 }.<br>offsetPosition:TVector3 The position offset of the TMovableObject on the bone. The default is { 0 , 0 , 0 }.
	ENDREM
	Method attachObjectToBone:TTagPoint(boneName:String, pMovable:TMovableObject, offsetOrientation:TQuaternion = Null, offsetPosition:TVector3 = Null) 
		'Workaround because of a BlitzMax limitation-
		'Default orientation is IDENTITY Quaternion
		If offsetOrientation = Null Then
			offsetOrientation = New TQuaternion
			offsetOrientation.Pointer = TQuaternion.IDENTITY().Pointer
			offsetOrientation.managed = True
		End If
		
		'Default position is ZERO vector
		If offsetPosition = Null Then
			offsetPosition = New TVector3
			offsetPosition.Pointer = TVector3.ZERO().Pointer
			offsetPosition.managed = True
		End If
		
		Local Obj:TTagPoint = New TTagPoint
		Obj.Pointer = ENT_attachObjectToBone(boneName.ToCString() , pMovable.Pointer, offsetOrientation.Pointer, offsetPosition.Pointer, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Detach a TMovableObject that has been attached to a bone on this TEntity with attachObjectToBone.
	ENDREM
	Method detachObjectFromBone(obj:TMovableObject) 
		ENT_detachObjectFromBone(obj.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Detach a TMovableObject that has been attached to a bone on this TEntity with attachObjectToBone by name. Unlike detachFromObject, if the TMovableObject is not found, an exception will be thrown.
	ENDREM
	Method detachObjectFromBoneWithName:TMovableObject(movableName:String) 
		Local Obj:TMovableObject = New TMovableObject
		Obj.Pointer = ENT_detachObjectFromBoneWithName(movableName.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Detach all previously attachObjectToBone(...) objects from this TEntity's bones. BONES :)
	ENDREM
	Method detachAllObjectsFromBone() 
		ENT_detachAllObjectsFromBone(Self.Pointer) 
	End Method
	
	
	Rem
		bbdoc: Gets an iterator allowing you to enumerate all of the objects attached to this TEntity's bones.
	ENDREM
	Method getAttachedObjectIterator:TChildObjectListIterator() 
		Local Obj:TChildObjectListIterator = New TChildObjectListIterator
		Obj.Pointer = ENT_getAttachedObjectIterator(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Internal method for getting the matrices of the bones attached to this TEntity. You should probably not be using this for anything.
	ENDREM
	Method _getBoneMatrices:TMatrix4() 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = ENT__getBoneMatrices(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: This internal method gets the number of matrices attached to bones on this TEntity. You should probably not be using this for anything.
	ENDREM
	Method _getNumBoneMatrices:Int() 
		Return ENT__getNumBoneMatrices(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Check whether or not this TEntity has a skeleton attached to it.
	ENDREM
	Method hasSkeleton:Int() 
		Return ENT_hasSkeleton(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get this TEntity's own TSkeleton instance.
	ENDREM
	Method getSkeleton:TSkeletonInstance() 
		Local Obj:TSkeletonInstance = New TSkeletonInstance
		Obj.Pointer = ENT_getSkeleton(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Check whether or not hardware animation is enable for this TEntity.
	ENDREM
	Method isHardwareAnimationEnabled:Int() 
		Return ENT_isHardwareAnimationEnabled(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the number of requests made for software animation.
	ENDREM
	Method getSoftwareAnimationRequests:Int() 
		Return ENT_getSoftwareAnimationRequests(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the number of requests made for software animation for normals.
	ENDREM
	Method getSoftwareAnimationNormalsRequests:Int() 
		Return ENT_getSoftwareAnimationNormalsRequests(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Add a request for software animation.<br>normalsAlso:Int Does this request for software animation include the normals?
	ENDREM
	Method addSoftwareAnimationRequest(normalsAlso:Byte) 
		ENT_addSoftwareAnimationRequest(normalsAlso, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove a request for software animation.<br>normalsAlso:Int Does this request for software animation include the normals?
	ENDREM
	Method removeSoftwareAnimationRequest(normalsAlso:Byte) 
		ENT_removeSoftwareAnimationRequest(normalsAlso, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Share a TSkeleton(Instance) with another TEntity. The two TEntities must have the same TSkeleton however.
	ENDREM
	Method shareSkeletonInstanceWith(entity:TEntity) 
		ENT_shareSkeletonInstanceWith(entity.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Check to see whether this TEntity contains any vertex animation.
	ENDREM
	Method hasVertexAnimation:Int() 
		Return ENT_hasVertexAnimation(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Stop sharing this TEntity's TSkeletonInstance with other TEntities.
	ENDREM
	Method stopSharingSkeletonInstance() 
		ENT_stopSharingSkeletonInstance( Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Check to see whether or not this TEntity is sharing it's TSkeletonInstance.
	ENDREM
	Method sharesSkeletonInstance:Int() 
		Return ENT_sharesSkeletonInstance(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the set of entities that share this TEntity's TSkeletonInstance.
	ENDREM
	Method getSkeletonInstanceSharingSet:TEntitySet() 
		Local Obj:TEntitySet = New TEntitySet
		Obj.Pointer = ENT_getSkeletonInstanceSharingSet(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Refresh the animation state set to get the newest animations for the TSkeletonInstance.
	ENDREM
	Method refreshAvailableAnimationState() 
		ENT_refreshAvailableAnimationState(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Manually perform all updating required for this animated TEntity.
	ENDREM
	Method _updateAnimation() 
		ENT__updateAnimation(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not any animation is being applied to this TEntity.
	ENDREM
	Method _isAnimated:Int() 
		Return ENT__isAnimated(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not any animation is being applied to this TEntity's skeleton.
	ENDREM
	Method _isSkeletonAnimated:Int() 
		Return ENT__isSkeletonAnimated(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get temporarily blended skeletal vertex information for software-skinned TEntities. If there is no TVertexData for this all attributes will equal 0. Only use this method if you know what you're doing.
	ENDREM
	Method _getSkelAnimVertexData:TVertexData() 
		Return TVertexData.FromPtr(ENT__getSkelAnimVertexData(Self.Pointer) , 1) 
	End Method
	
	Rem
		bbdoc: Get temporarily blended vertex information for software-skinned TEntities. Only use this method if you know what you're doing.
	ENDREM
	Method _getSoftwareVertexAnimVertexData:TVertexData() 
		Return TVertexData.FromPtr(ENT__getSoftwareVertexAnimVertexData(Self.Pointer), 1) 
	End Method
	
	Rem
		bbdoc: Get temporarily blended vertex information for hardware-skinned TEntities. Only use this method if you know what you're doing.
	ENDREM
	Method _getHardwareVertexAnimVertexData:TVertexData() 
		Return TVertexData.FromPtr(ENT__getHardwareVertexAnimVertexData(Self.Pointer), 1) 
	End Method
	
	Rem
		bbdoc: Retreive buffer information for software skeletal animation. Only use this method if you know what you're doing.
	ENDREM
	Method _getSkelAnimTempBufferInfo:TTempBlendedBufferInfo() 
		Local Obj:TTempBlendedBufferInfo = New TTempBlendedBufferInfo
		Obj.Pointer = ENT__getSkelAnimTempBufferInfo(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Retreive buffer information for vertex animation. Only use this method if you know what you're doing.
	ENDREM
	Method _getVertexAnimTempBufferInfo:TTempBlendedBufferInfo() 
		Local Obj:TTempBlendedBufferInfo = New TTempBlendedBufferInfo
		Obj.Pointer = ENT__getVertexAnimTempBufferInfo(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get vertex data that can be used for GPU binding.
	ENDREM
	Method getVertexDataForBinding:TVertexData() 
		Local Obj:TVertexData = New TVertexData
		Obj.Pointer = ENT_getVertexDataForBinding(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Chooses and returns which vertex data to bind to the renderer.<br>hasVertexAnim:Int Does this vertex data include animation?<br>An integer value corresponding to these constants: </br>TEntity.BIND_ORIGINAL = 0</br>TEntity.BIND_SOFTWARE_SKELETAL = 1 </br> TEntity.BIND_SOFTWARE_MORPH = 2 </br> TEntity.BIND_HARDWARE_MORPH = 3
	ENDREM
	Method chooseVertexDataForBinding:Int(hasVertexAnim:Byte) 
		Return ENT_chooseVertexDataForBinding(hasVertexAnim, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Check to see if this TEntity's buffers are marked for vertex animation or not.
	ENDREM
	Method _getBuffersMarkedForAnimation:Int() 
		Return ENT__getBuffersMarkedForAnimation(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Mark this TEntity's vertex data as animated.
	ENDREM
	Method _markBuffersUsedForAnimation() 
		ENT__markBuffersUsedForAnimation(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Check to see if this TEntity has been initialised or not.
	ENDREM
	Method isInitialised:Int() 
		Return ENT_isInitialised(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Attempt to initialise this TEntity manually.<br>forceReinitialise:Int If the TEntity is already initialised, do you want to reinitialise it?
	ENDREM
	Method _initialise(forceReinitialise:Byte = False) 
		ENT__initialise(forceReinitialise, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: _deinitialise this TEntity manually.
	ENDREM
	Method _deinitialise() 
		ENT__deinitialise(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Notify this TEntity that a delay-loaded TMesh is now loaded.
	ENDREM
	Method backgroundLoadingComplete(res:TResource) 
		ENT_backgroundLoadingComplete(res.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Entities are managed by Ogre and thus have no GC enabled destructor, however you can destroy an entity manually if you feel the need with this destroy method.
	ENDREM
	Method destroy()
		ENT_delete( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Helper function to create a TEntity from a reference/pointer.
	ENDREM
	Function FromPtr:TEntity(Pointer:Byte Ptr) 
		Local Obj:TEntity = New TEntity
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
	
	
End Type

Rem
	bbdoc: The type that describes dynamic light sources in a scene.
ENDREM
Type TLight Extends TMovableObject
	
	Rem
		bbdoc: Point Light Constant.
	ENDREM
	Const LT_POINT:Int = 0
	Rem
		bbdoc: Directional Light Constant.
	END REM
	Const LT_DIRECTIONAL:Int = 1
	Rem
		bbdoc: Spot Light Constant.
	ENDREM
	Const LT_SPOTLIGHT:Int   = 2
	
	Rem
		bbdoc: Keep type information consistent.
	ENDREM
	Method New()
		Self.vartype = 2	
	End Method

	
	Rem
		bbdoc: Sets the light type to one of the basic types of lighting: </BR> "TLight.LT_POINT"  An omni light projecting in all directions equally. </Br> "TLight.LT_DIRECTIONAL" - Lighting with direction but no true position. A frustum of light. </BR> "TLight.LT_SPOTLIGHT" - A cone of light.
	ENDREM
	Method setType(lightType:Int) 
		LIGHT_setType( lightType , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the light type for this TLight.  LT_POINT = 0 , LT_DIRECTIONAL = 1 , LT_SPOTLIGHT = 2. 
	ENDREM
	Method getType:Int() 
		Return LIGHT_getType( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the rgb colour of the light diffused from this light source. Ie - The color of the light.
	ENDREM
	Method setDiffuseColour(r:Float, g:Float, b:Float) 
		LIGHT_setDiffuseColour( r , g , b , Self.Pointer )	
	End Method
	
	Rem
		bbdoc: Sets the colour of the light diffused from this light source. Ie - The colour of the light. This method uses a TColourValue instead of seperate rgb float components.
	ENDREM
	Method setDiffuseColourWithColourValue(colour:TColourValue) 
		LIGHT_setDiffuseColourWithColourValue( colour.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the current diffuse colour of this TLight as a TColourValue.
	ENDREM
	Method getDiffuseColour:TColourValue() 
		Return TColourValue.FromPtr(LIGHT_getDiffuseColour(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Sets the rgb colour of the specular light given off by this TLight.
	ENDREM
	Method setSpecularColour(r:Float, g:Float, b:Float) 
		LIGHT_setSpecularColour( r , g , b , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the colour of the specular light from this light source.
	ENDREM
	Method setSpecularColourWithColourValue(colour:TColourValue) 
		LIGHT_setSpecularColourWithColourValue( colour.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the current diffuse colour of this TLight as a TColourValue.
	ENDREM
	Method getSpecularColour:TColourValue() 
		Return TColourValue.FromPtr(LIGHT_getSpecularColour(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Set light attenuation parameters. These parameters describe how light diminishes or loses focus with distance.<br>range:Float The absolute farthest that this light can be cast in world units.<br>constant:Float Degree of attenuation. 0.0 means no attenuation, 1.0 means full attenuation. <br>Linear:Float How evenly to attenuate the light. 1 means to attenuate evenly.<br>quadratic:Float The curvature of the attenuation.
	ENDREM
	Method setAttenuation(range:Float, constant:Float, linear:Float, quadratic:Float) 
		LIGHT_setAttenuation( range , constant , linear , quadratic , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns the range of this light's attenuation settings. This specifies how far the light can travel in world units.
	ENDREM
	Method getAttenuationRange:Float() 
		Return LIGHT_getAttenuationRange( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns the constant of this light's attenuation settings. This specifies how much attenuation affects the TLight.
	ENDREM
	Method getAttenuationConstant:Float() 
		Return LIGHT_getAttenuationConstant( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns the linear component of this light's attenuation settings. This specifies how eventually the attenuation effects are spread acrossed the distance of the TLight.
	ENDREM
	Method getAttenuationLinear:Float() 
		Return LIGHT_getAttenuationLinear( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns the quadratic component of this light's attenuation settings. This specifies the curvature of the attenuation.
	ENDREM
	Method getAttenuationQuadric:Float() 
		Return LIGHT_getAttenuationQuadric( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the xyz position of the TLight. This will not work for LT_DIRECTIONAL lights as they have no position.
	ENDREM
	Method setPosition(x:Float, y:Float, z:Float) 
		LIGHT_setPosition( x , y , z , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the position of this TLight with a TVector3.  This will not work for LT_DIRECTIONAL lights as they have no position.
	ENDREM
	Method setPositionWithTVector3(vec:TVector3) 
		LIGHT_setPositionWithVector3( vec.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the position of this TLight as a TVector3. This will not work for LT_DIRECTIONAL lights as they have no position.
	ENDREM
	Method getPosition:TVector3() 
		Return TVector3.FromPtr(LIGHT_getPosition(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Sets the xyz direction that a TLight points. 
	ENDREM
	Method setDirection(x:Float, y:Float, z:Float) 
		LIGHT_setDirection( x , y , z , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the direction that a TLight points using a TVector3.
	ENDREM
	Method setDirectionWithVector3(vec:TVector3) 
		LIGHT_setDirectionWithVector3( vec.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the direction this TLight is pointing as a TVector3.
	ENDREM
	Method getDirection:TVector3() 
		Return TVector3.FromPtr(LIGHT_getDirection(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc:Sets the general range of a LT_SPOTLIGHT.<br>innerAngle:TRadian The angle of the inner cone of the LT_SPOTLIGHT.<br>outerAngle:TRadian The angle of the outer cone of the LT_SPOTLIGHT.<br>falloff:Float The rate of attenuation. 1.0 is linear , less means less falloff, more means more falloff.
	ENDREM
	Method setSpotlightRange(innerAngle:TRadian, outerAngle:TRadian, falloff:Float = 1.0) 
		LIGHT_setSpotlightRange( innerAngle.Pointer , outerAngle.Pointer , falloff , Self.Pointer )		
	End Method
	
	Rem
		bbdoc: Get the angle of the inner cone of this LT_SPOTLIGHT in radians.
	ENDREM
	Method getSpotlightInnerAngle:TRadian() 
		Local Obj:TRadian = New TRadian
		Obj.Pointer = LIGHT_getSpotlightInnerAngle( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the angle of the outer cone of this LT_SPOTLIGHT in radians.
	ENDREM
	'@returns: A TRadian containing the angle of the outer cone.
	Method getSpotlightOuterAngle:TRadian()
		Local Obj:TRadian = New TRadian
		Obj.Pointer = LIGHT_getSpotlightOuterAngle( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the falloff of this LT_SPOTLIGHT.
	ENDREM
	Method getSpotlightFalloff:Float() 
		Return LIGHT_getSpotlightFalloff( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the inner angle of the light cone for an LT_SPOTLIGHT with radians.
	ENDREM
	Method setSpotlightInnerAngle(val:TRadian) 
		LIGHT_setSpotlightInnerAngle( val.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the outer angle of the light cone for an LT_SPOTLIGHT with radians.
	ENDREM
	Method setSpotlightOuterAngle(val:TRadian) 
		LIGHT_setSpotlightOuterAngle( val.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the falloff of this LT_SPOTLIGHT.
	ENDREM
	Method setSpotlightFalloff(val:Float) 
		LIGHT_setSpotlightFalloff( val , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the relative power scale of this TLight.
	ENDREM
	Method setPowerScale(power:Float) 
		LIGHT_setPowerScale( power , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the relative power scale of this TLight.
	ENDREM
	Method getPowerScale:Float() 
		Return LIGHT_getPowerScale( Self.Pointer )
	End Method
	
	Rem
		bbdoc: INTERNAL - Tell this TMovableObject that it has been attached to a TOgreNode.<br>parent:TOgreNode The TOgreNode that this object is told it is attached to.<br>Specifies whether or not parent:TOgreNode is a TTagPoint.
	ENDREM
	Method _notifyAttached(parent:TOgreNode, isTagPoint:Int = False) 
		LIGHT__notifyAttached( parent.Pointer , isTagPoint , Self.Pointer )
	End Method
	
	Rem
		bbdoc: INTERNAL - Tell this TMovableObject that it's position has changed in some way.
	ENDREM
	Method _notifyMoved()
		LIGHT__notifyMoved( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the local bounding box for this object.
	ENDREM
	Method getBoundingBox:TAxisAlignedBox() 
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = LIGHT_getBoundingBox(Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: INTERNAL - Internal call that takes place when this object is to be rendered. This causes the object to create TRenderableOnstances on the TRenderQueue given.
	ENDREM
	Method _updateRenderQueue(queue:TRenderQueue) 
		LIGHT__updateRenderQueue( queue.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the TMovableObject child class type for this object and return it as a string.
	ENDREM
	Method getMovableType:String() 
		Local temp$
		Return temp.fromcstring( LIGHT_getMovableType( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Gets the position of the light along as affected by any transforms it might be attached to.
	ENDREM
	Method getDerivedPosition:TVector3() 
		Return TVector3.FromPtr(LIGHT_getDerivedPosition(Self.Pointer)) 	
	End Method
	
	Rem
		bbdoc: Gets the direction of the light along as affected by any transforms it might be attached to.
	ENDREM
	Method getDerivedDirection:TVector3() 
		Return TVector3.FromPtr(LIGHT_getDerivedDirection(Self.Pointer)) 	
	End Method
	
	Rem
		bbdoc: Set this object to visible or invisible if it can be.
	ENDREM
	Method setVisible(visible:Byte)
		LIGHT_setVisible( visible , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the centered bounding radius for this object.
	ENDREM
	Method getBoundingRadius:Float() 
		Return LIGHT_getBoundingRadius( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get a detailed description of this light as a TVector4.
	ENDREM
	Method getAs4DVector:TVector4() 
		Return TVector4.FromPtr(LIGHT_getAs4DVector(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: INTERNAL - Used by Ogre to calculate the near clip volume between the camera and the light.
	ENDREM
	Method _getNearClipVolume:TPlaneBoundedVolume(cam:TCamera) 
		Local Obj:TPlaneBoundedVolume = New TPlaneBoundedVolume
		Obj.Pointer = LIGHT__getNearClipVolume( cam.Pointer , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: INTERNAL - Method for calculating clip volumes outside of the camera's frustum to determine what object's are casting shadows insidethe view frustum.
	ENDREM
	Method _getFrustumClipVolumes:TPlaneBoundedVolumeList(cam:TCamera) 
		Local Obj:TPlaneBoundedVolumeList = New TPlaneBoundedVolumeList
		Obj.Pointer = LIGHT__getFrustumClipVolumes( cam.Pointer , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get this TMovableObject type's specific bitpattern for use in inclusion/exclusion in scenequeries.
	ENDREM
	Method getTypeFlags:Int() 
		Return LIGHT_getTypeFlags( Self.Pointer )
	End Method
	
	Rem
		bbdoc: You can use the returned value here to animate this TLight in some way.
	ENDREM
	Method createAnimableValue:TAnimableValue(valueName:String) 
		Local Obj:TAnimableValue = New TAnimableValue
		Obj.Pointer = LIGHT_createAnimableValue( valueName.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: You can set a custom shadow camera for when you are rendering shadows with this TLight with this function.
	ENDREM
	Method setCustomShadowCameraSetup(customShadowSetup:TShadowCameraSetup) 
		LIGHT_setCustomShadowCameraSetup( customShadowSetup.Pointer , Self.Pointer )		
	End Method
	
	Rem
		bbdoc: Reset to default shadow camera.
	ENDREM
	Method resetCustomShadowCameraSetup()
		LIGHT_resetCustomShadowCameraSetup( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the current custom shadow camera setup for this TLight.
	ENDREM
	Method getCustomShadowCameraSetup:TShadowCameraSetup() 
		Local Obj:TShadowCameraSetup = New TShadowCameraSetup
		Obj.Pointer = LIGHT_getCustomShadowCameraSetup( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Helper function to create a TLight from a reference/pointer.
	ENDREM
	Function FromPtr:TLight(Pointer:Byte Ptr) 
		Local Obj:TLight = New TLight
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
End Type

Rem
	bbdoc: Type for defining camera objects in the scene.
ENDREM
Type TCamera Extends TMovableObject
	
	Rem
		bbdoc: Make sure type data is propogated.
	ENDREM
	Method New()
		Self.vartype = 2
	End Method
	
	Rem
		bbdoc: Get the field of view for the y-axis on this camera.	Parameter is a TRadian.
	ENDREM
	Method setFOVy(fovy:TRadian)
		CAM_setFOVy(fovy.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the field of view for the y-axis on this camera.	Value is a TRadian.
	ENDREM
	Method getFOVy:TRadian()
		Local Obj:TRadian = New TRadian
		Obj.Pointer = CAM_getFOVy(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the scene manager that this camera resides in.
	ENDREM
	Method getSceneManager:TSceneManager()
		Local Obj:TSceneManager = New TSceneManager
		Obj.Pointer = CAM_getSceneManager( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Sets the polygon mode to one of the three constants: PM_POINTS , PM_WIREFRAME , PM_SOLID 
	ENDREM
	Method setPolygonMode( polygonMode:Int )
		CAM_setPolygonMode( polygonMode , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the polygon mode that this camera renders in: PM_POINTS , PM_WIREFRAME , PM_SOLID
	ENDREM
	Method getPolygonMode:Int()
		Return CAM_getPolygonMode( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set this camera's XYZ position in the scene.
	ENDREM
	Method setPosition( x:Float , y:Float , z:Float )
		CAM_setPosition( x , y , z , Self.Pointer )		
	End Method
	
	Rem
		bbdoc: Get this camera's XYZ position as a TVector3.
	ENDREM
	Method getPosition:TVector3() 
		Return TVector3.FromPtr(CAM_getPosition(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Translate this camera from it's current position with XYZ offset coordinates.
	ENDREM
	Method move(vec:TVector3) 
		CAM_move(vec.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the up vector for this camera as a TVector3.
	ENDREM
	Method getUp:TVector3() 
		Return TVector3.FromPtr(CAM_getUp(Self.Pointer)) 
	End Method

	Rem
		bbdoc: Set this camera to "Look" at a given point in world space.
	ENDREM
	Method lookAt(x:Float, y:Float, z:Float) 
		CAM_lookAt( x , y , z , Self.Pointer )		
	End Method
	
	Rem
		bbdoc: Set this camera to "Look" at a given point in world space specified by a TVector3.
	ENDREM
	Method lookAtWithVector3(targetPoint:TVector3) 
		CAM_lookAtWithVector3(targetPoint.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: 	Set the near clipping plane's distance from the camera. The distance is in world units.
	ENDREM
	Method setNearClipDistance( NearClipDistance:Float )
		CAM_setNearClipDistance(NearClipDistance, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: 	Get the near clipping plane's distance from the camera. The distance is in world units.
	ENDREM
	Method getNearClipDistance:Float()
		Return CAM_getNearClipDistance(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the far clipping plane's distance from the camera. The distance is in world units.
	ENDREM
	Method setFarClipDistance (FarClipDistance:Float)
		CAM_setFarClipDistance( FarClipDistance , Self.Pointer )
	End Method
	
	Rem
		bbdoc: 	Get the far clipping plane's distance from the camera. The distance is in world units.
	ENDREM
	Method getFarClipDistance:Float()
		Return CAM_getFarClipDistance(Self.Pointer)
	End Method
	
	Rem
		bbdoc:	Set the aspect ratio that this camera will display in.
	ENDREM
	Method setAspectRatio ( AspectRatio:Float )
		CAM_setAspectRatio( AspectRatio , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the aspect ratio of this camera.
	ENDREM
	Method getAspectRatio:Float()
		Return CAM_getAspectRatio(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set this camera's yaw with a radian value. Use deg2rad( 180 ) to specify in degrees.
	ENDREM
	Method yaw( ogreRadian:Float )
		CAM_yaw( ogreRadian , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set this camera's pitch with a radian value.Use deg2rad( 180 ) to specify in degrees. 
	ENDREM
	Method pitch( ogreRadian:Float )
		CAM_pitch( ogreRadian , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set this camera's roll with a radian value.Use deg2rad( 180 ) to specify in degrees.  
	ENDREM
	Method roll( ogreRadian:Float )
		CAM_roll( ogreRadian , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get a camera to viewport ray by specifying the relative viewport coordinates.<br/>posx:Float and posy:Float - Relative x and y screen coordinates. As an example, if you wanted to get the center of the viewport, you would specify 0.5,0.5.<br/>A Tray that you have initialised. Once getCameraToViewportRay(...) is done it will store the resulting ray into this TRay instance.
	ENDREM
	Method getCameraToViewportRay( posx:Float , posy:Float , ogreRay:TRay )
		CAM_getCameraToViewportRay( posx , posy , ogreRay.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether you want this camera to auto-track a given node. The camera will track the specified node no matter how the camera or the scene node move.<br/>x:Float , y:Float , z:Float - The local offset, in case you want to look a little to the left, right, etc of the tracked node. Please note that you need to turn auto tracking off before deleting the given scene node.
	ENDREM
	Method setAutoTracking( enabled:Int , ogreSceneNode:TSceneNode , x:Float , y:Float , z:Float )
		CAM_setAutoTracking( enabled , ogreSceneNode.Pointer , x , y , z , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the orientation of this TCamera.
	ENDREM
	Method setOrientation(ogreQuaternion:TQuaternion) 
		CAM_setOrientation( ogreQuaternion.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the view matrix for this frustum. You probably won't need to use this.
	ENDREM
	Method getViewMatrix:TMatrix4()
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = CAM_getViewMatrix( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Sets the viewing window's dimensions and position.
	ENDREM
	Method setWindow(x:Float, y:Float, width:Float, height:Float) 
		CAM_setWindow(x, y, width, height, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the TViewport this camera is currently attached to.
	ENDREM
	Method GetViewport:TViewport() 
		Local Obj:TViewport = New TViewport
		Obj.Pointer = CAM_getViewport(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Helper function to create a TCamera from a reference/pointer.
	ENDREM
	Function FromPtr:TCamera(Pointer:Byte Ptr) 
		Local Obj:TCamera = New TCamera
		Obj.Pointer = Pointer
		Return Obj
	End Function
	


End Type

Rem
	bbdoc: Class to define and manipulate particle based systems. TParticleSystems are not to be created directly, but through the use of a TSceneManager.
ENDREM
Type TParticleSystem Extends TMovableObject

	Rem
		bbdoc: Propogate type values.
	ENDREM
	Method New()
		Self.vartype = 5
	End Method
	
	Rem
		bbdoc: Sets the type of TParticleSystemRenderer to be used for this TParticleSystem. The type in use's factory must be registered with the TParticleSystemManager.
	ENDREM 
	Method setRenderer(typeName:String) 
		PS_setRenderer( typeName.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the current TParticleSystemRenderer for this TParticleSystem.
	ENDREM
	Method getRenderer:TParticleSystemRenderer() 
		Local Obj:TParticleSystemRenderer = New TParticleSystemRenderer
		Obj.Pointer = PS_getRenderer( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the TParticleSystemRenderer responsible for rendering this TParticleSystem's name.
	ENDREM
	Method getRendererName:String() 
		Local tempString:String
		Return tempString.FromCString( PS_getRendererName( Self.Pointer ))
	End Method
	
	Rem
		bbdoc: Adds an emitter that can emit particles into this TParticleSystem.<br>emitterType:String Type of emitter as governed by the types of registered TParticlEmitterFactory objects for this TRenderSystem. Your stock options from the ParticleFX plugin are as follows:</BR> "Point" - Emit particles from a single point. </BR> "Box" - Emit particles somewhere within a 3d Cube. </BR> "Cylinder" - Emit particles within a cylindrical area. </BR> "Ellipsoid" - Emit particles from within an ellipsoidial area. </BR> "Hollow Ellipsoid" - Similar to the "Ellipsoid emitter, except there is an area in the center that is hollow in which no particles are emitted. </BR> "Ring" - Emits particles from a ring-shaped area. </BR> "Emitting" - An emitter that emits more emitters.
	ENDREM
	Method addEmitter:TParticleEmitter(emitterType:String) 
		Local Obj:TParticleEmitter = New TParticleEmitter
		Obj.Pointer = PS_addEmitter( emitterType.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets an emitter in this TParticleSystem by index, starting with zero.<br>index:Int The index of the TParticleEmitter you want returned. Starts at zero.
	ENDREM
	Method getEmitter:TParticleEmitter(index:Int) 
		Local Obj:TParticleEmitter = New TParticleEmitter
		Obj.Pointer = PS_getEmitter( index , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets the number of emitters currently in the TParticleSystem.
	ENDREM
	Method getNumEmitters:Int() 
		Return PS_getNumEmitters( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Removes a TEmitter from this TParticleSystem.
	ENDREM
	Method removeEmitter(index:Int) 
		PS_removeEmitter( index , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Removes ALL TEmitters from this TParticleSystem.
	ENDREM
	Method removeAllEmitters()
		PS_removeAllEmitters( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Adds a TParticleAffector ( an affector ) to this TParticleSystem. Particle Affectors modify the particles or their behavior within the particle system, such as pushing them around, or shrinking them.<br>affectorType:String The type of factory you want to be used to create the TParticleAffector.  The stock affectors that come with the particleFX plugin are listed below: </BR> "Linear Force" - Applies for along a vector to the particles. </BR> "ColourFader" - Change the RGBA values of particles affected. </BR> "ColourFader2" - Same as "ColourFader" except oscillates between two different colours. </BR> "Scaler" - Scales the particles affected. </BR> "Rotator" - Rotates all particles affected. </BR> "ColourInterpolater" - Like "ColourFader2" except has many stages, so use this if you need to roam between more than two colours. </BR> "ColourImage" - Colours particles based on an image file. Neat! </BR> "DeflectorPlane" - Basic collision plane for particles.
	ENDREM
	Method addAffector:TParticleAffector(affectorType:String) 
		Local Obj:TParticleAffector = New TParticleAffector
		Obj.Pointer = PS_addAffector( affectorType.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Returns a TParticleAffector with the given index ( starts at 0 ) for this TParticleSystem.
	ENDREM
	Method getAffector:TParticleAffector(index:Int) 
		Local Obj:TParticleAffector = New TParticleAffector
		Obj.Pointer = PS_getAffector( index , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Returns the number of TParticleAffectors in this TParticleSystem.
	ENDREM
	Method getNumAffectors:Int() 
		Return PS_getNumAffectors( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Removes a TParticleAffector from this TParticleSystem by it's index. Starts at zero.
	ENDREM
	Method removeAffector(index:Int) 
		PS_removeAffector( index , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Removes all TParticleAffectors from this TParticleSystem.
	ENDREM
	Method removeAllAffectors()
		PS_removeAllAffectors( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Removes all particles from this TParticleSystem.
	ENDREM
	Method clear()
		PS_clear( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns the number of particles currently in this TParticleSystem.
	ENDREM
	Method getNumParticles:Int() 
		Return PS_getNumParticles( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Creates a single particle within the TParticleSystem. You must check that TParticle.Pointer is not NULL, as there are a few conditions that could cause this.<br/>1. You must have called fG.renderWorld() , scenemanager.renderOneFrame() or , scenemanager.startRendering() once since the particle system was placed in your scene.<br/>2. The particle quota must be greater than the number of particles. You can get these numbers in run-time by doing particlesystem.getParticleQuota() and particlesystem.getNumParticles().<br/>After calling this you will need to init the particle settings.
	ENDREM
	Method createParticle:TParticle() 
		Local Obj:TParticle = New TParticle
		Obj.Pointer = PS_createParticle( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Creates a single particle that emits other particles within this TParticleSystem.
	ENDREM
	Method createEmitterParticle:TParticle(emitterName:String) 
		Local Obj:TParticle = New TParticle
		Obj.Pointer = PS_createEmitterParticle( emitterName.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets a particle from the TParticleSystem via index, starting with zero.<br/>Normally you should consider using an affector if you want to modify how a particle moves.
	ENDREM
	Method getParticle:TParticle(index:Int) 
		Local Obj:TParticle = New TParticle
		Obj.Pointer = PS_getParticle( index , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the maximum number of particles allowed for this TParticleSystem.
	ENDREM
	Method getParticleQuota:Int() 
		Return PS_getParticleQuota( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the maximum number of particles allowed for this TParticleSystem.
	ENDREM
	Method setParticleQuota(quota:Int) 
		PS_setParticleQuota( quota , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the maximum number of emitted emitters this TParticleSystem is allowed to have.
	ENDREM
	Method getEmittedEmitterQuota:Int() 
		Return PS_getEmittedEmitterQuota( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the maximum number of emitted emitters this TParticleSystem is allowed to have.
	ENDREM
	Method setEmittedEmittersQuota(quota:Int) 
		PS_setEmittedEmitterQuota( quota , Self.Pointer )
	End Method
	
	Rem
		bbdoc: = Substitute. When you use ps1.eq(ps2) then ps2's emitters and affectors will be copied over to ps1.
	ENDREM
	Method Eq(b:TParticleSystem) 
		PS_eq(Self.Pointer, b.Pointer) 
	End Method
	
	Rem
		bbdoc: Updates the particles in this TParticleSystem by the amount of time elapsed in seconds.
	ENDREM
	Method _Update(timeElapsed:Float) 
		PS__update( timeElapsed , Self.Pointer )
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED: Get an easy iterator to allow you to iterate through particles in the system.
	ENDREM
	Method _getIterator:TParticleIterator() 
		Local Obj:TParticleIterator = New TParticleIterator
		Obj.Pointer = PS__getIterator( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Sets the name of the material to be used for this TParticleSystem.
	ENDREM
	Method setMaterialName(Name:String) 
		PS_setMaterialName( name.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the name of the material being used for this TParticleSystem.
	ENDREM
	Method getMaterialName:String() 
		Local tempString:String
		Return tempString.FromCString( PS_getMaterialName( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Notify this TParticleSystem what camera is currently being used to render it.
	ENDREM
	Method _notifyCurrentCamera(cam:TCamera) 
		PS__notifyCurrentCamera( cam.Pointer , Self.Pointer )		
	End Method
	
	Rem
		bbdoc: INTERNAL - Tell this TParticleSystem  that it has been attached to a TOgreNode.<br>parent:TOgreNode The TOgreNode that this object is told it is attached to.<br>Specifies whether or not parent:TOgreNode is a TTagPoint.
	ENDREM

	Method _notifyAttached( parent:TOgreNode , isTagPoint:Int = False )
		PS__notifyAttached( parent.Pointer , isTagPoint , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the local bounding box for this object.
	ENDREM
	Method getBoundingBox:TAxisAlignedBox() 
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = PS_getBoundingBox(Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the centered bounding radius for this object.
	ENDREM
	Method getBoundingRadius:Float() 
		Return PS_getBoundingRadius( Self.Pointer )
	End Method
	
	Rem
		bbdoc: INTERNAL - Internal call that takes place when this object is to be rendered. This causes the object to create TRenderableOnstances on the TRenderQueue given.
	ENDREM
	Method _updateRenderQueue(queue:TRenderQueue) 
		PS__updateRenderQueue( queue.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Fast forward the TParticleSystem x number of seconds.<br> time:Float Number of seconds to fast forward.<br>interval:Float The interval that describes how many iterations the fast foreward will make. Smaller values give a more thorough fast foreward as they require more iterations.
	ENDREM
	Method fastForward(time:Float, interval:Float = 0.1) 
		PS_fastForward( time , interval , Self.Pointer )	
	End Method
	
	Rem
		bbdoc: The ParticleSystem speed factor is how the particle system scales it's time system calculations. Use this to make the TParticleSystem operate faster or slower.
	ENDREM
	Method setSpeedFactor(speedFactor:Float) 
		PS_setSpeedFactor( speedFactor , Self.Pointer )
	End Method
	
	Rem
		bbdoc: The ParticleSystem speed factor is how the particle system scales it's time system calculations. Use this to get the speedFactor of the particle system.
	ENDREM
	Method getSpeedFactor:Float() 
		Return PS_getSpeedFactor( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Use this to scale particle system time by elapsed framerate, causing the TParticleSystem to operate at the same speed regardless of framerate. 
	ENDREM
	Method setIterationInterval(iterationInterval:Float) 
		PS_setIterationInterval( iterationInterval , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Represents how often the TParticleSystem will perform an iteration against elapsed time per frame. 
	ENDREM
	Method getIterationInterval:Float() 
		Return PS_getIterationInterval( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets how long it should take for the TParticleSystem to stop updating after it has not been visible.
	ENDREM
	Method setNonVisibleUpdateTimeout(timeout:Float) 
		PS_setNonVisibleUpdateTimeout(timeout, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets how long it should take for the TParticleSystem to stop updating after it has not been visible.
	ENDREM
	Method getNonVisibleUpdateTimeout:Float() 
		Return PS_getNonVisibleUpdateTimeout( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the TMovableObject child class type for this object and return it as a string.
	ENDREM
	Method getMovableType:String() 
		Local temp:String
		Return temp.fromcstring( PS_getMovableType( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: INTERNAL - Notifies particle parent that particle has been resized. Callback used by TParticles.
	ENDREM
	Method _notifyParticleResized()
		PS__notifyParticleResized( Self.Pointer )
	End Method
	
	Rem
		bbdoc: INTERNAL - Notifies particle parent that particle has been rotated. Callback used by TParticles.
	ENDREM
	Method _notifyParticleRotated()
		PS__notifyParticleRotated( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the default dimensions of the particles in this TRenderSystem.
	ENDREM
	Method setDefaultDimensions(width:Float, Height:Float) 
		PS_setDefaultDimensions( width , height , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the default width of the particles in this TRenderSystem.
	ENDREM
	Method setDefaultWidth(width:Float) 
		PS_setDefaultWidth( width , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the default width of the particles in this TRenderSystem.
	ENDREM
	Method getDefaultWidth:Float() 
		Return PS_getDefaultWidth( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the default height of the particles in this TRenderSystem.
	ENDREM
	Method setDefaultHeight(Height:Float) 
		PS_setDefaultHeight( height , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the default height of the particles in this TRenderSystem.
	ENDREM
	Method getDefaultHeight:Float() 
		Return PS_getDefaultHeight( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get whether or not the particles are tested individually to see if they should be culled.
	ENDREM
	Method getCullIndividually:Int() 
		Return PS_getCullIndividually( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether or not the particles are tested individually to see if they should be culled.
	ENDREM
	Method setCullIndividually(cullIndividual:Int) 
		PS_setCullIndividually( cullIndividual , Self.Pointer )
	End Method 
	
	Rem
		bbdoc: Gets this TParticleSystem's resource group name.
	ENDREM
	Method getResourceGroupName:String() 
		Local temp:String
		Return temp.FromCString( PS_getResourceGroupName( Self.Pointer )  )
	End Method
	
	Rem
		bbdoc: Get where this TParticleSystem was created from.
	ENDREM
	Method GetOrigin:String() 
		Local temp:String
		Return temp.FromCString( PS_getOrigin( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Notify this TParticleSystem of it's origin.
	ENDREM
	Method _notifyOrigin(origin:String) 
		PS__notifyOrigin( origin.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets a renderqueue group for this object, allowing it to be rendered earlier or later than other objects.</br> Const RENDER_QUEUE_BACKGROUND:Int = 0 </br>Const RENDER_QUEUE_SKIES_EARLY:Int = 5 </br>Const RENDER_QUEUE_1:Int = 10 </br>Const RENDER_QUEUE_2:Int = 20 </br>Const RENDER_QUEUE_WORLD_GEOMETRY_1:Int = 25 </br>Const RENDER_QUEUE_3:Int = 30 </br>Const RENDER_QUEUE_4:Int = 40 </br>Const RENDER_QUEUE_MAIN:Int = 50 </br>Const RENDER_QUEUE_6:Int = 60 </br>Const RENDER_QUEUE_7:Int = 70 </br>Const RENDER_QUEUE_WORLD_GEOMETRY_2:Int = 75 </br>Const RENDER_QUEUE_8:Int = 80 </br>Const RENDER_QUEUE_9:Int = 90 </br>Const RENDER_QUEUE_SKIES_LATE:Int = 95 </br>Const RENDER_QUEUE_OVERLAY:Int = 100 </br></br> Where 50 is the default render queue for most TMovableObject derivatives. 0 is Drawn first, 100 is drawn last.
	ENDREM
	Method setRenderQueueGroup(queueID:Int) 
		PS_setRenderQueueGroup(queueID, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set if particles are sorted from the TCamera's perspective.
	ENDREM
	Method setSortingEnabled(enabled:Int) 
		PS_setSortingEnabled( enabled , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get if particles are sorted from the TCamera's perspective.
	ENDREM
	Method getSortingEnabled:Int() 
		Return PS_getSortingEnabled( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the initial bounds of the TParticleSystem.
	ENDREM
	Method setBounds(aabb:TAxisAlignedBox) 
		PS_setBounds( aabb.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether bounds are automatically updated for this TParticleSystem.<br>stopIn:Float If autoUpdate is true,the number of seconds after the autoupdate will stop.
	ENDREM
	Method setBoundsAutoUpdated(autoUpdate:Int, stopIn:Float = 0.0) 
		PS_setBoundsAutoUpdated( autoUpdate , stopIn , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set whether particle coordinates remain in local space with the node in which they are attached being the origin.
	ENDREM
	Method setKeepParticlesInLocalSpace(keepLocal:Int) 
		PS_setKeepParticlesInLocalSpace( keepLocal , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get whether particle coordinates remain in local space with the node in which they are attached being the origin.
	ENDREM
	Method getKeepParticlesInLocalSpace:Int() 
		Return PS_getKeepParticlesInLocalSpace( Self.Pointer )
	End Method
	
	Rem
		bbdoc: INTERNAL - For updating the boundaries of the TParticleSystem.
	ENDREM
	Method _updateBounds()
		PS__updateBounds( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get this TMovableObject type's specific bitpattern for use in inclusion/exclusion in scenequeries.
	ENDREM
	Method getTypeFlags:Int() 
		Return PS_getTypeFlags(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the Default Iteration Interval for ALL TParticleSystems.	This basically sets when each system will be updated.<br/> Value is in seconds.
	ENDREM
	Function setDefaultIterationInterval(iterationInterval:Float) 
		PS_setDefaultIterationInterval( iterationInterval )
	End Function
	
	Rem
		bbdoc: Get the Default Iteration Interval for ALL TParticleSystems. Value is in seconds.
	ENDREM
	Function getDefaultIterationInterval:Float() 
		Return PS_getDefaultIterationInterval()
	End Function
	
	Rem
		bbdoc: Set the default timeout for how long objects have to be out of visibility in order to stop updating ( in seconds ).
	ENDREM
	Function setDefaultNonVisibleUpdateTimeout(timeout:Float) 
		PS_setDefaultNonVisibleUpdateTimeout( timeout )
	End Function
	
	Rem
		bbdoc: Get the default timeout for how long objects have to be out of visibility in order to stop updating ( in seconds ).
	ENDREM
	Function getDefaultNonVisibleUpdateTimeout:Float() 
		Return PS_getDefaultNonVisibleUpdateTimeout()
	End Function
	
	'-----------------------
	'From TStringInterface
	'-----------------------
	
	Rem
		bbdoc: NOT SUPPORTED: Retreives the list of available parameters for this interface.
	ENDREM
	Method getParamDictionary:TParamDictionary() 
		Local Obj:TParamDictionary = New TParamDictionary
		Obj.Pointer = STRINT_getParamDictionary(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED: Gets a list of useable parameters for this object.
	ENDREM
	Method getParameters:TParameterList()
		Local Obj:TParameterList = New TParameterList
		Obj.Pointer = STRINT_getParameters(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: General method for setting parameters for this object. Returns true if setting the parameter was successful.
	ENDREM
	Method setParameter:Int(Name:String, Value:String) 
		Return STRINT_setParameter(name.ToCString() , value.ToCString() , Self.Pointer)
	End Method
	
	Rem
		bbdoc: Generic method for setting multiple parameters for this object.
	ENDREM
	Method setParameterList(paramList:TNameValuePairList) 
		STRINT_setParameterList(paramList.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: General method for retreiving the value of a parameter specified.
	ENDREM
	Method getParameter:String(Name:String) 
		Return String.FromCString(STRINT_getParameter(name.ToCString() , Self.Pointer))
	End Method
	
	Rem
		bbdoc: Method for copying this TStringInterface's parameters to another TStringInterface.<br/>Note: Unfortunately, TStringInterface is a method that is often used with multiple inheritance, which BlitzMax does not support. If you know that your object is derived from StringInterface in Ogre but it is not in Flow3D, you may need to do something like this:  originalObject.copyParametersTo( TStringInterface.FromPtr( objectToCopy.Pointer ) ).
	ENDREM
	Method copyParametersTo(dest:TStringInterface) 
		STRINT_copyParametersTo(dest.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Cleans up so that it's containers are not left with invalid pointers. Required if you wish to reset Ogre.
	ENDREM
	Function cleanupDictionary()
		STRINT_cleanupDictionary() 
	End Function
	
	Rem
		bbdoc: Because we cannot do double-inheritance, TParticleSystem cannot be derived from TStringInterface cleanly, so we allow it to be converted here.
	ENDREM
	Method toTStringInterface:TStringInterface()
		Local Obj:TStringInterface = New TStringInterface
		Obj.Pointer = Self.Pointer
		Return Obj
	End Method
	
	
	
	'-----------------------
	'From TMovableObject
	'-----------------------
	
	Rem
		bbdoc: INTERNAL - Notify this TMovableObject of the factory interface it was created by.<br/>fact:TMovableObject The TMovableObjectFactory that this object is to be told it was created by.
	ENDREM
	Method _notifyCreator(fact:TMovableObjectFactory)
		PS__notifyCreator(fact.Pointer, Self.Pointer)
	End Method

	Rem
		bbdoc: INTERNAL - Get the factory interface that created this TMovableObject.
	ENDREM
	Method _getCreator:TMovableObjectFactory() 
		Local Obj:TMovableObjectFactory = New TMovableObjectFactory
		Obj.Pointer = PS__getCreator(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: INTERNAL - Notify this TMovableObject of the TSceneManager that created it.
	ENDREM
	Method _notifyManager(man:TSceneManager) 
		PS__notifyManager(man.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: INTERNAL - Get the TSceneManager that governs this object.
	ENDREM
	Method _getManager:TSceneManager() 
		Local Obj:TSceneManager = New TSceneManager
		Obj.Pointer = PS__getManager(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the name of this object.
	ENDREM
	Method getName:String() 
		Local temp:String
		Return temp.FromCString(PS_getName(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Get the TOgreNode that this object is attached to.
	ENDREM
	Method getParentNode:TOgreNode() 
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = PS_getParentNode(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the TSceneNode that this object is attached to. 
	ENDREM
	Method getParentSceneNode:TSceneNode() 
		Local Obj:TSceneNode = New TSceneNode
		Obj.Pointer = PS_getParentSceneNode(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get whether or not this object is attached to a TOgreNode or TTagPoint
	ENDREM
	Method isAttached:Byte()
		Return PS_isAttached(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not this object is attached to a TSceneNode or TTagPoint that is  active within the current TSceneManager. Ie. Is it in the current scene?
	ENDREM
	Method isInScene:Byte()
		Return PS_isInScene(Self.Pointer)
	End Method
	
	Rem
		bbdoc: INTERNAL - Tell this TMovableObject that it's position has changed in some way.
	ENDREM
	Method _notifyMoved()
		PS__notifyMoved(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the world bounding box for this object.
	ENDREM
	Method getWorldBoundingBox:TAxisAlignedBox(derive:Int = False)
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = PS_getWorldBoundingBox(derive, Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the world bounding sphere for this object.
	ENDREM
	Method getWorldBoundingSphere:TSphere(derive:Int = False)
		Local Obj:TSphere = New TSphere
		Obj.Pointer = PS_getWorldBoundingSphere(derive, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Set this object to visible or invisible if it can be.
	ENDREM
	Method setVisible(visible:Byte)
		PS_setVisible(visible, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether this object is visible or not.
	ENDREM
	Method getVisible:Byte()
		Return PS_getVisible(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Gets if this object is supposed to be visible or not. This method also considers visibility flags and rendering distance.
	ENDREM
	Method isVisible:Byte()
		Return PS_isVisible(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the viewing distance/radius where the object will be culled out of visibility.
	ENDREM
	Method setRenderingDistance(dist:Float) 
		PS_setRenderingDistance(dist, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the viewing distance where the object will be culled out of visibility.
	ENDREM
	Method getRenderingDistance:Float() 
		Return PS_getRenderingDistance(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Through use of the TUserDefinedObject interface, allows you to specify any C++ Native Types/Objects  and inject them into this TMovableObject.
	ENDREM
	Method setUserObject(obj:TUserDefinedObject) 
		PS_setUserObject(obj.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Retreive the TUserDefinedObject used and the information within it from this object.
	ENDREM
	Method getUserObject:TUserDefinedObject() 
		Local Obj:TUserDefinedObject = New TUserDefinedObject
		Obj.Pointer = PS_getUserObject(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED: Sets any user specified numeric value type on this object.
	ENDREM
	Method setUserAny(anything:TAny) 
		PS_setUserAny(anything.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED: Gets any user specified numeric value on this object.
	ENDREM
	Method getUserAny:TAny() 
		Local Obj:TAny = New TAny
		Obj.Pointer = PS_getUserAny(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets the renderqueue group for this object, specifying whether it will be earlier or later than other objects. <br>Different render queues:<br></br> Const RENDER_QUEUE_BACKGROUND:Int = 0 </br>Const RENDER_QUEUE_SKIES_EARLY:Int = 5 </br>Const RENDER_QUEUE_1:Int = 10 </br>Const RENDER_QUEUE_2:Int = 20 </br>Const RENDER_QUEUE_WORLD_GEOMETRY_1:Int = 25 </br>Const RENDER_QUEUE_3:Int = 30 </br>Const RENDER_QUEUE_4:Int = 40 </br>Const RENDER_QUEUE_MAIN:Int = 50 </br>Const RENDER_QUEUE_6:Int = 60 </br>Const RENDER_QUEUE_7:Int = 70 </br>Const RENDER_QUEUE_WORLD_GEOMETRY_2:Int = 75 </br>Const RENDER_QUEUE_8:Int = 80 </br>Const RENDER_QUEUE_9:Int = 90 </br>Const RENDER_QUEUE_SKIES_LATE:Int = 95 </br>Const RENDER_QUEUE_OVERLAY:Int = 100 </br></br> Where 50 is the default render queue for most TMovableObject derivatives. 0 is Drawn first, 100 is drawn last
	ENDREM
	Method getRenderQueueGroup:Int()
		Return PS_getRenderQueueGroup(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the full transform of the parent node.
	ENDREM
	Method _getParentNodeFullTransform:TMatrix4() 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = PS__getParentNodeFullTransform(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Sets a query flag for this object as a specifier for when scene queries such as TRaySceneQuery are done. This value is a bitwise mask value that can and should be manipulated by bitwise operators if multiple values are used.
	ENDREM
	Method setQueryFlags(flags:Int) 
		PS_setQueryFlags(flags, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Adds flags/bits to the query flag that is used in scene queries, such as TRaySceneQuery, to include or exclude objects. This value is a bitwise mask, and the values for the flags given are appended to the current query mask.
	ENDREM
	Method addQueryFlags(flags:Int) 
		PS_addQueryFlags(flags, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Removes flags/bits to the query flag that is used in scene queries, such as TRaySceneQuery, to include or exclude objects. This value is a bitwise mask, and the values for the flags given are removed from the current query mask.
	ENDREM
	Method removeQueryFlags(flags:Int) 
		PS_removeQueryFlags(flags, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get this object's query flags. The value is a bitwise mask.
	ENDREM
	Method getQueryFlags:Int() 
		Return PS_getQueryFlags(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Sets a bitwise mask of flags called visibility flags. Visibility flags comprise the visibility mask, which is used with a scene manager to determine what object are visible at any given time. This can be used dynamically for group based occlusions, etc.
	ENDREM
	Method setVisibilityFlags(flags:Int) 
		PS_setVisibilityFlags(flags, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Adds flags/bits to the current visibility flags by appending them. These comprise the visibility mask, which is used by the scene manager to determine if object are visible based on bitwise flags. This can be used dynamically for grouping, etc.
	ENDREM
	Method addVisibilityFlags(flags:Int) 
		PS_addVisibilityFlags(flags, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Removes flags/bits from the current visbility flags. This comprises the visibility mask, which is used by the scene manager to determine if object are visible based on bitwise flags. This can be used dynamically for grouping, etc.
	ENDREM
	Method removeVisibilityFlags(flags:Int) 
		PS_removeVisibilityFlags(flags, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get this object's visibility flags. This comprises the visibility mask, which is used by the scene manager to determine if object are visible based on bitwise flags. This can be used dynamically for grouping, etc.
	ENDREM
	Method getVisibilityFlags:Int() 
		Return PS_getQueryFlags(Self.Pointer)
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED: Set a listener for this object so you you listen in and react to events for this object, such as attach and detachments, etc.
	ENDREM
	Method setListener(listener:TMovableObjectListener) 
		PS_setListener(listener.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED: Get the listener currently attached to this object.
	ENDREM
	Method getListener:TMovableObjectListener() 
		Local Obj:TMovableObjectListener = New TMovableObjectListener
		Obj.Pointer = PS_getListener(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED: Gets a list of lights that affect this object, ordered from closest to furthest.
	ENDREM
	Method queryLights:TLightList() 
		Local Obj:TLightList = New TLightList
		Obj.Pointer = PS_queryLights(Self.Pointer)
		Return Obj
	End Method
	
	
	'----------------------
	'From TShadowCaster
	'----------------------
	
	Rem
		bbdoc: Sets whether or not this object will cast shadows when a scene manager shadow technique is set with TSceneManager.setShadowTechnique(...).
	ENDREM
	Method setCastShadows(enabled:Int) 
		PS_setCastShadows(enabled, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Retreive the flag that specifies if the object casts a shadow or not.
	ENDREM
	Method getCastShadows:Int() 
		Return PS_getCastShadows(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the edgelist for this shadowcaster in TEdgeData vector form.
	ENDREM
	Method getEdgeList:TEdgeList() 
		Local Obj:TEdgeList = New TEdgeList
		Obj.Pointer = PS_getEdgeList(Self.Pointer)
		Return Obj
	End Method
	
	
	Rem
		bbdoc: Retreive the flag that specifies if the object has an edge list or not.
	ENDREM
	'@returns : An Int that is TRUE if there is an edge list, and FALSE if there is not.
	Method hasEdgeList:Int()
		Return PS_hasEdgeList(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Retreive the world TAxisAlignedBox for the light cap.
	ENDREM
	Method getLightCapBounds:TAxisAlignedBox() 
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = PS_getLightCapBounds(Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Retreive the world TAxisAlignedBox for the dark cap, using the light for extrusion.<br>dirLightExtrusionDist:Float The distance of the extrusion.<br>Returns a TAxisAlignedBox that contains the metrics of the dark cap  axis-aligned box for the TShadowCaster. 
	ENDREM
	Method getDarkCapBounds:TAxisAlignedBox(l:TLight, dirLightExtrusionDist:Float) 
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = PS_getDarkCapBounds(l.Pointer, dirLightExtrusionDist, Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	Rem
		bbdoc: Retreive the distance the shadow volume must be extruded from a specified TLight.
	ENDREM
	Method getPointExtrusionDistance:Float(l:TLight) 
		Return PS_getPointExtrusionDistance(l.Pointer, Self.Pointer)
	End Method
	
	
	
	
End Type

Rem
	bbdoc: A type for rendering a chain of billboards that are connected.
ENDREM
Type TBillboardChain Extends TMovableObject
	
	Rem
		bbdoc:
	ENDREM
	Const TCD_U:Int = 0
	
	Rem
		bbdoc:
	ENDREM
	Const TCD_V:Int = 1
	
	Rem
		bbdoc: Set the max number of elements per chain.
	ENDREM
	Method setMaxChainElements(maxElements:Int) 
		BC_setMaxChainElements(maxElements, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the number of max elements allowed per chain for this TBillboardChain.
	ENDREM
	Method getMaxChainElements:Int() 
		Return BC_getMaxChainElements(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the max number of chain segments to be rendered at once.
	ENDREM
	Method setNumberOfChains(numChains:Int) 
		BC_setNumberOfChains(numChains, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the max number of chain segments to be rendered at once.
	ENDREM
	Method getNumberOfChains:Int() 
		Return BC_getNumberOfChains(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set wheter or not texture coordinate information should be used with this TBillboardChain.
	ENDREM
	Method setUseTextureCoords(use:Int) 
		BC_setUseTextureCoords(use, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not texture coordinate information is used with this TBillboardChain.
	ENDREM
	Method getUseTextureCoords:Int() 
		Return BC_getUseTextureCoords(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Sets the direction that the texture coordinates run along the chain.<br>dir:Int Default is TBillboardChain.TCD_U. Your options are TBillboardChain.TCDU and TBillboardChain.TCD_V.
	ENDREM
	Method setTextureCoordDirection(dir:Int) 
		BC_setTextureCoordDirection(dir, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets he direction that the texture coordinates run along the chain. Will return either: <br>0 = TBillboardChain.TCD_U </br>1 = TBillboardChain.TCD_V
	ENDREM
	'@returns: 
	Method getTextureCoordDirection:Int() 
		Return BC_getTextureCoordDirection(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the range of the texture coordinates generated across the width of this TBillboardChain.<br> start:Float The start of the range, default 0.0.<br>endRange:Float The end of the range, default 1.0.
	ENDREM
	Method setOtherTextureCoordRange(start:Float, endRange:Float) 
		BC_setOtherTextureCoordRange(start, endRange, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the range of the texture coordinates as a float pointer, and iterate through it to get both Float values.<br>Returns a float pointer that can be used to refrence 2 values. </br> Global pointer:Float Ptr = bbc.getOtherTextureCoordRange() </br> Global start:Float = pointer[0] </br> Global endRange:Float = pointer [1]
	ENDREM
	Method getOtherTextureCoordRange:Float Ptr() 
		Return BC_getOtherTextureCoordRange(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set whether or not vertex colours will be available with this TBillboardChain.
	ENDREM
	Method setUseVertexColours(use:Byte) 
		BC_setUseVertexColours(use, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not vertex colours are available for this TBillboardChain.
	ENDREM
	Method getUseVertexColours:Int() 
		Return BC_getUseVertexColours(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set whether or not the buffers this object uses are dynamic.
	ENDREM
	Method setDynamic(dyn:Byte) 
		BC_setDynamic(dyn, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not the buffers this object uses are dynamic.
	ENDREM
	Method getDynamic:Int() 
		Return BC_getDynamic(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Add an element to the tip of the TBillboardChain.<br>chainIndex:Int The index of the chain tip to attach to.<br>billboardChainElement:TBillboardChainElement The TBillboardChainElement to add.
	ENDREM
	Method addChainElement(chainIndex:Int, billboardChainElement:TBillboardChainElement) 
		BC_addChainElement(chainIndex, billboardChainElement.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove an element from the end of a chain.<br>chainIndex:Int The index of the chain to remove the tail element from.
	ENDREM
	Method removeChainElement(chainIndex:Int) 
		BC_removeChainElement(chainIndex, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Update an existing TBillboardChainElement.<br>chainIndex:Int The index of the chain who's element you wish to update.<br>elementIndex:Int The index of the element you'd like to update.<br>billboardChainElements:TBillboardChainElements The element containing the information to update the element with.
	ENDREM
	Method updateChainElement(chainIndex:Int, elementIndex:Int, billboardChainElement:TBillboardChainElement) 
		BC_updateChainElement(chainIndex, elementIndex, billboardChainElement.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get a TBillboardChainElement from a chain via indexes.<br>chainIndex:Int The index of the chain you wish to get the element of.<br>elementIndex:Int The index of the element on the chain that you wish to get.
	ENDREM
	Method getChainElement:TBillboardChainElement(chainIndex:Int, elementIndex:Int) 
		Local Obj:TBillboardChainElement = New TBillboardChainElement
		Obj.Pointer = BC_getChainElement(chainIndex, elementIndex, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Remove all chain elements from the specified index.
	ENDREM
	Method clearChain(chainIndex:Int) 
		BC_clearChain(chainIndex, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove all chain elements from all chains on this TBillboardChain.
	ENDREM
	Method clearAllChains() 
		BC_clearAllChains(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the name of the current material applied to this TBillboardChain.
	ENDREM
	Method getMaterialName:String() 
		Local temp:String
		Return temp.FromCString(BC_getMaterialName(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Set a material name for this TBillboardChain.
	ENDREM
	Method setMaterialName(Name:String) 
		BC_setMaterialName(name.ToCString() , Self.Pointer) 
	End Method
	
	'---------------------
	'From TRenderable
	'---------------------
	
	Rem
		bbdoc: Gets a reference to the TMaterial object that this TRenderable uses.
	ENDREM
	Method getMaterial:TMaterial() 
		Local Obj:TMaterial = New TMaterial
		Obj.Pointer = RENDERABLE_getMaterial( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets the TTechnique object that is being used to render the TMaterial for this TRenderable.
	ENDREM
	Method getTechnique:TTechnique() 
		Local Obj:TTechnique = New TTechnique
		Obj.Pointer = RENDERABLE_getTechnique( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets the TRenderOperation that is required to send this TRenderable to the frame buffer.<br>op:TRenderOperation The TRenderOperation will be stored in this variable.
	ENDREM
	Method getRenderOperation(op:TRenderOperation) 
		RENDERABLE_getRenderOperation( op.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets any world transform matrices for this TRenderable. <br>xform:TMatrix4 Stores the world transform matrices.
	ENDREM
	Method getWorldTransforms(xform:TMatrix4) 
		RENDERABLE_getWorldTransforms( xform.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the number of world transforms this TRenderable has applied to it.
	ENDREM
	Method getNumWorldTransforms:Int() 
		Return BC_getNumWorldTransforms(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Sets whether an identity projection will be used or not.
	ENDREM
	Method setUseIdentityProjection(useIdentityProjection:Int) 
		RENDERABLE_setUseIdentityProjection( useIdentityProjection , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retreive whether or not this TRenderable uses an identity projection.
	ENDREM
	Method getUseIdentityProjection:Int() 
		Return RENDERABLE_getUseIdentityProjection( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets whether or not to use an identity view on this TRenderable.
	ENDREM
	Method setUseIdentityView(useIdentityView:Int) 
		RENDERABLE_setUseIdentityView( useIdentityView , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retreive whether or not this TRenderable uses an identity view.
	ENDREM
	Method getUseIdentityView:Int() 
		Return RENDERABLE_getUseIdentityView( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retreives a camera centric view depth of this TRenderable.
	ENDREM
	Method getSquaredViewDepth:Float(camera:TCamera) 
		Return RENDERABLE_getSquaredViewDepth( camera.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns a list of lights that affect this TRenderable, sorting them by nearest.
	ENDREM
	Method getLights:TLightList() 
		Local Obj:TLightList = New TLightList
		Obj.Pointer = BC_getLights(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Returns a flag specifying whether this TRenderable casts a shadow or not.
	ENDREM
	Method getCastsShadows:Int() 
		Return BC_getCastsShadows(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set custom rendering parameters for this TRenderable that can be used as quanta for affecting different rendering actions.<br>index:Long An index to associate the value with, somewhat like a key.<br>value:TVector4 The value of the custom parameter.
	ENDREM
	Method setCustomParameter(index:Int, Value:TVector4) 
		BC_setCustomParameter( index , value.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Retreive a custom parameter set for this TRenderable object.<br>index:Int The index associated with the custom parameter you are trying to retreive.<br>A TVector4 containing the value of the parameter retreived.
	ENDREM
	Method getCustomParameter:TVector4(index:Int) 
		Return TVector4.FromPtr(BC_getCustomParameter(index, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Use this to update custom Gpu Parameters for this TRenderable.<br>constantEntry:TAutoConstantEntry Entry referring to the parameter being updated.<br> params:TGpuProgramParameters The parameters object that the TAutoConstantEntry parameter object should call to update the paramseters.
	ENDREM
	Method _updateCustomGpuParameter(constantEntry:TAutoConstantEntry, params:TGpuProgramParameters) 
		RENDERABLE__updateCustomGpuParameter( constantEntry.Pointer , params.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets whether or not this camera's detail level can be overridden by the camera settings.
	ENDREM
	Method setPolygonModeOverrideable(overrideable:Int) 
		RENDERABLE_setPolygonModeOverrideable( overrideable , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Return whether or not this TRenderable can have it's detail level overridden by camera settings.
	ENDREM
	Method getPolygonModeOverrideable:Int() 
		Return RENDERABLE_getPolygonModeOverrideable( Self.Pointer )
	End Method
	
	
End Type


Rem
	bbdoc: A set of billboards that are reasonably close to one another. This type is not yet fully implemented.
ENDREM
Type TBillboardSet Extends TMovableObject

	Rem
		bbdoc: Create a new billboard in this billboard set.
	ENDREM
	Method createBillboard:TBillboard(position:TVector3, colour:TColourValue)
		Local Obj:TBillboard = New TBillboard
		Obj.Pointer = BILLBOARDSET_createBillboard(position.Pointer, colour.Pointer, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the material that all the billboards from this billboard set will use.
	ENDREM
	Method setMaterialName(name:String)
		BILLBOARDSET_setMaterialName(name.ToCString() , Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not billboards are rendered according to their distance from the camera. 
	ENDREM
	Method getSortingEnabled:Byte()
		Return BILLBOARDSET_getSortingEnabled(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set whether or not billboards are rendered according to their distnace from the camera. Billboard systems that are sorted have a decrease in performance but no visual anomalies from improperly sorted billboards. 
	ENDREM
	Method setSortingEnabled(sortenabled:Byte)
		BILLBOARDSET_setSortingEnabled(sortenabled, Self.Pointer)
	End Method
	
End Type

Rem
	bbdoc: A billboard contained within a billboard set.
ENDREM
Type TBillboard
	
	Rem
		bbdoc: Pointer to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get the current rotation of this billboard as a TRadian.
	ENDREM
	Method GetRotation:TRadian()
		Local Obj:TRadian = New TRadian
		Obj.Pointer = BILLBOARD_getRotation(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the current rotation of this billboard with a TRadian.
	ENDREM
	Method SetRotation(rotation:TRadian)
		BILLBOARD_setRotation(rotation.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the position of this billboard relative to its attached parent.
	ENDREM
	Method setPosition(position:TVector3)
		BILLBOARD_setPosition(position.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the dimensions of this billboard in world units.
	ENDREM
	Method setDimensions(width:Float, height:Float)
		BILLBOARD_setDimensions(width, height, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the diffuse colour of this billboard. The colouring is done via vertex colours.
	ENDREM
	Method setColour(colour:TColourValue)
		BILLBOARD_setColour(colour.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the diffuse colour of this billboard.
	ENDREM
	Method getColour:TColourValue()
		Local Obj:TColourValue = New TColourValue
		Obj.Pointer = BILLBOARD_getColour(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the specific texture coordinates you want this billboard to utilise during rendering.
	ENDREM
	Method setTexcoordRect(u0:Float, v0:Float, u1:Float, v1:Float)
		BILLBOARD_setTexcoordRect(u0, v0, u1, v1, Self.Pointer)
	End Method
	
End Type

Rem
	bbdoc: A section of data that holds rendering info about TBillboardChain segments. This type is not yet fully implemented. 
ENDREM
Type TBillboardChainElement
	Field Pointer:Byte Ptr
	Field position:TVector3
	Field width:Float Ptr
	Field texCoord:Float Ptr
	Field colour:TColourValue
	Field managed:Int = 1
	
	Rem
		bbdoc: Create an empty TBillboardChainElement for use with TBillboardChains. It should be noted that this function pushes around many pointer references, and may become cpu intensive easier than expected.
	ENDREM
	Function createEmpty:TBillboardChainElement() 
		Local Obj:TBillboardChainElement = New TBillboardChainElement
		Obj.Pointer = BCE_createEmpty() 
		Return Obj
	End Function
	
	Rem
		bbdoc: Create an TBillboardChainElement For use with TBillboardChains. It should be noted that this Function pushes around many pointer references, And may become cpu intensive easier than expected.<br>position:TVector3 The position of this element.<br>width:Float The width of this element.<br>texCoord:Float The U or V texture coordinate.<br>colour:TColourValue The colour of this element.
	ENDREM
	Function Create:TBillboardChainElement(Position:TVector3, width:Float, texCoord:Float, colour:TColourValue) 
		Local Obj:TBillboardChainElement = New TBillboardChainElement
		Obj.Pointer = BCE_create(Position.Pointer, width, texCoord, colour.Pointer) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor, destroys this object with GC if not managed somewhere else.
	ENDREM
	Method Delete() 
		If(Self.managed = 0) 
			BCE_delete(Self.Pointer) 
		End If
	End Method
End Type

Rem
	bbdoc: Note: Ribbon Trails are not yet supported. A type of TBillboardChain that creates a trail attached to TOgreNode instances. This type is not yet fully implemented.
ENDREM
Type TRibbonTrail Extends TBillboardChain

End Type


Rem
	bbdoc: An interface for performing intersection and other query-like tests with objects in the scene. This class and it's children are to be utilized through the current TSceneManager, not created on their own.
ENDREM
Type TSceneQuery Abstract
	
	Rem
		bbdoc: Reference to object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: World Fragment Type : Return no world geometry hits.
	ENDREM
	Const WFT_NONE:Int = 0
	
	Rem
		bbdoc:World Fragment Type : Return plane bounded regions.
	ENDREM
	Const WFT_PLANE_BOUNDED_REGION:Int = 1
	
	Rem
		bbdoc: World Fragment Type : Return a single intersection point ( RaySceneQuery ).
	ENDREM
	Const WFT_SINGLE_INTERSECTION:Int = 2
	
	Rem
		bbdoc: World Fragment Type : Return custom geometry.
	ENDREM
	Const WFT_CUSTOM_GEOMETRY:Int = 3
	
	Rem
		bbdoc: World Fragment Type : General render operation.
	ENDREM
	Const WFT_RENDER_OPERATION:Int = 4
	
	Rem
		bbdoc: Sets the query mask for this query. This query mask will filter out TMovableObjects based on their own query mask. The value contained is operated in in a bitwise fashion, so modify and read values according to this.
	ENDREM
	Method setQueryMask(mask:Int) 
		SQ_setQueryMask( mask , Self.Pointer )	
	End Method
	
	Rem
		bbdoc: Gets the query mask set for this query.  This query mask will filter out TMovableObjects based on their own query mask. The value contained is operated in in a bitwise fashion, so modify and read values according to this.
	ENDREM
	Method getQueryMask:Int() 
		Return SQ_getQueryMask( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Similar to setQueryMask , except, instead of specifying arbitrary objects for a mask, this mask filters out specified types based on it's bit-pattern.
	ENDREM
	Method setQueryTypeMask(mask:Int) 
		SQ_setQueryTypeMask( mask , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Similar to getQueryMask , except, instead of specifying arbitrary objects for a mask, this mask filters out specified types based on it's bit-pattern.
	ENDREM
	Method getQueryTypeMask:Int()
		Return SQ_getQueryTypeMask(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the type of geometry you wish for this scene query to return with this query. The options are as follows: </br> WFT_NONE - Return no geometry. </br> WFT_PLANE_BOUNDED_REGION Return convex plane bounded regions. </br> WFT_SINGLE_INTERSECTION - Return a single point of intersection. </br> WFT_CUSTOM_GEOMETRY - Custom geometry defined by the TSceneManager </br> WFT_RENDER_OPERATION - TRenderOperation structure.
	ENDREM
	Method setWorldFragmentType(wft:Int) 
		SQ_setWorldFragmentType( wft , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the type of geometry you wish for this scene query to return with this query. The options are as follows: </br> WFT_NONE = 0  - Return no geometry. </br> WFT_PLANE_BOUNDED_REGION = 1 -  Return convex plane bounded regions. </br> WFT_SINGLE_INTERSECTION = 2 - Return a single point of intersection. </br> WFT_CUSTOM_GEOMETRY = 3 - Custom geometry defined by the TSceneManager </br> WFT_RENDER_OPERATION = 4  - TRenderOperation structure.
	ENDREM
	Method getWorldFragmentType:Int() 
		Return SQ_getWorldFragmentType( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns a set of world fragment types that this query supports.
	ENDREM
	Method getSupportedWorldFragmentTypes:TSet_Int() 
		Local Obj:TSet_Int = New TSet_Int
		Obj.Pointer = SQ_getSupportedWorldFragmentTypes( Self.Pointer )	   
		Return Obj
	End Method
	
End Type
Rem
	bbdoc: A Type for querying the contents of a scene with a TRay.
ENDREM
Type TRaySceneQuery Extends TSceneQuery

	Rem
		bbdoc: Sets the TRay to be used in this query.
	ENDREM
	Method setRay(ray:TRay) 
		RSQUERY_setRay( ray.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the TRay to be used in this query.
	ENDREM
	Method getRay:TRay()
		Local Obj:TRay = New TRay
		Obj.Pointer = RSQUERY_getRay( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Set whether or not the objects touching the ray will be sorted by distance along the ray.<br>sort:Int - True for distance sorting, False for no distance sorting.<br>maxResults With sorting enabled, you may use this to put a cap on how many results can be returned from this query. Zero turns the cap off. 
	ENDREM
	Method setSortByDistance(Sort:Int, maxResults:Int = 0) 
		RSQUERY_setSortByDistance( sort , maxResults , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get whether or not the objects touching the ray will be sorted by distance along the ray.
	ENDREM
	Method getSortByDistance:Int() 
		Return RSQUERY_getSortByDistance( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the maximum number of results allowed  for this query. If the result is false/0 , then result capping is off.
	ENDREM
	Method getMaxResults:Int() 
		Return RSQUERY_getMaxResults( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Performs the query and puts the results in a list or TRaySceneQuery result. This list will always exist until clearResults() is called.
	ENDREM
	Method execute:TRaySceneQueryResult() 
		Local Obj:TRaySceneQueryResult = New TRaySceneQueryResult
		Obj.Pointer = RSQUERY_execute( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Lighter weight execution method that returns each match through the listener. The results of the query are not stored.
	ENDREM
	Method executeWithListener(listener:TRaySceneQueryListener) 
		RSQUERY_executeWithListener( listener.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the last results of execute().
	ENDREM
	Method getLastResults:TRaySceneQueryResult() 
		Local Obj:TRaySceneQueryResult = New TRaySceneQueryResult
		Obj.Pointer = RSQUERY_getLastResults( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Clears the results of the last execute() command for this query.
	ENDREM
	Method clearResults()
		RSQUERY_clearResults( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Callback used when a TMovableObject is returned.<br>obj:TMovableObject Object to be added to the list of results.<br>distance:Float Object distance from  ray origin.
	ENDREM
	Method queryResult:Int(obj:TMovableObject, distance:Float) 
		RSQUERY_queryResult( obj.Pointer , distance , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Callback used when TWorldFragment is returend.<br>fragment:TWorldFragment TWorldFragment to be added to the list of results.<br>distance:Float Object distance from  ray origin.
	ENDREM
	Method queryResultWithWorldFragment(fragment:TWorldFragment, distance:Float) 
		RSQUERY_queryResultWithWorldFragment( fragment.Pointer , distance , Self.Pointer )
	End Method
	
	
End Type

Rem
	bbdoc: A type for querying for pairs of objects that are possibly intersecting. This type is not fully implemented yet.
ENDREM
Type TIntersectionSceneQuery Extends TSceneQuery
End Type

Rem
	bbdoc: Abstract type that defines returning results from a region. This type is not fully implemented yet.
ENDREM
Type TRegionSceneQuery Extends TSceneQuery Abstract
	
End Type

Rem
	bbdoc: A Type for querying the contents of a scene with a TAxisAlignedBox. This type is not fully implemented yet.
ENDREM
Type TAxisAlignedBoxSceneQuery Extends TRegionSceneQuery
End Type

Rem
	bbdoc: A Type for querying the contents of a scene with a TSphere. This type is not fully implemented yet.
ENDREM
Type TSphereSceneQuery Extends TRegionSceneQuery
End Type

Rem
	bbdoc: A Type for querying the contents of a scene with a TPlaneBoundedVolumeList. This type is not fully implemented yet. 
ENDREM
Type TPlaneBoundedVolumeListSceneQuery Extends TRegionSceneQuery
End Type

Rem
	bbdoc: Type that stores the results of a ray scene query.
ENDREM
Type TRaySceneQueryResult
	
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get a ray scene query result iterator that starts at the beginning.
	ENDREM
	Method getBeginning:TRaySceneQueryResultIterator()
		Local Obj:TRaySceneQueryResultIterator = New TRaySceneQueryResultIterator
		Obj.Pointer = RSQR_begin( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a ray scene query result iterator that starts at the end.
	ENDREM
	Method getEnd:TRaySceneQueryResultIterator()
		Local Obj:TRaySceneQueryResultIterator = New TRaySceneQueryResultIterator
		Obj.Pointer = RSQR_end( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a ray scene query result entry from a given index.
	ENDREM
	Method at:TRaySceneQueryResultEntry( index:Int )
		Local Obj:TRaySceneQueryResultEntry = New TRaySceneQueryResultEntry
		Obj.Pointer = RSQR_at( index , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete() 
		RSQR_delete(Self.Pointer) 
	End Method
	
End Type

Rem
	bbdoc: Type that represents an iterator that iterates through ray scene query results.
ENDREM
Type TRaySceneQueryResultIterator

	Rem
		bbdoc: Reference to the object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get the TRaySceneQueryResultEntry for the point that the iterator is currently at.
	ENDREM
	Method getEntry:TRaySceneQueryResultEntry()
		Local Obj:TRaySceneQueryResultEntry = New TRaySceneQueryResultEntry
		Obj.Pointer = RSQRI_getEntry( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Inequality sign for testing this iterator against another. a.isNotEqualTo(b) is like using a <> b on normal variables.
	ENDREM
	Method isNotEqualTo:Int( b:TRaySceneQueryResultIterator )
		Return RSQRI_isNotEqualTo( Self.Pointer , b.Pointer )
	End Method
	
	Rem
		bbdoc: Equality sign for testing this iterator against another. a.isEqualTo(b) is like using a = b on normal variables.
	ENDREM
	Method isEqualTo:Int( b:TRaySceneQueryResultIterator )
		Return RSQRI_isEqualTo( Self.Pointer , b.Pointer )	
	End Method
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete() 
		RSQRI_delete(Self.Pointer) 
	End Method
End Type

Rem
	bbdoc: Type that gives the information from a single ray scene query hit.
ENDREM
Type TRaySceneQueryResultEntry

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get the movable object that was hit by the query.
	ENDREM
	Method getMovable:TMovableObject()
		Local Obj:TMovableObject = New TMovableObject
		Obj.Pointer = RSQRENTRY_getMovable( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the distance down the ray that the hit occured.
	ENDREM
	Method getDistance:Float()
		Return RSQRENTRY_getDistance( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the world fragment of the hit that occured.
	ENDREM
	Method getWorldFragment:TWorldFragment()
		Local Obj:TWorldFragment = New TWorldFragment
		Obj.Pointer = RSQRENTRY_getWorldFragment( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Less than method.  a.isLessThan( b ) is like using  a < b on normal variables.
	ENDREM
	Method isLessThan:Int( b:TRaySceneQueryResultEntry )
		Return RSQRENTRY_isLessThan( Self.Pointer , b.Pointer )
	End Method
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete(r:TRaySceneQueryResultEntry) 
		RSQRENTRY_delete(Self.Pointer) 
	End Method
End Type

Rem
	bbdoc: Type that describes a ray scene query listener. This type is not yet fully implemented.
ENDREM
Type TRaySceneQueryListener

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Type describing a world fragment. This type is not yet fully implemented.
ENDREM
Type TWorldFragment

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Type describe a data stream. This type is not yet fully implemented.
ENDREM
Type TOgreDataStream

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Type containing a position/orientation pair.
ENDREM
Type TViewPoint
	
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Viewpoint world position.
	ENDREM
	Field position:TVector3
	
	Rem
		bbdoc: Viewpoint world orientation.
	ENDREM
	Field orientation:TQuaternion
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete() 
		VIEWPOINT_delete(Self.Pointer) 
	End Method
End Type

Rem
	bbdoc: A basic structure for holding sky plane information.
ENDREM
Type TSkyPlaneGenParameters
	
	Rem
		bbdoc: Reference to the Ogre SkyPlaneGenparameters structure in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: The scaling value of hte sky plane described.
	ENDREM
	Method getSkyPlaneScale:Float()
		Return SKYPLANEGENPARAMETERS_getSkyPlaneScale(Self.Pointer)
	End Method
	
	Rem
		bbdoc: The texture tiling value for the sky plane described.
	ENDREM
	Method getSkyPlaneTiling:Float()
		Return SKYPLANEGENPARAMETERS_getSkyPlaneTiling(Self.Pointer)
	End Method
	
	Rem
		bbdoc: How much the sky plane described bows or curves.
	ENDREM
	Method getSkyPlaneBow:Float()
		Return SKYPLANEGENPARAMETERS_getSkyPlaneBow(Self.Pointer)
	End Method
	
	Rem
		bbdoc: The amount of segments along the x axis that the sky plane described has.
	END REM
	Method getSkyPlaneXSegments:Int()
		Return SKYPLANEGENPARAMETERS_getSkyPlaneXSegments(Self.Pointer)
	End Method
	
	Rem
		bbdoc: The amount of segments along the y axis that the sky plane described has.
	END REM
	Method getSkyPlaneYSegments:Int()
		Return SKYPLANEGENPARAMETERS_getSkyPlaneYSegments(Self.Pointer)
	End Method

	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete() 
		SKYPLANEGENPARAMETERS_delete(Self.Pointer) 
	End Method
End Type

Rem
	bbdoc: A basic structure for holding sky box information.
ENDREM
Type TSkyBoxGenParameters

	Rem
		bbdoc: Reference to the Ogre SkyBoxGenParameters structure in memory.
	ENDREM
	Field Pointer:Byte Ptr

	Rem
		bbdoc: The distance from the camera that the described skybox is rendered.
	ENDREM
	Method getSkyBoxDistance:Float()
		Return SKYBOXGENPARAMETERS_getSkyBoxDistance(Self.Pointer)
	End Method

	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete() 
		SKYBOXGENPARAMETERS_delete(Self.Pointer) 
	End Method
End Type

Rem
	bbdoc: A basic structure for holding sky dome information.
ENDREM
Type TSkyDomeGenParameters

	Rem
		bbdoc: Reference to the Ogre SkyBoxDomeGenParameters structure in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: The curvature of the skydome described. 
	ENDREM
	Method getSkyDomeCurvature:Float()
		Return SKYDOMEGENPARAMETERS_getSkyDomeCurvature(Self.Pointer)
	End Method
	
	Rem
		bbdoc: The texture tiling value of the skydome described.
	ENDREM
	Method getSkyDomeTiling:Float()
		Return SKYDOMEGENPARAMETERS_getSkyDomeTiling(Self.Pointer)
	End Method
	
	Rem
		bbdoc: The distance from the camera that the described skydome is rendered.
	ENDREM
	Method getSkyDomeDistance:Float()
		Return SKYDOMEGENPARAMETERS_getSkyDomeDistance(Self.Pointer)
	End Method
	
	Rem
		bbdoc: The number of X segments in the sky dome described.
	ENDREM
	Method getSkyDomeXSegments:Int()
		Return SKYDOMEGENPARAMETERS_getSkyDomeXSegments(Self.Pointer)
	End Method
	
	Rem
		bbdoc: The number of Y segments in the sky dome described.
	ENDREM
	Method getSkyDomeYSegments:Int()
		Return SKYDOMEGENPARAMETERS_getSkyDomeYSegments(Self.Pointer)
	End Method
	
	Rem
		bbdoc: The number of Y segments in the sky dome described to keep.
	ENDREM
	Method getSkyDomeYSegments_keep:Int()
		Return SKYDOMEGENPARAMETERS_getSkyDomeYSegments_keep(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete() 
		SKYDOMEGENPARAMETERS_delete(Self.Pointer) 
	End Method
	
End Type

Rem
	bbdoc: Describes a tag point on a skeleton where you can attach entities to. This type is not yet fully implemented.
ENDREM
Type TTagPoint

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Iterates through child a child object list. This type is not yet fully implemented.
ENDREM
Type TChildObjectListIterator

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Describes a set of entities. This type is not yet fully implemented.
ENDREM
Type TEntitySet

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Represents an instance of a bone in a skeleton. Bones should not be created manually with a constructor, but rather through TSkeleton.createBone(...).
ENDREM
Type TBone

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create a child bone of this bone with the specified position and orientation.
	ENDREM
	Method createChild:TBone(handle:Int, translate:TVector3 = Null, rotate:TQuaternion = Null)
		
		If translate = Null Then
			translate = New TVector3
			translate.Pointer = TVector3.ZERO().Pointer
		End If
		
		If rotate = Null Then
			rotate = New TQuaternion
			rotate.Pointer = TQuaternion.IDENTITY().Pointer
		End If
		
		Local Obj:TBone = New TBone
		Obj.Pointer = BONE_createChild(handle, translate.Pointer, rotate.Pointer, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the numeric handle for this bone.
	ENDREM
	Method GetHandle:Int()
		Return BONE_getHandle(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set this bone's binding pose to its current position. The binding pose is the pose where the bone is actually bound to a mesh.
	ENDREM
	Method setBindingPose()
		BONE_setBindingPose(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Reset this bone's position to that of its binding pose.
	ENDREM
	Method reset()
		BONE_reset(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set whether or not this bone is manually controlled. A manually controlled bone is freed from animation constraints and can be translated/rotated manually without resetting its position due to animations.
	ENDREM
	Method setManuallyControlled(manuallyControlled:Byte)
		BONE_setManuallyControlled(manuallyControlled, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not this bone is manually controlled.
	ENDREM
	Method isManuallyControlled:Byte()
		Return BONE_isManuallyControlled(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the offset transform for this bone as a TMatrix4.
	ENDREM
	Method _getOffsetTransform(matrix4:TMatrix4)
		BONE__getOffsetTransform(matrix4.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get this bone's inverse scale during its binding pose.
	ENDREM
	Method _getBindingPoseInverseScale:TVector3()
		Return TVector3.FromPtr(BONE__getBindingPoseInverseScale(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Get this bone's inverse position during its binding pose.
	ENDREM
	Method _getBindingPoseInversePosition:TVector3()
		Return TVector3.FromPtr(BONE__getBindingPoseInversePosition(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Get this bone's inverse orientation during its binding pose.
	ENDREM
	Method _getBindingPoseInverseOrientation:TQuaternion()
		Return TQuaternion.FromPtr(BONE__getBindingPoseInverseOrientation(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Specify whether or not this bone needs an update.
	ENDREM
	Method needUpdate(forceParentUpdate:Byte = False)
		BONE_needUpdate(forceParentUpdate, Self.Pointer)
	End Method
	
	'*** TOgreNode Methods ***
		
	Rem
		bbdoc: Get the name of this node.	
	ENDREM
	Method getName:String() 
		Return String.FromCString(BONE_getName(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get this node's parent as a TOgreNode.  This will return NULL if there is no parent.
	ENDREM
	Method getParent:TOgreNode()
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = BONE_getParent( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Return a TQuaternion that contains this node's current orientation.
	ENDREM
	Method getOrientation:TQuaternion() 
		Return TQuaternion.FromPtr(BONE_getOrientation(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Sets the orientation/rotation of this node via a TQuaternion.
	ENDREM
	Method setOrientationWithQuaternion(ogreQuaternion:TQuaternion) 
		BONE_setOrientationWithQuaternion(ogreQuaternion.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Sets the orientation/rotation of this node via a TQuaternion defined as a series of floats.
	ENDREM
	Method setOrientation(w:Float, x:Float, y:Float, z:Float) 
		BONE_setOrientation(w, x, y, z, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Resets the node to it's local axes position with no orientation in world space.
	ENDREM
	Method resetOrientation() 
		BONE_resetOrientation(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the position of this node relative to it's parent node with a TVector3.
	ENDREM
	Method setPositionWithVector3(pos:TVector3) 
		BONE_setPositionWithVector3( pos.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the position of this node relative to it's parent node with 3 floats.
	ENDREM
	Method setPosition(x:Float, y:Float, z:Float) 
		BONE_setPosition( x , y , z ,Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the position of this node relative to it's parent node as a TVector3.
	ENDREM
	Method getPosition:TVector3() 
		Return TVector3.FromPtr(BONE_getPosition(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Set the scaling of this node with a TVector3.
	ENDREM
	Method setScaleWithVector3(scale:TVector3) 
		BONE_setScaleWithVector3( scale.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the scaling of this node with three axes as floats.
	ENDREM
	Method SetScale(x:Float, y:Float, z:Float) 
		BONE_setScale( x , y , z , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the scaling of this node.
	ENDREM
	Method GetScale:TVector3() 
		Return TVector3.FromPtr(BONE_getScale(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Set whether or not this node inherits orientation/rotation from it's parent.
	ENDREM
	Method setInheritOrientation(inherit:Byte) 
		BONE_setInheritOrientation( inherit , Self.Pointer ) 
	End Method
	
	Rem
		bbdoc: Get whether or not this node inherits orientation/rotation from it's parent.
	ENDREM
	Method getInheritOrientation:Byte() 
		Return BONE_getInheritOrientation(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set whether or not this node inherits scale/sizing from it's parent.
	ENDREM
	Method setInheritScale(inherit:Byte) 
		BONE_setInheritScale(inherit, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not this node inherits scale/sizing from it's parent.
	ENDREM
	Method getInheritScale:Byte() 
		Return BONE_getInheritScale(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Scales this node from it's current scale via the passed TVector3.
	ENDREM
	Method scaleWithVector3(scale:TVector3) 
		BONE_scaleWithVector3(scale.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Scales this node from it's current scale via three floats as axes.
	ENDREM
	Method scale(x:Float, y:Float, z:Float) 
		BONE_scale( x , y , z , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Translate this node along cartesian axes using a TVector3. Note that you may specify what space you wish to translate in. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space.
	ENDREM
	Method translateWithVector3(d:TVector3, relativeTo:Int = TOgreNode.TS_PARENT) 
		BONE_translateWithVector3(d.Pointer, relativeTo, Self.Pointer) 
	End Method
		
	Rem
		bbdoc: Translate this node along cartesian axes with three axes as floats. Note that you may specify what space you wish to translate in. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space.
	ENDREM
	Method translate(x:Float, y:Float, z:Float, relativeTo:Int = TOgreNode.TS_PARENT) 
		BONE_translate(x, y, z, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Translate this node along arbitrary axes defined by a TMatrix3. Once the axes are defined, you may translate/move using the move:TVector3 Variable. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space.
	ENDREM
	Method translateAlongAxesWithVector3(axes:TMatrix3, move:TVector3, relativeTo:Int = TOgreNode.TS_PARENT) 
		BONE_translateAlongAxesWithVector3(axes.Pointer, move.Pointer, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Translate this node along arbitrary axes defined by a TMatrix3. Once the axes are defined, you may translate/move along them using the x,y,z floats provided. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space.
	ENDREM
	Method translateAlongAxes(axes:TMatrix3, x:Float, y:Float, z:Float, relativeTo:Int = TOgreNode.TS_PARENT) 
		BONE_translateAlongAxes(axes.Pointer, x, y, z, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate around the Z-Axis(in degrees) of the specified transform space. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space. 	
	ENDREM
	Method roll(Degrees:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		BONE_roll(Degrees, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate around the Z-Axis(in radians) of the specified transform space. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space. 	
	ENDREM
	Method rollWithRadians(Radians:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		BONE_rollWithRadians(Radians, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate around the X-Axis(in degrees) of the specified transform space. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space. 	
	ENDREM
	Method pitch(Degrees:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		BONE_pitch(Degrees, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate around the X-Axis(in radians) of the specified transform space. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space. 	
	ENDREM
	Method pitchWithRadians(Radians:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		BONE_pitchWithRadians(Radians, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate around the Y-Axis(in degrees) of the specified transform space. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space. 	
	ENDREM
	Method yaw(Degrees:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		BONE_yaw(Degrees, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate around the Y-Axis(in radians) of the specified transform space. TOgreNode.TS_PARENT translates according to this node's parent. TOgreNode.TS_World will translate in world space. TOgreNode.TS_LOCAL will translate in this node's local space. 	
	ENDREM
	Method yawWithRadians(Radians:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		BONE_yawWithRadians(Radians, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate this node( in degrees ) around an arbitrary axis.
	ENDREM
	Method rotate(axis:TVector3, Degrees:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		BONE_rotate(axis.Pointer, Degrees, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate this node( in radians ) around an arbitrary axis.
	ENDREM
	Method rotateWithRadians(axis:TVector3, Radians:Float, relativeTo:Int = TOgreNode.TS_LOCAL) 
		BONE_rotateWithRadians(axis.Pointer, Radians, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Rotate this node around an arbitrary axis using a TQuaternion.
	ENDREM
	Method rotateWithQuaternion(q:TQuaternion, relativeTo:Int = TOgreNode.TS_LOCAL) 
		BONE_rotateWithQuaternion(q.Pointer, relativeTo, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the local space axes for this node as a TMatrix3.
	ENDREM
	Method getLocalAxes:TMatrix3() 
		Local Obj:TMatrix3 = New TMatrix3
		Obj.Pointer = BONE_getLocalAxes(Self.Pointer) 
		Return Obj
	End Method

	Rem
		bbdoc: Add a pre-created node to this node as a child.
	ENDREM
	Method addChild(child:TOgreNode) 
		BONE_addChild(child.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Return the number of children attached to this node. Note that this is not the recursive number of children, only the number directly attached to this node.
	ENDREM
	Method numChildren:Int() 
		Return BONE_numChildren( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get a handle to a child node via the child node's index value. Index goes from 0-n. If no node is found under the given index, the TOgreNode.Pointer will be null, and you may check quickly by using the .isNull() function.
	ENDREM
	Method getChild:TOgreNode(index:Int) 
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = BONE_getChild( index , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a handle to a child node via the child node's name. Unlike plain .getChild(...) which retrives the child by index, if .getChildWithName(...) fails your program will throw an exception! 
	ENDREM
	Method getChildWithName:TOgreNode(name:String) 
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = BONE_getChildWithName( name.ToCString() , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets an iterator that can be used for looping through all of the children of this node.
	ENDREM
	Method getChildIterator:TChildNodeIterator() 
		Local Obj:TChildNodeIterator = New TChildNodeIterator
		Obj.Pointer = BONE_getChildIterator(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Detaches a given child from this TOgreNode by index. Note that the child object is not destroyed and is returned to be potentially reattached elsewhere.
	ENDREM
	Method removeChildWithIndex:TOgreNode(index:Int) 
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = BONE_removeChildWithIndex(index, Self.Pointer) 
		Return Obj
	End Method

	Rem
		bbdoc: Detaches a given child from this TOgreNode by object reference. Note that the child object is not destroyed and is returned to be potentially reattached elsewhere.
	ENDREM
	Method removeChild:TOgreNode(child:TOgreNode) 
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = BONE_removeChild(child.Pointer, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Detaches a given child from this TOgreNode by name. Note that the child object is not destroyed and is returned to be potentially reattached elsewhere.
	ENDREM
	Method removeChildWithName:TOgreNode(name:String) 
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = BONE_removeChildWithName(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Removes all children from this node. Note that this does not destroy the child objects and they may be reattached elsewhere.
	ENDREM
	Method removeAllChildren()
		BONE_removeAllChildren( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the orienation of this TOgreNode as defined from all of it's parents. Note that nodes can avoid inherting orientation from parents by using setInheritOrientation( False ).
	ENDREM
	Method _getDerivedOrientation:TQuaternion() 
		Return TQuaternion.FromPtr(BONE__getDerivedOrientation(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the position of this TOgreNode as defined from all of it's parents.
	ENDREM
	Method _getDerivedPosition:TVector3() 
		Return TVector3.FromPtr(BONE__getDerivedPosition(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the scale of this TOgreNode as defiend from all it's parents. Note that nodes can avoid inheriting scale from their parents by using setInheritScale( False ).
	ENDREM
	Method _getDerivedScale:TVector3() 
		Return TVector3.FromPtr(BONE__getDerivedScale(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the full transform matrix for this TOgreNode.
	ENDREM
	Method _getFullTransform:TMatrix4() 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = BONE__getFullTransform(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Internal method that updates this TOgreNode and any children specified, as well as possibly taking parent transforms into account. Specify True for updateChildren if you wish to update the children also. Specify True for parentHasChanged if you wish to take into account changing parent transforms. You should probably never need to use this as it is usually for scene manager implementations.
	ENDREM
	Method _update(updateChildren:Byte, parentHasChanged:Byte) 
		BONE__update(updateChildren, parentHasChanged, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set up a listener for this TOgreNode. Note that only one listener per TOgreNode is allowed. ( Listeners are not implemented yet. )
	ENDREM
	Method setListener(listener:TOgreNodeListener) 
		BONE_setListener(listener.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets the curernt listener attached to this TOgreNode. ( Listeners are not implemented yet. )
	ENDREM
	Method getListener:TOgreNodeListener() 
		Local Obj:TOgreNodeListener = New TOgreNodeListener
		Obj.Pointer = BONE_getListener(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the initial state of this node to it's current transforms. Generally these values are used for keyframe animation. If you do not use this and are not animating the node, then the transforms will equal an identity matrix, which will do nothing.
	ENDREM
	Method setInitialState() 
		BONE_setInitialState(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Reset this node to it's initial state, which will apply all transforms from the initial state to the node.
	ENDREM
	Method resetToInitialState() 
		BONE_resetToInitialState(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Helper function that allows you to determine whether or not this bone is Null.
	ENDREM
	Method isNull:Byte() 
		If Self.Pointer <> Null Then
			Return False
		Else
			Return True
		End If
	End Method
	
	Rem
		bbdoc: Helper function to cast/return this bone to a TOgreNode.
	ENDREM
	Method ToOgreNode:TOgreNode()
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = Self.Pointer
		Return Obj
	End Method

	
End Type

Rem
	bbdoc: An iterator type that allows you to iterate through a list of bones.
ENDREM
Type TBoneIterator
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: A container class two things: A user properties key/value map that gets data injected when a scene loader is used and a custom object that allows you to store a custom object reference. In practical use you should keep your TUserDefinedObjects in a list for two reasons. The first being that when you mo.setUserObject, only a reference is being stored, so if your object instances goes out of scope and is destroyed, the reference is no longer valid. Secondly, this class's internal memory is not governed by GC and you will need to call .Destroy for every one you create.
ENDREM
Type TUserDefinedObject

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Constructor. Creates a blank user defined object.
	ENDREM
	Function Create:TUserDefinedObject() 
		Local Obj:TUserDefinedObject = New TUserDefinedObject
		Obj.Pointer = UO_create() 
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor. - Note that this object requires manual deletion. It's best to place it in a list and use the .destroy method.
	ENDREM
'	Method Delete() 
'		If Managed = False Then
'			UO_delete(Self.Pointer) 
'		End If
'	End Method
	
	Rem
		bbdoc: User method for manually destroying a user object. Please use this with care and make sure the object is not attached to any TMovableObject before you destroy it.
	ENDREM
	Method Destroy() 
		UO_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the custom object associated with this TUserDefinedObject. This will store the reference which can be gotten later from an MovableObject, so long as the data the reference points to remains valid.
	ENDREM
	Method getCustomObject:Object() 
		Return UO_getCustomObject(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Sets an object as this User Defined Object's custom object. The user defined object stores a reference, so if the object past gets destroyed, then you will have an invalid reference. Best use for this is to decide what your movable objects are going to need in the way of extended data, create a new type that contains it comfortably, then roll a list of that type and fill in your movable object data with this method.
	ENDREM
	Method setCustomObject(newObject:Object) 
		UO_setCustomObject(newObject, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the OSM property list that contains all of the properties of a given object, if loaded from an OSM, in a TNameValuePair list.
	ENDREM
	Method getOSMPropertyList:TNameValuePairList() 
		Local Obj:TNameValuePairList = New TNameValuePairList
		Obj.Pointer = UO_getOSMPropertyList(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Helper function to determine whether or not the reference to this user-defined object is null.
	ENDREM
	Method isNull:Byte() 
		If Self.Pointer = Null Then
			Return True
		Else
			Return False
		End If
	End Method
	
End Type


Rem
	bbdoc: A collection of bones that can be used to animate skinned geometry.
ENDREM
Type TSkeleton

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Average animation blending.
	ENDREM
	Const ANIMBLEND_AVERAGE:Int = 0
	
	
	Rem
		bbdoc: Cumulative animation blending.
	ENDREM
	Const ANIMBLEND_CUMULATIVE:Int = 1
	
	Rem
		bbdoc: Create a bone with no specified name or handle.
	ENDREM
	Method createBone:TBone()
		Local Obj:TBone = New TBone
		Obj.Pointer = SKELETON_createBone(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Create a bone and give it a specific integer based handle. The handle serves as an index as well as an indentifier.
	ENDREM
	Method createBoneWithHandle:TBone(handle:Int)
		Local Obj:TBone = New TBone
		Obj.Pointer = SKELETON_createBoneWithHandle(handle, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Create a bone and give it a name.
	ENDREM
	Method createBoneWithName:TBone(name:String)
		Local Obj:TBone = New TBone
		Obj.Pointer = SKELETON_createBoneWithName(name.ToCString() , Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Create a bone, giving it both a specified name and integer-based handle.
	ENDREM
	Method createBoneWithNameHandle:TBone(name:String, handle:Int)
		Local Obj:TBone = New TBone
		Obj.Pointer = SKELETON_createBoneWithNameHandle(name.ToCString() , handle, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the number of bones in this skeleton.
	ENDREM
	Method getNumBones:Int()
		Return SKELETON_getNumBones( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get a root bone iterator to iterate through all root bones ( skeletons can have more than one ) in this skeleton.
	ENDREM
	Method getRootBoneIterator:TBoneIterator()
		Local Obj:TBoneIterator = New TBoneIterator
		Obj.Pointer = SKELETON_getRootBoneIterator(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a bone iterator to iterate through all bones in this skeleton.
	ENDREM
	Method getBoneIterator:TBoneIterator()
		Local Obj:TBoneIterator = New TBoneIterator
		Obj.Pointer = SKELETON_getBoneIterator(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a bone in this skeleton by its specified handle.
	ENDREM
	Method getBone:TBone(handle:Int)
		Local Obj:TBone = New TBone
		Obj.Pointer = SKELETON_getBone(handle, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Retreive a bone from the skeleton by name.
	ENDREM
	Method getBoneWithName:TBone(name:String) 
		Local Obj:TBone = New TBone
		Obj.Pointer = SKELETON_getBoneWithName(name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Check to see if a bone exists with a given name.
	ENDREM
	Method hasBone:Byte(name:String)
		Return SKELETON_hasBone(name.ToCString() , Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the current positions of this skeleton to be the binding pose, or the pose in which the skeleton was bound to the mesh.
	ENDREM
	Method setBindingPose()
		SKELETON_setBindingPose(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Reset the skeleton's position to the binding pose. You can use setBindingPose() to set this skeleton's binding pose. You can also specify whether or not you want to reset any manual bones in this skeleton.
	ENDREM
	Method reset(resetManualBones:Byte = False)
		SKELETON_reset(resetManualBones, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Create a TAnimation object that can be used to animate this skeleton.
	ENDREM
	Method createAnimation:TAnimation(name:String, length:Float)
		Local Obj:TAnimation = New TAnimation
		Obj.Pointer = SKELETON_createAnimation(name.ToCString() , length, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get a TAnimation used in this skeleton by its specified name.
	ENDREM
	Method getAnimation:TAnimation(name:String)
		Local Obj:TAnimation = New TAnimation
		Obj.Pointer = SKELETON_getAnimation(name.ToCString() , Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Check to see whether an animation exists for this TSkeleton with the specified name.
	ENDREM
	Method hasAnimation:Byte(name:String)
		Return SKELETON_hasAnimation(name.ToCString() , Self.Pointer)
	End Method
	
	Rem
		bbdoc: Remove an animation from this skeleton by name.
	ENDREM
	Method removeAnimation(name:String)
		SKELETON_removeAnimation(name.ToCString() , Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the number of animations contained in this TSkeleton. 
	ENDREM
	Method getNumAnimations:Int()
		Return SKELETON_getNumAnimations(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Optimise the animations of this TSkeleton.<br/>preservingIdentityNodeTracks:Byte = False - If you want to keep identity node tracks, set this to true.
	ENDREM
	Method optimiseAllAnimations(preservingIdentityNodeTracks:Byte = False)
		SKELETON_optimiseAllAnimations(preservingIdentityNodeTracks, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the blend mode used by this skeleton for animation. The constants the values refer to are TSkeleton.ANIMBLEND_CONSTANT and TSkeleton.ANIMBLEND_CUMULATIVE.
	ENDREM
	Method getBlendMode:Int()
		Return SKELETON_getBlendMode(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the blend mode used by this skeleton for animation. The constants the values refer to are TSkeleton.ANIMBLEND_CONSTANT and TSkeleton.ANIMBLEND_CUMULATIVE.
	ENDREM
	Method setBlendMode(state:Int)
		SKELETON_setBlendMode(state, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Notify this skeleton that its manual bones need updated.
	ENDREM
	Method _notifyManualBonesDirty()
		SKELETON__notifyManualBonesDirty(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Notify a bone in this skeleton that it is a manual bone.
	ENDREM
	Method _notifyManualBoneStateChange(bone:TBone)
		SKELETON__notifyManualBoneStateChange(bone.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not the manual bones in this skeleton need updated.
	ENDREM
	Method getManualBonesDirty:Byte()
		Return SKELETON_getManualBonesDirty(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not this skeleton has any manual bones.
	ENDREM
	Method hasManualBones:Byte()
		Return SKELETON_hasManualBones(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the name of this TSkeleton.
	ENDREM
	Method getName:String()
		Return String.FromCString( SKELETON_getName( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Helper function to create a TSkeleton from a reference/pointer.
	ENDREM
	Function FromPtr:TSkeleton(Pointer:Byte Ptr) 
		Local Obj:TSkeleton = New TSkeleton
		Obj.Pointer = Pointer
		Return Obj
	End Function
End Type

Rem
	bbdoc: A map of indexes.  This type is not yet fully implemented.
ENDREM
Type TIndexMap
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Describes a managed instance of a skeleton. This type is not yet fully implemented.
ENDREM
Type TSkeletonInstance Extends TSkeleton
End Type

Rem
	bbdoc: Describes a temporary blended buffer. This type is not yet fully implemented.
ENDREM
Type TTempBlendedBufferInfo
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: An abstract class used to load scripts and resources for factory objects, etc.  Classes deriving from this class require explicit destruction via the destroy method. Due to constraints on how the objects are handle, garbage collection is not possible.
ENDREM
Type TScriptLoader Abstract

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Allows for manual destruction of this object. Due to the nature of the objects that derive from this class, garbage collector destruction is not allowed.
	ENDREM
	Method destroy()
		SL_destroy( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the file patterns which should be used for scripts of this class.
	ENDREM
	Method getScriptPatterns:TStringVector() 
		Local Obj:TStringVector = New TStringVector
		Obj.Pointer = SL_getScriptPatterns( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Parses any script from a data stream/reference to the script which is created by giving the datastream a file name or handle to the resource script.<br>stream:TOgreDataStream  Standard data stream to be used.<br>groupName:String If any resources are created from the data stream, they will belong to the group specified.
	ENDREM
	Method parseScript(stream:TOgreDataStream, groupName:String)
		SL_parseScript( stream.Pointer , groupName.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the loading order of scripts of this type. The higher the number, the later the script is loaded.
	ENDREM
	Method getLoadingOrder:Float() 
		Return SL_getLoadingOrder( Self.Pointer )
	End Method
	
End Type

Rem
	bbdoc: Singleton for managing resource groups.
ENDREM
Type TResourceGroupManager
	
	Rem
		bbdoc: Initialise a single resource group by name.
	ENDREM
	Function initialiseResourceGroup(name:String)
		RGM_initialiseResourceGroup(name.ToCString())
	End Function
	
	Rem
		bbdoc: Initialise all resource groups specified.
	ENDREM
	Function initialiseAllResourceGroups()
		RGM_initialiseAllResourceGroups()
	End Function
	
	Rem
		bbdoc: Remove and unload all resources in a named resource group.
	ENDREM
	Function clearResourceGroup(name:String)
		RGM_clearResourceGroup(name.ToCString())
	End Function
	
	Rem
		bbdoc: Remove and unload all resources in a named resource group, then destroy the group.
	ENDREM
	Function destroyResourceGroup(name:String)
		RGM_destroyResourceGroup(name.ToCString())
	End Function

	Rem
		bbdoc: Add a resource location to a given resource group.<br/>name:String The name of the resource location. This could be a directory, zip file, etc.<br/>A string the specifies which type of resource this is. "FileSystem" and "Archive" are the most common.<br/>resGroup:String - The resource group name that this location falls under.
	ENDREM
	Function addResourceLocation(name:String, locType:String, resGroup:String, recursive:Byte = False)
		RGM_addResourceLocation(name.ToCString() , locType.ToCString() , resGroup.ToCString() , recursive)
	End Function
	
	Rem
		bbdoc: Remove a resource group by specifying its name and group that it belongs to.
	ENDREM
	Function removeResourceLocation( name:String , resGroup:String )
		RGM_removeResourceLocation( Byte Ptr(name.ToCString() ) , Byte Ptr( resGroup.ToCString() ) ) 
	End Function
	
	Rem
		bbdoc: This helper function loads a resource configuration file like resource.cfg in the sample applications cfg directory.
	ENDREM
	Function loadResourceFile( resourceFile$ )
		FLOW_loadResourceFile( Byte Ptr( resourceFile.ToCString()) )
	End Function
	
	Rem
		bbdoc:  Check to see if a given resource exists.
	ENDREM
	Function resourceExists:Byte(group:String, filename:String) 
		Return RGM_resourceExists( group.ToCString() , filename.ToCString() )
	End Function
	 
End Type

Rem
	bbdoc: Static type that manages the ogre log.
ENDREM
Type TLogManager

	Rem
		bbdoc: Reference to object in memory. Only valid if create() is called.
	ENDREM
	Field Pointer:Byte Ptr

	Rem
		bbdoc: Create a new log manager instance. This is used to create an instance before Ogre root is created. Ogre attaches itself automatically to the new instance, giving you more control over how logging is initialized.
	ENDREM
	Function Create:TLogManager()
		Local Obj:TLogManager = New TLogManager
		Obj.Pointer = LM_create()
		Return Obj
	End Function

	Rem
		bbdoc: Create a new log with the specified parameters.
	ENDREM
	Function createLog:TOgreLog(name:String, defaultLog:Byte = False, debuggerOutput:Byte = True, suppressFileOutput:Byte = False)
		Local Obj:TOgreLog = New TOgreLog
		Obj.Pointer = LM_createLog(name.ToCString() , defaultLog, debuggerOutput, suppressFileOutput)
		Return Obj
	End Function

	Rem
		bbdoc: Add a message to the Ogre log file.
	ENDREM
	Function logMessage( loggingMessageLevel:Int , message$ , debug:Int )
		LM_logMessage( loggingMessageLevel , message.toCString() , debug )	
	End Function
	
	Rem
		bbdoc: Set the logging detail level.
	ENDREM
	Function setLogDetail( loggingLevel:Int)
		LM_setLogDetail( loggingLevel )
	End Function
	
	Rem
		bbdoc: Get the default TOgreLog used for logging.
	ENDREM
	Function getDefaultLog:TOgreLog()
		Local Obj:TOgreLog = New TOgreLog
		Obj.Pointer = LM_getDefaultLog()
		Return Obj
	End Function
	
End Type

Rem
	bbdoc: An ogre log instance that's used for handling log messages.
ENDREM
Type TOgreLog
	
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Set whether or not we want to output data to debug output/debug stream.
	ENDREM
	Method setDebugOutputEnabled(debugOutput:Byte)
		OGRELOG_setDebugOutputEnabled(debugOutput, Self.Pointer)
	End Method
End Type

Rem
	bbdoc: This static class handles the mesh resources in Ogre.
ENDREM
Type TMeshManager

	Rem
		bbdoc: Create a plane resource by specifying the resource name and resource group.<br/>width:Float and height:Float - The height and with of the plane in world units.<br/>xSegments:Int and ySegments:Int - Numbe of x and y segments.
	ENDREM
	Function createPlane( name$ , groupname$ , width:Float , height:Float , xSegments:Int , ySegments:Int )
		MM_createPlane( name.ToCString() , groupname.tocstring() , width , height , xSegments , ySegments )
	End Function
	
	Rem
		bbdoc: Load a mesh from a file by specifying the filename.<br/>groupName:String - The resource group name to store the mesh resource to.<br/>vertexBufferUsage:Int and indexBufferUsage:Int - Vertex and index buffer usage. Check THardwareBuffer.HBU_ vars for constants.<br/>vertexBufferShadowed:Byte and indexBufferShadowed:Byte - Shadow buffers with system memory copies for fast read access.
	ENDREM
	Function Load:TMesh(filename:String, groupName:String, vertexBufferUsage:Int = THardwareBuffer.HBU_STATIC_WRITE_ONLY, indexBufferUsage:Int = THardwareBuffer.HBU_STATIC_WRITE_ONLY, vertexBufferShadowed:Byte = True, indexBufferShadowed:Byte = True) 
		Return TMesh.FromPtr(MM_load(filename, groupName, vertexBufferUsage, indexBufferUsage, vertexBufferShadowed, indexBufferShadowed), 1) 
	End Function
	
	Rem
		bbdoc: Create a new manual mesh that is created rather than loaded from file.<br/>name:String - The name that this mesh resource will have.<br/>group:String - The groupname this resource will have.<br/>loader:TManualResourceLoader = Null - Specify a manual resource loader if you are using one.
	ENDREM
	Function createManual:TMesh(name:String, groupName:String, loader:TManualResourceLoader = Null) 
		If loader = Null Then
			Return TMesh.FromPtr(MM_createManual(name.ToCString() , groupName.ToCString() , Null), 1) 
		Else
			Return TMesh.FromPtr(MM_createManual(name.ToCString() , groupName.ToCString() , loader), 1) 
		End If
	End Function
	
	Rem
		bbdoc: Get a TMesh resource by name. Returns Null if the resource does not exist.
	ENDREM
	Function getByName:TResource(name:String) 
		Local Obj:TResource = New TResource
		Obj.Pointer = MM_getByName(name.ToCString()) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Check to see whether or not a resource exists.
	ENDREM
	Function resourceExists:Byte(name:String) 
		Return MM_resourceExists(name.ToCString()) 
	End Function
	
End Type

Rem
	bbdoc: Singleton for managing skeletons within Ogre.
ENDREM
Type TSkeletonManager Extends TResourceGroupManager

	Rem
		bbdoc: Create a new TSkeleton for use in this Ogre instance.
	ENDREM
	Function Create:TSkeleton(name:String, group:String, isManual:Byte = False, manual:TManualResourceLoader = Null, createParams:TNameValuePairList = Null)
		
		'Satisfy our custom type parameter defaults
		If manual = Null Then
			manual = New TManualResourceLoader
			manual.Pointer = Null
		End If
		
		If createParams = Null Then
			createParams = New TNameValuePairList
			createParams.Pointer = Null
		End If
		
		Return TSkeleton.FromPtr(SKELETONMANAGER_create(name.ToCString() , group.ToCString() , isManual, manual.Pointer, createParams.Pointer))
	End Function
	
	Rem
		bbdoc: Remove a managed TSkeleton by name from this Ogre instance. Note that it is not completely destroyed until all references to it are removed.
	ENDREM
	Function remove(name:String)
		SKELETONMANAGER_remove(name.ToCString())
	End Function
End Type

Rem
	bbdoc: A type for managing different compositors within Ogre. 
ENDREM
Type TCompositorManager Extends TResourceGroupManager

	Rem
		bbdoc: Adds a compositor to the given viewport. The parameter addPosition specifies where in the chain of compositors currently on the viewport to add this one, -1 meaning it defaults to the end of the compositor chain.
	ENDREM
	Function addCompositor:TCompositorInstance(vp:TViewport, compositor:String, addPosition:Int = -1) 
		Local Obj:TCompositorInstance = New TCompositorInstance
		Obj.Pointer = COMPMANAGER_addCompositor(vp.Pointer, compositor.ToCString() , addPosition) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Sets a compositor to an enabled or disabled state. Note that disabling a compositor does not free it's associated resource, and you should use removeCompositor if you wish to achieve this.
	ENDREM
	Function setCompositorEnabled(vp:TViewport, compositor:String, value:Byte) 
		COMPMANAGER_setCompositorEnabled(vp.Pointer, compositor.ToCString() , value) 
	End Function
	
	Rem
		bbdoc: Removes a given compositor from the viewport specified. This also frees resources associated with the given compositor.
	ENDREM
	Function removeCompositor(vp:TViewport, compositor:String) 
		COMPMANAGER_removeCompositor(vp.Pointer, compositor.ToCString()) 
	End Function
	
	Rem
		bbdoc: Remove all compositors and compositor chains from all viewports in this ogre instance.
	ENDREM
	Function removeAll()
		COMPMANAGER_removeAll()
	End Function

End Type

Rem
	bbdoc: A interface for manipulating compositor instances. This type is not yet fully implemented.
ENDREM
Type TCompositorInstance

	Rem
		bbdoc: A reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: A static type that manages material resources in Ogre.This type is not yet fully implemented. 
ENDREM
Type TMaterialManager
	
	Rem
		bbdoc: Creates and returns a TMaterial with the given parameters.<br>name:String The name to be used For the material resource.<br>group:String The resource group To be used For the material resource.	<br>isManual:Int Specifies If you want To use a ManualResourceLoader subclass.		<br>loader:TManualResourceLoader Specifies the manual resource loader that you want To use. Blitz Types are Not compatible with C++ classes, so If you need To use this Function, you must overload Ogre::ManualResourceLoader in C++ and send the pointer to FLOW.		<br>createParams:TNameValuePairList A valuepair list specifying the material's attributes.
	ENDREM
	Function Create:TMaterial(name:String, group:String, isManual:Int = False, loader:TManualResourceLoader = Null, createParams:TNameValuePairList = Null)
	
		'Satisfy our default custom objects
		If loader = Null Then
			loader = New TManualResourceLoader
			loader.Pointer = Null
		End If
		
		If createParams = Null Then
			createParams = New TNameValuePairList
			createParams.Pointer = Null
		End If
	
		Local Obj:TMaterial = New TMaterial
		Obj.Pointer = MATERIALMANAGER_create(name.ToCString() , group.ToCString() , isManual, loader.Pointer, createParams.Pointer)
		Return Obj
	End Function
End Type

Rem
	bbdoc: A static type that manages texture resources in Ogre.
ENDREM
Type TTextureManager
	
	Rem
		bbdoc: Load an image into the list of available textures using a TOgreImage.<br/>name:String - The name you want to give this managed TTexture.<br/>group:String - The resource group you want to give this managed TTexture.<br/>img:TOgreImage - The TOgreImage used to make the new TTexture.<br/>texType:Int = TTexture.TEX_TYPE_2D - The type of texture you want to create. Look At TTexture.TEX_TYPE* for your options.<br/>iNumMipMaps:Int = MIP_DEFAULT - Number of mip maps. Can use MIP_DEFAULT or MIP_UNLIMITED.<br/>gamma:Float = 1.0 - Gamma level for this image.<br/>isAlpha:Byte = False - Only applies to greyscale images, this specifies whether or not the loaded image is an alpha channel.<br/>desiredFormat:Int = PF_UNKNOWN - Desired pixel format for the created TTexture. Check flow_constants.bmx for all PF_ types.<br/>hwGammaCorrection:Byte = False - Hardware gamma correction, more accurate gamma correction if hardware supports it.
	ENDREM
	Function LoadImage:TTexture(name:String, group:String, img:TOgreImage, texType:Int = TTexture.TEX_TYPE_2D, iNumMipMaps:Int = MIP_DEFAULT, gamma:Float = 1.0, isAlpha:Byte = False, desiredFormat:Int = PF_UNKNOWN, hwGammaCorrection:Byte = False)
		Local Obj:TTexture = New TTexture
		Obj.Pointer = TEXTUREMANAGER_loadImage(name.ToCString() , group.ToCString() , img.Pointer, texType, iNumMipMaps, gamma, isAlpha, desiredFormat, hwGammaCorrection)
		Return Obj
	End Function
End Type

Rem
	bbdoc: An abstract type for specifying a generic hardware buffer.
ENDREM
Type THardwareBuffer

	Rem
		bbdoc: Hardware Buffer Usage : Static buffer that is rarely modified.
	ENDREM
	Const HBU_STATIC:Int = 1
	
	Rem
		bbdoc: Hardware Buffer Usage : Identifies buffer usage that will be modified often.
	ENDREM
	Const HBU_DYNAMIC:Int = 2
	
	Rem
		bbdoc: Hardware Buffer Usage : Buffer wll be only be written to. Generally useful for getting stuff into AGP memory.
	ENDREM
	Const HBU_WRITE_ONLY:Int = 4
	
	Rem
		bbdoc: Hardware Buffer Usage : Buffer will be discared and refilled when locked. If you lock a buffer with this you will get a new pointer returned.
	ENDREM
	Const HBU_DISCARDABLE:Int = 8
	
	Rem
		bbdoc: Hardware Buffer Usage : HBU_STATIC and HBU_WRITE_ONLY.
	ENDREM
	Const HBU_STATIC_WRITE_ONLY:Int = 5
	
	Rem
		bbdoc: Hardware Buffer Usage : HBU_DYNAMIC and HBU_WRITE_ONLY.
	ENDREM
	Const HBU_DYNAMIC_WRITE_ONLY:Int = 6
	
	Rem
		bbdoc: Hardware Buffer Usage : HBU_DYNAMIC , HBU_WRITE_ONLY and HBU_DISCARDABLE.
	ENDREM
	Const HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE:Int = 14
	
	Rem
		bbdoc: Hardware Buffer Lock Options : Discards the entire buffer when locked.
	ENDREM
	Const HBL_DISCARD:Int = 1
	
	Rem
		bbdoc: Hardware Buffer Lock Options : Not for buffers that use HUB_WRITE_ONLY. Guarantees no overwriting.
	ENDREM
	Const HBL_NO_OVERWRITE:Int = 3
	
	Rem
		bbdoc: Hardware Buffer Lock Options : Read and write enabled, normal buffer lock.
	ENDREM
	Const HBL_NORMAL:Int = 0
	
	Rem
		bbdoc: Hardware Buffer Lock Options : Read only buffer lock.
	ENDREM
	Const HBL_READ_ONLY:Int = 2
End Type

Rem
	bbdoc: Type that holds constants defining texture usage.
ENDREM
Type TTextureUsage

	Rem
		bbdoc: Texture Usage : Won't be written to often.
	ENDREM
	Const TU_STATIC:Int = THardwareBuffer.HBU_STATIC
	
	Rem
		bbdoc: Texture Usage : Will be written to often.
	ENDREM
	Const TU_DYNAMIC:Int = THardwareBuffer.HBU_DYNAMIC
	
	Rem
		bbdoc: Texture Usage : Write only.
	ENDREM
	Const TU_WRITE_ONLY:Int = THardwareBuffer.HBU_WRITE_ONLY
	
	Rem
		bbdoc: Texture Usage : TU_STATIC and TU_WRITE_ONLY
	ENDREM
	Const TU_STATIC_WRITE_ONLY:Int = THardwareBuffer.HBU_STATIC_WRITE_ONLY
	
	Rem
		bbdoc: Texture Usage : TU_DYNAMIC and TU_WRITE_ONLY
	ENDREM
	Const TU_DYNAMIC_WRITE_ONLY:Int = THardwareBuffer.HBU_DYNAMIC_WRITE_ONLY
	
	Rem
		bbdoc: Texture Usage : TU_DYNAMIC and TU_WRITE_ONLY and Discardable, which means that whole buffer will be refilled.
	ENDREM
	Const TU_DYNAMIC_WRITE_ONLY_DISCARDABLE:Int = THardwareBuffer.HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE
	
	Rem
		bbdoc: Texture Usage : Mipmaps auto-mipmap.
	ENDREM
	Const TU_AUTOMIPMAP:Int = 256
	
	Rem
		bbdoc: Texture Usage : Texture will be used for render target.
	ENDREM
	Const TU_RENDERTARGET:Int = 512
	
	Rem
		bbdoc: Texture Usage : Default texture setup where mipmaps are generated and textures are static write only.
	ENDREM
	Const TU_DEFAULT:Int = TU_AUTOMIPMAP | TU_STATIC_WRITE_ONLY
End Type

Rem
	bbdoc: Type that manages the scene object renderqueue.
ENDREM
Type TRenderQueue

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get a render queue group by its queue ID.
	ENDREM
	Method getQueueGroup:TRenderQueueGroup( quid:Int )
		Local Obj:TRenderQueueGroup = New TRenderQueueGroup
		Obj.Pointer = RQ_getQueuegroup( quid:Int , Self.Pointer )
		Return Obj
	End Method
End Type

Rem
	bbdoc: A subgroup of a renderqueue.
ENDREM
Type TRenderQueueGroup

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Set whether or not shadows are enabled for this render queue.
	ENDREM	
	Method setShadowsEnabled( enabled:Int )
		RQG_setShadowsEnabled( enabled , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get whether or not shadows are enabled for this render queue.
	ENDREM
	Method getShadowsEnabled:Int()
		Return RQG_getShadowsEnabled( Self.Pointer )
	End Method
	
End Type

Rem
	bbdoc: A class that wraps the basic TDegree and TRadian types.  When constructed, the type is initialized as the angle unit specified in TMath.setAngleUnit(...).
ENDREM
Type TAngle

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Constructor, takes an angle as a float. The value is processed as either a TRadian or TDegree, based on how TMath.setAngleUnit(...) is set.
	ENDREM
	Function Create:TAngle(angle:Float) 
		Local Obj:TAngle = New TAngle
		Obj.Pointer = ANGLE_create( angle )
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor
	ENDREM
	Method Delete()
		ANGLE_delete( Self.Pointer )
	End Method
	
End Type

Rem
	bbdoc: A class for loading and manipulating raw image files from a memory source such as the hard drive. Used in conjunction with TTextureManager to load textures from file easily.
ENDREM
Type TOgreImage
	
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Set whether or not this object is cleaned up with GC.
	ENDREM
	Field managed:Byte
	
	Rem
		bbdoc: Create this object from a Byte Ptr. You can specify whether or not this object is to be cleaned up by garbage collection by setting isManaged to true or false.
	ENDREM
	Function FromPtr:TOgreImage(pointer:Byte Ptr, isManaged:Byte = False)
		Local Obj:TOgreImage = New TOgreImage
		Obj.Pointer = pointer
		Obj.managed = isManaged
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a new, blank TOgreImage.
	ENDREM
	Function Create:TOgreImage()
		Return TOgreImage.FromPtr(OGREIMAGE_create())
	End Function
	
	Rem
		bbdoc: Destructor. Destroys this object if it's not managed.
	ENDREM
	Method Delete()
		If managed <> True And Pointer <> Null Then
			OGREIMAGE_delete(Self.Pointer)
			Pointer = Null
		End If
	End Method
	
	Rem
		bbdoc: Manual destructor. Note that you can only destroy this object if it is unmanaged.
	ENDREM
	Method Destroy()
		If managed <> True And Pointer <> Null Then
			OGREIMAGE_delete(Self.Pointer)
			Pointer = Null
		End If
	End Method
	
	Rem
		bbdoc: Load an image from file. You can load any format that Ogre itself supports. You must also specify a resource group for the image.
	ENDREM
	Method Load(strFileName:String, groupName:String)
		OGREIMAGE_load(strFileName.ToCString() , groupName.ToCString() , Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the width of the loaded image.
	ENDREM
	Method getWidth:Int()
		Return OGREIMAGE_getWidth(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the height of the loaded image.
	ENDREM
	Method getHeight:Int()
		Return OGREIMAGE_getHeight(Self.Pointer)
	End Method
	
End Type

Rem
	bbdoc: Describes a rendering operation that can be performed on a TVertexBuffer.
ENDREM
Type TRenderOperation

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Set whether or not this object is cleaned up with gc.
	ENDREM
	Field managed:Byte
	
	Rem
		bbdoc: Represents a list of points.
	ENDREM
	Const OT_POINT_LIST:Int = 1
	
	Rem
		bbdoc: Represents a list of lines.
	ENDREM
	Const OT_LINE_LIST:Int = 2
	
	Rem
		bbdoc: Represents a list of connected lines with 1 vertex per point as well as 1 starting vertex.
	ENDREM
	Const OT_LINE_STRIP:Int = 3
	
	Rem
		bbdoc: Represents a list of triangles.
	ENDREM
	Const OT_TRIANGLE_LIST:Int = 4
	
	Rem
		bbdoc: Represents a strip of connected triangles with 3 vertices for the first triangle and one after that.
	ENDREM
	Const OT_TRIANGLE_STRIP:Int = 5
	
	Rem
		bbdoc: Represents a fan of connected triangles with 3 vertices for the first triangle and one vertice thereafter.
	ENDREM
	Const OT_TRIANGLE_FAN:Int = 6
	
	Rem
		bbdoc: Create a TRenderOperation object from a Byte Ptr. If you set isManaged to TRUE, then this TRenderOperation will not be cleaned up by Garbage Collection. Do not specify that this TRenderOperation is managed to true unless you are certain that it will be cleaned up.
	ENDREM
	Function FromPtr:TRenderOperation(Pointer:Byte Ptr, isManaged:Byte = 0) 
		Local Obj:TRenderOperation = New TRenderOperation
		Obj.Pointer = Pointer
		Obj.managed = isManaged
		Return Obj
	End Function
	
	Rem
		bbdoc: Create an empty, unamanaged render operation.
	ENDREM
	Function Create:TRenderOperation() 
		Return TRenderOperation.FromPtr(RENDEROP_create()) 
	End Function
	
	Rem
		bbdoc: Destructor for any unmanaged TRenderOperations.
	ENDREM
	Method Delete() 
		If Self.managed = False Then RENDEROP_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the vertex data for this TRenderOperation.
	ENDREM
	Method getVertexData:TVertexData() 
		Return TVertexData.FromPtr(RENDEROP_getVertexData(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Set the vertex data for this TRenderOperation.
	ENDREM
	Method setVertexData(vertexData:TVertexData) 
		RENDEROP_setVertexData(vertexData.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the type of render operation that will be performed on the vertex data. You can find a list of constants in TRenderOperation.
	ENDREM
	Method getOperationType:Int() 
		Return RENDEROP_getOperationType(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the type of render operation that will be performed ont he vertex data. You can find a list of constants in TRenderOperation.
	ENDREM
	Method setOperationType(operationType:Int) 
		RENDEROP_setOperationType(operationType, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not this TRenderOperation uses index data.
	ENDREM
	Method getUseIndexes:Byte() 
		Return RENDEROP_getUseIndexes(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set whether or not this TRenderOperation uses index data.
	ENDREM
	Method setUseIndexes(useIndexes:Byte) 
		RENDEROP_setUseIndexes(useIndexes, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the index data for this TRenderOperation. Note that the pointer will only be valid of getUseIndexes returns true.
	ENDREM
	Method getIndexData:TIndexData() 
		Local Obj:TIndexData = New TIndexData
		Obj.Pointer = RENDEROP_getIndexData(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the index data for this TRenderOperation.
	ENDREM
	Method setIndexData(indexData:TIndexData) 
		RENDEROP_setIndexData(indexData.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the source renderable that's responsible for creating this render operation.
	ENDREM
	Method getSrcRenderable:TRenderable() 
		Local Obj:TRenderable = New TRenderable
		Obj.Pointer = RENDEROP_getSrcRenderable(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the source renderable that's responsible for creating this render operation.
	ENDREM
	Method setSrcRenderable(srcRenderable:TRenderable) 
		RENDEROP_setSrcRenderable(srcRenderable.Pointer, Self.Pointer) 
	End Method
		
End Type

Rem
	bbdoc: Class that holds vertex source information.
ENDREM
Type TVertexData

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Set whether or not this object is cleaned up with garbage collection.
	ENDREM
	Field managed:Byte
	
	Rem
		bbdoc: Create a TVertexData object from a Byte Ptr. If you set isManaged to TRUE, then this TVertexData will not be cleaned up by Garbage Collection. Do not specify that this TVertexData is managed to true unless you are certain that it will be cleaned up.
	ENDREM
	Function FromPtr:TVertexData(Pointer:Byte Ptr, isManaged:Byte = 0) 
		Local Obj:TVertexData = New TVertexData
		Obj.Pointer = Pointer
		Obj.managed = isManaged
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a blank, unmanaged TVertexData object.
	ENDREM
	Function Create:TVertexData() 
		Return TVertexData.FromPtr(VERTEXDATA_create()) 
	End Function
	
	Rem
		bbdoc: Check to see whether or not this TVertexData object is a Null vertex data object.
	ENDREM
	Method isNull:Byte() 
		If Self.Pointer <> Null Then
			Return False
		Else
			Return True
		End If
	End Method
	
	Rem
		bbdoc: Deconstructor for unmanaged objects.
	ENDREM
	Method Delete() 
		If Self.managed = False Then VERTEXDATA_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the vertex declaration for this vertex data.
	ENDREM
	Method GetVertexDeclaration:TVertexDeclaration() 
		Local Obj:TVertexDeclaration = New TVertexDeclaration
		Obj.Pointer = VERTEXDATA_getVertexDeclaration(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the vertex declaration for this vertex data.
	ENDREM
	Method SetVertexDeclaration(vertexDeclaration:TVertexDeclaration) 
		VERTEXDATA_setVertexDeclaration(vertexDeclaration.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the vertex buffer binding for this vertex data.
	ENDREM
	Method GetVertexBufferBinding:TVertexBufferBinding() 
		Local Obj:TVertexBufferBinding = New TVertexBufferBinding
		Obj.Pointer = VERTEXDATA_getVertexBufferBinding(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the vertex buffer binding for this vertex data.
	ENDREM
	Method SetVertexBufferBinding(vertexBufferBinding:TVertexBufferBinding) 
		VERTEXDATA_setVertexBufferBinding(vertexBufferBinding.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the index that this vertex data starts from.
	ENDREM
	Method getVertexStart:Int() 
		Return VERTEXDATA_getVertexStart(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the index that this vertex data starts from.
	ENDREM
	Method setVertexStart(vertexStart:Int) 
		VERTEXDATA_setVertexStart(vertexStart, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the number of vertices used in the vertex data operation.
	ENDREM
	Method getVertexCount:Int() 
		Return VERTEXDATA_getVertexCount(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the number of vertices used in the vertex data operation.
	ENDREM 
	Method setVertexCount(vertexCount:Int) 
		VERTEXDATA_setVertexCount(vertexCount, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the vertex elements used for hardware morphing and posing animations.
	ENDREM
	Method getHwAnimationDataList:THardwareAnimationDataList() 
		Local Obj:THardwareAnimationDataList = New THardwareAnimationDataList
		Obj.Pointer = VERTEXDATA_getHwAnimationDataList(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the vertex elements used for hardware morphing and posing animations.
	ENDREM
	Method setHwAnimationDataList(hwAnimationDataList:THardwareAnimationDataList) 
		VERTEXDATA_setHwAnimationDataList(hwAnimationDataList.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the number of hardware animation data items used.
	ENDREM
	Method getHwAnimDataItemsUsed:Int() 
		Return VERTEXDATA_getHwAnimDataItemsUsed(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the number of hardware animation data items used.
	ENDREM
	Method setHwAnimDataItemsUsed(hwAnimDataItemsUsed:Int) 
		VERTEXDATA_setHwAnimDataItemsUsed(hwAnimDataItemsUsed, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the shadow volume vertex buffer.
	ENDREM
	Method getHardwareShadowVolWBuffer:THardwareVertexBuffer() 
		Return THardwareVertexBuffer.FromPtr(VERTEXDATA_getHardwareShadowVolWBuffer(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Set the shadow volume vertex buffer.
	ENDREM
	Method setHardwareShadowVolWBuffer(hardwareShadowVolWBuffer:THardwareVertexBuffer) 
		VERTEXDATA_setHardwareShadowVolWBuffer(hardwareShadowVolWBuffer.Pointer, Self.Pointer) 
	End Method
	
	
End Type


Rem
	bbdoc: A type that describes the format of vertex inputs.
ENDREM
Type TVertexDeclaration

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get the number of elements in this vertex declaration.
	ENDREM
	Method getElementCount:Int() 
		Return VERTEXDECLARATION_getElementCount( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Add an element to the vertex declaration.
	ENDREM
	Method addElement:TVertexElement(source:Int, offset:Int, theType:Int, semantic:Int, index:Int = 0)
		Local Obj:TVertexElement = New TVertexElement
		Obj.Pointer = VERTEXDECLARATION_addElement(source, offset, theType, semantic, index, Self.Pointer)
		Return Obj
	End Method
	

End Type

Rem
	bbdoc: Type that describes the state of all the vertex buffer bindings required to make a vertex declaration.
ENDREM
Type TVertexBufferBinding

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Set the binding to associate a vertex buffer with a given index.
	ENDREM
	Method setBinding(index:Int, buffer:THardwareVertexBuffer)
		VBB_setBinding(index, buffer.Pointer, Self.Pointer)
	End Method
End Type

Rem
	bbdoc: Describes the usage of a single vertex buffer as part of the vertex declaration.
ENDREM
Type TVertexElement

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		VERTEXELEMENT_delete(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Static function to get the size of a vertex element type. See TVertexElementType for vertex type constants.
	ENDREM
	Function getTypeSize:Int(etype:Int)
		Return VERTEXELEMENT_getTypeSize(eType)
	End Function
End Type

Rem
	bbdoc: Singleton manager for hardware buffers, including vertex buffers.
ENDREM
Type THardwareBufferManager
	
	Rem
		bbdoc: Create a managed vertex buffer with the specified parameters.
	ENDREM
	Function createVertexBuffer:THardwareVertexBuffer(vertSize:Int, numVerts:Int, usage:Int, useShadowBuffer:Byte = False)
		Local Obj:THardwareVertexBuffer = New THardwareVertexBuffer
		Obj.Pointer = HBM_createVertexBuffer(VERTSIZE, numVerts, usage, useShadowBuffer)
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a managed index buffer with the specified parameters.
	ENDREM
	Function createIndexBuffer:THardwareIndexBuffer(iType:Int, numINdexes:Int, usage:Int, useShadowBuffer:Byte = False)
		Local Obj:THardwareIndexBuffer = New THardwareIndexBuffer
		Obj.Pointer = HBM_createIndexBuffer(iType, numIndexes, usage, useShadowBuffer)
		Return Obj
	End Function
	
End Type

Rem
	bbdoc: Interface for buffer/addresses in memory where vertex data is stored.
ENDREM
Type THardwareVertexBuffer

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create a THardwareVertexBuffer object from a Byte Ptr. 
	ENDREM
	Function FromPtr:THardwareVertexBuffer(Pointer:Byte Ptr) 
		Local Obj:THardwareVertexBuffer = New THardwareVertexBuffer
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
	Rem
		bbdoc: Get the size of this buffer in bytes.
	ENDREM
	Method getSizeInBytes:Int()
		Return HVB_getSizeInBytes(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Write data to this vertex buffer using a TFloatList.
	ENDREM
	Method writeDataWithFloatList(offset:Int, length:Int, pSource:TFLoatList, discardWholeBuffer:Byte = False)
		HVB_writeDataWithFloatList(offset, length, pSource.Pointer, discardWholeBuffer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Destructor. This doesn't destroy the vertex buffer, only the reference to it that this object makes.
	ENDREM
	Method Delete()
		HVB_delete(Self.Pointer)
	End Method
	
End Type

Rem
	bbdoc: Interface for buffer/addresses in memory where index data is stored.
ENDREM
Type THardwareIndexBuffer

	Rem
		bbdoc: 16-bit index type.
	ENDREM
	Const IT_16BIT:Int = 0
	
	Rem
		bbdoc: 32-bit index type.
	ENDREM
	Const IT_32BIT:Int = 1

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create a THardwareIndexBuffer object from a Byte Ptr. 
	ENDREM
	Function FromPtr:THardwareIndexBuffer(Pointer:Byte Ptr)
		Local Obj:THardwareIndexBuffer = New THardwareIndexBuffer
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
	Rem
		bbdoc: Get the size of this buffer in bytes.
	ENDREM
	Method getSizeInBytes:Int()
		Return HIB_getSizeInBytes(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Write data to this index buffer using a TIndexList.
	ENDREM
	Method writeDataWithIndexList(offset:Int, length:Int, pSource:TIndexList, discardWholeBuffer:Byte = False)
		HIB_writeDataWithIndexList(offset, length, pSource.Pointer, discardWholeBuffer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Destructor. This doesn't destroy the index buffer, only the reference to it that this object makes.
	ENDREM
	Method Delete()
		HIB_delete(Self.Pointer)
	End Method
	
End Type


Rem
	bbdoc: Describes a hardware animation data list. This type is not yet implemented.
ENDREM
Type THardwareAnimationDataList

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Type describing index data information.
ENDREM
Type TIndexData

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Set the index buffer for this index data.
	ENDREM
	Method setIndexBuffer(indexBuffer:THardwareIndexBuffer)
		INDEXDATA_setIndexBuffer(indexBuffer.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the index starting point/index.
	ENDREM
	Method setIndexStart(indexStart:Int)
		INDEXDATA_setIndexStart(indexStart, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the number of indexes that will be used from the index buffer.
	ENDREM
	Method setIndexCount(indexCount:Int)
		INDEXDATA_setIndexCount(indexCount, Self.Pointer)
	End Method
	
End Type


Rem
	bbdoc: Describes a renderer type to render particle systems in an Ogre scene. This type is not yet fully implemented.
ENDREM
Type TParticleSystemRenderer

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Represents a single particle instance. Note that to create particle instances you must create them with yourparticlesystem.createParticle(...). Please see the documentation on createParticle(...) for the caveats on creating particles.
ENDREM
Type TParticle

	Rem
		bbdoc: Reference to the particle object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Set the dimensions of this TParticle. Note that this only sets the individual dimensions of the particle, and you must check to see make sure that hasOwnDimensions() returns true.<br/>Note that particle systems using different sizes of particles are markedly less efficient than particle systems that use particles that are all the same size.
	ENDREM
	Method setDimensions(width:Float, height:Float)
		PARTICLE_setDimensions(width, height, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Check to see whether this TParticle uses it's own dimension settings or if it uses the parent Particle System's set size.  Note that a set size is more efficient than all particles having their own sizes.
	ENDREM
	Method hasOwnDimensions:Byte()
		Return PARTICLE_hasOwnDimensions(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the individual width of this TParticle. Note it may not be the actual width if hasOwnDimensions() is false.
	ENDREM
	Method getOwnWidth:Float()
		Return PARTICLE_getOwnWidth(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the individual height of this TParticle. Note it may not be the actual height if hasOwnDimensions() is false.
	ENDREM
	Method getOwnHeight:Float()
		Return PARTICLE_getOwnHeight(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Specify the rotation of this TParticle with a TRadian.
	ENDREM
	Method SetRotation(rad:TRadian)
		PARTICLE_setRotation(rad.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the rotation of this TParticle as a TRadian.
	ENDREM
	Method GetRotation:TRadian()
		Local Obj:TRadian = New TRadian
		Obj.Pointer = PARTICLE_getRotation(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Internal method that notifies this particle of its owner.
	ENDREM
	Method _notifyOwner(owner:TParticleSystem)
		PARTICLE__notifyOwner(owner.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED: Internal method to notify this particle of it's extended visual data. Note that in general you will not use this for anything, it is for specializing and extending particle type classes and you can't extend C++ classes from BlitzMax types.
	ENDREM
	Method _notifyVisualData(vis:TParticleVisualData)
		PARTICLE__notifyVisualData(vis.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED: Get extra visual data associated with this particle type. Note that in general you will not use this for anything, it is for specializing and extending particle type classes and you can't extend C++ classes from BlitzMax types. 
	ENDREM
	Method getVisualData:TParticleVisualData()
		Local Obj:TParticleVisualData = New TParticleVisualData
		Obj.Pointer = PARTICLE_getVisualData(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Reset the dimensions of this TParticle.
	ENDREM
	Method resetDimensions()
		PARTICLE_resetDimensions(Self.Pointer)
	End Method
	
	'Accessors---
	
	Rem
		bbdoc: Set whether or not you want this particle to inherit standard sizing from its parent Particle System, or use its own height and width. Note that using the same particle sizes for every particle in the Particle System is markedly more efficient.
	ENDREM
	Method setHasOwnDimensions(hasOwnDimensions:Byte)
		PARTICLE_setHasOwnDimensions(hasOwnDimensions, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the world position of this particle.
	ENDREM
	Method setPosition(position:TVector3)
		PARTICLE_setPosition(position.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the world position of this particle.
	ENDREM
	Method getPosition:TVector3()
		Return TVector3.FromPtr(PARTICLE_getPosition(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Set the direction of this particle.  Also the magnitude specifies the speed of the direction.
	ENDREM
	Method setDirection(direction:TVector3)
		PARTICLE_setDirection(direction.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the direction of this particle. The magnitude of the vector given also specifies its speed.
	ENDREM
	Method getDirection:TVector3()
		Return TVector3.FromPtr(PARTICLE_getDirection(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Set the current colour of this TParticle.
	ENDREM
	Method setColour(colour:TColourValue)
		PARTICLE_setColour(colour.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the current colour of this TParticle.
	ENDREM
	Method getColour:TColourValue()
		Return TColourValue.FromPtr(PARTICLE_getColour(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Set the number of seconds that this particle has left to live.
	ENDREM
	Method setTimeToLive(timeToLive:Float)
		PARTICLE_setTimeToLive(timeToLive, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the number of seconds that this particle has left to live.
	ENDREM
	Method getTimeToLive:Float()
		Return PARTICLE_getTimeToLive(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the total number of seconds of this particle's life.
	ENDREM
	Method setTotalTimeToLive(timeToLive:Float)
		PARTICLE_setTotalTimeToLive(timeToLive, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the total number of seconds of this particle's life.
	ENDREM
	Method getTotalTimeToLive:Float()
		Return PARTICLE_getTotalTimeToLive(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the current speed of rotation in radians per second.
	ENDREM
	Method setRotationSpeed(rotationSpeed:TRadian)
		PARTICLE_setRotationSpeed(rotationSpeed.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the current speed of rotations in radians per second.
	ENDREM
	Method getRotationSpeed:TRadian()
		Local Obj:TRadian = New TRadian
		Obj.Pointer = PARTICLE_getRotationSpeed(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Set the particle type which corresponds to the constants in TParticleType.
	ENDREM
	Method setParticleType(particleType:Int)
		PARTICLE_setParticleType(particleType, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the particle type which corresponds to the constants in TParticleType.  
	ENDREM
	Method getParticleType:Int()
		Return PARTICLE_getParticleType(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Allows for easy casting to type TParticleEmitter.
	ENDREM
	Method toParticleEmitter:TParticleEmitter()
		Local Obj:TParticleEmitter = New TParticleEmitter
		Obj.Pointer = Self.Pointer
		Return Obj
	End Method
	
	Rem
		bbdoc: Check to see if this TParticle has a null reference.
	ENDREM
	Method isNotNull:Byte()
		If Self.Pointer <> Null Then
			Return True
		Else
			Return False
		End If
	End Method
	
		
End Type

Rem
	bbdoc: Structure that holds constant values for particle types.
ENDREM
Type TParticleType
	Rem
		bbdoc: Particle Type : Standard visual particle.
	ENDREM
	Const Visual:Int = 0
	
	Rem
		bbdoc: Particle Type : Particle that emits particles.
	ENDREM
	Const Emitter:Int = 1
End Type


Rem
	bbdoc: Base type for particle emitters.
ENDREM
Type TParticleEmitter Extends TParticle
	
	Rem
		bbdoc: Initialises a particle with this emitter's parameters.
	ENDREM
	Method _initParticle( pParticle:TParticle )
		PARTICLEEMITTER__initParticle( pParticle.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Enable this emitter. 
	ENDREM
	Method SetEnabled(enabled:Byte) 
		PARTICLEEMITTER_setEnabled(enabled, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not this emitter is enabled.
	ENDREM
	Method getEnabled:Byte() 
		Return PARTICLEEMITTER_getEnabled(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the base colour of this emitter's particles.
	ENDREM
	Method setColour(colour:TColourValue) 
		PARTICLEEMITTER_setColour( colour.Pointer , Self.Pointer )
	End Method
	
End Type

Rem
	bbdoc: NOT SUPPORTED: Hold extra visual data about particle systems. Only meant for extending and specialisation, and you cannot extend C++ classes from BlitzMax currently.
ENDREM
Type TParticleVisualData

	Rem
		bbdoc: Reference to the particle object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Describes a particle affector. This class is not yet implemented fully.
ENDREM
Type TParticleAffector

	Rem
		bbdoc: Reference to the particle object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Iterate through a list of particles. This type is not yet fully implemented.
ENDREM
Type TParticleIterator

	Rem
		bbdoc: Reference to the particle object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: A list of plane bounded volumes. This type is not yet fully implemented.
ENDREM
Type TPlaneBoundedVolumeList

	Rem
		bbdoc: Reference to the particle object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: An edge list for shadows, etc. This type is not yet fully implemented.
ENDREM
Type TEdgeList

	Rem
		bbdoc: Reference to the particle object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type


Rem
	bbdoc: Generic resource handler implementation. This type is not yet fully implemented.
ENDREM
Type TResourceManager

	Rem
		bbdoc: Reference to the particle object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
End Type


Rem
	bbdoc: A pointer to a TTexture. This type is being phased out and is not supported.
ENDREM
Type TTexturePtr Extends TTexture

	Rem
		bbdoc: Reference to the particle object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		TEXTUREPTR_delete( Self.Pointer )
	End Method
End Type

Rem
	bbdoc: A name value pair list that keeps a list of pairs of strings for storing settings.
ENDREM
Type TNameValuePairList
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create an empty name-value pair list.
	ENDREM
	Function Create:TNameValuePairList()
		Local Obj:TNameValuePairList = New TNameValuePairList
		Obj.Pointer = NVPL_create()
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		NVPL_delete( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Insert a key/value pair into the list using strings. The pair will be added to the end of the list.
	ENDREM
	Method insert( key:String , value:String )
		NVPL_insert( key.ToCString() , value.ToCString() , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns an iterator pointing to the beginning of this name-value pair list. This will most commonly be used to retreive the beginning of the list so you can begin to iterate thoguh it.
	ENDREM
	Method begin:TNameValuePairListIter() 
		Local Obj:TNameValuePairListIter = New TNameValuePairListIter
		Obj.Pointer = NVPL_begin(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Returns an iterator pointing To the end of this name-value pair list. This will most commonly be used to compare the current iterator to the end of the list to see if you are at the end. Note that end is a keyword in BlitzMax and could not be used, so "ending" was put in as a substitute.
	ENDREM
	Method ending:TNameValuePairListIter() 
		Local Obj:TNameValuePairListIter = New TNameValuePairListIter
		Obj.Pointer = NVPL_end(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Helper function to determine whether or not the reference to this name-value pair list object is null.
	ENDREM
	Method isNull:Byte() 
		If Self.Pointer = Null Then
			Return True
		Else
			Return False
		End If
	End Method
	
End Type

Rem
	bbdoc: An iterator class that allows you to iterate through TNameValuePairList values efficiently.
ENDREM
Type TNameValuePairListIter
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create an empty iterator.
	ENDREM
	Function Create:TNameValuePairListIter() 
		Local Obj:TNameValuePairListIter = New TNameValuePairListIter
		Obj.Pointer = NVPLI_create() 
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete() 
		NVPLI_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Assignment method. IterA.eq( IterB ) is IterA = IterB.
	ENDREM
	Method eq(rkIterator:TNameValuePairListIter) 
		NVPLI_eq(rkIterator.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Non-equality logical test method. IterA.isNotEqualTo(IterB) is If IterA <> IterB.
	ENDREM
	Method isNotEqualTo:Byte(rkIterator:TNameValuePairListIter) 
		Return NVPLI_isNotEqualTo(rkIterator.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Equality logical test method. IterA.isEqualTo(IterB) is If IterA = IterB.
	ENDREM
	Method isEqualTo:Byte(rkIterator:TNameValuePairListIter) 
		Return NVPLI_isEqualTo(rkIterator.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Increments this iterator foreward once, allowing you to access the first and second values in the next element in the TNameValuePairList.
	ENDREM
	Method increment() 
		NVPLI_increment(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Decrements this iterator backwards once, allowing you to access the first and second values in the previous element in the TNameValuePairList.
	ENDREM
	Method decrement() 
		NVPLI_decrement(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gives you access to the first value ( key ) in the element that this iterator currently points to. The key is a string.
	ENDREM
	Method first:String() 
		Return String.FromCString(NVPLI_first(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Gives you access to the second value ( value ) in the element that this iterator currently points to. The value is a string.
	ENDREM
	Method second:String() 
		Return String.FromCString(NVPLI_second(Self.Pointer)) 
	End Method
End Type

Rem
	bbdoc: A list that holds a set of TVector3s representing a list of vertices.
ENDREM
Type TVertexList
	
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create an empty vertex list.
	ENDREM
	Function Create:TVertexList()
		Local Obj:TVertexList = New TVertexList
		Obj.Pointer = VERTEXLIST_create()
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor.					  
	ENDREM
	Method Delete()
		VERTEXLIST_delete(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Add a TVector3 to the end of this vertex list.
	ENDREM
	Method push_back(vector:TVector3)
		VERTEXLIST_push_back(vector.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get a vertice specified by the given index from this TVectorList.
	ENDREM
	Method getValue:TVector3(index:Int)
		Local Obj:TVector3 = New TVector3
		Obj.Pointer = VERTEXLIST_getValue(index, Self.Pointer)
		Obj.managed = True
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the number of elements in this list.
	ENDREM
	Method size:Int()
		Return VERTEXLIST_size(Self.Pointer)
	End Method
	
End Type

Rem
	bbdoc: A list that holds a set of longs that correspond to index data.
ENDREM
Type TIndexList
	
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create an empty index list.
	ENDREM
	Function Create:TIndexList()
		Local Obj:TIndexList = New TIndexList
		Obj.Pointer = INDEXLIST_create()
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor.					  
	ENDREM
	Method Delete()
		INDEXLIST_delete(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Add an index to this index list.
	ENDREM
	Method push_back(index:Int)
		INDEXLIST_push_back(index, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get an indice with the specified index from this index list.
	ENDREM
	Method getValue:Int(index:Int)
		Return INDEXLIST_getValue(index, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Helper for adding a triangle index to the index list.
	ENDREM
	Method addTriangle(t1:Int, t2:Int, t3:Int)
		INDEXLIST_addTriangle(t1, t2, t3, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the number of elements in this list.
	ENDREM
	Method size:Int()
		Return INDEXLIST_size(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Reserve the number of elements you plan to use in memory for this list. This will help performance by not resizing the list every time you add an element. Note that this will not change the size of the list.
	ENDREM
	Method reserve(size:Int)
		INDEXLIST_reserve(size, Self.Pointer)
	End Method
	
End Type

Rem
	bbdoc: A list that holds only floating point values.
ENDREM
Type TFLoatList
	
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create an empty float list.
	ENDREM
	Function Create:TFLoatList()
		Local Obj:TFLoatList = New TFLoatList
		Obj.Pointer = FLOATLIST_create()
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		FLOATLIST_delete(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the number of elements in this list.
	ENDREM
	Method size:Int()
		Return FLOATLIST_size(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Reserve the number of elements you plan to use in memory for this list. This will help performance by not resizing the list every time you add an element. Note that this will not change the size of the list.
	ENDREM
	Method reserve(size:Int)
		FLOATLIST_reserve(size, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Push a value onto the end of this list.
	ENDREM
	Method push_back(value:Float)
		FLOATLIST_push_back(value, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Push 3 floating point values to the end of the list.
	ENDREM
	Method add3DPoint(p1:Float, p2:Float, p3:Float)
		FLOATLIST_add3DPoint(p1, p2, p3, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Add 2 floating point values to the end of this list.
	ENDREM
	Method add2DPoint(p1:Float, p2:Float)
		FLOATLIST_add2DPoint(p1, p2, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Helper function for adding UV coordinates to the end of this list.
	ENDREM
	Method addUV(u:Float, v:Float)
		FLOATLIST_add2DPoint(u, v, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Helper function for adding vertex position to the end of the list.
	ENDREM
	Method addVertex(p1:Float, p2:Float, p3:Float)
		FLOATLIST_add3DPoint(p1, p2, p3, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Helper function for adding normal coords to the end of the list.
	ENDREM
	Method addNormal(p1:Float, p2:Float, p3:Float)
		FLOATLIST_add3DPoint(p1, p2, p3, Self.Pointer)
	End Method
	
	
	Rem
		bbdoc: Clear the list of all elements.
	ENDREM
	Method clear()
		FLOATLIST_clear(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get a value from this floatlist by index.
	ENDREM
	Method getValue:Float(index:Int)
		Return FLOATLIST_getValue(index, Self.Pointer)
	End Method
	
	
	
End Type

Rem
	bbdoc: A class that represents a single quaternion.
ENDREM
Type TQuaternion

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Specify whether or not this object is cleaned up by garbage collection.
	ENDREM
	Field managed:Int
	
	Rem
		bbdoc: Create a TQuaternion from a Byte Ptr. If you set isManaged to TRUE, then this TQuaternion will not be cleaned up by Garbage Collection. Do not specify that this TQuaternion is managed to true unless you are certain that it will be cleaned up.
	ENDREM
	Function FromPtr:TQuaternion(Pointer:Byte Ptr, isManaged:Int = 0) 
		Local Obj:TQuaternion = New TQuaternion
		Obj.Pointer = Pointer
		Obj.managed = isManaged
		Return Obj
	End Function
	
	Rem
		bbdoc: Create an empty TQuaternion. {  w=1.0 , x=0.0 , y=0.0 , z=0.0 }
	ENDREM
	Function createEmpty:TQuaternion() 
		Return TQuaternion.FromPtr(QUAT_createEmpty()) 
	End Function
	
	Rem
		bbdoc: Create a TQuaternion with the familiar { w , x , y , z }  components as floats.
	ENDREM
	Function Create:TQuaternion(fW:Float = 1.0, fX:Float = 0.0, fY:Float = 0.0, fZ:Float = 0.0) 
		Return TQuaternion.FromPtr(QUAT_create(fW, fX, fY, fZ)) 
	End Function
	
	Rem
		bbdoc: Create a TQuaternion from the values of another TQuaternion, effectively copying it.
	ENDREM
	Function createWithQuaternion:TQuaternion(rkQ:TQuaternion) 
		Return TQuaternion.FromPtr(QUAT_createWithQuaternion(rkQ.Pointer)) 
	End Function
	
	Rem
		bbdoc: Create a TQuaternion by getting the rotation values from a TMatrix3.
	ENDREM
	Function createWithMatrix3:TQuaternion(rot:TMatrix3) 
		Return TQuaternion.FromPtr(QUAT_createWithMatrix3(rot.Pointer)) 
	End Function
		
	Rem
		bbdoc: Create a TQuaternion from an angle-axis pair.<br>rfAngle:TRadian The angle to use.<br>rkAxis:TVector3 the 3d axis to use.
	ENDREM
	Function createWithAngleAxis:TQuaternion(rfAngle:TRadian, rkAxis:TVector3) 
		Return TQuaternion.FromPtr(QUAT_createWithAngleAxis(rfAngle.Pointer, rkAxis.Pointer)) 
	End Function
	
	Rem
		bbdoc: Create a TQuaternion from 3 Orthonormal Axes.
	ENDREM
	Function createWith3OrthoAxes:TQuaternion(xAxis:TVector3, yAxis:TVector3, zAxis:TVector3) 
		Return TQuaternion.FromPtr(QUAT_createWith3OrthoAxes(xaxis.Pointer, yaxis.Pointer, zaxis.Pointer)) 
	End Function
	
	Rem
		bbdoc: This is similar to the vanilla constructor for TQuaternion, but instead of taking four floats, it takes a float ptr, which is assumed to have four floats in contingent memory at the address of the pointer. Usage: <br> Global array:Float[] = [ 1.0 , 2.0 , 3.0 , 4.0 ] <br> Global quat1:TQuaternion = TQuaternion.createFromFloatPtr( array ) <br>
	ENDREM
	Function createFromFloatPtr:TQuaternion(valptr:Float Ptr) 
		Return TQuaternion.FromPtr(QUAT_createWithFloatPtr(valptr)) 
	End Function
	
	Rem
		bbdoc: Gets the value based on an index. You may think of this as a work-around for using [] brackets to signify array indexing. <br> quat1.getValue(i) can be used for convieniently looping thorugh values, etc. <br>i:Int 0-3 value that is mapped like [ w , x , y , z ]
	ENDREM
	'@params: 
	'@returns: The float value at the given index.
	Method getValue:Float( i:Int )
		Return QUAT_getValue( i , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the value based on an index. You may think of this as a work-around for using [] brackets to signify array indexing. <br> quat1.setValue(i) can be used for convieniently looping thorugh values, etc. <br>i:Int 0-3 value that is mapped like [ w , x , y , z ]
	ENDREM
	Method SetValue(i:Int, Value:Float) 
		QUAT_setValue(i, value, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Populate this TQuaternion from a rotation matrix ( TMatrix3 ).
	ENDREM
	Method FromRotationMatrix(kRot:TMatrix3) 
		QUAT_FromRotationMatrix( kRot.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Populate a rotation matrix with values from this TQuaternion.<br>kRot:TMatrix3 The rotation matrix to populte with this TQuaternion's data.
	ENDREM
	Method ToRotationMatrix(kRot:TMatrix3) 
		QUAT_ToRotationMatrix( kRot.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Populate this TQuaternion from Angle-Axis data.<br>rfAngle:TRadian The angle ( radian ).<br>rkAxis:TVector3 The axis.
	ENDREM
	Method FromAngleAxis(rfAngle:TRadian, rkAxis:TVector3) 
		QUAT_FromAngleAxis( rfAngle.Pointer , rkAxis.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Populate an angle and axis with TQuaternion data.<br>rfAngle:TRadian The angle variable to populate.<br>rkAxis:TVector3 The axis variable to populate.
	ENDREM
	Method ToAngleAxis(rfAngle:TRadian, rkAxis:TVector3) 
		QUAT_ToAngleAxis( rfAngle.Pointer , rkAxis.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Populate an angle and axis with TQuaternion data. In this instance the angle is a TDegree.<br>dAngle:TDegree The angle to populate in degrees.<br>rkAxis:TVector3 The axis to populate
	ENDREM
	Method ToAngleAxisWithDegree(dAngle:TDegree, rkAxis:TVector3) 
		QUAT_ToAngleAxisWithDegree( dAngle.Pointer , rkAxis.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Populate this TQuaternion from three axes.
	ENDREM
	Method FromAxes(xAxis:TVector3, yAxis:TVector3, zAxis:TVector3) 
		QUAT_FromAxes(xAxis.Pointer, yAxis.Pointer, zAxis.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Populate three axes with data from this TQuaternion.
	ENDREM
	Method ToAxes(xAxis:TVector3, yAxis:TVector3, zAxis:TVector3) 
		QUAT_ToAxes(xAxis.Pointer, yAxis.Pointer, zAxis.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the local x-axis from this TQuaternion.
	ENDREM
	Method xAxis:TVector3() 
		Return TVector3.FromPtr(QUAT_xAxis(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the local y-axis from this TQuaternion.
	ENDREM
	Method yAxis:TVector3() 
		Return TVector3.FromPtr(QUAT_yAxis(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the local z-axis from this TQuaternion.
	ENDREM
	Method zAxis:TVector3() 
		Return TVector3.FromPtr(QUAT_zAxis(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Equals operator for TQuaternions. Example: a.eq(b) is the same as a = b. 
	ENDREM
	Method Eq(rkQ:TQuaternion) 
		QUAT_eq(rkQ.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Addition operator for TQuaternions. Example: a.add(b) is the same as a + b.
	ENDREM
	Method add:TQuaternion(rkQ:TQuaternion) 
		Return TQuaternion.FromPtr(QUAT_add(rkQ.Pointer, Self.Pointer)) 
	End Method
	
	
	Rem
		bbdoc: Subtraction operator for TQuaternions. Example: a.sub(b) is the same as a - b.
	ENDREM
	Method sub:TQuaternion(rkQ:TQuaternion) 
		Return TQuaternion.FromPtr(QUAT_sub(rkQ.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Multiplication operator for TQuaternions. Example: a.mul(b) is the same as a * b.
	ENDREM
	Method mul:TQuaternion(rkQ:TQuaternion) 
		Return TQuaternion.FromPtr(QUAT_mul(rkQ.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Mutliply this TQuaternion with a scalar component. Example: a.mulScalar(5) is the same as a * 5.
	ENDREM
	Method mulWithScalar:TQuaternion(fScalar:Float) 
		Return TQuaternion.FromPtr(QUAT_mulWithScalar(fScalar, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Negativity operator. Effectively adds a - sign to the front of this TQuaternino. Example: a.negative() is the same as -a.
	ENDREM
	Method negative:TQuaternion() 
		Return TQuaternion.FromPtr(QUAT_negative(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Conditional equality test operator for TQuaternions. Example: a.isEqualTo(b) is the same is If a = b.
	ENDREM
	Method isEqualTo:Int(rhs:TQuaternion) 
		Return QUAT_isEqualTo(rhs.Pointer, Self.Pointer) 
	End Method
		
	Rem
		bbdoc: Conditional inequality test operator for TQuaternions. Example: a.isNotEqualTo(b) is the same as if a <> b.
	ENDREM
	Method isNotEqualTo:Int(rhs:TQuaternion) 
		Return QUAT_isNotEqualTo(rhs.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the dot product of this TQuaternion with another TQuaternion.
	ENDREM
	Method Dot:Float(rkQ:TQuaternion) 
		Return QUAT_Dot(rkQ.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the squared length of this TQuaternion.
	ENDREM
	Method Norm:Float() 
		Return QUAT_Norm(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Normalises the TQuaternion then returns it's previous length.
	ENDREM
	Method normalise:Float() 
		Return QUAT_normalise(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the inverse of this TQuaternion.
	ENDREM
	Method Inverse:TQuaternion() 
		Return TQuaternion.FromPtr(QUAT_Inverse(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the unitized inverse of this TQuaternion.
	ENDREM
	Method UnitInverse:TQuaternion() 
		Return TQuaternion.FromPtr(QUAT_UnitInverse(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the exponential of the TQuaternion.
	ENDREM
	Method Exp:TQuaternion() 
		Return TQuaternion.FromPtr(QUAT_Exp(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the logarithm of the TQuaternion.
	ENDREM
	Method Log:TQuaternion() 
		Return TQuaternion.FromPtr(QUAT_Log(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Multiply this TQuaternion by a TVector3.
	ENDREM
	Method mulWithVector3:TVector3(rkVector:TVector3) 
		Return TVector3.FromPtr(QUAT_mulWithVector3(rkVector.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the local Roll for this TQuaternion.<br>reprojectAxis:Int If true, use the intuitive angle to get the roll. If false, use actual yaw to get the result. Returns in radians.
	ENDREM
	Method getRoll:TRadian(reprojectAxis:Byte = True) 
		Local Obj:TRadian = New TRadian
		Obj.Pointer = QUAT_getRoll(reprojectAxis, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the local Pitch for this TQuaternion.<br>reprojectAxis:Int If true, use the intuitive angle to get the pitch. If false, use actual yaw to get the result. Returns in radians.
	ENDREM
	Method getPitch:TRadian(reprojectAxis:Byte = True) 
		Local Obj:TRadian = New TRadian
		Obj.Pointer = QUAT_getPitch(reprojectAxis, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the local Yaw for this TQuaternion. <br>reprojectAxis:Int If true, use the intuitive angle to get the yaw. If false, use actual yaw to get the result. Returns in radians.
	ENDREM
	Method getYaw:TRadian(reprojectAxis:Byte = True) 
		Local Obj:TRadian = New TRadian
		Obj.Pointer = QUAT_getYaw(reprojectAxis, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Conditional equality test operator for TQuaternions that includes angle tolerance as a TRadian.<br>rhs:TQuaternion  The TQuaternion to to test equality with.<br>tolerance:TRadian The maximum angle difference for the equality operation as a TRadian.<br>Returns true if the TQuaternions are equal, False if they are not.
	ENDREM
	Method isEqualToWithTolerance:Int(rhs:TQuaternion, tolerance:TRadian) 
		Return QUAT_isEqualToWithTolerance(rhs.Pointer, tolerance.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: This method returns a string describing all of the values of this TQuaternion. Format: "Quaternion( w , x , y , z )"
	ENDREM
	Method toStr:String() 
		Return String.FromCString(QUAT_toStr(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete() 
		If Self.managed = False Then QUAT_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the w component of this quaternion.
	ENDREM
	Method getW:Float() 
		Return QUAT_getW(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the x component of this quaternion.
	ENDREM
	Method getX:Float() 
		Return QUAT_getX(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the y component of this quaternion.
	ENDREM
	Method getY:Float() 
		Return QUAT_getY(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the z component of this quaternion.
	ENDREM
	Method getZ:Float() 
		Return QUAT_getZ(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the w component of this quaternion.
	ENDREM
	Method setW(w:Float) 
		QUAT_setW(w, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the x component of this quaternion.
	ENDREM
	Method setX(x:Float) 
		QUAT_setX(x, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the y component of this quaternion.
	ENDREM
	Method setY(y:Float) 
		QUAT_setY(y, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the z component of this quaternion.
	ENDREM
	Method setZ(z:Float) 
		QUAT_setZ(z, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the w,x,y and z components of this quaternion at once.
	ENDREM
	Method setAll(w:Float, x:Float, y:Float, z:Float) 
		QUAT_setAll(w, x, y, z, Self.Pointer) 
	End Method
	
	
	Rem
		bbdoc: Spherical Linear Interpolation helper function.
	ENDREM
	Function Slerp:TQuaternion(fT:Float, rkP:TQuaternion, rkQ:TQuaternion, shortestPath:Int = False) 
		Return TQuaternion.FromPtr(QUAT_Slerp(fT, rkP.Pointer, rkQ.Pointer, shortestPath)) 
	End Function
	
	Rem
		bbdoc: Spherical Linear Interpolation with extra spins helper function.
	ENDREM
	Function SlerpExtraSpins:TQuaternion(fT:Float, rkP:TQuaternion, rkQ:TQuaternion, iExtraSpins:Int) 
		Return TQuaternion.FromPtr(QUAT_SlerpExtraSpins(fT, rkP.Pointer, rkQ.Pointer, iExtraSpins)) 
	End Function
	
	Rem
		bbdoc: Setup for Spherical Quadradic Interpolation helper function.
	ENDREM
	Function Intermediate(rkQ0:TQuaternion, rkQ1:TQuaternion, rkQ2:TQuaternion, rka:TQuaternion, rkB:TQuaternion) 
		QUAT_Intermediate(rkQ0.Pointer, rkQ1.Pointer, rkQ2.Pointer, rka.Pointer, rkB.Pointer) 
	End Function

	Rem
		bbdoc: Spherical Quadratic Interpolation helper function.
	ENDREM
	Function Squad:TQuaternion(fT:Float, rkP:TQuaternion, rkA:TQuaternion, rkB:TQuaternion, rkQ:TQuaternion, shortestPath:Int = False) 
		Return TQuaternion.FromPtr(QUAT_Squad(fT, rkP.Pointer, rkA.Pointer, rkB.Pointer, rkQ.Pointer, shortestPath)) 
	End Function
	
	Rem
		bbdoc: Normalized Linear Interpolation helper function.
	ENDREM
	Function nlerp:TQuaternion(fT:Float, rkP:TQuaternion, rkQ:TQuaternion, shortestPath:Int = False) 
		Return TQuaternion.FromPtr(QUAT_nlerp(fT, rkP.Pointer, rkQ.Pointer, shortestPath)) 
	End Function
	
	Rem
		bbdoc: Get ms_fEpsilon, commonly used in SLERP, etc.
	ENDREM
	Function ms_fEpsilon:Float() 
		Return QUAT_ms_fEpsilon()[0] 
	End Function
	
	Rem
		bbdoc: Get a ZERO TQuaternion.
	ENDREM
	Function ZERO:TQuaternion() 
		Return TQuaternion.FromPtr(QUAT_ZERO()) 
	End Function
	
	Rem
		bbdoc: Get an IDENTITY TQuaternion.
	ENDREM
	Function IDENTITY:TQuaternion() 
		Return TQuaternion.FromPtr(QUAT_IDENTITY()) 
	End Function
	
End Type

Rem
	bbdoc: A Type For representing 2d vectors.
ENDREM
Type TVector2

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Specify whether or not this object is cleaned up by garbage collection.
	ENDREM
	Field managed:Int
	
	Rem
		bbdoc:  Create a TVector2 from a Byte Ptr. If you set isManaged to TRUE, then this TVector2 will not be cleaned up by Garbage Collection. Do not specify that this TVector2 is managed to true unless you are certain that it will be cleaned up.
	ENDREM
	Function FromPtr:TVector2(Pointer:Byte Ptr, isManaged:Int = 0) 
		Local Obj:TVector2 = New TVector2
		Obj.Pointer = Pointer
		Obj.managed = isManaged
		Return Obj
	End Function
	
	Rem
		bbdoc: Create an empty 2-dimensional vector.
	ENDREM
	Function createEmpty:TVector2() 
		Return FromPtr(VECTOR2_createEmpty()) 
	End Function
	
	Rem
		bbdoc: Create a 2-dimensional vector with the values specified.
	ENDREM
	Function Create:TVector2(fX:Float, fY:Float) 
		Return FromPtr(VECTOR2_create(fX, fY)) 
	End Function
	
	Rem
		bbdoc: Create a 2-dimensional vector with a single scalar. This fills both dimensions with the same scalar value.
	ENDREM
	Function createWithScalar:TVector2(scalar:Float) 
		Return FromPtr(VECTOR2_createWithScalar(scalar)) 
	End Function
		
	Rem
		bbdoc: Get a value from this TVector2.  <br> This is mostly for mimicing array/collection access as BlitzMax collections do not work with c arrays.<br>i:Int The index of the value to retreive. [ x , y ] = [ 0 , 1 ]
	ENDREM
	Method getValue:Float(i:Int) 
		Return VECTOR2_getValue(i, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set a value in this TVector2.  <br> This is mostly for mimicing array/collection access as BlitzMax collections do not work with c arrays.<br>i:Int The index of the value to retreive. [ x , y ] = [ 0 , 1 ]
	ENDREM
	Method SetValue(i:Int, Value:Float) 
		VECTOR2_setValue(i, value, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Create a 2-dimensional array with values specified in an array/collection of floats. Usage: <br> Global array:Float[] = [ 1.0 , 2.0 ] <br> Global vec1:TVector2 = TVector2.createFromFloatPtr( array ) <br>
	ENDREM
	Function createWithFloatPtr:TVector2(r:Float Ptr) 
		Return FromPtr(VECTOR2_createWithFloatPtr(r)) 
	End Function

	Rem
		bbdoc: Create a 2-dimensional vector from another 2-dimensional vector, effectively copying values over to a new TVector2.
	ENDREM
	Function createWithVector2:TVector2(rkVector:TVector2) 
		Return FromPtr(VECTOR2_createWithVector2(rkVector.Pointer)) 
	End Function

	Rem
		bbdoc: Equals operator for TVector2. Example: a.eq(b) is the same as a = b. 
	ENDREM
	Method Eq(rkVector:TVector2) 
		VECTOR2_eq(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Equals operator for TVector2 using a single scalar value. Example: a.eqWithScalar(2) is the same as a = 2.
	ENDREM
	Method eqWithScalar(fScalar:Float) 
		VECTOR2_eqWithScalar(fScalar, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Conditional equality test operator for TVector2. Example: a.isEqualTo(b) is the same is If a = b.
	ENDREM
	Method isEqualTo:Int(rkVector:TVector2) 
		Return VECTOR2_isEqualTo(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Conditional inequality test operator for TVector2. Example: a.isNotEqualTo(b) is the same is If a <> b.
	ENDREM
	Method isNotEqualTo:Int(rkVector:TVector2) 
		Return VECTOR2_isNotEqualTo(rkVector.Pointer, Self.Pointer) 
	End Method

	Rem
		bbdoc: Addition operator For TVector2 with a scalar value. Example: a.add(2) is the same as a + 2.
	ENDREM
	Method addWithScalar:TVector2(fScalar:Float) 
		Return TVector2.FromPtr(VECTOR2_addWithScalar(fScalar, Self.Pointer)) 
	End Method
		
	Rem
		bbdoc: Addition operator For TVector2. Example: a.add(b) is the same as a + b.
	ENDREM
	Method add:TVector2(rkVector:TVector2) 
		Return TVector2.FromPtr(VECTOR2_add(rkVector.Pointer, Self.Pointer)) 
	End Method
	
	
	Rem
		bbdoc: Subtraction operator for TVector2 with a scalar. Example: a.sub(2) is the same as a - 2.
	ENDREM.
	Method subWithScalar:TVector2(fScalar:Float) 
		Return TVector2.FromPtr(VECTOR2_subWithScalar(fScalar, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Subtraction operator for TVector2. Example: a.sub(b) is the same as a - b.
	ENDREM
	Method sub:TVector2(rkVector:TVector2) 
		Return TVector2.FromPtr(VECTOR2_sub(rkVector.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Multiplication operator for TVector2 with a scalar value. Example: a.mulWithScalar(2) is the same as a * 2.
	ENDREM
	Method mulWithScalar:TVector2(fScalar:Float) 
		Return TVector2.FromPtr(VECTOR2_mulWithScalar(fScalar, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Multiplication operator for TVector2. Example: a.mul(b) is the same as a * b.
	ENDREM
	Method mul:TVector2(rhs:TVector2) 
		Return TVector2.FromPtr(VECTOR2_mul(rhs.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Division operator for TVector2 with a scalar value. Example: a.divWithScalar(2) is the same as a / 2.
	ENDREM
	Method divWithScalar:TVector2(fScalar:Float) 
		Return TVector2.FromPtr(VECTOR2_divWithScalar(fScalar, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Division operator for TVector2. Example: a.div(b) is the same as a / b.
	ENDREM
	Method div:TVector2(rhs:TVector2) 
		Return TVector2.FromPtr(VECTOR2_div(rhs.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: + Prefix operator for TVector2. Example: a.positive() is the same as +a. For TVector2s this does not cause any change.
	ENDREM
	Method positive:TVector2() 
		Return TVector2.FromPtr(VECTOR2_positive(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: - Prefix operator for TVector2. Example: a.negative() is the same as -a.
	ENDREM
	Method negative:TVector2() 
		Return TVector2.FromPtr(VECTOR2_negative(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Addition-Equals operator for TVector2. Allows you to add a vector to this one in one operation. Example: a.addEq(b) is the same as a+=b.
	ENDREM
	Method addEq(rkVector:TVector2) 
		VECTOR2_addEq(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Addition-Equals operator for TVector2 with scalar. Allows you to add a scalar to this vector in one operation. Example: a.addEq(2) is the same as a+=2.
	ENDREM
	Method addEqWithScalar(fScalar:Float) 
		VECTOR2_addEqWithScalar(fScalar, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Subtract-Equals operator for TVector2. Allows you to subtract a vector to this one in one operation. Example: a.subEq(b) is the same as a-=b.
	ENDREM
	Method subEq(rkVector:TVector2) 
		VECTOR2_subEq(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Subtract-Equals operator for TVector2 with scalar. Allows you to subtract a scalar from this vector in one operation. Example: a.subEqWithScalar(2) is the same as a+=2.
	ENDREM
	Method subEqWithScalar(fScalar:Float) 
		VECTOR2_subEqWithScalar(fScalar, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Multiply-Equals operator for TVector2 with scalar. Allows you to multiply a scalar with this vector in one operation. Example: a.mulEqWithScalar(2) is the same as a*=2.
	ENDREM
	Method mulEqWithScalar(fScalar:Float) 
			VECTOR2_mulEqWithScalar(fScalar, Self.Pointer) 
	End Method
	
		
	Rem
		bbdoc: Multiply-Equals operator for TVector2. Allows you to multiply a vector with this one in one operation. Example: a.mulEq(b) is the same as a*=b.
	ENDREM
	Method mulEq(rkVector:TVector2) 
		VECTOR2_mulEq(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Divide-Equals operator for TVector2 with scalar. Allows you to divide this vector by a scalar in one operation. Example: a.divEqWithScalar(2) is the same as a/=2.
	ENDREM
	Method divEqWithScalar(fScalar:Float) 
			VECTOR2_divEqWithScalar(fScalar, Self.Pointer) 
	End Method
		
	Rem
		bbdoc: Divide-Equals operator for TVector2. Allows you to divide this vector by another vector in one operation. Example: a.divEq(b) is the same as a/=b.
	ENDREM
	Method divEq(rkVector:TVector2) 
		VECTOR2_divEq(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the length/magnitude of this TVector2.
	ENDREM
	Method length:Float() 
		Return VECTOR2_length(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the squared length/magnitude of this TVector2.
	ENDREM
	Method squaredLength:Float() 
		Return VECTOR2_squaredLength(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the dot product of this TVector2 and another TVector2.
	ENDREM
	Method dotProduct:Float(vec:TVector2) 
		Return VECTOR2_dotProduct(vec.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Normalise this TVector2 and return the previous length. Returns the previous length as a float.
	ENDREM
	Method normalise:Float() 
		Return VECTOR2_normalise(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the midway point between this TVector2 and another.<br>vec:TVector2 The other vector used to computer the midway point.
	ENDREM
	Method midPoint:TVector2(vec:TVector2) 
		Return TVector2.FromPtr(VECTOR2_midPoint(vec.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Less-Than evaluator for TVector2s. Example: a.isLessThan(b) is the same as a<b.
	ENDREM
	Method isLessThan:Int(rhs:TVector2) 
		Return VECTOR2_isLessThan(rhs.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Greater-Than evaluator for TVector2s. Example: a.isGreaterThan(b) is the same as a>b.
	ENDREM
	Method isGreaterThan:Int(rhs:TVector2) 
		Return VECTOR2_isGreaterThan(rhs.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Specify a TVector2 as a "floor" for this TVector2's values.
	ENDREM
	Method makeFloor(cmp:TVector2) 
		VECTOR2_makeFloor(cmp.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Specify a TVector2 as a "Ceiling" for this TVector2's values.
	ENDREM
	Method makeCeil(cmp:TVector2) 
		VECTOR2_makeCeil(cmp.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get a perpendicular vector to this TVector2.
	ENDREM
	Method perpendicular:TVector2() 
		Return TVector2.FromPtr(VECTOR2_perpendicular(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the cross-product of this TVector2 with another 2-dimensional vector.
	ENDREM
	Method crossProduct:Float(rkVector:TVector2) 
		Return VECTOR2_crossProduct(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Generate a random TVector2 that deviates a specified angle from the current vector.<br>angle:TRadian The angle at which to deviate the new TVector2 randomly.
	ENDREM
	Method randomDeviant:TVector2(angle:Float) 
		Return TVector2.FromPtr(VECTOR2_randomDeviant(angle, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get whether or not this vector is zero in length.
	ENDREM
	Method isZeroLength:Int() 
		Return VECTOR2_isZeroLength(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Same is normalise() except that it returns a normalised TVector2 instead of modifying this TVector2.
	ENDREM
	Method normalisedCopy:TVector2() 
		Return TVector2.FromPtr(VECTOR2_normalisedCopy(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Calculate this vector's reflection vector along a plane with the specified normal. <br>normal:TVector2 The normal of the plane to reflect from.
	ENDREM
	Method reflect:TVector2(normal:TVector2) 
		Return TVector2.FromPtr(VECTOR2_reflect(normal.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Format the values in this TVector2 as a String. Good For debugging Or tracking values quickly.
	ENDREM
	Method toStr:String() 
		Return String.FromCString(VECTOR2_toStr(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete() 
		If Self.managed = 0 Then VECTOR2_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the X component of this 2-dimensional vector.
	ENDREM
	Method getX:Float() 
		Return VECTOR2_getX(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the Y component of this 2-dimensional vector.
	ENDREM
	Method getY:Float() 
		Return VECTOR2_getY(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the X component of this 2-dimensional vector.
	ENDREM
	Method setX:Float(x:Float) 
		VECTOR2_setX(x, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the Y component of this 2-dimensional vector.
	ENDREM
	Method setY:Float(y:Float) 
		VECTOR2_setY(y, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set both the X and Y components of this 2-dimensional vector.
	ENDREM
	Method setAll(x:Float, y:Float) 
		VECTOR2_setAll(x, y, Self.Pointer) 
	End Method
		
	Rem
		bbdoc: Get a zero TVector2.
	ENDREM
	Function ZERO:TVector2() 
		Return TVector2.FromPtr(VECTOR2_ZERO()) 
	End Function
	
	Rem
		bbdoc: Get a x-unit TVector2.
	ENDREM
	Function UNIT_X:TVector2() 
		Return TVector2.FromPtr(VECTOR2_UNIT_X()) 
	End Function
	
	Rem
		bbdoc: Get a y-unit TVector2.
	ENDREM
	Function UNIT_Y:TVector2() 
		Return TVector2.FromPtr(VECTOR2_UNIT_Y()) 
	End Function

	Rem
		bbdoc: Get a negative x-unit TVector2.
	ENDREM
	Function NEGATIVE_UNIT_X:TVector2() 
		Return TVector2.FromPtr(VECTOR2_NEGATIVE_UNIT_X()) 
	End Function
	
	Rem
		bbdoc: Get a negative y-unit TVector2.
	ENDREM
	Function NEGATIVE_UNIT_Y:TVector2() 
		Return TVector2.FromPtr(VECTOR2_NEGATIVE_UNIT_Y()) 
	End Function
	
	Rem
		bbdoc: Get a scale unit TVector2.
	ENDREM
	Function UNIT_SCALE:TVector2() 
		Return TVector2.FromPtr(VECTOR2_UNIT_SCALE()) 
	End Function
	
End Type


Rem
	bbdoc: A type for representing 3d vectors.
ENDREM
Type TVector3

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Specify whether or not this object is cleaned up by garbage collection.
	ENDREM
	Field managed:Int
	
	Rem
		bbdoc:  Create a TVector3 from a Byte Ptr. If you set isManaged to TRUE, then this TVector3 will not be cleaned up by Garbage Collection. Do not specify that this TVector3 is managed to true unless you are certain that it will be cleaned up.
	ENDREM
	Function FromPtr:TVector3(Pointer:Byte Ptr, isManaged:Int = 0) 
		Local Obj:TVector3 = New TVector3
		Obj.Pointer = Pointer
		Obj.managed = isManaged
		Return Obj
	End Function
	
	Rem
		bbdoc: Create an empty 3-dimensional vector.
	ENDREM
	Function createEmpty:TVector3() 
		Return FromPtr(VECTOR3_createEmpty()) 
	End Function
	
	Rem
		bbdoc: Create a 3-dimensional array with the XYZ values specified.
	ENDREM
	Function Create:TVector3(fX:Float, fY:Float, fZ:Float) 
		Return FromPtr(VECTOR3_create(fX, fY, fZ)) 
	End Function
	
	Rem
		bbdoc: Create a 3-dimensional array with values specified in an array/collection of floats. Usage: <br> Global array:Float[] = [ 1.0 , 2.0 , 3.0  ] <br> Global vec1:TVector3 = TVector3.createFromFloatPtr( array ) <br>
	ENDREM
	Function createWithFloatPtr:TVector3(r:Float Ptr) 
		Return FromPtr(VECTOR3_createWithFloatPtr(r)) 
	End Function
	
	Rem
		bbdoc: Create a 3-dimensional vector with a single scalar. This fills all three dimensions with the same scalar value.
	ENDREM
	Function createWithScalar:TVector3(scalar:Float) 
		Return FromPtr(VECTOR3_createWithScalar(scalar)) 
	End Function
	
	Rem
		bbdoc: Create a 3-dimensional vector from another 3-dimensional vector, effectively copying values over to a new TVector3.
	ENDREM
	Function createWithVector3:TVector3(rkVector:TVector3) 
		Return FromPtr(VECTOR3_createWithVector3(rkVector.Pointer)) 
	End Function
	
	Rem
		bbdoc: Get a value from this TVector3.  <br> This is mostly for mimicing array/collection access as BlitzMax collections do not work with c arrays.<br>i:Int The index of the value to retreive. [ x , y , z ] = [ 0 , 1 , 2 ]
	ENDREM
	Method getValue:Float(i:Int) 
		Return VECTOR3_getValue(i, Self.Pointer) 
	End Method
		
	Rem
		bbdoc: Set a value in this TVector3.  <br> This is mostly for mimicing array/collection access as BlitzMax collections do not work with c arrays.<br>i:Int The index of the value to retreive. [ x , y , z ] = [ 0 , 1 , 2 ]
	ENDREM
	Method SetValue(i:Int, Value:Float) 
		VECTOR3_setValue(i, value, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Equals operator for TVector3. Example: a.eq(b) is the same as a = b. 
	ENDREM
	Method Eq(rkVector:TVector3) 
		VECTOR3_eq(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Equals operator for TVector3 using a single scalar value. Example: a.eq(2) is the same as a = 2.
	ENDREM
	Method eqWithScalar(fScalar:Float) 
		VECTOR3_eqWithScalar(fScalar, Self.Pointer) 
	End Method
	
	
	Rem
		bbdoc: Conditional equality test operator for TVector3. Example: a.isEqualTo(b) is the same is If a = b.
	ENDREM
	Method isEqualTo:Int(rkVector:TVector3) 
		Return VECTOR3_isEqualTo(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Conditional inequality test operator for TVector3. Example: a.isNotEqualTo(b) is the same is If a <> b.
	ENDREM
	Method isNotEqualTo:Int(rkVector:TVector3) 
		Return VECTOR3_isNotEqualTo(rkVector.Pointer, Self.Pointer) 
	End Method

	Rem
		bbdoc: Addition operator For TVector3 with a scalar value. Example: a.add(2) is the same as a + 2.
	ENDREM
	Method addWithScalar:TVector3(fScalar:Float) 
		Return TVector3.FromPtr(VECTOR3_addWithScalar(fScalar, Self.Pointer)) 
	End Method
		
	Rem
		bbdoc: Addition operator For TVector3. Example: a.add(b) is the same as a + b.
	ENDREM
	Method add:TVector3(rkVector:TVector3) 
		Return TVector3.FromPtr(VECTOR3_add(rkVector.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Subtraction operator for TVector3 with a scalar. Example: a.sub(2) is the same as a - 2.
	ENDREM
	Method subWithScalar:TVector3(fScalar:Float) 
		Return TVector3.FromPtr(VECTOR3_subWithScalar(fScalar, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Subtraction operator for TVector3. Example: a.sub(b) is the same as a - b.
	ENDREM
	Method sub:TVector3(rkVector:TVector3) 
		Return TVector3.FromPtr(VECTOR3_sub(rkVector.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Multiplication operator for TVector3 with a scalar value. Example: a.mulWithScalar(2) is the same as a * 2.
	ENDREM
	Method mulWithScalar:TVector3(fScalar:Float) 
		Return TVector3.FromPtr(VECTOR3_mulWithScalar(fScalar, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Multiplication operator for TVector3. Example: a.mul(b) is the same as a * b.
	ENDREM
	Method mul:TVector3(rhs:TVector3) 
		Return TVector3.FromPtr(VECTOR3_mul(rhs.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Division operator for TVector3 with a scalar value. Example: a.divWithScalar(2) is the same as a / 2.
	ENDREM
	Method divWithScalar:TVector3(fScalar:Float) 
		Return TVector3.FromPtr(VECTOR3_divWithScalar(fScalar, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Division operator for TVector3. Example: a.div(b) is the same as a / b.
	ENDREM
	Method div:TVector3(rhs:TVector3) 
		Return TVector3.FromPtr(VECTOR3_div(rhs.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: + Prefix operator for TVector3. Example: a.positive() is the same as +a. For TVector3s this does not cause any change.
	ENDREM
	Method positive:TVector3() 
		Return TVector3.FromPtr(VECTOR3_positive(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: - Prefix operator for TVector3. Example: a.negative() is the same as -a.
	ENDREM
	Method negative:TVector3() 
		Return TVector3.FromPtr(VECTOR3_negative(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Addition-Equals operator for TVector3. Allows you to add a vector to this one in one operation. Example: a.addEq(b) is the same as a+=b.
	ENDREM
	Method addEq(rkVector:TVector3) 
		VECTOR3_addEq(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Addition-Equals operator for TVector3 with scalar. Allows you to add a scalar to this vector in one operation. Example: a.addEq(2) is the same as a+=2.
	ENDREM
	Method addEqWithScalar(fScalar:Float) 
		VECTOR3_addEqWithScalar(fScalar, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Subtract-Equals operator for TVector3. Allows you to subtract a vector to this one in one operation. Example: a.subEq(b) is the same as a-=b.
	ENDREM
	Method subEq(rkVector:TVector3) 
		VECTOR3_subEq(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Subtract-Equals operator for TVector3 with scalar. Allows you to subtract a scalar from this vector in one operation. Example: a.subEqWithScalar(2) is the same as a+=2.
	ENDREM
	Method subEqWithScalar(fScalar:Float) 
		VECTOR3_subEqWithScalar(fScalar, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Multiply-Equals operator for TVector3 with scalar. Allows you to multiply a scalar with this vector in one operation. Example: a.mulEqWithScalar(2) is the same as a*=2.
	ENDREM
	Method mulEqWithScalar(fScalar:Float) 
			VECTOR3_mulEqWithScalar(fScalar, Self.Pointer) 
	End Method
	
		
	Rem
		bbdoc: Multiply-Equals operator for TVector3. Allows you to multiply a vector with this one in one operation. Example: a.mulEq(b) is the same as a*=b.
	ENDREM
	Method mulEq(rkVector:TVector3) 
		VECTOR3_mulEq(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Divide-Equals operator for TVector3 with scalar. Allows you to divide this vector by a scalar in one operation. Example: a.divEqWithScalar(b) is the same as a/=b.
	ENDREM
	Method divEqWithScalar(fScalar:Float) 
			VECTOR3_divEqWithScalar(fScalar, Self.Pointer) 
	End Method
	
		
	Rem
		bbdoc: Divide-Equals operator for TVector3. Allows you to divide this vector by another vector in one operation. Example: a.divEq(2) is the same as a/=2.
	ENDREM
	Method divEq(rkVector:TVector3) 
		VECTOR3_divEq(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the length/magnitude of this TVector3.
	ENDREM
	Method length:Float() 
		Return VECTOR3_length(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the squared length/magnitude of this TVector3.
	ENDREM
	Method squaredLength:Float() 
		Return VECTOR3_squaredLength(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the distance from this TVector3 to another TVector3.
	ENDREM
	Method distance:Float(rhs:TVector3) 
		Return VECTOR3_distance(rhs.Pointer, Self.Pointer) 
	End Method
		
	Rem
		bbdoc: Get the squared distance from this TVector3 to another TVector3.
	ENDREM
	Method squaredDistance:Float(rhs:TVector3) 
		Return VECTOR3_squaredDistance(rhs.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the dot product of this TVector3 and another TVector3.
	ENDREM
	Method dotProduct:Float(vec:TVector3) 
		Return VECTOR3_dotProduct(vec.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the absolute value dot product of this TVector3 and another TVector3.
	ENDREM
	Method absDotProduct:Float(vec:TVector3) 
		Return VECTOR3_absDotProduct(vec.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Normalise this TVector3 and return the previous length.
	ENDREM
	Method normalise:Float() 
		Return VECTOR3_normalise(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the cross-product of this Tvector3 with another 3-dimensional vector.
	ENDREM
	Method crossProduct:TVector3(rkVector:TVector3) 
		Return TVector3.FromPtr(VECTOR3_crossProduct(rkVector.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the midway point between this TVector3 and another.
	ENDREM
	Method midPoint:TVector3(vec:TVector3) 
		Return TVector3.FromPtr(VECTOR3_midPoint(vec.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Less-Than evaluator for TVector3s. Example: a.isLessThan(b) is the same as a<b. Returns true or false.
	ENDREM
	Method isLessThan:Int(rhs:TVector3) 
		Return VECTOR3_isLessThan(rhs.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Greater-Than evaluator for TVector3s. Example: a.isGreaterThan(b) is the same as a>b. Returns true or false.
	ENDREM
	Method isGreaterThan:Int(rhs:TVector3) 
		Return VECTOR3_isGreaterThan(rhs.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Specify a TVector3 as a "floor" for this TVector3's values.
	ENDREM
	Method makeFloor(cmp:TVector3) 
		VECTOR3_makeFloor(cmp.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Specify a TVector3 as a "Ceiling" for this TVector3's values.
	ENDREM
	Method makeCeil(cmp:TVector3) 
		VECTOR3_makeCeil(cmp.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get a perpendicular vector to this TVector3.
	ENDREM
	Method perpendicular:TVector3() 
		Return TVector3.FromPtr(VECTOR3_perpendicular(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Generate a random TVector3 that deviates a specified angle from the current vector.<br>angle:TRadian The angle at which to deviate the new TVector3 randomly.<br>up:TVector3 The up vector for generating the TVector3 from the angle. Default is TVector3.ZERO.
	ENDREM
	Method randomDeviant:TVector3(angle:TRadian, up:TVector3 = Null) 
		If up = Null Then up = TVector3.ZERO() 
		Return TVector3.FromPtr(VECTOR3_randomDeviant(angle.Pointer, up.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the shortest rotation arch from this TVector3 to another specified Tvector3.<br>dest:TVector3 The destination vector to find the shortest arc to.<br>fallbackAxis:Tvector3 The axis to rotate around. If not specified , an axis will be generated.
	ENDREM
	Method getRotationTo:TQuaternion(dest:TVector3, fallbackAxis:TVector3 = Null) 
		If fallbackAxis = Null Then fallbackAxis = TVector3.ZERO() 
		Return TQuaternion.FromPtr(VECTOR3_getRotationTo(dest.Pointer, fallbackAxis.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get whether or not this vector is zero in length.
	ENDREM
	Method isZeroLength:Int() 
		Return VECTOR3_isZeroLength(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Same is normalise() except that it returns a normalised TVector3 instead of modifying this TVector3.
	ENDREM
	Method normalisedCopy:TVector3() 
		Return TVector3.FromPtr(VECTOR3_normalisedCopy(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Calculate this vector's reflection vector along a plane with the specified normal.<br>normal:TVector3 The normal of the plane to reflect from.
	ENDREM
	Method reflect:TVector3(normal:TVector3) 
		Return TVector3.FromPtr(VECTOR3_reflect(normal.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Similar to isEqualTo, but with positional tolerance.<br>rhs:TVector3 The TVector3 to test equality with.<br>tolerance:Float The positional tolerance of the test.<br>True if equal within tolerance, false if not.
	ENDREM
	Method positionEquals:Int(rhs:TVector3, tolerance:Float = 1e-03) 
		Return VECTOR3_positionEquals(rhs.Pointer, tolerance, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Similar to positionEquals but takes the scale of the vectors into consideration.<br>rhs:TVector3 The TVector3 to test equality with.<br>tolerance:Float = 1e-03 The positional tolerance of the test.<br>True if equal within tolerance, false if not.
	ENDREM
	Method positionCloses:Int(rhs:TVector3, tolerance:Float = 1e-03) 
		Return VECTOR3_positionCloses(rhs.Pointer, tolerance, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not this TVector3's direction is equal to another TVectors, within tolerance. <br>rhs:TVector3 The TVector3's direction that you wish to test equality with.<br>tolerance:TRadian The angular tolerance.
	ENDREM
	Method directionEquals:Int(rhs:TVector3, tolerance:TRadian) 
		Return VECTOR3_directionEquals(rhs.Pointer, tolerance.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Format the values in this TVector3 as a String. Good For debugging Or tracking values quickly.
	ENDREM
	Method toStr:String() 
		Return String.FromCString(VECTOR3_toStr(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		If Self.managed = 0 Then VECTOR3_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Used for manual freeing of this TVector3. Use with caution.
	ENDREM
	Method Destroy()
		VECTOR3_delete(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the x component of this 3-dimensional vector.
	ENDREM
	Method getX:Float() 
		Return VECTOR3_getX(Self.Pointer) 
	End Method
	
		
	Rem
		bbdoc: Get the y component of this 3-dimensional vector.
	ENDREM
	Method getY:Float() 
		Return VECTOR3_getY(Self.Pointer) 
	End Method
	
		
	Rem
		bbdoc: Get the x component of this 3-dimensional vector.
	ENDREM
	Method getZ:Float() 
		Return VECTOR3_getZ(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the x component of this 3-dimensional vector.
	ENDREM
	Method setX(x:Float) 
		VECTOR3_setX(x, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the x component of this 3-dimensional vector.
	ENDREM
	Method setY(y:Float) 
		VECTOR3_setY(y, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the x component of this 3-dimensional vector.
	ENDREM
	Method setZ(z:Float) 
		VECTOR3_setZ(z, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the x , y  and z components of this 3-dimensional vector.
	ENDREM
	Method setAll(x:Float, y:Float, z:Float) 
		VECTOR3_setAll(x, y, z, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get a zero TVector3.
	ENDREM
	Function ZERO:TVector3() 
		Return TVector3.FromPtr(VECTOR3_ZERO()) 
	End Function
	
	Rem
		bbdoc: Get a x-unit TVector3.
	ENDREM
	Function UNIT_X:TVector3() 
		Return TVector3.FromPtr(VECTOR3_UNIT_X()) 
	End Function
	
	Rem
		bbdoc: Get a y-unit TVector3.
	ENDREM
	Function UNIT_Y:TVector3() 
		Return TVector3.FromPtr(VECTOR3_UNIT_Y()) 
	End Function
	
	Rem
		bbdoc: Get a z-unit TVector3.
	ENDREM
	Function UNIT_Z:TVector3() 
		Return TVector3.FromPtr(VECTOR3_UNIT_Z()) 
	End Function
	
	Rem
		bbdoc: Get a negative x-unit TVector3.
	ENDREM
	Function NEGATIVE_UNIT_X:TVector3() 
		Return TVector3.FromPtr(VECTOR3_NEGATIVE_UNIT_X()) 
	End Function
	
	Rem
		bbdoc: Get a negative y-unit TVector3.
	ENDREM
	Function NEGATIVE_UNIT_Y:TVector3() 
		Return TVector3.FromPtr(VECTOR3_NEGATIVE_UNIT_Y()) 
	End Function
	
	Rem
		bbdoc: Get a negative z-unit TVector3.
	ENDREM
	Function NEGATIVE_UNIT_Z:TVector3() 
		Return TVector3.FromPtr(VECTOR3_NEGATIVE_UNIT_Z()) 
	End Function
	
	Rem
		bbdoc: Get a unit scale TVector3.
	ENDREM
	Function UNIT_SCALE:TVector3() 
		Return TVector3.FromPtr(VECTOR3_UNIT_SCALE()) 
	End Function
	
	
	
End Type

Rem
	bbdoc: A type for representing 4d vectors.
ENDREM
Type TVector4

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Specify whether or not this object is cleaned up by garbage collection.
	ENDREM
	Field managed:Int
	
	Rem
		bbdoc:  Create a TVector4 from a Byte Ptr. If you set isManaged to TRUE, then this TVector4 will not be cleaned up by Garbage Collection. Do not specify that this TVector4 is managed to true unless you are certain that it will be cleaned up.
	ENDREM
	Function FromPtr:TVector4(Pointer:Byte Ptr, isManaged:Int = 0) 
		Local Obj:TVector4 = New TVector4
		Obj.Pointer = Pointer
		Obj.managed = isManaged
		Return Obj
	End Function
	
	Rem
		bbdoc: Create an empty 4-dimensional vector.
	ENDREM
	Function createEmpty:TVector4() 
		Return FromPtr(VECTOR4_createEmpty()) 
	End Function
	
	Rem
		bbdoc: Create a 4-dimensional array with the xyzw values specified.
	ENDREM
	Function Create:TVector4(fX:Float, fY:Float, fZ:Float, fW:Float) 
		Return FromPtr(VECTOR4_create(fX, fY, fZ, fW)) 
	End Function
	
	Rem
		bbdoc: Create a 4-dimensional vector with values specified in an array/collection of floats. Usage: <br> Global array:Float[] = [ 1.0 , 2.0 , 3.0 , 4.0  ] <br> Global vec1:TVector3 = TVector3.createFromFloatPtr( array ) <br>
	ENDREM
	Function createWithFloatPtr:TVector4(r:Float Ptr) 
		Return FromPtr(VECTOR4_createWithFloatPtr(r)) 
	End Function
	
	Rem
		bbdoc: Create a 4-dimensional vector with a single scalar. This fills all four dimensions with the same scalar value.
	ENDREM
	Function createWithScalar:TVector4(scalar:Float) 
		Return FromPtr(VECTOR4_createWithScalar(scalar)) 
	End Function
	
	Rem
		bbdoc: Create a 4-dimensional vector from a 3-dimensional vector, leaving the w-axis equal to one.
	ENDREM
	Function createWithVector3:TVector4(rhs:TVector3) 
		Return FromPtr(VECTOR4_createWithVector3(rhs.Pointer)) 
	End Function
	
	Rem
		bbdoc: Create a 4-dimensional vector from another 4-dimensional vector, effectively copying it.
	ENDREM
	Function createWithVector4:TVector4(rkVector:TVector4) 
		Return FromPtr(VECTOR4_createWithVector4(rkVector.Pointer)) 
	End Function
	
	Rem
		bbdoc: Get a value from this TVector4.  <br> This is mostly For mimicing array/collection access as BlitzMax collections do Not work with c arrays.<br>i:Int The index of the value to retreive. [ x , y , z , w ] = [ 0 , 1 , 2 , 3 ]
	ENDREM
	Method getValue:Float(i:Int) 
		Return VECTOR4_getValue(i, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set a value in this TVector4.  <br> This is mostly for mimicing array/collection access as BlitzMax collections do not work with c arrays.<br>i:Int The index of the value to retreive. [ x , y , z , w ] = [ 0 , 1 , 2 , 3 ]
	ENDREM
	Method SetValue(i:Int, Value:Float) 
		VECTOR4_setValue(i, value, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Equals operator for TVector4. Example: a.eq(b) is the same as a = b. 
	ENDREM
	Method Eq(rkVector:TVector4) 
		VECTOR4_eq(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Equals operator for TVector4 using a single scalar value. Example: a.eqWithScalar(2) is the same as a = 2.
	ENDREM
	Method eqWithScalar(fScalar:Float)
		VECTOR4_eqWithScalar(fScalar, Self.Pointer) 
	End Method
		
	Rem
		bbdoc: Conditional equality test operator for TVector4. Example: a.isEqualTo(b) is the same is If a = b.
	ENDREM
	Method isEqualTo:Int(rkVector:TVector4) 
		Return VECTOR4_isEqualTo(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Conditional inequality test operator for TVector4. Example: a.isNotEqualTo(b) is the same is If a <> b.
	ENDREM
	Method isNotEqualTo:Int(rkVector:TVector4) 
		Return VECTOR4_isNotEqualTo(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Equals operator to set a TVector4 equal to a TVector3. Example: a.eq(b) is the same as a = b.  ( The w axis will be set to 1 )
	ENDREM
	Method eqWithVector3(rhs:TVector3) 
		VECTOR4_eqWithVector3(rhs.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Addition operator For TVector4 with a scalar value. Example: a.addScalar(2) is the same as a + 2.
	ENDREM
	Method addWithScalar:TVector4(fScalar:Float) 
		Return TVector4.FromPtr(VECTOR4_addWithScalar(fScalar, Self.Pointer)) 
	End Method
		
	Rem
		bbdoc: Addition operator For TVector4. Example: a.add(b) is the same as a + b.
	ENDREM
	Method add:TVector4(rkVector:TVector4) 
		Return TVector4.FromPtr(VECTOR4_add(rkVector.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Subtraction operator for TVector4 with a scalar. Example: a.subWithScalar(2) is the same as a - 2.
	ENDREM
	Method subWithScalar:TVector4(fScalar:Float) 
		Return TVector4.FromPtr(VECTOR4_subWithScalar(fScalar, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Subtraction operator for TVector4. Example: a.sub(b) is the same as a - b.
	ENDREM
	Method sub:TVector4(rkVector:TVector4) 
		Return TVector4.FromPtr(VECTOR4_sub(rkVector.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Multiplication operator for TVector4 with a scalar value. Example: a.mulWithScalar(2) is the same as a * 2.
	ENDREM
	Method mulWithScalar:TVector4(fScalar:Float) 
		Return TVector4.FromPtr(VECTOR4_mulWithScalar(fScalar, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Multiplication operator for TVector4. Example: a.mul(b) is the same as a * b.
	ENDREM
	Method mul:TVector4(rhs:TVector4) 
		Return TVector4.FromPtr(VECTOR4_mul(rhs.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Division operator for TVector4 with a scalar value. Example: a.divWithScalar(2) is the same as a / 2.
	ENDREM
	Method divWithScalar:TVector4(fScalar:Float) 
		Return TVector4.FromPtr(VECTOR4_divWithScalar(fScalar, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Division operator for TVector4. Example: a.div(b) is the same as a / b.
	ENDREM
	Method div:TVector4(rhs:TVector4) 
		Return TVector4.FromPtr(VECTOR4_div(rhs.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: + Prefix operator for TVector4. Example: a.positive() is the same as +a. For TVector4s this does not cause any change.
	ENDREM
	Method positive:TVector4() 
		Return TVector4.FromPtr(VECTOR4_positive(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: - Prefix operator for TVector4. Example: a.negative() is the same as -a.
	ENDREM
	Method negative:TVector4() 
		Return TVector4.FromPtr(VECTOR4_negative(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Addition-Equals operator for TVector4. Allows you to add a vector to this one in one operation. Example: a.addEq(b) is the same as a+=b.
	ENDREM
	Method addEq(rkVector:TVector4) 
		VECTOR4_addEq(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Subtract-Equals operator for TVector4. Allows you to subtract a vector to this one in one operation. Example: a.subEq(b) is the same as a-=b.
	ENDREM
	Method subEq(rkVector:TVector4) 
		VECTOR3_subEq(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Multiply-Equals operator for TVector4 with scalar. Allows you to multiply a scalar with this vector in one operation. Example: a.mulEqWithScalar(2) is the same as a*=2.
	ENDREM
	Method mulEqWithScalar(fScalar:Float) 
			VECTOR4_mulEqWithScalar(fScalar, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Addition-Equals operator For TVector4 with scalar. Allows you To add a scalar To this vector in one operation. Example: a.addEq(b) is the same as a+=b.
	ENDREM
	Method addEqWithScalar(fScalar:Float) 
		VECTOR4_addEqWithScalar(fScalar, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Subtract-Equals operator for TVector4 with scalar. Allows you to subtract a scalar from this vector in one operation. Example: a.subEqWithScalar(2) is the same as a+=2.
	ENDREM
	Method subEqWithScalar(fScalar:Float) 
		VECTOR4_subEqWithScalar(fScalar, Self.Pointer) 
	End Method
	
			
	Rem
		bbdoc: Multiply-Equals operator for TVector4. Allows you to multiply a vector with this one in one operation. Example: a.mulEq(b) is the same as a*=b.
	ENDREM
	Method mulEq(rkVector:TVector4) 
		VECTOR4_mulEq(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Divide-Equals operator for TVector4 with scalar. Allows you to divide this vector by a scalar in one operation. Example: a.divEqWithScalar(b) is the same as a/=b.
	ENDREM
	Method divEqWithScalar(fScalar:Float) 
			VECTOR4_divEqWithScalar(fScalar, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Divide-Equals operator for TVector4. Allows you to divide this vector by another vector in one operation. Example: a.divEq(b) is the same as a/=b.
	ENDREM
	Method divEq(rkVector:TVector4) 
		VECTOR4_divEq(rkVector.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the dot product of this TVector4 and another TVector4.
	ENDREM
	Method dotProduct:Float(vec:TVector4) 
		Return VECTOR4_dotProduct(vec.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Format the values in this TVector4 as a String. Good For debugging Or tracking values quickly.
	ENDREM
	Method toStr:String() 
		Return String.FromCString(VECTOR4_toStr(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete() 
		If Self.managed = 0 Then VECTOR4_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the W component of this 4d vector.
	ENDREM
	Method getW:Float() 
		Return VECTOR4_getW(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the X component of this 4d vector.
	ENDREM
	Method getX:Float() 
		Return VECTOR4_getX(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the Y component of this 4d vector.
	ENDREM
	Method getY:Float() 
		Return VECTOR4_getY(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the Z component of this 4d vector.
	ENDREM
	Method getZ:Float() 
		Return VECTOR4_getZ(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the W component of this 4d vector.
	ENDREM
	Method setW(w:Float) 
		VECTOR4_setW(w, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the X component of this 4d vector.
	ENDREM
	Method setX(x:Float) 
		VECTOR4_setX(x, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the Y component of this 4d vector.
	ENDREM
	Method setY(y:Float) 
		VECTOR4_setY(y, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the Z component of this 4d vector.
	ENDREM
	Method setZ(z:Float) 
		VECTOR4_setZ(z, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the w,x,y and z components of this 4d vector at once.
	ENDREM
	Method setAll(x:Float, y:Float, z:Float, w:Float) 
		VECTOR4_setAll(x, y, z, w, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get a zero TVector4.
	ENDREM
	Function ZERO:TVector4() 
		Return TVector4.FromPtr(VECTOR4_ZERO()) 
	End Function
End Type

Rem
	bbdoc: Type to define a plane in 3d space. The plane contains a normal specifying direction of the plane, and a distance component d or distance from the origin.
ENDREM
Type TPlane

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Specify whether or not this object is cleaned up by garbage collection.
	ENDREM
	Field managed:Int
	
	Rem
		bbdoc: Refers to neither side of the plane.
	ENDREM
	Const NO_SIDE:Int = 0
	Rem
		bbdoc: Refers to the side of the plane that faces it's normal.
	ENDREM
	Const POSITIVE_SIDE:Int = 1
	Rem
		bbdoc: Refers to the side of the plane that faces away from the plane's normal.
	ENDREM
	Const NEGATIVE_SIDE:Int = 2
	Rem
		bbdoc: Refers to both sides of the plane.
	ENDREM
	Const BOTH_SIDE:Int = 3
	
	Rem
		bbdoc:  Create a TPlane from a Byte Ptr. If you set isManaged to TRUE, then this TPlane will not be cleaned up by Garbage Collection. Do not specify that this TPlane is managed to true unless you are certain that it will be cleaned up.
	ENDREM
	Function FromPtr:TPlane(Pointer:Byte Ptr, isManaged:Int = 0) 
		Local Obj:TPlane = New TPlane
		Obj.Pointer = Pointer
		Obj.managed = isManaged
		Return Obj
	End Function
	
	Rem
		bbdoc: Create an empty TPlane.
	ENDREM
	Function createEmpty:TPlane() 
		Return TPlane.FromPtr(PLANE_createEmpty()) 
	End Function
	
	Rem
		bbdoc: Create a TPlane from another TPlane, effectively copying it's values.
	ENDREM
	Function createWithPlane:TPlane(rhs:TPlane) 
		Return TPlane.FromPtr(PLANE_createWithPlane(rhs.Pointer)) 
	End Function
	
	Rem
		bbdoc: Create a TPlane from a normal and a distance scalar.<br>rkNormal:TVector3 The normal to define the direction of the plane.<br>fConstant:Float The scalar defining the distance of the plane.
	ENDREM
	Function Create:TPlane(rkNormal:TVector3, fConstant:Float) 
		Return TPlane.FromPtr(PLANE_create(rkNormal.Pointer, fConstant)) 
	End Function
	
	Rem
		bbdoc: Creates a TPlane similarly to create, except it uses a point to define distance.<br>rkNormal:TVector3 The normal to define the direction of the plane.<br>rkPoint:TVector3 The point to define the distance of the plane.
	ENDREM
	Function createWithPoint:TPlane(rkNormal:TVector3, rkPoint:TVector3) 
		Return TPlane.FromPtr(PLANE_createWithPoint(rkNormal.Pointer, rkPoint.Pointer)) 
	End Function
	
	Rem
		bbdoc: Create a TPlane using a series of points.
	ENDREM
	Function createWith3Points:TPlane(rkPoint0:TVector3, rkPoint1:TVector3, rkPoint2:TVector3) 
		Return TPlane.FromPtr(PLANE_createWith3Points(rkPoint0.Pointer, rkPoint1.Pointer, rkPoint2.Pointer)) 
	End Function
	
	Rem
		bbdoc: Returns the side where the given point lies. The side is an integer value corresponding to these constants: { TPlane.NO_SIDE = 0 , TPlane.POSITIVE_SIDE = 1 , TPlane.NEGATIVE_SIDE = 2 , TPlane.BOTH_SIDE = 3 }
	ENDREM
	Method getSide:Int(rkPoint:TVector3) 
		Return PLANE_getSide(rkPoint.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Returns the side where the given TAxisAlignedBox lies. The side is an integer value corresponding to these constants: { TPlane.NO_SIDE = 0 , TPlane.POSITIVE_SIDE = 1 , TPlane.NEGATIVE_SIDE = 2 , TPlane.BOTH_SIDE = 3 }
	ENDREM
	Method getSideWithAAB:Int(rkBox:TAxisAlignedBox) 
		Return PLANE_getSideWithAAB(rkBox.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Returns the side where the given point defined box resides. The side is an integer value corresponding to these constants: { TPlane.NO_SIDE = 0 , TPlane.POSITIVE_SIDE = 1 , TPlane.NEGATIVE_SIDE = 2 , TPlane.BOTH_SIDE = 3 }<br>centre:TVector3 Where the center of the box is defined.<br>halfSize:TVector3 Where the distance from the center of the box defines half of it's size.
	ENDREM
	Method getSideWithBox:Int(centre:TVector3, halfSize:TVector3) 
		Return PLANE_getSideWithBox(centre.Pointer, halfSize.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets the local distance from the TPlane a point has. The result will be positive if the point resides on the positive side and negative if the point resides on the negative side.
	ENDREM
	Method getDistance:Float(rkPoint:TVector3) 
		Return PLANE_getDistance(rkPoint.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Redefine the current TPlane that already exists by passing three points.
	ENDREM
	Method redefine(rkPoint0:TVector3, rkPoint1:TVector3, rkPoint2:TVector3) 
		PLANE_redefine(rkPoint0.Pointer, rkPoint1.Pointer, rkPoint2.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Redefine the current TPlane that already exists by passing a normal and a point for distance.<br>rkNormal:TVector3 The normal that defines the normal of the TPlane.<br>rkPoint:TVector3 The point that defines the distance of the TPlane.
	ENDREM
	Method redefineWithNormal(rkNormal:TVector3, rkPoint:TVector3) 
		PLANE_redefineWithNormal(rkNormal.Pointer, rkPoint.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Project a TVector3 onto the TPlane and get back the projected TVector3.<br>v:TVector3 The TVector3 you wish to project onto the TPlane.
	ENDREM
	Method projectVector:TVector3(v:TVector3) 
		Return TVector3.FromPtr(PLANE_projectVector(v.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Normalises the TPlane, returning the previous distance.
	ENDREM
	Method normalise:Float() 
		Return PLANE_normalise(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Conditional equality test operator for TPlane. Example: a.isEqualTo(b) is the same is If a = b.
	ENDREM
	Method isEqualTo:Int(rhs:TPlane) 
		Return PLANE_isEqualTo(rhs.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Conditional inequality test operator for TPlane. Example: a.isNotEqualTo(b) is the same is If a != b.
	ENDREM
	Method isNotEqualTo:Int(rhs:TPlane) 
		Return PLANE_isNotEqualTo(rhs.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Returns this TPlane as a string for easy viewing of it's values. 
	ENDREM
	Method toStr:String() 
		Local temp:String
		Return temp.FromCString(PLANE_toStr(Self.Pointer)) 
	End Method

	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete() 
		If Self.managed = 0 Then PLANE_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get a TVector3 containing this TPlane's normal.
	ENDREM
	Method getNormal:TVector3() 
		Return TVector3.FromPtr(PLANE_getNormal(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc:  Get the distance that this plane is moved along its normal and defines its position.
	ENDREM
	Method getD:Float() 
		Return Plane_getD(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set this plane's normal.
	ENDREM
	Method setNormal(normal:TVector3) 
		PLANE_setNormal(normal.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the distance along this plane's normal on which it resides.
	ENDREM
	Method setD(d:Float) 
		PLANE_setD(d, Self.Pointer) 
	End Method
	
End Type

Rem
	bbdoc: Describes a plane bounded volume. This type is not yet fully implemented.
ENDREM
Type TPlaneBoundedVolume

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
End Type

Rem
	bbdoc: An abstract wphere object. You can not use this as a renderable directly.
ENDREM
Type TSphere

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create an empty sphere object.
	ENDREM
	Function Create:TSphere() 
		Local Obj:TSphere = New TSphere
		Obj.Pointer = SPHERE_create()
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a sphere with a center position and raidus.
	ENDREM
	Function createWithAtributes:TSphere( center:TVector3 , radius:Float )
		Local Obj:TSphere = New TSphere
		Obj.Pointer = SPHERE_createWithAttributes( center.Pointer , radius )
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		SPHERE_delete( Self.Pointer )
	End Method
	
	
	Rem
		bbdoc: Get the radius of this spehere.
	ENDREM
	Method getRadius:Float()
		Return SPHERE_getRadius( Self.Pointer )
	End Method
End Type

Rem
	bbdoc: A 3d box that is axis-aligned.
ENDREM
Type TAxisAlignedBox

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Specifies whether or not this objects is cleaned up by GC.
	ENDREM
	Field Managed:Byte
	
	Rem
		bbdoc: Axis Aligned Box Position : Far left bottom.
	ENDREM
	Const FAR_LEFT_BOTTOM:Int = 0
	
	Rem
		bbdoc: Axis Aligned Box Position : Far left top.
	ENDREM
	Const FAR_LEFT_TOP:Int = 1
	
	Rem
		bbdoc: Axis Aligned Box Position : Far right top.
	ENDREM
	Const FAR_RIGHT_TOP:Int = 2
	
	Rem
		bbdoc: Axis Aligned Box Position : Far right bottom.
	ENDREM
	Const FAR_RIGHT_BOTTOM:Int = 3
	
	Rem
		bbdoc: Axis Aligned Box Position : Near right bottom.
	ENDREM
  	Const NEAR_RIGHT_BOTTOM:Int = 7
	
	Rem
		bbdoc: Axis Aligned Box Position : Near left bottom.
	ENDREM
  	Const NEAR_LEFT_BOTTOM:Int = 6
	
	Rem
		bbdoc: Axis Aligned Box Position : Near left top.
	ENDREM
    Const NEAR_LEFT_TOP:Int = 5
	
	Rem
		bbdoc: Axis Aligned Box Position : Near right top.
	ENDREM 
    Const NEAR_RIGHT_TOP:Int = 4
	
	Rem
		bbdoc: Specifies null/non-box extent.
	ENDREM
	Const EXTENT_NULL:Int = 0
	
	Rem
		bbdoc: Specifies a finite extent.
	ENDREM
	Const EXTENT_FINITE:Int = 1
	
	Rem
		bbdoc: Specifies an infinite extent.
	ENDREM
	Const EXTENT_INFINITE:Int = 2
	
	Rem
		bbdoc: Create an empty AAB.
	ENDREM
	Function createEmpty:TAxisAlignedBox()
		Return TAxisAlignedBox.FromPtr(AAB_createEmpty())
	End Function
	
	Rem
		bbdoc: Create an AAB with the given extents model. The choices are TAxisAlignedBox.EXTENT_NULL , TAxisAlignedBox.EXTENT_FINITE  and TAxisAlignedBox.EXTENT_INFINITE. 
	ENDREM
	Function createWithExtent:TAxisAlignedBox(e:Int)
		Return TAxisAlignedBox.FromPtr(AAB_createWithExtent(e))
	End Function
	
	Rem
		bbdoc: Create an AAB by copying the values of another AAB.
	ENDREM
	Function createWithAAB:TAxisAlignedBox(rkBox:TAxisAlignedBox)
		Return TAxisAlignedBox.FromPtr(AAB_createWithAAB(rkBox.Pointer))
	End Function
	
	Rem
		bbdoc: Create an AAB with min and max extents (TVector3 ).
	ENDREM
	Function createWithMinMax:TAxisAlignedBox(MinVec:TVector3, MaxVec:TVector3)
		Return TAxisAlignedBox.FromPtr(AAB_createWithMinMax(MinVec.Pointer, MaxVec.Pointer))
	End Function
	
	Rem
		bbdoc: Create an AAB with 3 min and 3 max values as floating point params.
	ENDREM
	Function Create:TAxisAlignedBox(m1X:Float, m1Y:Float, m1Z:Float, m2X:Float, m2Y:Float, m2Z:Float)
		Return TAxisAlignedBox.FromPtr(AAB_create(m1X, m1Y, m1Z, m2X, m2Y, m2Z))
	End Function
	
	Rem
		bbdoc: Equals operator. Using a.eq(b) would be the same as a = b. 
	ENDREM
	Method eq(rhs:TAxisAlignedBox)
		AAB_eq(rhs.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		If Self.Managed <> True And Self.Pointer <> Null Then
			AAB_delete(Self.Pointer)
			Pointer = Null
		End If	
	End Method
	
	Rem
		bbdoc: Manual Destructor. You cannot use this to delete managed objects, but you can free unmanaged objects early.
	ENDREM
	Method Destroy()
		If Self.Managed <> True And Self.Pointer <> Null Then
			AAB_delete(Self.Pointer)
			Pointer = Null
		End If	
	End Method
	
	Rem
		bbdoc: Get the minimum value/tip of this AAB as a TVector3.
	ENDREM
	Method getMinimum:TVector3()
		Return TVector3.FromPtr(AAB_getMinimum(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Get the maximum value/tip of this AAB as a TVector3.
	ENDREM
	Method getMaximum:TVector3()
		Return TVector3.FromPtr(AAB_getMaximum(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Set the minimum value/tip of this AAB with a 3-dimensional vector.
	ENDREM
	Method setMinimumWithVector(vec:TVector3)
		AAB_setMinimumWithVector(vec.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the minimum value/tip of this AAB using 3 dimensions as floats.
	ENDREM
	Method setMinimum(x:Float, y:Float, z:Float)
		AAB_setMinimum(x, y, z, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the minimum value/tip's X axis of this AAB.
	ENDREM
	Method setMinimumX(x:Float)
		AAB_setMinimumX(x, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the minimum value/tip's Y axis of this AAB.
	ENDREM
	Method setMinimumY(y:Float)
		AAB_setMinimumY(y, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the minimum value/tip's Z axis of this AAB.
	ENDREM
	Method setMinimumZ(z:Float)
		AAB_setMinimumZ(z, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the maximum value/tip of this AAB with a 3-dimensional vector.
	ENDREM
	Method setMaximumWithVector(vec:TVector3)
		AAB_setMaximumWithVector(vec.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the maximum value/tip of this AAB using 3 dimensions as floats.
	ENDREM
	Method setMaximum(x:Float, y:Float, z:Float)
		AAB_setMaximum(x, y, z, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the maximum value/tip's X axis of this AAB.
	ENDREM
	Method setMaximumX(x:Float)
		AAB_setMaximumX(x, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the maximum value/tip's Y axis of this AAB.
	ENDREM
	Method setMaximumY(y:Float)
		AAB_setMaximumY(y, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the maximum value/tip's Z axis of this AAB.
	ENDREM
	Method setMaximumZ(z:Float)
		AAB_setMaximumZ(z, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the minimum and maximum extents with two TVector3s.
	ENDREM
	Method setExtents(minVec:TVector3, maxVec:TVector3)
		AAB_setExtents(minVec.Pointer, maxVec.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the minimum and maximum extents with six floating point values.
	ENDREM
	Method setExtentsWithFloats(m1X:Float, m1Y:Float, m1Z:Float, m2X:Float, m2Y:Float, m2Z:Float)
		AAB_setExtentsWithFloats(m1X, m1Y, m1Z, m2X, m2Y, m2Z, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the position of a single corner as a TVector3.<br/>cornerToGet:Int - Corner to get, uses the TAxisAlignedBox.NEAR_ and TAxisAlignedBox.FAR_ constants.
	ENDREM
	Method getCorner:TVector3(cornerToGet:Int)
		Return TVector3.FromPtr(AAB_getCorner(cornerToGet, Self.Pointer))
	End Method

	Rem
		bbdoc: Merge this AAB with another AAB.
	ENDREM
	Method merge(rhs:TAxisAlignedBox)
		AAB_merge(rhs.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Merge this AAB with a TVector3 based point.
	ENDREM
	Method mergeWithPoint(point:TVector3)
		AAB_mergeWithPoint(point.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Transform this AAB according to a 4x4 matrix.
	ENDREM
	Method transform(matrix:TMatrix4)
		AAB_transform(matrix.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Affine transform this AAB according to a 4x4 matrix.
	ENDREM
	Method transformAffine(matrix:TMatrix4)
		AAB_transformAffine(matrix.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set this AAB	to be a null, non-box AAB.
	ENDREM
	Method setBoxNull()
		AAB_setBoxNull(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Check to see if this AAB is a null, non-box AAB.
	ENDREM
	Method isBoxNull:Byte()
		Return AAB_isBoxNull(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Check to see if this box has finite bounds.
	ENDREM
	Method isFinite:Byte()
		Return AAB_isFinite(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set this AAB to have infinite bounds.
	ENDREM
	Method setInfinite()
		AAB_setInfinite(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Check to see whether or not this AAB has infinite bounds.
	ENDREM
	Method isInfinite:Byte()
		Return AAB_isInfinite(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Check to see if this AAB intersects a given AAB.
	ENDREM
	Method intersects:Byte(b2:TAxisAlignedBox)
		Return AAB_intersects(b2.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the intersection of this AAB with another AAB as a third AAB.
	ENDREM
	Method intersection:TAxisAlignedBox(b2:TAxisAlignedBox)
		Return FromPtr(AAB_intersection(b2.Pointer, Self.Pointer))
	End Method
	
	Rem
		bbdoc: Get the volume of this AAB.
	ENDREM
	Method volume:Float()
		Return AAB_volume( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Scale this AAB with a TVector3.
	ENDREM
	Method scale(s:TVector3)
		AAB_scale(s.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Test to see if this TAxisAlignedBox intersects with a sphere.
	ENDREM
	Method intersectsSphere:Byte(s:TSphere)
		Return AAB_intersectsSphere(s.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Test to see if this TAxisAlignedBox intersects with a plane.
	ENDREM
	Method intersectsPlane:Byte(p:TPlane)
		Return AAB_intersectsPlane(p.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Test to see if this TAxisAlignedBox intersects with a vector.
	ENDREM
	Method intersectsVector:Byte(v:TVector3)
		Return AAB_intersectsVector(v.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the center point of this AAB.
	ENDREM
	Method getCenter:TVector3()
		Return TVector3.FromPtr(AAB_getCenter(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Get the overall size of this AAB.
	ENDREM
	Method getSize:TVector3()
		Return TVector3.FromPtr(AAB_getSize(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Get the half-size of this AAB.
	ENDREM
	Method getHalfSize:TVector3()
		Return TVector3.FromPtr(AAB_getHalfSize(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Check to see if this AAB contains a point/TVector3.
	ENDREM
	Method containsVector:Byte(v:TVector3)
		Return AAB_containsVector(v.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Check to see if this AAB contains the given AAB.
	ENDREM
	Method contains:Byte(other:TAxisAlignedBox)
		Return AAB_contains(other.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Check to see if this AAB and a given AAB are equal.
	ENDREM
	Method isEqualTo:Byte(rhs:TAxisAlignedBox)
		Return AAB_isEqualTo(rhs.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Check to see whether this AAB and another AAB are NOT equal.
	ENDREM
	Method isNotEqualTo:Byte(rhs:TAxisAlignedBox)
		Return AAB_isNotEqualTo(rhs.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: A prefab null AAB.
	ENDREM
	Function BOX_NULL:TAxisAlignedBox()
		Return FromPtr(AAB_BOX_NULL() , True)
	End Function
	
	Rem
		bbdoc: A prefab infinite AAB.
	ENDREM
	Function BOX_INFINITE:TAxisAlignedBox()
		Return FromPtr(AAB_BOX_INFINITE() , True)
	End Function
	
	Rem
		bbdoc: Get the contents of this AAB as a string.
	ENDREM
	Method ToStr:String()
		Return String.FromCString(AAB_toStr(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Helper method to get a TAxisAlignedBox from a pointer.
	ENDREM
	Function FromPtr:TAxisAlignedBox(Pointer:Byte Ptr, isManaged:Byte = False)
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = Pointer
		Obj.Managed = isManaged
		Return Obj
	End Function
	
	
End Type

Rem
	bbdoc: The interface that describes batch rendered geometry. You must create a TStaticGeometry object with your TSceneManager then addEntity(...) or addSceneNode(...) and build(). 
END REM
Type TStaticGeometry

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get the name of this TStaticGeometry instance from when it was created by the scene manager.
	END REM
	Method getName:String() 
		Return String.FromCString(SGEOMETRY_getName(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Adds an entity to this collection of batched geometry. You can add multiple identical or non-identical entities to batch. Note also that entities are not tied to the static geometry in any way and can be deleted after being added to the batch. After adding all of your geometry to the batch you must .build() your static geometry.<br>The other parameters position, orientation and scale allow you to specify the world position, orientation and scale for the added entity within the batched geometry.<br>Note: You may destroy the base entity and anything attaching it to the scene graph after you build this static object.
	END REM
	Method addEntity(ent:TEntity, Position:TVector3, orientation:TQuaternion = Null, scale:TVector3 = Null) 
		If orientation = Null Then orientation = TQuaternion.IDENTITY() 
		If scale = Null Then scale = TVector3.UNIT_SCALE() 
		SGEOMETRY_addEntity(ent.Pointer, Position.Pointer, orientation.Pointer, scale.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Adds all entities attached to a specified scene node and all of it's children. Unlike with addEntity(...), position, scale and orientation are taken from the TSceneNode properties so there is no need to specify them here. This method DOES NOT detach the node from the scene graph and you must do this manually yourself if you want to avoid rendering both sets of entities/geometry. After you are done adding geometry you must call .build() to build your static geometry.<br>Note: You may destroy any the scenenode specified and all of the entities once the static geometry is built.
	END REM	
	Method addSceneNode(NODE:TSceneNode) 
		SGEOMETRY_addSceneNode(NODE.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Builds the batched geometry based on the entities you added to this static geometry. No more entities can be added after you have called .build(). By default the geometry will be added to the scene and rendered unless you have specified for it to not be visible.
	END REM
	Method build() 
		SGEOMETRY_build(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Destroys all of the built static geometry. You may call build again after this and rebuild the static geometry frome previously queued entities.
	END REM
	Method Destroy() 
		SGEOMETRY_destroy(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Similar to destroy except it both destroys the static geometry AND clears all entities/nodes queued with this static geometry.
	END REM
	Method Reset() 
		SGEOMETRY_reset(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Allows you to specify the distance at which this static geometry is no longer rendered. When combined with a shader that fades the geometry out you can have a less sudden "pop out".<br>dist:float is set to 0 by default, which means that this static geometry will always be rendered.
	END REM
	Method setRenderingDistance(dist:Float) 
		SGEOMETRY_setRenderingDistance(dist, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets the distance at which this static geometry will no longer be rendered.
	END REM
	Method getRenderingDistance:Float() 
		Return SGEOMETRY_getRenderingDistance(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the squared distance at which this static geometry will no longer be rendered.
	END REM
	Method getSquaredRenderingDistance:Float() 
		Return SGEOMETRY_getSquaredRenderingDistance(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Specify whether or not this static geometry will be visible.
	END REM
	Method setVisible(visible:Byte) 
		SGEOMETRY_setVisible(visible, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Return whether or not this static geometry is visible.
	END REM
	Method isVisible:Byte() 
		Return SGEOMETRY_isVisible(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Sets whether or not shadows this static geometry will cast shadows (static geometry does NOT inherit shadow settings from added entities and nodes). You will need to set this before calling build and ensure that all meshes in this geometry are capable of supporting shadows. Generally since you are using static geometry your best option is to precalculate the shadows for a static object.<br>By default shadows on static geometry are not turned on.
	END REM
	Method setCastShadows(castShadows:Byte) 
		SGEOMETRY_setCastShadows(castShadows, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not this static geometry is set to cast shadows.
	END REM
	Method getCastShadows:Byte() 
		Return SGEOMETRY_getCastShadows(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Specify the size allotted for a single region of geometry. Entities will be placed into the batches based on region. If many entities overlap the bounds of the batches may need to be adjusted to balance culling against size. You must call this before you build().<br>The default region size is (1000,1000,1000).
	END REM
	Method setRegionDimensions(Size:TVector3) 
		SGEOMETRY_setRegionDimensions(Size.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the size allotted for a single region of goemetry.
	END REM
	Method getRegionDimensions:TVector3() 
		Return TVector3.FromPtr(SGEOMETRY_getRegionDimensions(Self.Pointer)) 
	End Method
		
	Rem
		bbdoc: Allows you to set the world origin of your geometry. Unless you have a massive world the default settings should work for you without having to specify a new world origin. This must be called before you build().
	END REM
	Method SetOrigin(origin:TVector3) 
		SGEOMETRY_setOrigin(origin.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the world origin of this static geometry.
	END REM
	Method GetOrigin:TVector3() 
		Return TVector3.FromPtr(SGEOMETRY_getOrigin(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Specify the renderqueue that this static geometry will be rendered through. The default render queue group will work fine, but if you need additional render queue settings the render queues available to you are:<br>RENDER_QUEUE_BACKGROUND<br>RENDER_QUEUE_SKIES_EARLY<br>RENDER_QUEUE_1<br>RENDER_QUEUE_2<br>RENDER_QUEUE_WORLD_GEOMETRY_1<br>RENDER_QUEUE_3<BR>RENDER_QUEUE_4<BR>RENDER_QUEUE_MAIN<BR>RENDER_QUEUE_6<BR>RENDER_QUEUE_7<BR>RENDER_QUEUE_WORLD_GEOMETRY_2<BR>RENDER_QUEUE_8<BR>RENDER_QUEUE_9<BR>RENDER_QUEUE_SKIES_LATE<BR>RENDER_QUEUE_OVERLAY<BR>
	END REM
	Method setRenderQueueGroup(queueID:Int) 
		SGEOMETRY_setRenderQueueGroup(queueID, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the renderqueue that this static geometry will be rendered through. Compare to constants to determine which renderqueue is returned.
	END REM
	
	Method getRenderQueueGroup:Int() 
		Return SGEOMETRY_getRenderQueueGroup(Self.Pointer) 
	End Method
	
	'Not yet implemented-
	'Method getRegionIterator:TRegionIterator()
	
	Rem
		bbdoc: Dump the contents of this static geometry to file for troubleshooting.
	END REM
	Method dump(filename:String) 
		SGEOMETRY_dump(filename.ToCString() , Self.Pointer) 
	End Method
	
End Type




Rem
	bbdoc: A simple interface for creating Catmull-Rom splines. Just create the points, and the tangents are generated automatically for you.Please note that these splines are not handled as any sort of renderable object, and are simple for computation. If you need a renderable object to represent splines, have a look at the TManualObject class.
ENDREM
Type TSimpleSpline

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create an empty spline.
	ENDREM
	Function Create:TSimpleSpline()
		Local Obj:TSimpleSpline = New TSimpleSpline
		Obj.Pointer = SIMPLESPLINE_create()
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor
	ENDREM
	Method Delete()
		SIMPLESPLINE_delete( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Add a control point to the spline. Every time you add a point, if you have not specified via the setAutoCalculate(...) method, then tangents will be recalculated on every add. It is reccommended that, if you are adding points as a batch, you should turn off auto calculation of tangents, then either update manually with recalcTangents() or switch auto calculation of tangents back on.
	ENDREM
	Method addPoint(p:TVector3) 
		SIMPLESPLINE_addPoint( p.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets a control point by specifying it's index from the beginning of the line.
	ENDREM
	Method getPoint:TVector3(index:Short) 
		Return TVector3.FromPtr(SIMPLESPLINE_getPoint(index, Self.Pointer)) 	
	End Method
	
	Rem
		bbdoc: Gets the number of control points in the TSimpleSpline.
	ENDREM
	Method getNumPoints:Short() 
		Return SIMPLESPLINE_getNumPoints( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Clear all control points in the TSimpleSpline.
	ENDREM
	Method clear()
		SIMPLESPLINE_clear( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Update the position of a single control point in the TSimpleSpline by index.<br>index:Short The index of the control point to update.<br>value:TVector3 The new position of the control point in 3d world space.
	ENDREM
	Method updatePoint(index:Short, Value:TVector3) 
		SIMPLESPLINE_updatePoint( index , value.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get an interpolated point based on a parametric value for the entire TSimpleSpline.<br>t:Float Parametric distance along the whole of the spline. ( 0.0-1.0 )
	ENDREM
	Method interpolate:TVector3(t:Float) 
		Return TVector3.FromPtr(SIMPLESPLINE_interpolate(t, Self.Pointer)) 		
	End Method
	
	Rem
		bbdoc: Gets an interpolated point of a single index within the TSimpleSpline based on a parametric value.<br>fromIndex:Int The index of the segment of the TSimpleSpline to operate on.<br>t:Float Parametric value to interpolate with.
	ENDREM
	Method interpolateWithIndex:TVector3(fromIndex:Int, t:Float) 
		Return TVector3.FromPtr(SIMPLESPLINE_interpolateWithIndex(fromIndex, t, Self.Pointer)) 	
	End Method
	
	Rem
		bbdoc: Toggles whether the spline auto calculates tangents or not.
	ENDREM
	Method setAutoCalculate(autoCalc:Int) 
		SIMPLESPLINE_setAutoCalculate( autoCalc , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Recalculate tangents for this spline.
	ENDREM
	Method recalcTangents()
		SIMPLESPLINE_recalcTangents( Self.Pointer )
	End Method
End Type

Rem
	bbdoc: Ogre's interface for representing a line with an origin and a direction.
ENDREM
Type TRay

	Rem
		bbdoc: Reference to the TRay in memory.
	ENDREM
	Field Pointer:Byte Ptr

	Rem
		bbdoc: Flag to specify whether or not this TRay is managed.
	ENDREM
	Field managed:Byte
	
	Rem
		bbdoc: Helper function for managing the creation of TRays. You should never have to use this.
	ENDREM
	Function _getRAY:TRay(pointer:Byte Ptr, isManaged:Byte = 0) 
		Local Obj:TRay = New TRay
		Obj.Pointer = pointer
		Obj.managed = isManaged
		Return Obj
	End Function
	
	Rem
		bbdoc: Returns an empty TRay object which points towards the positive z axis.
	ENDREM
	Function CreateEmpty:TRay() 
		Return TRay._getRAY(OGRERAY_createEmpty()) 
	End Function
	
	Rem
		bbdoc: Creates a TRay with a given origin and direction ( both in TVector3 format ).<br>origin:TVector3 The point in space where the ray begins.<br>direction:TVector3 The vector specifying the direction of this TRay.
	ENDREM
	Function Create:TRay(origin:TVector3, direction:TVector3) 
		Return TRay._getRAY(OGRERAY_create(origin.Pointer, direction.Pointer)) 
	End Function
	
	Rem
		bbdoc: Set the origin of this 3d ray with a TVector3.
	ENDREM
	Method SetOrigin(origin:TVector3) 
		OGRERAY_setOrigin(origin.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Returns the origin of this TRay as a TVector3.
	ENDREM
	Method GetOrigin:TVector3() 
		Return TVector3.FromPtr(OGRERAY_getOrigin(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Set the direction of this TRay with a TVector3.
	ENDREM
	Method setDirection(dir:TVector3) 
		OGRERAY_setDirection(dir.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the direction of this TRay as a TVector3.
	ENDREM
	Method GetDirection:TVector3() 
		Return TVector3.FromPtr(OGRERAY_getDirection(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get a point along the TRay using a scalar for distance.<br>t:Float The distance from the origin you wish to get.
	ENDREM
	Method getPoint:TVector3(t:Float) 
		Return TVector3.FromPtr(OGRERAY_getPoint(t, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Check to see whether or not this TRay intersects a specified plane. The results can be used as such: Collides:Byte = myByteFloatPtr.first[0] , distance:Float = myByteFloatPair.second[0].
	ENDREM
	Method intersectsPlane:TByteFloatPair(p:TPlane) 
		Return TByteFloatPair.FromPtr(OGRERAY_intersectsPlane(p.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc:
	ENDREM
	Method intersectsPlaneBoundedVolume:TByteFloatPair(p:TPlaneBoundedVolume) 
		Return TByteFloatPair.FromPtr(OGRERAY_intersectsPlaneBoundedVolume(p.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc:
	ENDREM
	Method intersectsSphere:TByteFloatPair(s:TSphere) 
		Return TByteFloatPair.FromPtr(OGRERAY_intersectsSphere(s.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc:
	ENDREM
	Method intersectsAxisAlignedBox:TByteFloatPair(box:TAxisAlignedBox) 
		Return TByteFloatPair.FromPtr(OGRERAY_intersectsAxisAlignedBox(box.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Helper method for viewing TRay data as a string.
	ENDREM
	Method toStr:String() 
		Return String.FromCString(OGRERAY_toStr(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Destructor for cleaning up unmanaged TRay objects.
	ENDREM
	Method Delete() 
		OGRERAY_delete( Self.Pointer )
	End Method

End Type

Rem
	bbdoc: Interface for degrees.
ENDREM
Type TDegree
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create this degree with a value in degrees.
	ENDREM
	Function Create:TDegree( d:Float )
		Local Obj:TDegree = New TDegree
		Obj.Pointer = DEGREE_create( d )
		Return Obj
	End Function
	
	Rem
		bbdoc: Get the value in degrees.
	ENDREM
	Method valueDegrees:Float()
		Return DEGREE_valueDegrees( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the value in radians.
	ENDREM
	Method valueRadians:Float()
		Return DEGREE_valueRadians( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the value in angle units.
	ENDREM
	Method valueAngleUnits:Float()
		Return DEGREE_valueAngleUnits( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Cast this TDegree to a TRadian.
	ENDREM
	Method toRad:TRadian()
		Local Obj:TRadian = New TRadian
		Obj.Pointer = RAD_create(Self.valueRadians())
		Return Obj
	End Method
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete() 
		DEGREE_delete( Self.Pointer )
	End Method
	
	
	
End Type

Rem
	bbdoc: Implementation for radians.
ENDREM
Type TRadian

	Rem
		bbdoc: Reference to the TRadian object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create a radian by specifying its value as a radian value.
	ENDREM
	Function Create:TRadian( r:Float )
		Local Obj:TRadian = New TRadian
		Obj.Pointer = RAD_create( r )
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a radian by specifying the floating point value in degrees. Note that this still creates a TRadian.
	ENDREM
	Function createWithDegrees:TRadian(d:Float)
		Local Obj:TRadian = New TRadian
		Obj.Pointer = RAD_createWithDegrees(d)
		Return Obj
	End Function
	
	Rem
		bbdoc: Get the value in angle units.
	ENDREM
	Method valueAngleUnits:Float()
		Return RAD_valueAngleUnits( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the value in degrees.
	ENDREM
	Method valueDegrees:Float()
		Return RAD_valueDegrees( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the value in radians.
	ENDREM
	Method valueRadians:Float()
		Return RAD_valueRadians( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Cast this TRadian to a TDegree.
	ENDREM
	Method toDeg:TDegree()
		Local Obj:TDegree = New TDegree
		Obj.Pointer = DEGREE_create(Self.valueDegrees())
		Return Obj
	End Method

	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete() 
		RAD_delete(Self.Pointer) 
	End Method
	
End Type

Rem
	bbdoc: Type describing a pair of values: A bool/byte and a float. This type refers to a std::pair<bool,float>.
ENDREM
Type TByteFloatPair

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Define whether or not this object is cleaned up with garbage collection.
	ENDREM
	Field managed:Byte
	
	Rem
		bbdoc:  Create a TByteFloatPair from a Byte Ptr. If you set isManaged to TRUE, then this TByteFloatPair will not be cleaned up by Garbage Collection. Do not specify that this TByteFloatPair is managed to true unless you are certain that it will be cleaned up.
	ENDREM
	Function FromPtr:TByteFloatPair(Pointer:Byte Ptr, isManaged:Byte = False) 
		Local Obj:TByteFloatPair = New TByteFloatPair
		Obj.Pointer = Byte Ptr(Pointer) 
		Obj.managed = isManaged
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a new TByteFloatPair object and specify values for both the first/byte value and the second/float value.
	ENDREM
	Function Create:TByteFloatPair(First:Byte, second:Float) 
		Return TByteFloatPair.FromPtr(BYTEFLOATPAIR_create(First, second)) 
	End Function
	
	Rem
		bbdoc: Destructor for unmanaged objects.
	ENDREM
	Method Delete() 
		If Self.managed = False Then BYTEFLOATPAIR_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the first element of this byte float pair, which is the boolean/byte value.
	ENDREM
	Method getFirst:Byte() 
		Return BYTEFLOATPAIR_getFirst(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the first element of this byte float pair, which is the boolean/byte value.
	ENDREM
	Method setFirst(First:Byte) 
		BYTEFLOATPAIR_setFirst(First, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the second element of this byte float pair, which is the float value.
	ENDREM
	Method getSecond:Float() 
		Return BYTEFLOATPAIR_getSecond(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the second element of this byte float pair, which is the float value.
	ENDREM
	Method setSecond(second:Float) 
		BYTEFLOATPAIR_setSecond(second, Self.Pointer) 
	End Method
	
End Type

Rem
	bbdoc: This type represents colour.
ENDREM
Type TColourValue

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Define whether or not this object is cleaned up with garbage collection.
	ENDREM
	Field managed:Byte
	
	Rem
		bbdoc:  Create a TColourValue from a Byte Ptr. If you set isManaged to TRUE, then this TColourValue will not be cleaned up by Garbage Collection. Do not specify that this TColourValue is managed to true unless you are certain that it will be cleaned up.
	ENDREM
	Function FromPtr:TColourValue(Pointer:Byte Ptr, isManaged:Byte = False) 
		Local Obj:TColourValue = New TColourValue
		Obj.Pointer = Pointer
		Obj.managed = isManaged
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a colour with the specified Red, Green, Blue and Alpha Values. The values range from 0.0-1.0.
	ENDREM
	Function Create:TColourValue(red:Float, green:Float, blue:Float, alpha:Float) 
		Return TColourValue.FromPtr(COLOURVALUE_create(red, green, blue, alpha)) 
	End Function
	
	Rem
		bbdoc: Destructor for unmanaged instances of TColourValue.
	ENDREM
	Method Delete()
		If managed = False Then COLOURVALUE_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the R Component of this TColourValue.
	ENDREM
	Method getR:Float() 
		Return COLOURVALUE_getR(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the R Component of this TColourValue.
	ENDREM
	Method setR(r:Float) 
		COLOURVALUE_setR(r, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the G Component of this TColourValue.
	ENDREM
	Method getG:Float() 
		Return COLOURVALUE_getG(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the G Component of this TColourValue.
	ENDREM
	Method setG(g:Float) 
		COLOURVALUE_setG(g, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the B Component of this TColourValue.
	ENDREM
	Method getB:Float() 
		Return COLOURVALUE_getB(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the B Component of this TColourValue.
	ENDREM
	Method setB(b:Float) 
		COLOURVALUE_setB(b, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the R Component of this TColourValue.
	ENDREM
	Method getA:Float() 
		Return COLOURVALUE_getA(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the A Component of this TColourValue.
	ENDREM
	Method setA(a:Float) 
		COLOURVALUE_setA(a, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the R,G,B and A components of this TColouValue all at once.
	ENDREM
	Method setAll(r:Float, g:Float, b:Float, a:Float) 
		COLOURVALUE_setAll(r, g, b, a, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Output the RGBA values of this TColourValue to a string.
	ENDREM
	Method toStr:String()
		Return String.FromCString(COLOURVALUE_toStr(Self.Pointer))
	End Method
	
End Type

Rem
	bbdoc: A 3x3 square matrix that can be used for storing and manipulating rotation data. All operations performed are assumed to be right-handed.
ENDREM
Type TMatrix3	
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Define whether or not this object is cleaned up with garbage collection.
	ENDREM
	Field isManaged:Byte = 0
	
	Rem
		bbdoc: Create an empty TMatrix3 with all elements set to 0.
	ENDREM
	Function createEmpty:TMatrix3() 
		Local Obj:TMatrix3 = New TMatrix3
		Obj.Pointer = MATRIX3_createEmtpy() 
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a TMatrix3 based on the values in another TMatrix3.
	ENDREM
	Function createWithMatrix3:TMatrix3(rkMatrix:TMatrix3) 
		Local Obj:TMatrix3 = New TMatrix3
		Obj.Pointer = MATRIX3_createWithMatrix3(rkMatrix.Pointer) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a TMatrix by specifying every value contained therein. The format is as follows </br> row0 { fEntry00 , fEntry01 , fEntry02 } </br> { fEntry10 , fEntry11 , fEntry 12 } </br> { fEntry20 , fEntry21 , fEntry 22 }
	ENDREM
	Function Create:TMatrix3(fEntry00:Float, fEntry01:Float, fEntry02:Float, fEntry10:Float, fEntry11:Float, fEntry12:Float, fEntry20:Float, fEntry21:Float, fEntry22:Float) 
		Local Obj:TMatrix3 = New TMatrix3
		Obj.Pointer = MATRIX3_create(fEntry00, fEntry01, fEntry02, fEntry10, fEntry11, fEntry12, fEntry20, fEntry21, fEntry22) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Get a specific float value by specifying the row and column coordinates.<br>iRow:Int The row to get the value from.<br>iCol:Int The column to get the value from.
	ENDREM
	Method getValue:Float(iRow:Int, iCol:Int) 
		Return MATRIX3_getValue(iRow, iCol, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get a column of data from this TMatrix3 by specifying the column.<br>iCol:Int The colum of data to retreive.
	ENDREM
	Method GetColumn:TVector3(iCol:Int) 
		Return TVector3.FromPtr(MATRIX3_GetColumn(iCol, Self.Pointer)) 	
	End Method
	
	Rem
		bbdoc: Sets a column of data in this TMatrix3 to the specified values.<br>iCol:Int The column to modify.
	ENDREM
	Method SetColumn(iCol:Int, vec:TVector3) 
		MATRIX3_SetColumn(iCol, vec.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Insert values into this TMatrix3 by using 3 TVector3s that represent the x,y and z axes.
	ENDREM
	Method FromAxes(xAxis:TVector3, yAxis:TVector3, zAxis:TVector3) 
		MATRIX3_FromAxes(xAxis.Pointer, yAxis.Pointer, zAxis.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Equality operator for TMatrix3 = TMatrix3. Usage:  yourmatrix3.eq(yourothermatrix3)
	ENDREM
	Method Eq(rkMatrix:TMatrix3) 
		MATRIX3_eq(rkMatrix.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Equality evaluation operator for TMatrix3 == TMatrix3. Usage: yourmatrix3.isEqualTo(yourothermatrix3) is like [ if yourmatrix3 = yourothermatrix3 ].
	ENDREM
	Method isEqualTo:Int(rkMatrix:TMatrix3) 
		Return MATRIX3_isEqualTo(rkMatrix.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Equality evaluation operator for TMatrix3 <> TMatrix3. Usage: yourmatrix3.isNotEqualTo(yourothermatrix3) is like [ if yourmatrix3 <> yourothermatrix3 ].
	ENDREM
	Method isNotEqualTo:Int(rkMatrix:TMatrix3) 
		Return MATRIX3_isNotEqualTo(rkMatrix.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Addition operator for TMatrxi3 + TMatrix3. Usage: newmatrix3 = yourmatrix3.add(yourothermatrix3)
	ENDREM
	Method add:TMatrix3(rkMatrix:TMatrix3) 
		Local Obj:TMatrix3 = New TMatrix3
		Obj.Pointer = MATRIX3_add(rkMatrix.Pointer, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Subtraction operator for TMatrix3 - TMatrix3. Usage: newmatrix3 = yourmatrix3.sub(yourothermatrix3)
	ENDREM
	Method sub:TMatrix3(rkMatrix:TMatrix3) 
		Local Obj:TMatrix3 = New TMatrix3
		Obj.Pointer = MATRIX3_sub(rkMatrix.Pointer, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Multiplication operator for TMatrix3 * TMatrix3. Usage: newmatrix3 = yourmatrix3.mul(yourothermatrix3)
	ENDREM
	Method mul:TMatrix3(rkMatrix:TMatrix3) 
		Local Obj:TMatrix3 = New TMatrix3
		Obj.Pointer = MATRIX3_mul(rkMatrix.Pointer, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Negation operator for -TMatrix3. Usage: newmatrix3 = oldmatrix3.neg()
	ENDREM
	Method neg:TMatrix3() 
		Local Obj:TMatrix3 = New TMatrix3
		Obj.Pointer = MATRIX3_neg(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Multiplcation operator for TMatrix3*TVector3. Returns a transformed TVector3.
	ENDREM
	Method mulWithVector3:TVector3(rkVector:TVector3) 
		Return TVector3.FromPtr(MATRIX3_mulWithVector3(rkVector.Pointer, Self.Pointer)) 		
	End Method
	
	Rem
		bbdoc: Multiply this TMatrix3 by a scalar value.
	ENDREM
	Method mulWithScalar:TMatrix3(fScalar:Float) 
		Local Obj:TMatrix3 = New TMatrix3
		Obj.Pointer = MATRIX3_mulWithScalar(fScalar, Self.Pointer) 
		Return Obj	
	End Method
	
	Rem
		bbdoc: Get the transpose of this TMatrix3.
	ENDREM
	Method Transpose:TMatrix3() 
		Local Obj:TMatrix3 = New TMatrix3
		Obj.Pointer = MATRIX3_Transpose(Self.Pointer) 
		Return Obj	
	End Method
	
	Rem
		bbdoc: Get the Inverse of this TMatrix3 and store the results in a parameter.
	ENDREM
	Method InverseWithMatrix3:Int(rkInverse:TMatrix3, fTolerance:Float =.000001) 
		Return MATRIX3_InverseWithMatrix3(rkInverse.Pointer, fTolerance, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Return the inverse of this TMatrix3.
	ENDREM
	Method Inverse:TMatrix3(fTolerance:Float =.000001) 
		Local Obj:TMatrix3 = New TMatrix3
		Obj.Pointer = MATRIX3_Inverse(fTolerance, Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Return the determinant of this TMatrix3.
	ENDREM
	Method Determinant:Float() 
		Return MATRIX3_Determinant(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: SVD Factorisation method.
	ENDREM
	Method SingularValueDecomposition(rkL:TMatrix3, rkS:TVector3, rkR:TMatrix3) 
		MATRIX3_SingularValueDecomposition(rkL.Pointer, rkS.Pointer, rkR.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Orthonormalize this TMatrix3.
	ENDREM
	Method Orthonormalize() 
		MATRIX3_Orthonormalize(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: 
	ENDREM
	Method QDUDecomposition(rkQ:TMatrix3, rkD:TVector3, rkU:TVector3) 
		MATRIX3_QDUDecomposition(rkQ.Pointer, rkD.Pointer, rkU.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the spectral norm of this TMatrix3. 
	ENDREM
	Method SpectralNorm:Float() 
		Return MATRIX3_SpectralNorm(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Store this TMatrix3 as an axis/angle pair. The angle is in TRadians.<br>rkAxis:TVector3 The var used to store the axis.<br>rfAngle:TRadian The var used to store the angle.
	ENDREM
	Method ToAxisAngle(rkAxis:TVector3, rfAngle:TRadian) 
		MATRIX3_ToAxisAngle(rkAxis.Pointer, rfAngle.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Store this TMatrix3 as an axis/angle pair. The angle is in TRadians.<br>rkAxis:TVector3 The var used to store the axis.<br>rfAngle:TDegree The var used to store the angle.
	ENDREM
	Method ToAxisAngleWithDegrees(rkAxis:TVector3, rfAngle:TDegree) 
		MATRIX3_ToAxisAngleWithDegrees(rkAxis.Pointer, rfAngle.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Construct the data in this TMatrix3 from an Angle/Axis pair. The angle is in TRadians.<br>rkAxis:TVector3 The axis.<br>rRadians:TRadian The angle.
	ENDREM
	Method FromAxisAngle(rkAxis:TVector3, fRadians:TRadian) 
		MATRIX3_FromAxisAngle(rkAxis.Pointer, fRadians.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Convert this TMatrix3 to YPR Euler Angles.
	ENDREM
	Method ToEulerAnglesXYZ:Int(rfYAngle:TRadian, rfPAngle:TRadian, rfRAngle:TRadian) 
		Return MATRIX3_ToEulerAnglesXYZ(rfYAngle.Pointer, rfPAngle.Pointer, rfRAngle.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Convert this TMatrix3 to YPR Euler Angles.
	ENDREM
	Method ToEulerAnglesXZY:Int(rfYAngle:TRadian, rfPAngle:TRadian, rfRAngle:TRadian) 
		Return MATRIX3_ToEulerAnglesXZY(rfYAngle.Pointer, rfPAngle.Pointer, rfRAngle.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Convert this TMatrix3 To YPR Euler Angles.
	ENDREM
	Method ToEulerAnglesYXZ:Int(rfYAngle:TRadian, rfPAngle:TRadian, rfRAngle:TRadian) 
		Return MATRIX3_ToEulerAnglesYXZ(rfYAngle.Pointer, rfPAngle.Pointer, rfRAngle.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Convert this TMatrix3 To YPR Euler Angles.
	ENDREM
	Method ToEulerAnglesYZX:Int(rfYAngle:TRadian, rfPAngle:TRadian, rfRAngle:TRadian) 
		Return MATRIX3_ToEulerAnglesYZX(rfYAngle.Pointer, rfPAngle.Pointer, rfRAngle.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Convert this TMatrix3 to YPR Euler Angles.
	ENDREM
	Method ToEulerAnglesZXY:Int(rfYAngle:TRadian, rfPAngle:TRadian, rfRAngle:TRadian) 
		Return MATRIX3_ToEulerAnglesZXY(rfYAngle.Pointer, rfPAngle.Pointer, rfRAngle.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Convert this TMatrix3 to YPR Euler Angles.
	ENDREM
	Method ToEulerAnglesZYX:Int(rfYAngle:TRadian, rfPAngle:TRadian, rfRAngle:TRadian) 
		Return MATRIX3_ToEulerAnglesZYX(rfYAngle.Pointer, rfPAngle.Pointer, rfRAngle.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Construct this TMatrix3 from YPR Euler Angles.
	ENDREM
	Method FromEulerAnglesXYZ(fYAngle:TRadian, fPAngle:TRadian, fRAngle:TRadian) 
		MATRIX3_FromEulerAnglesXYZ(fYAngle.Pointer, fPAngle.Pointer, fRAngle.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Construct this TMatrix3 from YPR Euler Angles.
	ENDREM
	Method FromEulerAnglesXZY(fYAngle:TRadian, fPAngle:TRadian, fRAngle:TRadian) 
		MATRIX3_FromEulerAnglesXZY(fYAngle.Pointer, fPAngle.Pointer, fRAngle.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Construct this TMatrix3 from YPR Euler Angles.
	ENDREM
	Method FromEulerAnglesYXZ(fYAngle:TRadian, fPAngle:TRadian, fRAngle:TRadian) 
		MATRIX3_FromEulerAnglesYXZ(fYAngle.Pointer, fPAngle.Pointer, fRAngle.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Construct this TMatrix3 from YPR Euler Angles.
	ENDREM
	Method FromEulerAnglesYZX(fYAngle:TRadian, fPAngle:TRadian, fRAngle:TRadian) 
		MATRIX3_FromEulerAnglesYZX(fYAngle.Pointer, fPAngle.Pointer, fRAngle.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Construct this YPR TMatrix3 from Euler Angles.
	ENDREM
	Method FromEulerAnglesZXY(fYAngle:TRadian, fPAngle:TRadian, fRAngle:TRadian) 
		MATRIX3_FromEulerAnglesZXY(fYAngle.Pointer, fPAngle.Pointer, fRAngle.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Construct this TMatrix3 from YPR Euler Angles.
	ENDREM
	Method FromEulerAnglesZYX(fYAngle:TRadian, fPAngle:TRadian, fRAngle:TRadian) 
		MATRIX3_FromEulerAnglesZYX(fYAngle.Pointer, fPAngle.Pointer, fRAngle.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Eigen Solve Symmetric! 
	ENDREM
	Method EigenSolveSymmetric(k1:Float, k2:Float, k3:Float, v1:TVector3, v2:TVector3, v3:TVector3) 
		MATRIX3_EigenSolveSymmetric(k1, k2, k3, v1.Pointer, v2.Pointer, v3.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the Tensor product.
	ENDREM
	Function TensorProduct(rkU:TVector3, rkV:TVector3, rkProduct:TMatrix3) 
		MATRIX3_TensorProduct(rkU.Pointer, rkV.Pointer, rkProduct.Pointer) 
	End Function

	Rem
		bbdoc: Helper value for getting epsilon.
	ENDREM
	Function EPSILON:Float() 
		Return MATRIX3_EPSILON() 
	End Function
	
	Rem
		bbdoc: Helper function for getting a ZERO 3x3 matrix.
	ENDREM
	Function ZERO:TMatrix3() 
		Local Obj:TMatrix3 = New TMatrix3
		Obj.Pointer = MATRIX3_ZERO() 
		Return Obj
	End Function
	
	Rem
		bbdoc: Helper function for getting an IDENTITY 3x3 matrix.
	ENDREM
	Function IDENTITY:TMatrix3() 
		Local Obj:TMatrix3 = New TMatrix3
		Obj.Pointer = MATRIX3_IDENTITY() 
		Return Obj
	End Function
	
	Rem
		bbdoc: Get a string that represents the values in this TMatrix3.
	ENDREM
	Method toStr:String() 
		Local tempString:String
		Return tempString.FromCString(MATRIX3_toStr(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Destructor for destroying unmanaged TMatrix3 Instances.
	ENDREM
	Method Delete() 
		If Self.isManaged = False Then MATRIX3_delete(Self.Pointer) 
	End Method
	
End Type

Rem
	bbdoc: A type that describes a homogenous 4x4 matrix.
ENDREM
Type TMatrix4

	Rem
		bbdoc: Pointer to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create an empty TMatrix4.
	ENDREM
	Function createEmpty:TMatrix4()
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_createEmpty()
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a 4x4 array from a group of 16 floats. Instead of a parameter list, a quick mapping of the parameters is shown below: </br> m00 , m01 , m02 , m03 </br> m10 , m11 , m12 , m13 </br> m20 , m21 , m22 , m23 </br> m30 , m31 , m32 , m33 <./br>
	ENDREM
	Function Create:TMatrix4( m00:Float , m01:Float , m02:Float , m03:Float , m10:Float , m11:Float , m12:Float , m13:Float , m20:Float , m21:Float , m22:Float , m23:Float , m30:Float , m31:Float , m32:Float , m33:Float )
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_create( m00 , m01 , m02 , m03 , m10 , m11 , m12 , m13 , m20 , m21 , m22 , m23 , m30 , m31 , m32 , m33 )
		Return Obj
	End Function
	
	Rem
		bbdoc: Creates a 4x4 array from a 3x3 with zero translation.<BR>m3x3:TMatrix3 The matrix you will use to create the 4x4 Matrix.
	ENDREM
	Function createWithMatrix3:TMatrix4(m3x3:TMatrix3) 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_createWithMatrix3( m3x3.Pointer )	   
		Return Obj
	End Function
	
	Rem
		bbdoc: Creates a 4x4 Transformation matrix with zero translation from a Quaternion.<BR>rot:TQuaternion The quaternion to derive the transform matrix from.
	ENDREM
	Function createWithQuaternion:TMatrix4(rot:TQuaternion) 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_createWithQuaternion( rot.Pointer )
		Return Obj
	End Function
	
	Rem
		bbdoc: Gets a single value in the 4x4 matrix, processing it like a 2-dimensional array from ( 0 , 0 ) to ( 3 , 3 ).<BR>iRow:Int The row of the value that you want to select.<BR>iColumn:Int The column of the value that you want to select.
	ENDREM
	Method getValue:Float(iRow:Int, iColumn:Int) 
		Return MATRIX4_getValue( iRow , iColumn , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Concatenate this 4x4 matrix with another 4x4 matrix and return the resulting 4x4 matrix. This is basically matrix multiplication. If you need to combine the effects of several matrices, generally you will concatenate them.
	ENDREM
	Method concatenate:TMatrix4(m2:TMatrix4) 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_concatenate( m2.Pointer , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Helper function, same as concatenate:  Concatenate this 4x4 matrix with another 4x4 matrix And Return the resulting 4x4 matrix. This is basically matrix multiplication. If you need To combine the effects of several matrices, generally you will concatenate them.
	ENDREM
	Method mul:TMatrix4(m2:TMatrix4) 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_concatenate( m2.Pointer , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Transforms a 3d vector by this 4x4 matrix and returns the transformed 3d vector. w = 1 is assumed, and the resulting vector is all vector elements divided by w.
	ENDREM
	Method mulWithVector3:TVector3(v:TVector3) 
		Return TVector3.FromPtr(MATRIX4_mulWithVector3(v.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Transforms a 4d vector by this 4x4 matrix and returns a transformed 4d vector.
	ENDREM
	Method mulWithVector4:TVector4(v:TVector4) 
		Return TVector4.FromPtr(MATRIX4_mulWithVector4(v.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Transforms a plane by this 4x4 matrix and returns the transformed plane.
	ENDREM
	Method mulWithPlane:TPlane(p:TPlane) 
		Local Obj:TPlane = New TPlane
		Obj.Pointer = MATRIX4_mulWithPlane( p.Pointer , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Adds this 4x4 matrix to this one and returns the sum.
	ENDREM
	Method add:TMatrix4(m2:TMatrix4) 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_add( m2.Pointer , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Subtracts this 4x4 matrix from this one and returns the difference.
	ENDREM
	Method sub:TMatrix4(m2:TMatrix4) 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_sub( m2.Pointer , Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Checks the equality of this TMatrix4 against another. Usage:  If matrix1.isEqualTo(matrix2) is the same as If matrix1 = matrix2.
	ENDREM
	Method isEqualTo:Int(m2:TMatrix4) 
		Return MATRIX4_isEqualTo( m2.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Checks the inequality of this TMatrix4 against another. Usage: If matrix1.isNotEqualTo(matrix2) is the same as If matrix1 <> matrix2.
	ENDREM
	Method isNotEqualTo:Int(m2:TMatrix4) 
		Return MATRIX4_isNotEqualTo( m2.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Operator allowing you to set this matrix's values ot the values of another 4x4 matrix.
	ENDREM
	Method Eq(m2:TMatrix4) 
		MATRIX4_eq( m2.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Operator allowing you to set this matrix's values ot that of a 3x3 matrix.<br>Usage: matrix1.eq(matrix2) is the same as matrix1 = matrix2.
	ENDREM
	Method eqWithMatrix3(mat3:TMatrix3) 
		MATRIX4_eqWithMatrix3( mat3.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Transposes this 4x4 matrix. When you transpose a matrix, you are swapping the rows and the colums.
	ENDREM
	Method Transpose:TMatrix4() 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_transpose( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Sets the part of the matrix concerned with translation via a 3d vector.
	ENDREM
	Method setTrans(v:TVector3) 
		MATRIX4_setTrans( v.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Gets the part of the matrix concerned with translation via a 3d vector.
	ENDREM
	Method getTrans:TVector3() 
		Return TVector3.FromPtr(MATRIX4_getTrans(Self.Pointer)) 	
	End Method
	
	Rem
		bbdoc: Makes this 4x4 matrix into strictly a translation matrix.<br>v:TVector3 The translation values as a 3-dimensional vector.
	ENDREM
	Method makeTrans(v:TVector3) 
		MATRIX4_makeTrans( v.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Makes this 4x4 matrix into strictly a translation matrix using 3 XYZ floating point values.
	ENDREM
	Method makeTransWithFloat(tx:Float, ty:Float, tz:Float) 
		MATRIX4_makeTransWithFloat( tx , ty , tz , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the scale values of the 4x4 matrix using a 3-dimensional vector.<br>v:TVector3 The 3-dimensional vector containing scale values.
	ENDREM
	Method SetScale(v:TVector3) 
		MATRIX4_setScale( v.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Derive a TMatrix3 from this 4x4 matrix. Please note that you must construct this TMatrix3 with .createEmpty or aonther .create method, as an unitiialized TMatrix3 variable is invalid.
	ENDREM
	Method extract3x3Matrix(m3x3:TMatrix3) 
		MATRIX4_extract3x3Matrix( m3x3.Pointer , Self.Pointer )	
	End Method
	
	Rem
		bbdoc: Extract the rotation and scaling information from this matrix as a TQuaternion.
	ENDREM
	Method extractQuaternion:TQuaternion() 
		Return TQuaternion.FromPtr(MATRIX4_extractQuaternion(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Multiply this 4x4 matrix by a float/scalar value and return the resulting matrix.
	ENDREM
	Method mulWithScalar:TMatrix4(scalar:Float)
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_mulWithScalar( scalar , Self.Pointer )   
		Return Obj
	End Method
	
	Rem
		bbdoc: Returns the adjoint of the 4x4 Matrix. This is computed by first cofactoring the matrix, then transposing the cofactor.
	ENDREM
	Method adjoint:TMatrix4() 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_adjoint( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Returns the determinant of this matrix.
	ENDREM
	Method Determinant:Float() 
		Return MATRIX4_determinant( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the inverse of this 4x4 matrix.
	ENDREM
	Method Inverse:TMatrix4() 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_inverse( Self.Pointer )
		Return obj
	End Method
	
	Rem
		bbdoc: Make this matrix purely into a transformation matrix with values for position, scale and orientation.<br>position:TVector3 The translation of this transformation matrix.<br>scale:TVector3 The scale of this transformation matrix.<br>orientation:TQuaternion The rotatin of this transformation matrix.
	ENDREM
	Method makeTransform(Position:TVector3, scale:TVector3, orientation:TQuaternion) 
		MATRIX4_makeTransform( position.Pointer , scale.Pointer , orientation.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Make this matrix purely into an inverse transformation matrix with values for position, scale and orientation.<br>position:TVector3 The translation of this transformation matrix.<br>scale:TVector3 The scale of this transformation matrix.<br>orientation:TQuaternion The rotatin of this transformation matrix.
	ENDREM
	Method makeInverseTransform(Position:TVector3, scale:TVector3, orientation:TQuaternion) 
		MATRIX4_makeInverseTransform( position.Pointer , scale.Pointer , orientation.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Determines if this matrix is an affine transformation matrix or not, and returns TRUE or FALSE.
	ENDREM
	Method isAffine:Int() 
		Return MATRIX4_isAffine( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns the inverse of this  4x4 affine  matrix. To work properly, this matrix must first be affine.
	ENDREM
	Method inverseAffine:TMatrix4() 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_inverseAffine( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Concatenate/Multiply this matrix against another 4x4 affine matrix and return the result.<br>m2:TMatrix4 The other 4x4 matrix to concatenate/mulitply.
	ENDREM
	Method concatenateAffine:TMatrix4(m2:TMatrix4) 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer =  MATRIX4_concatentateAffine( m2.Pointer, Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Transforms the given vector by an affine matrix. This matrix must be affine in order for this to compute properly.<br>v:TVector3 Vector given to transform by.
	ENDREM
	Method transformAffine:TVector3(v:TVector3) 
		Return TVector3.FromPtr(MATRIX4_transformAffine(v.Pointer, Self.Pointer)) 	
	End Method
	
	Rem
		bbdoc: Transforms the given vector by an affine matrix. This matrix must be affine in order for this to compute properly.
	ENDREM
	Method transformAffineWithVector4:TVector4(v:TVector4) 
		Return TVector4.FromPtr(MATRIX4_transformAffineWithVector4(v.Pointer, Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Gets a translation matrix from a given vector. Use these static functions to create stock translation and scale matrices.
	ENDREM
	Function getStaticTrans:TMatrix4(v:TVector3) 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_getStaticTrans( v.Pointer )
		Return Obj
	End Function
	
	Rem
		bbdoc: Gets a translation matrix from a set of xyz floats.	 Use these static functions to create stock translation and scale matrices.
	ENDREM
	Function getStaticTransWithFloat:TMatrix4(t_x:Float, t_y:Float, t_z:Float) 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_getStaticTransWithFloat( t_x , t_y , t_z )
		Return Obj	
	End Function
	
	Rem
		bbdoc: Gets a scale matrix derived from a 3d vector. Use these static functions to create stock translation and scale matrices.
	ENDREM
	Function getStaticScale:TMatrix4(v:TVector3) 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_getStaticScale( v.Pointer )
		Return Obj
	End Function
	
	Rem
		bbdoc: Gets a scale  matrix from a set of xyz floats. Use these static functions to create stock translation and scale matrices.
	ENDREM
	Function getStaticScaleWithFloat:TMatrix4(s_x:Float, s_y:Float, s_z:Float) 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_getStaticTransWithFloat( s_x , s_y , s_z )
		Return Obj	
	End Function
	
	Rem
		bbdoc: Use this static function to return a 4x4 ZERO matrix when needed.
	ENDREM
	Function ZERO:TMatrix4() 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_ZERO()
		Return Obj
	End Function
	
	Rem
		bbdoc: Use this static function to return a 4x4 IDENTITY matrix when needed.
	ENDREM
	Function IDENTITY:TMatrix4() 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_IDENTITY()
		Return Obj
	End Function
	
	Rem
		bbdoc: Use this static function to return a 4x4 CLIPSPACE2DTOIMAGESPACE matrix when needed. This provides you with a 2d clip space with an inverted y-axis.
	ENDREM
	Function CLIPSPACE2DTOIMAGESPACE:TMatrix4() 
		Local Obj:TMatrix4 = New TMatrix4
		Obj.Pointer = MATRIX4_CLIPSPACE2DTOIMAGESPACE()
		Return Obj
	End Function
	
	Rem
		bbdoc: Outputs a string containing the TMatrix4x4s contents. Used for debugging.
	ENDREM
	Method toStr:String() 
		Local temp:String
		Return temp.FromCString( MATRIX4_toStr( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		MATRIX4_delete( Self.Pointer )
	End Method
	
	
End Type

Rem
	bbdoc: Type that manages overlay files, allowing you to manipulate them after loaded from .overlay files, etc.
ENDREM
Type TOverlayManager
	 
	Rem
		bbdoc: Create a new, managed overlay. The name specified must be unique.
	ENDREM
	Function Create:TOverlay(name:String)
		Local Obj:TOverlay = New TOverlay
		Obj.Pointer = OM_create(name.ToCString())
		Return Obj
	End Function
	
	Rem
		bbdoc: Get an overlay object by specifying it's name.
	ENDREM
	Function getByName:TOverlay(name:String) 
		Local Obj:TOverlay = New TOverlay
		Obj.Pointer = OM_getByName(name.ToCString()) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Destroy an existing managed overlay by specifying its name.
	ENDREM
	Function destroyWithName(name:String)
		OM_destroyWithName(name.ToCString())
	End Function
	
	Rem
		bbdoc: Destroy an existing managed overlay by specifying an instance of it.
	ENDREM
	Function destroyWithInstance(overlay:TOverlay)
		OM_destroyWithInstance(overlay.Pointer)
	End Function
	
	Rem
		bbdoc: Destroy all managed overlays.
	ENDREM
	Function destroyAll()
		OM_destroyAll()
	End Function
	
	Rem
		bbdoc: Get an iterator to iterate through all the managed overlays. This is currently not fully implemented.
	ENDREM
	Function getOverlayIterator:TOverlayIterator()
		Local Obj:TOverlayIterator = New TOverlayIterator
		Obj.Pointer = OM_getOverlayIterator()
		Return Obj
	End Function
	
	Rem
		bbdoc: Internal method that queues all visible overlays for rendering with a specified camera, viewport and render queue.
	ENDREM	 
	Function _queueOverlaysForRendering(cam:TCamera, pQueue:TRenderQueue, vp:TViewport)
		OM__queueOverlaysForRendering(cam.Pointer, pQueue.Pointer, vp.Pointer)
	End Function
	
	Rem
		bbdoc: Check to see if the viewport has changed sizes since the last call to this function.
	ENDREM
	Function hasViewportChanged:Byte()
		Return OM_hasViewportChanged()
	End Function
	
	Rem
		bbdoc: Get the viewport height in pixels.
	ENDREM
	Function getViewportHeight:Int()
		Return OM_getViewportHeight()
	End Function
	
	Rem
		bbdoc: Get the viewport width in pixels.
	ENDREM
	Function getViewportWidth:Int()
		Return OM_getViewportWidth()
	End Function
	
	Rem
		bbdoc: Get viewport aspect ratio.
	ENDREM
	Function getViewportAspectRatio:Float()
		Return OM_getViewportAspectRatio()
	End Function
	
	Rem
		bbdoc: Create an overlay element of the type specified, giving it a unique instance name. Preconfigured types include "Panel" , "BorderPanel" and "TextArea".
	ENDREM
	Function createOverlayElement:TOverlayElement(typeName:String, instanceName:String, isTemplate:Byte = False)
		Local Obj:TOverlayElement = New TOverlayElement
		Obj.Pointer = OM_createOverlayElement(typeName.ToCString() , instanceName.ToString() , isTemplate)
		Return Obj
	End Function
	
	Rem
		bbdoc: Get an overlay element by name.
	ENDREM
	Function getOverlayElement:TOverlayElement(name:String, isTemplate:Byte = False)
		Local Obj:TOverlayElement = New TOverlayElement
		Obj.Pointer = OM_getOverlayElement(name.ToCString() , isTemplate)
		Return Obj	
	End Function
	
	Rem
		bbdoc: Destroy overlay element instance with name.
	ENDREM
	Function destroyOverlayElementWithName(instanceName:String, isTemplate:Byte = False)
		OM_destroyOverlayElementWithName(instanceName.ToCString() , isTemplate)
	End Function
	
	Rem
		bbdoc: Destroy an overlay element with a specified element instance.
	ENDREM
	Function destroyOverlayElement(pInstance:TOverlayElement, isTemplate:Byte = False)
		OM_destroyOverlayElement(pInstance.Pointer, isTemplate)
	End Function
	
	Rem
		bbdoc: Destroy all managed overlay elements.
	ENDREM
	Function destroyAllOverlayElements(isTemplate:Byte = False)
		OM_destroyAllOverlayElements(isTemplate)
	End Function
	
	Rem
		bbdoc: Add an overlay element factory. TOverlayElementFactories are not yet fully implemented.
	ENDREM
	Function addOverlayElementFactory(elemFactory:TOverlayElementFactory)
		OM_addOverlayElementFactory(elemFactory.Pointer)
	End Function
	
	Rem
		bbdoc: Create an overlay element from a template name and specify a unique instance name.
	ENDREM
	Function createOverlayElementFromTemplate:TOverlayElement(templateName:String, typeName:String, instanceName:String, isTemplate:Byte = False)
		Local Obj:TOverlayElement = New TOverlayElement
		Obj.Pointer = OM_createOverlayElementFromTemplate(templateName.ToCString() , typeName.ToCString() , instanceName.ToCstring() , isTemplate)
		Return Obj
	End Function
	
	Rem
		bbdoc: Clone an overlay element from a overlay element template and specify a unique instance name.
	ENDREM
	Function cloneOverlayElementFromTemplate:TOverlayElement(templateName:String, instanceName:String)
		Local Obj:TOverlayElement = New TOverlayElement
		Obj.Pointer = OM_cloneOverlayElementFromTemplate(templateName.ToCString() , instanceName.ToCString())
		Return Obj
	End Function
	
	Rem
		bbdoc: Create an overlay element from an overlay element factory ( typeName )  then specify a unique instance name.
	ENDREM
	Function createOverlayElementFromFactory:TOverlayElement(typeName:String, instanceName:String)
		Local Obj:TOverlayElement = New TOverlayElement
		Obj.Pointer = OM_createOverlayElementFromFactory(typeName.ToCString() , instanceName.ToCString())
		Return Obj
	End Function
	
	Rem
		bbdoc: Get an iterator to iterate through all managed template overlay elements. TTemplateIterator is not fully implemented yet.
	ENDREM
	Function getTemplateIterator:TTemplateIterator()
		Local Obj:TTemplateIterator = New TTemplateIterator
		Obj.Pointer = OM_getTemplateIterator()
		Return Obj
	End Function
	
	Rem
		bbdoc: Get whether or not a specified object ( by name ) is an overlay element template.
	ENDREM
	Function isTemplate:Byte(strName:String)
		Return OM_isTemplate(strName.ToCString())
	End Function
	
	'---TScriptLoader---
	
	Rem
		bbdoc: Gets the file patterns which are used to search for scripts of this type. Returns a list of patterns in the order they should be searched in. TStringVector is not yet fully implemented.
	ENDREM
	Function getScriptPatterns:TStringVector()
		Local Obj:TStringVector = New TStringVector
		Obj.Pointer = OM_getScriptPatterns()
		Return Obj
	End Function
	
	Rem
		bbdoc: Parses any script from a data stream/reference to the script which is created by giving the datastream a file name or handle to the resource script.<br>stream:TOgreDataStream  Standard data stream to be used.<br>groupName:String If any resources are created from the data stream, they will belong to the group specified.	This method is not currently supported.
	ENDREM
	Function parseScript(stream:TOgreDataStream, groupName:String)
		OM_parseScript(stream.Pointer, groupName.ToCString())
	End Function
	
	Rem
		bbdoc: Get the loading order of scripts of this type. The higher the number, the later the script is loaded.
	ENDREM
	Function getLoadingOrder:Float()
		Return OM_getLoadingOrder()
	End Function
	
	
End Type

Rem
	bbdoc: Type representing an area that is "overlayed" on top of a given rendering area. You may either use the TOverlayManager to .Create() a new overlay, or define them in special .overlay scripts.
END REM
Type TOverlay

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get a child TOverlay container that is part of this overlay.
	END REM
	Method getChild:TOverlayContainer(Name:String) 
		Local Obj:TOverlayContainer = New TOverlayContainer
		Obj.Pointer = OVERLAY_getChild(Name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the name of this overlay.
	ENDREM
	Method getName:String()
		Return String.FromCString(OVERLAY_getName(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Set the ZOrder of this overlay. Orders range from 0-650.
	ENDREM
	Method setZOrder(zorder:Int)
		OVERLAY_setZOrder(zorder, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the ZOrder for this overlay. Values range from 0-650.
	ENDREM
	Method getZOrder:Int()
		Return OVERLAY_getZOrder(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not this overlay is visible.
	END REM
	Method isVisible:Byte() 
		Return OVERLAY_isVisible(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not this TOverlay has been initialised.
	ENDREM
	Method isInitialised:Byte()
		Return OVERLAY_isInitialised(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Make this overlay visible.
	END REM
	Method show() 
		OVERLAY_show(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Make this overlay invisible.
	END REM
	Method hide() 
		OVERLAY_hide(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Add a 2D container to this overlay. Containers are what make up most of the things inside an overlay. You can create containers with the TOverlayManager.
	ENDREM
	Method add2D(cont:TOverlayContainer)
		OVERLAY_add2D(cont.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Remove a 2d container from this TOverlay.
	ENDREM
	Method remove2D(cont:TOverlayContainer)
		OVERLAY_remove2D(cont.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Adds a 3D scene node to this Overlay. This allows you to add 3d Geometry around the overlay that is relative to the camera's coordinate space and is guaranteed to be rendered over everything else. A good example of this would be something like a gun view in an FPS, or a cockpit view in a flight simulator.
	ENDREM
	Method add3D(node:TSceneNode)
		OVERLAY_add3D(node.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Remove a 3D scene node from this TOverlay.
	ENDREM
	Method remove3D(node:TSceneNode)
		OVERLAY_remove3D(node.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Clears the overlay of any of its attached items.
	ENDREM
	Method clear()
		OVERLAY_clear(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the scrolling factor of this overlay. Can be used to scroll around the screen. Coordinates are in relative space - an example would be that y = 0.5 would scroll down half a screen.
	ENDREM
	Method setScroll(x:Float, y:Float)
		OVERLAY_setScroll(x, y, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the x axis scaling factor for this TOverlay.
	ENDREM
	Method getScrollX:Float()
		Return OVERLAY_getScrollX(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the y axis scaling factor for this TOverlay.
	ENDREM
	Method getScrollY:Float()
		Return OVERLAY_getScrollY(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Scrolls by the offset provided. The offset is in relative coordinates (0.0-1.0).
	ENDREM
	Method scroll(xoff:Float, yoff:Float)
		OVERLAY_scroll(xoff, yoff, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the rotation of this overlay in radians.
	ENDREM
	Method setRotate(angle:TRadian)
		OVERLAY_setRotate(angle.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the rotation of this overlay in radians.
	ENDREM
	Method getRotate:TRadian()
		Local Obj:TRadian = New TRadian
		Obj.Pointer = OVERLAY_getRotate(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Add the given angle to this overlay's current rotation factor.
	ENDREM
	Method rotate(angle:TRadian)
		OVERLAY_rotate(angle.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the scale factors of this TOverlay. Allows you to effectively zoom in and out of the overlay. Coords are in relative values (0.0-1.0).
	ENDREM
	Method SetScale(x:Float, y:Float)
		OVERLAY_setScale(x, y, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the x axis scaling factor for this TOverlay.
	ENDREM
	Method getScaleX:Float()
		Return OVERLAY_getScaleX(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the x axis scaling factor for this TOverlay.
	ENDREM
	Method getScaleY:Float()
		Return OVERLAY_getScaleY(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get this overlay's transforms that specify all of its scaling, rotation, etc. in world space. You must pass in a created TMatrix4 and it will be populated.
	ENDREM
	Method _getWorldTransforms(xform:TMatrix4)
		OVERLAY__getWorldTransforms(xform.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Internal method that gets the overlay contents into the render queue.<br/>cam:TCamera - Specifies the camera that this TOverlay is rendered from.<br/>queue:TRenderQueue - Represents the render queue that this TOverlay is in.
	ENDREM
	Method _findVisibleObjects(cam:TCamera, queue:TRenderQueue)
		OVERLAY__findVisibleObjects(cam.Pointer, queue.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Gets an overlay element that is present at x,y. Returns a Null TOverlayElement if there is nothing at the specified point, so check with .isNull before using!
	ENDREM
	Method findElementAt:TOverlayElement(x:Float, y:Float)
		Local Obj:TOverlayElement = New TOverlayElement
		Obj.Pointer = OVERLAY_findElementAt(x, y, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get an iterator to iterate through all 2D elements. This method is not yet supported.
	ENDREM
	Method get2DElementsIterator:TOverlay2DElementsIterator()
		Local Obj:TOverlay2DElementsIterator = New TOverlay2DElementsIterator
		Obj.Pointer = OVERLAY_get2DElementsIterator(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the origin of this TOverlay. This could be the script file that loaded the overlay, or something a user set manually with _notifyOrigin.
	ENDREM
	Method GetOrigin:String()
		Return String.FromCString(OVERLAY_getOrigin(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Internal method to set the origin of this TOverlay. This is used by script loaders to set the origin, but you can use it for manually created instances to manage them by origin.
	ENDREM
	Method _notifyOrigin(origin:String)
		OVERLAY__notifyOrigin(origin.ToCString() , Self.Pointer)
	End Method
	
	Rem
		bbdoc:Helper Function To determine whether Or Not the reference To this overlay Object is Null.
	END REM
	Method isNull:Byte() 
		If Self.Pointer = Null Then
			Return True
		Else
			Return False
		End If
	End Method
	
End Type

Rem
	bbdoc: An iterator that holds 2D overlay elements. This type is not fully supported yet.
ENDREM
Type TOverlay2DElementsIterator

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
End Type

Rem
	bbdoc: A 2d element that can be displayed inside a TOverlay.
END REM
Type TOverlayElement Extends TRenderable

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Initialise this overlay element.
	ENDREM
	Method initialise()
		OVERLAYELEM_initialise(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the name of this overlay element.
	ENDREM
	Method getName:String()
		Return String.FromCString(OVERLAYELEM_getName(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Show this element.
	ENDREM
	Method show()
		OVERLAYELEM_show(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Hide this element.
	ENDREM
	Method hide()
		OVERLAYELEM_hide(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not this overlay element is visible.
	ENDREM
	Method isVisible:Byte()
		Return OVERLAYELEM_isVisible(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not this overlay element is enabled.
	ENDREM
	Method isEnabled:Byte()
		Return OVERLAYELEM_isEnabled(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set whether or not this overlay element is enabled.
	ENDREM
	Method setEnabled(b:Byte)
		OVERLAYELEM_setEnabled(b, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the dimensions of this overlay element in relative coordinates.(0.0-1.0).
	ENDREM
	Method setDimensions(width:Float, height:Float)
		OVERLAYELEM_setDimensions(width, height, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the position of this element in relative coordinates (0.0-1.0) relative to the top and left of the screen.
	ENDREM
	Method setPosition(Left:Float, top:Float)
		OVERLAYELEM_setPosition(Left, top, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the width of this element in relative coordinates (0.0-1.0).
	ENDREM
	Method setWidth(width:Float)
		OVERLAYELEM_setWidth(width, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the width of this element in relative coordinates (0.0-1.0).
	ENDREM
	Method getWidth:Float()
		Return OVERLAYELEM_getWidth(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the height of this element in relative coordinates (0.0-1.0).
	ENDREM
	Method setHeight(height:Float)
		OVERLAYELEM_setHeight(height, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the height of this element in relative coordinates (0.0-1.0).
	ENDREM
	Method getHeight:Float()
		Return OVERLAYELEM_getHeight(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the distance from the left of the overlay that this element sits in relative coordinates (0.0-1.0).
	ENDREM
	Method setLeft(Left:Float)
		OVERLAYELEM_setLeft(Left, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the distance from the left of the overlay that this element sits in relative coordinates (0.0-1.0).
	ENDREM
	Method getLeft:Float()
		Return OVERLAYELEM_getLeft(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the distance from the top of the overlay that this element sits in relative coordinates (0.0-1.0).
	ENDREM
	Method setTop(top:Float)
		OVERLAYELEM_setTop(top, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the dinstance from the top of the overlay that this element sits in relative coordinates (0.0-1.0).
	ENDREM
	Method getTop:Float()
		Return OVERLAYELEM_getTop(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the name of the material that this overlay element uses.
	ENDREM
	Method getMaterialName:String()
		Return String.FromCString(OVERLAYELEM_getMaterialName(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Set the name of the material that this overlay element uses.
	ENDREM
	Method setMaterialName(matName:String)
		OVERLAYELEM_setMaterialName(matName.ToCString() , Self.Pointer)
	End Method
	
	Rem
		bbdoc: Gets a reference to the TMaterial object that this TRenderable uses.
	ENDREM
	Method getMaterial:TMaterial() 
		Local Obj:TMaterial = New TMaterial
		Obj.Pointer = OVERLAYELEM_getMaterial(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets any world transform matrices for this TRenderable.<BR>xform:TMatrix4 Stores the world transform matrices.
	ENDREM
	Method getWorldTransforms(xform:TMatrix4) 
		OVERLAYELEM_getWorldTransforms( xform.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Tell this element to recalculate its size and position.
	ENDREM
	Method _positionsOutOfDate()
		OVERLAYELEM__positionsOutOfDate(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Internal method that updates this element with its given transforms.
	ENDREM
	Method _update()
		OVERLAYELEM__update(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Internal method that updates this element with transforms obtained by its parent.
	ENDREM
	Method _updateFromParent()
		OVERLAYELEM__updateFromParent(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Internal method for notifying this element's parent and overlay.
	ENDREM
	Method _notifyParent(parent:TOverlayContainer, overlay:TOverlay)
		OVERLAYELEM__notifyParent(parent.Pointer, overlay.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the derived Left position as derived from this element's position as well as its parents.
	ENDREM
	Method _getDerivedLeft:Float()
		OVERLAYELEM__getDerivedLeft(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the derived Top position as derived from this element's position as well as its parents.
	ENDREM
	Method _getDerivedTop:Float()
		OVERLAYELEM__getDerivedTop(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the relative width of this object by this objects width. This value is governed by the current metrics mode.
	ENDREM
	Method _getRelativeWidth:Float()
		OVERLAYELEM__getRelativeWidth(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the relative height of this object by this objects width. This value is governed by the current metrics mode.
	ENDREM
	Method _getRelativeHeight:Float()
		OVERLAYELEM__getRelativeHeight(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the clipping region/rectangle for this element. You pass in a created rectangle object and it is populated with the clipping rectangle values. This method is not yet fully supported.
	ENDREM
	Method _getClippingRegion:TRectangle(clippingRegion:TRectangle)
		Local Obj:TRectangle = New TRectangle
		Obj.Pointer = New TRectangle
		Return Obj
	End Method
	
	Rem
		bbdoc: Internal method that notifies this overlay element when its parent ZOrder has been changed. Returns old ZOrder.
	ENDREM
	Method _notifyZOrder:Int(newZOrder:Int)
		Return OVERLAYELEM__notifyZOrder(newZOrder, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Internal method to notify this element of when its world transform has changed ( via parent or other ).
	ENDREM
	Method _notifyWorldTransforms(xform:TMatrix4)
		OVERLAYELEM__notifyWorldTransforms(xform.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Internal method of notifying the viewport for this overlay's parent has changed.
	ENDREM
	Method _notifyViewport()
		OVERLAYELEM__notifyViewport(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Internal method that will put this overlay's contents into the renderqueue specified.
	ENDREM
	Method _updateRenderQueue(queue:TRenderQueue)
		OVERLAYELEM__updateRenderQueue(queue.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Returns this element's type name.
	ENDREM
	Method getTypeName:String()
		Return String.FromCString(OVERLAYELEM_getTypeName(Self.Pointer))
	End Method

	Rem
		bbdoc: Set a text caption for this overlay element. Note that only some elements support setting captions.
	END REM
	Method setCaption(text:String) 
		OVERLAYELEM_setCaption(text.ToCString() , Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the text caption for this overlay element if it supports it.
	ENDREM
	Method getCaption:String()
		Return String.FromCString(OVERLAYELEM_getCaption(Self.Pointer))
	End Method
	
	Rem
		bbdoc: Set the colour of elements that support colours.
	ENDREM
	Method setColour(col:TColourValue)
		OVERLAYELEM_setColour(col.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the colour of this element if it supports it.
	ENDREM
	Method getColour:TColourValue()
		Local Obj:TColourValue = New TColourValue
		Obj.Pointer = OVERLAYELEM_getColour(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Sets the current metrics mode, which describes how this element's current position and width infromation is interpreted.You will find the constants for this function in TGuiMetricsMode.  
	ENDREM
	Method setMetricsMode(gmm:Int)
		OVERLAYELEM_setMetricsMode(gmm, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Gets the current metrics mode, which describes how this element's current position and width information is interpreted. You will find the constants for this function in TGuiMetricsMode.
	ENDREM
	Method getMetricsMode:Int()
		Return OVERLAYELEM_getMetricsMode(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Sets the horizontal alignment, or where the origin is considered to be for this element. The values used here correspond with the constants in TGuiHorizontalAlignment.
	ENDREM
	Method setHorizontalAlignment(gha:Int)
		OVERLAYELEM_setHorizontalAlignment(gha, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Gets the horizontal alignment, or where the origin is considered to be for this element. The values used here correspond with the constants in TGuiHorizontalAlignment.
	ENDREM
	Method getHorizontalAlignment:Int()
		Return OVERLAYELEM_getHorizontalAlignment(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Sets the vertical alignment, or where the origin is considered to be for this element. The values used here correspond with the constants in TGuiVerticalAlignment.
	ENDREM
	Method setVerticalAlignment(gva:Int)
		OVERLAYELEM_setVerticalAlignment(gva, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Gets the vertical alignment, or where the origin is considered to be for this element. The values used here correspond with the constants in TGuiVerticalAlignment.
	ENDREM
	Method getVerticalAlignment:Int()
		Return OVERLAYELEM_getVerticalAlignment(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Returns whether or not (x,y) is inside this element.
	ENDREM
	Method contains:Byte(x:Float, y:Float)
		Return OVERLAYELEM_contains(x, y, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Returns the element that is found within this element at (x,y). You should check the element with .isNull before processing.
	ENDREM
	Method findElementAt:TOverlayElement(x:Float, y:Float)
		Local Obj:TOverlayElement = New TOverlayElement
		Obj.Pointer = OVERLAYELEM_findElementAt(x, y, Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get whether or not this class is a container type.
	ENDREM
	Method isContainer:Byte()
		Return OVERLAYELEM_isContainer(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not key is enabled.
	ENDREM
	Method isKeyEnabled:Byte()
		Return OVERLAYELEM_isKeyEnabled(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not this element is cloneable.
	ENDREM
	Method isCloneable:Byte()
		Return OVERLAYELEM_isCloneable(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set whether or not this element is cloneable.
	ENDREM
	Method setCloneable(c:Byte)
		OVERLAYELEM_setCloneable(c, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the parent container of this element.
	ENDREM
	Method getParent:TOverlayContainer()
		Local Obj:TOverlayContainer = New TOverlayContainer
		Obj.Pointer = OVERLAYELEM_getParent(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Internal method to set the parent of this element.
	ENDREM
	Method _setParent(parent:TOverlayContainer)
		OVERLAYELEM__setParent(parent.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the ZOrder of this element.
	ENDREM
	Method getZOrder:Int()
		Return OVERLAYELEM_getZOrder(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Returns the squared depth of this renderable, relative to the specified camera.
	ENDREM
	Method getSquaredViewDepth:Float(cam:TCamera)
		Return OVERLAYELEM_getSquaredViewDepth(cam.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get a list of lights, ordered by how close they are to this TRenderable. This method is not yet fully supported.
	ENDREM
	Method getLights:TLightList()
		Local Obj:TLightList = New TLightList
		Obj.Pointer = OVERLAYELEM_getLights(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Copy the settings for this element from a template element.
	ENDREM
	Method copyFromTemplate(templateOverlay:TOverlayElement)
		OVERLAYELEM_copyFromTemplate(templateOverlay.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Clone this element, thus creating a new overlay element with the specified instance name.
	ENDREM
	Method clone:TOverlayElement(instanceName:String)
		Local Obj:TOverlayElement = New TOverlayElement
		Obj.Pointer = OVERLAYELEM_clone(instanceName.ToCString() , Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the source template that was used to create this element.
	ENDREM
	Method getSourceTemplate:TOverlayElement()
		Local Obj:TOverlayElement = New TOverlayElement
		Obj.Pointer = OVERLAYELEM_getSourceTemplate(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED: Retreives the list of available parameters for this interface.
	ENDREM
	Method getParamDictionary:TParamDictionary() 
		Local Obj:TParamDictionary = New TParamDictionary
		Obj.Pointer = OVERLAYELEM_getParamDictionary(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: NOT SUPPORTED: Gets a list of useable parameters for this object.
	ENDREM
	Method getParameters:TParameterList()
		Local Obj:TParameterList = New TParameterList
		Obj.Pointer = OVERLAYELEM_getParameters(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: General method for setting parameters for this object. Returns true if setting the parameter was successful.
	ENDREM
	Method setParameter:Int(Name:String, Value:String) 
		Return OVERLAYELEM_setParameter(name.ToCString() , value.ToCString() , Self.Pointer)
	End Method
	
	Rem
		bbdoc: Generic method for setting multiple parameters for this object.
	ENDREM
	Method setParameterList(paramList:TNameValuePairList) 
		OVERLAYELEM_setParameterList(paramList.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: General method for retreiving the value of a parameter specified.
	ENDREM
	Method getParameter:String(Name:String) 
		Return String.FromCString(OVERLAYELEM_getParameter(name.ToCString() , Self.Pointer))
	End Method
	
	Rem
		bbdoc: Method for copying this TStringInterface's parameters to another TStringInterface.<br/>Note: Unfortunately, TStringInterface is a method that is often used with multiple inheritance, which BlitzMax does not support. If you know that your object is derived from StringInterface in Ogre but it is not in Flow3D, you may need to do something like this:  originalObject.copyParametersTo( yourobject.ToTStringInterface() ).
	ENDREM
	Method copyParametersTo(dest:TStringInterface) 
		OVERLAYELEM_copyParametersTo(dest.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Cleans up so that it's containers are not left with invalid pointers. Required if you wish to reset Ogre.
	ENDREM
	Function cleanupDictionary()
		OVERLAYELEM_cleanupDictionary()
	End Function
	
	Rem
		bbdoc: Gets the TTechnique object that is being used to render the TMaterial for this TRenderable.
	ENDREM
	Method getTechnique:TTechnique() 
		Local Obj:TTechnique = New TTechnique
		Obj.Pointer = OVERLAYELEM_getTechnique(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets the TRenderOperation that is required to send this TRenderable to the frame buffer.<BR> op:TRenderOperation The TRenderOperation will be stored in this variable.
	ENDREM
	Method getRenderOperation(op:TRenderOperation) 
		OVERLAYELEM_getRenderOperation(op.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Gets the number of world transforms this TRenderable has applied to it.
	ENDREM
	Method getNumWorldTransforms:Int() 
		Return OVERLAYELEM_getNumWorldTransforms(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Sets whether an identity projection will be used or not.
	ENDREM
	Method setUseIdentityProjection(useIdentityProjection:Int)
		OVERLAYELEM_setUseIdentityProjection(useIdentityProjection, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Retreive whether or not this TRenderable uses an identity projection.
	ENDREM
	Method getUseIdentityProjection:Int()
		Return OVERLAYELEM_getUseIdentityProjection(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Sets whether or not to use an identity view on this TRenderable.
	ENDREM
	Method setUseIdentityView(useIdentityView:Int)
		OVERLAYELEM_setUseIdentityView(useIdentityView, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Retreive whether or not this TRenderable uses an identity view.
	ENDREM
	Method getUseIdentityView:Int()
		Return OVERLAYELEM_getUseIdentityView(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Returns a flag specifying whether this TRenderable casts a shadow or not.
	ENDREM
	Method getCastsShadows:Int()
		Return OVERLAYELEM_getCastsShadows(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set custom rendering parameters for this TRenderable that can be used as quanta for affecting different rendering actions.<br> index:Long An index To associate the Value with, somewhat like a Key.<br>value:TVector4 The value of the custom parameter.
	ENDREM
	Method setCustomParameter(index:Int, Value:TVector4) 
		OVERLAYELEM_setCustomParameter(index, value.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Retreive a custom parameter set for this TRenderable object by index.
	ENDREM
	Method getCustomParameter:TVector4(index:Int) 
		Return TVector4.FromPtr(OVERLAYELEM_getCustomParameter(index, Self.Pointer))
	End Method
	
	Rem
		bbdoc: Use this to update custom Gpu Parameters for this TRenderable.<br>constantEntry:TAutoConstantEntry Entry referring to the parameter being updated.<br>params:TGpuProgramParameters The parameters object that the TAutoConstantEntry parameter object should call to update the paramseters.
	ENDREM
	Method _updateCustomGpuParameter(constantEntry:TAutoConstantEntry, params:TGpuProgramParameters) 
		OVERLAYELEM__updateCustomGpuParameter(constantEntry.Pointer, params.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Sets whether or not this camera's detail level can be overridden by the camera settings.
	ENDREM
	Method setPolygonModeOverrideable(overrideable:Int)
		OVERLAYELEM_setPolygonModeOverrideable(overrideable, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Return whether or not this TRenderable can have it's detail level overridden by camera settings.
	ENDREM
	Method getPolygonModeOverrideable:Int()
		Return OVERLAYELEM_getPolygonModeOverrideable(Self.Pointer)
	End Method	
	
	Rem
		bbdoc: Cast this TOverlayElement to a TOverlayContainer.
	ENDREM
	Method toOverlayContainer:TOverlayContainer()
		Local Obj:TOverlayContainer = New TOverlayContainer
		Obj.Pointer = Self.Pointer
		Return Obj
	End Method
	

	
	Rem
		bbdoc:Helper Function To determine whether Or Not the reference To this overlay element object is Null.
	END REM
	Method isNull:Byte() 
		If Self.Pointer = Null Then
			Return True
		Else
			Return False
		End If
	End Method
	
End Type

Rem
	bbdoc: Type that holds position / size data constants for absolute/relative coordinate systems.
ENDREM
Type TGuiMetricsMode
	Const GMM_RELATIVE:Int = 0
	Const GMM_PIXELS:Int = 1
	Const GMM_RELATIVE_ASPECT_ADJUSTED:Int = 2
End Type

Rem
	bbdoc: Type that holds where 0 is in relation to vertical alignment.
ENDREM
Type TGuiVerticalAlignment
	Const GVA_TOP:Int = 0
	Const GVA_CENTER:Int = 1
	Const GVA_BOTTOM:Int = 2
End Type

Rem
	bbdoc: Type that holds where 0 is in relation to horizontal alignment.
ENDREM
Type TGuiHorizontalAlignment
	Const GHA_LEFT:Int = 0
	Const GHA_CENTER:Int = 1
	Const GHA_RIGHT:Int = 2
End Type

Rem
	bbdoc: An overlay item that contains multiple TOverlayElements.
END REM
Type TOverlayContainer Extends TOverlayElement
	
	Rem
		bbdoc: Get an overlay element contained in this container by name.
	END REM
	Method getChild:TOverlayElement(Name:String) 
		Local Obj:TOverlayElement = New TOverlayElement
		Obj.Pointer = OVERLAYCONTAINER_getChild(Name.ToCString() , Self.Pointer) 
		Return Obj
	End Method
	
End Type

Rem
	bbdoc: An overlay element that contains panel information.
ENDREM
Type TPanelOverlayElement Extends TOverlayContainer
	
End Type

Rem
	bbdoc: An overlay element that contains border panel information.
ENDREM
Type TBorderPanelOverlayElement Extends TPanelOverlayElement
End Type


Rem
	bbdoc: Iterates through a set of TOverlays. This is not fully implemented yet.
ENDREM
Type TOverlayIterator
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
End Type

Rem
	bbdoc: Iterates through a set of overlay element templates.
ENDREM
Type TTemplateIterator
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
End Type

Rem
	bbdoc: Factory that creates overlay elements. This Type is not fully implemented.
ENDREM
Type TOverlayElementFactory
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Parameter dictionary. This type is not yet fully implemented.
ENDREM
Type TParamDictionary

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: A simple rectangle implementation.
ENDREM
Type TRectangle

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Parameter list. This type is not yet fully implemented.
ENDREM
Type TParameterList

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Vector type for std::vector. This type is not yet fully implemented.
ENDREM
Type TVector

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: A list of render systems. 
ENDREM
Type TRenderSystemList

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create an empty render system list.
	ENDREM
	Function Create:TRenderSystemList()
		Local Obj:TRenderSystemList = New TRenderSystemList
		Obj.Pointer = RSL_Create()
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		RSL_delete( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get an iterator to the beginning of this list.
	ENDREM
	Method begin:TRenderSystemListIterator()
		Local Obj:TRenderSystemListIterator = New TRenderSystemListIterator
		Obj.Pointer = RSL_begin( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get an iterator to the end of this list.
	ENDREM
	Method Ending:TRenderSystemListIterator()
		Local Obj:TRenderSystemListIterator = New TRenderSystemListIterator
		Obj.Pointer = RSL_end(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the size of this list.
	ENDREM
	Method size:Int()
		Return RSL_size(Self.Pointer)
	End Method
	
End Type

Rem
	bbdoc: Type for iterating through a TRenderSystemList.
ENDREM
Type TRenderSystemListIterator

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Get the render system we are currently on in this iterator.
	ENDREM
	Method getRenderSystem:TRenderSystem()
		Local Obj:TRenderSystem = New TRenderSystem
		Obj.Pointer = RSLI_getRenderSystem( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Substitutes as the equals operator. Ex. yourIterator.eq(anotherIterator) is the same as yourIterator = anotherIterator.
	ENDREM
	Method eq(a:TRenderSystemListIterator)
		RSLI_eq(a.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Substitutes as the equality operator. Ex.  If yourIterator.isEqualTo(anotherIterator) is the same as If youriterator = anotherIterator.
	ENDREM
	Method isEqualTo(a:TRenderSystemListIterator)
		RSLI_isEqualTo(a.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Substitutes as the non-equality operator. Ex.  If yourIterator.isNotEqualTo(anotherIterator) is the same as If youriterator <> anotherIterator.
	ENDREM
	Method isNotEqualTo(a:TRenderSystemListIterator)
		RSLI_isNotEqualTo(a.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Moves the iterator forward one item.
	ENDREM
	Method moveForward()
		RSLI_moveForward(Self.Pointer)
	End Method
	
End Type

Rem
	bbdoc: A list of strings. This type is not yet fully implemented.
ENDREM
Type TStringVector

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: An animable value. Used with animable objects. This type is not yet fully implemented.
ENDREM
Type TAnimableValue
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: A class for drawing lines quickly, for debugging or other purposes. If you need to draw lines for production use, you may need to use TRibbonTrails, TBillboardChains, or TManualObjects instead for greater flexibility. This class is not yet supported.
ENDREM
Type TQuickLine

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create a quickline in the given scene manager.
	ENDREM
	Function Create:TQuickLine( sm:TSceneManager )
		Local Obj:TQuickLine = New TQuickLine
		Obj.Pointer = QUICKLINE_create( sm.Pointer )
		Return Obj
	End Function
	
	Rem
		bbdoc: Add a point to the quickline in world coordinates.
	ENDREM
	Method addPoint( x:Float , y:Float , z:Float )
		QUICKLINE_addPoint( x , y , z , Self.Pointer )
	End Method
End Type

Rem
	bbdoc: Type used to describe any value. This type is not yet supported.
ENDREM
Type TAny

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: A movable object listener. This type is not yet supported.
ENDREM
Type TMovableObjectListener

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: NOT SUPPORTED : Support for TLightList is not in yet.
ENDREM
Type TLightList

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: A list of planes. This type is not yet supported.
ENDREM
Type TPlaneList

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: A set of integers.This type is not yet supported.
ENDREM
Type TSet_Int

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: A renderable iterator. This type is not yet fully supported.
ENDREM
Type TRenderableVisitor

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Scene manager factory implementation. This type is not yet supported.
ENDREM
Type TSceneManagerFactory

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr	
End Type

