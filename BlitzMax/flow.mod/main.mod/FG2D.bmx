REM
	bbdoc: A type for managing all 2D functions in fG. These functions will serve as analogs for Max2D style functions.	 Note: This interface is completely unfinished.
ENDREM
Type FG2D

	Function LoadImage:FG2DImage(fileName:String, flags:Int = -1)
	
		'RETURN NULL: If file does not exist
		If FileSize(fileName) = -1 Then
			Return Null
		End If
		
		'use TResourceGroupManager.declareResource to use the resoruce provided
		
		'Get resource directory
		Local resourceDir:String = ExtractDir(fileName)
		DebugLog(resourceDir)
		
		'Check to see if resource dir is already in use
			'if it is, don't load it again
			'if it isn't, register dir
			
		
			
		REM
		If (flags & MASKEDIMAGE) = MASKEDIMAGE Then
			'Image is masked, use mask colour.	
		End If
		
		If (flags & FILTEREDIMAGE) = FILTEREDIMAGE Then
			'Image uses quality sampling
		End If
		
		If (flags & MIPMAPPEDIMAGE) = MIPMAPPEDIMAGE Then
			'Image is mipmapped
		End If
		
		If (flags & DYNAMICIMAGE) = DYNAMICIMAGE Then
			'Image is in a dynamic buffer
		End If
		ENDREM
		
		Return Null
		
	End Function
	
	REM
		***** Text Functions *****
	ENDREM
	
	REM
		bbdoc:
	ENDREM
	Function DrawText(t:String, x:Int, y:Int)
		
	End Function
	
End Type

REM
	bbdoc:
ENDREM
Type FG2DImage
	
	Field isMasked:Byte;
	Field isMipMapped:Byte;
	Field isFiltered:Byte;
	Field fileName:String;
	Field resourceDir:String;
	
End Type

