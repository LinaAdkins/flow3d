REM
	bbdoc: The primary OgreMax class that governs when and how OgreMax dotscene files are loaded.
ENDREM
Type TOgreMaxScene

	REM
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	REM
		bbdoc: Creates a new TOgreMaxScene object in memory.
	ENDREM
	Function Create:TOgreMaxScene()
		Local Obj:TOgreMaxScene = New TOgreMaxScene
		Obj.Pointer = OGREMAXSCENE_create()
		Return Obj
	End Function
	
	REM
		bbdoc: Get the scene manage that this OgreMaxScene uses to create its objects.
	ENDREM
	Method GetSceneManager:TSceneManager()
		Local Obj:TSceneManager = New TSceneManager
		Obj.Pointer = OGREMAXSCENE_GetSceneManager(Self.Pointer)
		Return Obj
	End Method
	
	REM
		bbdoc: Specify a prefix to put in front of all names of the specified type.<br/>name:String - The prefix string to use.<br/>prefixes:Int - A flag specifying which types of scene objects will receive the prefix. Check TWhichNamePrefix for flag types.
	ENDREM
	Method SetNamePrefix(name:String, prefixes:Int)
		OGREMAXSCENE_SetNamePrefix(name.ToCString() , prefixes, Self.Pointer)
	End Method
	
	REM
		bbdoc: Load an OgreMax Scene with the given parameters.<br/>fileNameOrContent:String - The file name or path that the secene you are trying to load resides. Note that all of the scenes resources will need to be specified in the resource.cfg or loaded in code somewhere.<br/>renderWindow:TRenderWindow - The render window you're planning to render the scene with.<br/>loadOptions:Int = TSceneLoadOptions.NO_OPTIONS - Check the TSceneLoadOptions structure for more information.<br/>sceneManager:TSceneManager = Null - The scene manager you want to render your scene in. If you let this default then a scene manager will be created for you which you can get with .GetSceneManager().<br/>rootNode:TSceneNode = Null  - Sets the root scene node for all of the objects loaded into the scene. Default is the root scene node of the scene manager used.<br/>callback:TOgreMaxCallback = Null - Callback used for loading the scene. See fG_DotScene Loading.bmx for more details.<br/>defaultResourceGroupName:String - The resource group that all the resources in this scene will belong to. "General" is used by default.
	ENDREM
	Method Load(fileNameOrContent:String, renderWindow:TRenderWindow, loadOptions:Int = TSceneLoadOptions.NO_OPTIONS, sceneManager:TSceneManager = Null, rootNode:TSceneNode = Null, callback:TOgreMaxSceneCallback = Null, defaultResourceGroupName:String = DEFAULT_RESOURCE_GROUP_NAME)
	
		'Take care of the Null defaults, since BlitzMax can't handle custom type defaults.
		If sceneManager = Null Then
			sceneManager = New TSceneManager
			sceneManager.Pointer = Null
		End If
		
		If rootNode = Null Then
			rootNode = New TSceneNode
			rootNode.Pointer = Null
		End If
		
		If callback = Null Then
			callback = New TOgreMaxSceneCallback
			callback.Pointer = Null
		End If
		
		'Call the actual function to load the filename or content.
		OGREMAXSCENE_load(fileNameOrContent.ToCString() , renderWindow.Pointer, loadOptions, sceneManager.Pointer, rootNode.Pointer, callback.Pointer, defaultResourceGroupName.ToCString(), Self.Pointer)
	
	End Method
	
	REM
		bbdoc: Destroys and clears this TOgreMaxScene from memory.
	ENDREM
	Method Delete()
		OGREMAXSCENE_delete(Self.Pointer)
	End Method
	
	REM
		bbdoc: Helper to quickly create a TOgreMaxScene from a pointer.
	ENDREM
	Function FromPtr:TOgreMaxScene(Pointer:Byte Ptr)
		Local Obj:TOgreMaxScene = New TOgreMaxScene
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
End Type

REM
	bbdoc: Structure that holds flags for use with TOgreMaxScene.SetNamePrefix.
ENDREM
Type TWhichNamePrefix

	REM
		bbdoc: Specifies to only change prefixes for MovableObjects.
	ENDREM
	Const OBJECT_NAME_PREFIX:Int = 1
	
	REM
		bbdoc: Specifies to only change prefixes for Nodes.
	ENDREM
	Const NODE_NAME_PREFIX:Int = 2
	
	REM
		bbdoc: Specifies to only change prefixes for node animation names.
	ENDREM
	Const NODE_ANIMATION_NAME_PREFIX:Int = 4
	
	REM
		bbdoc: Specifies to change prefixes for all prefixable entities.
	ENDREM
	Const ALL_NAME_PREFIXES:Int = OBJECT_NAME_PREFIX | NODE_NAME_PREFIX | NODE_ANIMATION_NAME_PREFIX

End Type

REM
	bbdoc: Option constants for use in TOgreMaxScene.Load(...).
ENDREM
Type TSceneLoadOptions
	
	REM
		bbdoc: No special options, default loading scheme.
	ENDREM
	Const NO_OPTIONS:Int = 0
	
	REM
		bbdoc: Skips the environment settings. This will also skip shadow settings.
	ENDREM
	Const SKIP_ENVIRONMENT:Int = 1
	
	REM
		bbdoc: Skips the shadow settings in the file.
	ENDREM
	Const SKIP_SHADOWS:Int = 2
	
	REM
		bbdoc: Skip the sky settings.
	ENDREM
	Const SKIP_SKY:Int = 4
	
	REM
		bbdoc: Skip any nodes in the scene file.
	ENDREM
	Const SKIP_NODES:Int = 8
	
	REM
		bbdoc: Skip any externals in the scene file.
	ENDREM
	Const SKIP_EXTERNALS:Int = 16
	
	REM
		bbdoc: Skip loading the terrain specified in the scene file.
	ENDREM
	Const SKIP_TERRAIN:Int = 32
	
	REM
		bbdoc: Skip any octree setups in the specified scene file.
	ENDREM
	Const SKIP_OCTREE:Int = 64
	
	REM
		bbdoc: Skip all scene level lighting ( lighting not attached to a node ) in the scene file.
	ENDREM
	Const SKIP_SCENE_LIGHT:Int = 128
	
	REM
		bbdoc: Skip all scene level cameras ( cameras not attached to a node ) in the scene file.
	ENDREM
	Const SKIP_SCENE_CAMERA:Int = 256
	
	REM
		bbdoc: Skip all scene level query flag aliases in the scene file.
	ENDREM
	Const SKIP_QUERY_FLAG_ALIASES:Int = 512
	
	
	REM
		bbdoc: Skip all scene level visibility flags in the scene file.
	ENDREM
	Const SKIP_VISIBILITY_FLAG_ALIASES:Int = 1024
	
	REM
		bbdoc: Skip all scene level resource locations in the scene file.
	ENDREM
	Const SKIP_RESOURCE_LOCATIONS:Int = 2048
	
	REM
		bbdoc: Import no animation states from the scene file.
	ENDREM
	Const NO_ANIMATION_STATES:Int = 4096
	
	REM
		bbdoc: Store no externals automatically. CreateExternal() will still be called in the callbacks, however.
	ENDREM
	Const NO_EXTERNALS:Int = 8192
	
	REM
		bbdoc: Forego the check to make sure that the file specified on Load to make sure the file exists in Ogre's resource directories.
	ENDREM
	Const NO_FILE_SYSTEM_CHECK:Int = 16384
	
	REM
		bbdoc: Create a light if the loaded scene does not have one.
	ENDREM
	Const SET_DEFAULT_LIGHTING:Int = 32768
	
	REM
		bbdoc: Specify whether or not the filename given on Load is actual XML content, rather than a file name.
	ENDREM
	Const FILE_NAME_CONTAINS_CONTENT:Int = 65536
	
	
	
	
End Type

REM
	bbdoc: The callback structure used to inject and use the scene callback system in OgreMax.
ENDREM
Type TOgreMaxSceneCallback

	REM
		bbdoc: Pointer to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	REM
		bbdoc: Constructor
	ENDREM
	Function Create:TOgreMaxSceneCallback()
		Local s:TOgreMaxSceneCallback = New TOgreMaxSceneCallback
		s.Pointer = OGREMAXCALLBACK_create()
		Return s
	End Function
	
	REM
		bbdoc: Initialize all of your callback functions. To see how each of these functions work, check the sample 'fG_DotScene Loading.bmx'.
	ENDREM
	Method initialize(nodeFunctionPtr:Byte Ptr, camFunctionPtr:Byte Ptr, entityFunctionPtr:Byte Ptr, lightFunctionPtr:Byte Ptr, movableObjectFunctionPtr:Byte Ptr, progressFunctionPtr:Byte Ptr)
		OGREMAXCALLBACK_initialize(nodeFunctionPtr, camFunctionPtr, entityFunctionPtr, lightFunctionPtr, movableObjectFunctionPtr, progressFunctionPtr, Self.Pointer)
	End Method
	

	REM
		bbdoc: Destructor
	ENDREM
	Method Delete() 
		OGREMAXCALLBACK_delete(Self.Pointer)
	End Method
	
End Type
