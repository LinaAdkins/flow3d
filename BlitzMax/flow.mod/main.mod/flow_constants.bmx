'Global Constants--------------------------------------------'
'------------------------------------------------------------'
'Endian Settings
Const OGRE_ENDIAN_LITTLE:Int = 1
Const OGRE_ENDIAN_BIG:Int = 2
Const OGRE_ENDIAN:Int = OGRE_ENDIAN_LITTLE

'Shadow Technique constants'
Const SHADOWTYPE_NONE:Int	 = 0
Const SHADOWTYPE_STENCIL_ADDITIVE:Int = 17
Const SHADOWTYPE_STENCIL_MODULATIVE:Int = 18
Const SHADOWTYPE_TEXTURE_MODULATIVE:Int = 34
Const SHADOWTYPE_TEXTURE_ADDITIVE:Int = 33
Const SHADOWTYPE_TEXTURE_ADDITIVE_INTEGRATED:Int = 37
Const SHADOWTYPE_TEXTURE_MODULATIVE_INTEGRATED:Int = 38
'Transform Space Constants'
Const TS_LOCAL:Int = 0
Const TS_PARENT:Int= 1
Const TS_WORLD:Int = 2
'SceneType Constants'
Const ST_GENERIC:Int = 1
Const ST_EXTERIOR_CLOSE:Int = 2					 
Const ST_EXTERIOR_FAR:Int = 4
Const ST_EXTERIOR_REAL_FAR:Int = 8
Const ST_INTERIOR:Int = 16
'Logging Level Constants'
Const LL_LOW:Int = 1
Const LL_NORMAL:Int = 2
Const LL_BOREME:Int = 3
'Logging Message Level Constants'
Const LML_TRIVIAL:Int = 1
Const LML_NORMAL:Int = 2
Const LML_CRITICAL:Int = 3
'Resource Group Manager Constants'
Const DEFAULT_RESOURCE_GROUP_NAME:String = "General"
'Camera Polygon Mode Constants'
Const PM_POINTS:Int = 1
Const PM_WIREFRAME:Int = 2
Const PM_SOLID:Int = 3
'Render Queue Constants'
Const RENDER_QUEUE_BACKGROUND:Int = 0 
Const RENDER_QUEUE_SKIES_EARLY:Int = 5
Const RENDER_QUEUE_1:Int = 10
Const RENDER_QUEUE_2:Int = 20
Const RENDER_QUEUE_WORLD_GEOMETRY_1:Int = 25
Const RENDER_QUEUE_3:Int = 30
Const RENDER_QUEUE_4:Int = 40
Const RENDER_QUEUE_MAIN:Int = 50
Const RENDER_QUEUE_6:Int = 60
Const RENDER_QUEUE_7:Int = 70
Const RENDER_QUEUE_WORLD_GEOMETRY_2:Int = 75
Const RENDER_QUEUE_8:Int = 80
Const RENDER_QUEUE_9:Int = 90
Const RENDER_QUEUE_SKIES_LATE:Int = 95
Const RENDER_QUEUE_OVERLAY:Int = 100
'Ogre::FrameBufferType
Const FBT_COLOUR:Int = 1
Const FBT_DEPTH:Int = 2 	
Const FBT_STENCIL:Int = 4 
'Texture Type
Const TEX_TYPE_1D:Int = 1		
Const TEX_TYPE_2D:Int = 2			
Const TEX_TYPE_3D:Int = 3		 
Const TEX_TYPE_CUBE_MAP:Int = 4  	
'PixelFormat
Const PF_UNKNOWN:Int = 0
Const PF_L8:Int = 1
Const PF_BYTE_L:Int = PF_L8
Const PF_L16:Int = 2
Const PF_SHORT_L:Int = PF_L16
Const PF_A8:Int = 3 
Const PF_BYTE_A:Int = PF_A8 
Const PF_A4L4:Int = 4 
Const PF_BYTE_LA:Int = 5 
Const PF_R5G6B5:Int = 6 
Const PF_B5G6R5:Int = 7 
Const PF_R3G3B2:Int = 31 
Const PF_A4R4G4B4:Int = 8 
Const PF_A1R5G5B5:Int = 9 
Const PF_R8G8B8:Int = 10 
Const PF_B8G8R8:Int = 11 
Const PF_A8R8G8B8:Int = 12 
Const PF_A8B8G8R8:Int = 13 
Const PF_B8G8R8A8:Int = 14 
Const PF_R8G8B8A8:Int = 28 
Const PF_X8R8G8B8:Int = 26 
Const PF_X8B8G8R8:Int = 27 

If OGRE_ENDIAN = OGRE_ENDIAN_BIG Then
	Const PF_BYTE_RGB:Int = PF_R8G8B8 
	Const PF_BYTE_BGR:Int = PF_B8G8R8 
	Const PF_BYTE_BGRA:Int = PF_B8G8R8A8 
	Const PF_BYTE_RGBA:Int = PF_R8G8B8A8 
Else
	Const PF_BYTE_RGB:Int = PF_B8G8R8 
	Const PF_BYTE_BGR:Int = PF_R8G8B8 
	Const PF_BYTE_BGRA:Int = PF_A8R8G8B8 
	Const PF_BYTE_RGBA:Int = PF_A8B8G8R8 
EndIf

Const PF_A2R10G10B10:Int = 15 
Const PF_A2B10G10R10:Int = 16 
Const PF_DXT1:Int = 17 
Const PF_DXT2:Int = 18 
Const PF_DXT3:Int = 19 
Const PF_DXT4:Int = 20 
Const PF_DXT5:Int= 21 
Const PF_FLOAT16_R:Int = 32 
Const PF_FLOAT16_RGB:Int = 22 
Const PF_FLOAT16_RGBA:Int = 23 
Const PF_FLOAT32_R:Int = 33 
Const PF_FLOAT32_RGB:Int = 24 
Const PF_FLOAT32_RGBA:Int = 25 
Const PF_DEPTH:Int = 29 
Const PF_SHORT_RGBA:Int = 30 
Const PF_COUNT:Int = 34

'MipMap Limits
Const MIP_DEFAULT:Int = -1
Const MIP_UNLIMITED:Int = 2147483647

'Comparison functions for buffers and blending
Const CMPF_ALWAYS_FAIL:Int = 0
Const CMPF_ALWAYS_PASS:Int = 1
Const CMPF_LESS:Int = 2
Const CMPF_LESS_EQUAL:Int = 3
Const CMPF_EQUAL:Int = 4
Const CMPF_NOT_EQUAL:Int = 5
Const CMPF_GREATER_EQUAL:Int = 6
Const CMPF_GREATER:Int = 7

'Culling modes for triangle queries
Const CULL_NONE:Int = 1
Const CULL_CLOCKWISE:Int = 2
Const CULL_ANTICLOCKWISE:Int = 3
Const CULL_BACKFACES:Int = 4
Const CULL_FRONTFACES:Int = 5

'Manual culling modes for culling with CPU
Const MANUAL_CULL_NONE:Int = 1
Const MANUAL_CULL_BACK:Int = 2
Const MANUAL_CULL_FRONT:Int = 3

'Render Queue IDs

'FogMode
Const FOG_NONE:Int = 0
Const FOG_EXP:Int = 1
Const FOG_EXP2:Int = 2
Const FOG_LINEAR:Int = 3

'------------------------------------------------------------'
'------------------------------------------------------------'


