Rem----------------------------------------------------
OgreNewt_DLL.bmx

This file holds the OgreNewt DLL loading code and all of the function definitions contained
in the DLL. The DLL will be loaded when this file is included into a project.
Generally you should be loading OgreNewt_Classes.bmx, which will in turn load this file.

©2009 BoxSnap Studios

-------------------------------------------------------
ENDREM


'Summary: Function to load OgreNewt.dll. This is used to avoid global name collisions.
'Function LoadOgreNewt() 
	
	'Load the DLL-
	Local libname:String = "OgreNewt.dll"
	Local libhandle:Int = LoadLibraryA(libname) 

	
	'If loading is unsucessful display error and quit-
	If libhandle = 0 Then
		Notify("FAILED : Cannot locate " + libname + "!") 
		End
	EndIf
	
'End Function

' --- Begin Function Definitions ---


'TOgreNewtWorld
Global OGRENEWTWORLD_create:Byte Ptr() = GetProcAddress(libhandle, "OGRENEWTWORLD_create")
Global OGRENEWTWORLD_destroy(w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_destroy")
Global OGRENEWTWORLD_update(t_step:Float, w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_update")
Global OGRENEWTWORLD_invalidateCache(w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_invalidateCache")
Global OGRENEWTWORLD_getNewtonWorld:Byte Ptr(w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_getNewtonWorld")
Global OGRENEWTWORLD_getDefaultMaterialID:Byte Ptr(w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_getDefaultMaterialID")
Global OGRENEWTWORLD_destroyAllBodies(w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_destroyAllBodies")
Global OGRENEWTWORLD_setSolverModel(model:Int, w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_setSolverModel")
Global OGRENEWTWORLD_setFrictionModel(model:Int, w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_setFrictionModel")
Global OGRENEWTWORLD_setPlatformArchitecture(mode:Int, w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_setPlatformArchitecture")
Global OGRENEWTWORLD_getBodyCount:Int(w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_getBodyCount")
Global OGRENEWTWORLD_setMultithreadSolverOnSingleIsland(mode:Int, w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_setMultithreadSolverOnSingleIsland")
Global OGRENEWTWORLD_setThreadCount(threads:Int, w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_setThreadCount")
Global OGRENEWTWORLD_getThreadCount:Int(w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_getThreadCount")
Global OGRENEWTWORLD_criticalSectionLock(w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_criticalSectionLock")
Global OGRENEWTWORLD_criticalSectionUnlock(w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_criticalSectionUnlock")
Global OGRENEWTWORLD_setMinimumFrameRate(rate:Int, ogreNewtWorld:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_setMinimumFrameRate")
Global OGRENEWTWORLD_setWorldSize(minSize:Byte Ptr, maxSize:Byte Ptr, w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_setWorldSize")
Global OGRENEWTWORLD_setWorldSizeWithAAB(box:Byte Ptr, w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_setWorldSizeWithAAB")
Global OGRENEWTWORLD_getWorldSize:Byte Ptr(w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_getWorldSize")
Global OGRENEWTWORLD_getVersion:Float(ogreNewtWorld:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_getVersion")
Global OGRENEWTWORLD_setLeaveWorldCallback(callback:Byte Ptr, w:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTWORLD_setLeaveWorldCallback")

'TBasicRaycast
Global OGRENEWTBR_create:Byte Ptr( world:Byte Ptr , startpt:Byte Ptr , endpt:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTBR_create" )
Global OGRENEWTBR_delete( BasicRaycast:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTBR_delete" )
Global OGRENEWTBR_getFirstHit:Byte Ptr(BasicRayCast:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTBR_getFirstHit") 
Global OGRENEWTBR_getHitCount:Int( BasicRaycast:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTBR_getHitCount" )
Global OGRENEWTBR_getInfoAt:Byte Ptr(pos:Int, BasicRaycast:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTBR_getInfoAt") 

'TBasicRaycastInfo
Global OGRENEWTBRINFO_create:Byte Ptr() = GetProcAddress(libhandle, "OGRENEWTBRINFO_create") 
Global OGRENEWTBRINFO_delete(i:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTBRINFO_delete") 
Global OGRENEWTBRINFO_getDistance:Float(i:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTBRINFO_getDistance") 
Global OGRENEWTBRINFO_getBody:Byte Ptr(i:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTBRINFO_getBody") 
Global OGRENEWTBRINFO_getCollisionID:Int(i:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTBRINFO_getCollisionID") 
Global OGRENEWTBRINFO_getNormal:Byte Ptr(i:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTBRINFO_getNormal") 

'TOgreNewtDebugger
Global OGRENEWTDEBUGGER_init( smgr:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTDEBUGGER_init" )
Global OGRENEWTDEBUGGER_deInit() = GetProcAddress( libhandle , "OGRENEWTDEBUGGER_deInit" )
Global OGRENEWTDEBUGGER_showLines( world:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTDEBUGGER_showLines" )
Global OGRENEWTDEBUGGER_hideLines() = GetProcAddress( libhandle , "OGRENEWTDEBUGGER_hideLines" )

'TMaterialPair
Global OGRENEWTMP_create:Byte Ptr( world:Byte Ptr , mat1:Byte Ptr , mat2:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTMP_create" )
Global OGRENEWTMP_delete( mp:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTMP_delete" )
Global OGRENEWTMP_setDefaultSoftness( softness:Float , mp:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTMP_setDefaultSoftness" )
Global OGRENEWTMP_setDefaultElasticity( elasticity:Float , mp:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTMP_setDefaultElasticity" )
Global OGRENEWTMP_setDefaultCollidable( state:Int , mp:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTMP_setDefaultCollidable" )
Global OGRENEWTMP_setDefaultFriction( stat:Float , kinetic:Float , mp:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTMP_setDefaultFriction" )
Global OGRENEWTMP_setContinuousCollisionMode( state:Int , mp:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTMP_setContinuousCollisionMode" )
Global OGRENEWTMP_setContactCallback( callback:Byte Ptr , mp:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTMP_setContactCallback" )

'TMaterialID
Global OGRENEWTMID_create:Byte Ptr( world:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTMID_create" )
Global OGRENEWTMID_delete( ID:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTMID_delete" )
Global OGRENEWTMID_getID:Int( ID:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTMID_getID" )

'TCollision
Global OGRENEWTCOL_createBox:Byte Ptr( world:Byte Ptr , size:Byte Ptr , orient:Byte Ptr , pos:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTCOL_createBox")
Global OGRENEWTCOL_createConvexHull:Byte Ptr( world:Byte Ptr , sn:Byte Ptr , x:Float , y:Float , z:Float ) = GetProcAddress( libhandle , "OGRENEWTCOL_createConvexHull" )
Global OGRENEWTCOL_createEllipsoid:Byte Ptr( world:Byte Ptr , size:Byte Ptr ,orient:Byte Ptr , pos:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTCOL_createEllipsoid" )
Global OGRENEWTCOL_createCylinder:Byte Ptr( world:Byte Ptr ,  radius:Float , height:Float , orient:Byte Ptr , pos:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTCOL_createCylinder" )
Global OGRENEWTCOL_destroy( greNewtCollision:Byte Ptr ) = GetProcAddress(libhandle , "OGRENEWTCOL_destroy")
Global OGRENEWTCOL_calculateInertialMatrix( inertia:Byte Ptr , offset:Byte Ptr , c:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTCOL_calculateInertialMatrix")

'TTreeCollision
Global OGRENEWTTC_create:Byte Ptr( world:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTTC_create" )
Global OGRENEWTTC_createWithSceneNode:Byte Ptr( world:Byte Ptr , sn:Byte Ptr , optimize:Int , fw:Int ) = GetProcAddress( libhandle , "OGRENEWTTC_createWithSceneNode" )
Global OGRENEWTTC_start( tc:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTTC_start" )
Global OGRENEWTTC_addPoly( polys:Byte Ptr , ID:Int , tc:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTTC_addPoly" )
Global OGRENEWTTC_finish( tc:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTTC_finish" )

'TBody
Global BODY_create:Byte Ptr( W:Byte Ptr , col:Byte Ptr , bodytype:Int ) = GetProcAddress( libhandle , "BODY_create" )
Global BODY_destroy(b:Byte Ptr) = GetProcAddress(libhandle, "BODY_destroy") 
Global BODY_setUserData(data:Object, b:Byte Ptr) = GetProcAddress(libhandle, "BODY_setUserData") 
Global BODY_getUserData:Object(b:Byte Ptr) = GetProcAddress(libhandle, "BODY_getUserData") 
Global BODY_hasUserData:Byte(b:Byte Ptr) = GetProcAddress(libhandle, "BODY_hasUserData") 
Global BODY_clearUserData(b:Byte Ptr) = GetProcAddress(libhandle, "BODY_clearUserData") 
Global BODY_getOgreNode:Byte Ptr(b:Byte Ptr) = GetProcAddress(libhandle, "BODY_getOgreNode") 
Global BODY_getWorld:Byte Ptr(b:Byte Ptr) = GetProcAddress(libhandle, "BODY_getWorld") 
Global BODY_setType(bodytype:Int, b:Byte Ptr) = GetProcAddress(libhandle, "BODY_setType") 
Global BODY_getType:Int(b:Byte Ptr) = GetProcAddress(libhandle, "BODY_getType") 
Global BODY_attachNode(node:Byte Ptr, b:Byte Ptr) = GetProcAddress(libhandle, "BODY_attachNode")
Global BODY_setStandardForceCallback(b:Byte Ptr) = GetProcAddress(libhandle, "BODY_setStandardForceCallBack") 
Global BODY_setCustomForceAndTorqueCallback(callback:Byte Ptr, b:Byte Ptr) = GetProcAddress(libhandle, "BODY_setCustomForceAndTorqueCallback") 
Global BODY_removeForceAndTorqueCallback(b:Byte Ptr) = GetProcAddress(libhandle, "BODY_removeForceAndTorqueCallback") 
Global BODY_setCustomTransformCallback(callback:Byte Ptr, b:Byte Ptr) = GetProcAddress(libhandle, "BODY_setCustomTransformCallback") 
Global BODY_removeTransformCallback(b:Byte Ptr) = GetProcAddress(libhandle, "BODY_removeTransformCallback") 
Global BODY_setPositionOrientation(pos:Byte Ptr, orient:Byte Ptr, b:Byte Ptr) = GetProcAddress(libhandle, "BODY_setPositionOrientation")
Global BODY_setMassMatrix(mass:Float, inertia:Byte Ptr, b:Byte Ptr) = GetProcAddress(libhandle, "BODY_setMassMatrix") 
Global BODY_setCenterOfMass( centerOfMass:Byte Ptr , b:Byte Ptr) = GetProcAddress( libhandle , "BODY_setCenterOfMass" )
Global BODY_setContinuousCollisionMode(state:Int, b:Byte Ptr) = GetProcAddress(libhandle, "BODY_setContinuousCollisionMode")
Global BODY_setJointRecursiveCollision(state:Int, b:Byte Ptr) = GetProcAddress(libhandle, "BODY_setJointRecursiveCollision")
Global BODY_setAutoSleep(flag:Int, b:Byte Ptr) = GetProcAddress(libhandle, "BODY_setAutoSleep")
Global BODY_getAutoSleep:Int(b:Byte Ptr) = GetProcAddress(libhandle, "BODY_getAutoSleep")
Global BODY_setSleepState(state:Int, b:Byte Ptr) = GetProcAddress(libhandle, "BODY_setSleepState")
Global BODY_getSleepState:Int(b:Byte Ptr) = GetProcAddress(libhandle, "BODY_getSleepState")
Global BODY_addForce(force:Byte Ptr, ogreNewtBody:Byte Ptr) = GetProcAddress(libhandle, "BODY_addForce")
Global BODY_addLocalForce(force:Byte Ptr, pos:Byte Ptr, b:Byte Ptr) = GetProcAddress(libhandle, "BODY_addLocalForce")
Global BODY_addTorque(torque:Byte Ptr, ogreNewtBody:Byte Ptr) = GetProcAddress(libhandle, "BODY_addTorque")
Global BODY_setTorque(torque:Byte Ptr, b:Byte Ptr) = GetProcAddress(libhandle, "BODY_setTorque")
Global BODY_getTorque:Byte Ptr( b:Byte Ptr ) = GetProcAddress( libhandle , "BODY_getTorque" )
Global BODY_setVelocity( vel:Byte Ptr , ogreNewtBody:Byte Ptr ) = GetProcAddress( libhandle , "BODY_setVelocity" )
Global BODY_getMassMatrix(mass:Float, inertia:Byte Ptr, ogreNewtBody:Byte Ptr) = GetProcAddress(libhandle, "BODY_getMassMatrix") 
Global BODY_setOmega( omega:Byte Ptr , ogreNewtBody:Byte Ptr ) = GetProcAddress( libhandle , "BODY_setOmega" )
Global BODY_getOmega:Byte Ptr(b:Byte Ptr) = GetProcAddress(libhandle, "BODY_getOmega") 
Global BODY_getPositionOrientation( pos:Byte Ptr , orientation:Byte Ptr , ogreNewtBody:Byte Ptr ) = GetProcAddress( libhandle , "BODY_getPositionOrientation" )
Global BODY_getVelocity:Byte Ptr(body:Byte Ptr) = GetProcAddress(libhandle, "BODY_getVelocity")
Global BODY_getMaterialGroupID:Byte Ptr(body:Byte Ptr) = GetProcAddress(libhandle, "BODY_getMaterialGroupID") 
Global BODY_setMaterialGroupID(ID:Byte Ptr, body:Byte Ptr) = GetProcAddress(libhandle, "BODY_setMaterialGroupID")
Global BODY_addImpulse(deltav:Byte Ptr, posit:Byte Ptr, b:Byte Ptr) = GetProcAddress(libhandle, "BODY_addImpulse") 
Global BODY_getPosition:Byte Ptr(b:Byte Ptr) = GetProcAddress(libhandle, "BODY_getPosition") 
Global BODY_calculateGravity:Byte Ptr( b:Byte Ptr ) = GetProcAddress( libhandle , "BODY_calculateGravity" )
Global BODY_addBuoyancyForce(fluidDensity:Float, fluidLinearViscosity:Float, fluidAngularViscosity:Float, gravity:Byte Ptr, callback:Byte Ptr, b:Byte Ptr) = GetProcAddress(libhandle, "BODY_addBuoyancyForce")

'TContactCallback
Global CONTACTCALLBACK_create:Byte Ptr(userBeginMaxFunc:Byte Ptr, userProcessMaxFunc:Byte Ptr) = GetProcAddress(libhandle, "CONTACTCALLBACK_create")
Global CONTACTCALLBACK_delete(c:Byte Ptr) = GetProcAddress(libhandle, "CONTACTCALLBACK_delete")
Global CONTACTCALLBACK_getBody0:Byte Ptr(c:Byte Ptr) = GetProcAddress(libhandle, "CONTACTCALLBACK_getBody0") 
Global CONTACTCALLBACK_getBody1:Byte Ptr(c:Byte Ptr) = GetProcAddress(libhandle, "CONTACTCALLBACK_getBody1") 

'TContactIterator
Global CI_getCurrent:Byte Ptr(c:Byte Ptr) = GetProcAddress(libhandle, "CI_getCurrent")
Global CI_moveNext:Byte(c:Byte Ptr) = GetProcAddress(libhandle, "CI_moveNext")
Global CI_reset(c:Byte Ptr) = GetProcAddress(libhandle, "CI_reset")
Global CI_hasMoreElements:Byte(c:Byte Ptr) = GetProcAddress(libhandle, "CI_hasMoreElements")
Global CI_getContactCount:Int(c:Byte Ptr) = GetProcAddress(libhandle, "CI_getContactCount")

'TContact
Global CONTACT_removeContact(c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_removeContact")
Global CONTACT_getContactFaceAttribute:Int(c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_getContactFaceAttribute")
Global CONTACT_getBodyCollisionID:Int(body:Byte Ptr, c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_getBodyCollisionID")
Global CONTACT_getContactNormalSpeed:Float(c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_getContactNormalSpeed")
Global CONTACT_getContactForce:Byte Ptr(c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_getContactForce")
Global CONTACT_getContactPositionAndNormal(pos:Byte Ptr, norm:Byte Ptr, c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_getContactPositionAndNormal")
Global CONTACT_getContactTangentDirections(dir0:Byte Ptr, dir1:Byte Ptr, c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_getContactTangentDirections")
Global CONTACT_getContactTangentSpeed:Float(index:Int, c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_getContactTangentSpeed")
Global CONTACT_setContactSoftness(softness:Float, c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_setContactSoftness")
Global CONTACT_setContactElasticity(elasticity:Float, c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_setContactElasticity")
Global CONTACT_setContactFrictionState(state:Int, index:Int, c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_setContactFrictionState")
Global CONTACT_setContactFrictionCoef(stat:Float, kinetic:Float, index:Int, c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_setContactFrictionCoef")
Global CONTACT_setContactTangentAcceleration(accel:Float, index:Int, c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_setContactTangentAcceleration")
Global CONTACT_rotateTangentDirections(dir:Byte Ptr, c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_rotateTangentDirections")
Global CONTACT_setContactNormalDirection(dir:Byte Ptr, c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_setContactNormalDirection")
Global CONTACT_setContactNormalAcceleration(accel:Float, c:Byte Ptr) = GetProcAddress(libhandle, "CONTACT_setContactNormalAcceleration")

'TJoint
Global OGRENEWTJOINT_create:Byte Ptr() = GetProcAddress(libhandle, "OGRENEWTJOINT_create") 
Global OGRENEWTJOINT_delete(j:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTJOINT_delete") 
Global OGRENEWTJOINT_getCollisionState:Int(j:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTJOINT_getCollisionState") 
Global OGRENEWTJOINT_setCollisionState(State:Int, j:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTJOINT_setCollisionState") 
Global OGRENEWTJOINT_getStiffness:Float(j:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTJOINT_getStiffness") 
Global OGRENEWTJOINT_setStiffness(stiffness:Float, j:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTJOINT_setStiffness") 
Global OGRENEWTJOINT_setUserData(userObject:Object, j:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTJOINT_setUserData") 
Global OGRENEWTJOINT_getUserData:Object(j:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTJOINT_getUserData") 
Global OGRENEWTJOINT_clearUserData(j:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTJOINT_clearUserData") 
Global OGRENEWTJOINT_hasUserData:Byte(j:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTJOINT_hasUserData") 

'TBallAndSocket
Global OGRENEWTBAS_create:Byte Ptr(world:Byte Ptr, child:Byte Ptr, parent:Byte Ptr, Pos:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTBAS_create") 
Global OGRENEWTBAS_getJointAngle:Byte Ptr(b:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTBAS_getJointAngle") 
Global OGRENEWTBAS_getJointOmega:Byte Ptr(b:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTBAS_getJointAngle") 
Global OGRENEWTBAS_getJointForce:Byte Ptr(b:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTBAS_getJointAngle") 
Global OGRENEWTBAS_setLimits( pin:Byte Ptr , maxCone:Byte Ptr , maxTwist:Byte Ptr , b:Byte Ptr ) = GetProcAddress ( libhandle , "OGRENEWTBAS_setLimits" )

'THingeJoint
Global OGRENEWTHINGE_create:Byte Ptr(world:Byte Ptr, child:Byte Ptr, parent:Byte Ptr, Pos:Byte Ptr, pin:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTHINGE_create") 
Global OGRENEWTHINGE_getJointAngle:Byte Ptr(h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTHINGE_getJointAngle") 
Global OGRENEWTHINGE_getJointOmega:Float(h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTHINGE_getJointOmega") 
Global OGRENEWTHINGE_getJointForce:Byte Ptr(h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTHINGE_getJointForce") 
Global OGRENEWTHINGE_setCallback(HingeCallback:Byte Ptr, h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTHINGE_setCallback") 
Global OGRENEWTHINGE_setCallbackAccel(accel:Float, h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTHINGE_setCallbackAccel") 
Global OGRENEWTHINGE_setCallbackFrictionMin(minValue:Float, h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTHINGE_setCallbackFrictionMin") 
Global OGRENEWTHINGE_setCallbackFrictionMax(maxValue:Float, h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTHINGE_setCallbackFrictionMax") 
Global OGRENEWTHINGE_getCallbackTimestep:Float(h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTHINGE_getCallbackTimestep") 
Global OGRENEWTHINGE_calculateStopAlpha:Float(angle:Byte Ptr, h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTHINGE_calculateStopAlpha") 

'TSliderJoint
Global OGRENEWTSLIDER_create:Byte Ptr(world:Byte Ptr, child:Byte Ptr, parent:Byte Ptr, Pos:Byte Ptr, pin:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTSLIDER_create") 
Global OGRENEWTSLIDER_getJointPosit:Float(h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTSLIDER_getJointPosit") 
Global OGRENEWTSLIDER_getJointVeloc:Float(h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTSLIDER_getJointVeloc") 
Global OGRENEWTSLIDER_getJointForce:Byte Ptr(h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTSLIDER_getJointForce") 
Global OGRENEWTSLIDER_setCallback(HingeCallback:Byte Ptr, h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTSLIDER_setCallback") 
Global OGRENEWTSLIDER_setCallbackAccel(accel:Float, h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTSLIDER_setCallbackAccel") 
Global OGRENEWTSLIDER_setCallbackFrictionMin(minValue:Float, h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTSLIDER_setCallbackFrictionMin") 
Global OGRENEWTSLIDER_setCallbackFrictionMax(maxValue:Float, h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTSLIDER_setCallbackFrictionMax") 
Global OGRENEWTSLIDER_getCallbackTimestep:Float(h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTSLIDER_getCallbackTimestep") 
Global OGRENEWTSLIDER_calculateStopAccel:Float(dist:Float, h:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTSLIDER_calculateStopAccel") 
 
'TUniversalJoint
Global OGRENEWTUNIVERSAL_create:Byte Ptr(world:Byte Ptr, child:Byte Ptr, parent:Byte Ptr, Pos:Byte Ptr, pin0:Byte Ptr, pin1:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTUNIVERSAL_create") 
Global OGRENEWTUNIVERSAL_getJointAngle0:Byte Ptr(u:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTUNIVERSAL_getJointAngle0") 
Global OGRENEWTUNIVERSAL_getJointAngle1:Byte Ptr(u:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTUNIVERSAL_getJointAngle1") 
Global OGRENEWTUNIVERSAL_getJointOmega0:Float(u:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTUNIVERSAL_getJointOmega0") 
Global OGRENEWTUNIVERSAL_getJointOmega1:Float(u:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTUNIVERSAL_getJointOmega1") 
Global OGRENEWTUNIVERSAL_getJointForce:Byte Ptr(u:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTUNIVERSAL_getJointForce") 
Global OGRENEWTUNIVERSAL_setCallback(me:Byte Ptr, u:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTUNIVERSAL_setCallback") 
Global OGRENEWTUNIVERSAL_setCallbackAccel(accel:Float, axis:Int, u:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTUNIVERSAL_setCallbackAccel") 
Global OGRENEWTUNIVERSAL_setCallbackFrictionMin(minValue:Float, axis:Int, u:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTUNIVERSAL_setCallbackFrictionMin") 
Global OGRENEWTUNIVERSAL_setCallbackFrictionMax(maxValue:Float, axis:Int, u:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTUNIVERSAL_setCallbackFrictionMax") 
Global OGRENEWTUNIVERSAL_getCallbackTimestep:Float(u:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTUNIVERSAL_getCallbackTimestep") 
Global OGRENEWTUNIVERSAL_calculateStopAlpha0:Float(angle:Float, u:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTUNIVERSAL_calculateStopAlpha0") 
Global OGRENEWTUNIVERSAL_calculateStopAlpha1:Float(angle:Float, u:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTUNIVERSAL_calculateStopAlpha1") 

'TUpVectorJoint
Global OGRENEWTUV_create:Byte Ptr(world:Byte Ptr, body:Byte Ptr, pin:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTUV_create") 
Global OGRENEWTUV_setPin(pin:Byte Ptr, u:Byte Ptr) = GetProcAddress(libhandle, "OGRENEWTUV_setPin") 
Global OGRENEWTUV_getPin:Byte Ptr( u:Byte Ptr ) = GetProcAddress( libhandle , "OGRENEWTUV_getPin" )




