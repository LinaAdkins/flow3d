Rem----------------------------------------------------
OgreNewt_Classes.bmx

This file holds the OgreNewt class definitions that wrap the DLL functions into Blitz OOP. This file
will also load the OgreNewt_DLL.bmx that is required and load the OgreNewt.dll on inclusion.

�2009 BoxSnap Studios

ENDREM-------------------------------------------------

'Include the dll functions and load the OgreNewt.dll
Include "OgreNewt_DLL.bmx"



Rem
	bbdoc: This object describes the physics world as a whole and contains methods to manipulate that world. ( eg. Change it's size, change it's friction, update, etc. ).
ENDREM
Type TOgreNewtWorld
		
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Default is TRUE. Flag to specify whether bodies and joints present in the world are destroyed on world destruction. If you do NOT want your bodies and joints cleaned up for you on world destruction set this to False.
	ENDREM
	Field autoCleanupBodies:Byte
	
	Rem
		bbdoc: Specifies whether this instance of TOgreNewtWorld is managed. All instances returned from functions like TBody.getWorld should consider their instance returned a managed instance.
	ENDREM
	Field managed:Byte
			
	Rem
		bbdoc: Adaptive Friction Model.	Generally this is the fastest friction configuration.
	ENDREM
	Const FM_ADAPTIVE:Int = 1
	
	Rem
		bbdoc: Exact Friction Model.
	ENDREM
	Const FM_EXACT:Int = 0
	
	Rem
		bbdoc: Adaptive Solver Model. Generally this is the fastest configuration for the solver.
	ENDREM
	Const SM_ADAPTIVE:Int = 1
	
	Rem
		bbdoc: Exact Solver Model.
	ENDREM
	Const SM_EXACT:Int = 0
	
	Rem
		bbdoc: Override New() so we can set autoCleanupBodies and managed.
	ENDREM
	Method New()
		Self.autoCleanupBodies = True
		Self.managed = False
	End Method
	
	Rem
		bbdoc: Constructor.
	ENDREM
	Function Create:TOgreNewtWorld() 
		Local Obj:TOgreNewtWorld = New TOgreNewtWorld
		Obj.Pointer = OGRENEWTWORLD_create()
		Return Obj
	End Function
	
	Rem
		bbdoc: Destroy the physics world. Used for manually destroying the physics world instead of waiting for the destructor. Once you use this the reference to the object will be null and you will not be able to call any methods.<br/>Note that .Destroy() does not care about managed instances.
	ENDREM
	Method Destroy()	
		
		'If our reference is valid, then destroy!
		If Self.Pointer <> Null Then
		
			'Destroy all bodies and joints on destroy() if autoCleanupBodies is true.
			If Self.autoCleanupBodies = True Then
				Self.destroyAllBodies()
			End If
			
			'Destroy the world and set our pointer to Null
			OGRENEWTWORLD_destroy(Self.Pointer)
			Self.Pointer = Null
			
		End If
	End Method
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
	
		'RETURN: If this is a managed instance then don't let garbage collection kill the world.
		If Self.managed = True Then
			Return
		End If
	
		'If our reference is valid, then destroy!
		If Self.Pointer <> Null Then	
	
			'Destroy all bodies and joints on destroy() if autoCleanupBodies is true.
			If Self.autoCleanupBodies = True Then
				Self.destroyAllBodies()
			End If
			
			'Destroy the world and set our pointer to Null
			OGRENEWTWORLD_destroy(Self.Pointer)
			Self.Pointer = Null
		End If
		
	End Method
	
	
	Rem
		bbdoc: Update the physics world by the specified time slice ( in seconds ). If you wish to have deterministic results ( same every time ), then you MUST avoid changing the timeslice as the simulation is running.<br>Some example values would be: 1/60 - Update the physics 1/60th of a second. 1/120 - Update the physics 1/120th of a second.<br>Range is 1/60-1/600 , anything higher or lower will be clipped to these values.
	ENDREM
	Method update( t_step:Float )
		OGRENEWTWORLD_update( t_step , Self.Pointer )
	End Method
	
	REM
		bbdoc: Reset the performance caching used by Newton. When you call this, the bodies in this world will not be affected by the simulation forces in the last frame. This is useful if you need to reset positions or place entites again after world creation.
	ENDREM
	Method invalidateCache()
		OGRENEWTWORLD_invalidateCache(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get a pointer to the Newton world in memory. You can use this pointer in your own newton implementation if you have one.
	ENDREM
	Method getNewtonWorld:Byte Ptr()
		Return OGRENEWTWORLD_getNewtonWorld(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the default material that is applied to all bodies in this world when they are created.
	ENDREM
	Method getDefaultMaterialID:TMaterialID()
		Local Obj:TMaterialID = New TMaterialID
		Obj.Pointer = OGRENEWTWORLD_getDefaultMaterialID(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Destroy all bodies and joints in this world. You won't have to free them manually when you use this.
	ENDREM
	Method destroyAllBodies()
		OGRENEWTWORLD_destroyAllBodies( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the solver model to either TOgreNewtWorld.SM_EXACT or TOgreNewtWorld.SM_ADAPTIVE. SM_ADAPTIVE is generally considered the fastest configuration. You may also use a >1 integer to specify a number of linear passes, each pass reducing the error margin somewhat. (2-n) 
	ENDREM
	Method setSolverModel(model:Int) 
		OGRENEWTWORLD_setSolverModel( model , Self.Pointer )
	End Method
	
   	Rem
		bbdoc: Set the friction model to either TOgreNewtWorld.FM_ADAPTIVE or TOgreNewtWorld.FM_EXACT. FM_ADAPTIVE is a bit faster but may cause odd behavior sometimes because it is not aware of friction continuity.
	ENDREM
	Method setFrictionModel( model:Int )
		OGRENEWTWORLD_setFrictionModel( model , Self.Pointer )
	End Method
	
	Rem
		bbdoc: This function allows the application to configure Newton to take advantage for specific hardware architecture in the same platform.	<br>0 Force the hardware lower common denominator for the running platform. This is the slowest mode.<br>1 Will try to use common floating point enhancement like special instruction sets on the specific architecture. This mode may lead to results that differ from mode 0 and 2 as the accumulation round off errors maybe different.<br>2 The engine will try to use the best possible hardware setting found in the current platform architecture. This is the default configuration. This mode made lead to results that differ from mode 0 and 1 as the accumulation round off errors maybe different.
	ENDREM
	Method setPlatformArchitecture( mode:Int )
		OGRENEWTWORLD_setPlatformArchitecture( mode , Self.Pointer )
	End Method
	
	REM
		bbdoc: Get the number of bodies present in this world.
	ENDREM
	Method getBodyCount:Int()
		Return OGRENEWTWORLD_getBodyCount(Self.Pointer)
	End Method
	
	REM
		bbdoc: Set the number of threads to perform on a single physics/collision island.
	ENDREM
	Method setMultithreadSolverOnSingleIsland(mode:Int)
		OGRENEWTWORLD_setMultithreadSolverOnSingleIsland(mode, Self.Pointer)
	End Method
	
	REM
		bbdoc: Set the number of threads to use in this world's physics simulation.
	ENDREM
	Method setThreadCount(threads:Int)
		OGRENEWTWORLD_setThreadCount(threads, Self.Pointer)
	End Method
	
	REM
		bbdoc: Get the number of threads to use in this world's physics simulation.
	ENDREM
	Method getThreadCount:Int()
		Return OGRENEWTWORLD_getThreadCount(Self.Pointer)
	End Method
	
	REM
		bbdoc: Lock memory during critical sections of the physics simulation. Generally you will use this during a callback to lock memory to modify body attributes or delete contacts, etc.
	ENDREM
	Method criticalSectionLock()
		OGRENEWTWORLD_criticalSectionLock(Self.Pointer)
	End Method
	
	REM
		bbdoc: Unlock a critical section after calling .criticalSectionLock(). Generally you will use this during a callback to lock memory to modify body attributes or delete contacts, etc.
	ENDREM
	Method criticalSectionUnlock()
		OGRENEWTWORLD_criticalSectionUnlock(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the minimum framerate for this physics world.
	ENDREM
	Method setMinimumFrameRate(rate:Float) 
		OGRENEWTWORLD_setMinimumFrameRate( rate , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the world size by defining two points in Ogre units from which a box will be created.
	ENDREM
	Method setWorldSize( minsize:TVector3 , maxsize:TVector3 )
		OGRENEWTWORLD_setWorldSize( minsize.Pointer , maxsize.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the world size by specifying an Axis Aligned Bounding Box.
	ENDREM
	Method setWorldSizeWithAAB(box:TAxisAlignedBox)
		OGRENEWTWORLD_setWorldSizeWithAAB(box.Pointer, Self.Pointer)
	End Method
	
	REM
		bbdoc: Get the physics world size as an axis aligned box.
	ENDREM
	Method getWorldSize:TAxisAlignedBox()
		Local Obj:TAxisAlignedBox = New TAxisAlignedBox
		Obj.Pointer = OGRENEWTWORLD_getWorldSize(Self.Pointer)
		Obj.Managed = False
		Return Obj
	End Method
	
	REM
		bbdoc: Get the newton SDK version!
	ENDREM
	Method getVersion:Float()
		Return OGRENEWTWORLD_getVersion( Self.Pointer )
	End Method
	
	REM
		bbdoc: Set the leave world callback function which will be called for every physics object that leaves the world bounds. The general layout of the function is:<br/>Function LeaveWorldCallback( body:Byte Ptr , threadIndex:Int )<br/>If you need more details, check the FlowPhysics_WorldLeaveCallbacks.bmx file.
	ENDREM
	Method setLeaveWorldCallback( callback:Byte Ptr )
		OGRENEWTWORLD_setLeaveWorldCallback(callback, Self.Pointer)
	End Method
	
End Type

Rem
	bbdoc: This interface allows you to debug rigid bodies visually. This class has a singleton interface and should not be instanced for any reason.
ENDREM
Type TOgreNewtDebugger
	
	Rem
		bbdoc: This function initializes the debugger for use. You must pass the specified scene manager where you wish for debug lines to appear.
	ENDREM
	Function Init(smgr:TSceneManager) 
		OGRENEWTDEBUGGER_init( smgr.Pointer )
	End Function
	
	Rem
		bbdoc: De-initialize the debugger and cleanup temporary objects.
	ENDREM
	Function deInit()
		OGRENEWTDEBUGGER_deInit()
	End Function
	
	Rem
		bbdoc: Show debug lines for a given Newton world. Note that this function will only draw the lines once, and will have to be called every time the physics world is updated to be current.<br>world:TOgreNewtWorld The Newton world that you are debugging.
	ENDREM
	Function showLines(world:TOgreNewtWorld) 
		OGRENEWTDEBUGGER_showLines( world.Pointer )
	End Function
	
	Rem
		bbdoc: Remove the lines being drawn currently.
	ENDREM
	Function hideLines()
		OGRENEWTDEBUGGER_hideLines()
	End Function
End Type

Rem
	bbdoc: Base interface for using standard Newton raycasts. Should not be used directly. You should subclass the TRaycast class for specific raycast behavior, or use the TBasicRaycast subclass.
ENDREM
Type TRaycast

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
End Type

Rem
	bbdoc: Interface for using simple Newton raycasts. Any bodies inside the currrent world will be used. 
ENDREM
Type TBasicRaycast Extends TRaycast
	
	Rem
		bbdoc: This is the constructor which also executes the raycast.
	ENDREM
	Function Create:TBasicRaycast(world:TOgreNewtWorld, startpt:TVector3, endpt:TVector3)
		Local Obj:TBasicRaycast = New TBasicRaycast
		Obj.Pointer = OGRENEWTBR_create( world.Pointer , startpt.Pointer , endpt.Pointer )
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor
	ENDREM
	Method Delete()
		OGRENEWTBR_delete( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get a TBasicRaycastInfo object that describes the first ray cast intersection.
	ENDREM
	Method getFirstHit:TBasicRaycastInfo()
		Local Obj:TBasicRaycastInfo = New TBasicRaycastInfo
		Obj.Pointer = OGRENEWTBR_getFirstHit(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the number of hits the ray cast had. This is useful for iterating through all the hits with getInfoAt(...).
	ENDREM
	Method getHitCount:Int()
		Return OGRENEWTBR_getHitCount( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get a TBasicRaycastInfo object that describes the given intersection along the ray.<br>pos:Int The specific hit you want information on. Starts at 0.
	ENDREM
	Method getInfoAt:TBasicRaycastInfo(Pos:Int) 
		Local Obj:TBasicRaycastInfo = New TBasicRaycastInfo
		Obj.Pointer = OGRENEWTBR_getInfoAt( pos , Self.Pointer ) 
		Return Obj
	End Method
End Type

Rem
	bbdoc: A class to hold basic raycast result information.
ENDREM
Type TBasicRaycastInfo

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr

	Rem
		bbdoc: Plain constructor, you probably will not need to use this often, if ever.
	ENDREM
	Function Create:TBasicRaycastInfo() 
		Local Obj:TBasicRaycastInfo = New TBasicRaycastInfo
		Obj.Pointer = OGRENEWTBRINFO_create() 
		Return Obj
	End Function
	
	Rem
		bbdoc: Destuctor
	ENDREM
	Method Delete() 
		OGRENEWTBRINFO_delete(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the distance to the ray intersection for this TBasicRaycastInfo hit.
	ENDREM
	Method getDistance:Float() 
		Return OGRENEWTBRINFO_getDistance(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the body that was collided with for this TBasicRaycastInfo hit.
	ENDREM
	Method getBody:TBody() 
		Local Obj:TBody = New TBody
		Obj.Pointer = OGRENEWTBRINFO_getBody(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the Collision ID of the primitive hit. This is mostly for compound collision objects.
	ENDREM
	Method getCollisionID:Int() 
		Return OGRENEWTBRINFO_getCollisionID(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the normal of the hit's intersection.
	ENDREM
	Method getNormal:TVector3() 
		Return TVector3.FromPtr(OGRENEWTBRINFO_getNormal(Self.Pointer)) 
	End Method
	
	
End Type

Rem
	bbdoc: A class for representing and using different collision shapes.
ENDREM
Type TCollision

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Create a box-shaped collision object.
	ENDREM
	Function createBox:TCollision(World:TOgreNewtWorld, Size:TVector3, orient:TQuaternion, Pos:TVector3) 
		Local obj:TCollision = New TCollision
		obj.Pointer = OGRENEWTCOL_createBox(World.Pointer, Size.Pointer, orient.Pointer, Pos.Pointer) 
		Return obj
	End Function
	
	Rem
		bbdoc: Create a convex hull that will wrap around the given scene-node's attached entities.
	ENDREM
	Function createConvexHull:TCollision( world:TOgreNewtWorld , sn:TSceneNode , x:Float , y:Float , z:Float )
		Local Obj:TCollision = New TCollision
		Obj.Pointer = OGRENEWTCOL_createConvexHull( world.Pointer , sn.Pointer , x , y , z )
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a cylinder-shaped collision.
	ENDREM
	Function createCylinder:TCollision( world:TOgreNewtWorld , radius:Float , height:Float , orient:TQuaternion , pos:TVector3 )
		Local Obj:TCollision = New TCollision
		Obj.Pointer = OGRENEWTCOL_createCylinder( world.Pointer , radius , height , orient.Pointer , pos.Pointer )
		Return Obj
	End Function
	
	Rem
		bbdoc: Create an ellipsoid-shaped collision.
	ENDREM
	Function createEllipsoid:TCollision(world:TOgreNewtWorld, size:TVector3, orient:TQuaternion, pos:TVector3 ) 
		Local Obj:TCollision = New TCollision
		Obj.Pointer = OGRENEWTCOL_createEllipsoid(world.Pointer, size.Pointer, orient.Pointer, pos.Pointer) 
		Return Obj
	End Function
	
	Rem
		bbdoc: Calclate the MOI for this collision primitive, as well as the computed center of mass. You must pass in two valid TVector3s and they will be populated with the info. For boxes, ellipsoids and convex hulls only! This will NOT work on TreeCollisions.
	ENDREM
	Method calculateInertialMatrix(inertia:TVector3, offset:TVector3)
		OGRENEWTCOL_calculateInertialMatrix(inertia.Pointer, offset.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Destructor.
	ENDREM
	Method Delete()
		OGRENEWTCOL_destroy( Self.Pointer )
	End Method
End Type

Rem
	bbdoc: This class describes complex polygonal collisions that cannot be marginalized into convex hull shapes. Tree collisions are static and one created they may not be moved and will not be affected by other physics objects.
ENDREM
Type TTreeCollision Extends TCollision

	Rem
		bbdoc: Face Winding : Clockwise
	ENDREM
	Const FW_DEFAULT:Int = 0
	
	Rem
		bbdoc: Face Winding : Anticlockwise
	ENDREM
	Const FW_REVERSE:Int = 1

	Rem
		bbdoc: Create an empty TTreeCollision object in the specified world.
	ENDREM
	Function Create:TTreeCollision( world:TOgreNewtWorld )
		Local Obj:TTreeCollision = New TTreeCollision
		Obj.Pointer = OGRENEWTTC_create( world.Pointer )
		Return Obj
	End Function
	
	Rem
		bbdoc: Create a TTreeCollision by using a mesh or set of meshes attached to a TSceneNode.<br>world:TOgreNewtWorld The OgreNewtWorld that you want to create this collision mesh in.<br>sn:TSceneNode The TSceneNode that you want to use to create the TTReeCollision mesh.<br>optimize:Int Whether or not you want to perform optimizations on the mesh or leave it as a 1:1 collision mesh.<br>fw:Int = FW_DEFAULT Set the triangle winding for the mesh and whether it should be inverted or not.
	ENDREM
	Function createWithSceneNode:TTreeCollision( world:TOgreNewtWorld , sn:TSceneNode , optimize:Int , fw:Int = FW_DEFAULT )
		Local Obj:TTreeCollision = New TTreeCollision
		Obj.Pointer = OGRENEWTTC_createWithSceneNode( world.Pointer , sn.Pointer , optimize , fw )
		Return Obj
	End Function
	
	Rem
		bbdoc: Call to start is required before you start adding polygons. 
	ENDREM
	Method start()
		OGRENEWTTC_start( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Add a polygon to the tree collision using an array of TVector3s. You must also specify an id. 
	ENDREM
	Method addPoly( polys:TVector3 , ID:Int )
		OGRENEWTTC_addPoly( polys.Pointer , id , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Call to finish adding polygons.
	ENDREM
	Method finish()
		OGRENEWTTC_finish( Self.Pointer )
	End Method
	
End Type

Rem
	bbdoc: Primay type for representing rigid bodies. This type does not cleanup automatically with GC after construction and will need to be .Destroy() ed after creation. This is generally done using a TList to hold all TBodies, then destroying them on shutdown.
ENDREM
Type TBody

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Constructor for TBodies. Here you specify three things: W:TOgreNewtWorld - The world that your physics body will be created in.  col:TCollision - The collision shape of your rigid body.  bodytype:Int - An integer for use in identifying this TBody after creation. This is very useful when coupled with callbacks.
	ENDREM
	Function Create:TBody(W:TOgreNewtWorld, col:TCollision, bodytype:Int = 0) 
		Local obj:TBody = New TBody
		obj.Pointer = BODY_create( W.Pointer , col.Pointer , bodytype )
		Return obj
	End Function
	
	Rem
		bbdoc:Method To Destroy TBody.TBody does Not have a destructor And requires the user To Destroy any instances.
	ENDREM
	Method Destroy() 
		BODY_destroy( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Store a reference to an object within this TBody. Keep in mind that it is just a reference and if the original object is destroyed then the reference will be invalid. If you need to use a single numeric value, you still must wrap it in a type. For practical use, you would likely use a custom type with many fields, and hold all of the types in a TList.
	ENDREM
	Method setUserData(data:Object) 
		BODY_setUserData( data , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get a reference ( to an object ) stored in this TBody. You will have to cast from the Object type to the type of reference stored. If the reference is invalid an exception will be thrown.
	ENDREM
	Method getUserData:Object() 
		Return BODY_getUserData( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Returns the Ogre Node that this body is attached to. While this function returns a plain TNode, you may cast it using yourbody.getOgreNode().ToSceneNode().
	ENDREM
	Method getOgreNode:TOgreNode() 
		Local Obj:TOgreNode = New TOgreNode
		Obj.Pointer = BODY_getOgreNode( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Retreive a reference object to the physics world that this body resides in.
	ENDREM
	Method getWorld:TOgreNewtWorld()
		Local Obj:TOgreNewtWorld = New TOgreNewtWorld
		Obj.managed = True
		Obj.Pointer = BODY_getWorld( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Assign a user specified integer value to this body for your own use. 
	ENDREM
	Method setType( bodytype:Int )
		BODY_setType( bodytype , Self.Pointer )		
	End Method
	
	Rem
		bbdoc: Retreive the user specified integer value for this body.
	ENDREM
	Method getType:Int()
		Return BODY_getType( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Attaches this body with an Ogre node and allows the physical system to manipulate the node.
	ENDREM
	Method attachNode (node:TOgreNode)
		BODY_attachNode(node.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Sets a basic standard 9.8m/s^2 gravity callback. Usually you will need to set your own custom callback to simulate different forces on the rigid bodies you create.
	ENDREM
	Method setStandardForceCallback() 
		BODY_setStandardForceCallBack( Self.Pointer ) 
	End Method
	
	Rem
		bbdoc: Allows you to set a callback function for this TBody. A force callback function has a simple format, and is the most efficient way to manipulate bodies that need more than simple gravity. A simple example:  <br>Function CubeCallback(b:Byte Ptr) <br>     Local body:TBody = TBody.FromPtr(b) <br>     body.addForce(TVector3.Create(0, - 10, 0))  <br> End Function<br/> Note that you may also extend the function to include timeStep and thread information like so:<br/>Function CubeCallback(b:Byte Ptr , timeStep:Float , threadIndex:Int )
	ENDREM
	Method setCustomForceAndTorqueCallback( callback:Byte Ptr )
		BODY_setCustomForceAndTorqueCallback( callback , Self.Pointer )	
	End Method
	
	Rem
		bbdoc: Remove the custom force and torque callback attached to this body if one exists.
	ENDREM
	Method removeForceAndTorqueCallback() 
		BODY_removeForceAndTorqueCallback(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set up a custom transform callback.<br/>The callback should be structured like: Function Callback( body:Byte Ptr , orient:Byte Ptr , pos:Byte Ptr , threadIndex:Int ).
	ENDREM
	Method setCustomTransformCallback(callback:Byte Ptr) 
		BODY_setCustomTransformCallback(callback, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Remove the applied transform callback from this rigid body.
	ENDREM
	Method removeTransformCallback() 
		BODY_removeTransformCallback(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set mass matrix for this TBody.
	ENDREM
	Method setMassMatrix( mass:Float , momentOfInertia:TVector3 )
		BODY_setMassMatrix( mass, momentOfInertia.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the position and orientation of this TBody.
	ENDREM
	Method setPositionOrientation( pos:TVector3 , orientation:TQuaternion )
		BODY_setPositionOrientation( pos.Pointer , orientation.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Add force to this TBody. This should only be used inside a force callback.
	ENDREM
	Method addForce( force:TVector3 )
		BODY_addForce( force.Pointer , Self.Pointer )
	End Method
	
	REM
		bbdoc: Add a local force to this body from a specified position.
	ENDREM
	Method addLocalForce(force:TVector3, pos:TVector3)
		BODY_addLocalForce(force.Pointer, pos.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Add rotational force/torque to this TBody. This should only be used inside a torque callback.
	ENDREM
	Method addTorque(torque:TVector3) 
		BODY_addTorque( torque.Pointer,  Self.Pointer )
	End Method
	
	REM
		bbdoc: Set the current torque of this body with a 3-dimensional vector.
	ENDREM
	Method setTorque(torque:TVector3)
		BODY_setTorque(torque.Pointer, Self.Pointer)
	End Method
	
	REM
		bbdoc: Get the torque of this body as a 3-dimensional vector.
	ENDREM
	Method getTorque:TVector3()
		Return TVector3.FromPtr( BODY_getTorque( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Set the current velocity of this body with a 3-dimensional vector.
	ENDREM
	Method setVelocity( vel:TVector3 )
		BODY_setVelocity( vel.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the mass matrix for this TBody and store it in the two provided variables.
	ENDREM
	Method getMassMatrix( mass:Float , inertia:TVector3 )
		BODY_getMassMatrix( mass , inertia.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the omega of this body.
	ENDREM
	Method setOmega( omega:TVector3 )
		BODY_setOmega( omega.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the center of mass for a rigid body, at runtime, without having to offset it.
	ENDREM
	Method setCenterOfMass(centerOfMass:TVector3) 
		BODY_setCenterOfMass(centerOfMass.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set a continuous collision mode for this object. This will prevent an object that moves at high speeds from tunneling through another object. There is a performance hit with this, so use with discretion. You may also set this up with material callbacks, rather than per body. 
	ENDREM
	Method setContinuousCollisionMode(state:Int)
		BODY_setContinuousCollisionMode(state, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set whether or not all bodies that are connected tot his one through jonits should be allowed to collide with this body or not.
	ENDREM
	Method setJointRecursiveCollision(state:Int)
		BODY_setJointRecursiveCollision(state, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set whether or not this body goes to sleep or freezes when equilibrium is reached.
	ENDREM
	Method setAutoSleep(flag:Int)
		BODY_setAutoSleep(flag, Self.Pointer)
	End Method
	
	Rem
		bbodc:  Get whether or not this body goes to sleep or freezes when equilibrium is reached.
	ENDREM
	Method getAutoSleep:Int()
		Return BODY_getAutoSleep(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set whether or not this body is currently asleep/frozen. True for sleeping, False for awake.
	ENDREM
	Method setSleepState(state:Int)
		BODY_setSleepState(state, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get whether or not this body is currently asleep/frozen. True for sleeping, False for awake.
	ENDREM
	Method getSleepState()
		BODY_getSleepState(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the omega of this body.
	ENDREM
	Method getOmega:TVector3() 
		Return TVector3.FromPtr(BODY_getOmega(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the position and orientation of this TBody and store them both in the provided variables.
	ENDREM
	Method getPositionOrientation( pos:TVector3 , orientation:TQuaternion )
		BODY_getPositionOrientation( pos.Pointer , orientation.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the current velocity of this TBody as a 3-dimensional vector.
	ENDREM
	Method getVelocity:TVector3()
		Return TVector3.FromPtr(BODY_getVelocity(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the MaterialID of this TBody.
	ENDREM
	Method getMaterialGroupID:TMaterialID()
		Local Obj:TMaterialID = New TMaterialID
		Obj.Pointer = BODY_getMaterialGroupID( Self.Pointer )
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the MaterialID of this TBody.
	ENDREM
	Method setMaterialGroupID( ID:TMaterialID )
		BODY_setMaterialGroupID(id.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Adds a global force to this TBody.<br>deltav:TVector3 specifices the direction and magnitude from the global point that the force will have.<br>posit:TVector3 specifies the global position to add the force to. This is usually going to be the global position of the TBody.<br>Adding an impulse to an object is useful for simulating explosions and globally directional forces like a pool stick striking a ball. 
	ENDREM
	Method addImpulse(deltav:TVector3, posit:TVector3) 
		BODY_addImpulse(deltav.Pointer, posit.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Add buoyancy force to this body. You must use a callback to implement this properly.
	ENDREM
	Method addBuoyancyForce(fluidDensity:Float, fluidLinearViscosity:Float, fluidAngularViscosity:Float, gravity:TVector3, callback:Byte Ptr)
		BODY_addBuoyancyForce(fluidDensity, fluidLinearViscosity, fluidAngularViscosity, gravity.Pointer, callback, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Helper function that returns the current position of this TBody.
	ENDREM
	Method getPosition:TVector3() 
		Return TVector3.FromPtr( BODY_getPosition( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Helper function that returns this body's mass*-9.8ms^2 gravity. Helpful for adding gravity forces easily in callbacks.
	ENDREM
	Method calculateGravity:TVector3() 
		Return TVector3.FromPtr( BODY_calculateGravity( Self.Pointer ) )
	End Method
	
	Rem
		bbdoc: Helper function to create a TBody from a reference/pointer.
	ENDREM
	Function FromPtr:TBody(Pointer:Byte Ptr) 
		Local Obj:TBody = New TBody
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
	Rem
		bbdoc: Get whether or not this rigid body has user data.
	ENDREM
	Method hasUserData:Byte() 
		Return BODY_hasUserData(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Clear the user data from this TBody. Keep in mind that this only removes the reference to the data. The actual data will have to be either cleared by Garbage Collection or destroyed manually.
	ENDREM
	Method clearUserData() 
		BODY_clearUserData(Self.Pointer) 
	End Method

	
End Type

Rem
	bbdoc: This class defines the Newton materials interface.  The materials themselves are just integral ids, but when you pair them together into TMaterialPairs, you can define how reactions occur between the two, setting physical properties such as friction and elasticity, or a custom callback for when the two materials collide.
ENDREM
Type TMaterialID

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Creates an material ID for attaching to a body.
	ENDREM
	Function Create:TMaterialID( world:TOgreNewtWorld )
		Local Obj:TMaterialID = New TMaterialID
		Obj.Pointer = OGRENEWTMID_create( world.Pointer )
		Return Obj
	End Function
	
	Rem
		bbdoc: Destructor
	ENDREM
	Method Destroy()
		OGRENEWTMID_delete( Self.Pointer )
	End Method
	
	Rem
		bbdoc: Get the integral id value of this TMaterialID. You can use these for comparisons, etc.
	ENDREM
	Method getID:Int()
		Return OGRENEWTMID_getID( Self.Pointer )
	End Method
End Type


Rem
	bbdoc: Base joint class that provides basic joint functionality to all other joint classes. Constructors for this and all basic joint types are located in TBasicJoints. Joints are automatically cleaned up with the TOgreNewtWorld they are created in is destroyed, but you may also call .destroy to get rid of them manually.
ENDREM
Type TJoint
	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Specifies whether or not this object is cleaned up with garbage collection.
	ENDREM
	Field managed:Byte
	
	Rem
		bbdoc: Creats a TJoint object from a given pointer.
	ENDREM
	Function FromPtr:TJoint(Pointer:Byte Ptr)
		Local Obj:TJoint = New TJoint
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
	Rem
		bbdoc: Get the collision state for this TJoint. The collision state decides whether or not collisions should be calculated between the child and parent of the TJoint.<br>1 = collision ON<br>0 = collision OFF
	ENDREM
	Method getCollisionState:Int() 
		Return OGRENEWTJOINT_getCollisionState(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the collision state for this TJoint. The collision state decides whether or not collisions should be calculated between the child and parent of the TJoint.<br>1 = collision ON<br>0 = collision OFF
	ENDREM
	Method setCollisionState(State:Int) 
		OGRENEWTJOINT_setCollisionState(State, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the joint stiffness for this TJoint. Joint stiffness represents how much friction there will be when moving the joint pieces. Higher stiffness values create more friction and less stability. (0.0-1.0)
	ENDREM
	Method getStiffness:Float() 
		Return OGRENEWTJOINT_getStiffness(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the joint stiffness for this TJoint. Joint stiffness represents how much friction there will be when moving the joint pieces. Higher stiffness values create more friction and less stability. (0.0-1.0)
	ENDREM
	Method setStiffness(stiffness:Float) 
		OGRENEWTJOINT_setStiffness(stiffness, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Store a reference to an object within this TJoint. Keep in mind that it is just a reference and if the original object is destroyed then the reference will be invalid. If you need to use a single numeric value, you still must wrap it in a type. For practical use, you would likely use a custom type with many fields, and hold all of the types in a TList.
	ENDREM
	Method setUserData(data:Object) 
		OGRENEWTJOINT_setUserData(data, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get a reference ( to an object ) stored in this TBody. You will have to cast from the Object type to the type of reference stored. If the reference is invalid an exception will be thrown.
	ENDREM
	Method getUserData:Object() 
		Return OGRENEWTJOINT_getUserData(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Manually destroy this joint.
	ENDREM
	Method destroy()
		OGRENEWTJOINT_delete(Self.Pointer) 
	End Method
	
		Rem
		bbdoc: Clear the user data from this TBody. Keep in mind that this only removes the reference to the data. The actual data will have to be either cleared by Garbage Collection or destroyed manually.
	ENDREM
	Method clearUserData() 
		OGRENEWTJOINT_clearUserData(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get whether or not this joint has valid user data.
	ENDREM
	Method hasUserData:Byte() 
		Return OGRENEWTJOINT_hasUserData(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Check to see if the reference to this object in memory is null.
	ENDREM
	Method isNull:Byte() 
		If Self.Pointer = Null Then Return True Else Return False
	End Method
	
End Type

Rem
	bbdoc: Interface that allows you to construct any of the basic joint types. 
ENDREM
Type TBasicJoints
	
	Rem
		bbdoc: Create a base unspecialized joint. Generally you will not use this, as you will want to use one of the extended joint types.
	ENDREM
	Function CreateJoint:TJoint() 
		Return TJoint.FromPtr(OGRENEWTJOINT_create()) 
	End Function
	
	Rem
		bbdoc: Create a ball and socket based joint with settings for limits.
	ENDREM
	Function CreateBallAndSocket:TBallAndSocket(world:TOgreNewtWorld, child:TBody, parent:TBody, Pos:TVector3) 
		Return TBallAndSocket.FromPtr(OGRENEWTBAS_create(world.Pointer, child.Pointer, parent.Pointer, Pos.Pointer)) 
	End Function
	
	Rem
		bbdoc: Create a hinged joint by specifying the parent and child rigid bodies as well as the hinge and pin positions.
	ENDREM
	Function CreateHinge:THingeJoint(world:TOgreNewtWorld, child:TBody, parent:TBody, Pos:TVector3, pin:TVector3) 
		Return THingeJoint.FromPtr(OGRENEWTHINGE_create(world.Pointer, child.Pointer, parent.Pointer, Pos.Pointer, pin.Pointer)) 
	End Function
	
	Rem
		bbdoc: Create a slider joint by specifying the parent and child rigid bodies as well as the joint and pin positions.
	ENDREM
	Function CreateSlider:TSliderJoint(world:TOgreNewtWorld, child:TBody, parent:TBody, Pos:TVector3, pin:TVector3) 
		Return TSliderJoint.FromPtr(OGRENEWTSLIDER_create(world.Pointer, child.Pointer, parent.Pointer, Pos.Pointer, pin.Pointer)) 
	End Function
	
	Rem
		bbdoc: Create a universal joint by specifying the parent and child rigid bodies as well as the joint and pin positions.
	ENDREM
	Function CreateUniversal:TUniversalJoint(world:TOgreNewtWorld, child:TBody, parent:TBody, Pos:TVector3, pin0:TVector3, pin1:TVector3) 
		Return TUniversalJoint.FromPtr(OGRENEWTUNIVERSAL_create(world.Pointer, child.Pointer, parent.Pointer, Pos.Pointer, pin0.Pointer, pin1.Pointer)) 
	End Function
	
	Rem
		bbdoc: Create an up vector joint by specifying the body as well as the single pin position.
	ENDREM
	Function CreateUpVector:TUpVectorJoint(world:TOgreNewtWorld, body:TBody, pin:TVector3) 
		Return TUpVectorJoint.FromPtr(OGRENEWTUV_create(world.Pointer, body.Pointer, pin.Pointer)) 
	End Function

End Type

Rem
	bbdoc: Ball and socket implementation including angular limits.
ENDREM
Type TBallAndSocket Extends TJoint

	Rem
		bbdoc: Creats a TBallAndSocket object from a given pointer. Note that if isManaged is true then the TBallAndSocket will have to be destroyed by yourself or another object, otherwise it will create a memory leak. Alternatively, you could just set managed to false when done and let TBallAndSocket destroy itself.
	ENDREM
	Function FromPtr:TBallAndSocket(Pointer:Byte Ptr)
		Local Obj:TBallAndSocket = New TBallAndSocket
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
	Rem
		bbdoc: Get the current angle of the joint as a TVector3.
	ENDREM
	Method getJointAngle:TVector3() 
		Return TVector3.FromPtr(OGRENEWTBAS_getJointAngle(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the current omega of the joint as a TVector3.
	ENDREM
	Method getJointOmega:TVector3() 
		Return TVector3.FromPtr(OGRENEWTBAS_getJointOmega(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Get the current force of the joint as a TVector3.
	ENDREM
	Method getJointForce:TVector3() 
		Return TVector3.FromPtr(OGRENEWTBAS_getJointForce(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Set the rotation limits for this ball and socket joint.<br/>pin:TVector3 - The pin direction for this TBallSocketJoint.<br/>maxCone:TRadian - The maximum angle at which the child is allowed to swing.<br/>maxTwist:Tradian - The maximum angle at which the child is allowed to twist.
	ENDREM
	Method setLimits(pin:TVector3, maxCone:TRadian, maxTwist:TRadian) 
		OGRENEWTBAS_setLimits(pin.Pointer, maxCone.Pointer, maxTwist.Pointer, Self.Pointer) 
	End Method
End Type

Rem
	bbdoc: Hinge joint that implements limits and motors through a callback.
ENDREM
Type THingeJoint Extends TJoint

	Rem
		bbdoc: Creates a THingeJoint object from a given pointer. 
	ENDREM
	Function FromPtr:THingeJoint(Pointer:Byte Ptr)
		Local Obj:THingeJoint = New THingeJoint
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
	Rem
		bbdoc: Get the angle around the pin.
	ENDREM
	Method getJointAngle:TRadian() 
		Local Obj:TRadian = New TRadian
		Obj.Pointer = OGRENEWTHINGE_getJointAngle(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the rotational velocity around the pin.
	ENDREM
	Method getJointOmega:Float() 
		Return OGRENEWTHINGE_getJointOmega(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the force on the joint.
	ENDREM
	Method getJointForce:TVector3() 
		Return TVector3.FromPtr(OGRENEWTHINGE_getJointForce(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Set the callback for this joint. Setting a callback will allow you to specify limits and apply various custom forces to the joint. Syntax:<br>Function HingeCallback(me:Byte Ptr)<br>	Global h:THingeJoint = THingeJoint.FromPtr(me, True)<br>	'Do Stuff<br>End Function<br><br>yourHinge.setCallback(HingeCallback) 
	ENDREM
	Method setCallback(callback:Byte Ptr) 
		OGRENEWTHINGE_setCallback(callback, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the value for acceleration around the joint pin. This is only to be used inside the hinge callback function.
	ENDREM
	Method setCallbackAccel(accel:Float) 
		OGRENEWTHINGE_setCallbackAccel(accel, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the minimum amount of joint friction. This is only to be used inside the hinge callback function.
	ENDREM
	Method setCallbackFrictionMin(minValue:Float) 
		OGRENEWTHINGE_setCallbackFrictionMin(minValue, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the maximum value of joint friction. This is only to be used inside the hinge callback function.
	ENDREM
	Method setCallbackFrictionMax(maxValue:Float) 
		OGRENEWTHINGE_setCallbackFrictionMax(maxValue, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets the current physics timestep. This is only to be used inside the hinge callback function.
	ENDREM
	Method getCallbackTimestep:Float() 
		Return OGRENEWTHINGE_getCallbackTimestep(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the acceleration value necessary to stop this hinge joint at a specified angle. This is for implementing joint limits and should not be used outside of a hinge callback.
	ENDREM
	Method calculateStopAlpha:Float(angle:TRadian) 
		Return OGRENEWTHINGE_calculateStopAlpha(angle.Pointer , Self.Pointer )
	End Method
	
	
End Type

Rem
	bbdoc: Slider joint that implements limits and motors through a callback.
ENDREM
Type TSliderJoint Extends TJoint

	Rem
		bbdoc: Creates a TSliderJoint object from a given pointer. 
	ENDREM
	Function FromPtr:TSliderJoint(Pointer:Byte Ptr)
		Local Obj:TSliderJoint = New TSliderJoint
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
	Rem
		bbdoc: Get the position of the child body along the pin.
	ENDREM
	Method getJointPosit:Float() 
		Return OGRENEWTSLIDER_getJointPosit(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the rotational velocity along the pin.
	ENDREM
	Method getJointVeloc:Float() 
		Return OGRENEWTSLIDER_getJointVeloc(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the force on the joint.
	ENDREM
	Method getJointForce:TVector3() 
		Return TVector3.FromPtr(OGRENEWTSLIDER_getJointForce(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Set the callback for this joint. Setting a callback will allow you to specify limits and apply various custom forces to the joint. Syntax:<br>Function SliderCallback(me:Byte Ptr)<br>	Global h:TSliderJoint = TSliderJoint.FromPtr(me, True)<br>	'Do Stuff<br>End Function<br><br>yourSlider.setCallback(SliderCallback) 
	ENDREM
	Method setCallback(callback:Byte Ptr) 
		OGRENEWTSLIDER_setCallback(callback, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the value for acceleration along the joint pin. This is only to be used inside the slider callback function.
	ENDREM
	Method setCallbackAccel(accel:Float) 
		OGRENEWTSLIDER_setCallbackAccel(accel, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the minimum amount of joint friction. This is only to be used inside the slider callback function.
	ENDREM
	Method setCallbackFrictionMin(minValue:Float) 
		OGRENEWTSLIDER_setCallbackFrictionMin(minValue, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the maximum value of joint friction. This is only to be used inside the slider callback function.
	ENDREM
	Method setCallbackFrictionMax(maxValue:Float) 
		OGRENEWTSLIDER_setCallbackFrictionMax(maxValue, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Gets the current physics timestep. This is only to be used inside the slider callback function.
	ENDREM
	Method getCallbackTimestep:Float() 
		Return OGRENEWTSLIDER_getCallbackTimestep(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the acceleration value necessary to stop this slider joint at a specified distance. This is for implementing joint limits and should not be used outside of a slider callback.
	ENDREM
	Method calculateStopAccel:Float(dist:Float) 
		Return OGRENEWTSLIDER_calculateStopAccel(dist, Self.Pointer) 
	End Method
	
	
End Type

Rem
	bbdoc: Basic universal joint implementation. You can implement limits and motors through the callback.
ENDREM
Type TUniversalJoint Extends TJoint
	
	Rem
		bbdoc: Creates a TUniversalJoint object from a given pointer. Note that if isManaged is true then the TUniversalJoint will have to be destroyed by yourself or another object, otherwise it will create a memory leak. Alternatively, you could just set managed to false when done and let TUniversalJoint destroy itself.
	ENDREM
	Function FromPtr:TUniversalJoint(Pointer:Byte Ptr)
		Local Obj:TUniversalJoint = New TUniversalJoint
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
	Rem
		bbdoc: Get the current angle around pin 0.
	ENDREM
	Method getJointAngle0:TRadian() 
		Local Obj:TRadian = New TRadian
		Obj.Pointer = OGRENEWTUNIVERSAL_getJointAngle0(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the current angle around pin 1.
	ENDREM
	Method getJointAngle1:TRadian() 
		Local Obj:TRadian = New TRadian
		Obj.Pointer = OGRENEWTUNIVERSAL_getJointAngle1(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the rotational velocity around pin 0.
	ENDREM
	Method getJointOmega0:Float() 
		Return OGRENEWTUNIVERSAL_getJointOmega0(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the rotational velocity around pin 1.
	ENDREM
	Method getJointOmega1:Float() 
		Return OGRENEWTUNIVERSAL_getJointOmega1(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the force on this joint.
	ENDREM
	Method getJointForce:TVector3() 
		Return TVector3.FromPtr(OGRENEWTUNIVERSAL_getJointForce(Self.Pointer)) 
	End Method
	
	Rem
		bbdoc: Set the callback for this joint. Setting a callback will allow you to specify limits and apply various custom forces to the joint. Syntax:<br>Function UniversalCallback(me:Byte Ptr)<br>	Global u:TUniversalJoint = TUniversalJoint.FromPtr(me, True)<br>	'Do Stuff<br>End Function<br><br>yourUniversal.setCallback(UniversalCallback) 
	ENDREM
	Method setCallback(callback:Byte Ptr) 
		OGRENEWTUNIVERSAL_setCallback(callback, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the acceleration of this joint around a given pin (0-1). This is only to be used within a joint callback function.
	ENDREM
	Method setCallbackAccel(accel:Float, axis:Int) 
		OGRENEWTUNIVERSAL_setCallbackAccel(accel, axis, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the minimum amount of friction around a given pin (0-1). This is only to be used within a joint callback function.
	ENDREM
	Method setCallbackFrictionMin(minValue:Float, axis:Int) 
		OGRENEWTUNIVERSAL_setCallbackFrictionMin(minValue, axis, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Set the maximum amount of friction around a given pin (0-1). This is only to be used within a joint callback function.
	ENDREM
	Method setCallbackFrictionMax(maxValue:Float, axis:Int) 
		OGRENEWTUNIVERSAL_setCallbackFrictionMax(maxValue, axis, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get the current physics timestep. This is only to be used within a joint callback function.
	ENDREM
	Method getCallbackTimestep:Float() 
		Return OGRENEWTUNIVERSAL_getCallbackTimestep(Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Calculate the acceleration required to stop the joint at the angle specified for pin 0.This is only to be used within a joint callback function. 
	ENDREM
	Method calculateStopAlpha0:Float(angle:Float) 
		Return OGRENEWTUNIVERSAL_calculateStopAlpha0(angle, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Calculate the acceleration required to stop the joint at the angle specified for pin 1.This is only to be used within a joint callback function. 
	ENDREM
	Method calculateStopAlpha1:Float(angle:Float) 
		Return OGRENEWTUNIVERSAL_calculateStopAlpha1(angle, Self.Pointer) 
	End Method
	
	
	
	
End Type

Rem
	bbdoc: Very basic upvector joint that will remove all rotation except for a single pin. This is useful for many things, including controlling characters within the physics system.
ENDREM
Type TUpVectorJoint Extends TJoint
	
	Rem
		bbdoc: Creats a TUpVectorJoint object from a given pointer. 
	ENDREM
	Function FromPtr:TUpVectorJoint(Pointer:Byte Ptr)
		Local Obj:TUpVectorJoint = New TUpVectorJoint
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
	Rem
		bbdoc: Set this joint's single pin direction.
	ENDREM
	Method setPin(pin:TVector3) 
		OGRENEWTUV_setPin(pin.Pointer, Self.Pointer) 
	End Method
	
	Rem
		bbdoc: Get this joint's single pin direction.
	ENDREM
	Method getPin:TVector3() 
		Return TVector3.FromPtr(OGRENEWTUV_getPin(Self.Pointer)) 
	End Method
End Type


Rem
	bbdoc: A contact callback class that you should subclass to create custom collision behavior.
ENDREM
Type TContactCallback

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Gets whether or not this is object will be cleaned up by Garbage Collection.
	ENDREM
	Field managed:Byte = False
	
	Rem
		bbdoc: Constructor.
	ENDREM
	Function Create:TContactCallback() 
		Local Obj:TContactCallback = New TContactCallback
		Obj.Pointer = CONTACTCALLBACK_create(userBegin, userProcess)
		Return Obj
	End Function
	
	Rem
		bbdoc: Gets the first body in this collision callback. This is only valid during collisions and is otherwise void.
	ENDREM
	Method getBody0:TBody() 
		Local Obj:TBody = New TBody
		Obj.Pointer = CONTACTCALLBACK_getBody0(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: Gets the second body in this collision callback. This is only valid during collisions and is otherwise void.
	ENDREM
	Method getBody1:TBody() 
		Local Obj:TBody = New TBody
		Obj.Pointer = CONTACTCALLBACK_getBody1(Self.Pointer) 
		Return Obj
	End Method
	
	Rem
		bbdoc: This is called whenever two collision body's AABBs overlap. It is basically a "warning" that a collision is likely to occur.
	ENDREM
	Function userBegin:Int(threadIndex:Int)
		Return 1
	End Function
	
	Rem
		bbdoc: This function will be called everytime a "real" collision occurs. All collision data will be available.
	ENDREM
	Function userProcess(contactIterator:Byte Ptr, timeStep:Float, threadIndex:Int)
	End Function
	
	Rem
		bbdoc: Delete this object if it is unamanged and goes out of scope.
	ENDREM
	Method Delete()
		If Not Self.managed Then CONTACTCALLBACK_delete(Self.Pointer)
	End Method
	
End Type

Rem
	bbdoc: A class for defining interactions between materials. It is constructed with two materials, and defines the kind of surface properties the two objecsts have towards one another. This covers attributes like softness, elasticity, etc, and also allows for custom callbacks on collision.
ENDREM
Type TMaterialPair

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Reference to the current callback in use.
	ENDREM
	Field contactCallback:TContactCallback = Null
	
	Rem
		bbdoc: Constructor, creates a material pair from two given materials. <br>world:TOgreNewtWorld Your physics world.<br>mat1:TMaterialID One of the materials you wish to have in the pair.<br>mat2:TMaterialID Another material you wish to have in the pair.
	ENDREM
	Function Create:TMaterialPair(world:TOgreNewtWorld, mat1:TMaterialID, mat2:TMaterialID) 
		Local Obj:TMaterialPair = New TMaterialPair
		Obj.Pointer = OGRENEWTMP_create( world.Pointer , mat1.Pointer , mat2.Pointer )
		Return Obj
	End Function
	
	Rem
		bbdoc: Sets the softness for reactions between these two materials.
	ENDREM
	Method setDefaultSoftness( softness:Float )
		OGRENEWTMP_setDefaultSoftness( softness , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Sets the elasticity for reactions between these two materials.
	ENDREM
	Method setDefaultElasticity( elasticity:Float )
		OGRENEWTMP_setDefaultElasticity( elasticity , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Specifies properties about whether these materials are collidable or not.
	ENDREM
	Method setDefaultCollidable( state:Int )
		OGRENEWTMP_setDefaultCollidable( state , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Set the friction properties between these materials.
	ENDREM
	Method setDefaultFriction( stat:Float , kinetic:Float )
		OGRENEWTMP_setDefaultFriction( stat , kinetic , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Turn continuous collision on or off for these materials.
	ENDREM
	Method setContinuousCollisionMode( state:Int )
		OGRENEWTMP_setContinuousCollisionMode( state , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Assign a custom callback for handling collisions between these materials.
	ENDREM
	Method setContactCallback(callback:TContactCallback)
		
		'If there was a callback before this one, set it back to unamanged to avoid memory leak.
		If contactCallback <> Null Then
			contactCallback.managed = False
		End If
		
		'Manage this callback and make it the current callback for this material pair.
		callback.managed = True
		Self.contactCallback = callback
		
		OGRENEWTMP_setContactCallback( callback.Pointer , Self.Pointer )
	End Method
	
	Rem
		bbdoc: Destructor
	ENDREM
	Method Destroy()
		OGRENEWTMP_delete( Self.Pointer )
	End Method
	
	Rem
		bbdoc: helper used when setting up callbacks.
	ENDREM
	Function SetupCallbacks(Callback:TContactCallback, userBeginMaxFunc:Byte Ptr, userProcessMaxFunc:Byte Ptr)
		callback.Pointer = CONTACTCALLBACK_create(userBeginMaxFunc, userProcessMaxFunc)
	End Function
	

End Type

Rem
	bbdoc: An iterator for a set of contacts retreived from a contact callback during the userProcess function.
ENDREM
Type TContactIterator

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Construct a new TContactIterator from a pointer.
	ENDREM
	Function FromPtr:TContactIterator(Pointer:Byte Ptr)
		Local Obj:TContactIterator = New TContactIterator
		Obj.Pointer = Pointer
		Return Obj
	End Function
	
	Rem
		bbdoc: Get the current contact for this iterator.
	ENDREM
	Method getCurrent:TContact()
		Local Obj:TContact = New TContact
		Obj.Pointer = CI_getCurrent(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Move to the next element in the iterator. If there was a new element to move to, this method will return True.
	ENDREM
	Method moveNext:Byte()
		Return CI_moveNext(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Reset this iterator to the first contact.
	ENDREM
	Method reset()
		CI_reset(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Check to see whether or not this iterator has more elements above the current one.
	ENDREM
	Method hasMoreElements:Byte()
		Return CI_hasMoreElements(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the number of contacts for this contact iterator.
	ENDREM
	Method getContactCount:Int()
		Return CI_getContactCount(Self.Pointer)
	End Method
	
End Type

Rem
	bbdoc: Represents a single point of contact in a contact callback.
ENDREM
Type TContact

	Rem
		bbdoc: Reference to this object in memory.
	ENDREM
	Field Pointer:Byte Ptr
	
	Rem
		bbdoc: Disable this contact.
	ENDREM
	Method removeContact()
		CONTACT_removeContact(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the face ID of the tree collision if you have collided with one.
	ENDREM
	Method getContactFaceAttribute:Int()
		CONTACT_getContactFaceAttribute(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the collision ID of a currently colliding body.
	ENDREM
	Method getBodyCollisionID:Int(body:TBody)
		Return CONTACT_getBodyCollisionID(body.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the contact normal speed. That is, the general speed of the collision.
	ENDREM
	Method getContactNormalSpeed:Float()
		Return CONTACT_getContactNormalSpeed(Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the contact force as a TVector3. This gives you the direction as a vector and the amount of force as the magnitude of the vector.
	ENDREM
	Method getContactForce:TVector3()
		Local Obj:TVector3 = New TVector3
		Obj.Pointer = CONTACT_getContactForce(Self.Pointer)
		Return Obj
	End Method
	
	Rem
		bbdoc: Get the contact position and normal. Note that you pass in created TVector3s and they are populated with the data. The reason for this is that you don't want to constantly allocate and deallocate objects inside of callbacks.
	ENDREM
	Method getContactPositionAndNormal(pos:TVector3, norm:TVector3)
		CONTACT_getContactPositionAndNormal(pos.Pointer, norm.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the tangent vectors of this collision. Note that you pass in created TVector3s and they are populated with the data. The reason for this is that you don't want to constantly allocate and deallocate objects inside of callbacks.  
	ENDREM
	Method getContactTangentDirections(dir0:TVector3, dir1:TVector3)
		CONTACT_getContactTangentDirections(dir0.Pointer, dir1.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Get the speed of this collision's contact tangent witht he specified tangent index. (0-1)
	ENDREM
	Method getContactTangentSpeed:Float(index:Int)
		Return CONTACT_getContactTangentSpeed(index, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the softness of this contact.
	ENDREM
	Method setContactSoftness(softness:Float)
		CONTACT_setContactSoftness(softness, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the elasticity of this contact.
	ENDREM
	Method setContactElasticity(elasticity:Float)
		CONTACT_setContactElasticity(elasticity, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the current friction state of the contact.<br/>state:Index - The new state of friction. 0 Makes the state a frictionless state.<br/>index:Int - Describes friction along the primary collision tangent ( 0 ) or the secondary tangent ( 1 ).
	ENDREM
	Method setContactFrictionState(state:Int, index:Int)
		CONTACT_setContactFrictionState(state, index, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the parameters for general friction for this contact.<br/>stat:Float - Describes static friction.<br/>kinetic:Float - Describes the amount of kinetic friction.<br/>index:Int - Describes friction along the primary collision tangent ( 0 ) or the secondary tangent ( 1 ).
	ENDREM
	Method setContactFrictionCoef(stat:Float, kinetic:Float, index:Int)
		CONTACT_setContactFrictionCoef(stat, kinetic, index, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the tangent acceleration for this contact.<br/>index:Int - Describes acceleration along the primary collision tangent ( 0 ) or the secondary tangent ( 1 ).
	ENDREM
	Method setContactTangentAcceleration(accel:Float, index:Int)
		CONTACT_setContactTangentAcceleration(accel, index, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Rotate the collision tangent directions to a supplied vector.
	ENDREM
	Method rotateTangentDirections(dir:TVector3)
		CONTACT_rotateTangentDirections(dir.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the contact normal direction.
	ENDREM
	Method setContactNormalDirection(dir:TVector3)
		CONTACT_setContactNormalDirection(dir.Pointer, Self.Pointer)
	End Method
	
	Rem
		bbdoc: Set the acceleration along the collision normal.
	ENDREM
	Method setContactNormalAcceleration(accel:Float)
		CONTACT_setContactNormalAcceleration(accel, Self.Pointer)
	End Method
	
End Type
