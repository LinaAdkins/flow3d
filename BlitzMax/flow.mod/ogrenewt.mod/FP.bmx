REM
	bbdoc: A static interface for manipulating physics in Flow. The module you have loaded is the OgreNewt module.
ENDREM
Type fP
	Global _world:TOgreNewtWorld = New TOgreNewtWorld
	Global _isEnabled:Byte = False
	Global _debugEnabled:Byte = False

	Global _RayHitPosition:TVector3
	Global _RayHitBody:TBody

	
	
	REM
		bbdoc: Enable the physics world, which is 10,000^3 in size by default ( world units ). This translates to roughly 10,000 square meters.<br/>frameRate:Int = 60 Describes the frame rate at which the physics will be updated. If the objects in your physics world are going to be travelling very fast, you may want to give the world a higher framerate update.<br/>You must call this function AFTER you have initialised fG.<br/>Returns: The TOgreNewt world created.
	ENDREM
	Function enablePhysics:TOgreNewtWorld(frameRate:Int = 60)
	
		'Return: The current _world variable if the world is already enabled.
		If _isEnabled = True Then
			Local newWorld:TOgreNewtWorld = New TOgreNewtWorld
			newWorld.Pointer = _world.Pointer
			newWorld.managed = True
			Return newWorld
		End If
	
		'Create the world and prepare it to be returned
		Local World:TOgreNewtWorld = TOgreNewtWorld.Create()
		world.setWorldSize(Vec3(- 10000.0, - 10000.0, - 10000.0) , Vec3(10000.0, 10000.0, 10000.0))
		_world.Pointer = World.Pointer
		World.managed = True
		_isEnabled = True
		
		'Set up our physics timer
		TPhysicsTimer.Initialise(frameRate, fG._ogre.getTimer() , _world)
		
		'Return a managed version of world
		World.managed = True
		
		'Go ahead and set the debugger to on, it doesn't start taking up cycles until lines are being drawn
		TOgreNewtDebugger.init(fG._scene)
		
		'Set the solver and friction models to the most efficient
		world.setSolverModel(1)
		world.setFrictionModel(1)
		
		Return World
	End Function
	
	REM
		bbdoc: Disable the physics world. This will remove all associations that your physics objects have and you will have to add physics all over again. This will destroy the world and all rigid bodies and joints with it.
	ENDREM
	Function disablePhysics()
	
		'RETURN: Do nothing if the physics system is already disabled.
		If _isEnabled = False Then
			Return
		End If
	
		'Get rid of the ogrenewt debugger node
		fG._scene.destroySceneNode("__OgreNewt__Debugger__")
		
		_world.Destroy()
		_isEnabled = False
	End Function
	
	REM
		bbdoc: Add physics to an entity with the specified parameters.<br/>entity:TMovableObject - The entity you want to make a physics object out of.<br/>isTreeCollision:Byte = False -  A tree collision is a static collision that does not move. Tree collisions have the added bonus of not having to be convex, so set this to true if you want a non-moving, complicated collision surface.<br/>mass:Float - The weight of the physics object in kilograms.<br/>callback:Byte Ptr = Null - If you have a custom force and torque callback you may specify it here. If you do not specify a tree collision, your object will be wrapped with a convex collision hull with computed Moment of Inertia and Center of Mass. 
	ENDREM
	Function addPhysics:TBody(entity:TMovableObject, isTreeCollision:Byte = False, mass:Float = 10, callback:Byte Ptr = Null)
		
		'Get the parent node
		Local parentNode:TSceneNode = entity.getParentSceneNode()

		'If we are doing tree collision, then make it and return
		If isTreeCollision Then
			Local collision:TCollision = TTreeCollision.createWithSceneNode(_world, parentNode, True)
			Local body:TBody = TBody.Create(_world, collision)
			body.attachNode(parentNode)
			body.setPositionOrientation(parentNode._getDerivedPosition() , parentNode._getDerivedOrientation())
			Return body
		End If
		
		'Since we're not doing a tree collision, do a convex hull collision
		Local collision:TCollision = TCollision.createConvexHull(_world, parentNode, 0, 0, 0)
			
		'Get MOI and Center of Mass -  inertia * mass
		Local moi:TVector3 = Vec3()
		Local com:TVector3 = Vec3()
		collision.calculateInertialMatrix(moi, com)
		moi.mulEqWithScalar(mass)
		
		'Setup and attach the body
		Local body:TBody = TBody.Create(_world, collision)
		body.setPositionOrientation(parentNode._getDerivedPosition() , parentNode._getDerivedOrientation())
		body.setCenterOfMass(com)
		body.setMassMatrix(mass, moi)
		body.setAutoSleep(False)
		body.attachNode(parentNode)
		
		'If we have a callback, set it up, else just give the object gravity.
		If callback <> Null Then
			body.setCustomForceAndTorqueCallback(callback)
		Else
			body.setStandardForceCallback()
		End If
		
		'RETURN: The body created.
		Return body	
	
	End Function
	
	REM
		bbdoc: Add physics to a specified entity using a simple Box or Cube as the hull.<br/>mass:Float = 10 - Mass in kilograms.<br/>size:TVector3 - The scale of the cube in 3 dimensions in world units.(Default: 1,1,1)<br/>orientation:TQuaternion - The rotation of the physics box in respect to the entity it's attached to. (Default: No Rotation)<br/>offset:TVector3 - The offset of the box from the entity's position.(Default: 0,0,0)
	ENDREM
	Function addPhysicsBox:TBody(entity:TMovableObject, mass:Float = 10, size:TVector3 = Null, orientation:TQuaternion = Null, offset:TVector3 = Null, callback:Byte Ptr = Null)
		
		'Get the parent node
		Local parentNode:TSceneNode = entity.getParentSceneNode()
		
		'Handle custom type defaults
		If size = Null Then size = Vec3(1, 1, 1)
		If orientation = Null Then orientation = TQuaternion.createEmpty()
		If offset = Null Then offset = Vec3()
		
		'Create the box collision hull
		Local collision:TCollision = TCollision.createBox(_world, size, orientation, offset)
		
		'Get MOI and Center of Mass - intertia * mass
		Local moi:TVector3 = Vec3()
		Local com:TVector3 = Vec3()
		collision.calculateInertialMatrix(moi, com)
		moi.mulWithScalar(mass)
		
		'Set up the body
		Local body:TBody = TBody.Create(_world, collision)
		body.setPositionOrientation(parentNode._getDerivedPosition() , parentNode._getDerivedOrientation())
		body.setCenterOfMass(com)
		body.setMassMatrix(mass, moi)
		body.setAutoSleep(False)
		body.attachNode(parentNode)
		
		'If we have a callback, set it up, else just give the object gravity.
		If callback <> Null Then
			body.setCustomForceAndTorqueCallback(callback)
		Else
			body.setStandardForceCallback()
		End If
		
		'RETURN: The body created.
		Return body		
	
	End Function
	
	REM
		bbdoc: Add physics to a specified entity using a simple Ellipsoid shape.<br/>mass:Float = 10 - Mass in kilograms.<br/>size:TVector3 - The scale of the cube in 3 dimensions in world units.(Default: 1,1,1)<br/>orientation:TQuaternion - The rotation of the physics box in respect to the entity it's attached to. (Default: No Rotation)<br/>offset:TVector3 - The offset of the box from the entity's position.(Default: 0,0,0)<br/>callback:Byte Ptr = Null - If you have a custom force and torque callback you may specify it here.
	ENDREM
	Function addPhysicsEllipsoid:TBody(entity:TMovableObject, mass:Float = 10, size:TVector3 = Null, orientation:TQuaternion = Null, offset:TVector3 = Null, callback:Byte Ptr = Null)
	
		'Handle custom type defaults
		If size = Null Then size = Vec3(1, 1, 1)
		If orientation = Null Then orientation = TQuaternion.createEmpty()
		If offset = Null Then offset = Vec3()	
	
		'Get the parent node
		Local parentNode:TSceneNode = entity.getParentSceneNode()
		
		'Create the ellipsoid hull
		Local collision:TCollision = TCollision.createEllipsoid(_world, size, orientation, offset)
		
		'Get MOI and Center of Mass - intertia * mass
		Local moi:TVector3 = Vec3()
		Local com:TVector3 = Vec3()
		collision.calculateInertialMatrix(moi, com)
		moi.mulWithScalar(mass)
		
		'Set up the body
		Local body:TBody = TBody.Create(_world, collision)
		body.setPositionOrientation(parentNode._getDerivedPosition() , parentNode._getDerivedOrientation())
		body.setCenterOfMass(com)
		body.setMassMatrix(mass, moi)
		body.setAutoSleep(False)
		body.attachNode(parentNode)
		
		'If we have a callback, set it up, else just give the object gravity.
		If callback <> Null Then
			body.setCustomForceAndTorqueCallback(callback)
		Else
			body.setStandardForceCallback()
		End If
		
		'RETURN: The body created.
		Return body		
	
	
	End Function
	
	REM
		bbdoc: Create a ball and socket joint and attach two bodies by it.<br/>parent:TBody and child:TBody - The two physics bodies to be linked.<br/>collideSelf:Byte = False - Specify whether or not the two bodies in the joint will collide with each other. Off by default.<br/>pinPosition:TVector3 - Joint position in world space. Once set it stays with the Joint bodies. By default it is the midpoint between the two bodies.<br/>You will likely have to set limits after the joint is created. To do this use yourjoint.setLimits(...).
	ENDREM
	Function addBallSocket:TBallAndSocket(parent:TBody, child:TBody, collideSelf:Byte = False, jointPosition:TVector3 = Null)
	
		'Default - If position is null then calculate the halfway point and use that.
		If jointPosition = Null Then
			Local parentPos:TVector3 = parent.getOgreNode()._getDerivedPosition()
			Local childPos:TVector3 = Child.getOgreNode()._getDerivedPosition()
			jointPosition = parentPos.midPoint(childPos)
		End If
		
		'Create the joint and set limits.
		Local joint:TBallAndSocket = TBasicJoints.CreateBallAndSocket(_world, Child, Parent, jointPosition)
		joint.setCollisionState(collideSelf)
		
		Return joint
	End Function
	
	REM
		bbdoc: Get the physics world as a TOgreNewtWorld
	ENDREM
	Function getWorld:TOgreNewtWorld()
		Local newWorld:TOgreNewtWorld = New TOgreNewtWorld
		newWorld.Pointer = _world.Pointer
		newWorld.managed = True
		Return newWorld
	End Function
	
	REM
		bbdoc: Update the physics.
	ENDREM
	Function update()
	
		'If physics is enabled then apply physics
		If _isEnabled Then
		
			'Show debug lines if they are turned on
			If _debugEnabled Then TOgreNewtDebugger.showLines(_world)
		
			'Update the physics by timeslice
			TPhysicsTimer.Update()
			
		End If
		
	End Function
	
	REM
		bbdoc: Set the leave world callback function which will be called for every physics object that leaves the world bounds. The general layout of the function is:<br/>Function LeaveWorldCallback( body:Byte Ptr , threadIndex:Int )<br/>If you need more details, check the FlowPhysics_WorldLeaveCallbacks.bmx file.
	ENDREM
	Function setLeaveWorldCallback(callback:Byte Ptr)
		_world.setLeaveWorldCallback(callback)
	End Function
	
	REM
		bbdoc: Set the world's overall size in world units. You will specify a box with 2 3-dimensional points as the min and max.
	ENDREM
	Function setWorldSize(minPoint:TVector3, maxPoint:TVector3)
		_world.setWorldSize(minPoint, maxPoint)
	End Function
	
	REM
		bbdoc: Set whether or not debug lines are displayed.
	ENDREM
	Function setDebugEnabled(enabled:Byte)
		_debugEnabled = enabled	
	End Function
	
	REM
		bbdoc: Get whether or not debug lines are displayed.
	ENDREM
	Function getDebugEnabled:Byte()
		Return _debugEnabled
	End Function
	
	
	
	
	REM
		BBDOC: Retrieve the X component of the last Newton query's intersect.
	ENDREM
	Function PickedX:Float()
		Return _RayHitPosition.getX()
	End Function

	REM
		BBDOC: Retrieve the Y component of the last Newton query's intersect.
	ENDREM
	Function PickedY:Float()
		Return _RayHitPosition.getY()
	End Function

	REM
		BBDOC: Retrieve the Z component of the last Newton query's intersect.
	ENDREM
	Function PickedZ:Float()
		Return _RayHitPosition.getZ()
	End Function

	REM
		BBDOC:
	ENDREM
	Function PickedBody:Tbody()
		Return _RayHitBody
	End Function

	REM
	 BBDOC: This Function performs a "raypick" using newton physics.< br > StartPos:The point we're casting from.<br>Direction: the direction from startpos.
	EndRem
	Function RayPick:TBody(StartPos:tvector3, Direction:TVector3)
		'Local NewtonCollision:TNewtonCollision = New TNewtonCollision
		Local endvec:TVector3 = startPos.add(Direction)
		Local unit:TVector3 = endvec.sub(startPos)
		unit.normalise()
		Local _raycast:TBasicRaycast = TBasicRaycast.Create(fP.getWorld(), startPos, endvec)
		Local IntersectPoint:TVector3 = endvec
		If _raycast.getHitCount() > 0 Then 'If the ray hit something! Lets calculate it!
			'First, get the distance to the first hit. Multiply it by the length of the ray
			'to get the position of the intersection local to the start point.
			Local Raycastinfo:TBasicRaycastInfo
			raycastinfo = _raycast.getFirstHit()
			If raycastinfo <> Null Then
				Local Dist:Float = Raycastinfo.getDistance() * (endvec.Distance(startpos))
				Local ActualDistance:TVector3 = unit.mulwithscalar(dist)
				'then add this to the start point to get the world position of the intersection.
				IntersectPoint = startpos.add(actualdistance)
				'store the result and return the newtoncollision instance.
				_RayHitPosition = IntersectPoint
				_RayHitBody = Raycastinfo.getBody()
				Return _rayhitbody
			EndIf
		EndIf
		'No hit, return the end point of the ray!
		_RayHitPosition = endvec
		_RayHitBody = Null
		Return Null
	End Function
	
End Type

REM
	bbdoc: A small static type to handle updating the physics thru time-slices.
ENDREM 
Type TPhysicsTimer

Global frameRate:Float
Global timer:TOgreTimer
Global physicsWorld:TOgreNewtWorld
Global timeDelta:Float
Global timeCurrent:Float
Global timePrevious:Float
Global timeElapsed:Float

	REM
		bbdoc: Initialise all of our type variables and propogate the user objects. 
	ENDREM
	Function Initialise(fps:Float, inTimer:TOgreTimer, world:TOgreNewtWorld) 
		frameRate:Float = 1.0 / Float(fps) 
		timer:TOgreTimer = inTimer
		physicsWorld = World
		timeDelta:Float = 0
		timeCurrent:Float = 0
		timePrevious:Float = 0
		timeElapsed:Float = 0
	End Function

	REM
		bbdoc: This function updates the physics world according to the FPS set in the Initialise function. We first attempt to update in slices to keep the updates deterministic, but if we fall too far behind we vary the timeslice to catch up, causing non-deterministic results.
	ENDREM
	Function Update()
	
		'Get the elapsed time between this update and the last-
		timeCurrent = timer.getTime() 
		timeDelta = timeCurrent - timePrevious
		timePrevious = timeCurrent
		timeElapsed = timeElapsed + timeDelta
		
		'Render the number of times elapsed as long as it's no more than 25 times-
		If timeElapsed > frameRate And timeElapsed < frameRate * 25
			While timeElapsed > frameRate
				physicsWorld.update(frameRate) 
				timeElapsed = timeElapsed - frameRate
			Wend
		Else
			'Not enough time has passed for an update-
			If timeElapsed < frameRate
			'Too much time has passed, so do non-deterministic update to keep up-
			Else
				physicsWorld.update(timeElapsed)
				timeElapsed = 0
			End If
			
		End If	
	End Function

End Type


