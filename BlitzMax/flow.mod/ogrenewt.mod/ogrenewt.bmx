SuperStrict

REM
	bbdoc: OgreNewt Module for Flow. 
ENDREM
Module flow.ogrenewt

ModuleInfo "Name: OgreNewt Module for Flow3D"
ModuleInfo "Description: Allows the use of OgreNewt physics inside Flow"
ModuleInfo "License: Private"
ModuleInfo "Authors: Lina Adkins, Damien Sturdy and Adrian Tysoe"

Import flow.main

Include "OgreNewt_Classes.bmx"

'OgreNewt's implementation of the physics helper module.
Include "FP.bmx"