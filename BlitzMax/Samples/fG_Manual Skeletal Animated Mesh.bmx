REM----------------------------------------------------
fG_Manual Skeletal Animated Mesh.bmx
Author: Lina Adkins

This sample will show you how to roll your own meshes, and then attach a manual skeleton to that mesh and move the bones.
This example is a good place to start if you are writing a custom model format importer or need custom physics meshes ( just
disregard the skeleton portion ).

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.Main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG
fG.SetOption("vsync","true")
fG.init(False, 1024, 768, 16, 0, fG.RS_D3D)

'Position Our Camera
fG._camera.setPosition(25, 5, 25)
fG._camera.lookAt(0, 0, 0)

' *** Begin Mesh Building Code ***

'Our Data Lists
Global verts:TFLoatList = TFLoatList.Create()
Global uvs:TFLoatList = TFloatLIst.Create()
Global indices:TIndexList = TIndexList.Create()

'Create our mesh and submesh
Global msh:TMesh = TMeshManager.createManual("msh", "General")
Global sub:TSubMesh = msh.createSubMesh()

'Add the required geometry to our lists
SetupGeometryLists()

'Compute sizes
Global nVertices:Int = verts.size() / 6
Global ibufCount:Int = indices.size()

'Alloc our vertex data
Global vertexData:TVertexData = TVertexData.Create()
msh.setSharedVertexData(vertexData)
vertexData.managed = True
msh.getSharedVertexData().setVertexCount(nVertices)

'Get our vertex declaration, which will be used in identifying data inside the buffers
Global decl:TVertexDeclaration = msh.getSharedVertexData().GetVertexDeclaration()
Global offset:Int = 0

'*** Populate vertex/normal buffer ***

'Add declaration elements to tell that we have 3 floats for a position, then 3 floats for a normal
decl.addElement(0, offset, TVertexElementType.VET_FLOAT3, TVertexElementSemantic.VES_POSITION)
offset = offset + TVertexElement.getTypeSize(TVertexElementType.VET_FLOAT3)

decl.addElement(0, offset, TVertexElementType.VET_FLOAT3, TVertexElementSemantic.VES_NORMAL)
offset = offset + TVertexElement.getTypeSize(TVertexElementType.VET_FLOAT3)


'Create our hardware buffer and write to it
Global vbuf:THardwareVertexBuffer = THardwareBufferManager.createVertexBuffer(offset, msh.getSharedVertexData().getVertexCount() , THardwareBuffer.HBU_STATIC_WRITE_ONLY)
vbuf.writeDataWithFloatList(0, vbuf.getSizeInBytes() , verts, True)

'Bind the buffer
Global bind:TVertexBufferBinding = msh.getSharedVertexData().GetVertexBufferBinding()
bind.setBinding(0, vbuf)

'*** Populate UV buffer

offset = 0

'Add declaration elements to tell that we have 2 floats for a uv coordinate
decl.addElement(1, offset, TVertexElementType.VET_FLOAT2, TVertexElementSemantic.VES_TEXTURE_COORDINATES)
offset = offset + TVertexElement.getTypeSize(TVertexElementType.VET_FLOAT2)

'Create buffer and write to it
Global uvbuf:THardwareVertexBuffer = THardwareBufferManager.createVertexBuffer(offset, msh.getSharedVertexData().getVertexCount() , THardwareBuffer.HBU_STATIC_WRITE_ONLY)
uvbuf.writeDataWithFloatList(0, uvbuf.getSizeInBytes() , uvs, True)

'Bind buffer
bind.setBinding(1, uvbuf)

'*** Setup Indices

'Create an indice buffer that uses 32 bit ( int ) indices and write to it
Global ibuf:THardwareIndexBuffer = THardwareBufferManager.createIndexBuffer(THardwareIndexBuffer.IT_32BIT, ibufCount, THardwareBuffer.HBU_STATIC_WRITE_ONLY)
ibuf.writeDataWithIndexList(0, ibuf.getSizeInBytes() , indices, True)

'Populate indice data
sub.setUseSharedVertices(True)
sub.getIndexData().setIndexBuffer(ibuf)
sub.getIndexData().setIndexCount(ibufCount)
sub.getIndexData().setIndexStart(0)

'Generate tangent vectors for this mesh
msh.buildTangentVectors(TVertexElementSemantic.VES_TANGENT, 0, 0)

'Give mesh some basic bounds for rendering/culling
msh._setBounds(TAxisAlignedBox.Create(- 10, - 10, - 10, 10, 10, 10))
msh._setBoundingSphereRadius(10)

'*** Create our skeleton and setup bones
Global skel:TSkeleton = TSkeletonManager.Create("skeleton", "General")

'Root Bone(0)
Global pRootBone:TBone = skel.createBoneWithNameHandle("Bone01", 0)
pRootBone.resetOrientation()
pRootBone.setOrientation(- 0.5, 0.5, - 0.5, 0.5)
pRootBone.setPosition(0.0509392, 14.8066, - 0.451164)

'Bone(1)	
Global pBone1:TBone = pRootBone.createChild(1)
pBone1.resetOrientation()
pBone1.setOrientation(1, - 0, - 0, - 0)
pBone1.setPosition(10.0898, 0, .0000137091)

'Bone(2)
Global pBone2:TBone = pBone1.createChild(2)
pBone2.setPosition(9.92574, 0, .0000018999) ;
pBone2.resetOrientation() ;
pBone2.setOrientation(0.999992, - 0, 0.00402646, 0) ;

'Attach skeleton
msh._notifySkeleton(skel)
msh.setSkeletonName(skel.getName())

'Bind vertice indexes to skeleton
SetupVertexBindings()

'Set binding pose
skel.setBindingPose()

'Show our mesh
Local ent:TEntity = fG.LoadMesh("mshEnt", msh.getName())
ent.setMaterialName("MeshMaterial")
ent.setDisplaySkeleton(True)

' Set up a single animation that is 10 frames long and linearly interpoalted
Global entSkel:TSkeleton = ent.getSkeleton()
Global skelAnim:TAnimation = entSkel.createAnimation("animation", 10.0)
skelAnim.setInterpolationMode(TAnimation.IM_LINEAR)

'Create the track for transforms
Global pBone2Track:TNodeAnimationTrack = skelAnim.createNodeTrackWithBone(pBone2.GetHandle() , pBone2)

'Create several keyframes with some basic data
Global key:TTransformKeyFrame
key = pBone2Track.createNodeKeyFrame(2.5)
key.setTranslate(Vec3(2.5, - 2.5, 2.5))

key = pBone2Track.createNodeKeyFrame(5.0)
key.setTranslate(Vec3(2.5, 2.5, 2.5))

key = pBone2Track.createNodeKeyFrame(7.5)
key.setTranslate(Vec3(2.5, 2.5, - 2.5))

key = pBone2Track.createNodeKeyFrame(10.0)
key.setTranslate(Vec3(2.5, - 2.5, - 2.5))

'Last, we create an animation state to control our new node animation
Global animState:TAnimationState = ent.getAllAnimationStates().createAnimationState("animation", 0.0, 10.0, 1.0, True)
animState.setEnabled(True)
animState.setLoop(True)

'Timer variables to get the delta time to feed to animState.addTime(...)
Global timer:TOgreTimer = fG._ogre.getTimer()
Global newtime:Float = timer.getTime()
Global oldtime:Float = timer.getTime()
Global delta:Float = 0

'Basic Render Loop
Repeat

	'Calculate the time since last frame
	oldtime = newtime
	newtime = timer.getTime()
	delta = newtime - oldtime
	
	'Add time to the animation state
	animState.addTime(delta)

	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Manual Skeletal Animated Mesh - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
		
	'Render at (very) roughly 60fps
	fG.renderWorld()
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()

Function SetupGeometryLists()

	' Reserve for performance reasons
	verts.reserve(16 * 2)
	uvs.reserve(16)
	indices.reserve(16)
	
	' 0
	verts.addVertex(  3.35208 ,-5.33884 ,2.20858 )
	verts.addNormal( -3.54431e-008 ,-0.115857 ,0.993266 )
	uvs.addUV( 0.336599 ,0.980516 )

	' 1
	verts.addVertex( -3.32942 ,-5.33884 ,2.20858 )
	verts.addNormal( -2.79874e-008 ,-0.115857 ,0.993266 )
	uvs.addUV( 0.336599 ,0.763429 )

	' 2
	verts.addVertex(  0.0113291 ,-15.393 ,-0.477235 )
	verts.addNormal( -3.44746e-008 ,-0.258085 ,0.966122 )
	uvs.addUV( 0.00993206 ,0.871973 )                  

	' 3
	verts.addVertex( 3.35207 ,4.71529 ,2.20858 )
	verts.addNormal( -7.45576e-009 ,0.115857 ,0.993266 )
	uvs.addUV( 0.663265 ,0.980516 )

	' 4
	verts.addVertex( -3.32942 ,4.71529 ,2.20858 )
	verts.addNormal( 0 ,0.115857 ,0.993266 )
	uvs.addUV( 0.663265 ,0.763429 )

	' 5
	verts.addVertex( 0.0113291 ,14.7694 ,-0.477235 )
	verts.addNormal( 0 ,0.258085 ,0.966122 )
	uvs.addUV( 0.989932 ,0.871973 )

	' 6
	verts.addVertex( 3.35208 ,-5.33884 ,-3.16305 )
	verts.addNormal( 0.699698 ,-0.183787 ,-0.690395 )
	uvs.addUV( 0.335744 ,0.182162 )

	' 7
	verts.addVertex( 3.35208 ,-5.33884 ,2.20858 )
	verts.addNormal( 0.989286 ,-0.145993 ,2.82088e-008 )
	uvs.addUV( 0.336667 ,0.00750566 )

	' 8
	verts.addVertex( 0.0113291 ,-15.393 ,-0.477235 )
	verts.addNormal( 3.89835e-008 ,-0.619354 ,-0.785112 )
	uvs.addUV( 0.00903652 ,0.290792 )

	' 9
	verts.addVertex( 3.35208 ,4.71529 ,-3.16305 )
	verts.addNormal( 0.699698 ,0.183787 ,-0.690395 )
	uvs.addUV( 0.66241 ,0.182162 )

	' 10
	verts.addVertex( 3.35207 ,4.71529 ,2.20858 )
	verts.addNormal( 0.989286 ,0.145993 ,1.00778e-007 )
	uvs.addUV( 0.663333 ,0.00750566 )				

	' 11
	verts.addVertex( 0.0113291 ,14.7694 ,-0.477235 )
	verts.addNormal( -7.7967e-008 ,0.619354 ,-0.785112 )
	uvs.addUV( 0.985246 ,0.292056 )

	' 12
	verts.addVertex( -3.32942 ,-5.33884 ,-3.16305 )
	verts.addNormal( -0.699698 ,-0.183787 ,-0.690395 )
	uvs.addUV( 0.335221 ,0.399443 )

	' 13
	verts.addVertex( -3.32942 ,4.71529 ,-3.16305 )
	verts.addNormal( -0.699698 ,0.183787 ,-0.690395 )
	uvs.addUV( 0.661888 ,0.399443 )

	' 14
	verts.addVertex( -3.32942 ,-5.33884 ,2.20858 )
	verts.addNormal( -0.989286 ,-0.145993 ,4.39092e-008 )
	uvs.addUV( 0.335622 ,0.574038 )

	' 15
	verts.addVertex( -3.32942 ,4.71529 ,2.20858 )
	verts.addNormal( -0.989286 ,0.145993 ,-2.86602e-008 )
	uvs.addUV( 0.662288 ,0.574038 )


	' Indices
	indices.addTriangle( 0 ,1 ,2 )
	indices.addTriangle( 1 ,0 ,3 )
	indices.addTriangle( 3 ,4 ,1 )
	indices.addTriangle( 4 ,3 ,5 )
	indices.addTriangle( 6 ,7 ,8 )
	indices.addTriangle( 7 ,6 ,9 )
	indices.addTriangle( 9 ,10 ,7 )
	indices.addTriangle( 10 ,9 ,11 )
	indices.addTriangle( 12 ,6 ,8 )
	indices.addTriangle( 6 ,12 ,13 )
	indices.addTriangle( 13 ,9 ,6 )
	indices.addTriangle( 9 ,13 ,11 )
	indices.addTriangle( 14 ,12 ,8 )
	indices.addTriangle( 12 ,14 ,15 )
	indices.addTriangle( 15 ,13 ,12 )
	indices.addTriangle( 13 ,15 ,11 )	


End Function

'Bind vertices to skeleton
Function SetupVertexBindings()

	Local vba:TVertexBoneAssignment  = TvertexBoneAssignment.Create()

	vba.setVertexIndex(0) ;vba.setBoneIndex(1) ;vba.setWeight(0.499065) ;msh.addBoneAssignment(vba) ;
	vba.setVertexIndex(0) ;vba.setBoneIndex(2) ;vba.setWeight(0.500935) ;msh.addBoneAssignment(vba) ;
	vba.setVertexIndex( 1);vba.setBoneIndex(  1);vba.setWeight( 0.497731);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 1);vba.setBoneIndex(  2);vba.setWeight( 0.502269);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 2);vba.setBoneIndex(  2);vba.setWeight( 1);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 3);vba.setBoneIndex(  0);vba.setWeight( 0.499999);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 3);vba.setBoneIndex(  1);vba.setWeight( 0.500001);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 4);vba.setBoneIndex(  0);vba.setWeight( 0.500001);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 4);vba.setBoneIndex(  1);vba.setWeight( 0.499999);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 5);vba.setBoneIndex(  0);vba.setWeight( 1);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 6);vba.setBoneIndex(  1);vba.setWeight( 0.499041);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 6);vba.setBoneIndex(  2);vba.setWeight( 0.500959);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 7);vba.setBoneIndex(  1);vba.setWeight( 0.499065);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 7);vba.setBoneIndex(  2);vba.setWeight( 0.500935);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 8);vba.setBoneIndex(  2);vba.setWeight( 1);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 9);vba.setBoneIndex(  0);vba.setWeight( 0.499999);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 9);vba.setBoneIndex(  1);vba.setWeight( 0.500001);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 10);vba.setBoneIndex(  0);vba.setWeight( 0.499999);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 10);vba.setBoneIndex(  1);vba.setWeight( 0.500001);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 11);vba.setBoneIndex(  0);vba.setWeight( 1);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 12);vba.setBoneIndex(  1);vba.setWeight( 0.497669);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 12);vba.setBoneIndex(  2);vba.setWeight( 0.502331);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 13);vba.setBoneIndex(  0);vba.setWeight( 0.500001);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 13);vba.setBoneIndex(  1);vba.setWeight( 0.499999);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 14);vba.setBoneIndex(  1);vba.setWeight( 0.497731);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 14);vba.setBoneIndex(  2);vba.setWeight( 0.502269);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 15);vba.setBoneIndex(  0);vba.setWeight( 0.500001);msh.addBoneAssignment(vba);
	vba.setVertexIndex( 15);vba.setBoneIndex(  1);vba.setWeight( 0.499999);msh.addBoneAssignment(vba);

End Function

