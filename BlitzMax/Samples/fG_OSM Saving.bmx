REM----------------------------------------------------
fG_OSM Saving.bmx
Author: Damien Sturdy and Lina Adkins

A demo application that covers using the fG.saveOSM(...) function, as well as saving selectively using the
callbacks object. Alongside this, we will also cover adding user properties to objects thru the callbacks,
which can then be picked up when the scene is loaded in.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
AppTitle = "fG - OSM Saving with Callbacks"
fG.init()


'Create a pair of objects-
Global Sphere:TEntity = fG.CreateSphere("Sphere") 
fG.PositionEntity(Sphere, - 3, 0, 0)
Sphere.setMaterialName("Red") 

Global Cube:TEntity = fG.createCube("Cube") 
fG.PositionEntity(Cube, 3, 0, 0)
Cube.setMaterialName("Blue") 

'Setup our callback object-
Global Callbacks:TOSMSerializerCallbacks = TOSMSerializerCallbacks.Create() 

'Here we specify each function in the correct order which will point to our blitz functions below.
'What this will do is, each time the serializer chooses an object to save, it will call the function
'for the corresponding type, giving you full control over what it does.
Callbacks.initialize(OnWriteNodeCallback, OnWriteMeshCallback, OnWriteCamCallback, OnWriteEntityCallback, OnWriteLightCallback, OnWriteSkeletonCallback, OnWriteHelperCallback) 

'Save the OSM-
fG.SaveOSM("Scene.OSM", Callbacks) 
	

'Basic Render Loop
Repeat
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()



'Serializer Callbacks--------------
'Don't let the large amount of code here seem intimidating, you will likely only need
'a single set of serializer callbacks, and the amount of code comes from the general
'flexibility afforded by the callbacks. A detailed description of each callback is available below.
'There are are two general rules for callbacks:
'	o Callback functions take a Byte Ptr in, and in the function the Byte Ptr is cast to the correct object.
'	o You tell the callback if you want to save an object by returning True. If not you return false.


'Summary: If you have a Node object in your OSM.
Function OnWriteNodeCallback:Byte(s:Byte Ptr) 
	Local sn:TSceneNode = TSceneNode.FromPtr(s) 
	Return False
End Function

'Summary: If you wish to also save out your .mesh files, you would Return True here.
Function OnWriteMeshCallback:Byte(m:Byte Ptr) 
	Local mesh:TMesh = TMesh.FromPtr(m, True)
	'Don't want to save any meshes-
	Return False
End Function

'Summary: This function is called for every TCamera instance.
'We have decided here to save out every camera in the scene, and if
'we find our default fG Camera, add a user property to it.
Function OnWriteCamCallback:Byte(c:Byte Ptr) 
	Local cam:TCamera = TCamera.FromPtr(c) 
	
	'Specify main camera
	If cam.getName() = "_camera" Then
		Callbacks.addProperty("CameraType", "MainCam") 
	End If
	
	Return True
End Function

'Summary: This function is called for every TEntity instance.
'Here we are going to save a "Colour" user property specific to each object.
'We are also going to let the user decide via a Confirm dialog.
Function OnWriteEntityCallback:Byte(e:Byte Ptr) 
	Local entity:TEntity = TEntity.FromPtr(e)
	
	'First let's decide if we're even going to save this entity-
	Local OK:Byte = Confirm("Would you like to save Entity " + entity.getName() + "?")
	If OK <> True Then Return False
	
	'We are going to save, so add the colour attribute property-
	Select entity.getName()
		Case "Sphere"
			Callbacks.addProperty("Colour", "Green") 
		Case "Cube"
			Callbacks.addProperty("Colour", "Blue")
	End Select
	
	Return True
End Function

'Summary: This function is called for every TLight instance.
'Nothing complicated here, we will simply save out all the lights with no user properties.
Function OnWriteLightCallback:Byte(l:Byte Ptr) 
	Local light:TLight = TLight.FromPtr(l)
	
	'If this is the fG standard light then don't save.
	If light.getName() = "_light" Then
		Return False
	End If
	
	Return True
End Function

'Summary: This function is called for every TSkeleton instance.
'Nothing complicated here, we will simply avoid saving all .skeleton data/files.
Function OnWriteSkeletonCallback:Byte(s:Byte Ptr) 
	Local newskeleton:TSkeleton = TSkeleton.FromPtr(s)
	Return False
End Function

'Summary: Helpers are basically scene nodes that hold extra information or anchor points.
'Here we are not saving helpers to the osm file.
Function OnWriteHelperCallback:Byte(h:Byte Ptr) 
	Local helper:TSceneNode = TSceneNode.FromPtr(h)
	Return False
End Function