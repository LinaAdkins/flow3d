REM----------------------------------------------------
fG_StaticGeometry.bmx
Author: Damien Sturdy and Lina Adkins

This demo application shows you how To use static geometry.Static geometry is batched geometry that uses regions of 
batches oftriangles to speed up rendering by reducing api call overhead. 
In this demo we will generate a sizeable amount of robots and swap between single entities and static geometry.

Press SPACE to toggle between normal geometry and static geometry.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

'Import Modules
Import flow.main
EnablePolledInput()

'Init fG
AppTitle = "fG - Static Geometry Demo [Press ESC to Exit]"
fG.Init()

'Quick plane as part of the standard fG stage
Global plane:TEntity = fG.createPlane("Plane")
plane.setMaterialName("tile1")

'Our global static geometry instance-
Global batch:TStaticGeometry = fG._scene.createStaticGeometry("batch") 

'Create the robots initially-
createRobots() 

'Flag for letting our app know whether we are in static geometry mode or entity mode-
Global usingStaticGeometry:Byte = False

Repeat

	'Press space to switch from static to individual entities
	'This is the ONLY part of the demo where we actually work with static geometry.
	If KeyDown(KEY_SPACE) 
		'Toggling Static Geometry OFF
		If usingStaticGeometry = True Then
			'Destroy the geometry and the queued entities/scene nodes-
			batch.Reset() 
			'Create individual entities-
			createRobots() 
			'Toggle Flag and wait for keyup-
			usingStaticGeometry = False
			Repeat
			Until KeyDown(KEY_SPACE) = False
			
		Else
			'This is the primary static geometry creation function for the demo.
			'Here we take a scene node and the entire subtree below has it's entities
			'queued to be built into a static object.
			batch.addSceneNode(fG._scene.getSceneNode("Robots")) 
			'Now we build the static object, it will be visible after this step unless you have used yourstaticgeom.setVisible(False).
			batch.build() 
			'Destroy all entities and scene nodes associated with the robots.
			'Now that we have the static geometry built we need neither the nodes or the entities.
			destroyRobots() 
			'Toggle Flag and wait for keyup-
			usingStaticGeometry = True
			Repeat
			Until KeyDown(KEY_SPACE) = False
		End If
	End If
	
	'Get a string for which mode we are in.
	Global geometryMode:String = ""
	If usingStaticGeometry Then geometryMode = "Static" Else geometryMode = "Normal"
	
	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Static Geometry - Mode: " + geometryMode + " FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Render as quickly as possible
	fG.renderWorld()
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()


'Summary: Generate a 10x10 lattice of robots spaced 35 units apart-
Function createRobots() 
	'Create a root node for the entities if it doesn't exist-
	If fG._scene.hasSceneNode("Robots") = False Then
		fG._rootnode.createChildSceneNode("Robots", 0, 0, 0)
	End If
	
	'Get a handle to the root node and set an index-
	Local robotsNode:TSceneNode = fG._scene.getSceneNode("Robots") 
	Local robotIndex:Int = 1
	
	'Generate a lattice of 10x10 robots with indexed names.
	For Local i:Int = 1 To 10 Step 1
		For Local j:Int = 1 To 10 Step 1
			Local robot:TEntity = fG.loadMesh("Robot" + String(robotIndex) , "walkie.mesh")
			fG._rootnode.removeChild(fG.getNode(robot))
			robotsNode.addChild(fG.getNode(robot))
			fG.PositionEntity(robot, j - 5.5, 0, i - 5.5)
			fG.ScaleEntity(robot, 0.09, 0.09, 0.09)
			robotIndex = robotIndex + 1
		Next
	Next
	
End Function

'Summary: Destroy all the robot scene nodes and entities-
Function destroyRobots() 
	'Make sure we've built the robots before we try to destroy them-

	If fG._scene.hasSceneNode("Robots") = False Then
		Notify "Robots were not created! Exiting!"
		End
	
	'Destroying the Robots-
	Else
	
		'Destroy all 100 entities-
		For Local i:Int = 1 To 100 Step 1
			If fG._scene.hasEntity("Robot" + String(i)) 
				
				'fG._scene.getSceneNode("RobotNode" + String(i)).detachObject(fG._scene.getEntity("Robot" + String(i))) 
				fG._scene.destroyEntityWithName("Robot" + String(i))
			End If
		Next
		'Remove all the child nodes-
		fG._scene.getSceneNode("Robots").removeAndDestroyAllChildren() 
		'Get rid of the root node-
		fG._scene.destroySceneNode("Robots") 
	End If
End Function
