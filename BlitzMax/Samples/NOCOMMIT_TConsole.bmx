REM
** TDebugConsole.bmx for MazeMonkeys
** Description: This file holds the debug console class implementation that allows for easy debug console output.
** Author: Lina Adkins
ENDREM

REM
	bbdoc: TDebugConsole is for holding debug output.
ENDREM
Type TDebugConsole

	REM
		bbdoc: An array of ten text lines that hold debug output.
	ENDREM
	Global debugText:String[] = ["", "", "", "", "", "", "", "", "", ""]

	REM
		bbdoc: Handle to the overlay element we use to write debug lines to.
	ENDREM
	Global ogreOverlayElement:TOverlayElement = Null
	
	REM
		bbdoc: Holds whether this overlay element is visible or not.
	ENDREM
	Global isVisible:Byte = False
	
	REM
		bbdoc: Attach the debug console to a text overlay element.
	ENDREM
	Function init()
		ogreOverlayElement = TOverlayManager.getByName("DebugConsole").GetChild("MainPanel").GetChild("ConsoleText")
	End Function
	
	REM
		bbdoc: Set whether or not the debug console is visible.
	ENDREM
	Function setVisible(visible:Byte)
		If visible Then
			ogreOverlayElement.show()
			isVisible = True
		Else
			ogreOverlayElement.hide()
			isVisible = False
		End If
	End Function
	
	REM
		bbdoc: Set a line in the debug console (0-9).
	ENDREM
	Function setLine(index:Short, line:String)
		debugText[index] = line
		_update()
	End Function
	
	REM
		bbdoc: Push the debug text into the rendered overlay.
	ENDREM
	Function _update()
		
		'Iterate through string array and push them to a single string seperated by ~n
		Local allLines:String
		
		For Local line:String = EachIn debugText
        	allLines = allLines + line
			allLines = allLines + "~n"
		Next
		
		DebugLog( Int( ogreOverlayElement.Pointer) )
		'Set the overlay text
		ogreOverlayElement.setCaption(allLines)
	
	End Function
	
	REM
		bbdoc: Clear all debug text.
	ENDREM
	Function clear()
		For Local line:String = EachIn debugText
        	line = ""
		Next
		
		ogreOverlayElement.setCaption("")
	End Function
	
End Type