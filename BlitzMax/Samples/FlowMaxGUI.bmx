REM----------------------------------------------------
fG_Using With MaxGUI.bmx
Author: Damien Sturdy

This demo will show you how to start a basic application using MaxGUI with fG. You will create  a MaxGUI window and then pass
its handle to fG when you initialise.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

SuperStrict

Import flow.Main
Import flow.ogrenewt
Import maxgui.win32maxguiex

'Create a Window and canvas. Ogre will render onto the canvas.
Global WINDOW:TGADGET = CreateWindow("Flow in MaxGUI", 10, 10, 800, 600)
Global canvas:tgadget = CreateCanvas(0, 0, 800, 600, window)

'Retrieve the canvas' HWnd. We pass this to fG.Init.
Global hwnd:Int = canvas.Query(QUERY_HWND)

'Initialise flow using the Hwnd. Ogre will render to this window.
fG.init(False, 800, 600, 32, False, fG.RS_OPENGL, hwnd)

'Set the focus to the newly setup window
SetFocus(hwnd)

'Load some robot walker legs and scale them down
Global RobotWalker:TEntity = fG.LoadMesh("RobotWalker", "walkie.mesh")
fG.scaleEntity(RobotWalker, 0.5, 0.5, 0.5)

'Throw in a plane for them to stand on and give it the standard unit tile material
Global plane:TEntity = fG.createPlane("Plane")
plane.setMaterialName("tile1")

'Add some quick stencil shadows
fG.enableShadows()

'Variable to tell when the application is done
Global done:Byte = False

While Not done
	'Turn our entity 1 degree along the y-axis every frame ( LOCAL )
	fG.TurnEntity(RobotWalker, 0, 1, 0)
	
	'Deal with the events
	PollEvent()
	Select EventID()
		Case EVENT_KEYDOWN
			If EventData() = KEY_ESCAPE Then done = True
		Case EVENT_WINDOWCLOSE
			done = True
	End Select
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667

Wend

End
