REM----------------------------------------------------
fG_Manual Animated Mesh Creation.bmx
Author: Lina Adkins

This demo will show you how to use TManualObject and TSkeleton/TSkeletonManager to create a manual mesh with bones. This is useful for making
your own mesh loaders and converters. First we create a basic quad using TManualObject, then we convert it to a mesh. After this, we create a skeleton
and attach it to the mesh, then add some bones and bind them according to vertex indices.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

'Pull the camera back
fG.getNode(fG._camera).setPosition(25, 25, 25)
fG._camera.lookAt(0, 0, 0)

'We use a TManualObject initially to create the mesh - here we specify that we want to use a triangle list
Global mo:TManualObject = fG._scene.createManualObject("mo")
mo.begin("PrimaryBlue", TRenderOperation.OT_TRIANGLE_LIST)

'Here we begin plotting triangle positions
mo.position(0, 5, 0)
mo.normal(0, 0, 1)
mo.textureCoord(0, 0)
mo.position(5, 5, 0)
mo.normal(0, 0, 1)
mo.textureCoord(1, 0)
mo.position(0, 0, 0)
mo.normal( 0,0,1)
mo.textureCoord(1, 1)
mo.position(5, 0, 0)
mo.normal(0, 0, 1)
mo.textureCoord(0, 1)

'In order to animate the mesh with bones, we must set indices for each triangle
mo.index(0)
mo.index(2)
mo.index(1)

mo.index(2)
mo.index(3)
mo.index(1)

'Here we end the manual object's creation
mo.ending()

'Now we convert our manual object to a mesh
Global mesh:TMesh = mo.convertToMesh("mesh")

'By default our manual object's triangles are in the mesh's first submesh
'We will ensure that they stay there by not using shared vertices
Global submesh:TSubMesh = mesh.getSubMeshWithIndex(0)
submesh.setUseSharedVertices(False)


'Now we create our skeleton and attach our skeleton to our mesh via _notifySkeleton(...)
Global skel:TSkeleton = TSkeletonManager.Create("skeleton", "General")
mesh._notifySkeleton(skel)
mesh.setSkeletonName("skeleton")

'Create a root bone at the origin of the skeleton
Global rootBone:TBone = skel.createBoneWithHandle(0)
rootBone.resetOrientation()
rootBone.setPosition(0, 0, 0)

'Create three bones to bind to the quad
Global bone1:TBone = rootBone.createChild(1, Vec3(- 10, - 10, 0))
Global bone2:TBone = rootBone.createChild(2, Vec3(10, - 10, 0))
Global bone3:TBone = rootBone.createChild(3, Vec3(0, 10, 0))

'We use TVertexBoneAssignment and addBoneAssignment(...) with our mesh's triangle indices to
'bind the skeleton to the mesh.
Global vba:TVertexBoneAssignment = TVertexBoneAssignment.Create()
vba.setWeight(1.0)

'Bind the root bone to indice 0.
vba.setBoneIndex(0) ; vba.setVertexIndex(0) ;
submesh.addBoneAssignment(vba)

'Bind bone1 to indice 1.
vba.setBoneIndex(1) ; vba.setVertexIndex(1) ;
submesh.addBoneAssignment(vba)

'Etc.
vba.setBoneIndex(2) ; vba.setVertexIndex(2) ;
submesh.addBoneAssignment(vba)

vba.setBoneIndex(3) ; vba.setVertexIndex(3) ;
submesh.addBoneAssignment(vba)


'Set the current 'pose' as the default binding pose
skel.setBindingPose()

'Set this mesh's aabb and load the mesh
mesh._setBounds(TAxisAlignedBox.Create(- 10.0, - 10.0, - 10.0, 10.0, 10.0, 10.0))
mesh.Load()

Local sourcecoordset:Int = 0
Local index:Int = 0
If mesh.suggestTangentVectorBuildParams(TVertexElementSemantic.VES_TANGENT, sourcecoordset, index) = False Then
	mesh.buildTangentVectors(TVertexElementSemantic.VES_TANGENT, sourcecoordset, index)
End If

'Load our newly crated mesh into the scene.
Global manualentity:TEntity = fG.LoadMesh("manualmesh", mesh.getName())
manualentity.setMaterialName("tile1")

'Show bone positions
manualentity.setDisplaySkeleton(True)

fG.SaveOSM()

'Basic Render Loop
Repeat

	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Manual Animated Mesh Creation - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()