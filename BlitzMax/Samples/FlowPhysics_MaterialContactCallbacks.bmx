REM----------------------------------------------------
FlowPhysics_MaterialContactCallbacks.bmx
Author: Damien Sturdy and Lina Adkins

This demo will show you how to use material contact callbacks. That is, you will learn how to process collisions
between a specific group of bodies against another specific group of bodies. The demo will create a slanted plane, 
and a sphere. The sphere will then collide with the plane and roll off.
When a significant collision occurs, a red sphere will be placed at the point of the collision.
If we collide with the floor at a significant speed, the floor's texture will also be changed.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

'Import modules
Import flow.main
Import flow.ogrenewt
EnablePolledInput()

'Init Flow3D and Newton Physics
fG.init()
fG.enableShadows()
fP.enablePhysics()	
		
			 
'Sphere---

Global Sphere:TEntity = fG.CreateSphere("Sphere")
SeedRnd(MilliSecs())
fG.PositionEntity(Sphere, Rnd(- 5, 5), 5, Rnd(- 5, 5))
Global SphereBody:TBody = fP.addPhysics(Sphere, False, 25)

'Set Sphere Material
Global SphereMaterial:TMaterialID = TMaterialID.Create(fP.getWorld())
SphereBody.setMaterialGroupID(SphereMaterial)

'Floor---

'Create Floor Physics Object
Global Floor:TEntity = fG.createPlane("Floor")
fG.getNode(Floor).rotate(TVector3.UNIT_Y() , 25)
Floor.setMaterialName("tile1")
Global FloorBody:TBody = fP.addPhysics(Floor, True)

'Set Floor Material
Global FloorMaterial:TMaterialID = TMaterialID.Create(fP.getWorld())
FloorBody.setMaterialGroupID(FloorMaterial)
TFBCallback.floorID = FloorMaterial.getID()

'Set up the interaction between the two materials.
'The callback is included here, see below for how the callback type is setup.
Global FloorBallPair:TMaterialPair = TMaterialPair.Create(fP.getWorld(), FloorMaterial, SphereMaterial)
Global FloorBallCallback:TFBCallback = TFBCallback.Create()
FloorBallPair.setContactCallback(FloorBallCallback)


'Sphere for showing where the collision contact was.
Global ContactSphere:TEntity = fG.CreateSphere("ContactSphere", .3)
ContactSphere.setMaterialName("Red")

'Main Loop
Repeat
	fG.changeAppTitle("fP - Collision Callbacks - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	fP.update()
	fG.renderWorld()
Until KeyDown(KEY_ESCAPE) Or fG.AppTerminated()

		
'Summary: You create a callback by extending TContactCallback, then you instantiate it and call
'yourmaterialpair.setContactCallback(...). The different components of the callback are described
'below.
Type TFBCallback Extends TContactCallback
	
	'Point the collision occured in world space.
	Global contactPoint:TVector3 = TVector3.CreateEmpty()
	
	'The collision normal.
	Global contactNormal:TVector3 = TVector3.createEmpty()
	
	'The floorID(Integer) that was populated earlier in the code. For testing for hitting the floor.
	Global floorID:Int = 0
	
	'This is the standard constructor. You will always have to include it like this due to BlitzMax OOP.
	Function Create:TFBCallback()
		Local Obj:TFBCallback = New TFBCallback
		TMaterialPair.SetupCallbacks(obj, userbegin, userprocess)
		Return Obj
	End Function

	'This function will be called when the two materials are within each other's aabbs.
	'We use this here only for resetting the collision speed.
	'Note that you may return 0 and choose not to collide with the other body at this stage.
	Function userBegin:Int(threadIndex:Int)
		Return 1
	End Function
	
	'This is called when the objects have collided.
	'We use this to populate our collision speeed, contactPoint and contactNormal variables.
	'Note that you may return 0 and choose not to collide with the other body at this stage.
	Function userProcess(contactIterator:Byte Ptr, timeStep:Float, threadIndex:Int)
		Local ci:TContactIterator = TContactIterator.FromPtr(contactIterator)
		Local currentContact:TContact = ci.getCurrent()
		
		'If we have a high contact speed then get the position and move the contact ball to the position 
		If currentContact.getContactNormalSpeed() > 5 Then
			currentContact.getContactPositionAndNormal(contactPoint, contactNormal)
			fG.getNode(ContactSphere).setPositionWithVector3(contactPoint)
			
			'Get the body collided with, check to see if it's the floor, if it is, change the texutre.
			Local body1:TBody = FloorBallCallback.getBody1()
			If body1.Pointer <> Null Then
				If body1.getMaterialGroupID().getID() = floorID Then
					Floor.setMaterialName("ground")
				End If
			End If
			
		End If
		
	End Function
	
End Type
