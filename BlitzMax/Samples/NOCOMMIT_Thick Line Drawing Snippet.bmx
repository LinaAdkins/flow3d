REM----------------------------------------------------
fG_Thick Line Drawing Snippet.bmx
Author: Lina Adkins

This is an ongoing snipped to attempt to efficiently approximate line thickness using regular lines so that it can be
eventually placed in TLineManager. For now it only displays a perpendicular set of lines.The perpendicular line needs
to be setup so that points on it can be calculated according to thickness, thus giving us the points for a "thick"
line made out of triangles.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

fG._viewport.setBackgroundColour(TColourValue.Create(0, 0, 0, 0))

'Draw a 2d box at the center of the screen-
Global lm:TLineManager = TLineManager.Create(fG._scene)

'http://www.gamedev.net/community/forums/topic.asp?topic_id=488998
'http://www.purplemath.com/modules/distform.htm

Global x1:Float = 0.0; Global y1:Float = 0.0;
Global x2:Float = 0.1; Global y2:Float = 0.1;

Global rise:Float = y2 - y1;
Global run:Float = x2 - x1;

Global prise:Float = -run
Global prun:Float = rise



lm.Draw2DLine(x1, y1, x2, y2)
lm.Draw2DLine(x1 - prun, y1 - prise, prun, prise)


'Basic Render Loop
Repeat

	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Getting Started - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Turn our entity 1 degree along the y-axis every frame ( LOCAL )
'	fG.TurnEntity(RobotWalker, 0, 1, 0)
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()
