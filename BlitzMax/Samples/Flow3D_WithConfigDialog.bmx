REM----------------------------------------------------
Flow3D_WithConfigDialog.bmx
Author: Damien Sturdy and Lina Adkins

This sample shows you how to initialise Ogre inside a Graphics(...) window manually without fG.
When the application starts it will show the standard Ogre config dialog and allow the user to choose their rendering
settings.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

AppTitle = "Flow3D - With Config Dialog"

'Initializing Ogre
	'Init the ogre instance and create a Graphics Window
	Global Ogre:Togre = Togre.Create("Conf/Plugins.cfg", "Conf/ogre.cfg", "Conf/log.txt") 
	If Ogre.ShowConfigDialog() = False Then End
	TResourceGroupManager.LoadResourceFile("Conf/Resources.cfg") 
	Ogre.initialise(False) 
	Graphics Ogre.getWidth(), Ogre.getHeight() 

	
	'TRenderWindow Setup - Attach the Ogre renderer to the graphics window
	Global opts:TNameValuePairList = New TNameValuePairList.Create() 
	opts.insert("externalWindowHandle", String(GetActiveWindow())) 
	Global mainRenderWindow:TRenderWindow = ogre.createRenderWindow("mainWindow", ogre.getWidth(), ogre.getHeight(), ogre.getFullScreen(), opts) 
	
	'Initialize resources - Do this after the render window is created in case materials require shaders, etc.
	TResourceGroupManager.InitialiseAllResourceGroups() 
 
	'Scene Manager
	Global mainSceneManager:TSceneManager = ogre.createSceneManager(ST_Generic, "MainScene") 
	'mainSceneManager.setShadowTechnique(SHADOWTYPE_STENCIL_MODULATIVE) 

	'Main Camera Setup
	Global mainCamera:TCamera = mainSceneManager.createCamera("mainCamera") 
	mainCamera.setPosition(100.0, 200.0, 100.0) 
	mainCamera.setNearClipDistance(0.1) 
	mainCamera.setAspectRatio(1.0) 
	mainCamera.lookat(0, 0, 0) 
	
 	'Viewport Setup
	Global mainViewport:TViewport = mainRenderWindow.addViewPort(mainCamera) 
	mainViewport.setBackgroundColour(TColourValue.Create(0.344, 0.0, 0.0, 1.0)) 
'/Initializing Ogre

'Creating an Entity
	'Get the Root Scene node for extending from
	Global RootNode:TSceneNode = mainSceneManager.getRootSceneNode() 
	
	'Add lighting to our scene
	Global Light:Tlight = mainSceneManager.createLight("mainLght") 
	Light.setPosition(50, 150, 50) 
	
	'Create our Robot and attach it to the TSceneNode
	Global Robot:tentity = MainSceneManager.createEntity("ROBOT", "robot.mesh") 
 	Global RobotNode:TSceneNode = RootNode.CreateChildSceneNode("Robotnode", 0, 0, 0) 
	RobotNode.Attachobject(Robot) 
'/Creating an Entity
	

'Main Loop
Repeat
	'Rotate our robot a little every 1/60th of a frame-
	RobotNode.rotate(TVector3.UNIT_Y() , 1)
	Ogre.renderOneFrame() 
	Delay 16.6666667
Until KeyDown(KEY_ESCAPE) Or AppTerminate()