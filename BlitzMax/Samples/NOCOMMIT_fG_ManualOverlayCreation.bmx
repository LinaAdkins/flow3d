REM----------------------------------------------------
fG_ManualOverlayCreation.bmx
Author: Damien Sturdy and Lina Adkins

This sample will show you how to create overlays procedurally through code.

©2009 BoxSnap Studios
Note: This example is a skeleton and will be filled in once all overlay types have been completed.
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

'Code Here-

'Main Loop
Repeat

	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.AppTerminated()

