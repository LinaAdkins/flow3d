REM----------------------------------------------------
FlowPhysics_CustomCallbacks.bmx
Author: Damien Sturdy and Lina Adkins

This demo will show you how to use custom callbacks in order to control the behavior of your rigid bodies more
precisely. You can do simple things, like add a stronger or lighter gravity force - or complicated things like 
adding limits, reactions or control schemes.

Use WASD to control force and torque on the sphere.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

'Import modules
Import flow.main
Import flow.ogrenewt
EnablePolledInput()

'Init Flow3D and Newton Physics
fG.init()
fG.enableShadows()
fP.enablePhysics()

'Create the ground, add texture and physics
groundPlane:TEntity = fG.createPlane("groundPlane")
groundPlane.setMaterialName("tile1")
fP.addPhysics(groundPlane, True)

'Create the Sphere
Sphere:TEntity = fG.createSphere("Sphere")
Sphere.setMaterialName("ground")
fG.TranslateEntity(Sphere, 0, 2, 0)

'As we add physics to the Sphere, this time we specify the mass as well as the location of the callback function.
'The callback function is defined below.
fP.addPhysics(Sphere, False, 50, SphereCallback)

'Callback Variables
Global forceX:TVector3 = TVector3.createEmpty()
Global torqueX:TVector3 = TVector3.createEmpty()

'2d Movement Vectors for capturing direction from keypresses-
Global Slide:Int = 0
Global Roll:Int = 0
		
	
'Main Loop
Repeat
	'Update our AppTitle
	fG.changeAppTitle( "fP - Getting Started - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered() )
	
	'Determine our WASD movement by using two integers which serve as 2D vectors, one for X and one for Y.
	'We push the Sphere if D or A are pressed. We roll the Sphere if W or S are pressed.
	Slide = KeyDown(KEY_D) - KeyDown(KEY_A) 
	Roll = KeyDown(KEY_W) - KeyDown(KEY_S) 
	
	'Set our force and torque variables based on keypresses.
	forceX.setX(Slide * -100)
	torqueX.setX(Roll * 75)

	'Update physics and rendering.
	fP.update()
	fG.renderWorld()

Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()

'Summary: This is the callback we use for the Sphere. It utilizes some global variables to allow us
'to manipulate force and torque by keypresses as seen above. Note that you must always cast the
'pointer in the callback as shown below in order to utilize the callback.
Function SphereCallback(b:Byte Ptr)
	
	'The callback takes a pointer, so we have to cast it to a body to use it.
	Local body:TBody = TBody.FromPtr(b) 
	
	'Add gravity-
	body.addForce(body.calculateGravity()) 
	
	'Add force to push the Sphere side to side-
	body.addForce(forceX) 
	
	'Apply torque if omega along the x is in a certain limit. This helps us limit our rotation speed.
	'This is also a good example of how logic works in custom callbacks.
	Local omegaX:Float = body.getOmega().getX()
	If omegaX < 8 And omegaX > - 8
		body.addTorque(torqueX)
	End If
	

End Function

