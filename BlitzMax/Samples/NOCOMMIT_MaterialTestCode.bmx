REM----------------------------------------------------
fG_GettingStarted.bmx
Author: Damien Sturdy and Lina Adkins

This demo will show you how to get started with initialising Flow3D and fG and start buidling your 3d applications as
quickly as possible. Note that with fG init you can use the config window, or specify several options. You don't HAVE 
to use the config window and you should have a look at the fG.init docs to see what you can do with it.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init(False, 800, 600, 16, 0, fG.RS_D3D)

'Load up the scene
Global RobotWalker:TEntity = fG.LoadMesh("RobotWalker", "walkie.mesh")
fG.scaleEntity(RobotWalker, 0.5, 0.5, 0.5)
Global plane:TEntity = fG.createPlane("Plane")
plane.setMaterialName("tile1")

'Add some quick stencil shadows
fG.enableShadows()

Global img:TOgreImage = TOgreImage.Create()
img.Load("tex1.dds", "General")

Global tex:TTexture = TTextureManager.LoadImage("clouds", "General", img)

Global mat:TMaterial = TMaterialManager.Create("cloudMat", "General")
Global pass:TPass = mat.getTechniqueWithIndex(0).getPassWithIndex(0)
pass.setSceneBlending(TSceneBlendType.SBT_REPLACE)
pass.setAlphaRejectSettings(CMPF_GREATER_EQUAL, 128)
pass.setCullingMode(CULL_NONE)
pass.setManualCullingMode(MANUAL_CULL_NONE)
pass.createTextureUnitState(tex.getName())
plane.setMaterialName("cloudMat")


'Basic Render Loop
Repeat

	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Getting Started - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Turn our entity 1 degree along the y-axis every frame ( LOCAL )
	'fG.TurnEntity(ep, 0, 1, 0)
		
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
	'fG.Cls()
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()

TTextManager.uninit()
