REM----------------------------------------------------
fG_UsingCompositors.bmx
Author: Lina Adkins

This demo will show you how to load and unload compositors using fG. To control what compositor is used, press the number key corresponding to the 
compositorArray variable below! Note that if there is no corresponding compositor for the key, no compositor will load.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Array of compositors, each element in the array corresponds to which key needs to be pressed to enable it!
Global compositorArray:String[] = ["Glass", "Bloom2", "Posterize", "OldMovie", "Tiling", "Invert", "HeatVision", "KEY_8", "KEY_9"]

'Keeps track of compositor states ( on/off )
Global compositorStates:Byte[9]

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

'Load some robot walker legs and scale them down
Global RobotWalker:TEntity = fG.LoadMesh("RobotWalker", "walkie.mesh")
fG.scaleEntity(RobotWalker, 0.5, 0.5, 0.5)


'Throw in a plane for them to stand on and give it the standard unit tile material
Global plane:TEntity = fG.createPlane("Plane")
plane.setMaterialName("tile1")

'Add some quick stencil shadows
fG.enableShadows()

'Get our overlay setup
'Get the overlay specified in the script, then get the main panel it uses.
Global HUD:TOverlay = TOverlayManager.getByName("SingleOverlay")
Global HUDMainPanel:TOverlayContainer = HUD.GetChild("SingleOverlayPanel")

'Get the first and second text element lines in the Hud Main Panel.
Global TextArea:TOverlayElement = HudMainPanel.GetChild("SingleTextArea")

HUD.show()

UpdateHUD()

'Basic Render Loop
Repeat

	Local currentKey:Int = GetChar()
	
	'If our key was a valid number key (1-9), enable or disable the proper compositor!
	If currentKey > 48 And currentKey < 58 Then
		compositorName:String = compositorArray[currentKey - 49]
		compositorState:Byte = compositorStates[currentKey - 49]
		
		'If our compositor is enabled, disable it.
		If compositorState = True Then
			fG.enableCompositor(False, compositorName)
			compositorStates[currentKey - 49] = False
			 DebugLog(compositorName + " disabled.")
			 
		'If our compositor is disabled, enable it.
		Else If compositorState = False Then
			fG.enableCompositor(True, compositorName)
			compositorStates[currentKey - 49] = True
			DebugLog(compositorName + " enabled.")
		End If
		
		'Update HUD
		updateHUD()
	End If


	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Getting Started - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Turn our entity 1 degree along the y-axis every frame ( LOCAL )
	fG.TurnEntity(RobotWalker, 0, 1, 0)
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()

Function updateHUD()
	Local text:String = "Overlay Demo : Press the corresponding key to enable~nor disable an overlay.~n~n"
	
	Local i:Int = 0
	
	While(i < 9)
	 	Local boolToText:String
		If compositorStates[i] = True Then boolToText = "enabled." Else boolToText = "disabled."
	
		text = text + "Key[" + String(i + 1) + "] : " + compositorArray[i] + " " + boolToText + "~n"
		i = i + 1
		
	Wend
	
	TextArea.setCaption(text)
End Function
