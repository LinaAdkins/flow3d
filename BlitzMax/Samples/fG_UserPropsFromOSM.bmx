REM----------------------------------------------------
fG_UserPropsFromOSM.bmx
Author: Damien Sturdy and Lina Adkins

This is a small demo demonstrating two key features. One is the ability to use and integrate TUserDefinedObject objects 
into your TMovableObjects. The second is how to extract and use the Ofusion user properties after an OSM has loaded.
With these tools you will be able to have a more fluid content pipeline, as well as easy access to custom objects
from your TMovableObjects.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

'Load the OSM containiner user properties
Global scene:TOSMScene = fG.LoadOSM("userDEF.OSM") 


'Iterate through all MOs in the list and print out their propserties-
Notify("Press OK when you are ready to print the User Properties lists!") 
Print("START LISTS") 
Print("ENTITIES----------------") 
PrintPropertiesFromList(scene.entitylist) 
Print("LIGHTS------------------") 
PrintPropertiesFromList(scene.lightlist) 
Print("CAMERAS-----------------") 
PrintPropertiesFromList(scene.cameralist) 
Print("END LISTS") 

'This section of code covers getting, setting and manipulating items in TUserDefinedObject.
'First we will create an entity and then a TUserDefined object, and couple them. Remeber that
'if CubeProps below is destroyed at runtime, the reference stored by setUserObject will
'become invalid. For practical usage you should keep your properties in a list.

Global Cube:TEntity = fG.createCube("Cube") 
Global CubeProps:TUserDefinedObject = TUserDefinedObject.Create() 
Cube.setUserObject(CubeProps) 

'At the bottom of this source file we have defined TGameData. TGameData is our container class
'that stores all extended information into a single object. We will fill it with some values, say
'hitpoints and a TCamera to be recalled later.


'Dummy Data
Global CubeData:TGameData = New TGameData
CubeData.hitPoints = 500
CubeData.Camera = fG._scene.getCamera("Camera01") 

'Inject the Game Data
CubeProps.setCustomObject(CubeData) 

'Retreiving Objects: 
Global rCube:TEntity = fG._scene.getEntity("Cube") 
Global rUserObject:TUserDefinedObject = rCube.getUserObject() 
'You should always check to see if the user object has returned null-
If rUserObject.isNull() = False Then
	'Here we retreive the Game Data, casting the results of getCustomObject to TGameData-
	Global rCubeData:TGameData = TGameData(rUserObject.getCustomObject()) 
	
	'Now we print out the Game Data info to simulate usage-
	Notify("Press OK when you are ready to print the Game Data information!") 
	Print("GAME DATA------------------") 
	Print("hitPoints: " + rCubeData.hitPoints) 
	Print("Camera: " + rCubeData.Camera.getName()) 
	Print("END GAME DATA--------------") 
End If

'After you're done with your user object, you should destroy it. To keep track
'of them you should use a list in real-world applications.
'Note: Even though we are using rUserObject, it still points to the same instance-
rUserObject.Destroy() 


'Main Loop
Repeat
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()



'Summary: Prints the properties to the console from a list. Please note that the list must contain movable objects.
Function PrintPropertiesFromList(list:TList) 

	'First we start iterating through the list-
	For Local o:TMovableObject = EachIn list
		
		'Get the userobject-
		Local userobj:TUserDefinedObject = o.getUserObject() 

		'If user object is not null then get the OSM property list-
		If userobj.isNull() = False Then
		
			'Get the OSM list and make sure it is not null-
			Local osmlist:TNameValuePairList = userobj.getOSMPropertyList() 
			If osmlist.isNull() = False Then
				
				'Print header-
				Print("Object Properties -- " + o.getName() + "---") 
				
				'Loop through the OSM Property list and get the key and value pairs-
				Local i:TNameValuePairListIter = osmlist.begin() 
				Repeat
					Print("[ " + i.first() + " : " + i.second() + "]") 
					i.increment() 
				Until i.isEqualTo(osmlist.ending()) 
				
			End If
	
		End If
	Next
	
End Function

'Summary: Dummy container structure for holding game properties-
Type TGameData
	Field hitPoints:Int
	Field Camera:TCamera
End Type