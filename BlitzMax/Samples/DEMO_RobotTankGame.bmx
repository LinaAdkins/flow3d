REM----------------------------------------------------
DEMO_RobotTankGame.bmx
Author: Damien Sturdy

This demo will show you how to create some basic gameplay mechanics using Flow3D and Newton physics.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------
SuperStrict

'Import Modules
Import flow.main
Import flow.ogrenewt
EnablePolledInput()

AppTitle = "Flow3D - Demo game"
HideMouse()
fG.init()
fG.enableShadows()
fP.enablePhysics(60)

EnablePolledInput()


	'Setting the Scene
	
	'Here we modify the normal fG light.
		Global light:TLight = fG._light
		fG.PositionEntity(light, - 100, 100, - 150)
			
	'Load the Robot OSM-

		fG.loadosm("robocastle.OSM")
	  
	'Player setup
		
		Global players:TRobot[3]
		players[0] = TRobot.Create("Walkie", "Box03")
		players[1] = TRobot.Create("Walkie2", "Box03v2")
		players[2] = TRobot.Create("Walkie3", "Box03v3")
		players[0].SetPosition(TVector3.Create(5, 0, - 15))
		players[1].SetPosition(TVector3.Create(5, 0, 15))
		players[2].SetPosition(TVector3.Create(30, 0, 0))

	'Main Camera Setup
		'The default fG camera setup isn't best suited to what we need, so lets adjust it.
		'First, move the camera to its default position.
		
		fG._camera.getParentSceneNode().setPosition(0, 0, 0)
		fG._camera.getParentSceneNode().setOrientationWithQuaternion(TQuaternion.IDENTITY())
		

	'Set up the floor
	
		Global Floor:TEntity = fG._scene.createEntity("floor", "Plane01.mesh")
		Global floornode:TSceneNode = fG._rootnode.createChildSceneNode("floornode", 0, 0, 0)
		floornode.attachObject(Floor)
		Floor.setMaterialName("ground")
		fP.addPhysics(Floor, True)
			
	
	'Create some physics boxes and stack them.
	
		Local i:Int = -1
		Local bricksize:Float = 1
		While i < 100	'loop until we've created 100 blocks.
			i = i + 1
			Local y:Float = Int(i Mod 20)
			Local x:Float = (Int(i / 20)) * 1.01
			x = x + (y Mod 2) * 0.5
			x = x * bricksize
			y = y * bricksize
			Local cube:TEntity = fG.createCube("cube" + String(i))
			cube.getParentSceneNode().scale(bricksize * 2, bricksize, bricksize * 4)
			cube.setMaterialName("Rocks")
			fG.TranslateEntity(cube, x * 2, y + (bricksize / 2), 0)
			Local body:TBody = fP.addPhysicsBox(Cube, 2, TVector3.Create(bricksize * 2, bricksize, bricksize * 4))
			body.setAutoSleep(True)
		Wend
			

	'Here we have variables used to calculate mouse speed.
	
		Global CentrePos_x:Int = fG._ogre.GETwidth() / 2
		Global CentrePos_y:Int = fG._ogre.GETheight() / 2

		MoveMouse(CentrePos_x, CentrePos_y)
	
	'Set the solver model to 2- single linear pass.
	
		fP.Getworld().setSolverModel(2)
	
		Global delta:Float
		Global timeoflastrender:Int = MilliSecs()
		

	'lets create a particle system- we'll use this when something is hit.
		Global ParticleSystem:TParticleSystem = fG.loadParticleSystem("ParticleSystem/BulletHit", "BulletHit") 'fG.createEmptyParticleSystem("ps")
				
	'OK, lets load an overlay to display scores on.
	
		Global HUD:TOverlay = TOverlayManager.getByName("Robots/DemoHUD")
		Global HUDMainPanel:TOverlayContainer = HUD.GetChild("Robots/MainPanel")
		

	
	'Find and change the header text at the top centre of this overlay, and change it to something more suitable.
		Global InfoLine:TOverlayElement = hudmainpanel.getChild("Robots/Title")
		InfoLine.setCaption("Flow3D: Demo Game Tutorial.")
		
	'Lets get the score line from the Hud
		Global ScoreOverlay:TOverlayElement = HUDMainPanel.getChild("Robots/Score")
		
	'Show the hud!
		HUD.show()
		
	'Let's load some sounds-
		Global ShootSound:TSound = LoadSound("Media\Sounds\Gunshot.wav")
		Global HitSound:TSound = LoadSound("Media\Sounds\rico2.WAV")
		
		Global cameratilt:Float = 15
	'Lets loop and process the game!
	
	Global totaltimer:Int
	Repeat
	
		Local mouserel_x:Int = MouseX() - CentrePos_x
		Local mouserel_y:Int = MouseY() - CentrePos_y
		MoveMouse(CentrePos_x, CentrePos_y)
		
		'Update the player robot:
		players[1].Turn(- mouserel_x *.25 / delta)	'divide by delta because mouse movement is in smaller chunks at higher framerates anyway!
		players[1].pitch(mouserel_y * -0.1)
		
		'Do the AI for the CPU player
		players[0].DoAI(players[1])
		players[2].DoAI(players[0])
		
		Local Strafe:Int = KeyDown(KEY_D) - KeyDown(KEY_A)
		Local Walk:Int = KeyDown(KEY_W) - KeyDown(KEY_S)
			
		'Actually move the players
			players[1].move(TVector3.Create(Strafe, 0, Walk))
			

			If MouseHit(1) Then '"fire!"
				players[1].Fire(players[0])
				'lets play the fire sound-
				PlaySound(ShootSound)
			EndIf
			
			

		'Update the physics:
			fP.update()
			
		'Update the overlay with the scores!
			ScoreOverlay.setCaption("Player 1 : " + players[1].Score + "~nPlayer 2 : " + players[0].Score)
		
		'Update the players- after the physics since the head nodes need repositioning and physics has an affect on this
			players[2].update()
			players[1].update()
			players[0].update()
		
		'Lets use a newton ray query to prevent the camera from going through walls etc.
			Local NewtonCollision:TNewtonCollision = TNewtonCollision.RayQuery(players[1].GetHeadNode(), 0, 15, - 50, True)
			fG.PositionEntity(fG._camera, newtoncollision.Position.getX(), newtoncollision.Position.gety(), newtoncollision.Position.getz())
			
		'Point camera at this before rendering.
			fG._camera.lookAt(players[1].GetPosition().getX(), players[1].GetPosition().getY() + 1, players[1].GetPosition().getz())
			fG._camera.pitch(8)	'Look up a bit
		'Render!
	 		fG.renderWorld()
		
		'Delta calculation. Quick and simple.
			delta = (Float(MilliSecs() - timeoflastrender) / (1000.0 / 60.0))
			timeoflastrender = MilliSecs()
	   	
	Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()

'Types used by the game. Nothing special here.

Type TRobot
	Field bodyyaw:Float			'Yaw for the robot body
	Field headyaw:Float 		'Yaw for the robot's head
	Field headAssemblynode:TSceneNode
	Field spine:TBone
	
	Field animWalk:TAnimationState
	Field animStrafe:TAnimationState
	Field animStand:TAnimationState
		
	Field WalkAnimSpeed:Float
	Field WalkAnimEnabled:Float
	Field StrafeAnimSpeed:Float
	Field StrafeAnimEnabled:Float
	Field legsturning:Float		'non-zero if the legs are rotating.
	
	Field IsMoving:Float
	
	Field _Node:TSceneNode
	
	Field PhysicsBody:TBody

	Field headtilt:Float = 0
	Field turnspeed:Float		'used to dampen turning.
	Field movespeed:TVector3	'Used to move the robot.
	
	Field UpvectorJoint:TUpVectorJoint
	
	Field Score:Int			'Holds this robots score.
	
	Global animSpeed:Float =.03
	Global strafeSpeed:Float = 0.4
	Global walkSpeed:Float = 0.6
	
	
	'Create a TRobot
	Function Create:trobot(Robotname:String, Headname:String)
		Local Newrobot:TRobot = New TRobot
		'Grab the robot legs entity and node-
		Local robot:TEntity = fG._scene.getEntity(Robotname)
		Local robotnode:TSceneNode = robot.getParentSceneNode()
		Newrobot._node = robotnode
		'Grab the robot head assembly ( head, weapons, mounts ) node-
		Newrobot.HeadAssemblyNode:TSceneNode = fG._scene.getentity(Headname).getParentSceneNode()
	
		'Get the robot's spine for attaching the head assembly to-
		Newrobot.spine:TBone = robot.getSkeleton().getBoneWithName("MechPelvis")
	
		'Get the robot leg's animations, enable standing by default-
		Newrobot.animWalk:TAnimationState = robot.getAnimationState("Walk")
		Newrobot.animStrafe:TAnimationState = robot.getAnimationState("Strafe")
		Newrobot.animStand:TAnimationState = robot.getAnimationState("Idle")
		Newrobot.animStand.setEnabled(True)
		
		'Downscale - because the robots are much larger than they should be.
		ROBOT.getParentSceneNode().scale(0.1, 0.1, 0.1)
		newrobot.headAssemblynode.scale(0.1, 0.1, 0.1)		'
		newrobot.PhysicsBody = fP.addPhysicsbox(robot, 2, TVector3.Create(1, 1.2, 1),, TVector3.Create(0, .56, 0) , _PhysicsCallback)
				
		'lets set the userdata
		newrobot.PhysicsBody.setUserData(newrobot)
		
		'and create a joint to keep the robot standing.
		newrobot.UpVectorJoint = TBasicJoints.CreateUpVector(fP.Getworld(), newrobot.PhysicsBody, TVector3.Create(0, 1, 0))
		
		Return newrobot
	End Function
		
	Method GetRootNode:TSceneNode()
		Return _node
	End Method
	
	Method GetHeadNode:TSceneNode()
		Return headAssemblynode
	End Method
	
	
	Method Fire(Target:Trobot)
		'Perform a newton ray query and check what is in front of the robot.
		'Use the robot's weapon node to cast from- if you cast from the center of a robot,
		'the query will hit the robot instead!
		Local NewtonCollision:TNewtonCollision = TNewtonCollision.RayQuery(GetHeadNode(), 0, 0, 5000, False)
		If newtoncollision.Body <> Null Then 'We have a hit!
			'Add an impulse to the body the ray hit.
			newtoncollision.body.addImpulse(newtoncollision.direction.mulWithScalar(25), newtoncollision.body.getPosition())

			'OK, for visual effects what we're doing here is creating a vector that points back along 
			'the ray with some randomisation.
			'First, get the direction of the ray and invert it:
			Local directionvector:TVector3 = newtoncollision.direction.negative()
			'Now add some variance:
			directionvector = directionvector.add(TVector3.Create(Rnd(-.5, .5), Rnd(-.5, .5), Rnd(-.5, .5)))
			'Now we normalise this so that it has a length of 1.
			directionvector.normalise()
			'and finally multiply it by a factor of 10, so that all of the emitted particles have the same velocity.
			directionvector = directionvector.mulwithscalar(10)
			If newtoncollision.Body.getOgreNode().getName() = target.PhysicsBody.getOgreNode().getName() Then
				score = score + 100
				'lets play the Hit sound here. To make it sound a little better, lets vary the pitch a bit.
				Local Hitsound_channel:TChannel = PlaySound(HitSound)
				hitsound_channel.SetRate(Rnd(0.9, 1.1))
				hitsound_channel.SetVolume(0.5)

			End If
			'OK lets create the particle!
			'Local particle:TParticle = fG.createParticle(ParticleSystem, DirectionVector, 1.0, newtoncollision.Position)
		End If
	End Method
	
	Method DoAI(Target:Trobot)
		'Lets do some basic AI.
		'Clever use of TformPoint can allow you to know which way to turn the Ai robot so that it faces the player.
		Local AIrelative:TVector3 = TformPoint(target.GetHeadNode()._getDerivedPosition(),, Self.GetHeadNode())
		Local movedirection:Int = 1	'move forward = 1, move backwards = 2
		Local strafedirection:Int = 0
		Local distancetotarget:Int = Target.GetHeadNode()._getDerivedPosition().distance(Self.GetHeadNode()._getDerivedPosition())

		If distancetotarget < 13 Then strafedirection = -Sgn(airelative.getX()) 'if we're getting close, start to strafe.
		
		If distancetotarget < 12 Then 'if the AI is too close, we need to turn around and go away, 
			If distancetotarget < 12 And distancetotarget > 8 Then
				movedirection = 0
			EndIf
			'To move away from a point, invert the data the AI uses.
			movedirection = -movedirection
		EndIf
		DebugLog airelative.getX() + "  " + airelative.getZ()
		Local turnval:Float = 0
		If airelative.getz() > 0 Then turnval = -Sgn(airelative.getX()) Else turnval = (- airelative.getX()) / 4.0;If Abs(turnval) > 4 Then turnval = Sgn(turnval) * 4
		'OK, if we're hardly turning, shoot.
		If Abs(turnval) <.3 And Rand(1, 100 / delta) = 1 Then Self.Fire(target)
		Self.turn(turnval)
		Self.move(TVector3.Create(strafedirection, 0, movedirection))
	End Method
	
	Method Turn(value:Float)
	value = value * delta
		If Abs(value) > Abs(turnspeed) Then turnspeed = value
	End Method
	
	Method pitch(value:Float)
		headtilt = headtilt + value
		If Abs(headtilt) > 45 Then headtilt = Sgn(headtilt) * 45
	End Method
	
	Method _Updatebody()
		
		'update turning stuff.	
		headyaw = headyaw + (turnspeed)
		If Abs(bodyyaw - headyaw) > 60 Then bodyyaw = (bodyyaw + turnspeed) ; legsturning = Sgn(turnspeed) *.5
		turnspeed = 0

	
		If Abs((headyaw - bodyyaw)) > 0 Then	'if we need to rotate the legs slowly,
			legsturning = Sgn(headyaw - bodyyaw) *.5
			If Abs(headyaw - bodyyaw) < 2 * delta Then bodyyaw = headyaw
			bodyyaw = bodyyaw + Sgn(headyaw - bodyyaw) * 2 * delta
			If Sgn(headyaw - bodyyaw) *.5 <> legsturning Then bodyyaw = bodyyaw + Sgn(headyaw - bodyyaw) ;legsturning = 0
		Else	'otherwise, force alignment
			bodyyaw = headyaw
		End If
	End Method
	
	Method Move(vec:TVector3)
		vec = vec.mul(TVector3.Create(strafeSpeed, 1, - walkSpeed))
		movespeed = vec
		movespeed = Tformpoint(vec, _node, Null).sub(_node._getderivedPosition())
		If vec.getX() <> 0 Then strafeanimenabled = True;strafeanimspeed = vec.getX()
		If vec.getZ() <> 0 Then walkanimenabled = True;walkanimspeed = -vec.getz()
		vec.setY(0)
		ismoving = True
	End Method
	
	Method GetPosition:TVector3()
		Return _node._getderivedposition()
	End Method
	
	Method SetPosition(vec:TVector3)
		_node.setPositionwithvector3(vec)
	End Method
	
	Method SetOrientation(Orientation:TQuaternion)
		_node.setOrientationWithQuaternion(Orientation)
	End Method

	Method Update()
		Self._updatebody()

		
		'do any re-orientating:
		_node.setOrientationWithQuaternion(TQuaternion.IDENTITY())
		_node.yaw(bodyyaw)
		
		HeadAssemblynode.setOrientationWithQuaternion(TQuaternion.IDENTITY())
		HeadAssemblynode.yaw(headyaw)
		headassemblynode.pitch(headtilt)
			
		'keep the head aligned to the body.
		HeadAssemblyNode.setPositionWithVector3(_node._getFullTransform().mulWithVector3(spine._getDerivedPosition()))
		'update this node, useful for a few things - By default nodes are "updated" on render.
		Headassemblynode._update(True, False)
		
		'Deal with animation
			
		If legsturning <> 0 Then strafeanimspeed = 0;walkanimspeed = 0
		
		If strafeanimenabled = False Then animStrafe.setEnabled(False)
		If Walkanimenabled = False Then animWalk.setEnabled(False)
		
		If strafeanimenabled = False And walkanimenabled = False Then	'No animation is going on, lets default to the standing animation.			
			animWalk.setEnabled(False)
			animStrafe.setEnabled(False)
			animStand.setenabled(True) 
			animStand.addTime(animSpeed *.1 * delta)
		End If
		
		If StrafeAnimEnabled Or legsturning Then
			animStrafe.setenabled(True)
			animStrafe.addTime((animSpeed * (strafeanimspeed - legsturning) * 2.3) * delta)
		End If
		
		If WalkAnimEnabled Or legsturning Then
			animWalk.setenabled(True)
			animWalk.addTime((animSpeed * (walkanimspeed) * 1.5) * delta)
		End If
		
		'Reset animation flags
		strafeanimenabled = False;walkanimenabled = False
		strafeanimspeed = 0;walkanimspeed = 0
		ismoving = False
		legsturning = False
	End Method
	
	Function _PhysicsCallback(me:Byte Ptr)
		'Let's get the body. This is required for all the physics callbacks.
		Local body:TBody = TBody.FromPtr(me)

		'now lets get the robot:
		Local robot:TRobot = TRobot(body.getUserData())

		'Force should be gravity plus whatever we are trying to move the robot by.
		Local force:TVector3 = TVector3.Create(0, -.18, 0).add(robot.movespeed.mulWithScalar(5))
		
		Body.setPositionOrientation(robot._Node._getderivedposition(), robot._Node._getderivedorientation())

		Local mass:Float = 1
		Local inertia:TVector3 = TVector3.CreateEmpty() 
		body.getMassMatrix(mass, inertia) 
		force.mulwithScalar(mass) 
		body.setVelocity(body.getVelocity().mulWithScalar(0.9).add(force))
		body.setOmega(TVector3.ZERO())
	End Function
	
End Type


Function TformPoint:TVector3(vector:TVector3, fromspace:TSceneNode = Null, tospace:TSceneNode = Null)
	'This function will convert between cloordinate systems.
	'It will do so linearly- converting first from FROMSPACE to World space - If necesary!
	'and then from World space to Tospace. This will give us all three options
	'necesary to do the required conversions provided by the old Blitz3D function
	Global matrix:TMatrix4 = TMatrix4.createEmpty() 

	If fromspace <> Null Then	'Translate from localspace to worldspace
		fromspace.getWorldTransforms(matrix) 
		vector:TVector3 = matrix.mulWithVector3(vector) 
		If tospace = Null Then Return vector 'If this is all we need to do, then return the vector. otherwise, we continue to cast it back into TOspace.
	EndIf
	
	If tospace <> Null Then	'If we need to convert to global, then this is where we do so.
		tospace.getWorldTransforms(matrix)
		Return matrix.inverse().mulWithVector3(vector) 
	EndIf
	Return vector
	
End Function


'Basic helper class for newton raycasts.
Type TNewtonCollision
	Field Body:TBody	'hit body
	Field Position:TVector3	'hit location in world space
	Field direction:TVector3	'Direction of hit.
	
	Global returnval:TVector3
	Global endvec:TVector3
	
	'This function performs a "raypick" using newton physics.
	'StartOgreNode : The node we're casting from.
	'DirX,DirY,DirZ: The direction we're casting.
	'IsPhysicsObject: Is the node provided controlled by newton?
	'if this is set, the query will return data on the second hit instead of the first.
	Function RayQuery:TNewtonCollision(StartOgreNode:TSceneNode, dirx:Float, diry:Float, dirz:Float, IsPhysicsObject:Int = False, globalcoords:Int = False)
		Local NewtonCollision:TNewtonCollision = New TNewtonCollision
		
		Local startvec:TVector3 = starTOgreNode._Getderivedposition()
		If globalcoords = False Then endvec = TformPoint(TVector3.Create(dirx, diry, - dirz), starTOgreNode) Else endvec = startvec.add(TVector3.Create(dirx, diry, dirz))
		Local unit:TVector3 = endvec.sub(startvec)
		unit.normalise()
		Local _raycast:TBasicRaycast = TBasicRaycast.Create(fP.getWorld(), startvec, endvec)
		Local IntersectPoint:TVector3 = endvec
		
		If _raycast.getHitCount() > 0 Then 'If the ray hit something! Lets calculate it!
			'First, get the distance to the first hit. Multiply it by the length of the ray
			'to get the position of the intersection local to the start point.
			Local Raycastinfo:TBasicRaycastInfo
			
			'If the node provided is affected by physics then we want to ignore the first hit and use the second one.
			'otherwise, we use the first one. This is to prevent a query returning the node it was cast from.
			If isphysicsobject = True And _raycast.getHitCount() > 1 Then
				raycastinfo = _raycast.getInfoAt(1)
			Else
				raycastinfo = _raycast.getFirstHit()
			EndIf
			
			If raycastinfo <> Null Then
				Local Dist:Float = Raycastinfo.getDistance() * (endvec.Distance(startvec))
				Local ActualDistance:TVector3 = unit.mulwithscalar(dist)
				'then add this to the start point to get the world position of the intersection.
				IntersectPoint = startvec.add(actualdistance)
				'store the result and return the newtoncollision instance.
				NewtonCollision.Position = IntersectPoint
				NewtonCollision.body = Raycastinfo.getBody()
				newtoncollision.direction = unit
				Return NewtonCollision
			EndIf
		EndIf
	
		'No hit, return the end point of the ray!
		newtoncollision.Position = endvec
		newtoncollision.direction = unit
		Return newtoncollision

	End Function
	
	
End Type


