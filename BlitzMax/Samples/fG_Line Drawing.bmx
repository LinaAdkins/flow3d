REM----------------------------------------------------
fG_Line Drawing.bmx
Author: Damien Sturdy and Lina Adkins

This example will show you how to easily draw 2d and 3d lines in your scene using the line manager in fG.
of the lines you create. 

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

'Load some robot walker legs and scale them down
Global RobotWalker:TEntity = fG.LoadMesh("RobotWalker", "walkie.mesh")
fG.scaleEntity(RobotWalker, 0.5, 0.5, 0.5)

'Throw in a plane for them to stand on and give it the standard unit tile material
Global plane:TEntity = fG.createPlane("Plane")
plane.setMaterialName("tile1")

'Add some quick stencil shadows
fG.enableShadows()

'Initialise the line manager-
Global lm:TLineManager = fG.InitLineDrawing()

'Draw a 2d rectangle
lm.Draw2DLine(- 0.2, - 0.2, 0.2, - 0.2)
lm.Draw2DLine(0.2, - 0.2, 0.0, .2)
lm.Draw2DLine(0.0, 0.2, - 0.2, - 0.2)

'Draw two 3d lines that intersect the robot cockpit
lm.Draw3DLine(10, 3.5, 0, - 10, 3.5, 0)
lm.Draw3DLine(0, 3.5, - 10, 0, 3.5, 10)

'Basic Render Loop
Repeat

	'Turn our entity 1 degree along the y-axis every frame ( LOCAL )
	fG.TurnEntity(RobotWalker, 0, 1, 0)

	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Line Drawing - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()
