'Initialize
Import flow.Main
EnablePolledInput()
fG.init()

'Load some robot walker legs and scale them down
Global RobotWalker:TEntity = fG.LoadMesh("RobotWalker", "walkie.mesh")
fG.scaleEntity(RobotWalker, 0.5, 0.5, 0.5)

'Throw in a plane for them to stand on and give it the standard unit tile material
Global plane:TEntity = fG.createPlane("Plane")
plane.setMaterialName("tile1")

'Add some quick stencil shadows
fG.enableShadows()

fG._ogre.getRenderSystem()._setViewport(fG._viewport)

'Basic Render Loop
Repeat

	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Getting Started - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Turn our entity 1 degree along the y-axis every frame ( LOCAL )
	fG.TurnEntity(RobotWalker, 0, 1, 0)

	'Our manual render loop, containg an update and then a corresponding buffer swap
	fG._renderwindow.update(False)
	fG._renderwindow.SwapBuffers()
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()
