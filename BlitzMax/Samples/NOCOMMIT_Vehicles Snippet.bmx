REM----------------------------------------------------
flowPhysics_gettingStarted.bmx
Author: Damien Sturdy and Lina Adkins

This demo will show you how to quickly create 50 physics boxes. The box hulls are generated convex hulls, so you may use 
addPhysics(...) to wrap irregularly shaped objects as well.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

'Import modules
Import flow.main
Import flow.ogrenewt
EnablePolledInput()

'Init Flow3D and Newton Physics
fG.init()
fG.enableShadows()
fP.enablePhysics()

'Create the ground, add texture and physics
groundPlane:TEntity = fG.createPlane("groundPlane")
groundPlane.setMaterialName("tile1")
fP.addPhysics(groundPlane, True)

	cube:TEntity = fG.createCube("cube" + String(i))
	cube.setMaterialName("Red")
	fG.TranslateEntity(cube, 0, 2, 0)
	cubeBody:TBody = fP.addPhysics(cube, False)
	Global vehicle:TVehicle = TVehicle.Create()
	vehicle.init(cubeBody, Vec3(1, 0, 0) , Vec3(0, 1, 1))
	
	Global tire:TVehicleTire = TVehicleTire.Create(vehicle, TQuaternion.createEmpty() , Vec3(0, 0, 0) , Vec3(- 1, 0, 0) , 5, 1, 1, 1, 1, 1, 0)
'	Notify("OH")
'	fP.setDebugEnabled(True)


'Main Loop
Repeat
	fG.changeAppTitle( "fP - Getting Started - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered() )
	fP.update()
	fG.renderWorld()
Until KeyDown(KEY_ESCAPE) Or fG.AppTerminated()

