Rem - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
fG_attachToOgre.bmx
Author: Mark Laprairie (Based on code by: Damien Sturdy and Lina Adkins)

This sample shows you how to initialise Ogre inside a Graphics(...) window manually without fG.
When the application starts it will show the standard Ogre config dialog and allow the user to choose their rendering
settings. fG is 'attached' to a running instance of Ogre to allow for you to take advantage of fG functionality.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

AppTitle = "Flow3D - With Config Dialog"

'Initializing Ogre
	'Init the ogre instance and create a Graphics Window
	Global Ogre:TOgre = TOgre.Create("Conf/Plugins.cfg", "Conf/ogre.cfg", "Conf/log.txt")
	If Ogre.ShowConfigDialog() = False Then End
	TResourceGroupManager.LoadResourceFile("Conf/Resources.cfg") 
	Ogre.initialise(False) 
	Graphics Ogre.getWidth(), Ogre.getHeight() 

	
	'TRenderWindow Setup - Attach the Ogre renderer to the graphics window
	Global opts:TNameValuePairList = New TNameValuePairList.Create() 
	opts.insert("externalWindowHandle", String(GetActiveWindow())) 
	Global mainRenderWindow:TRenderWindow = ogre.createRenderWindow("mainWindow", ogre.getWidth(), ogre.getHeight(), ogre.getFullScreen(), opts) 
	
	'Initialize resources - Do this after the render window is created in case materials require shaders, etc.
	TResourceGroupManager.InitialiseAllResourceGroups() 
 
	'Scene Manager
	Global mainSceneManager:TSceneManager = ogre.createSceneManager(ST_Generic, "MainScene")
	Ogre._setCurrentSceneManager(mainSceneManager)
	'mainSceneManager.setShadowTechnique(SHADOWTYPE_STENCIL_MODULATIVE) 

	'fg.attach can handle automatic camera creation (same as init)
	'or they can be created externally
	rem
		'Main Camera Setup
		Global mainCamera:TCamera = mainSceneManager.createCamera("mainCamera")
		mainCamera.setPosition(0, 10, - 10)
		mainCamera.setNearClipDistance(0.1)
		mainCamera.setAspectRatio(1.0)
		mainCamera.lookat(0, 0, 0)
		
	 	'Viewport Setup
		Global mainViewport:TViewport = mainRenderWindow.addViewPort(mainCamera) 
		mainViewport.setBackgroundColour(TColourValue.Create(0.344, 0.0, 0.0, 1.0)) 
	 EndRem
 
	'Default Light setup
	'If we don't deifne _Light, attach will create a default light. 
	fG._light = mainSceneManager.createLight("_light")' fG.createLight("_light", TLight.LT_POINT)
	Local lightNode:TSceneNode = mainSceneManager.getRootSceneNode().createChildSceneNode("_light", 0, 0, 0)
	lightNode.attachObject(fG._light)
	lightNode.setPosition(0, 10, 10)
	
'/Initializing Ogre


'Attach Ogre to fG						
	fG.attach(Ogre, mainRenderWindow)	'If you want attach to create a camera for you
	'fG.attach(Ogre, mainRenderWindow, mainCamera)  'If you want attach to handle camera creation
'/Attach Ogre to fG

'Load some robot walker legs and scale them down
Global RobotWalker:TEntity = fG.LoadMesh("RobotWalker", "walkie.mesh")
fG.scaleEntity(RobotWalker, 0.5, 0.5, 0.5)

'Throw in a plane for them to stand on and give it the standard unit tile material
Global plane:TEntity = fG.createPlane("Plane")
plane.setMaterialName("tile1")

'Add some quick stencil shadows
fG.enableShadows()

'For Max2D FPS counter
lastTime:Int = MilliSecs()
FPSCounter:Int = 0

'Basic Render Loop
Repeat
	
	'Turn our entity 1 degree along the y-axis every frame ( LOCAL )
	fG.TurnEntity(RobotWalker, 0, 1, 0)
	
	If KeyDown(KEY_SPACE)
		'Draw Max2d
		
		Cls
		SetColor(255, 255, 255)
		
		DrawText ("BlitzMax 2D modal drawing!", 20, 20)
		For i = 20 To 240 Step 20
			SetColor(0, i, 0)
			DrawRect (40 + i, 40 + i, 100, 100)
		Next
		
		Flip
		
		If(MilliSecs() - lasttime > 1000)
			lastTime = MilliSecs() fG.changeAppTitle("fG - Max2D - FPS : " + FPSCounter)
			FPSCounter = 0
		EndIf
		FPSCounter = FPSCounter + 1
		
	Else
	    'Draw Ogre3D
		
		'Put the FPS and triangle count in the apptitle
		fG.changeAppTitle("fG - Ogre3D - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	    'Render at (very) roughly 60fps
		fG.renderWorld()
		Delay 16.6666667
	End If
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()