Rem----------------------------------------------------
Flow3D_NoConfigDialog.bmx
Author: Lina Adkins and Damien Sturdy

This file illustrates how to manually instantiate Flow3D without using the config dialog. We use a standard Graphics window
and then hook into it with Ogre.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

'Header
	Import flow.main
	'We create our own graphics resolutions here, rather than inhering any from config dialog settings-
	Graphics(1024, 768) 
'/Header
 
'Initializing Ogre
	Global Ogre:Togre = Togre.Create("Conf/Plugins.cfg", "Conf/ogre.cfg", "Conf/log.txt") 
	
	'Get available render systems then set the first one in the list(GL) to the current render system-
	Global renderSystems:TRenderSystemList = Ogre.getAvailableRenderers() 
	Global r_it:TRenderSystemListIterator = renderSystems.begin() 
	Ogre.setRenderSystem(r_it.getRenderSystem()) 
	Ogre.loadPlugin("RenderSystem_GL") 
	
	'Load our resources for initialization later-
	TResourceGroupManager.LoadResourceFile("Conf/Resources.cfg") 
	
	Ogre.initialise(False) 

	'Create a TNameValuePairList and populate it with rendering properties, then create the TRenderWindow-
	Global opts:TNameValuePairList = New TNameValuePairList.Create() 
	opts.insert("resolution", "1024x768") 
	opts.insert("fullscreen", "false") 
	opts.insert("vsync", "false") 
	opts.insert("externalWindowHandle", String(GetActiveWindow())) 
	Global mainRenderWindow:TRenderWindow = ogre.createRenderWindow("mainWindow", 1024, 728, False, opts) 
	
	'Initialize resources - Do this after the render window is created in case materials require shaders, etc.
	TResourceGroupManager.InitialiseAllResourceGroups()
 
	'Scene Manager
	Global mainSceneManager:TSceneManager = ogre.createSceneManager(ST_Generic, "MainScene") 
	mainSceneManager.setShadowTechnique(SHADOWTYPE_STENCIL_MODULATIVE) 

	'Main Camera Setup
	Global mainCamera:TCamera = mainSceneManager.createCamera("mainCamera") 
	mainCamera.setPosition(100.0, 200.0, 100.0) 
	mainCamera.setNearClipDistance(0.1) 
	mainCamera.setAspectRatio(1.0) 
	mainCamera.lookat(0, 0, 0) 
	
 	'Viewport Setup
	Global mainViewport:TViewport = mainRenderWindow.addViewPort(mainCamera) 
	mainViewport.setBackgroundColour(TColourValue.Create(1.0, 1.0, 1.0, 1.0)) 
'/Initializing Ogre


'Get the root scene node
Global RootNode:TSceneNode = mainSceneManager.getRootSceneNode()

'Adding an omni light
Global Light:TLight = mainSceneManager.createLight("mainLght")
Light.setPosition(50, 150, 50)

'Create our Robot and attach it to the TSceneNode
Global Robot:tentity = MainSceneManager.createEntity("ROBOT", "robot.mesh") 
Global RobotNode:TSceneNode = RootNode.CreateChildSceneNode("Robotnode", 0, 0, 0)
RobotNode.Attachobject(Robot) 



Repeat

	'Rotate our robot a little every 1/60th of a frame-
	RobotNode.rotate(TVector3.UNIT_Y(), 1, TS_LOCAL)
	
	'Render at (very) roughly 60fps
	Ogre.RenderOneFrame()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or AppTerminate()
