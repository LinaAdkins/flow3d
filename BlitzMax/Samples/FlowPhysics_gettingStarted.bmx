REM----------------------------------------------------
flowPhysics_gettingStarted.bmx
Author: Damien Sturdy and Lina Adkins

This demo will show you how to quickly create 50 physics boxes. The box hulls are generated convex hulls, so you may use 
addPhysics(...) to wrap irregularly shaped objects as well.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

'Import modules
Import flow.main
Import flow.ogrenewt
EnablePolledInput()

'Init Flow3D and Newton Physics
fG.init()
fG.enableShadows()
fP.enablePhysics()

'Create the ground, add texture and physics
groundPlane:TEntity = fG.createPlane("groundPlane")
groundPlane.setMaterialName("tile1")
fP.addPhysics(groundPlane, True)

'Create physics boxes
Global i:Int = 0
While i < 50
	i = i + 1
	cube:TEntity = fG.createCube("cube" + String(i))
	cube.setMaterialName("Red")
	fG.TranslateEntity(cube, 0, i * 2, 0)
	fP.addPhysics(cube, False)
Wend

'Main Loop
Repeat
	fG.changeAppTitle( "fP - Getting Started - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered() )
	fP.update()
	fG.renderWorld()
Until KeyDown(KEY_ESCAPE) Or fG.AppTerminated()

