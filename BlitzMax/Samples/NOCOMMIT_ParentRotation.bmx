REM----------------------------------------------------
NOT IN DEMO
fG_ParentRotations.bmx
Author: Damien Sturdy and Lina Adkins
©2009 BoxSnap Studios

Demo to show how to inherit local and parent rotations easily.
Note: This demo doesn't seem to do what was intended anymore.
ENDREM-------------------------------------------------

Import flow.main
EnablePolledInput()

AppTitle = "fG - Getting Started [Press ESC to Exit]"
fG.init()

'Setup the camera-
Global Camera:TCamera = fG.getDefaultCamera() 
Camera.setPosition(200, 250, 0)

'Setup the robot-
Global robot:TEntity = fG.LoadMesh("robot", "robot.mesh")
Global robotNode:TSceneNode = robot.getParentSceneNode()
Camera.lookAtWithVector3(robot.getParentNode()._getDerivedPosition())

'Setup our second robot parent-
Global robot2:TEntity = fG.LoadMesh("robot2", "robot.mesh")
Global robot2Node:TSceneNode = robot2.getParentSceneNode()
robot2Node.setPosition(- 45, 0, 0)
robot2Node.yaw(100)

'Setup the robotChild and set it as the child of "robot".
Global robotChild:TEntity = fG.LoadMesh("robotChild", "robot.mesh")
Global robotChildNode:TSceneNode = robotChild.getParentSceneNode()
robotChildNode.setPosition(100, 0, 0)
fG._rootnode.removeChild(robotChildNode)
robot.getParentSceneNode().addChild(robotChild.getParentSceneNode())
robotChildNode.pitch(45)

'Get the starting orientation
Global startingChildOrientation = robotChildNode.getOrientation()
Local oldChildOrientation:TQuaternion
Local fullInverseTransform:TMatrix4
Local newposition:TVector3
Local transform:TMatrix4
Local orient:TQuaternion

Repeat
	'If the key down is S, we will swap scene node parents for th child node.
	If KeyDown(KEY_S)
		If robotChildNode.GetParent().getName() = robotNode.getName() Then
			'Get the position-
			fullInverseTransform = robot2Node._getFullTransform().Inverse()
			newPosition = fullINverseTransform.mulWithVector3(robotChildNode._getDerivedPosition())
			
			'Get the Orientation-
			transform = fullInverseTransform.mul(robotChildNode._getFullTransform())
			orient = transform.extractQuaternion()
			orient.normalise()
			
			'Set position & orientation
			robotNode.removeChild(robotChildNode)
			robot2Node.addChild(robotChildNode)
			
			'Set the values
			robotChildNode.setPositionWithVector3(newPosition)
			robotChildNode.setOrientationWithQuaternion(orient)
		Else If robotChildNode.GetParent().getName() = robot2Node.getName() Then
			'Get the position-
			fullInverseTransform = robotNode._getFullTransform().Inverse()
			newPosition = fullInverseTransform.mulWithVector3(robotChildNode._getDerivedPosition())
			
			'Get the orientation
			transform = fullInverseTransform.mul(robotChildNode._getFullTransform())
			orient = transform.extractQuaternion()
			orient.normalise()
			
			'Perform attachment-
			robot2Node.removeChild(robotChildNode)
			robotNode.addChild(robotChildNode)
			
			'Set position & orientation
			robotChildNode.setPositionWithVector3(newPosition)
			robotChildNode.setOrientationWithQuaternion(orient)
			
		End If
	End If
	
	If KeyDown(KEY_SPACE)
		'If we are currently inherting orientation then:
		If robotChildNode.getInheritOrientation() = True Then
			'Save the current "robot" parent's orientation.
			Local currentParentOrientation:TQuaternion = robot.getParentSceneNode().getOrientation()
			'Save the current childRobot orientation
			Local currentChildOrientation:TQuaternion = robotChildNode.getOrientation()
			
			'Turn off inherting orientation for the robotChild.
			robotChildNode.setInheritOrientation(False)
			'Set the child's orientation to the robot * robot child's orientation.
			robotChildNode.setOrientationWithQuaternion(currentParentOrientation.mul(currentChildOrientation))
		Else
			'If we are currently NOT inherting then set inherit orientation to true.
			robotChild.getParentSceneNode().setInheritOrientation(True)
			'Reset the child node's orientation to 0 if you want to nullify rotation that occured while it was true.
			robotChild.getParentSceneNode().setOrientationWithQuaternion(startingChildOrientation)
		End If
	End If
	FlushKeys()
	
'	fG.TurnEntity(robot, 0, 1, 0)
'	fG.TurnEntity(robot2, 0, - 1, 0)
	fG.TurnEntity(robotChild, 0, 1, 0, TS_GLOBAL)
	fG.renderWorld()  
	Delay 16.6666667
Until KeyDown(KEY_ESCAPE) Or fG.AppTerminated()
