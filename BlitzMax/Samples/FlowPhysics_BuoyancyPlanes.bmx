REM----------------------------------------------------
FlowPhysics_BuoyancyPlanes.bmx
Author: Damien Sturdy and Lina Adkins

This demo will show you how to implement buoyancy planes in your callbacks to create realistic, physical water with
density and viscosity.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

'Import modules
Import flow.main
Import flow.ogrenewt
EnablePolledInput()

'Init Flow3D and Newton Physics
fG.init()
fG.enableShadows()
fP.enablePhysics()

'Create the ground, add texture and physics
groundPlane:TEntity = fG.createPlane("groundPlane")
groundPlane.setMaterialName("tile1")
fP.addPhysics(groundPlane, True)

'Create a quick water plane, the water plane is purely for illustration and
'no physical properties are attached to the plane itself.
waterPlane:TEntity = fG.createPlane("waterPlane", 3)
fG.PositionEntity(waterPlane, 0, 2, 0)
waterPlane.setMaterialName("TransparentBlue")

'Create physics boxes
Global i:Int = 0
While i < 100

	cube:TEntity = fG.createCube("cube" + String(i))
	cube.setMaterialName("Red")
	fG.TranslateEntity(cube, 0, i * 2, 0)
	
	'Buoyancy Planes are utilised in callbacks, so we use our cube callback here.
	fP.addPhysics(cube, False, 10, CubeCallback)
	
	i = i + 1
Wend



'Main Loop
Repeat
	fG.changeAppTitle( "fP - Getting Started - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered() )
	fP.update()
	fG.renderWorld()
Until KeyDown(KEY_ESCAPE) Or fG.AppTerminated()


'We use a standard callback in order to call buoyancy. We also calculate gravity here because
'gravity is overridden in a custom callback. Here you can set the fluid density, fluid linear viscosity and
'the angular viscosity. You MUST also use a buoyancy callback to let the physics system know the current location
'of the plane.
Function CubeCallback(b:Byte Ptr)
	Local body:TBody = TBody.FromPtr(b) 
	body.addForce(body.calculateGravity())
	body.addBuoyancyForce(12, 0, 0, Vec3(0, - 9.8, 0), BuoyancyCallback)
End Function

'The purpose of this basic buoyancy callback is to simply let the physics system know where the plane is
'at any given moment. So here we define the buoyancy plane each cycle. Makes sure that when you get the
'plane from FromPtr that you specify that it's managed. And YES, you must do this if you want your
'objects to behave properly.
Function BuoyancyCallback:Byte(colID:Int, me:Byte Ptr, orient:Byte Ptr, Pos:Byte Ptr, plane:Byte Ptr)
		Local p:TPlane = TPlane.FromPtr(plane, True)
		p.redefineWithNormal(TVector3.Create(0, 1, 0) , TVector3.Create(0, 2, 0))
		Return True
End Function

