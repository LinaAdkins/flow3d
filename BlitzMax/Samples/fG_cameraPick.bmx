REM----------------------------------------------------
fG_cameraPick.bmx
Author: Damien Sturdy and Lina Adkins

This file shows you how to use fG's .camPick function to make 1:1 triangle picks in your scenes. It loads mini.osm and
allows you to click around and note the point of your collision.

©2009 BoxSnap Studios

ENDREM-------------------------------------------------

'Init Flow3D
Import flow.main
EnablePolledInput()
fG.init()

'Load an OSM containing a mini and some basic terrain
fG.LoadOSM("mini.OSM")
Global Mini:TEntity = fG.getEntity("Mini")

'Create a "PickBox" that shows where we picked visually
Global PickBox:TEntity = fG.CreateCube("PickBox", .3)
PickBox.setMaterialName("Red")
PickBox.setVisible(False)

'Main Loop
Repeat

	'If left mouse button was clicked-
	If MouseHit(1) 
		
		'Get the relative mouse coordinates for the fG render window
		Local relativeX:Float = Float(MouseX()) / Float(fG._Ogre.getWidth())
		Local relativeY:Float = Float(MouseY()) / Float(fG._Ogre.getHeight())
		
		'Run the camera pick
		fG.cameraPick(relativeX, relativeY)
		
		'If selection was valid set pickerbox to collision point and toggle bounding box on entity
		If TCameraPickResult.isNotNull()			
			'Put our pick box into place
			Local pos:TVector3 = TCameraPickResult.getPos()
			PickBox.setVisible(True)
			fG.PositionEntity(PickBox, pos.getX() , pos.getY() , pos.getZ())
		End If
		
	End If

	'Turn the mini on y-axis
	fG.TurnEntity( Mini , 0 , 1 , 0 )
	
	'Set our title
	fG.changeAppTitle("fG - cameraPick - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	fG.renderWorld()
	Delay 1000/60
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()