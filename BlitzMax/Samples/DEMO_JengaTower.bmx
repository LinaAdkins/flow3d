REM----------------------------------------------------
DEMO_JengaTower.bmx
Author: Lina Adkins

This demo was written to test the physics system. You may find some things of interest in here, but in general
it's just test code. :-) 

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

'Import modules
Import flow.main
Import flow.ogrenewt
EnablePolledInput()

'Init Flow3D and Newton Physics
fG.init()
fG.enableShadows()
fP.enablePhysics()
fG.PositionEntity(fG._camera, 5, 5, 5)
fG._camera.lookAt(0, 0, 0)

'Create the ground, add texture and physics
groundPlane:TEntity = fG.createPlane("groundPlane")
groundPlane.setMaterialName("tile1")
fP.addPhysics(groundPlane, True)

'fG.PositionEntity(fG.getDefaultCamera() , 0, 5, 5)
'fG.getDefaultCamera().lookAt(0, 0, 0)

Local i:Int = 3

While i <> 54 + 3
	'Get currently level by dividing by 3 and rounding off.
	Local currentLevel:Int = i / 3
	
	'Get whether this stack is oriented normally or 90 degrees to the right.
	Local orientRight:Int = currentLevel Mod 2
	
	'Current horiz block
	Local currentHorizontalBlock:Int = i Mod 3
	
	
	If orientRight = 1 Then
		createBlock(orientRight, Float(currentHorizontalBlock) *.3, (Float(currentLevel) *.25) -.24, .35)
	Else
		createBlock(orientRight, .35, (Float(currentLevel) *.25) -.24, Float(currentHorizontalBlock) *.3)
	End If
	


	i = i + 1

Wend


'Block Gravity is set at about 1 unit = 6 inches
Global blockGravity:TVector3 = Vec3(0, - 9.8, 0)

'Main Loop
Repeat



	If MouseHit(1) Then
	
	'Get cam to viewport ray
	Local relativeX:Float = Float(MouseX()) / Float(fG._ogre.getWidth())
	Local relativeY:Float = Float(MouseY()) / Float(fG._ogre.getHeight())
	Local ray:TRay = TRay.CreateEmpty()
	fG._camera.getCameraToViewportRay(relativex, relativey, ray)
	
	'Do a newton raycast with the ray
	Local raycast:TBasicRaycast = TBasicRaycast.Create(fP.getWorld() , ray.GetOrigin() , ray.getPoint(500))
	If raycast.getFirstHit().getBody().Pointer <> Null Then
		raycast.getFirstHit().getBody().addImpulse(Vec3(0, 5, 0) , Vec3(0, 10, 0))
	End If
	
	End If

	fG.changeAppTitle( "fP - Getting Started - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered() )
	fP.update()
	fG.renderWorld()
Until KeyDown(KEY_ESCAPE) Or fG.AppTerminated()

Global BlockCount:Int = 0

'Summary: Create a Jenga block of average size and mass that is relative to 1 unit = 1 meter.
Function createBlock(orient:Int, x:Float, y:Float, z:Float)
	'Add to the block count
	BlockCount = BlockCount + 1

	'Create the cube and scale it to the right size.
	Local block:TEntity = fG.createCube("block" + BlockCount)
	block.setMaterialName("ground")
	fG.getNode(block).scale(.81, .182, .26)
	fG.PositionEntity(block, x, y, z)
	
	If orient = True Then
		fG.getNode(block).rotate(Vec3(0, 1, 0) , 90)
	End If
	
	'Create physics entity
	fP.addPhysics(block, False, .2).setAutoSleep(True)
	


End Function