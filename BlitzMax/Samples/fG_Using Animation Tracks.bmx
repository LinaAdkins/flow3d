REM----------------------------------------------------
fG_UsingAnimationTracks.bmx
Author: Damien Sturdy and Lina Adkins

This demo will show you how to easily use animation tracks that you've created. First we will load an OSM containing some animation tracks,
then we will get those animation tracks through the scene manager and update them all using iterators.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

' Add the OSM resource location, load the OSM and use its camera
TResourceGroupManager.addResourceLocation("./media/Balls", "FileSystem", "General")
fG.LoadOSM("Balls.OSM")
fG._viewport.setCamera(fG._scene.getCamera("Camera01"))

Global currentTrack:TNodeAnimationTrack

'Basic Render Loop
Repeat

	'Get the animation iterator for all animations in the scene
	Local animIter:TAnimationIterator = fG._scene.getAnimationIterator()
	
	'While this iterator has more elements, get the track list for the current animation
	While animIter.hasMoreElements()
		Local anim:TAnimation = animIter.getNext()
		Local animTrackList:TNodeTrackList = anim._getNodeTrackList()
		
		'Get the tack list's beginning and ending iterators
		Local first:TNodeTrackListIter = animTrackList.begin()
		Local last:TNodeTrackListIter = animTrackList.ending()
		
		' Reset the animations to their initial states so adding time works properly
		While first.isNotEqualTo(last)
			GCSuspend()
			currentTrack = first.getAnimationTrack()
			currentTrack.getAssociatedNode().resetToInitialState()
			first.moveNext()
			GCResume()
		Wend
		
		'Add the current ogre time
		anim.apply(fG._ogre.getTimer().getTime())
		
	WEnd


	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Using Animation Tracks - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()
