REM----------------------------------------------------
fG_SkeletalAnimation.bmx
Author: Damien Sturdy and Lina Adkins

This demo will show you how to take an animated mesh and apply the animations in tune with a control scheme.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()
HideMouse()

'Init fG and let the user pick the options from the ogre config window
AppTitle = "Flow [Escape to exit - C to toggle Shadows, R for offset normal ground in D3D only]"
fG.init()

'Setting the Scene
	
	'Main scene light positioned to the right-
		Global Light:TLight = fG._light
		Light.Setposition(- 40, 600, - 20) 
		Light.setDiffuseColour(1, 1, 1) 
		Light.setSpecularColour(.5, .5, .5) 

	'Turn on shadows-
		fG.enableShadows()
	
	'Load the Robot OSM-
		fG.LoadOSM("walkie.OSM") 
	
		'Grab the robot legs entity and node-
		Global robot:TEntity = fG._scene.getEntity("Walkie") 
		Global robotnode:Tscenenode = robot.getParentSceneNode() 
		'Grab the robot head assembly ( head, weapons, mounts ) node-
		Global robotHeadAssembly:tscenenode = fG._scene.getentity("Box03").getParentSceneNode() 
	
		'Get the robot's spine for attaching the head assembly to-
		Global spine:TBone = robot.getSkeleton().getBoneWithName("MechPelvis") 
	
		'Get the robot leg's animations, enable standing by default-
		Global animWalk:TAnimationState = Robot.getAnimationState("Walk") 
		Global animStrafe:TAnimationState = Robot.getAnimationState("Strafe") 
		Global animStand:TAnimationState = Robot.getAnimationState("Idle") 
		animStand.setEnabled(True) 

		
	'Main Camera Setup
		'The default fG camera setup isn't best suited to what we need, so lets adjust it.
		'First, move the camera to its default position.
		fG._camera.getParentSceneNode().setPosition(0, 0, 0)
		fG._camera.getParentSceneNode().setOrientationWithQuaternion(TQuaternion.IDENTITY())
		'then, lets retrieve the scenenode the camera is attached to,
		Global cameraNode:TSceneNode = fG._camera.getParentSceneNode()
		'Remove it from its current parent.
		cameraNode.getParentSceneNode().removeChild(cameraNode)
		'And then attach it to the robot so that it follows the robot.
		robotnode.addChild(cameraNode)
		cameraNode.setInheritOrientation(False)
		fG._camera.setPosition(0, 15, - 45)
		
	'Set up the floor
		Global Floor:TEntity = fG._scene.createEntity("floor", "Plane01.mesh") 
		Global floornode:TSceneNode = fG._rootnode.createChildSceneNode("floornode", 0, 0, 0) 
		floornode.attachObject(Floor) 
		Floor.setMaterialName("Rocks")

'/Setting the Scene

	
'Variables used for mouselook-
	Global yaw:Float = 0
	Global mousepos_x:Int = fg._ogre.GETwidth() / 2
	Global mousepos_y:Int = fg._ogre.GETheight() / 2
	Global currMouse_x:Int = fg._ogre.GETwidth() / 2
	Global currMouse_y:Int = fg._ogre.GETheight() / 2
	Global mouserel_x:Int
	Global mouserel_y:Int
	Global rotyaw:Float = 0
	Global rotpitch:Float = 0
	
	cameraNode.rotate(TVector3.Create(0, 1, 0), 180)
	
	
	Global shadows:Int = True
	Global showRocks:Byte = True
	
	Global animSpeed:Float =.03
	Global strafeSpeed:Float =.5
	Global walkSpeed:Float = 1

Repeat
	fG._camera.lookAt(robotnode.getPosition().getX() , robotnode.getPosition().getY() , robotnode.getPosition().getZ())
	fG._camera.pitch(15)	'Look up a bit

	'Increase the animation timer each frame-
	'Capture relative mouse coords-
	currMouse_x = MouseX()
	currMouse_y = MouseY() 
	mouserel_x = currMouse_x - mousepos_x
	mouserel_y = currMouse_y - mousepos_y
	MoveMouse(mousepos_x, mousepos_y) 
	
	'Transform the camera based on these coords and keyboard activity-
	cameraNode.yaw(rotyaw + (mouserel_x * 3 *.025) , TS_WORLD)
	
	'Instead of actual movement, set up animations-
	
	'Check input to determine the states of the robot-
		animStand.setEnabled(False) 
		animStrafe.setEnabled(False)
		animWalk.setenabled(False) 
		
		'The following variables contain movement details:		
		Local Strafe:Int = KeyDown(KEY_D) - KeyDown(KEY_A) 
		Local Walk:Int = KeyDown(KEY_W) - KeyDown(KEY_S) 
		
		'Actually move the mesh:
		robotnode.translate(Strafe * strafeSpeed, 0, Walk * -walkSpeed, TS_LOCAL) 
		
		'Set the head assembly node's position to that of the spine bone. Get spine bone by transforming spine's local position
		'From it's parent bone by the leg entity's world transform.
		robotHeadAssembly.setPositionWithVector3(robot._getParentNodeFullTransform().mulWithVector3(spine._getDerivedPosition())) 

		'Lets reset animation type to keep animations synced together.
		If KeyHit(KEY_D) Or KeyHit(KEY_A) Or ..
		   KeyHit(KEY_W) Or KeyHit(KEY_S) Then animStrafe.setTimePosition(0) ;animWalk.setTimePosition(0) 
		
		If (Strafe Or Walk) Then
			If Strafe Then
				animStrafe.setenabled(True) 
				animStrafe.addTime(animSpeed * strafe) 
			End If
			
			If Walk Then
				animWalk.setenabled(True) 
				animWalk.addTime(animSpeed * walk) 
			End If
		Else
			animWalk.setEnabled(False) 
			animStrafe.setEnabled(False) 
			animStand.setenabled(True) 
			animStand.addTime(animSpeed *.1) 
		EndIf
		
		'If user hits "R" then toggle between offset materials and standard ( Direct3D Only! )
		If KeyHit(KEY_R) 
     		If showRocks = True Then
          		showRocks = False
          		Floor.setMaterialName("OffsetMappingSample") 
     		Else
           		showRocks = True
           		Floor.setMaterialName("Rocks") 
     		EndIf
		EndIf
		
		'If user hits "C" then toggle shadows
		If KeyHit(KEY_C) Then shadows = 1 - shadows;fG._viewport.setShadowsEnabled(shadows) 
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()