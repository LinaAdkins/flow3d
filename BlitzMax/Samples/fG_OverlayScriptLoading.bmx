REM----------------------------------------------------
fG_OverlayScriptLoading.bmx
Author: Damien Sturdy

This sample will show you how to  overlays from an .overlay script file and manipulate them.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

'Get the overlay specified in the script, then get the main panel it uses.
Global HUD:TOverlay = TOverlayManager.getByName("Demo/BasicDemo")
Global HUDMainPanel:TOverlayContainer = HUD.GetChild("OVERLAY/MainPanel")

'Get the first and second text element lines in the Hud Main Panel.
Global Line1:TOverlayElement = HudMainPanel.GetChild("OVERLAY/1")
Global Line2:TOverlayElement = HudMainPanel.GetChild("OVERLAY/2")

'Make the entire HUD visible.
HUD.show()

'Main Loop
Repeat

	'Display the current FPS and Triangle Count in the overlay main panel.
	Line1.setCaption("FPS: " + Int(fG.getFPS()))
	Line2.setCaption("Triangle Count: " + fG.TrisRendered())
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.AppTerminated()


