REM----------------------------------------------------
fG_ManualObjects.bmx
Author: Damien Sturdy and Lina Adkins

This demo shows you how to create a simple manual object using line strips. You will use the line strips to create a basic
circle shape.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

'Load some robot walker legs and scale them down
Global RobotWalker:TEntity = fG.LoadMesh("RobotWalker", "walkie.mesh")
fG.scaleEntity(RobotWalker, 0.5, 0.5, 0.5)

'Add some quick stencil shadows
fG.enableShadows()

Global man:TManualObject = fG._scene.createManualObject("man")
man.begin("Red", TRenderOperation.OT_LINE_STRIP)

Global radius:Float = 7
Global accuracy:Float = 32
Global theta:Float = 0

Global point_index:Int = 0

'theta = theta + (Pi / (radius * accuracy))
While(theta <= 2 * Pi)
	theta = theta + (Pi / (radius * accuracy))
	man.position(radius * Cos(Rad2Deg(theta)), 0, radius * Sin(Rad2Deg(theta)))
	point_index = point_index + 1
	man.normal(0, 1, 0)
	man.index(point_index)
	
Wend

man.index(0)
man.ending()

Global sn:TSceneNode = fG._rootnode.createChildSceneNode("ManualNode", 0, 0, 0)
sn.attachObject(man)

'Basic Render Loop
Repeat

	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Getting Started - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Turn our entity 1 degree along the y-axis every frame ( LOCAL )
	fG.TurnEntity(RobotWalker, 0, 1, 0)
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()