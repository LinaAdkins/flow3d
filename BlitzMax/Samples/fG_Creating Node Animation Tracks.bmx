REM----------------------------------------------------
fG_Creating Node Animation Tracks.bmx
Author: Damien Sturdy and Lina Adkins

This sample will show you how to create node track animations procedurally in code, rather than in an editing program. You will go through creating a new animation,
setting the interpolation mode and then finally you will add the keyframes. The node animation can be scaled, translated or rotated per key frame. At the end you wrap
the animation up into a TAnimationState and enable it.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

'Load some robot walker legs and scale them down
Global Cube:TEntity = fG.createCube("animatedCube")

'Throw in a plane for them to stand on and give it the standard unit tile material
Global plane:TEntity = fG.createPlane("Plane")
plane.setMaterialName("tile1")

'Add some quick stencil shadows
fG.enableShadows()

'Set up a new animation with 10 keyframes and linear interpolation
Global animation:TAnimation = fG._scene.createAnimation("animation", 10)
animation.setInterpolationMode(TAnimation.IM_LINEAR)

'Add a node track to the animation
Global track:TNodeAnimationTrack = animation.createNodeTrack(0, fG.getNode(Cube))

'We must keep track of scale manually, as it's always reset to 1.0 if we don't set it each frame
Global scale:TVector3 = fG.getNode(Cube).GetScale()

Global key:TTransformKeyFrame

'Translate the node in this track up three units on the y-axis
key = track.createNodeKeyFrame(0.0)
key.setTranslate(Vec3(0, 3, 0))
key.SetScale(scale)

'Here we modify the position as well as scale, making the object two times larger
key = track.createNodeKeyFrame(2.5)
key.setTranslate(Vec3(0, 4, 0))
key.SetScale(scale.mulWithScalar(2))

key = track.createNodeKeyFrame(5)
key.setTranslate(Vec3(- 1, 3, - 1))
key.SetScale(scale.mulWithScalar(3))

'Here we add a little bit of simple rotation with .setRotation(...)
key = track.createNodeKeyFrame(7.5)
key.setTranslate(Vec3(1, 4, - 1))
key.SetRotation(TQuaternion.Create(1.0, .2, 0.0, .2))
key.SetScale(scale)

'Our last keyframe
key = track.createNodeKeyFrame(10)
key.setTranslate(Vec3(0, 3, 0))
key.SetScale(scale)

'Last, we create an animation state to control our new node animation
Global animState:TAnimationState = fG._scene.createAnimationState("animation")
animState.setEnabled(True)
animState.setLoop(True)

'Timer variables to get the delta time to feed to animState.addTime(...)
Global timer:TOgreTimer = fG._ogre.getTimer()
Global newtime:Float = timer.getTime()
Global oldtime:Float = timer.getTime()
Global delta:Float = 0

'Basic Render Loop
Repeat

	'Calculate the time since last frame
	oldtime = newtime
	newtime = timer.getTime()
	delta = newtime - oldtime

	'Add the delta time to the animation
	animState.addTime(delta)
	
	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Creating Node Animation Tracks - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()
