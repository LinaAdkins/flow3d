Rem - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
FlowWX.bmx
Author: Damien Sturdy and Lina Adkins

This demo shows you how to use Flow with WXMax

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

SuperStrict

'Import the required modules
Framework wx.wxApp
Import wx.wxFrame
Import wx.wxGLCanvas
Import wx.wxTimer
Import flow.Main
Import flow.ogrenewt


' Start your application.
New MyApp.run()


Type MyApp Extends wxApp
	Field frame:MyFrame
	Field timer:MyTimer
	Method OnInit:Int()
	
		'Let's Create the frame that our application will use. We pass it the resolution we want to use.
		frame = MyFrame(New MyFrame.Create(Null, - 1, "Flow3D wxWidgets App", 100, 100, 640, 480))
		'Show the frame
		frame.show()
		'create a timer and set it to trigger at 60fps.
		timer = MyTimer(New MyTimer.Create())
		timer.Start(1000.0 / 60.0)
		Return True
		
	End Method
End Type

Type MyFrame Extends wxFrame
	
	Global robot:TEntity	'the robot entity...


	Method OnInit()
		
		'we need to get the frame position and size to create a wxWindow that Ogre will render to.
		Local windowwidth:Int
		Local windowheight:Int
		Local windowx:Int
		Local windowy:Int
		Self.GetScreenRect(windowx, windowy, Windowwidth, windowheight)
		'Done- lets create the window.
		Local window:wxWindow = wxWindow.CreateWindow(Self, - 1, windowx, windowy, windowwidth, windowheight)
		
		'OK, lets use the data we have been given to initialise Ogre. We need to pass the window's Hwnd, we can get this using Window.Gethandle()
		fG.init(False, windowwidth, windowheight, 32, False, fG.rs_opengl, Int(Window.GetHandle()))
		
		'Initialise your scene here-
		'Let's load and position a robot to render.
		robot = fG.LoadMesh("Robot.mesh", "Robot.mesh")
		fG.PositionEntity(robot, 0, 0, 150)
		fG.PointEntity(fG._camera, robot)
		fG.TurnEntity(fG._camera, 15, 0, 0)
		
	End Method

	'render is called from MyTimer whenever the timer is triggered
	Function Render()
	
		fG.TurnEntity(robot, 0, .1, 0)
		fG.renderWorld()
		
	End Function
End Type


'MyTimer is a standard wxTimer- nothing special here. This one calls MyFrame.Render() in turn triggering a render.
Type MyTimer Extends wxTimer
	Field count:Int
	
	Method Notify()
		count:+1
		MyFrame.Render()
	End Method
End Type
