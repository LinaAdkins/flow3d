REM----------------------------------------------------
FlowPhysics_BallAndSocketJoint.bmx
Author: Lina Adkins

This example will spawn 25 Ball and Socket joints into the scene to interact with one another.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

'Import modules
Import flow.main
Import flow.ogrenewt
EnablePolledInput()

'Init Flow3D and Newton Physics
fG.init()
fG.enableShadows()
fP.enablePhysics(120)

'Create the ground, add texture and physics
groundPlane:TEntity = fG.createPlane("groundPlane")
groundPlane.setMaterialName("tile1")
fP.addPhysics(groundPlane, True)

'Create 25 Ball & Socket Pairs
Global i:Int = 0
While i < 25

	'Increment
	i = i + 1
	
	'Create the socket from Socket.mesh
	Local Socket:TEntity = fG.LoadMesh("Socket" + String(i), "Socket.mesh")
	fG.PositionEntity(Socket, 0, 1 * i, 0)
	fG.getNode(Socket).Roll(90)
	Local SocketBody:TBody = fP.addPhysics(Socket)

	'Create the ball from Ball.mesh
	Local Ball:TEntity = fG.LoadMesh("Ball" + String(i), "Ball.mesh")
	fG.getNode(Ball).Roll(- 90)
	fG.PositionEntity(Ball, 0, 1 * i, 0)
	Local BallBody:TBody = fP.addPhysics(Ball)

	'Create the socket joint specifying the parent and the child.
	'By using the defaults here we ensure that the joint bodies won't collide with one another.
	'We also ensure that the joints position is the midway point betweent he two bodies' positions.
	Local joint:TBallAndSocket = fP.addBallSocket(SocketBody, BallBody)
	
	'Here we set the rotation limits of the joint by specifying the direction of the joint pin.
	'We also allow 80 degrees of "swing" space and 45 degrees of "twist" space in the joint.
	joint.setLimits(Vec3(1.0, 0.0, 0.0) , Radians(Deg2Rad(80)) , Radians(Deg2Rad(45)))
		
Wend

'Main Loop
Repeat
	fG.changeAppTitle("fP - Ball and Socket Joints - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	fP.update()
	fG.renderWorld()
Until KeyDown(KEY_ESCAPE) Or fG.AppTerminated()

