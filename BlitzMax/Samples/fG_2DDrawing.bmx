Rem - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- - -- -
fG_2DDrawing.bmx
Author: Damien Sturdy and Lina Adkins

This demo shows you how to draw and texture 2D shapes using fG's 2D functionality.
Press the Left and Right Arrow Keys to move the textured sprite.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

SuperStrict
Import flow.Main

'Use polled input for keypresses
EnablePolledInput()

'Create a 640x480 render window
fG.init(False, 640, 480)

'Initialise 2D Drawing
fG.Init2D

'Our movement variable for the movable sprite.
Global spritex:Float = 320


'Basic Render Loop
Repeat

	'Draw a green, unfilled rectangle
	fG.SetMaterial("PrimaryGreen")
	fG.DrawRect(10, 10, 620, 460, False)
	

	'Draw a blue, unfilled oval.
	fG.SetMaterial("PrimaryBlue")
	fG.DrawOval(240, 240, 240, 240, 32, False)
	
	
	'Update spritex variable
	If KeyDown(KEY_LEFT)
		spritex = spritex - 1
	End If
	
	If KeyDown(KEY_RIGHT)
		spritex = spritex + 1
	End If
	
	'Draw a textured rectangle that moves according to the spritex variable
	fG.SetMaterial("ground")
	fG.DrawRect(spritex, 360, 50, 100, True)
	

	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - 2D Shapes - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	
	'Render at (very) roughly 60fps
	fG.RenderWorld()
	Delay 16.6666667

	'Clear all 2D shapes which will then be redrawn on the next loop.
	fG.Cls()
	
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()