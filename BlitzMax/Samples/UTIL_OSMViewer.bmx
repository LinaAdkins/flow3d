REM----------------------------------------------------
UTIL_OSMViewer.bmx
Author: Damien Sturdy and Lina Adkins

This demo will load a given OSM from ./media/OSM or a specified resource directory
and allow you to use a WASD camera to explore the scenery.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Set the OSM name and resource dir here:
Const OSMName:String = "mini.OSM"
Const OSMDir:String = "./media/OSM"

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

'Load the external OSMDir specified
TResourceGroupManager.addResourceLocation(OSMDir, "FileSystem", "General")
TResourceGroupManager.initialiseAllResourceGroups()

Global scene:TOSMScene = fG.LoadOSM(OSMName)
'Get the default camera's node ( we're using that for rotation/translation )
Global cameraNode:TSceneNode = fG.getNode(fG.getDefaultCamera())
cameraNode.setFixedYawAxis(True, 0, 1, 0)
cameraNode.setPosition(0, 2, 10)
'Lets rotate a little to start with. (just to make the camera look at the mini that was loaded.)
cameraNode.yaw(160, TS_WORLD)
cameraNode.Pitch(30, TS_LOCAL)


'Variables used for mouselook- Blitzmax doesn't have mouse speed funcs!
Global Center_x:Int = fG._ogre.getWidth() / 2
Global Center_y:Int = fG._ogre.getHeight() / 2
Global currMouse_x:Int = fG._ogre.getWidth() / 2
Global currMouse_y:Int = fG._ogre.getHeight() / 2

Repeat

	'Hide the mouse
	HideMouse()

	'Get mouse delta coordintes
	currMouse_x = MouseX()
	currMouse_y = MouseY()
	'Calculate the speed!
	Local mouserel_x:Float = Center_x - currMouse_x
	Local mouserel_y:Float = Center_y - currMouse_y
	
	'Recenter the mouse
	MoveMouse(Center_x, Center_y)
	
	'Transform the camera based on these coords and keyboard activity-
	cameraNode.yaw((mouserel_x *.05) , TS_WORLD)	'Yaw is always global, so use TS_WORLD
	cameraNode.pitch((mouserel_y *.05) , TS_LOCAL)	'pitch is always local, so use TS_LOCAL.

	'Calculate x and z axes local translation based on keypress
	Local x:Float = (KeyDown(KEY_D) - KeyDown(KEY_A)) *.5
	Local z:Float = (KeyDown(KEY_S) - KeyDown(KEY_W)) *.5

	'and translate by that much.
	cameraNode.translate(x, 0, z, TS_LOCAL)

	'Render roughly every 1/60th second
	fG.renderWorld()
	fG.changeAppTitle("Util - OSM Viewer - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
   	Delay 16.6666667
Until KeyHit(KEY_ESCAPE) Or fG.appTerminated()