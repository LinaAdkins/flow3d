REM----------------------------------------------------
fG_ParticleSystemLoading.bmx
Author: Damien Sturdy and Lina Adkins

This demo will load a particle system from a particle script template file.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

REM
     Here we are loading a particle system from the template in resources\particles\Jet.particle named "Jet".
     Since resource\particles is specified in our resource.cfg, all we have to do is specify the name given inside
     the .partcle file.
ENDREM
Global ps:TParticleSystem = fG.loadParticleSystem("SparkSystem", "Spark")

'Basic Render Loop
Repeat

	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Particle System Loading - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()