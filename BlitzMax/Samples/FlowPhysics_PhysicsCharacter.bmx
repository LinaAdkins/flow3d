REM----------------------------------------------------
flowPhysics_PhysicsCharacter.bmx
Author: Lina Adkins

This demo illustrates how to build a character movement system using Newton.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

'Import modules
Import flow.main
Import flow.ogrenewt
EnablePolledInput()

'Init Flow3D and Newton Physics
fG.init()
fG.enableShadows()

Const DEBUG_LINES_ENABLED:Byte = True

fP.enablePhysics()

' Load mini.OSM and setup the physics on the terrain
fG.LoadOSM("mini.OSM")
Global MiniPlane:TEntity = fG.getEntity("MiniPlane")
Global planeBody:TBody = fP.addPhysics(MiniPlane, True)
Global planeMaterial:TMaterialID = TMaterialID.Create(fP.getWorld())

'Create physics walker
Global RobotWalker:TEntity = fG.LoadMesh("RobotWalker", "walkie.mesh")
fG.scaleEntity(RobotWalker, 0.25, 0.25, 0.25)
Global walkerBody:TBody = fP.addPhysicsEllipsoid(RobotWalker, 50, Vec3(1, 1, 1) , fG.getNode(RobotWalker).getOrientation() , Vec3(0, 1, 0) , characterCallback)
Global walkerMaterial:TMaterialID = TMaterialID.Create(fP.getWorld())
Global upVecJoint:TUpVectorJoint = TBasicJoints.CreateUpVector(fP.getWorld() , walkerBody, TVector3.UNIT_Y())

'Create a material pair'
Global walkerGroundMaterialPair:TMaterialPair = TMaterialPair.Create(fP.getWorld() , planeMaterial, walkerMaterial)
walkerGroundMaterialPair.setDefaultElasticity(0)
walkerGroundMaterialPair.setDefaultFriction(1.5, 2.0)

fG._camera.setAutoTracking(True, fG.getNode(RobotWalker) , 0, 0, 0)

If DEBUG_LINES_ENABLED
	fP.setDebugEnabled(True)
End If


'Variables used for mouselook- Blitzmax doesn't have mouse speed funcs!
Global MouseXCenter:Float = Float(fG._ogre.getWidth()) / Float(2)
Global MouseYCenter:Float = Float(fG._ogre.getHeight()) / Float(2)
Global NewMouseX:Float = Float(fG._ogre.getHeight()) / Float(2)
Global NewMouseRelativeX:Float = 0.0
Global OldMouseRelativeX:Float = 0.0
Global MouseDamping:Float = 0.5

'Animation variables for the walker
Global animWalk:TAnimationState = RobotWalker.getAnimationState("Walk")
Global animStrafe:TAnimationState = RobotWalker.getAnimationState("Strafe")
Global animStand:TAnimationState = RobotWalker.getAnimationState("Idle")
animStand.setEnabled(True) 

'Set-up our timing vars
Global timer:TOgreTimer = fG._ogre.getTimer() 
Global newtime:Float = timer.getTime() 
Global oldtime:Float = timer.getTime() 
Global delta:Float = 0
Global fps:Float = Float(1) / Float(60) 
Global elapsed:Float = 0

HideMouse()

Global currentTorque:TVector3 = Vec3()

Global Strafe:Int = 0
Global Walk:Int = 0

Repeat

	'Update our times
	oldtime = newtime
	newtime = timer.getTime() 
	delta = newtime - oldtime
	elapsed = elapsed + delta

	'Get WASD keys	
	Strafe = KeyDown(KEY_D) - KeyDown(KEY_A)
	Walk = KeyDown(KEY_S) - KeyDown(KEY_W)
	
	If KeyDown(KEY_W) Then
		animWalk.setEnabled(True)
		animWalk.addTime(delta)
	End If
	
	If KeyDown(KEY_S) Then
		animWalk.setEnabled(True)
		animWalk.addTime(- delta)
	End If
	
	If KeyDown(KEY_A) Then
		animStrafe.setEnabled(True)
		animStrafe.addTime(- delta)
	End If
	
	If KeyDown(KEY_D) Then
		animStrafe.setEnabled(True)
		animSTrafe.addTime(delta)
	End If
	
	animStand.addTime(delta)
	
	'Get mouse and relative coords
	NewMouseX = MouseX()
	NewMouseRelativeX = MouseXCenter - NewMouseX
	
	'If rotspeed is not zero, do some walking
	If OldMouseRelativeX <> 0 Then
		animWalk.setEnabled(True)
		animWalk.addTime(delta)
	Else
		If animWalk.getTimePosition() < 0 Then
			animWalk.addTime(- delta)
		End If
	End If
	
	'If we have a non-zero movement, then make it our current rotation speed.
	If NewMouseRelativeX <> 0 Then
		OldMouseRelativeX = NewMouseRelativeX
	End If
	
	'If our current rotation speed is greater than our new one, damp up
	If OldMouseRelativeX > NewMouseRelativeX Then
		OldMouseRelativeX = OldMouseRelativeX - MouseDamping
	End If
	
	'If our current rotation speed is less than our new one, damp down
	If OldMouseRelativeX < NewMouseRelativeX Then
		OldMouseRelativeX = OldMouseRelativeX + MouseDamping
	End If

	'Snap mouse back to center
	MoveMouse(MouseXCenter, MouseYCenter)
	
	fG.changeAppTitle("fP - Character Physics - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	fP.update()
	fG.renderWorld()
Until KeyDown(KEY_ESCAPE) Or fG.AppTerminated()

REM
	bbdoc: Custom force and torque callback for handling keypresses.
ENDREM
Function characterCallback(body:Byte Ptr)
	Local b:TBody = TBody.FromPtr(body)
	
	If OldMouseRelativeX <> 0 Then
		b.setOmega(Vec3(0, OldMouseRelativeX, 0))
	Else
   		b.setOmega(Vec3())
	End If



	 
	 b.addLocalForce(Vec3(Strafe * 1000, 0, Walk * 1000) , Vec3())
	 b.addForce(b.calculateGravity())
	 
	 Local velocity:TVector3 = b.getVelocity()
	
	 'If our velocity vector gets larger than 5, make it 5.
	 If velocity.length() > 5 Then
	 	velocity.normalise()
		velocity.mulEqWithScalar(5)
	 End If
	 

	 If Walk = 0 And Strafe = 0 Then
	 	velocity.setZ(0)
		velocity.setX(0)
	 End If
	 	 
	 b.setVelocity(velocity)

End Function

REM
	bbdoc: Defines a walking tank instance.
ENDREM
Type TWalkingTank

	Field entity:TEntity
	Const meshName:String = "walkie.mesh"

	REM
		bbdoc: Creates the walking tank entity.
	ENDREM
	Function Create(name:String, scale:Float = 0.25)
		
		'Create basic ogre entity
		entity = fG.LoadMesh(name, meshName)
		fG.scaleEntity(entity, scale, scale, scale)
		
		'Set up the physics body and pin it so that it is always upright		
		Global walkerBody:TBody = fP.addPhysicsEllipsoid(RobotWalker, 50, Vec3(scale * 4, scale * 4, scale * 4) , fG.getNode(RobotWalker).getOrientation() , Vec3(0, scale * 4, 0) , characterCallback)
		Global upVecJoint:TUpVectorJoint = TBasicJoints.CreateUpVector(fP.getWorld() , walkerBody, TVector3.UNIT_Y())
	End Function
	
End Type
