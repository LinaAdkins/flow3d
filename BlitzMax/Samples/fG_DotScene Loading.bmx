REM----------------------------------------------------
fG_DotScene Loading.bmx
Author: Damien Sturdy and Lina Adkins

This demo application shows you how to easily load .scene files exported as OgreMax .scene files and to some extent other .scene file formats. Since loading
a .scene file is rather simplistic, we will also cover the use of callbacks, which will enable you to screen the content that you're loading from the file 
and do a number of things with it.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Use polled input for user input
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()


'Load the resource directory for our .scene file and initialise it
TResourceGroupManager.addResourceLocation("./media/Castlescene", "FileSystem", "General")
TResourceGroupManager.initialiseAllResourceGroups()

'Create and set-up our callback object
Global Callbacks:TOgreMaxSceneCallback = TOgreMaxSceneCallback.Create()

'Here we register all of our functions listed below to our scene callback. 
'To find out what each of the functions listed below does, scroll down to each of them.
Callbacks.initialize(OnCreateNodeCallback, OnCreateCamCallback, OnCreateEntityCallback, OnCreateLightCallback, OnCreateMovableObject, OnProgressChanged)

'Our entity list where we will store a list of entities gotten from the OnCreateEntityCallback
Global entityList:TList = New TList

'Load the .scene file with the initialised callback
fG.LoadDotScene("Castlescene.scene", TSceneLoadOptions.NO_OPTIONS, Null, Null, Callbacks)

'Set the camera in the scene
fG._viewport.setCamera(fG._scene.getCamera("Camera01"))

'Turn off fG's default light
fG._light.setVisible(False)

'Basic Render Loop
Repeat

	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - DotScene Loading - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()


'Summary: This function will be called whenever a node is created from your .scene file. 
'For simplicity's sake, we just print the node's name in the debug log.
Function OnCreateNodeCallback(scene:Byte Ptr, s:Byte Ptr)
	Local sn:TSceneNode = TSceneNode.FromPtr(s)
	DebugLog("Created Node: " + sn.getName())
End Function

'Summary: This function is called every time a camera is loaded from the .scene file.
'Again we will write the camera's name out to the debug log.
Function OnCreateCamCallback(scene:Byte Ptr, c:Byte Ptr)
	Local cam:TCamera = TCamera.FromPtr(c) 
	DebugLog("Created Camera: " + cam.getName())
End Function

'Summary: This function is called every time an entity is created from the .scene file.
'For entities, we will store then all to a global list. This mimics one way that a user
'would commonly use callbacks: to get a list of certain objects from the loaded scene.
Function OnCreateEntityCallback(scene:Byte Ptr, e:Byte Ptr)
	Local entity:TEntity = TEntity.FromPtr(e)
	
	'Save the loaded entity in the global entity list
	entityList.AddLast(entity)
	
	DebugLog("Created Entity: " + entity.getName())
																														
End Function

'Summary: This function is called every time a light is created from the scene file.
'For this example we will change all light output colours to red.
Function OnCreateLightCallback(scene:Byte Ptr, l:Byte Ptr)
	Local light:TLight = TLight.FromPtr(l)
	DebugLog("Created Light: " +  light.getName() )
End Function

'Summary: This function is called for every TMovableObject child that's created in the scene. 
'Note that you may have to cast up using the ".to*" methods ( ex. yourMO.toParticleSystem() ) 
'in conjuction with .getMovableType() to get specialised functions for manipulating particle 
'systems or whichever TMovableObject child that you have.
Function OnCreateMovableObject(scene:Byte Ptr, m:Byte Ptr)
	Local mo:TMovableObject = TMovableObject.FromPtr(m)
	DebugLog("Created MovableObject: " + mo.getName())
End Function

'Summary: This function is called every time loading progress is changed.
'Here we simply display the load percentage in the debug log.
Function OnProgressChanged:Byte(scene:Byte Ptr, progress:Float)
	DebugLog("Load Progress: " + progress)
End Function