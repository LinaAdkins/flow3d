REM----------------------------------------------------
fG_Retreiving Mesh Vertices.bmx
Author: Damien Sturdy and Lina Adkins

This demo will show you how to get started with initialising Flow3D and fG and start buidling your 3d applications as
quickly as possible. Note that with fG init you can use the config window, or specify several options. You don't HAVE 
to use the config window and you should have a look at the fG.init docs to see what you can do with it.

NOTE: This is an incomplete and non-functioning example. getVertexInformation is still undergoing testing and development.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

'Load some robot walker legs and scale them down
Global RobotWalker:TEntity = fG.LoadMesh("RobotWalker", "walkie.mesh")
fG.scaleEntity(RobotWalker, 0.5, 0.5, 0.5)

Global RobotMesh:TMesh = RobotWalker.getMesh()
Global vcount:Int
Global verticeList:TVertexList = TVertexList.Create()

Global icount:Int

Global indexList:TIndexList = TIndexList.Create()


RobotMesh.getVertexInformation(vcount, verticeList, icount, indexList)

Global iter:Int = 0

While(iter < vcount)
	
	DebugLog(verticeList.getValue(iter).toStr())
	iter = iter + 1

Wend

'Basic Render Loop
Repeat

	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Getting Started - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Turn our entity 1 degree along the y-axis every frame ( LOCAL )
	fG.TurnEntity(RobotWalker, 0, 1, 0)
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()