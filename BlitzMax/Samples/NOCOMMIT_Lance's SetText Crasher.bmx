 Import flow.Main

'---------------------------------------------------

'Used polled input.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.SetOption ("vsync", "true")
fG.init ( False, 1024, 768, 32, 0, fG.RS_OPENGL )

'Pull the camera back
Global cam:TSceneNode = fG.GetNode ( fG._camera )
cam.SetPosition ( 25, 25, 25 )
cam.SetFixedYawAxis ( True, 0, 1, 0 )

'Lets rotate a little to start with. (just to make the camera look at the mini that was loaded.)
cam.yaw(160, TS_WORLD)
cam.Pitch(30, TS_LOCAL)
'fG._camera.lookAt ( 0, 0, 0 )

'---------------------------------------------------

fG._light.setVisible ( False )

'---------------------------------------------------

Global Light0:TLight = CreateLum ( "Light0" )
Light0.SetAttenuation ( 10.0, 1.0, 1.0, 1.0 ) 
Light0.SetDiffuseColour ( 0.8, 0.3, 0.3 )
fG.PositionEntity ( Light0, -6, 4, 4 )

'---------------------------------------------------

Global Light1:TLight = CreateLum ( "Light1" )
Light1.SetAttenuation ( 10.0, 1.0, 1.0, 1.0 ) 
Light1.SetDiffuseColour ( 0.3, 0.8, 0.3 )
fG.PositionEntity ( Light1, 6, 4, 4 )

'---------------------------------------------------

Global thisLight:TLight = Light0

'---------------------------------------------------

Global timer:TOgreTimer = fG._ogre.GetTimer()
Global newtime:Float = timer.GetTime()
Global oldtime:Float = timer.GetTime()
Global delta:Float = 0.0

'---------------------------------------------------

'Variables used for mouselook - Blitzmax doesn't have mouse speed funcs!
Global center_x:Int = fG._ogre.GetWidth() / 2
Global center_y:Int = fG._ogre.GetHeight() / 2
Global currMouse_x:Int = fG._ogre.GetWidth() / 2
Global currMouse_y:Int = fG._ogre.GetHeight() / 2

'---------------------------------------------------

'Hide the mouse.
HideMouse()

'---------------------------------------------------

'Initialise the text manager
TTextManager.Init()

'Here we set a default font. If you look in /media/2d Overlays/Fonts you will find .fontdef files, which
'are simple text files that describe the name ( used below ) the font you want to use, and some other parameters.
'You must have a .fontdef file in a resource directory in order to use the font it describes.
TTextManager.SetDefaultFont ( "arial" )

'Now we define a text box. This box can be referred to later, as we will do to set the colour.
TTextManager.AddTextBox ( "newBox", "Hello World!", 10, 10, 50, 20 )

'Now we will change the text colour on the top and bottom, and they will be nicely blended together.
TTextManager.setTextColourTop("newBox", TColourValue.Create ( 1, .8, .8, 1 ) )
TTextManager.setTextColourBottom("newBox", TColourValue.Create ( 1, 1, 1, 1 ) )

'----------------------------------------------------------------------------------------------------------
'----------------------------------------------------------------------------------------------------------

'Basic Render Loop
Repeat

	'Get mouse delta coordintes.
	currMouse_x = MouseX()
	currMouse_y = MouseY()

	'Calculate the speed!
	Local mouserel_x:Float = center_x - currMouse_x
	Local mouserel_y:Float = center_y - currMouse_y

	'Recenter the mouse.
	MoveMouse ( center_x, center_y )
	
	'Transform the camera based on these coords and keyboard activity.
	cam.yaw ( ( mouserel_x * .05 ), TS_WORLD )		'Yaw is always global, so use TS_WORLD.
	cam.pitch ( ( mouserel_y * .05 ), TS_LOCAL )		'Pitch is always local, so use TS_LOCAL.

	'Calculate x and z axes local translation based on keypress.
	Local x:Float = (KeyDown(KEY_D) - KeyDown(KEY_A)) * 0.2
	Local z:Float = (KeyDown(KEY_S) - KeyDown(KEY_W)) * 0.5

	'and translate by that much.
	cam.Translate ( x, 0, z, TS_LOCAL )


	If KeyDown (KEY_RIGHT)
		fg.TranslateEntity ( thisLight, 0, 0, -.1 )
	EndIf

	If KeyDown (KEY_LEFT)
		fg.TranslateEntity ( thisLight, 0, 0, .1 )
	EndIf

	If KeyDown (KEY_UP)
		fg.TranslateEntity ( thisLight, -.1, 0, 0 )
	EndIf

	If KeyDown (KEY_DOWN)
		fg.TranslateEntity ( thisLight, .1, 0, 0 )
	EndIf

	If KeyDown (KEY_PAGEUP)
		fg.TranslateEntity ( thisLight, 0, .1, 0 )
	EndIf

	If KeyDown (KEY_PAGEDOWN)
		fg.TranslateEntity ( thisLight, 0, -.1, 0 )
	EndIf

	If KeyHit (KEY_INSERT)
		If thisLight = Light0
			thisLight = Light1
		Else
			thisLight = Light0
		EndIf
	EndIf

	'Calculate the time since last frame.
	oldtime = newtime
	newtime = timer.GetTime()
	delta = newtime - oldtime

	'Put the FPS and triangle count in the apptitle.
	fG.ChangeAppTitle ("fG - Manual Animated Mesh Creation - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	Global tempString:String
	tempString = "FPS: " + fG.getFPS() + " Tricount: " + fG.TrisRendered()
	TTextManager.SetText ("newBox", tempString)

	'Render at (very) roughly 60fps.
	fG.RenderWorld()
	
Until KeyDown ( KEY_ESCAPE ) Or fG.appTerminated()

'----------------------------------------------------------------------------------------------------------
'----------------------------------------------------------------------------------------------------------

Function CreateLum:TLight ( name$ )

	'Create light.
	Local Light_L:TLight = fG.CreateLight ( name + "_Light", LT_POINT ) 

	'Create node and attach the light.
	Local Light_L_Node:TSceneNode = fG.getNode(Light_L)
	'Light_L_Node.AttachObject ( Light_L ) 

	'Create sphere light representation.
	Local Light_S:TEntity = fG.CreateSphere ( name + "_Sphere" ) 

	'Get the sphere node, detach it from the scene graph, then reattach it to the light's node. 
	Local Light_S_Node:TSceneNode = fG.GetNode ( Light_S )
	Light_S_Node.GetParentSceneNode().RemoveChild ( Light_S_Node ) 
	Light_L_Node.AddChild ( Light_S_Node )

	Return Light_L

End Function
