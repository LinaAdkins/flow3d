REM----------------------------------------------------
fG_ManualParticleSystems.bmx
Author: Damien Sturdy and Lina Adkins

This demo will show you how to create an empty particle system and spawn a few particles.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

'Create our empty particle system and give it a mask texture
Global ps:TParticleSystem = fG.createEmptyParticleSystem("ps")
ps.setMaterialName("PE/lensflare")

'Call to renderWorld to init the created particle system
fG.renderWorld()
                          
'Spawn four particles in a cross shape
'fG.createParticle(TParticleSystem, Direction, Size)
fG.createParticle(ps, Vec3(0, 2, 0) , 1.0)
fG.createParticle(ps, Vec3(0, - 2, 0) , 1.0)
fG.createParticle(ps, Vec3(- 2, 0, 0) , 1.0)
fG.createParticle(ps, Vec3(2, 0, 0) , 1.0)


'Basic Render Loop
Repeat

	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Manual Particles - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()