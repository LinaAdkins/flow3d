REM----------------------------------------------------
fG_Using Billboards.bmx
Author: Damien Sturdy and Lina Adkins

This demo will show you how to place billboard objects into your scene. Billboards are textured quads that will always be facing the active camera during rendering.
For this demo will will be generating a small, random forest of trees onto the standard fG plane. Billboards are created by creating a billboard set, and
then adding individual billboards. Each billboard set is restricted to one material per set, so if you need varied materials you will have to make multiple sets for 
each one, or have multipletextures in one material and use setTexcoordRect(...) on each billboard.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Use polled input for input
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()


'Throw in a plane for them to stand on and give it the standard unit tile material
Global plane:TEntity = fG.createPlane("Plane")
plane.setMaterialName("tile1")


'Create a billboard set that will hold all of our tree billboards
Global billboardSet:TBillboardSet = fG._scene.createBillboardSet("billboardSet", 50)
billboardSet.setMaterialName("BillboardTree")

'Here we enable z-sorting on our billboard set to give it a nicer appearance
billboardSet.setSortingEnabled(True)

'Attach the billboard set to a node
Global billboardNode:TSceneNode = fG._rootnode.createChildSceneNode("billboardNode", 0, 0, 0)
billboardNode.attachObject(billboardSet)


Const MAXTREES:Int = 50
Global treeCount:Int = 0

'Our tree drawing loop
While(treeCount < MAXTREES)

	'Calculate our trees' physical properties
	Local height:Float = Rnd(1, 3)
	Local width:Float = Float(height) / Float(2.0)
	Local x:Float = Rnd(- 4, 4)
	Local z:Float = Rnd(- 4, 4)
	
	'Calculate the rotation and convert it to radians
	Local rot:Float = Rnd(- 3, 3)
	rot = rot *.1

	'Creat the billboard with the proper dimensions and positioning
	Local billboard:TBillboard = billboardSet.createBillboard(Vec3(0, 0, 0) , TColourValue.Create(1, 1, 1, 1))
	billboard.setDimensions(width, height)
	billboard.setPosition(Vec3(x, Float(height) / 2.0, z))
	billboard.SetRotation(TRadian.Create(rot))
	
	'Increment the tree count
	treeCount = treeCount + 1

Wend

'Basic Render Loop
Repeat

	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Using Billboards - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Spin the terrain and the plane to show the billboarding effect
	fG.TurnEntity(billboardSet, 0, 1, 0)
	fG.TurnEntity(plane, 0, 0, 1)
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()
