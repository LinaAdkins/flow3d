REM----------------------------------------------------
FlowPhysics_WorldLeaveCallbacks.bmx
Author: Lina Adkins

This demo shows you how you could potentially handle bodies leaving your world with a leave world callback function. 
Generally you will use this to either destroy rogue entites, or reset important ones back to a starting position. 
In this demo we just spawn boxes into the physics world endlessly and clean them up as they fall outside the small world.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

'Import modules
Import flow.main
Import flow.ogrenewt
EnablePolledInput()

'Init Flow3D and Newton Physics
fG.init()
fG.enableShadows()
fP.enablePhysics()

'Set our world size to something smaller and enable the leave world callback
fP.setWorldSize(Vec3(- 2, 0, - 2) , Vec3(2, 10, 2))
fP.setLeaveWorldCallback(worldLeaveCallback)

'Load a couple sounds for use
Global popSound:TSound = LoadSound("media/Sounds/BubblePop.wav")
Global ringSound:TSound = LoadSound("media/Sounds/FeintRing.wav")

'Create the ground, add texture and physics
groundPlane:TEntity = fG.createPlane("groundPlane")
groundPlane.setMaterialName("tile1")
fP.addPhysics(groundPlane, True)


'Time variables
Global recentTime:Float
Global cachedTime:Float
Global elapsedTime:Float

'Iterator variable ( for unique name generation )
Global i:Int = 0

'Main Loop
Repeat

	'Get the elapsed time!
	recentTime = fG._ogre.getTimer().getTime()
	elapsedTime = elapsedTime + (recentTime - cachedTime)
	cachedTime = recentTime
	
	'Every .7 seconds create a new box!
	If elapsedTime >.7 Then
		'Create our physics cube
		Local cube:TEntity = fG.createCube("cube" + String(i))
		cube.setMaterialName("Red")
		fP.addPhysics(cube, False)
		
		'Play creation sound
		PlaySound(ringSound)
		
		'Update our time and counters
		elapsedTime = 0
		i = i + 1
	End If	

  	'Update Physics, Rendering and AppTitle
	fG.changeAppTitle( "fP - Getting Started - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered() )
	fP.update()
	fG.renderWorld()
	
Until KeyDown(KEY_ESCAPE) Or fG.AppTerminated()

'Summary: This is the world leave callback. It is called for each body that leaves the physics world.
Function worldLeaveCallback(body:Byte Ptr, threadIndex:Int)
	
	'Get the body and attached node.
	Local b:TBody = TBody.FromPtr(body)
	Local node:TSceneNode = b.getOgreNode().ToSceneNode()
	
	'Destroy the body and the entity. We know the box only has entity attached to its node
	'so we can just use 0 for the index.
	b.Destroy()
	fG.FreeEntity(node.getAttachedObject(0))
	
	'Play destroyed sound.
	PlaySound(popSound)
End Function

