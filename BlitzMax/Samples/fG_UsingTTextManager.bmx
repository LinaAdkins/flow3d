Rem----------------------------------------------------
fG_UsingTTextManager
Author: Lina Adkins

This small sample will show you how to draw text on the screen quickly, with the font, size and colour of your choice. In this
basic example we print "Hello World!" with a seperate top and bottom colour.

�2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.Main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

'Initialise the text manager
TTextManager.init()

'Set Character Height
TTextManager.setDefaultCharacterHeight( 50)

'Here we set a default font. If you look in /media/2d Overlays/Fonts you will find .fontdef files, which
'are simple text files that describe the name ( used below ) the font you want to use, and some other parameters.
'You must have a .fontdef file in a resource directory in order to use the font it describes.
TTextManager.setDefaultFont("arial")

'Now we define a text box. This box can be referred to later, as we will do to set the colour.
TTextManager.addTextBox("newBox", "Hello World!", 10, 10, 50, 20)

'Now we will change the text colour on the top and bottom, and they will be nicely blended together.
TTextManager.setTextColourTop("newBox", TColourValue.Create(1, .2, .3, 1.0))
TTextManager.setTextColourBottom("newBox", TColourValue.Create(1, 1, 1, 1))


'Basic Render Loop
Repeat

	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Using TTextManager - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Render
	fG.renderWorld()
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()
