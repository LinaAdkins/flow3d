Rem----------------------------------------------------
fG_CEGUILayoutFiles.bmx
Author: Damien Sturdy and Lina Adkins

This example shows you how to get started using the CEGUI gui built into flow. You will create a small window, then
you will toggle the visibility of an entity. It's important to note that CEGUI has not been simplified into a GUI
object, so for now, the GUI takes a little bit of code to setup as you must set it up just like it is in its native
language. In the final release CEGUI will be simplified and far easier to use.

©2009 BoxSnap Studios

ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

 
'Init the GUI
	'Set up the renderer, font and mouse cursor
	Global guiRenderer:TCEGUIRenderer = TCEGUIRenderer.Create(fG._renderwindow, RENDER_QUEUE_OVERLAY, False, 0, fG._scene)
	Global guiSystem:TCEGUISystem = TCEGUISystem.Create(guiRenderer) 
	TCEGUISchemeManager.loadScheme("TaharezLook.scheme") 
	
	'The .font file is an XML file containing a path to the actual truetype font, so make sure both are correct- 
	TCEGUIFontManager.CreateFont("BlueHighway-12.font") 
	guiSystem.setDefaultFont("BlueHighway-12") 
	guiSystem.setDefaultMouseCursor("TaharezLook", "MouseArrow")
'/Init the GUI
		
		
'Create all the windows used-
	'The GUI Sheet for entire GUI
	Global sheet:TCEGUIWindow = TCEGUIWindowManager.loadWindowLayout("CEGUILayoutFiles_Layout.layout")
	guiSystem.setGUISheet(sheet)
	Global mainButton:TCEGUIPushButton = TCEGUIWindowManager.getPushButton("Quit")
	Global visibleButton:TCEGUIPushButton = TCEGUIWindowManager.getPushButton("ToggleVisible")
'/Create all the windows used-

'Hide the mouse as we will be using a GUI mouse		
HideMouse()

'Add entities to the scene
	'Set up the scene a bit
	fG.enableShadows()
	Global plane:TEntity = fG.createPlane("plane")
	plane.setMaterialName("tile1")
	
	'Load some robot walker legs and scale them down
	Global RobotWalker:TEntity = fG.LoadMesh("RobotWalker", "walkie.mesh")
	fG.scaleEntity(RobotWalker, 0.5, 0.5, 0.5)
'/Add entities to the scene

 
'Variables for calculating relative mouse coords
Global currMouse_x:Int = fG._ogre.getWidth() / 2
Global currMouse_y:Int = fG._ogre.getHeight() / 2
Global mouserel_x:Int = currMouse_x
Global mouserel_y:Int = currMouse_y
Global mousepos_x:Int = MouseX() 
Global mousepos_y:Int = MouseY() 


'Main Loop
Repeat
	'Calculate relative mouse coordinates then inject them into CEGUI
	mousepos_x=currMouse_x
	mousepos_y=currmouse_y
	currMouse_x = MouseX()
	currMouse_y = MouseY()
	mouserel_x = currMouse_x - mousepos_x
	mouserel_y = currMouse_y - mousepos_y
	guiSystem.injectMouseMove(mouserel_x, mouserel_y) 
	
	'Inject our left mouse down and up
	If MouseHit(1) Then
		guiSystem.injectMouseButtonDown(1)
	Else
		guiSystem.injectMouseButtonUp(1)
	End If
	
	
	'If quit button pushed end the app
	If mainButton.isPushed() = True Then End
	
	'Toggle visiblity of our robot
	If visibleButton.isPushed() = True Then
		If RobotWalker.isVisible() Then
			RobotWalker.setVisible(False)
		Else
			RobotWalker.setVisible(True)
		End If
	End If

	
	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Getting Started CEGUI - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())

	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()
 