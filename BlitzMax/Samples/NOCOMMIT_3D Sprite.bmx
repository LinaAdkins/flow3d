Rem—————————————————-
fG_ManualSoftShadows.bmx
Author: Damien Sturdy and Lina Adkins

This sample shows you how to create Soft Shadows the hard way using Shadow Caster and Shadow Receiver materials.
You register the shadow caster materials with the scene manager, and use the shadow receiver materials on anything
that will receive a shadow.  You MUST set shadow receiver materials to use the “diffuse_template” and the materials that
receive shadows MUST use textures.

©2009 BoxSnap Studios
ENDREM————————————————-

Import flow.Main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init(False,1024,768)
fG.Init2D
fG.enableShadows(SHADOWTYPE_STENCIL_MODULATIVE)
'Enable the shadow texture technique   
'fG._scene.setShadowTechnique(SHADOWTYPE_TEXTURE_ADDITIVE_INTEGRATED)
Global spritex:Float = 320
Global spritey:Float = 100

'Load some robot walker legs and scale them down
Global RobotWalker:TEntity = fG.LoadMesh("RobotWalker", "walkie.mesh")
fG.scaleEntity(RobotWalker, 0.5, 0.5, 0.5)
RobotWalker.setMaterialName("ShadowRustedMetal")
RobotWalker.setCastShadows(True)
   
'Throw in a plane for them to stand on and give it the standard unit tile material
Global plane:TEntity = fG.createPlane("Plane")
plane.setMaterialName("ShadowTile1")

'Billboarded plane
Global spritePlane:TEntity = fG.createPlane("spritePlane")
spritePlane.setMaterialName("BillboardTree")
fG.TurnEntity(spritePlane, - 90, 0, 0, TS_PARENT)
fG.TurnEntity(spritePlane, 0 , 0 , 180 , TS_PARENT)
fG.ScaleEntity(spritePlane, .1, 0.1, 0.1)
fG.TranslateEntity(spritePlane, 0, .5, 0, TS_PARENT)
   
'Here we modify the normal fG light to function as a spotlight.
'Soft-shadows/Shadow Textures only work with directional and spotlight types.
Global light:TLight = fG._light
light.setType(TLight.LT_SPOTLIGHT)
fG.getNode(light).lookAt(Vec3(0, 0, 0) , TS_WORLD, Vec3(0, 0, - 1))
fG.PositionEntity(light, 0, 10, - 4)
light.setDiffuseColour(10, 5, 5)
light.setSpotlightInnerAngle(TRadian.Create(1))
light.setSpotlightOuterAngle(TRadian.Create(2))
light.setAttenuation(5000, 1, 1, 1)
light.setDirection(0, - 1, - 1)
light.setCastShadows(True)
   
'Set up soft size by setting shadow texture count, size, the caster material used, etc.
fG._scene.setShadowTextureSelfShadow(True)
fG._scene.setShadowTextureCasterMaterial("shadow_caster")
fG._scene.setShadowTextureCount(4)
fG._scene.setShadowTextureSize(1024)
fG._scene.setShadowTexturePixelFormat(PF_FLOAT16_RGB)
fG._scene.setShadowCasterRenderBackFaces(True)
   
'Set the background colour, etc, for all of the shadow texture targets.
Global numShadowRTTs:Int = fG._scene.getShadowTextureCount()
For Local i:Int = 0 To numShadowRTTs - 1
    Local tex:TTexture = fG._scene.getShadowTexture(i)
    Local vp:TViewport = tex.getBuffer().getRenderTarget().GetViewport(0)
    vp.setBackgroundColour(TColourValue.Create(1, 1, 1, 1))
    vp.setClearEveryFrame(True)
Next
	


				  
Repeat

    'Put the FPS and triangle count in the apptitle
    fG.changeAppTitle("fG - Manual Soft Shadows - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
   
        'Update spritex variable
    If KeyDown(KEY_LEFT)
    	fG.TranslateEntity(spritePlane, .5, 0, 0, TS_PARENT)
    End If
   
    If KeyDown(KEY_RIGHT)
        fG.TranslateEntity(spritePlane, -.5, 0, 0, TS_PARENT)
    End If
    If KeyDown(KEY_UP)
        fG.TranslateEntity(spritePlane, 0, 0, .5, TS_PARENT)
    End If
   
    If KeyDown(KEY_DOWN)
        fG.TranslateEntity(spritePlane, 0, 0, -.5, TS_PARENT)
    End If   

   
    fG.TurnEntity(RobotWalker, 0, 1, 0)
   
    'Render at (very) roughly 60fps
    fG.renderWorld()
    Delay 16.6666667
    fG.Cls()
   
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()

Function planeBillboard(plane:TEntity)
	
End Function
