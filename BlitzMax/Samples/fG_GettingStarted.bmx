REM----------------------------------------------------
fG_GettingStarted.bmx
Author: Damien Sturdy and Lina Adkins

This demo will show you how to get started with initialising Flow3D and fG and start buidling your 3d applications as
quickly as possible. Note that with fG init you can use the config window, or specify several options. You don't HAVE 
to use the config window and you should have a look at the fG.init docs to see what you can do with it.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

Import flow.main

'Used polled input, fG does not FORCE you to use a given input system.
EnablePolledInput()

'Init fG and let the user pick the options from the ogre config window
fG.init()

'Load some robot walker legs and scale them down
Global RobotWalker:TEntity = fG.LoadMesh("RobotWalker", "walkie.mesh")
Global robotnode:TSceneNode = RobotWalker.getParentSceneNode()
fG.scaleEntity(RobotWalker, 0.5, 0.5, 0.5)

'Throw in a plane for them to stand on and give it the standard unit tile material
Global plane:TEntity = fG.createPlane("Plane")
plane.setMaterialName("maxcbsge1")

'Add some quick stencil shadows
fG.enableShadows()

'Main Camera Setup
    fG._camera.getParentSceneNode().setPosition(0, 0, 0)
    fG._camera.getParentSceneNode().setOrientationWithQuaternion(TQuaternion.IDENTITY())
    Global cameraNode:TSceneNode = fG._camera.getParentSceneNode()
    cameraNode.getParentSceneNode().removeChild(cameraNode)
    robotnode.addChild(cameraNode)
    cameraNode.setInheritOrientation(False)

'Variables used for mouselook - Blitzmax doesn't have mouse speed funcs!
    Global MouseXCenter:Float = Float(fG._ogre.getWidth()) / Float(2)
    Global MouseYCenter:Float = Float(fG._ogre.getHeight()) / Float(2)
    Global NewMouseX:Float = Float(fG._ogre.getHeight()) / Float(2)
    Global NewMouseRelativeX:Float = 0.0
    Global NewMouseRelativeY:Float = 0.0
    Global NewMouseRelativeZ:Float = 3.0
    Global OldMouseRelativeZ:Float = 3.0
    Global camera_distance:Float = 3.0
    Global camera_height:Float = 1.0


'Basic Render Loop
Repeat

'Get mouse and relative coords
    NewMouseX = MouseX()
    NewMouseRelativeX = MouseXCenter - NewMouseX

    NewMouseY = MouseY()
    NewMouseRelativeY = MouseYCenter - NewMouseY
    camera_height = camera_height + NewMouseRelativeY *.01
    If camera_height < 0.1 Then camera_height = 0.1
    If camera_height > 10 Then camera_height = 10
	
	Print fG.getNode( RobotWalker ).setOrientationWithQuaternion( fG._camera.getRealOrientation() )
   
'Mouse wheel
    NewMouseRelativeZ = MouseZ()
    If OldMouseRelativeZ < NewMouseRelativeZ Then camera_distance = camera_distance - 1
    If OldMouseRelativeZ > NewMouseRelativeZ Then camera_distance = camera_distance + 1
    If camera_distance < 3 Then camera_distance = 3
    If camera_distance > 10 Then camera_distance = 10
    OldMouseRelativeZ = NewMouseRelativeZ

'Snap mouse back to center
    MoveMouse(MouseXCenter, MouseYCenter)
	
   
'Camera
    fG._camera.setPosition(0, camera_height, camera_distance)
    fG._camera.lookAt(robotnode.getPosition().getX() , robotnode.getPosition().getY() , robotnode.getPosition().getZ())
    fG._camera.pitch(20 - mwheel *.2)    'Look up a bit accordingly to the mouse wheel
    cameraNode.yaw(NewMouseRelativeX *.075, TS_WORLD)

	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Getting Started - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	'Turn our entity 1 degree along the y-axis every frame ( LOCAL )
	'fG.TurnEntity(RobotWalker, 0, 1, 0)
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()
