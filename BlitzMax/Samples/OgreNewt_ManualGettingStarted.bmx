REM----------------------------------------------------
OgreNewt_ManualGettingStarted.bmx
Author: Damien Sturdy and Lina Adkins

This demo will show you how to use raw OgreNewt commands without utilising fP. This demo is similar in setup to 
FlowPhysics_gettingStarted in setup, but is syntactically larger. For simplicity's sake, we will still be utilising
fG for rendering.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

'Import modules
Import flow.main
Import flow.ogrenewt
EnablePolledInput()

'Init Flow3D and Newton Physics
fG.init()
fG.enableShadows()

'Create our TOgreNewtWorld, specifying the bounds.
Global world:TOgreNewtWorld = TOgreNewtWorld.Create()
world.setWorldSize(TVector3.Create(- 10000.0, - 10000.0, - 10000.0) , TVector3.Create(10000.0, 10000.0, 10000.0))

'Create the ground, add texture. Grab the node for later use.
groundPlane:TEntity = fG.createPlane("groundPlane")
groundPlane.setMaterialName("tile1")
Global groundNode:TSceneNode = fG.getNode(groundPlane)

'Create the physical properties of this plane by creating a collision hull
'Then attaching it to a body. Tree collisions are complex collisions that do not move.
Global groundCol:TTreeCollision = TTreeCollision.CreateWithSceneNode(world, groundNode, 1)
Global groundBody:TBody = TBody.Create(world, groundCol)
groundBody.attachNode(groundNode)
groundBody.setPositionOrientation(groundNode.getPosition(), groundNode.getOrientation())


'Create a group of 25 physics cubes.
Global i:Int = 0
While i < 50

	'Increment
	i = i + 1

	'Create the cube, add texture. Grab the node for later use.
	cube:TEntity = fG.createCube("cube" + String(i))
	cube.setMaterialName("Red")
	fG.TranslateEntity(cube, 0, i * 2, 0)
	Local cubeNode:TSceneNode = fG.getNode(cube)
	
	'Create the physical properties of the cube
	
	'Set up the collision as an auto-generated convex hull and create a body from it
	Local cubeCollision:TCollision = TCollision.createConvexHull(world, cubeNode, 0, 0, 0)
	Local cubeBody:TBody = TBody.Create(world, cubeCollision)
	cubeBody.setPositionOrientation(cubeNode.getPosition() , cubeNode.getOrientation())
	
	'Calculate the center of mass and moment of inertia for the convex hull.
	Local com:TVector3 = Vec3()
	Local moi:TVector3 = Vec3()
	cubeCollision.calculateInertialMatrix(moi, com)
	
	'Inertia * Mass to get the proper MOI
	Local mass:Float = 10
	moi.mulEqWithScalar(mass)
	
	'Set the COM and mass matrix for this body
	cubeBody.setCenterOfMass(com)
	cubeBody.setMassMatrix(mass, moi)
	
	'Keep the body from freezing give it gravity.
	cubeBody.setAutoSleep(False)
	cubeBody.setStandardForceCallback()
	
	'Attach the body to the scene node.
	cubeBody.attachNode(cubeNode)
	
	'/Cube Physics Setup
Wend
	

'Physics timer variables
Global elapsed:Float
Global timer:TOgreTimer = fG._Ogre.getTimer()
Global delta:Float = 0
Global CurrentTime:Float = 0
Global previousTime:Float = 0

'Physics Framerate
Global frameRate:Float = 1.0 / 60.0

Repeat
	
	'Get the elapsed and delta( time between this update and last ) times.
	CurrentTime = timer.getTime() 
	delta = CurrentTime - previousTime
	previousTime = CurrentTime
	elapsed = elapsed + delta
	

	'Update the physics using time-slices. We use the same timesplice and update the world
	'multiple times with it to achieve our goal. If we changed the timeslice the physics
	'would become non-deterministic and this is undesireable.
	
	'If the elapsed time is higher than the physics framerate, yet less that 25 updates
	If elapsed > frameRate And elapsed < frameRate * 25
	
		'Update the required number of slices.
		While elapsed > frameRate
			world.update(frameRate) 
			elapsed = elapsed - frameRate	
		Wend
	Else
		'Not enough time has passed for an update-
		If elapsed < frameRate
			'Empty-
		
		'Too much time has passed for an update, update as far as we can ahead.
		Else
			world.update(elapsed) 
			elapsed = 0
		End If
		
	End If
	
	'/Physics Update
	
	'Update the AppTitle
	fG.changeAppTitle("OgreNewt - Getting Started Manually - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())

	'Render a frame
	fG.renderworld()


Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()