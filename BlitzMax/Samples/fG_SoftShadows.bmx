REM----------------------------------------------------
fG_SoftShadows.bmx
Author: Damien Sturdy and Lina Adkins

This soft shadows demo shows you how to enable soft-shadows quickly in your applications. There are a few caveats to using
soft shadows and you should know them before utilising them in your applications. These soft shadows work off of texture
based shadows, that is, they render the shadows to textures and then apply these textures to "shadow receivers". Shadow
receiver materials can be set up in a custom material for your object. For some basic examples look at SoftShadows.material
in /media/materials/SoftShadows.material.

Please note that your material must also include a texture unit that is aliased to ambient_tex and diffuse_tex. Texture
shadows require that you use materials with texture units, inherently.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

'Import Modules
Import flow.main
EnablePolledInput()

'Init
fG.init()

'Create the robot and give it a shadow texture receiving material.
Global RobotWalker:TEntity = fG.LoadMesh("RobotWalker", "walkie.mesh")
fG.scaleEntity(RobotWalker, 0.5, 0.5, 0.5)
RobotWalker.setMaterialName("ShadowRustedMetal")
	
'Create the plane and give it a shadow texture receiving material.
Global plane:TEntity = fG.createPlane("Plane")
plane.setMaterialName("ShadowTile1")

'Enable soft shadows with the default settings.	
fG.enableSoftShadows(True)


Repeat
	'Put the FPS and triangle count in the apptitle
	fG.changeAppTitle("fG - Soft Shadows - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
	
	fG.TurnEntity(RobotWalker, 0, 1, 0)
	
	'Render at (very) roughly 60fps
	fG.renderWorld()
	Delay 16.6666667
	
Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()