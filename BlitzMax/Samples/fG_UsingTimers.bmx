REM----------------------------------------------------
fG_userTimers.bmx
Author: Damien Sturdy and Lina Adkins

This demo gives you a basic idea of how to handle time using Ogre's main timer ( goten here through fG._ogre.getTimer() ). 
This basic demo shows you how to calculate elapsed and delta times and then render only once per a set fps interval.

©2009 BoxSnap Studios
ENDREM-------------------------------------------------

'Init Flow3D
Import flow.main
EnablePolledInput()
fG.init(True)

'Something to look at!
fG.createPlane("newPlane")

'Set-up our timing vars
Global timer:TOgreTimer = fG._ogre.getTimer() 
Global newtime:Float = timer.getTime() 
Global oldtime:Float = timer.getTime() 
Global delta:Float = 0
Global fps:Float = Float(1) / Float(60) 
Global elapsed:Float = 0

'Main Loop
Repeat

	'Update our times
	oldtime = newtime
	newtime = timer.getTime() 
	delta = newtime - oldtime

	elapsed = elapsed + delta
	
	'Render the world once every 1/60th a second - Set elapsed back to 0 so we can start for the next frame.
	If elapsed > fps Then
		fG.renderWorld()
		fG.changeAppTitle("fG - Using Timers - FPS : " + fG.getFPS() + " Tricount: " + fG.TrisRendered())
		elapsed = 0
	End If
	
	

Until KeyDown(KEY_ESCAPE) Or fG.appTerminated()
