#ifndef H_OGRENEWTBASICRAYCASTINFO
#define H_OGRENEWTBASICRAYCASTINFO

EXPORTFUNC OgreNewt::BasicRaycast::BasicRaycastInfo* OGRENEWTBRINFO_create()
{
	return new OgreNewt::BasicRaycast::BasicRaycastInfo();
}

EXPORTFUNC void OGRENEWTBRINFO_delete( OgreNewt::BasicRaycast::BasicRaycastInfo* i )
{
	delete i;
}


EXPORTFUNC float OGRENEWTBRINFO_getDistance( OgreNewt::BasicRaycast::BasicRaycastInfo* i )
{
	return i->mDistance;
}

EXPORTFUNC OgreNewt::Body* OGRENEWTBRINFO_getBody( OgreNewt::BasicRaycast::BasicRaycastInfo* i )
{
	return i->mBody;
}

EXPORTFUNC int OGRENEWTBRINFO_getCollisionID( OgreNewt::BasicRaycast::BasicRaycastInfo* i )
{
	return i->mCollisionID;
}

EXPORTFUNC Ogre::Vector3* OGRENEWTBRINFO_getNormal( OgreNewt::BasicRaycast::BasicRaycastInfo* i )
{
	return new Ogre::Vector3( i->mNormal );
}



#endif
