#ifndef H__OGRENEWT
#define H__OGRENEWT

#define EXPORTFUNC extern "C" __declspec(dllexport)

#include "OgreNewt.h"
#include "_Joint.h"
#include "_CollisionPrimitives.h"
#include "ogreNewtRaycast.h"
#include "ogreNewtBasicRaycast.h"
#include "ogreNewtBasicRaycastInfo.h"
#include "ogreNewtBody.h"
#include "ogreNewtWorld.h"
#include "ogreNewtDebugger.h"
#include "ogreNewtMaterialPair.h"
#include "ogreNewtMaterialID.h"
#include "ogreNewtContactCallback.h"
#include "TContactIterator.h"
#include "TContact.h"


#endif
