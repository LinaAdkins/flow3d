#ifndef H_TBALLANDSOCKET
#define H_TBALLANDSOCKET

EXPORTFUNC OgreNewt::BasicJoints::BallAndSocket* OGRENEWTBAS_create( OgreNewt::World* world , OgreNewt::Body* child , OgreNewt::Body* parent , Ogre::Vector3* pos )
{
	return new OgreNewt::BasicJoints::BallAndSocket( world, child, parent, *pos );
}

EXPORTFUNC Ogre::Vector3* OGRENEWTBAS_getJointAngle( OgreNewt::BasicJoints::BallAndSocket* b )
{ 
	return new Ogre::Vector3( b->getJointAngle() );
}

EXPORTFUNC Ogre::Vector3* OGRENEWTBAS_getJointOmega( OgreNewt::BasicJoints::BallAndSocket* b )
{ 
	return new Ogre::Vector3( b->getJointOmega() );
}

EXPORTFUNC Ogre::Vector3* OGRENEWTBAS_getJointForce( OgreNewt::BasicJoints::BallAndSocket* b )
{ 
	return new Ogre::Vector3( b->getJointForce() );
}

EXPORTFUNC void OGRENEWTBAS_setLimits( Ogre::Vector3* pin , Ogre::Radian* maxCone , Ogre::Radian* maxTwist , OgreNewt::BasicJoints::BallAndSocket* b )
{
	b->setLimits( *pin , *maxCone , *maxTwist );
}



#endif
