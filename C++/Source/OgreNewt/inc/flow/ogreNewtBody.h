#ifndef H_OGRENEWTBODY
#define H_OGRENEWTBODY

EXPORTFUNC OgreNewt::Body* BODY_create( OgreNewt::World* W , OgreNewt::Collision* col , int bodytype )
{
	return new OgreNewt::Body( W , col , bodytype );
}

EXPORTFUNC void BODY_destroy( OgreNewt::Body* b )
{
	delete b;
}

EXPORTFUNC void BODY_setUserData( Ogre::UserDefinedObject* data , OgreNewt::Body* b )
{
	b->setUserData( data );
}

EXPORTFUNC Ogre::UserDefinedObject* BODY_getUserData( OgreNewt::Body* b )
{
	return b->getUserData();
}

EXPORTFUNC bool BODY_hasUserData( OgreNewt::Body* b  )
{ 
	return b->hasUserData();
}

EXPORTFUNC void BODY_clearUserData( OgreNewt::Body* b )
{
	b->clearUserData();
}

//NewtonBody * 	getNewtonBody () const	// Unsupported-

EXPORTFUNC Ogre::Node* BODY_getOgreNode(  OgreNewt::Body* b ) 
{
	return b->getOgreNode();
}
EXPORTFUNC const OgreNewt::World* BODY_getWorld( OgreNewt::Body* b )
{
	return b->getWorld();
}
EXPORTFUNC void BODY_setType( int type , OgreNewt::Body* b )
{
	b->setType( type );
}
EXPORTFUNC int BODY_getType( OgreNewt::Body* b )
{
	return b->getType();
}

EXPORTFUNC void BODY_attachNode( Ogre::Node* node , OgreNewt::Body* b )
{
	b->attachNode( node );
}
EXPORTFUNC void BODY_setStandardForceCallBack( OgreNewt::Body* b )
{
	b->setStandardForceCallback();
}

//void 	setCustomForceAndTorqueCallback (ForceCallback callback) // REDUNDANT


EXPORTFUNC void BODY_setCustomForceAndTorqueCallback( void (*callback)( OgreNewt::Body* b , float timeStep , int threadIndex ) , OgreNewt::Body* b )
{
	b->setCustomForceAndTorqueCallback( callback );
}

EXPORTFUNC void BODY_removeForceAndTorqueCallback( OgreNewt::Body* b )
{
	b->removeForceAndTorqueCallback();
}
//void 	setCustomTransformCallback (TransformCallback callback) // REDUNDANT

EXPORTFUNC void BODY_setCustomTransformCallback( void(*callback)(OgreNewt::Body*, const Ogre::Quaternion*, const Ogre::Vector3*, int threadIndex), OgreNewt::Body* b )
{
	b->setCustomTransformCallback( callback );
}

EXPORTFUNC void BODY_removeTransformCallback( OgreNewt::Body* b )
{
	b->removeTransformCallback();
}


EXPORTFUNC void BODY_setPositionOrientation( Ogre::Vector3* pos , Ogre::Quaternion* orient , OgreNewt::Body* b )
{
	b->setPositionOrientation( *pos ,  *orient );
}

EXPORTFUNC void BODY_setMassMatrix(float mass , Ogre::Vector3* inertia , OgreNewt::Body* b)
{
	b->setMassMatrix( mass , *inertia );
}

EXPORTFUNC void BODY_setCenterOfMass( Ogre::Vector3* centerOfMass , OgreNewt::Body* b  )
{
	b->setCenterOfMass( *centerOfMass );
}

EXPORTFUNC void BODY_setContinuousCollisionMode( unsigned int state , OgreNewt::Body* b )
{
	b->setContinuousCollisionMode( state );
}

EXPORTFUNC void BODY_setJointRecursiveCollision( unsigned int state , OgreNewt::Body* b )
{
	b->setJointRecursiveCollision( state );
}

EXPORTFUNC void BODY_setAutoSleep( int flag , OgreNewt::Body* b )
{
	b->setAutoSleep(flag);
}

EXPORTFUNC int BODY_getAutoSleep( OgreNewt::Body* b )
{
	return b->getAutoSleep();
}

EXPORTFUNC void BODY_setSleepState( int state , OgreNewt::Body* b )
{
	b->setSleepState( state );
}

EXPORTFUNC int BODY_getSleepState( OgreNewt::Body* b )
{
	return b->getSleepState();
}


EXPORTFUNC void BODY_getMassMatrix( float mass , Ogre::Vector3* inertia , OgreNewt::Body* b )
{
	b->getMassMatrix( mass , *inertia );
}

EXPORTFUNC void BODY_setOmega( Ogre::Vector3* omega , OgreNewt::Body* ogreNewtBody )
{
	ogreNewtBody->setOmega( *omega );
}

EXPORTFUNC Ogre::Vector3* BODY_getOmega( OgreNewt::Body* b )
{
	return new Ogre::Vector3( b->getOmega() );
}

EXPORTFUNC void BODY_getPositionOrientation( Ogre::Vector3* pos , Ogre::Quaternion* orientation , OgreNewt::Body* ogreNewtBody )
{
	ogreNewtBody->getPositionOrientation( *pos , *orientation );
}


EXPORTFUNC void BODY_addForce( Ogre::Vector3* force , OgreNewt::Body* ogreBody )
{
	ogreBody->addForce( *force );
}

EXPORTFUNC void BODY_addLocalForce( Ogre::Vector3* force , Ogre::Vector3* pos , OgreNewt::Body* b )
{
	b->addLocalForce( *force , *pos  );
}

EXPORTFUNC void BODY_addTorque( Ogre::Vector3* torque , OgreNewt::Body* b )
{
	b->addTorque( *torque );
}

EXPORTFUNC void BODY_setTorque( Ogre::Vector3* torque , OgreNewt::Body* b )
{
	b->setTorque( *torque );
}

EXPORTFUNC Ogre::Vector3* BODY_getTorque( OgreNewt::Body* b )
{
	return new Ogre::Vector3( b->getTorque() );
}

EXPORTFUNC void BODY_setVelocity( Ogre::Vector3* vel , OgreNewt::Body* ogreBody)
{
	ogreBody->setVelocity( *vel );
}

EXPORTFUNC Ogre::Vector3* BODY_getVelocity( OgreNewt::Body* body )
{
	return new Ogre::Vector3( body->getVelocity() );
}

EXPORTFUNC const OgreNewt::MaterialID* BODY_getMaterialGroupID( OgreNewt::Body* body )
{
	return body->getMaterialGroupID();
}

EXPORTFUNC void BODY_setMaterialGroupID( const OgreNewt::MaterialID* ID , OgreNewt::Body* body )
{
	body->setMaterialGroupID( ID );
}

EXPORTFUNC void BODY_addImpulse( Ogre::Vector3* deltav , Ogre::Vector3* posit , OgreNewt::Body* b )
{
	b->addImpulse( *deltav , *posit );
}

EXPORTFUNC void BODY_addBuoyancyForce( float fluidDensity , float fluidLinearViscosity , 
						    float fluidAngularViscosity , Ogre::Vector3* gravity , 
						    bool (*callback)(int, OgreNewt::Body*, Ogre::Quaternion*, Ogre::Vector3*, Ogre::Plane*), 
							OgreNewt::Body* b )
{
	b->addBouyancyForce( fluidDensity , fluidLinearViscosity ,fluidAngularViscosity , *gravity , callback );
}

EXPORTFUNC Ogre::Vector3* BODY_getPosition( OgreNewt::Body* b )
{
	Ogre::Vector3* vec = new Ogre::Vector3();
	Ogre::Quaternion q;
	b->getPositionOrientation( *vec , q);
	return vec;
}

EXPORTFUNC Ogre::Vector3* BODY_calculateGravity( OgreNewt::Body* b )
{
	float mass;
	Ogre::Vector3 inertia;
	b->getMassMatrix(mass , inertia); 
	Ogre::Vector3 gravity = Ogre::Vector3(0,-9.8,0)*mass;
	return new Ogre::Vector3( gravity );
}


//void BODY_addBouyancyForce( float fluidDensity , float fluidLinearViscosity , float fluidAngularViscosity , Ogre::Vector3* gravity , ::c

//OgreNewt::Body::addBouyancyForce(Ogre::Real fluidDensity, Ogre::Real fluidLinearViscosity, Ogre::Real fluidAngularViscosity, const Ogre::Vector3 &gravity, boost::function<bool(c*,int,Body*,const Ogre::Quaternion&,const Ogre::Vector3&,Ogre::Plane&)> callback, c *instancedClassPointer)
//OgreNewt::Body::addBouyancyForce(Ogre::Real fluidDensity, Ogre::Real fluidLinearViscosity, Ogre::Real fluidAngularViscosity, const Ogre::Vector3 &gravity, OgreNewt::Body::buoyancyPlaneCallback callback)



#endif
