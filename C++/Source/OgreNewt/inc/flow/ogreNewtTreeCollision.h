#ifndef H_OGRENEWTTREECOLLISION
#define H_OGRENEWTTREECOLLISION

EXPORTFUNC OgreNewt::CollisionPrimitives::TreeCollision* OGRENEWTTC_create( OgreNewt::World* world )
{
	OgreNewt::CollisionPrimitives::TreeCollision* tc = new OgreNewt::CollisionPrimitives::TreeCollision( world );
	return tc;
}

EXPORTFUNC OgreNewt::CollisionPrimitives::TreeCollision* OGRENEWTTC_createWithSceneNode( OgreNewt::World* world , Ogre::SceneNode* sn , bool optimize , int fw )
{
	OgreNewt::CollisionPrimitives::TreeCollision* tc = new OgreNewt::CollisionPrimitives::TreeCollision( world , sn , optimize , ( OgreNewt::CollisionPrimitives::FaceWinding ) fw );
	return tc;
}

// These are not necessary at the moment-
//TreeCollision (const World *world, int numVertices, int numIndices, const float *vertices, const int *indices, bool optimize, FaceWinding fw=FW_DEFAULT)
//TreeCollision (const World *world, int numVertices, Ogre::Vector3 *vertices, Ogre::IndexData *indexData, bool optimize, FaceWinding fw=FW_DEFAULT)
//virtual 	~TreeCollision ()

EXPORTFUNC void OGRENEWTTC_start( OgreNewt::CollisionPrimitives::TreeCollision* tc )
{
	tc->start();
}

EXPORTFUNC void OGRENEWTTC_addPoly( Ogre::Vector3* polys , int ID , OgreNewt::CollisionPrimitives::TreeCollision* tc )
{
	tc->addPoly( polys , ID );
}

EXPORTFUNC void OGRENEWTTC_finish( OgreNewt::CollisionPrimitives::TreeCollision* tc )
{
	tc->finish();
}


#endif
