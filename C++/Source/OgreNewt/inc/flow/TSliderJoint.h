#ifndef H_TSLIDERJOINT
#define H_TSLIDERJOINT

EXPORTFUNC OgreNewt::BasicJoints::Slider* OGRENEWTSLIDER_create( OgreNewt::World* world , OgreNewt::Body* child , OgreNewt::Body* parent , Ogre::Vector3* pos , Ogre::Vector3* pin )
{
	return new OgreNewt::BasicJoints::Slider( world , child , parent , *pos , *pin );
}

EXPORTFUNC float OGRENEWTSLIDER_getJointPosit( OgreNewt::BasicJoints::Slider* s )
{
	return s->getJointPosit();
}

EXPORTFUNC float OGRENEWTSLIDER_getJointVeloc( OgreNewt::BasicJoints::Slider* s )
{
	return s->getJointVeloc();
}

EXPORTFUNC Ogre::Vector3* OGRENEWTSLIDER_getJointForce( OgreNewt::BasicJoints::Slider* s )
{
	return new Ogre::Vector3( s->getJointForce() );
}

EXPORTFUNC void OGRENEWTSLIDER_setCallback( void(*SliderCallback)(OgreNewt::BasicJoints::Slider* me ) , OgreNewt::BasicJoints::Slider* s )
{
	s->setCallback( SliderCallback );
}

EXPORTFUNC void OGRENEWTSLIDER_setCallbackAccel( float accel , OgreNewt::BasicJoints::Slider* s )
{
	s->setCallbackAccel( accel );
}

EXPORTFUNC void OGRENEWTSLIDER_setCallbackFrictionMin( float min , OgreNewt::BasicJoints::Slider* s )
{
	s->setCallbackFrictionMin( min );
}

EXPORTFUNC void OGRENEWTSLIDER_setCallbackFrictionMax( float max , OgreNewt::BasicJoints::Slider* s )
{
	s->setCallbackFrictionMax( max );
}

EXPORTFUNC float OGRENEWTSLIDER_getCallbackTimestep( OgreNewt::BasicJoints::Slider* s )
{
	return s->getCallbackTimestep();
}

EXPORTFUNC float OGRENEWTSLIDER_calculateStopAccel( float dist , OgreNewt::BasicJoints::Slider* s )
{
	return s->calculateStopAccel( dist );
}

#endif
