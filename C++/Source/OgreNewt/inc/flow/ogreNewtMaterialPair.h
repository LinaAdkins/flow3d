#ifndef H_OGRENEWTMATERIALPAIR
#define H_OGRENEWTMATERIALPAIR

EXPORTFUNC OgreNewt::MaterialPair* OGRENEWTMP_create( OgreNewt::World* world , OgreNewt::MaterialID* mat1 , OgreNewt::MaterialID* mat2 )
{
	OgreNewt::MaterialPair* mp = new OgreNewt::MaterialPair( world , mat1 , mat2 );
	return mp;
}

EXPORTFUNC void OGRENEWTMP_delete( OgreNewt::MaterialPair* mp )
{
	delete mp;
}

EXPORTFUNC void OGRENEWTMP_setDefaultSoftness( float softness , OgreNewt::MaterialPair* mp )
{
	mp->setDefaultSoftness( softness );
}

EXPORTFUNC void OGRENEWTMP_setDefaultElasticity( float elasticity , OgreNewt::MaterialPair* mp )
{
	mp->setDefaultElasticity( elasticity );
}

EXPORTFUNC void OGRENEWTMP_setDefaultCollidable( int state , OgreNewt::MaterialPair* mp )
{
	mp->setDefaultCollidable( state );
}

EXPORTFUNC void OGRENEWTMP_setDefaultFriction( float stat , float kinetic , OgreNewt::MaterialPair* mp )
{
	mp->setDefaultFriction( stat , kinetic );
}

EXPORTFUNC void OGRENEWTMP_setContinuousCollisionMode( int state , OgreNewt::MaterialPair* mp )
{
	mp->setContinuousCollisionMode( state );
}

EXPORTFUNC void OGRENEWTMP_setContactCallback( OgreNewt::ContactCallback* callback , OgreNewt::MaterialPair* mp )
{
	mp->setContactCallback( callback );
}

#endif
