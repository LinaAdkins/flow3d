#ifndef H_OGRENEWTWORLD
#define H_OGRENEWTWORLD

EXPORTFUNC OgreNewt::World* OGRENEWTWORLD_create()
{
	return new OgreNewt::World();
}

EXPORTFUNC void OGRENEWTWORLD_destroy( OgreNewt::World* ogreNewtWorld )
{
	delete ogreNewtWorld;
}

EXPORTFUNC void OGRENEWTWORLD_update( float t_step , OgreNewt::World* w )
{
	w->update( t_step );
}

EXPORTFUNC void OGRENEWTWORLD_invalidateCache( OgreNewt::World* w )
{
	w->invalidateCache();
}

EXPORTFUNC const NewtonWorld* OGRENEWTWORLD_getNewtonWorld( OgreNewt::World* w )
{
	return w->getNewtonWorld();
}

EXPORTFUNC const OgreNewt::MaterialID* OGRENEWTWORLD_getDefaultMaterialID( OgreNewt::World* w )
{
	return w->getDefaultMaterialID();
}

EXPORTFUNC void OGRENEWTWORLD_destroyAllBodies( OgreNewt::World* w )
{
	w->destroyAllBodies();
}

EXPORTFUNC void OGRENEWTWORLD_setSolverModel( int model , OgreNewt::World* w )
{
	w->setSolverModel( model );
}

EXPORTFUNC void OGRENEWTWORLD_setFrictionModel( int model , OgreNewt::World* w )
{
	w->setFrictionModel( model );
}

EXPORTFUNC void OGRENEWTWORLD_setPlatformArchitecture( int mode , OgreNewt::World* w )
{
	w->setPlatformArchitecture( mode );
}

EXPORTFUNC int OGRENEWTWORLD_getBodyCount( OgreNewt::World* w )
{
	return w->getBodyCount();
}

EXPORTFUNC void OGRENEWTWORLD_setMultithreadSolverOnSingleIsland( int mode , OgreNewt::World* w )
{
	w->setMultithreadSolverOnSingleIsland( mode );
}

EXPORTFUNC void OGRENEWTWORLD_setThreadCount( int threads , OgreNewt::World* w )
{
	w->setThreadCount( threads );
}

EXPORTFUNC int OGRENEWTWORLD_getThreadCount( OgreNewt::World* w )
{ 
	return w->getThreadCount();
}

EXPORTFUNC void OGRENEWTWORLD_criticalSectionLock( OgreNewt::World* w )
{
	w->criticalSectionLock();
}

EXPORTFUNC void OGRENEWTWORLD_criticalSectionUnlock( OgreNewt::World* w )
{
	w->criticalSectionUnlock();
}

EXPORTFUNC void OGRENEWTWORLD_setMinimumFrameRate( float frame, OgreNewt::World* w )
{
	w->setMinimumFrameRate( frame );
}

EXPORTFUNC void OGRENEWTWORLD_setWorldSize( Ogre::Vector3* min , Ogre::Vector3* max , OgreNewt::World* w )
{
	w->setWorldSize( *min , *max );
}

EXPORTFUNC void OGRENEWTWORLD_setWorldSizeWithAAB( Ogre::AxisAlignedBox* aab , OgreNewt::World* w )
{
	w->setWorldSize( *aab );
}

EXPORTFUNC Ogre::AxisAlignedBox* OGRENEWTWORLD_getWorldSize( OgreNewt::World* w )
{
	return new Ogre::AxisAlignedBox( w->getWorldSize() );
}

EXPORTFUNC const float OGRENEWTWORLD_getVersion( OgreNewt::World* ogreNewtWorld )
{
	return ogreNewtWorld->getVersion();
}


//OgreNewt::World::setLeaveWorldCallback(boost::function<void(c*,Body*)> callback, c *instancedClassPointer)


EXPORTFUNC void OGRENEWTWORLD_setLeaveWorldCallback( void(*callback)(OgreNewt::Body* , int threadIndex ) , OgreNewt::World* w )
{
	w->setLeaveWorldCallback(callback);
}





















#endif
