#ifndef H_OGRENEWTCOLLISION
#define H_OGRENEWTCOLLISION

EXPORTFUNC OgreNewt::Collision* OGRENEWTCOL_createBox( OgreNewt::World* world , Ogre::Vector3* size , Ogre::Quaternion* orient , Ogre::Vector3* pos )
{
	return new OgreNewt::CollisionPrimitives::Box( world , *size , *orient , *pos );
}

EXPORTFUNC OgreNewt::Collision* OGRENEWTCOL_createConvexHull( OgreNewt::World* ogreNewtWorld , Ogre::SceneNode* ogreSceneNode , float x,  float y , float z )
{
	OgreNewt::Collision* col = new OgreNewt::CollisionPrimitives::ConvexHull( ogreNewtWorld , ogreSceneNode );
	return col;
}

EXPORTFUNC OgreNewt::Collision* OGRENEWTCOL_createEllipsoid( OgreNewt::World* ogreNewtWorld , Ogre::Vector3* size , Ogre::Quaternion* orient , Ogre::Vector3* pos )
{
	OgreNewt::Collision* col = new OgreNewt::CollisionPrimitives::Ellipsoid( ogreNewtWorld , *size , *orient , *pos );
	return col;
}

EXPORTFUNC OgreNewt::Collision* OGRENEWTCOL_createCylinder( OgreNewt::World* world , float radius , float height , Ogre::Quaternion* orient , Ogre::Vector3* pos  )
{
	OgreNewt::Collision* col = new OgreNewt::CollisionPrimitives::Cylinder( world , radius , height , *orient , *pos ); 
	return col;
}

EXPORTFUNC OgreNewt::Collision* OGRENEWTCOL_createTreeCollisionSceneParser( OgreNewt::World* world )
{
	OgreNewt::Collision* col = new OgreNewt::CollisionPrimitives::TreeCollisionSceneParser( world );
	return col;
}

EXPORTFUNC void OGRENEWTCOL_calculateInertialMatrix( Ogre::Vector3* inertia , Ogre::Vector3* offset , OgreNewt::ConvexCollision* c )
{
	c->calculateInertialMatrix( *inertia , *offset );
}


EXPORTFUNC void OGRENEWTCOL_destroy( OgreNewt::Collision* ogreNewtCollision )
{
	delete ogreNewtCollision;
}

#endif
