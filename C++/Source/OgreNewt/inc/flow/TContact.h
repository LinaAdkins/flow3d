#ifndef H_TCONTACT
#define H_TCONTACT

// 	Contact (NewtonJoint *contactjoint, void *contact) - User should never instance this manually

EXPORTFUNC void CONTACT_removeContact( OgreNewt::Contact* c )
{
	c->removeContact();
}

EXPORTFUNC unsigned int CONTACT_getContactFaceAttribute( OgreNewt::Contact* c )
{
	return c->getContactFaceAttribute();
}

EXPORTFUNC unsigned int CONTACT_getBodyCollisionID( OgreNewt::Body* body , OgreNewt::Contact* c )
{
	return c->getBodyCollisionID( body );
}

EXPORTFUNC float CONTACT_getContactNormalSpeed( OgreNewt::Contact* c )
{
	return c->getContactNormalSpeed();
}

EXPORTFUNC Ogre::Vector3* CONTACT_getContactForce( OgreNewt::Contact* c )
{
	return new Ogre::Vector3( c->getContactForce() );
}

EXPORTFUNC void CONTACT_getContactPositionAndNormal( Ogre::Vector3* pos , Ogre::Vector3* norm , OgreNewt::Contact* c )
{
	c->getContactPositionAndNormal( *pos , *norm );
}

EXPORTFUNC void CONTACT_getContactTangentDirections( Ogre::Vector3* dir0 , Ogre::Vector3* dir1 , OgreNewt::Contact* c )
{
	c->getContactTangentDirections( *dir0 , *dir1 );
}

EXPORTFUNC float CONTACT_getContactTangentSpeed( int index , OgreNewt::Contact* c )
{
	return c->getContactTangentSpeed( index );
}

EXPORTFUNC void CONTACT_setContactSoftness( float softness , OgreNewt::Contact* c )
{
	c->setContactSoftness( softness );
}

EXPORTFUNC void CONTACT_setContactElasticity( float elasticity , OgreNewt::Contact* c )
{
	c->setContactElasticity( elasticity );
}

EXPORTFUNC void CONTACT_setContactFrictionState( int state , int index , OgreNewt::Contact* c )
{
	c->setContactFrictionState( state , index );
}

EXPORTFUNC void CONTACT_setContactFrictionCoef( float stat , float kinetic , int index , OgreNewt::Contact* c )
{
	c->setContactFrictionCoef( stat , kinetic , index );
} 

EXPORTFUNC void CONTACT_setContactTangentAcceleration( float accel , int index , OgreNewt::Contact* c )
{
	c->setContactTangentAcceleration( accel , index );
}

EXPORTFUNC void CONTACT_rotateTangentDirections( Ogre::Vector3* dir , OgreNewt::Contact* c )
{
	c->rotateTangentDirections( *dir );
}

EXPORTFUNC void CONTACT_setContactNormalDirection( Ogre::Vector3* dir , OgreNewt::Contact* c )
{
	c->setContactNormalDirection( *dir );
}

EXPORTFUNC void CONTACT_setContactNormalAcceleration( float accel , OgreNewt::Contact* c )
{
	c->setContactNormalAcceleration( accel );
}








#endif
