#ifndef H_OGRENEWTCONTACTCALLBACK
#define H_OGRENEWTCONTACTCALLBACK

class CustomCollision : public OgreNewt::ContactCallback
{
	int(*userBeginFunc)(int threadIndex);
	void(*userProcessFunc)(OgreNewt::ContactIterator* , float timeStep , int threadIndex);
	

public:
	void initialise( int(*userBeginMaxFunc)(int threadIndex) , void(*userProcessMaxFunc)(OgreNewt::ContactIterator* , float timeStep , int threadIndex) )
	{
		this->userBeginFunc = userBeginMaxFunc;
		this->userProcessFunc = userProcessMaxFunc;
	}

	int userBegin( int threadIndex) {
		return userBeginFunc(threadIndex);
	}

	void userProcess(OgreNewt::ContactIterator contactIterator , float timeStep , int threadIndex) { 
		userProcessFunc(&contactIterator , timeStep , threadIndex);
	}

	OgreNewt::Body* getBody0(){
		return this->m_body0;
	}

	OgreNewt::Body* getBody1(){
		return this->m_body1;
	}
};

EXPORTFUNC CustomCollision* CONTACTCALLBACK_create( int(*userBeginMaxFunc)(int threadIndex) , void(*userProcessMaxFunc)(OgreNewt::ContactIterator* , float timeStep , int threadIndex))
{	
	CustomCollision* c = new CustomCollision();
	c->initialise( userBeginMaxFunc , userProcessMaxFunc );
	return c;
}

EXPORTFUNC void CONTACTCALLBACK_delete( CustomCollision* c )
{
	delete c;
}

EXPORTFUNC OgreNewt::Body* CONTACTCALLBACK_getBody0( CustomCollision* c )
{
	return c->getBody0();
}

EXPORTFUNC OgreNewt::Body* CONTACTCALLBACK_getBody1( CustomCollision* c )
{
	return c->getBody1();
}



#endif
