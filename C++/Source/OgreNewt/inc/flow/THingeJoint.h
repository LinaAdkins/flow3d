#ifndef H_THINGEJOINT
#define H_THINGEJOINT

EXPORTFUNC OgreNewt::BasicJoints::Hinge* OGRENEWTHINGE_create( OgreNewt::World* world , OgreNewt::Body* child , OgreNewt::Body* parent , Ogre::Vector3* pos , Ogre::Vector3* pin )
{
	return new OgreNewt::BasicJoints::Hinge( world , child , parent , *pos , *pin );
}

EXPORTFUNC Ogre::Radian* OGRENEWTHINGE_getJointAngle( OgreNewt::BasicJoints::Hinge* h )
{
	return new Ogre::Radian( h->getJointAngle() );
}

EXPORTFUNC float OGRENEWTHINGE_getJointOmega( OgreNewt::BasicJoints::Hinge* h )
{
	return h->getJointOmega();
}

EXPORTFUNC Ogre::Vector3* OGRENEWTHINGE_getJointForce( OgreNewt::BasicJoints::Hinge* h )
{
	return new Ogre::Vector3( h->getJointForce() );
}

EXPORTFUNC void OGRENEWTHINGE_setCallback( void(*HingeCallback)(OgreNewt::BasicJoints::Hinge* me ) , OgreNewt::BasicJoints::Hinge* h )
{
	h->setCallback( HingeCallback );
}

EXPORTFUNC void OGRENEWTHINGE_setCallbackAccel( float accel , OgreNewt::BasicJoints::Hinge* h )
{
	h->setCallbackAccel( accel );
}

EXPORTFUNC void OGRENEWTHINGE_setCallbackFrictionMin( float min , OgreNewt::BasicJoints::Hinge* h )
{
	h->setCallbackFrictionMin( min );
}

EXPORTFUNC void OGRENEWTHINGE_setCallbackFrictionMax( float max , OgreNewt::BasicJoints::Hinge* h )
{
	h->setCallbackFrictionMax( max );
}

EXPORTFUNC float OGRENEWTHINGE_getCallbackTimestep( OgreNewt::BasicJoints::Hinge* h )
{
	return h->getCallbackTimestep();
}

EXPORTFUNC float OGRENEWTHINGE_calculateStopAlpha( Ogre::Radian* angle , OgreNewt::BasicJoints::Hinge* h )
{
	return h->calculateStopAlpha( *angle );
}




#endif
