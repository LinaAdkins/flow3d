#ifndef H_TUPVECTORJOINT
#define H_TUPVECTORJOINT

EXPORTFUNC OgreNewt::BasicJoints::UpVector* OGRENEWTUV_create( OgreNewt::World* world , OgreNewt::Body* body , Ogre::Vector3* pin )
{
	return new OgreNewt::BasicJoints::UpVector( world , body , *pin );
}

EXPORTFUNC void OGRENEWTUV_setPin( Ogre::Vector3* pin , OgreNewt::BasicJoints::UpVector* u )
{
	u->setPin( *pin );
}

EXPORTFUNC Ogre::Vector3* OGRENEWTUV_getPin( OgreNewt::BasicJoints::UpVector* u )
{
	return new Ogre::Vector3( u->getPin() );
}

#endif
