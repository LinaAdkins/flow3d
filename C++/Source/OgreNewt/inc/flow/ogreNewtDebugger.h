#ifndef H_OGRENEWTDEBUGGER
#define H_OGRENEWTDEBUGGER

EXPORTFUNC void OGRENEWTDEBUGGER_init( Ogre::SceneManager* smgr )
{
	OgreNewt::Debugger::getSingleton().init( smgr );
}

EXPORTFUNC void OGRENEWTDEBUGGER_deInit()
{
	OgreNewt::Debugger::getSingleton().deInit();
}

EXPORTFUNC void OGRENEWTDEBUGGER_showLines( OgreNewt::World* world )
{
	OgreNewt::Debugger::getSingleton().showLines( world );
}

EXPORTFUNC void OGRENEWTDEBUGGER_hideLines()
{
	OgreNewt::Debugger::getSingleton().hideLines();
}

#endif
