#ifndef H_TUNIVERSALJOINT
#define H_TUNIVERSALJOINT

EXPORTFUNC OgreNewt::BasicJoints::Universal* OGRENEWTUNIVERSAL_create( OgreNewt::World* world , OgreNewt::Body* child , OgreNewt::Body* parent , Ogre::Vector3* pos , Ogre::Vector3* pin0 , Ogre::Vector3* pin1 )
{
	return new OgreNewt::BasicJoints::Universal( world , child , parent , *pos , *pin0 , *pin1 );
}

EXPORTFUNC Ogre::Radian* OGRENEWTUNIVERSAL_getJointAngle0( OgreNewt::BasicJoints::Universal* u )
{
	return new Ogre::Radian( u->getJointAngle0() );
}

EXPORTFUNC Ogre::Radian* OGRENEWTUNIVERSAL_getJointAngle1( OgreNewt::BasicJoints::Universal* u )
{
	return new Ogre::Radian( u->getJointAngle1() );
}

EXPORTFUNC float OGRENEWTUNIVERSAL_getJointOmega0( OgreNewt::BasicJoints::Universal* u )
{
	return u->getJointOmega0();
}

EXPORTFUNC float OGRENEWTUNIVERSAL_getJointOmega1( OgreNewt::BasicJoints::Universal* u )
{
	return u->getJointOmega1();
}

EXPORTFUNC Ogre::Vector3* OGRENEWTUNIVERSAL_getJointForce( OgreNewt::BasicJoints::Universal* u )
{
	return new Ogre::Vector3( u->getJointForce() );
}

EXPORTFUNC void OGRENEWTUNIVERSAL_setCallback( void(*UniversalCallback)(OgreNewt::BasicJoints::Universal* me) , OgreNewt::BasicJoints::Universal* u )
{
	u->setCallback(UniversalCallback);
}

EXPORTFUNC void OGRENEWTUNIVERSAL_setCallbackAccel( float accel , int axis , OgreNewt::BasicJoints::Universal* u )
{
	u->setCallbackAccel(accel , axis );
}

EXPORTFUNC void OGRENEWTUNIVERSAL_setCallbackFrictionMin( float min , int axis , OgreNewt::BasicJoints::Universal* u )
{
	u->setCallbackFrictionMin( min , axis );
}

EXPORTFUNC void OGRENEWTUNIVERSAL_setCallbackFrictionMax( float max , int axis , OgreNewt::BasicJoints::Universal* u )
{
	u->setCallbackFrictionMax( max , axis );
}

EXPORTFUNC float OGRENEWTUNIVERSAL_getCallbackTimestep( OgreNewt::BasicJoints::Universal* u )
{
	return u->getCallbackTimestep();
}

EXPORTFUNC float OGRENEWTUNIVERSAL_calculateStopAlpha0( float angle , OgreNewt::BasicJoints::Universal* u )
{
	return u->calculateStopAlpha0( angle );
}

EXPORTFUNC float OGRENEWTUNIVERSAL_calculateStopAlpha1( float angle , OgreNewt::BasicJoints::Universal* u )
{
	return u->calculateStopAlpha1( angle );
}

#endif
