#ifndef H_OGRENEWTBASICRAYCAST
#define H_OGRENEWTBASICRAYCAST

EXPORTFUNC OgreNewt::BasicRaycast* OGRENEWTBR_create( OgreNewt::World* world , Ogre::Vector3* startpt , Ogre::Vector3* endpt )
{
	return new OgreNewt::BasicRaycast( world , *startpt , *endpt );

}

EXPORTFUNC void OGRENEWTBR_delete( OgreNewt::BasicRaycast* BasicRaycast )
{
	delete BasicRaycast;
}

//bool 	userCallback (Body *body, Ogre::Real distance, const Ogre::Vector3 &normal, int collisionID)
//Requires overloading the class to use it, at some point maybe implement setUserCallback custom implementation

EXPORTFUNC int OGRENEWTBR_getHitCount( OgreNewt::BasicRaycast* BasicRaycast )
{
	return BasicRaycast->getHitCount();
}

EXPORTFUNC OgreNewt::BasicRaycast::BasicRaycastInfo* OGRENEWTBR_getInfoAt( int pos , OgreNewt::BasicRaycast* BasicRaycast )
{
	return new OgreNewt::BasicRaycast::BasicRaycastInfo( BasicRaycast->getInfoAt( pos ) );
}

EXPORTFUNC OgreNewt::BasicRaycast::BasicRaycastInfo* OGRENEWTBR_getFirstHit( OgreNewt::BasicRaycast* BasicRaycast )
{
	return new OgreNewt::BasicRaycast::BasicRaycastInfo( BasicRaycast->getFirstHit() );

}




#endif
