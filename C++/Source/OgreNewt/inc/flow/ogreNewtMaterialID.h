#ifndef H_OGRENEWTMATERIALID
#define H_OGRENEWTMATERIALID

EXPORTFUNC OgreNewt::MaterialID* OGRENEWTMID_create( OgreNewt::World* world )
{
	OgreNewt::MaterialID* mid = new OgreNewt::MaterialID( world );
	return mid;
}

// Internal use only-
// MaterialID( const World *world , int ID )

EXPORTFUNC void OGRENEWTMID_delete( OgreNewt::MaterialID* ID )
{
	delete ID;
}

EXPORTFUNC int OGRENEWTMID_getID( OgreNewt::MaterialID* ID )
{
	return ID->getID();
}

#endif
