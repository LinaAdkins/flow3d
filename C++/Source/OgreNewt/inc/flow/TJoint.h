#ifndef H_TJOINT
#define H_TJOINT

EXPORTFUNC OgreNewt::Joint* OGRENEWTJOINT_create()
{
	return new OgreNewt::Joint();
}

EXPORTFUNC void OGRENEWTJOINT_delete( OgreNewt::Joint* j )
{
	delete j;
}

EXPORTFUNC int OGRENEWTJOINT_getCollisionState( OgreNewt::Joint* j )
{
	return j->getCollisionState();
}

EXPORTFUNC void OGRENEWTJOINT_setCollisionState( int state , OgreNewt::Joint* j )
{
	j->setCollisionState( state );
}

EXPORTFUNC float OGRENEWTJOINT_getStiffness( OgreNewt::Joint* j )
{
	return j->getStiffness();
}

EXPORTFUNC void OGRENEWTJOINT_setStiffness( float stiffness , OgreNewt::Joint* j )
{
	j->setStiffness( stiffness );
}

EXPORTFUNC Ogre::UserDefinedObject* OGRENEWTJOINT_getUserData(  OgreNewt::Joint* j )
{
	return j->getUserData();
}

EXPORTFUNC void OGRENEWTJOINT_setUserData( Ogre::UserDefinedObject* ptr , OgreNewt::Joint* j )
{
	j->setUserData( ptr );
}

EXPORTFUNC void OGRENEWTJOINT_clearUserData( OgreNewt::Joint* j )
{
	j->clearUserData();
}

EXPORTFUNC bool OGRENEWTJOINT_hasUserData( OgreNewt::Joint* j )
{
	return j->hasUserData();
}



#endif
