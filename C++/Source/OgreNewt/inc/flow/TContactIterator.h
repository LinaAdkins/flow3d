#ifndef H_TCONTACTITERATOR
#define H_TCONTACTITERATOR

//ContactIterator (NewtonJoint *contactjoint) - User should not construct these directly, only use them in the userProcess func.
EXPORTFUNC OgreNewt::Contact*  CI_getCurrent( OgreNewt::ContactIterator* i )
{
	return new OgreNewt::Contact( i->getCurrent() );
}

EXPORTFUNC bool CI_moveNext( OgreNewt::ContactIterator* i )
{
	return i->moveNext();
}

EXPORTFUNC void CI_reset( OgreNewt::ContactIterator* i )
{
	i->reset();
}

EXPORTFUNC bool CI_hasMoreElements( OgreNewt::ContactIterator* i )
{
	return i->hasMoreElements();
}

EXPORTFUNC int CI_getContactCount( OgreNewt::ContactIterator* i )
{
	return i->getContactCount();
}

#endif
