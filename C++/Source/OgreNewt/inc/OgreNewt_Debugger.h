/* 
	OgreNewt Library

	Ogre implementation of Newton Game Dynamics SDK

	OgreNewt basically has no license, you may use any or all of the library however you desire... I hope it can help you in any way.

		by Walaber

*/
#ifndef _INCLUDE_OGRENEWT_DEBUGGER
#define _INCLUDE_OGRENEWT_DEBUGGER


#include <Ogre.h>
#include <Newton.h>
#include "OgreNewt_World.h"

namespace OgreNewt
{

	//! For viewing the Newton rigid bodies visually.
	/*!
		This class implements a debug view of the Newton world.  it is a Singleton!
	*/
	class _OgreNewtExport Debugger
	{
	
	public:
		~Debugger();

		//! get the singleton reference
		static Debugger& getSingleton();

		//! init the debugger.
		/*
			\param smgr pointer to your Ogre::SceneManager
		*/
		void init( Ogre::SceneManager* smgr );

		//! de-init the debugger (cleanup)
		void deInit();

		//! show the newton world
		/*!
			Draws the Newton world as 3D lines.  immediately draws the world as of the time of calling the function.
			For a constant view of the world, call this function every frame.
			Static bodies are shown in light grey.
			Dynamic bodies are bright yellow when active, dull yellow when inactive (asleep).

			\param world pointer to the OgreNewt::World
			\param clearPrevious should any existing lines that have been drawn be cleared? defaults to TRUE.
		*/
		void showLines( OgreNewt::World* world, bool clearPrevious = true );

		//! show all contacts in the world.
		/*!
			Draws all current contacts between bodies in the world as 3D lines.
			The contact normal is drawn in RED.
			Tangent vector 0 is BLUE.
			Tangent vector 1 is GREEN.

			\param world pointer to the OgreNewt::World
			\param clearPrevious should any existing lines that have been drawn be cleared? defaults to FALSE.
		*/
		void showContacts( OgreNewt::World* world, bool clearPrevious = false );

		//! remove lines drawn.
		void hideLines();
	
	protected:
		Debugger();
	
	private:
		Ogre::SceneManager*		m_smgr;
		Ogre::SceneNode*		m_debugnode;
		Ogre::ManualObject*		m_debuglines;
	
		Ogre::Real				m_contactscale;

		Ogre::ColourValue		m_colorstatic;
		Ogre::ColourValue		m_coloractiveawake;
		Ogre::ColourValue		m_coloractiveasleep;

		static Ogre::ColourValue	m_currentcolor;

		void newtonPerBody( NewtonBody* body );
		static void _CDECL newtonPerPoly( void* userData, int vertexCount, const float* faceVertec, int id );
	};

}	// end namespace OgreNewt


#endif	// _INCLUDE_OGRENEWT_DEBUGGER

