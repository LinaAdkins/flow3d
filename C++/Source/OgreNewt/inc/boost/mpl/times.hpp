
#ifndef BOOST_MPL_TIMES_HPP_INCLUDED
#define BOOST_MPL_TIMES_HPP_INCLUDED

// Copyright Aleksey Gurtovoy 2000-2004
//
// Distributed under the Boost Software License, Version 1.0. 
// (See accompanying file LICENSE_1_0.txt or copy at 
// http://www.boost.org/LICENSE_1_0.txt)
//
// See http://www.boost.org/libs/mpl for documentation.

// $Source: /cvsroot/ogre/ogreaddons/ogrenewt/OgreNewt_Main/inc/boost/mpl/times.hpp,v $
// $Date: 2006/04/17 23:49:40 $
// $Revision: 1.1 $

#define AUX778076_OP_NAME times
#define AUX778076_OP_TOKEN *
#include <boost/mpl/aux_/arithmetic_op.hpp>

#endif // BOOST_MPL_TIMES_HPP_INCLUDED
