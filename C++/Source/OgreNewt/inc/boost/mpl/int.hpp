
#ifndef BOOST_MPL_INT_HPP_INCLUDED
#define BOOST_MPL_INT_HPP_INCLUDED

// Copyright Aleksey Gurtovoy 2000-2004
//
// Distributed under the Boost Software License, Version 1.0. 
// (See accompanying file LICENSE_1_0.txt or copy at 
// http://www.boost.org/LICENSE_1_0.txt)
//
// See http://www.boost.org/libs/mpl for documentation.

// $Source: /cvsroot/ogre/ogreaddons/ogrenewt/OgreNewt_Main/inc/boost/mpl/int.hpp,v $
// $Date: 2006/04/17 23:48:05 $
// $Revision: 1.1 $

#include <boost/mpl/int_fwd.hpp>

#define AUX_WRAPPER_VALUE_TYPE int
#include <boost/mpl/aux_/integral_wrapper.hpp>

#endif // BOOST_MPL_INT_HPP_INCLUDED
