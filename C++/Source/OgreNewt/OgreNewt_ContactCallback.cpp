#include <OgreNewt_ContactCallback.h>
#include <OgreNewt_Body.h>

namespace OgreNewt
{


	
//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
Contact::Contact(NewtonJoint *contactjoint, void *contact)
{
	m_contactjoint = contactjoint;
	m_contact = contact;
	m_material = NewtonContactGetMaterial( m_contact );
}


Ogre::Vector3 Contact::getContactForce() const 
{
	Ogre::Vector3 force;
	NewtonMaterialGetContactForce( m_material, &force.x );
	return force;
}


void Contact::getContactPositionAndNormal( Ogre::Vector3& pos, Ogre::Vector3& norm ) const 
{
	NewtonMaterialGetContactPositionAndNormal( m_material, &pos.x, &norm.x );
}


void Contact::getContactTangentDirections( Ogre::Vector3& dir0, Ogre::Vector3& dir1 ) const 
{
	NewtonMaterialGetContactTangentDirections( m_material, &dir0.x, &dir1.x );
}


//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
ContactIterator::ContactIterator(NewtonJoint *contactjoint)
{
	m_contactjoint = contactjoint;
	m_currentcontact = NewtonContactJointGetFirstContact( m_contactjoint );
}

Contact ContactIterator::getCurrent()
{
	return Contact( m_contactjoint, m_currentcontact );
}

bool ContactIterator::moveNext()
{
	m_currentcontact = NewtonContactJointGetNextContact( m_contactjoint, m_currentcontact );

	if (m_currentcontact)
		return true;
	else
		return false;
}

void ContactIterator::reset()
{
	m_currentcontact = NewtonContactJointGetFirstContact( m_contactjoint );
}

bool ContactIterator::hasMoreElements()
{
	void* next = NewtonContactJointGetNextContact( m_contactjoint, m_currentcontact );
	if (next)
		return true;
	else
		return false;
}


//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------
ContactCallback::ContactCallback()
{

}

ContactCallback::~ContactCallback()
{
}

int _CDECL ContactCallback::contactBegin( const NewtonMaterial* material, const NewtonBody* body0, const NewtonBody* body1, int threadIndex )
{
	ContactCallback* me;

	me = (ContactCallback*)NewtonMaterialGetMaterialPairUserData( material );

	//save the bodies...
	me->m_body0 = (OgreNewt::Body*)NewtonBodyGetUserData( body0 );
	me->m_body1 = (OgreNewt::Body*)NewtonBodyGetUserData( body1 );

	return me->userBegin( threadIndex );
}

void _CDECL ContactCallback::contactProcess( const NewtonJoint* contact, dFloat timestep, int threadIndex)
{
	ContactCallback *me;

	// get the material from the first contact in the joint, to get back to our callback!
	void* first_c = NewtonContactJointGetFirstContact(contact);
	NewtonMaterial* mat = NewtonContactGetMaterial( first_c );

	me = (ContactCallback*)NewtonMaterialGetMaterialPairUserData( mat );

	me->m_body0 = (OgreNewt::Body*)NewtonBodyGetUserData( NewtonJointGetBody0(contact) );
	me->m_body1 = (OgreNewt::Body*)NewtonBodyGetUserData( NewtonJointGetBody1(contact) );

	// call the user-defined callback function!
	me->userProcess( ContactIterator((NewtonJoint*)contact), (Ogre::Real)timestep, threadIndex );
}




}

