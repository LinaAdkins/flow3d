#include "OgreNewt_Debugger.h"
#include "OgreNewt_Body.h"
#include "OgreNewt_ContactCallback.h"

namespace OgreNewt
{

//////////////////////////////////////////////////////////
// DEUBBER FUNCTIONS
//////////////////////////////////////////////////////////
Ogre::ColourValue Debugger::m_currentcolor = Ogre::ColourValue(0.8f, 0.8f, 0.8f);

Debugger::Debugger()
{
	m_debuglines = NULL;
	m_debugnode = NULL;

	m_contactscale = 0.5f;

	m_colorstatic = Ogre::ColourValue(0.8f, 0.8f, 0.8f);
	m_coloractiveawake = Ogre::ColourValue(0.9f, 0.9f, 0.0f);
	m_coloractiveasleep = Ogre::ColourValue(0.6f, 0.6f, 0.0f);
}

Debugger::~Debugger()
{
	Debugger::getSingleton().deInit();
}

Debugger& Debugger::getSingleton()
{
	static Debugger instance;
	return instance;
}
		
void Debugger::init( Ogre::SceneManager* smgr )
{
	m_smgr = smgr;

	m_debugnode = smgr->getRootSceneNode()->createChildSceneNode("__OgreNewt__Debugger__");
	m_debuglines = smgr->createManualObject("__OgreNewt__Debugger__");

	m_debugnode->attachObject(m_debuglines);
}

void Debugger::deInit()
{
	if (m_debugnode)
	{
		m_debugnode->detachAllObjects();
		if (m_debuglines) { m_smgr->destroyManualObject(m_debuglines); m_debuglines = NULL; }
		m_debugnode->getParentSceneNode()->removeAndDestroyChild( m_debugnode->getName() );
		m_debugnode = NULL;
	}
}


void Debugger::showLines( OgreNewt::World* world, bool clearPrevious )
{
	
	if (clearPrevious)
		m_debuglines->clear();

	m_debuglines->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST );

    // make the new lines.
	for (NewtonBody* b = NewtonWorldGetFirstBody(world->getNewtonWorld()); b; b = NewtonWorldGetNextBody(world->getNewtonWorld(), b))
	{
		// set color.
		Ogre::Real mass;
		Ogre::Vector3 inertia;
		Body* body = (Body*)NewtonBodyGetUserData(b);

		body->getMassMatrix(mass, inertia);

		if (mass == 0.0f)
			m_currentcolor = m_colorstatic;
		else
			m_currentcolor = (body->getSleepState() == 0) ? m_coloractiveawake : m_coloractiveasleep;
			
		newtonPerBody(b);
	}

    m_debuglines->end();

}

void Debugger::showContacts( OgreNewt::World* world, bool clearPrevious )
{
	if (clearPrevious)
		m_debuglines->clear();

	m_debuglines->begin("BaseWhiteNoLighting", Ogre::RenderOperation::OT_LINE_LIST );

	// using straight Newton functions here...
	for (NewtonBody* b = NewtonWorldGetFirstBody(world->getNewtonWorld()); b;
		b = NewtonWorldGetNextBody(world->getNewtonWorld(), b))
	{
		for (NewtonJoint* j = NewtonBodyGetFirstContactJoint(b); j; 
			j = NewtonBodyGetNextContactJoint(b, j))
		{
			ContactIterator it(j);

			do
			{
				Ogre::Vector3 pos;
				Ogre::Vector3 norm;
				Ogre::Vector3 tan0, tan1;

				it.getCurrent().getContactPositionAndNormal(pos, norm);
				it.getCurrent().getContactTangentDirections(tan0, tan1);

				// draw this contact!
				// NORMAL (RED)
				m_debuglines->position( pos );
				m_debuglines->colour( Ogre::ColourValue::Red );

				m_debuglines->position( pos + (norm * m_contactscale) );
				m_debuglines->colour( Ogre::ColourValue::Red );

				// TANGENT0 (BLUE)
				m_debuglines->position( pos );
				m_debuglines->colour( Ogre::ColourValue::Blue );

				m_debuglines->position( pos + (tan0 * m_contactscale) );
				m_debuglines->colour( Ogre::ColourValue::Blue );

				// TANGENT0 (GREEN)
				m_debuglines->position( pos );
				m_debuglines->colour( Ogre::ColourValue::Green );

				m_debuglines->position( pos + (tan1 * m_contactscale) );
				m_debuglines->colour( Ogre::ColourValue::Green );
				
			}
			while (it.moveNext());

		}
	}

	m_debuglines->end();
}

void Debugger::hideLines()
{
	// erase any existing lines!
	m_debuglines->clear(); 
}


void Debugger::newtonPerBody( NewtonBody* body )
{
	float matrix[16];
	NewtonBodyGetMatrix(body, &matrix[0]);
	NewtonCollisionForEachPolygonDo( NewtonBodyGetCollision(body), &matrix[0], newtonPerPoly, NULL );
}

void _CDECL Debugger::newtonPerPoly( void* userData, int vertexCount, const float* faceVertec, int id )
{
	Ogre::Vector3 p0, p1;

	int i= vertexCount - 1;
	p0 = Ogre::Vector3( faceVertec[(i*3) + 0], faceVertec[(i*3) + 1], faceVertec[(i*3) + 2] );

	for (i=0;i<vertexCount;i++)
	{
		p1 = Ogre::Vector3( faceVertec[(i*3) + 0], faceVertec[(i*3) + 1], faceVertec[(i*3) + 2] );

		Debugger::getSingleton().m_debuglines->position( p0 );
		Debugger::getSingleton().m_debuglines->colour( m_currentcolor );

		Debugger::getSingleton().m_debuglines->position( p1 );
		Debugger::getSingleton().m_debuglines->colour( m_currentcolor );

		p0 = p1;
	}
}




}	// end namespace OgreNewt
