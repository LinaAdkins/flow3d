/**********************************************************************
 *<
	oFusion Scene Serializer Pro (see License.txt)

	FILE: oSceneSerializer.cpp

	DESCRIPTION: Contains the OSM Scene serializer implementation code

	IMPLEMENTED BY: Andres Carrera

 *>	Copyright (c) 2006, All Rights Reserved.
 **********************************************************************/

#include "oSceneSerializer.h"
#include "oSceneObjects.h"

using namespace Ogre;

enum {
	SCENE_SKYPLANE = 1,
	SCENE_SKYBOX,
	SCENE_SKYDOME,
};

OSMSerializer::OSMSerializer(SceneManager* pSceneMgr, RenderWindow* win)
{
	// Use specific scene manager if defined
	if(pSceneMgr) {
		mSceneMgr = pSceneMgr;
	}
	else {
		SceneManagerEnumerator::SceneManagerIterator it = Root::getSingleton().getSceneManagerIterator();
		if(it.hasMoreElements())
			mSceneMgr = it.getNext();
	}

	// Use specific render window if defined
	if(win)	
		mWindow = win;		
	else
		mWindow = Ogre::Root::getSingleton().getAutoCreatedWindow();
	
	// Default export all object and animations
	mFlags = OSM_WRITE_ALL | OSM_WRITE_HIDDEN;

	mCallbacks = 0;
}

bool OSMSerializer::writeScene(const Ogre::String& filename, 
							   OSMSerializerCallbacks* pCallbacks, 
							   UINT precision)
{

	// Hook up callback interface
	mCallbacks = pCallbacks;

	// The precision value for the matrix floats in the scene
	mPrecision = precision;

	LogManager::getSingleton().logMessage("********************************");	
	LogManager::getSingleton().logMessage("**    oScene Serializer Lib   **");
	LogManager::getSingleton().logMessage("********************************");

	String msg("oSceneSerializer: Writing '");
	msg += filename;
	msg += "' file";
	LogManager::getSingleton().logMessage(msg);	

	mFilename = filename;
	
	mXMLDoc.InsertEndChild(TiXmlElement("oe_scene"))->ToElement();	
	mXMLRoot = mXMLDoc.RootElement();

	// Create scene properties if defined
	if( (mFlags & OSM_WRITE_SCENE_PROPS) != 0) {
		addSceneProperties();
	}

	// Create entities if defined
	if((mFlags & OSM_WRITE_ENTITIES) != 0) {

		SceneManager::MovableObjectIterator it = mSceneMgr->getMovableObjectIterator("Entity");

		while(it.hasMoreElements()) {
			Entity* entity = static_cast<Entity*>(it.getNext());

			addEntity(entity);
		}
	}

	// Create lights if defined
	if((mFlags & OSM_WRITE_LIGHTS) != 0) {

		SceneManager::MovableObjectIterator it = mSceneMgr->getMovableObjectIterator("Light");

		while(it.hasMoreElements()) {
			Light* light = static_cast<Light*>(it.getNext());

			addLight(light);
		}
	}

	// Create cameras if defined
	if((mFlags & OSM_WRITE_CAMERAS) != 0) {

		SceneManager::CameraIterator camIt = mSceneMgr->getCameraIterator();

		while(camIt.hasMoreElements()) {
			Camera* camera = static_cast<Camera*>(camIt.getNext());

			addCamera(camera);
		}
	}

	// Create helpers if defined
	if((mFlags & OSM_WRITE_HELPERS) != 0) {

		// Use root scene node to parse for helpers
		SceneNode* rootNode = mSceneMgr->getRootSceneNode();

		addHelpers(rootNode);
	}

	// Create animations (node type) if defined
	if((mFlags & OSM_WRITE_ANIMATIONS) != 0) {

		writeAnimKeys();
	}

	// Save file
	mXMLDoc.SaveFile(filename.c_str());

	LogManager::getSingleton().logMessage("***********************************");
	LogManager::getSingleton().logMessage("** oSceneSerializer: Scene saved **");
	LogManager::getSingleton().logMessage("***********************************");		

	return true;
}

// Set object flags
void OSMSerializer::setSceneFlags(int flags)
{
	mFlags = flags;
}

// Implementation
bool OSMSerializer::addMesh(const MeshPtr& meshPtr, const String& filename)
{
	assert(!meshPtr.isNull());

	if(meshPtr.isNull())
		return false;

	oMesh mesh;

	mesh.filename = filename;
	mesh.meshPtr = meshPtr;

	if(meshPtr->hasSkeleton()) {
		const SkeletonPtr& skel = meshPtr->getSkeleton();

		if(!skel.isNull()) {
			mesh.skel.skelPtr = skel;
			mesh.skel.filename = skel->getName();

			addSkeleton(skel, mesh.skel.filename + ".skeleton");
		}
	}

	bool writeObject = true;

	if(mCallbacks)
		writeObject = mCallbacks->OnWriteMesh(mesh);

	if(writeObject) {
		try {
			Ogre::MeshSerializer meshSerializer;
			meshSerializer.exportMesh(mesh.meshPtr.get(), filename);
		}
		catch(Exception& /*e*/) {
			LogManager::getSingleton().logMessage("Error while writing mesh file");
		}
	}

	return true;
}

bool OSMSerializer::addEntity(Ogre::Entity* entity)
{

	if(!mXMLRoot) return true;

	if( (mFlags & OSM_WRITE_HIDDEN) == 0 && !entity->isVisible())
		return true;

	oEntity entityData;
	entityData.entityInstance = entity;
	entityData.name = entity->getName();

	const MeshPtr& mesh = entity->getMesh();
	Ogre::String meshFilename;

	if(!mesh.isNull()) {
		meshFilename = mesh->getName();
		//meshFilename = entity->getName() + ".mesh";
		addMesh(mesh, meshFilename);
	}

	entityData.mesh.meshPtr = mesh;
	entityData.mesh.filename = meshFilename;

	entityData.node = entity->getParentSceneNode();

	getBaseData(entityData);

	entityData.castShadows = entity->getCastShadows();

	if(!entity->isVisible())
		entityData.isHidden = true;

	bool writeObject = true;

	if(mCallbacks)
		writeObject = mCallbacks->OnWriteEntity(entityData);

	if(writeObject) {
		// Find the list of entities to insert this node on
		TiXmlElement* list = mXMLRoot->FirstChildElement("entities");
		if(list == NULL)
			list = mXMLRoot->InsertEndChild(TiXmlElement("entities"))->ToElement();

		// Insert new mesh in list
		TiXmlElement* meshElem = list->InsertEndChild(TiXmlElement("entity"))->ToElement();
		meshElem->SetAttribute("name", entityData.name.c_str());
		meshElem->SetAttribute("hidden", entityData.isHidden ? "true" : "false");

		meshElem->SetAttribute("filename", entityData.mesh.filename.c_str());

		writeNodeTM(entityData, meshElem);

		// If we have a parent, we write the ID
		if(entityData.parentNode)
			meshElem->SetAttribute("parent", entityData.parentName.c_str());

		meshElem->SetAttribute("CastShadows", entityData.castShadows ? "yes" : "no");
		//meshElem->SetAttribute("ReceiveShadows", (entity.receiveShadows ? "yes" : "no"));

		// Write animation keys
		addAnimations(entityData, meshElem);

		// Write object properties
		addObjectProperties(entityData, meshElem);
	}

	return true;
}

bool OSMSerializer::addCamera(Ogre::Camera* camera)
{

	if(!mXMLRoot) return true;

	oCamera cameraData;
	cameraData.node = camera->getParentSceneNode();
	cameraData.name = camera->getName();
	cameraData.fov = camera->getFOVy().valueRadians();
	cameraData.nearClip = camera->getNearClipDistance();
	cameraData.farClip = camera->getFarClipDistance();
	cameraData.camInstance = camera;

	if(cameraData.node)
		cameraData.targetNode = cameraData.node->getAutoTrackTarget();
	
	if(cameraData.targetNode)
		cameraData.targetName = cameraData.targetNode->getName();

	getBaseData(cameraData);

	bool writeObject = true;

	if(mCallbacks)
		writeObject = mCallbacks->OnWriteCamera(cameraData);

	if(writeObject) {
		// Find the list of cameras to insert this node on
		TiXmlElement* list = mXMLRoot->FirstChildElement("cameras");
		if(list == NULL)
			list = mXMLRoot->InsertEndChild(TiXmlElement("cameras"))->ToElement();

		// Insert camera node in this list
		TiXmlElement* camElem = list->InsertEndChild(TiXmlElement("camera"))->ToElement();
		camElem->SetAttribute("name", cameraData.name.c_str());

		writeNodeTM(cameraData, camElem);
		//writeUserProps(cam.node, camElem);

		// If we have a parent, we write the ID
		if(cameraData.parentNode)
			camElem->SetAttribute("parent", cameraData.parentName);

		camElem->SetAttribute("FOV", Ogre::StringConverter::toString(cameraData.fov));

		TiXmlElement* clipPlane = camElem->InsertEndChild(TiXmlElement("clipPlane"))->ToElement();
		clipPlane->SetAttribute("NEAR", Ogre::StringConverter::toString(cameraData.nearClip));
		clipPlane->SetAttribute("FAR", Ogre::StringConverter::toString(cameraData.farClip));

		// Write object properties
		addObjectProperties(cameraData, camElem);

		// Write animation keys
		addAnimations(cameraData, camElem);

		// If we have a target object, export the target TM too
		if(cameraData.targetNode) {
			TiXmlElement* targetElem = camElem->InsertEndChild(TiXmlElement("target"))->ToElement();
			// Name sould not be needed ..
			targetElem->SetAttribute("name", cameraData.targetName.c_str());

			oBaseObject target;
			target.node = cameraData.targetNode;
			getNodeTM(target);

			writeNodeTM(target, targetElem);
			
			addAnimations(target, targetElem);
		}
	}

	return true;
}

bool OSMSerializer::addLight(Ogre::Light* light)
{
	
	if(!mXMLRoot) return true;

	oLight lightData;
	lightData.lightInstance = light;
	lightData.node = light->getParentSceneNode();
	lightData.name = light->getName();
	lightData.type = light->getType();
	lightData.state = light->isVisible();
	lightData.castShadows = light->getCastShadows();
	lightData.color = light->getDiffuseColour();
	lightData.specular = light->getSpecularColour();
	lightData.range = light->getAttenuationRange();
	lightData.constant = light->getAttenuationConstant();
	lightData.linear = light->getAttenuationLinear();
	lightData.quadratic = light->getAttenuationQuadric();
	lightData.useAtten = false;

	if(lightData.range != 100000 || lightData.constant != 1.0f || 
		lightData.linear != 0.0f || lightData.quadratic != 0.0f)
		lightData.useAtten = true;

	if(lightData.type == Ogre::Light::LT_SPOTLIGHT) {
		lightData.hotSpot = light->getSpotlightInnerAngle().valueDegrees();
		lightData.fallOff = light->getSpotlightOuterAngle().valueDegrees();
		//lightData.hotSpot = light->getSpotlightInnerAngle().valueRadians();
		//lightData.fallOff = light->getSpotlightOuterAngle().valueRadians();
	}

	getBaseData(lightData);

	if(lightData.node) {
		lightData.targetNode = lightData.node->getAutoTrackTarget();

		if(lightData.targetNode)
			lightData.targetName = lightData.targetNode->getName();
	}

	bool writeObject = true;

	if(mCallbacks)
		writeObject = mCallbacks->OnWriteLight(lightData);

	if(writeObject) {

		// Find the list of lights to insert this node on
		TiXmlElement* list = mXMLRoot->FirstChildElement("lights");
		if(list == NULL)
			list = mXMLRoot->InsertEndChild(TiXmlElement("lights"))->ToElement();

		// Create new element
		TiXmlElement* lightElem = list->InsertEndChild(TiXmlElement("light"))->ToElement();
		lightElem->SetAttribute("name", lightData.name.c_str());
		writeNodeTM(lightData, lightElem);
		//writeUserProps(light.node, lightElem);

		// If we have a parent, we write the ID
		if(lightData.parentNode)
			lightElem->SetAttribute("parent", lightData.parentName.c_str());

		// get object state
		switch(lightData.type) {
			case Light::LT_POINT:
				lightElem->SetAttribute("type", "omni");break;							
			case Light::LT_DIRECTIONAL:
				lightElem->SetAttribute("type", "directional");break;
			case Light::LT_SPOTLIGHT:
				lightElem->SetAttribute("type", "spot");break;				
		}

		// Write state of light		
		lightElem->SetAttribute("on", lightData.state ? "true" : "false");

		lightElem->SetAttribute("CastShadows", lightData.castShadows ? "yes" : "no");

		// Write light data
		if(lightData.type != Light::LT_POINT) {
			lightElem->SetAttribute("hotspot", Ogre::StringConverter::toString(lightData.hotSpot));
			lightElem->SetAttribute("falloff", Ogre::StringConverter::toString(lightData.fallOff));
		}

		//lightElem->SetAttribute("intensity", Ogre::StringConverter::toString(light.intensity));

		// Diffuse color
		TiXmlElement* pColor = lightElem->InsertEndChild(TiXmlElement("color"))->ToElement();
		const Ogre::ColourValue& color = lightData.color;
		pColor->SetAttribute("r", Ogre::StringConverter::toString(color.r));
		pColor->SetAttribute("g", Ogre::StringConverter::toString(color.g));
		pColor->SetAttribute("b", Ogre::StringConverter::toString(color.b));

		// Specular color
		const Ogre::ColourValue& specular = lightData.specular;
		pColor = lightElem->InsertEndChild(TiXmlElement("specular"))->ToElement();
		pColor->SetAttribute("r", Ogre::StringConverter::toString(specular.r));
		pColor->SetAttribute("g", Ogre::StringConverter::toString(specular.g));
		pColor->SetAttribute("b", Ogre::StringConverter::toString(specular.b));

		// Write light attenuation settings
		if(lightData.useAtten) {
			TiXmlElement* pAtten = lightElem->InsertEndChild(TiXmlElement("attenuation"))->ToElement();
			pAtten->SetAttribute("range", Ogre::StringConverter::toString(lightData.range));
			pAtten->SetAttribute("constant", Ogre::StringConverter::toString(lightData.constant));
			pAtten->SetAttribute("linear", Ogre::StringConverter::toString(lightData.linear));
			pAtten->SetAttribute("quadratic", Ogre::StringConverter::toString(lightData.quadratic));
		}

		// Write object properties
		addObjectProperties(lightData, lightElem);

		// Write animation keys
		addAnimations(lightData, lightElem);

		// If we have a target object, export the target TM too
		if(lightData.targetNode) {
			TiXmlElement* targetElem = lightElem->InsertEndChild(TiXmlElement("target"))->ToElement();
			targetElem->SetAttribute("name", lightData.targetName.c_str());

			oBaseObject target;
			target.node = lightData.targetNode;
			getNodeTM(target);

			writeNodeTM(target, targetElem);

			// Write animation keys
			addAnimations(target, targetElem);
		}
		
	}

	return true;
}

bool OSMSerializer::addSkeleton(const SkeletonPtr& skel, const String& filename)
{
	assert(!skel.isNull());

	oSkeleton skelData;
	skelData.skelPtr = skel;
	skelData.filename = filename;

	bool writeObject = true;

	if(mCallbacks)
		writeObject = mCallbacks->OnWriteSkeleton(skelData);

	if(skelData.skelPtr.isNull())
		return false;

	if(writeObject) {

		try {
			SkeletonSerializer skelSerializer;
			skelSerializer.exportSkeleton(skelData.skelPtr.get(), filename);
		}
		catch(Exception& /*e*/) {
			LogManager::getSingleton().logMessage("Error while writing skeleton file");
		}

	}

	return true;
}

bool OSMSerializer::addHelpers(Ogre::SceneNode* sNode) 
{
	if(!mXMLRoot) return true;

	Node::ChildNodeIterator it = sNode->getChildIterator();

	while(it.hasMoreElements()) 
	{
		Ogre::SceneNode* sceneNode = static_cast<Ogre::SceneNode*>(it.getNext());

		bool addNode = true;

		int numObjects = sceneNode->numAttachedObjects();

		for(int i=0; i<numObjects; ++i) 
		{
			MovableObject* object = sceneNode->getAttachedObject(i);

			const String& type = object->getMovableType();

			if(type == "Entity" || type == "Camera" || type == "Light") 
			{
				addNode = false;

				break;
			}
		}

		if(addNode) {
			const String& name = sceneNode->getName();

			if(!name.empty()) {
				if(!StringUtil::startsWith(name, "osm_") && !StringUtil::endsWith(name, ".target")) {
					addHelperNode(sceneNode);
				}
			}

			
		}

		if(sceneNode->getChildIterator().hasMoreElements()) 
		{
			addHelpers(sceneNode);
		}

	}

	return true;
}

bool OSMSerializer::addHelperNode(Ogre::SceneNode* helper) {
	oHelper helperData;

	helperData.node = helper;
	helperData.name = helper->getName();

	getBaseData(helperData);

	bool writeObject = true;

	if(mCallbacks)
		writeObject = mCallbacks->OnWriteHelper(helperData);

	if(writeObject) 
	{
		// Find the list of helpers to insert this node on
		TiXmlElement* list = mXMLRoot->FirstChildElement("helpers");
		if(list == NULL)
			list = mXMLRoot->InsertEndChild(TiXmlElement("helpers"))->ToElement();

		// Insert new helper in list
		TiXmlElement* helperElem = list->InsertEndChild(TiXmlElement("helper"))->ToElement();
		helperElem->SetAttribute("name", helperData.name.c_str());

		writeNodeTM(helperData, helperElem);

		// If we have a parent, we write the ID
		if(helperData.parentNode)
			helperElem->SetAttribute("parent", helperData.parentName.c_str());

		// Write object properties
		addObjectProperties(helperData, helperElem);

		// Write animation keys
		addAnimations(helperData, helperElem);
	}

	return true;
}

bool OSMSerializer::addAnimations(const oBaseObject& object, TiXmlElement* pElem)
{
	mNodeXmlList[object.node] = pElem;

	return true;

}

bool OSMSerializer::addObjectProperties(const oBaseObject& object, TiXmlElement* pElem)
{
	const oBaseObject::ObjectPropList& props = object.getProperties();

	if(!props.empty()) {

		TiXmlElement* propList = pElem->InsertEndChild(TiXmlElement("properties"))->ToElement();

		oBaseObject::ObjectPropList::const_iterator it = props.begin();
		oBaseObject::ObjectPropList::const_iterator iend = props.end();

		for(; it != iend; ++it) {
			const Ogre::String& name = (*it).first;
			const Ogre::String& value = (*it).second;

			TiXmlElement* propElem = propList->InsertEndChild(TiXmlElement("property"))->ToElement();

			propElem->SetAttribute(name.c_str(), value.c_str());
		}
	}

	return true;
}

bool OSMSerializer::addSceneProperties() 
{

	oSceneProperties sceneProps;

	// Scene manager type
	const Ogre::String& type = mSceneMgr->getTypeName();

	if(type == "DefaultSceneManager" || type == "OsctreeSceneManager")
		sceneProps.sceneType = STYPE_GENERIC;
	else if(type == "TerrainSceneManager")
		sceneProps.sceneType = STYPE_EXTERIOR_CLOSE;

	// Bkg color
	try 
	{
		sceneProps.bkgColor = mWindow->getViewport(0)->getBackgroundColour();
	}
	catch(Exception& /*e*/) 
	{

	}

	// Scene Ambient color
	sceneProps.ambientColor = mSceneMgr->getAmbientLight();
	
	// Scene shadow type
	ShadowTechnique shadowType = mSceneMgr->getShadowTechnique();

	switch(shadowType)
	{
		case SHADOWTYPE_STENCIL_MODULATIVE:
			sceneProps.shadows.type = 1;
			break;

		case SHADOWTYPE_STENCIL_ADDITIVE:
			sceneProps.shadows.type = 2;
			break;

		case SHADOWTYPE_TEXTURE_MODULATIVE:
			sceneProps.shadows.type = 3;
			break;

		case SHADOWTYPE_TEXTURE_ADDITIVE:
			sceneProps.shadows.type = 4;
			break;
	}

	sceneProps.shadows.textureCount = mSceneMgr->getShadowTextureCount();
	//sceneProps.shadows.textureSize = mSceneMgr->getShadowTextureSize();
	sceneProps.shadows.color = mSceneMgr->getShadowColour();

	// Scene sky
	Ogre::SceneNode* skyNode = mSceneMgr->getSkyBoxNode();

	if(skyNode) 
	{
		sceneProps.sky.type = SCENE_SKYBOX;
		const SceneManager::SkyBoxGenParameters& params = mSceneMgr->getSkyBoxGenParameters();

		sceneProps.sky.dist = params.skyBoxDistance;
	}
	else
	{
		skyNode = mSceneMgr->getSkyPlaneNode();

		if(skyNode)
		{
			sceneProps.sky.type = SCENE_SKYPLANE;
			const SceneManager::SkyPlaneGenParameters& params = mSceneMgr->getSkyPlaneGenParameters();

			sceneProps.sky.scale = params.skyPlaneScale;
			sceneProps.sky.bow = params.skyPlaneBow;
			sceneProps.sky.tiling = params.skyPlaneTiling;
			sceneProps.sky.xSegments = params.skyPlaneXSegments;
			sceneProps.sky.ySegments = params.skyPlaneYSegments;
		}
		else
		{
			skyNode = mSceneMgr->getSkyDomeNode();

			if(skyNode) {
				sceneProps.sky.type = SCENE_SKYDOME;
				const SceneManager::SkyDomeGenParameters& params = mSceneMgr->getSkyDomeGenParameters();

				sceneProps.sky.dist = params.skyDomeDistance;
				sceneProps.sky.bow = params.skyDomeCurvature;
				sceneProps.sky.tiling = params.skyDomeTiling;
				sceneProps.sky.xSegments = params.skyDomeXSegments;
				sceneProps.sky.xSegments = params.skyDomeYSegments;
			}
		}
	}

	// Scene fog
	sceneProps.fog.mode = mSceneMgr->getFogMode();
	sceneProps.fog.color = mSceneMgr->getFogColour();
	sceneProps.fog.density = mSceneMgr->getFogDensity();
	sceneProps.fog.linearStart = mSceneMgr->getFogStart();
	sceneProps.fog.linearEnd = mSceneMgr->getFogEnd();

	bool writeObject = true;

	if(mCallbacks)
		writeObject = mCallbacks->OnWriteSceneProps(sceneProps);

	if(writeObject) {
		// SceneManager properties
		TiXmlElement* sceneElem = mXMLRoot->InsertEndChild(TiXmlElement("sceneManager"))->ToElement();
		sceneElem->SetAttribute("type", StringConverter::toString(sceneProps.sceneType));
		if(!sceneProps.worldGeometry.empty()) {
			String filename;
			String path;
			StringUtil::splitFilename(sceneProps.worldGeometry, filename, path);
			sceneElem->SetAttribute("worldGeometry", filename.c_str());
		}

		TiXmlElement* pColor = mXMLRoot->InsertEndChild(TiXmlElement("bkgcolor"))->ToElement();
		pColor->SetAttribute("r", StringConverter::toString(sceneProps.bkgColor.r));
		pColor->SetAttribute("g", StringConverter::toString(sceneProps.bkgColor.g));
		pColor->SetAttribute("b", StringConverter::toString(sceneProps.bkgColor.b));

		pColor = mXMLRoot->InsertEndChild(TiXmlElement("lightColor"))->ToElement();
		pColor->SetAttribute("r", StringConverter::toString(sceneProps.ambientColor.r));
		pColor->SetAttribute("g", StringConverter::toString(sceneProps.ambientColor.g));
		pColor->SetAttribute("b", StringConverter::toString(sceneProps.ambientColor.b));

		TiXmlElement* shadowElem = mXMLRoot->InsertEndChild(TiXmlElement("shadowTechnique"))->ToElement();
		shadowElem->SetAttribute("type", StringConverter::toString(sceneProps.shadows.type));
		shadowElem->SetAttribute("tex_size", StringConverter::toString(sceneProps.shadows.textureSize));
		shadowElem->SetAttribute("tex_count", StringConverter::toString(sceneProps.shadows.textureCount));

		pColor = shadowElem->InsertEndChild(TiXmlElement("color"))->ToElement();
		pColor->SetAttribute("r", StringConverter::toString(sceneProps.shadows.color.r));
		pColor->SetAttribute("g", StringConverter::toString(sceneProps.shadows.color.g));
		pColor->SetAttribute("b", StringConverter::toString(sceneProps.shadows.color.b));

		if(sceneProps.sky.type > 0) {
			TiXmlElement* skyElem = mXMLRoot->InsertEndChild(TiXmlElement("skyTechnique"))->ToElement();
			skyElem->SetAttribute("type", StringConverter::toString(sceneProps.sky.type));

			// Set default sky material name if a sky type is selected without a material
			if(!sceneProps.sky.materialName.empty()) {
				skyElem->SetAttribute("material", sceneProps.sky.materialName.c_str());
			}
			else {
				if(sceneProps.sky.type == 1)
					skyElem->SetAttribute("material", "_SkyMaterial/SkyPlane");
				else if(sceneProps.sky.type == 2)
					skyElem->SetAttribute("material", "_SkyMaterial/SkyBox");
				else if(sceneProps.sky.type == 3)
					skyElem->SetAttribute("material", "_SkyMaterial/SkyDome");
			}

			skyElem->SetAttribute("drawFirst", sceneProps.sky.drawFirst ? "yes" : "no");
			skyElem->SetAttribute("tiling", StringConverter::toString(sceneProps.sky.tiling));
			skyElem->SetAttribute("scale", StringConverter::toString(sceneProps.sky.scale));
			skyElem->SetAttribute("dist", StringConverter::toString(sceneProps.sky.dist));
			skyElem->SetAttribute("bow", StringConverter::toString(sceneProps.sky.bow));
			skyElem->SetAttribute("xSegments", StringConverter::toString(sceneProps.sky.xSegments));
			skyElem->SetAttribute("ySegments", StringConverter::toString(sceneProps.sky.ySegments));
		}

		if(sceneProps.fog.mode > 0) {
			TiXmlElement* fogElem = mXMLRoot->InsertEndChild(TiXmlElement("fogMode"))->ToElement();

			fogElem->SetAttribute("type", StringConverter::toString(sceneProps.fog.mode));
			fogElem->SetAttribute("density", StringConverter::toString(sceneProps.fog.density));
			fogElem->SetAttribute("linearStart", StringConverter::toString(sceneProps.fog.linearStart));
			fogElem->SetAttribute("linearEnd", StringConverter::toString(sceneProps.fog.linearEnd));

			TiXmlElement* pColor = fogElem->InsertEndChild(TiXmlElement("color"))->ToElement();
			pColor->SetAttribute("r", StringConverter::toString(sceneProps.fog.color.r));
			pColor->SetAttribute("g", StringConverter::toString(sceneProps.fog.color.g));
			pColor->SetAttribute("b", StringConverter::toString(sceneProps.fog.color.b));
		}
	}

	return true;
}

bool OSMSerializer::writeNodeTM(const oBaseObject& node, TiXmlElement* pElem)
{
	const Ogre::Vector3& pos = node.pos;
	const Ogre::Vector3& scale = node.scale;
	const Ogre::Quaternion& rot = node.rot;

	// Position
	TiXmlElement* pData = pElem->InsertEndChild(TiXmlElement("position"))->ToElement();
	pData->SetAttribute("x", StringConverter::toString(pos[0], mPrecision));
	pData->SetAttribute("y", StringConverter::toString(pos[1], mPrecision));
	pData->SetAttribute("z", StringConverter::toString(pos[2], mPrecision));

	// Rotation
	pData = pElem->InsertEndChild(TiXmlElement("rotation"))->ToElement();
	pData->SetAttribute("x", StringConverter::toString(rot.x, mPrecision));
	pData->SetAttribute("y", StringConverter::toString(rot.y, mPrecision));
	pData->SetAttribute("z", StringConverter::toString(rot.z, mPrecision));
	pData->SetAttribute("w", StringConverter::toString(rot.w, mPrecision));
	
	// Scale
	pData = pElem->InsertEndChild(TiXmlElement("scale"))->ToElement();
	pData->SetAttribute("x", StringConverter::toString(scale[0], mPrecision));
	pData->SetAttribute("y", StringConverter::toString(scale[1], mPrecision));
	pData->SetAttribute("z", StringConverter::toString(scale[2], mPrecision));

	return true;
}

bool OSMSerializer::writeAnimKeys()
{
	Ogre::SceneManager::AnimationIterator it = mSceneMgr->getAnimationIterator();

	while(it.hasMoreElements()) {
		Animation* anim = it.getNext();

		writeNodeTrackKeys(anim);
	}

	return true;
}

bool OSMSerializer::writeNodeTrackKeys(Ogre::Animation* anim)
{
	Animation::NodeTrackIterator it = anim->getNodeTrackIterator();

	while(it.hasMoreElements()) {
		NodeAnimationTrack* track = it.getNext();

		Ogre::Node* node = track->getAssociatedNode();

		NodeXmlList::iterator nodeIt = mNodeXmlList.find(static_cast<SceneNode*>(node));

		if(nodeIt != mNodeXmlList.end()) {
			TiXmlElement* nodeElem = nodeIt->second;
			writeKeyframes(anim, track->getHandle(), nodeElem);
		}
	}

	return true;
}

bool OSMSerializer::writeKeyframes(Animation* anim, UINT index, TiXmlElement* pElem)
{

	if(anim) {
		// Create animation track list
		TiXmlElement* pAnimList = pElem->FirstChildElement("animations");
		if(pAnimList == NULL)
			pAnimList = pElem->InsertEndChild(TiXmlElement("animations"))->ToElement();

		NodeAnimationTrack* track = anim->getNodeTrack(index);

		// Create animation track
		TiXmlElement* pAnimTrack = pAnimList->InsertEndChild(TiXmlElement("animation"))->ToElement();
		pAnimTrack->SetAttribute("name", anim->getName().c_str());
		//pAnimTrack->SetAttribute("loop", ap.loop ? "true" : "false");
		pAnimTrack->SetAttribute("length", Ogre::StringConverter::toString(anim->getLength()));

		int keySize = static_cast<int>(track->getNumKeyFrames());

		for(int n=0; n<keySize; ++n) {
			TransformKeyFrame* key = track->getNodeKeyFrame(n);

			TiXmlElement* keyNode = pAnimTrack->InsertEndChild(TiXmlElement("keyframe"))->ToElement();
			keyNode->SetAttribute("time", Ogre::StringConverter::toString(key->getTime()));

			TiXmlElement* transNode = keyNode->InsertEndChild(TiXmlElement("position"))->ToElement();        
			transNode->SetAttribute("x", Ogre::StringConverter::toString(key->getTranslate().x));
			transNode->SetAttribute("y", Ogre::StringConverter::toString(key->getTranslate().y));
			transNode->SetAttribute("z", Ogre::StringConverter::toString(key->getTranslate().z));

			TiXmlElement* rotNode = keyNode->InsertEndChild(TiXmlElement("rotation"))->ToElement();        
			rotNode->SetAttribute("w", Ogre::StringConverter::toString(key->getRotation().w));
			rotNode->SetAttribute("x", Ogre::StringConverter::toString(key->getRotation().x));
			rotNode->SetAttribute("y", Ogre::StringConverter::toString(key->getRotation().y));
			rotNode->SetAttribute("z", Ogre::StringConverter::toString(key->getRotation().z));	
			
			TiXmlElement* scaleNode = keyNode->InsertEndChild(TiXmlElement("scale"))->ToElement();        
			scaleNode->SetAttribute("x", Ogre::StringConverter::toString(key->getScale().x));
			scaleNode->SetAttribute("y", Ogre::StringConverter::toString(key->getScale().y));
			scaleNode->SetAttribute("z", Ogre::StringConverter::toString(key->getScale().z));
		}
	}

	return true;
}

void OSMSerializer::getBaseData(oBaseObject& object)
{
	if(object.node && object.node != mSceneMgr->getRootSceneNode()) {
		Ogre::SceneNode* parentNode = object.node->getParentSceneNode();

		if(parentNode) {
			const String& name = parentNode->getName();

			bool isInRootNode = false;

			if(!name.empty() && name.length() > 5) {
				if(name[0] == 'O' && 
				   name[1] == 'S' && 
				   name[2] == 'M' && 
				   name[3] == '_') {
					// Scene was loaded in the Root node (using an auto created OSM node)
					isInRootNode = true;
				}
			}

			if(!isInRootNode) {
				object.parentNode = object.node->getParentSceneNode();
				object.parentName = object.parentNode->getName();
			}
		}
	}

	getNodeTM(object);
}

void OSMSerializer::getNodeTM(oBaseObject& object)
{
	if(object.node) {
		object.pos = object.node->getPosition();
		object.scale = object.node->getScale();
		object.rot = object.node->getOrientation();
	}
}