/**********************************************************************
 *<
	oFusion Pro Scene Properties (see License.txt)

	FILE: oSceneProperties.h

	DESCRIPTION: Scene Properties include header

	CREATED BY: Andres Carrera

	HISTORY:

 *>	Copyright (c) 2007, All Rights Reserved.
 **********************************************************************/

#ifndef _OSCENEPROPERTIES_H_
#define _OSCENEPROPERTIES_H_

#include "deque"

enum OShadowTechnique {
	ST_NONE					= 0x00,
	ST_STENCIL_MODULATIVE	= 0x12,
	ST_STENCIL_ADDITIVE		= 0x11,
	ST_TEXTURE_MODULATIVE	= 0x22,
	ST_TEXTURE_ADDITIVE		= 0x21,
};

enum OSceneType {
	STYPE_GENERIC,
	STYPE_EXTERIOR_CLOSE,
};

typedef struct TAG_SHADOWPROPERTIES {

	TAG_SHADOWPROPERTIES() {
		type = ST_NONE;
		textureSize = 512;
		textureCount = 1;
		color.r = 0;
		color.g = 0;
		color.b = 0;
	}

	int type;
	//OShadowTechnique type;
	Ogre::ColourValue color;
	int textureSize;
	int textureCount;

} ShadowProperties;


typedef struct TAG_SKYPROPERTIES {

	TAG_SKYPROPERTIES() {
		type = 0;
		dist = 5000;
		drawFirst = true;
		tiling = 10;
		bow = 0;
		xSegments = 16;
		ySegments = 16;
	}

	int type;
	Ogre::MaterialPtr mtl;
	float dist;
	float scale;
	Ogre::String materialName;
	float tiling;
	bool drawFirst;
	float bow;
	int xSegments;
	int ySegments;

} SkyProperties;

typedef struct TAG_FOGPROPERTIES {

	TAG_FOGPROPERTIES() {
		mode = Ogre::FOG_NONE;
		density = 0.001f;
		linearStart = 0;
		linearEnd = 1.0f;
	}

	Ogre::FogMode mode;
	
	Ogre::ColourValue color;

	float density;
	float linearStart;
	float linearEnd;
} FogProperties;

typedef struct TAG_SCENEPROPERTIES {

	OSceneType sceneType;
	Ogre::String worldGeometry;
	
	ShadowProperties shadows;
	
	SkyProperties sky;

	Ogre::ColourValue bkgColor;
	Ogre::ColourValue ambientColor;

	FogProperties fog;

} oSceneProperties;

typedef struct TAG_STATIGEOM_PROPERTIES {
	TAG_STATIGEOM_PROPERTIES() : dist(0), regionSize(1000, 1000, 1000), shadows(0) {}

	float dist;
	Ogre::Vector3 regionSize;
	int shadows;
} StaticGeomProperties;

#endif // _OSCENEPROPERTIES_H_