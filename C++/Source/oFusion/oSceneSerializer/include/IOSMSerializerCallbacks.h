/**********************************************************************
 *<
	oFusion Scene Serializer Pro (see License.txt)

	FILE: IOSMSceneSerializerCallbacks.h

	DESCRIPTION: OSM Scene serializer listener include header	

	IMPLEMENTED BY: Andres Carrera

 *>	Copyright (c) 2007, All Rights Reserved.
 **********************************************************************/

#ifndef _IOSMSERIALIZERCALLBACKS_H_
#define _IOSMSERIALIZERCALLBACKS_H_

#include "oSceneObjects.h"
#include "oSceneProperties.h"

/** A interface class defining a listener which can be used to receive event notifications
	during the file save process

	To use this class, you should create a subclass of this interface and override
	the methods for which you would like to customize the resulting process.

	Each method returns a bool value, if you want the passed object to be included in the 
	scene file, return 'true' as the value, or 'false' if you dont want the object to
	be included in the scene file.

	You can modify the passed object values to export a customized object in the scene file.
*/
class OSMSerializerCallbacks {
public:

	virtual ~OSMSerializerCallbacks(void) {}

	// Called before a node is saved in the scene file
	virtual bool OnWriteNode(oBaseObject& nodeObject) { return true; }
	// Called before a mesh is exported
	virtual bool OnWriteMesh(oMesh& mesh) { return true; }
	// Called before a camera is saved in the scene file
	virtual bool OnWriteCamera(oCamera& camera) { return true; }
	// Called before an entity is saved in the scene file
	virtual bool OnWriteEntity(oEntity& entity) { return true; }
	// Called before a light is saved in the scene file
	virtual bool OnWriteLight(oLight& light) { return true; }
	// Called before a skeleton is exported
	virtual bool OnWriteSkeleton(oSkeleton& skel) { return true; }
	// Called before a helper is exported
	virtual bool OnWriteHelper(oHelper& helper) { return true; }
	// Called before the scene properties are saved in the scene file
	virtual bool OnWriteSceneProps(oSceneProperties& sceneProps) { return true; }

};

#endif // _IOSMSERIALIZERCALLBACKS_H_
