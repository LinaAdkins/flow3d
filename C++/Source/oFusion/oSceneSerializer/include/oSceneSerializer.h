/**********************************************************************
 *<
	oFusion Scene Serializer Pro (see License.txt)

	FILE: oSceneSerializer.h

	DESCRIPTION: OSM Scene serializer (oe_scene) include header	

	IMPLEMENTED BY: Andres Carrera

 *>	Copyright (c) 2007, All Rights Reserved.
 **********************************************************************/

#ifndef _OSCENESERILAIZER_H_
#define _OSCENESERILAIZER_H_

#include "Ogre.h"
#include "OgreSharedPtr.h"
#include "..\TinyXML\tinyxml.h"

#include "IOSMSerializerCallbacks.h"

enum {
	OSM_WRITE_ENTITIES =		0x0001,
	OSM_WRITE_LIGHTS =			0x0002,
	OSM_WRITE_CAMERAS =			0x0004,
	OSM_WRITE_MESHES =			0x0008,
	OSM_WRITE_SKELETONS =		0x0010,
	OSM_WRITE_ANIMATIONS =		0x0020,
	OSM_WRITE_HELPERS =			0x0040,

	OSM_WRITE_HIDDEN =			0x00010000,

	OSM_WRITE_SCENE_PROPS =		0x00080000,

	OSM_WRITE_ALL_OBJECTS =		OSM_WRITE_ENTITIES | OSM_WRITE_LIGHTS | OSM_WRITE_CAMERAS | OSM_WRITE_HELPERS,
	OSM_WRITE_ALL_FILES =		OSM_WRITE_MESHES | OSM_WRITE_SKELETONS,
	OSM_WRITE_ALL_ANIMATIONS =	OSM_WRITE_ANIMATIONS,
	OSM_WRITE_ALL =				OSM_WRITE_SCENE_PROPS | OSM_WRITE_ALL_OBJECTS | OSM_WRITE_ALL_FILES | OSM_WRITE_ALL_ANIMATIONS,
};

class OSMSerializer {
public:
	
	// If you have multiple scene managers or render windows
	// set the specific object pointers to use
	// If no defined the current scene manager and render windows will be used
	OSMSerializer(Ogre::SceneManager* pSceneMgr = 0, Ogre::RenderWindow* win = 0);
	~OSMSerializer(void) {}

	// Save the current scene to a file
	// Params:
	//	filename The name of the scene file
	//	pCallbacks	A pointer to an object derived from the OSMSerializerCallbacks  interface 
	//				that will be called to notify the application about the scene save process
	//				The application can decide what objects get saved or modify each object
	//				before saving it to the scene file (optional)
	// precision The accuracy of the saved float values
	bool writeScene(const Ogre::String& filename, OSMSerializerCallbacks* pCallbacks = NULL, UINT precision = 6);

	// Set the file save flags (what objects to add in the scene, what animations to write)
	// This method lets you define what objects get saved in the scene
	// To save all objects:
	//		setSceneFlags(OSM_WRITE_ALL | OSM_WRITE_HIDDEN);
	//
	// Remarks: If you need finer control (per object) over the file save process
	//			use an event callback
	void setSceneFlags(int flags);

protected:
	
	bool addMesh(const Ogre::MeshPtr& mesh, const Ogre::String& filename);
	bool addEntity(Ogre::Entity* entity);
	bool addCamera(Ogre::Camera* camera);
	bool addLight(Ogre::Light* light);
	bool addSkeleton(const Ogre::SkeletonPtr& skel, const Ogre::String& filename);
	bool addHelpers(Ogre::SceneNode* sNode);
	bool addShape(Ogre::SimpleSpline* spline);
	bool addSceneProperties(void);
	bool addLocation(const TCHAR* subDirName, bool autoRegister = true);
	//bool addStaticGeometry(const TCHAR* groupName, const TCHAR* nodeName, const StaticGeomProperties& sgProp);
	//bool addMaterialAnimator(const MaterialAnimation& materialAnim);
	bool addAnimations(const oBaseObject& object, TiXmlElement* pElem);
	bool addObjectProperties(const oBaseObject& object, TiXmlElement* pElem);
	bool addHelperNode(Ogre::SceneNode* helper);

	bool writeNodeTM(const oBaseObject& node, TiXmlElement* pElem);

	bool writeAnimKeys(void);
	bool writeNodeTrackKeys(Ogre::Animation* anim);
	bool writeKeyframes(Ogre::Animation* anim, UINT index, TiXmlElement* pElem);

	void getBaseData(oBaseObject& object);
	void getNodeTM(oBaseObject& object);

	// Callback interface
	OSMSerializerCallbacks* mCallbacks;

	// Scene Manager
	Ogre::SceneManager* mSceneMgr;
	// Render Window
	Ogre::RenderWindow* mWindow;

	int mFlags;
	UINT mPrecision;

	typedef std::map<Ogre::SceneNode*, TiXmlElement*> NodeXmlList;
	NodeXmlList mNodeXmlList;

	Ogre::String mFilename;

	// Scene XML Document
	TiXmlDocument mXMLDoc;
	TiXmlElement* mXMLRoot;
};

#endif // _OSCENESERILAIZER_H_