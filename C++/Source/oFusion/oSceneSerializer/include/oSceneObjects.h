/**********************************************************************
 *<
	oFusion Scene Serializer Pro (see License.txt)

	FILE: oSceneObjects.h

	DESCRIPTION: OSM Scene objects include header	

	IMPLEMENTED BY: Andres Carrera

 *>	Copyright (c) 2007, All Rights Reserved.
 **********************************************************************/

#ifndef _OSCENEOBJECTS_H_
#define _OSCENEOBJECTS_H_

#include <deque>

class oBaseObject {
public:

	typedef std::map<Ogre::String, Ogre::String> ObjectPropList;

	oBaseObject() : node(0), parentNode(0), isHidden(0) {}

	Ogre::String name;			// object name
	Ogre::String parentName;	// parent name
	
	Ogre::SceneNode* node;			// node pointer
	Ogre::SceneNode* parentNode;	// parent node pointer

	Ogre::Vector3		pos, scale;
	Ogre::Quaternion	rot;

	bool	isHidden;

	bool addProperty(const Ogre::String& name, const Ogre::String& value) {
		return propList.insert(ObjectPropList::value_type(name, value)).second;
	}

	const ObjectPropList& getProperties(void) const { return propList; }

protected:
	ObjectPropList	propList;			// Object properties list
};

class oTargetObject {
public:

	oTargetObject() : targetNode(0) {}

	Ogre::String		targetName;		// name of target node (if defined)
	Ogre::SceneNode*	targetNode;		// target node (if defined)

	Ogre::Vector3		targetPos, targetScale;
	Ogre::Quaternion	targetRot;
};

class oCamera : public oBaseObject, public oTargetObject {
public:

	float	fov;				// camera fov
	bool	manualClip;		// manual clip setting
	Ogre::Camera* camInstance;  // Camera Instance - Added!

	float	nearClip;			// near clip value
	float	farClip;			// far clip value
};

class oLight : public oBaseObject, public oTargetObject {
public:

	Ogre::Light* lightInstance;
	Ogre::Light::LightTypes type;	// Type of light (OMNI_LIGHT, DIR_LIGHT, TSPOT_LIGHT, FSPOT_LIGHT)

	bool		state;			// Light state (true == on)

	bool		castShadows;	// Cast shadows state

	////////////////////
	// OMNI_LIGHT only
	float		hotSpot;
	float		fallOff;
	////////////////////

	Ogre::ColourValue color, specular;			// Light color values

	float		intensity;		// Light intensity

	// Light Attenuation
	bool  useAtten;
	float range;
	float constant;
	float linear;
	float quadratic;
};

class oSkeleton {
public:

	Ogre::String		filename;
	Ogre::SkeletonPtr	skelPtr;
	
};

class oMesh {
public:

	Ogre::String	filename;
	Ogre::MeshPtr	meshPtr;

	oSkeleton	skel;
};

struct oMeshLod {
	oMeshLod() : node (0) {}

	Ogre::String		meshFileName;
	Ogre::SceneNode*	node;

	float distance;
};

typedef std::deque<oMeshLod> MeshLodList;

class oEntity : public oBaseObject {
public:
	
	oMesh	mesh;	// mesh used by this entity
	Ogre::Entity* entityInstance;

	bool	castShadows;
	bool	receiveShadows;

	MeshLodList lodList;
};

class oHelper : public oBaseObject {
public:

	Ogre::String typeName;		// Class type string
};

class oShape : public oBaseObject {
public:
	
	std::deque<Ogre::Vector3> pointList;		// List of knot points
};

#endif // _OSCENOBJECTS_H_
