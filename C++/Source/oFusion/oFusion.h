#ifndef H_OFUSION
#define H_OFUSION

#include "oSceneSerializer.h"
//This is our class extension for handling callback events for scene loading-
class oSceneCallback : public OSMSceneCallbacks {
public:
	//Store our function pointers in the class-
	void(*nodeFunc)(Ogre::SceneNode*);
	void(*entityFunc)(Ogre::Entity*);
	void(*cameraFunc)(Ogre::Camera*);
	void(*lightFunc)(Ogre::Light*);
	void(*helperFunc)(Ogre::SceneNode*);
	void(*shapeFunc)(Ogre::SimpleSpline*);
	void(*staticGeometryFunc)(Ogre::StaticGeometry*);


	//Our callback methods-
	void OnNodeCreate(Ogre::SceneNode *pNode, TiXmlElement* pNodeDesc)
	{
		nodeFunc(pNode);
	}

	void OnEntityCreate(Ogre::Entity *pEntity, TiXmlElement* pEntityDesc)
	{
		oSceneCallback::setObjectProperties( pEntity , pEntityDesc );
		entityFunc(pEntity);

	}

	void OnCameraCreate(Ogre::Camera *pCamera, TiXmlElement* pCameraDesc)
	{
		oSceneCallback::setObjectProperties( pCamera , pCameraDesc );
		cameraFunc(pCamera);
	}

	void OnLightCreate(Ogre::Light *pLight, TiXmlElement* pLightDesc)
	{
		oSceneCallback::setObjectProperties( pLight , pLightDesc );
		lightFunc(pLight);
	}

	void OnHelperCreated(Ogre::SceneNode* pHelper, TiXmlElement* pHelperDesc)
	{
		helperFunc(pHelper);
	}


	void OnShapeLoaded(const Ogre::SimpleSpline& spline)
	{
		Ogre::SimpleSpline* temp = new Ogre::SimpleSpline;
		*temp = spline;
		shapeFunc(temp);

	}

	bool OnStaticGeometryCreated(Ogre::StaticGeometry* pStatic, const NodeList& nodeList)
	{
		staticGeometryFunc(pStatic);
		return true;
	}

private:
	// This function adds all user properties specified in the OSM file to any Ogre::MovableObject
	// via setUserObject. The format is Ogre::NameValuePairList. Returns true if there were user 
	// properties for the give movable object.
	static bool setObjectProperties( Ogre::MovableObject* mo , TiXmlElement* objectParameters  )
	{
		TiXmlElement* pElem = objectParameters->FirstChildElement("properties");

		if(pElem)
		{
			//test
			std::string temp;
			FlowUserObject* userObject = new FlowUserObject();

			for( TiXmlElement* pAttribElem = pElem->FirstChildElement(); pAttribElem !=0; pAttribElem = pAttribElem->NextSiblingElement() ) {	
				TiXmlAttribute* pAttrib = pAttribElem->FirstAttribute();
				userObject->getOSMPropertyList()->insert( std::pair<std::string , std::string>( pAttrib->Name() , pAttrib->Value() ) );
				//temp = "MO Name: " + mo->getName() + " - [ " + std::string(pAttrib->Name()) + " : " + std::string(pAttrib->Value()) + "]";
				//MessageBox( 0 , temp.c_str() , "Test:" , 0 );

			}

			mo->setUserObject( userObject );
			
			//Tests userObjectPointers
			//std::ostringstream os;
			//os << long( mo->getUserObject());
			//MessageBox( NULL , os.str().c_str() , "Pointer" , 0 );

			return true;
		}

		return false;
	}


};



//This is our class
//OSMSCENE->Create
EXPORTFUNC OSMScene* OSMSCENE_Create( Ogre::SceneManager* ogreSceneManager , Ogre::RenderWindow* ogreRenderWindow )
{
	OSMScene* scene = new OSMScene( ogreSceneManager , ogreRenderWindow );
	return scene;
}
//OSMSCENE->Destroy
EXPORTFUNC void OSMSCENE_Destroy( OSMScene* scene )
{
	delete scene;
}

//OSMSCENE->initialize
EXPORTFUNC void OSMSCENE_initialise( const char* filename , oSceneCallback* callback , OSMScene* scene )
{
	scene->initialise( filename , callback );
}

//OSMSCENE->createScene
EXPORTFUNC void OSMSCENE_createScene( OSMScene* scene , Ogre::SceneNode* ogreSceneNode = 0 )
{
	scene->createScene( ogreSceneNode );
}

//OSMSCENE->getSceneManager
EXPORTFUNC Ogre::SceneManager* OSMSCENE_getSceneManager( OSMScene* scene )
{
	return scene->getSceneManager();
}
//OSMSCENE->callback extension

EXPORTFUNC void OSMSCENE_registerFunctions( void (*blitzNodeFunc)(Ogre::SceneNode*) ,
					    void (*blitzEntityFunc)(Ogre::Entity*) ,
						void (*blitzCameraFunc)(Ogre::Camera*) ,
						void (*blitzLightFunc)(Ogre::Light*) ,
						void (*blitzHelperFunc)(Ogre::SceneNode*) ,
						void (*blitzShapeFunc)(Ogre::SimpleSpline* ),
						void (*blitzStaticGeometryFunc)(Ogre::StaticGeometry*),
						oSceneCallback* pointer)
{
	pointer->nodeFunc = blitzNodeFunc;
	pointer->entityFunc = blitzEntityFunc;
	pointer->cameraFunc = blitzCameraFunc;
	pointer->lightFunc = blitzLightFunc;
	pointer->helperFunc = blitzHelperFunc;
	pointer->shapeFunc = blitzShapeFunc;
	pointer->staticGeometryFunc = blitzStaticGeometryFunc;
}

// Here we create an instance of our callback object-
EXPORTFUNC oSceneCallback* OSMSCENE_createCallback()
{
	oSceneCallback* callback = new oSceneCallback;
	return callback;
}
// and here's the function to delete it-
EXPORTFUNC void OSMSCENE_deleteCallback( oSceneCallback* callback )
{
	delete callback;
}



//This is our class extension for handling callback events for scene loading-
class serializerCallbacks : public OSMSerializerCallbacks {
public:

	void initialize( bool (*blitzNodeFunc)(Ogre::SceneNode*),
					 bool (*blitzMeshFunc)(Ogre::Mesh*),
					 bool (*blitzCamFunc)(Ogre::Camera*),
					 bool (*blitzEntityFunc)(Ogre::Entity*),
					 bool (*blitzLightFunc)(Ogre::Light*),
					 bool (*blitzSkeletonFunc)(Ogre::Skeleton*),
					 bool (*blitzHelperFunc)(Ogre::SceneNode*)
				   )
	{
		this->writeNodeFunc = blitzNodeFunc;
		this->writeMeshFunc = blitzMeshFunc;
		this->writeCamFunc  = blitzCamFunc;
		this->writeEntityFunc = blitzEntityFunc;
		this->writeLightFunc = blitzLightFunc;
		this->writeSkeletonFunc = blitzSkeletonFunc;
		this->writeHelperFunc = blitzHelperFunc;
		this->props.clear();
	}

	// Called before a node is saved in the scene file
	bool OnWriteNode(oBaseObject& nodeObject) 
	{
		return writeNodeFunc( nodeObject.node );
	}

	// Called before a mesh is exported
	bool OnWriteMesh(oMesh& mesh) { 
		return writeMeshFunc( mesh.meshPtr.getPointer() ); 
	}
	// Called before a camera is saved in the scene file
	bool OnWriteCamera(oCamera& camera) { 
		bool isUsed = writeCamFunc( camera.camInstance );
		for(iter = props.begin(); iter != props.end(); ++iter )
		{
			camera.addProperty( iter->first , iter->second );
		}
		props.clear();
		return isUsed;
	}
	// Called before an entity is saved in the scene file
	bool OnWriteEntity(oEntity& entity) {
		bool isUsed = writeEntityFunc( entity.entityInstance );
		for(iter = props.begin(); iter != props.end(); ++iter )
		{
			entity.addProperty( iter->first , iter->second );
		}
		props.clear();
		return isUsed;
	}
	// Called before a light is saved in the scene file
	bool OnWriteLight(oLight& light) { 
		bool isUsed = writeLightFunc( light.lightInstance );
		for(iter = props.begin(); iter != props.end(); ++iter )
		{
			light.addProperty( iter->first , iter->second );
		}
		props.clear();
		return isUsed;
	}

	//Called before a helper is saved in the scene file-
	bool OnWriteHelper( oHelper& helper ){
		return writeHelperFunc( helper.node );
	}

	// Called before a skeleton is exported
	bool OnWriteSkeleton(oSkeleton& skel) {
		return writeSkeletonFunc( skel.skelPtr.getPointer() );	
	}
	// Called before the scene properties are saved in the scene file
	bool OnWriteSceneProps(oSceneProperties& sceneProps) { return true; }

	// Allows us to add properties to the current object onEvent.
	void addProperty( std::string name , std::string value )
	{
		props.insert( std::pair<std::string , std::string>( name , value ) );
	}


private:

	// Function pointers to blitz callback functions.
	bool(*writeNodeFunc)(Ogre::SceneNode*);
	bool(*writeMeshFunc)(Ogre::Mesh*);
	bool(*writeCamFunc)(Ogre::Camera*);
	bool(*writeEntityFunc)(Ogre::Entity*);
	bool(*writeLightFunc)(Ogre::Light*);
	bool(*writeSkeletonFunc)(Ogre::Skeleton*);
	bool(*writeHelperFunc)(Ogre::SceneNode*);

	// Map and Iterator for holding oBaseObject properties that are digested onEvent.
	std::map<std::string , std::string> props;
	std::map<std::string, std::string>::iterator iter;

};

// OSM Serializer Methods here-
EXPORTFUNC ::OSMSerializer* OSMS_create( Ogre::SceneManager* sm , Ogre::RenderWindow* win )
{
	return( new OSMSerializer( sm , win ));
}

EXPORTFUNC int OSMS_writeScene(  const char* filename , serializerCallbacks* pCallbacks , int precision , OSMSerializer* osms )
{
	return osms->writeScene( filename , pCallbacks , precision );
}

EXPORTFUNC void OSMS_delete( OSMSerializer* osms )
{
	delete osms;
}


// OSMScene Callback Functions-
EXPORTFUNC serializerCallbacks* OSMSCallbacks_create()
{
	return new serializerCallbacks();
}

EXPORTFUNC void OSMSCallbacks_delete( serializerCallbacks* s )
{
	delete s;
}

EXPORTFUNC void OSMSCallbacks_addProperty( const char* name , const char* value , serializerCallbacks* s )
{
	s->addProperty( name , value );
}

EXPORTFUNC void OSMSCallbacks_initialize( bool (*blitzNodeFunc)(Ogre::SceneNode*) , bool (*blitzMeshFunc)(Ogre::Mesh*) , bool (*blitzCamFunc)(Ogre::Camera*) , bool (*blitzEntityFunc)(Ogre::Entity*) , bool (*blitzLightFunc)(Ogre::Light*) , bool (*blitzSkeletonFunc)(Ogre::Skeleton*) , bool (*blitzHelperFunc)(Ogre::SceneNode*) , serializerCallbacks* s )
{
	s->initialize( blitzNodeFunc , blitzMeshFunc , blitzCamFunc , blitzEntityFunc , blitzLightFunc , blitzSkeletonFunc , blitzHelperFunc );
}





#endif
