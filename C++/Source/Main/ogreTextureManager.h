#ifndef H_OGRETEXTUREMANAGER
#define H_OGRETEXTUREMANAGER

EXPORTFUNC Ogre::TexturePtr* TEXTUREMANAGER_createManual( const char* name, 
											    const char* group,
												int texType,
												int width,
												int height,
												int num_mips,
												int format,
												int usage,
												Ogre::ManualResourceLoader* loader )
{
	return new Ogre::TexturePtr( Ogre::TextureManager::getSingleton().createManual( name, group, (Ogre::TextureType) texType, width, height, num_mips, (Ogre::PixelFormat)format, usage, loader ));
}

EXPORTFUNC Ogre::Texture* TEXTUREMANAGER_loadImage( const char* name , 
												    const char* group , 
													Ogre::Image* img ,
													int texType ,
													int iNumMipMaps ,
													float gamma ,
													bool isAlpha ,
													int desiredFormat ,
													bool hwGammaCorrection )
{
	Ogre::TexturePtr ptr = Ogre::TextureManager::getSingleton().loadImage( name , 
																		   group ,
																		   *img , 
																		   Ogre::TextureType( texType ) ,
																		   iNumMipMaps , 
																		   gamma ,
																		   isAlpha , 
																		   Ogre::PixelFormat( desiredFormat ) ,
																		   hwGammaCorrection );


	return ptr.getPointer();
}
#endif
