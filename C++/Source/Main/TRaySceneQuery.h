#ifndef H_TRAYSCENEQUERY
#define H_TRAYSCENEQUERY

EXPORTFUNC void RSQUERY_setSortByDistance( bool sort , int maxResults , Ogre::RaySceneQuery* ogreRaySceneQuery )
{
	ogreRaySceneQuery->setSortByDistance( sort , maxResults );
}

EXPORTFUNC Ogre::MovableObject* RSQUERY_execute( Ogre::RaySceneQuery* ogreRaySceneQuery , Ogre::Vector3* ogreVector )
{
	Ogre::RaySceneQueryResult result = ogreRaySceneQuery->execute();
	Ogre::RaySceneQueryResult::iterator it = result.begin();

	if( it!= result.end() )
	{
		*ogreVector = ogreRaySceneQuery->getRay().getOrigin()+( ogreRaySceneQuery->getRay().getDirection() * it->distance );
		return MovableObjectChildCast(it->movable);
	}

	return 0;
}
EXPORTFUNC void RSQUERY_setRay( Ogre::Ray* ogreRay , Ogre::RaySceneQuery* ogreRaySceneQuery )
{
	ogreRaySceneQuery->setRay( *ogreRay );
}

EXPORTFUNC void RSQUERY_clearResults( Ogre::RaySceneQuery* ogreRaySceneQuery )
{
	ogreRaySceneQuery->clearResults();
}

#endif
