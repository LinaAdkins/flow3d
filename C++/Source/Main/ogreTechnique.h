#ifndef H_OGRETECHNIQUE
#define H_OGRETECHNIQUE

EXPORTFUNC void TECHNIQUE_setDiffuse( Ogre::ColourValue* diffuse , Ogre::Technique* t )
{
	t->setDiffuse( *diffuse );
}

EXPORTFUNC void TECHNIQUE_setSceneBlending( int sbt , Ogre::Technique* t )
{
	t->setSceneBlending( Ogre::SceneBlendType(sbt) );
}

EXPORTFUNC void TECHNIQUE_setDepthCheckEnabled( bool enabled , Ogre::Technique* t )
{
	t->setDepthCheckEnabled( enabled );
}

EXPORTFUNC Ogre::Pass* TECHNIQUE_getPassWithIndex( int index , Ogre::Technique* t )
{
	return t->getPass( index );
}

// Shoggoth Method
EXPORTFUNC void TECHNIQUE_setShadowReceiverMaterial( const char* name , Ogre::Technique* t )
{
	t->setShadowReceiverMaterial( name );
}

#endif
