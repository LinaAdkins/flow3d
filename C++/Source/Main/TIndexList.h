#ifndef H_TINDEXLIST
#define H_TINDEXLIST


typedef std::vector<int> IndexList;


EXPORTFUNC IndexList* INDEXLIST_create()
{
	return new IndexList();
}

EXPORTFUNC void INDEXLIST_delete( IndexList* l )
{
	delete l;
}

EXPORTFUNC int INDEXLIST_size( IndexList* l )
{
	return l->size();
}

EXPORTFUNC void INDEXLIST_reserve( int size , IndexList* l )
{
	l->reserve( size );
}

EXPORTFUNC void INDEXLIST_push_back( int value ,  IndexList* l  )
{
	l->push_back( value );
}

EXPORTFUNC void INDEXLIST_addTriangle( int t1 , int t2 , int t3 , IndexList* l )
{
	l->push_back( t1 ); l->push_back( t2 ); l->push_back(t3);
}

EXPORTFUNC void INDEXLIST_clear( IndexList* l )
{
	l->clear();
}

EXPORTFUNC int INDEXLIST_getValue( int index , IndexList* l  )
{
	return (*l)[index];
}

#endif
