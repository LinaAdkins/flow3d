#ifndef H_TSKYDOMEGENPARAMETERS
#define H_TSKYDOMEGENPARAMETERS

EXPORTFUNC float SKYDOMEGENPARAMETERS_getSkyDomeCurvature( Ogre::SceneManager::SkyDomeGenParameters* p )
{
	return p->skyDomeCurvature;
}

EXPORTFUNC float SKYDOMEGENPARAMETERS_getSkyDomeTiling( Ogre::SceneManager::SkyDomeGenParameters* p )
{
	return p->skyDomeTiling;
}

EXPORTFUNC float SKYDOMEGENPARAMETERS_getSkyDomeDistance( Ogre::SceneManager::SkyDomeGenParameters* p )
{
	return p->skyDomeDistance;
}

EXPORTFUNC int SKYDOMEGENPARAMETERS_getSkyDomeXSegments( Ogre::SceneManager::SkyDomeGenParameters* p )
{
	return p->skyDomeXSegments;
}

EXPORTFUNC int SKYDOMEGENPARAMETERS_getSkyDomeYSegments( Ogre::SceneManager::SkyDomeGenParameters* p )
{
	return p->skyDomeYSegments;
}

EXPORTFUNC int SKYDOMEGENPARAMETERS_getSkyDomeYSegments_keep( Ogre::SceneManager::SkyDomeGenParameters* p )
{
	return p->skyDomeYSegments_keep;
}


EXPORTFUNC void SKYDOMEGENPARAMETERS_delete( Ogre::SceneManager::SkyDomeGenParameters* p )
{
	delete p;
}

#endif
