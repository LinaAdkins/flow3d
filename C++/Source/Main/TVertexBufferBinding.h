#ifndef H__TVERTEXBUFFERBINDING
#define H__TVERTEXBUFFERBINDING

EXPORTFUNC void VBB_setBinding( int index , Ogre::HardwareVertexBufferSharedPtr* buffer , Ogre::VertexBufferBinding* b )
{
	b->setBinding( index , *buffer );
}
#endif