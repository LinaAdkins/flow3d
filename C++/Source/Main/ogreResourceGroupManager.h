#ifndef H_OGRERESOURCEGROUPMANAGER
#define H_OGRERESOURCEGROUPMANAGER

EXPORTFUNC void RGM_initialiseResourceGroup( const char* name )
{
	Ogre::ResourceGroupManager::getSingleton().initialiseResourceGroup( name );
}

EXPORTFUNC void RGM_initialiseAllResourceGroups()
{
	Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

EXPORTFUNC void RGM_clearResourceGroup( const char* name )
{
	Ogre::ResourceGroupManager::getSingleton().clearResourceGroup( name );
}

EXPORTFUNC void RGM_destroyResourceGroup( const char* name )
{
	Ogre::ResourceGroupManager::getSingleton().destroyResourceGroup( name );
}

EXPORTFUNC void RGM_addResourceLocation( const char* name , const char* locType , const char* resGroup , bool recursive )
{
	Ogre::ResourceGroupManager::getSingleton().addResourceLocation( name, locType, resGroup , recursive );
}

EXPORTFUNC void RGM_removeResourceLocation( const char* name , const char* resGroup )
{
	Ogre::ResourceGroupManager::getSingleton().removeResourceLocation( name , resGroup );
}

EXPORTFUNC bool RGM_resourceExists( const char* group , const char* filename )
{
	return Ogre::ResourceGroupManager::getSingleton().resourceExists( group , filename );
}

#endif
