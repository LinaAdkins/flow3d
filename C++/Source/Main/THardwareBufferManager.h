#ifndef H__THARDWAREBUFFERMANAGER
#define H__THARDWAREBUFFERMANAGER

EXPORTFUNC Ogre::HardwareVertexBufferSharedPtr* HBM_createVertexBuffer( int vertexSize , int numVerts , int usage , bool useShadowBuffer )
{

	return new Ogre::HardwareVertexBufferSharedPtr( Ogre::HardwareBufferManager::getSingleton().createVertexBuffer( vertexSize , numVerts , Ogre::HardwareBuffer::Usage( usage ) , useShadowBuffer ));
}

EXPORTFUNC Ogre::HardwareIndexBufferSharedPtr* HBM_createIndexBuffer( int itype, int numIndexes, int usage, bool useShadowBuffer )
{
	return new Ogre::HardwareIndexBufferSharedPtr( Ogre::HardwareBufferManager::getSingleton().createIndexBuffer( Ogre::HardwareIndexBuffer::IndexType(itype) , numIndexes , Ogre::HardwareBuffer::Usage(usage) , useShadowBuffer ) );
}

#endif
