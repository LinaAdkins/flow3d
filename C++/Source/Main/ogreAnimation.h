#ifndef H_OGREANIMATION
#define H_OGREANIMATION

EXPORTFUNC const char* ANIMATION_getName( Ogre::Animation* a )
{
	tempString = a->getName();
	return tempString.c_str();
}

EXPORTFUNC float ANIMATION_getLength( Ogre::Animation* a )
{
	return a->getLength();
}

EXPORTFUNC Ogre::NodeAnimationTrack* ANIMATION_createNodeTrack( int handle , Ogre::Node* node , Ogre::Animation* a )
{
	return a->createNodeTrack( handle , node );
}

EXPORTFUNC Ogre::NodeAnimationTrack* ANIMATION_createNodeTrackWithBone( int handle , Ogre::Bone* node , Ogre::Animation* a )
{
	return a->createNodeTrack( handle , node );
}

EXPORTFUNC int ANIMATION_getNumNodeTracks( Ogre::Animation* a )
{
	return a->getNumNodeTracks();
}

EXPORTFUNC Ogre::NodeAnimationTrack* ANIMATION_getNodeTrack( int handle , Ogre::Animation* a )
{
	return a->getNodeTrack( handle );
}

EXPORTFUNC int ANIMATION_getNumNumericTracks( Ogre::Animation* a )
{
	return a->getNumNumericTracks();
}

EXPORTFUNC int ANIMATION_getNumVertexTracks( Ogre::Animation* a )
{
	return a->getNumVertexTracks();
}

EXPORTFUNC void ANIMATION_apply( float timePos , float weight , float scale , Ogre::Animation* a )
{
	a->apply( timePos , weight , scale );
}

EXPORTFUNC void ANIMATION_setInterpolationMode( int im , Ogre::Animation* a )
{
	a->setInterpolationMode( Ogre::Animation::InterpolationMode( im ) );
}

EXPORTFUNC void ANIMATION_setRotationInterpolationMode( int im , Ogre::Animation* a )
{
	a->setRotationInterpolationMode( Ogre::Animation::RotationInterpolationMode( im ) );
}

EXPORTFUNC Ogre::Animation::NodeTrackList* ANIMATION__getNodeTrackList( Ogre::Animation* a )
{
	return new Ogre::Animation::NodeTrackList( a->_getNodeTrackList() );
}

EXPORTFUNC void ANIMATION_setDefaultInterpolationMode( Ogre::Animation::InterpolationMode im)
{
	Ogre::Animation::setDefaultInterpolationMode( im );
}



#endif
