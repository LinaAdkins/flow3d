#ifndef H_OGREMESH
#define H_OGREMESH

// Meshes are managed by mesh manager, creation and destruction handled by Ogre.
//Mesh (ResourceManager *creator, const String &name, ResourceHandle handle, const String &group, bool isManual=false, ManualResourceLoader *loader=0)
EXPORTFUNC void MESH_delete( Ogre::Mesh* m )
{
	delete m;
}

EXPORTFUNC Ogre::SubMesh* MESH_createSubMesh( Ogre::Mesh* m )
{
	return m->createSubMesh();
}

EXPORTFUNC Ogre::SubMesh* MESH_createSubMeshWithName( const char* name , Ogre::Mesh* m )
{
	return m->createSubMesh( name );
}

EXPORTFUNC void MESH_nameSubMesh( const char* name , int index , Ogre::Mesh* m )
{
	m->nameSubMesh( name , index );
}

EXPORTFUNC int MESH__getSubMeshIndex( const char* name , Ogre::Mesh* m )
{
	return m->_getSubMeshIndex( name );
}

EXPORTFUNC int MESH_getNumSubMeshes( Ogre::Mesh* m )
{
	return m->getNumSubMeshes();
}

EXPORTFUNC Ogre::SubMesh* MESH_getSubMeshWithIndex( int index , Ogre::Mesh* m )
{
	return m->getSubMesh( index );
}

EXPORTFUNC Ogre::SubMesh* MESH_getSubMesh( const char* name , Ogre::Mesh* m )
{
	return m->getSubMesh( name );
}

EXPORTFUNC Ogre::Mesh::SubMeshIterator* MESH_getSubMeshIterator( Ogre::Mesh* m )
{
	return new Ogre::Mesh::SubMeshIterator( m->getSubMeshIterator() );
}

EXPORTFUNC Ogre::Mesh* MESH_clone( const char* newName , const char* newGroup , Ogre::Mesh* m )
{
	return m->clone( newName , newGroup ).getPointer();
}

EXPORTFUNC Ogre::AxisAlignedBox* MESH_getBounds( Ogre::Mesh* m )
{
	return new Ogre::AxisAlignedBox( m->getBounds() );
}

EXPORTFUNC float MESH_getBoundingSphereRadius( Ogre::Mesh* m )
{
	return m->getBoundingSphereRadius();
}

EXPORTFUNC void MESH__setBounds( Ogre::AxisAlignedBox* bounds , bool pad , Ogre::Mesh* m )
{
	m->_setBounds( *bounds , pad );
}

EXPORTFUNC void MESH__setBoundingSphereRadius( float radius , Ogre::Mesh* m )
{
	m->_setBoundingSphereRadius( radius );
}

EXPORTFUNC void MESH_setSkeletonName( const char* skelName , Ogre::Mesh* m )
{
	m->setSkeletonName( skelName );
}

EXPORTFUNC int MESH_hasSkeleton( Ogre::Mesh* m )
{
	return m->hasSkeleton();
}

EXPORTFUNC int MESH_hasVertexAnimation( Ogre::Mesh* m )
{
	return m->hasVertexAnimation();
}

EXPORTFUNC Ogre::Skeleton* MESH_getSkeleton( Ogre::Mesh* m )
{
	return m->getSkeleton().getPointer();
}

EXPORTFUNC const char* MESH_getSkeletonName( Ogre::Mesh* m )
{
	tempString = m->getSkeletonName();
	return tempString.c_str();
}

EXPORTFUNC void MESH__initAnimationState( Ogre::AnimationStateSet* animSet , Ogre::Mesh* m )
{
	m->_initAnimationState( animSet );
}

EXPORTFUNC void MESH__refreshAnimationState( Ogre::AnimationStateSet* animSet , Ogre::Mesh* m )
{
	m->_refreshAnimationState( animSet );
}

EXPORTFUNC void MESH_addBoneAssignment( Ogre::VertexBoneAssignment* vertBoneAssign , Ogre::Mesh* m )
{
	m->addBoneAssignment( *vertBoneAssign );
}

EXPORTFUNC void MESH_clearBoneAssignments( Ogre::Mesh* m )
{
	m->clearBoneAssignments();
}

EXPORTFUNC void MESH__notifySkeleton( Ogre::Skeleton* pSkel , Ogre::Mesh* m )
{
	m->_notifySkeleton( Ogre::SkeletonPtr( pSkel ) );
}

EXPORTFUNC Ogre::Mesh::BoneAssignmentIterator* MESH_getBoneAssignmentIterator( Ogre::Mesh* m )
{
	return new Ogre::Mesh::BoneAssignmentIterator( m->getBoneAssignmentIterator() );
}

EXPORTFUNC void MESH_generateLodLevels( Ogre::Mesh::LodDistanceList* lodDistances , int reductionMethod , float reductionValue , Ogre::Mesh* m )
{
	m->generateLodLevels( *lodDistances , Ogre::ProgressiveMesh::VertexReductionQuota(reductionMethod) , reductionValue );
}

EXPORTFUNC int MESH_getNumLodLevels( Ogre::Mesh* m )
{
	return m->getNumLodLevels();
}

EXPORTFUNC Ogre::MeshLodUsage* MESH_getLodLevel( int index , Ogre::Mesh* m )
{
	return new Ogre::MeshLodUsage( m->getLodLevel( index ) );
}

EXPORTFUNC void MESH_createManualLodLevel( float fromDepth , const char* meshName , Ogre::Mesh* m )
{
	m->createManualLodLevel( fromDepth , meshName );
}

EXPORTFUNC void MESH_updateManualLodLevel( int index , const char* meshName , Ogre::Mesh* m )
{
	m->updateManualLodLevel( index , meshName );
}

EXPORTFUNC int MESH_getLodIndex( float depth , Ogre::Mesh* m )
{
	return m->getLodIndex( depth );
}

EXPORTFUNC int MESH_getLodIndexSquaredDepth( float squaredDepth , Ogre::Mesh* m )
{
	return m->getLodIndexSquaredDepth( squaredDepth );
}

EXPORTFUNC int MESH_isLodManual( Ogre::Mesh* m )
{
	return m->isLodManual();
}

EXPORTFUNC void MESH__setLodInfo( int numLevels , bool isManual , Ogre::Mesh* m )
{
	m->_setLodInfo( numLevels , isManual );
}

EXPORTFUNC void MESH__setLodUsage( int level , Ogre::MeshLodUsage* usage , Ogre::Mesh* m )
{
	m->_setLodUsage( level , *usage );
}

EXPORTFUNC void MESH__setSubMeshLodFaceList( int subldx , int level  , Ogre::IndexData* facedata , Ogre::Mesh* m )
{
	m->_setSubMeshLodFaceList( subldx , level , facedata );
}

EXPORTFUNC void MESH_removeLodLevels( Ogre::Mesh* m )
{
	m->removeLodLevels();
}

EXPORTFUNC void MESH_setVertexBufferPolicy( int usage , bool shadowBuffer , Ogre::Mesh* m )
{
	m->setVertexBufferPolicy( Ogre::HardwareBuffer::Usage( usage ) , shadowBuffer );
}

EXPORTFUNC void MESH_setIndexBufferPolicy( Ogre::HardwareBuffer::Usage( usage ) , bool shadowBuffer , Ogre::Mesh* m )
{
	m->setIndexBufferPolicy( usage , shadowBuffer );
}

EXPORTFUNC int MESH_getVertexBufferUsage( Ogre::Mesh* m )
{
	return m->getVertexBufferUsage();
}

EXPORTFUNC int MESH_getIndexBufferUsage( Ogre::Mesh* m )
{
	return m->getIndexBufferUsage();
}

EXPORTFUNC int MESH_isVertexBufferShadowed( Ogre::Mesh* m )
{
	return m->isVertexBufferShadowed();
}

EXPORTFUNC int MESH_isIndexBufferShadowed( Ogre::Mesh* m )
{
	return m->isIndexBufferShadowed();
}

EXPORTFUNC int MESH__rationaliseBoneAssignments( int vertexCount , Ogre::Mesh::VertexBoneAssignmentList* assignments , Ogre::Mesh* m )
{
	return m->_rationaliseBoneAssignments( vertexCount , *assignments );
}

EXPORTFUNC void MESH__compileBoneAssignments( Ogre::Mesh* m )
{
	m->_compileBoneAssignments();
}

EXPORTFUNC void MESH__updateCompiledBoneAssignments( Ogre::Mesh* m )
{
	m->_updateCompiledBoneAssignments();
}

EXPORTFUNC void MESH_buildTangentVectors( int targetSemantic , int sourceTexCoordSet , int index , bool splitMirrored , bool splitRotated , bool storeParityInW , Ogre::Mesh* m )
{
	m->buildTangentVectors( Ogre::VertexElementSemantic( targetSemantic ) , sourceTexCoordSet , index , splitMirrored , splitRotated , storeParityInW );
}

EXPORTFUNC int MESH_suggestTangentVectorBuildParams( int targetSemantic , int* outSourceCoordSet , int* outIndex , Ogre::Mesh* m )
{
	return m->suggestTangentVectorBuildParams( Ogre::VertexElementSemantic( targetSemantic ) , (unsigned short &)*outSourceCoordSet , (unsigned short &)*outIndex );
}

EXPORTFUNC void MESH_buildEdgeList( Ogre::Mesh* m )
{
	m->buildEdgeList();
}

EXPORTFUNC void MESH_freeEdgeList( Ogre::Mesh* m )
{
	m->freeEdgeList();
}

EXPORTFUNC void MESH_prepareForShadowVolume( Ogre::Mesh* m )
{
	m->prepareForShadowVolume();
}

EXPORTFUNC Ogre::EdgeData* MESH_getEdgeList( int lodIndex , Ogre::Mesh* m )
{
	return m->getEdgeList( lodIndex );
}

//const EdgeData * 	getEdgeList (unsigned int lodIndex=0) const - Not required

EXPORTFUNC int MESH_isPreparedForShadowVolumes( Ogre::Mesh* m )
{
	return m->isPreparedForShadowVolumes();
}

EXPORTFUNC int MESH_isEdgeListBuilt( Ogre::Mesh* m )
{
	return m->isEdgeListBuilt();
}

EXPORTFUNC Ogre::Mesh::SubMeshNameMap* MESH_getSubMeshNameMap( Ogre::Mesh* m )
{
	return new Ogre::Mesh::SubMeshNameMap( m->getSubMeshNameMap() );
}

EXPORTFUNC void MESH_setAutoBuildEdgeLists( bool autobuild, Ogre::Mesh* m )
{
	m->setAutoBuildEdgeLists( autobuild );
}

EXPORTFUNC int MESH_getAutoBuildEdgeLists( Ogre::Mesh* m )
{
	return m->getAutoBuildEdgeLists();
}

EXPORTFUNC int MESH_getSharedVertexDataAnimationType( Ogre::Mesh* m )
{
	return m->getSharedVertexDataAnimationType();
}

EXPORTFUNC Ogre::Animation* MESH_createAnimation( const char* name , float length , Ogre::Mesh* m )
{
	return m->createAnimation( name, length );
}

EXPORTFUNC Ogre::Animation* MESH_getAnimation( const char* name, Ogre::Mesh* m )
{
	return m->getAnimation( name );
}

EXPORTFUNC Ogre::Animation* MESH__getAnimationImpl( const char* name, Ogre::Mesh* m )
{
	return m->_getAnimationImpl( name );
}

EXPORTFUNC int MESH_hasAnimation( const char* name , Ogre::Mesh* m )
{
	return m->hasAnimation( name );
}

EXPORTFUNC void MESH_removeAnimation( const char* name , Ogre::Mesh* m )
{
	m->removeAnimation( name );
}

EXPORTFUNC int MESH_getNumAnimations( Ogre::Mesh* m )
{
	return m->getNumAnimations();
}

EXPORTFUNC Ogre::Animation* MESH_getAnimationWithIndex( int index , Ogre::Mesh* m )
{
	return m->getAnimation( index );
}

EXPORTFUNC void MESH_removeAllAnimations( Ogre::Mesh* m )
{
	m->removeAllAnimations();
}

EXPORTFUNC Ogre::VertexData* MESH_getVertexDataByTrackHandle( int handle , Ogre::Mesh* m )
{
	return m->getVertexDataByTrackHandle( handle );
}

EXPORTFUNC void MESH_updateMaterialForAllSubMeshes( Ogre::Mesh* m )
{
	m->updateMaterialForAllSubMeshes();
}

EXPORTFUNC void MESH__determineAnimationTypes( Ogre::Mesh* m )
{
	m->_determineAnimationTypes(); 
}

EXPORTFUNC int MESH__getAnimationTypesDirty( Ogre::Mesh* m )
{
	return (int)m->_getAnimationTypesDirty();
}

EXPORTFUNC Ogre::Pose* MESH_createPose( int target , const char* name , Ogre::Mesh* m )
{
	return m->createPose( target, name );
}

EXPORTFUNC int MESH_getPoseCount( Ogre::Mesh* m )
{
	return (int)m->getPoseCount();
}

EXPORTFUNC Ogre::Pose* MESH_getPoseWithIndex( int index , Ogre::Mesh* m )
{
	return m->getPose( index );
}

EXPORTFUNC Ogre::Pose* MESH_getPose( const char* name , Ogre::Mesh* m )
{
	return m->getPose(name);
}

EXPORTFUNC void MESH_removePoseWithIndex( int index, Ogre::Mesh* m )
{
	m->removePose( index );
}

EXPORTFUNC void MESH_removePose( const char* name , Ogre::Mesh* m )
{
	m->removePose(name);
}

EXPORTFUNC void MESH_removeAllPoses( Ogre::Mesh* m )
{
	m->removeAllPoses();
}

EXPORTFUNC Ogre::Mesh::PoseIterator* MESH_getPoseIterator( Ogre::Mesh* m )
{
	return new Ogre::Mesh::PoseIterator( m->getPoseIterator() );
}

//ConstPoseIterator 	getPoseIterator (void) const - Unecessary

EXPORTFUNC Ogre::PoseList* MESH_getPoseList( Ogre::Mesh* m )
{
	return new Ogre::PoseList( m->getPoseList() );
}

EXPORTFUNC void MESH_setSharedVertexData( Ogre::VertexData* v , Ogre::Mesh* m )
{
	m->sharedVertexData = v;
}

EXPORTFUNC Ogre::VertexData* MESH_getSharedVertexData( Ogre::Mesh* m )
{
	if(!m->sharedVertexData)
	{
		return NULL;
	}
	else
	{
		return m->sharedVertexData;
	}

}

EXPORTFUNC Ogre::Mesh::IndexMap* MESH_getSharedBlendIndexToBoneIndexMap( Ogre::Mesh* m )
{
	return &m->sharedBlendIndexToBoneIndexMap;
}

EXPORTFUNC void MESH_setSharedBlendIndexToBoneIndexMap( Ogre::Mesh::IndexMap* i , Ogre::Mesh* m )
{
	m->sharedBlendIndexToBoneIndexMap = *i;
}


	


void MESH_getVertexInformation2(
                        size_t &vertex_count,
                        Ogre::Vector3* &vertices,
                        size_t &index_count,
                        unsigned long* &indices,
                        const Ogre::Vector3 &position,
                        const Ogre::Quaternion &orient,
                        const Ogre::Vector3 &scale,
						const Ogre::Mesh* const mesh)
{
    bool added_shared = false;
    size_t current_offset = 0;
    size_t shared_offset = 0;
    size_t next_offset = 0;
    size_t index_offset = 0;


    vertex_count = index_count = 0;

    // Calculate how many vertices and indices we're going to need
    for ( unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
    {
        Ogre::SubMesh* submesh = mesh->getSubMesh(i);
        // We only need to add the shared vertices once
        if(submesh->useSharedVertices)
        {
            if( !added_shared )
            {
                vertex_count += mesh->sharedVertexData->vertexCount;
                added_shared = true;
            }
        }
        else
        {
            vertex_count += submesh->vertexData->vertexCount;
        }
        // Add the indices
        index_count += submesh->indexData->indexCount;
    }


    // Allocate space for the vertices and indices
    vertices = new Ogre::Vector3[vertex_count];
    indices = new unsigned long[index_count];

    added_shared = false;

    // Run through the submeshes again, adding the data into the arrays
    for (unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
    {
        Ogre::SubMesh* submesh = mesh->getSubMesh(i);

        Ogre::VertexData* vertex_data = submesh->useSharedVertices ? mesh->sharedVertexData : submesh->vertexData;

        if ((!submesh->useSharedVertices) || (submesh->useSharedVertices && !added_shared))
        {
            if(submesh->useSharedVertices)
            {
                added_shared = true;
                shared_offset = current_offset;
            }

            const Ogre::VertexElement* posElem =
                vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);

            Ogre::HardwareVertexBufferSharedPtr vbuf =
                vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());

            unsigned char* vertex =
                static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));

            // There is _no_ baseVertexPointerToElement() which takes an Ogre::Real or a double
            //  as second argument. So make it float, to avoid trouble when Ogre::Real will
            //  be comiled/typedefed as double:
            //Ogre::Real* pReal;
            float* pReal;

            for( size_t j = 0; j < vertex_data->vertexCount; ++j, vertex += vbuf->getVertexSize())
            {
                posElem->baseVertexPointerToElement(vertex, &pReal);
                Ogre::Vector3 pt(pReal[0], pReal[1], pReal[2]);
                vertices[current_offset + j] = (orient * (pt * scale)) + position;
            }
            
            vbuf->unlock();
            next_offset += vertex_data->vertexCount;
        }


        Ogre::IndexData* index_data = submesh->indexData;
        size_t numTris = index_data->indexCount / 3;
        Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer;
        
        bool use32bitindexes = (ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT);

        unsigned long* pLong = static_cast<unsigned long*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
        unsigned short* pShort = reinterpret_cast<unsigned short*>(pLong);

        size_t offset = (submesh->useSharedVertices)? shared_offset : current_offset;

        if ( use32bitindexes )
        {
            for ( size_t k = 0; k < numTris*3; ++k)
            {
                indices[index_offset++] = pLong[k] + static_cast<unsigned long>(offset);
            }
        }
        else
        {
            for ( size_t k = 0; k < numTris*3; ++k)
            {
                indices[index_offset++] = static_cast<unsigned long>(pShort[k]) +
                                          static_cast<unsigned long>(offset);
            }
        }

        ibuf->unlock();
        current_offset = next_offset;
    }
}

EXPORTFUNC void MESH_getVertexInformation(size_t* vcount , 
							   VertexList* vertexList , 
							   size_t* icount,
							   IndexList* indexList,
							   const Ogre::Mesh* const mesh)
{
	//size_t vcount;
	Ogre::Vector3* verts;
	//size_t icount;
	unsigned long *indices;
	Ogre::Vector3 pos = Ogre::Vector3(0,0,0);
	Ogre::Quaternion quat = Ogre::Quaternion();
	Ogre::Vector3 scale = Ogre::Vector3(1);

	MESH_getVertexInformation2( *vcount ,
								verts ,
								*icount ,
								indices ,
								pos ,
								quat ,
								scale ,
								mesh );

	// Load vertexes into vertexList
	 for (int i = 0; i < static_cast<int>(*vcount); i++)
     {
		 vertexList->push_back( verts[i] );
	 }

	 // Load indexes into indexList
	 for (int i = 0; i < static_cast<int>(*icount); i++)
     {
		 indexList->push_back( indices[i] );
	 }


/*
	std::ostringstream os;
	os << 
		"Mesh Info:\n" <<
		"Vcount: " << *vcount << "\n" <<
		"Verts address: " << (long)verts << "\n" <<
		"First 3 Verts: " << verts[0] << "," << verts[1] << "," << verts[2] << "\n" <<
		"Icount: " << icount << "\n" <<
		"Indice pointer: " << (long)indices << "\n" <<
		"Pos: " << pos << "\n" <<
		"Orient: " << quat << "\n" <<
		"Scale: " << scale << "\n";

	::MessageBox( NULL , os.str().c_str() , "C++ -> After function ->" , 0 );
	*/
}

// Add function to get the triangle count of a given mesh
EXPORTFUNC int MESH_getTriangleCount(Ogre::Mesh* m)
{
	int iTriangleCount = 0;

	int iSubMeshes = m->getNumSubMeshes();
	for (int iCurrentSubMesh = 0; iCurrentSubMesh < iSubMeshes; iCurrentSubMesh++)
	{
		Ogre::SubMesh* lpSubMesh = m->getSubMesh(iCurrentSubMesh);
		if (lpSubMesh)
		{
			Ogre::RenderOperation rend;
			lpSubMesh->_getRenderOperation(rend);

			int iIndexCount = lpSubMesh->indexData->indexCount;
			if (iIndexCount)
			{
				switch (rend.operationType)
				{
				case Ogre::RenderOperation::OT_TRIANGLE_LIST:
					{
						iTriangleCount += iIndexCount / 3;
					} break;

				case Ogre::RenderOperation::OT_TRIANGLE_STRIP:
					{
						iTriangleCount += iIndexCount - 2;
					} break;

				case Ogre::RenderOperation::OT_TRIANGLE_FAN:
					{
						iTriangleCount += iIndexCount - 2;
					} break;
				}

			}
		}
	}

	return iTriangleCount;
}

// Static Public Member Functions
//static void 	prepareMatricesForVertexBlend (const Matrix4 **blendMatrices, const Matrix4 *boneMatrices, const IndexMap &indexMap) - Requies array dereferencing that blitzmax cannot do
//static void 	softwareVertexBlend (const VertexData *sourceVertexData, const VertexData *targetVertexData, const Matrix4 *const *blendMatrices, size_t numMatrices, bool blendNormals) - Requires array dereferencing that blitzmax cannot do
EXPORTFUNC void MESH_softwareVertexMorph( float t , Ogre::HardwareVertexBuffer* b1 , Ogre::HardwareVertexBuffer* b2 , Ogre::VertexData* targetVertexData )
{
	Ogre::Mesh::softwareVertexMorph( t , Ogre::HardwareVertexBufferSharedPtr( b1 ) ,  Ogre::HardwareVertexBufferSharedPtr( b2 ) , targetVertexData );
}

EXPORTFUNC void MESH_softwareVertexPoseBlend( float weight , std::map< size_t , Ogre::Vector3> *vertexOffsetMap , Ogre::VertexData* targetVertexData )
{
	Ogre::Mesh::softwareVertexPoseBlend( weight , *vertexOffsetMap , targetVertexData );
}

#endif
