#ifndef H_TPARTICLE
#define H_TPARTICLE

//Particle ()	// Unused, particles are managed by TParticleSystem.createParticle.

EXPORTFUNC void PARTICLE_setDimensions( float width , float height , Ogre::Particle* p )
{
	p->setDimensions( width , height );
}

EXPORTFUNC bool PARTICLE_hasOwnDimensions( Ogre::Particle* p )
{
	return p->hasOwnDimensions();
}

EXPORTFUNC float PARTICLE_getOwnWidth( Ogre::Particle* p )
{
	return p->getOwnWidth();
}

EXPORTFUNC float PARTICLE_getOwnHeight( Ogre::Particle* p )
{
	return p->getOwnHeight();
}

EXPORTFUNC void PARTICLE_setRotation( Ogre::Radian* rad , Ogre::Particle* p )
{
	p->setRotation( *rad );
}

EXPORTFUNC Ogre::Radian* PARTICLE_getRotation( Ogre::Particle* p )
{
	return new Ogre::Radian( p->getRotation() );
}

EXPORTFUNC void PARTICLE__notifyOwner( Ogre::ParticleSystem* owner , Ogre::Particle* p )
{
	p->_notifyOwner( owner );
}

EXPORTFUNC void PARTICLE__notifyVisualData( Ogre::ParticleVisualData* vis , Ogre::Particle* p )
{
	p->_notifyVisualData( vis );
}

EXPORTFUNC Ogre::ParticleVisualData* PARTICLE_getVisualData( Ogre::Particle* p )
{
	return p->getVisualData();
}

EXPORTFUNC void PARTICLE_resetDimensions( Ogre::Particle* p )
{
	p->resetDimensions();
}

// Accessors

EXPORTFUNC void PARTICLE_setHasOwnDimensions( bool hasOwnDimensions , Ogre::Particle* p )
{
	p->mOwnDimensions = hasOwnDimensions;
}

EXPORTFUNC void PARTICLE_setPosition( Ogre::Vector3* position , Ogre::Particle* p )
{
	p->position = *position;
}

EXPORTFUNC Ogre::Vector3* PARTICLE_getPosition( Ogre::Particle* p )
{
	return new Ogre::Vector3(p->position);
}

EXPORTFUNC void PARTICLE_setDirection( Ogre::Vector3* direction , Ogre::Particle* p )
{
	p->direction = *direction;
}

EXPORTFUNC Ogre::Vector3* PARTICLE_getDirection( Ogre::Particle* p )
{
	return new Ogre::Vector3(p->direction);
}

EXPORTFUNC void PARTICLE_setColour( Ogre::ColourValue* colour , Ogre::Particle* p )
{
	p->colour = *colour;
}

EXPORTFUNC Ogre::ColourValue* PARTICLE_getColour( Ogre::Particle* p )
{
	return new Ogre::ColourValue( p->colour );
}

EXPORTFUNC void PARTICLE_setTimeToLive( float timeToLive , Ogre::Particle* p )
{
	p->timeToLive = timeToLive;
}

EXPORTFUNC float PARTICLE_getTimeToLive( Ogre::Particle* p )
{
	return p->timeToLive;
}

EXPORTFUNC void PARTICLE_setTotalTimeToLive( float totalTimeToLive , Ogre::Particle* p )
{
	p->totalTimeToLive = totalTimeToLive;
}

EXPORTFUNC float PARTICLE_getTotalTimeToLive( Ogre::Particle* p )
{
	return p->totalTimeToLive;
}

EXPORTFUNC void PARTICLE_setRotationSpeed( Ogre::Radian* rotationSpeed , Ogre::Particle* p )
{
	p->rotationSpeed = *rotationSpeed;
}

EXPORTFUNC Ogre::Radian* PARTICLE_getRotationSpeed( Ogre::Particle* p )
{
	return new Ogre::Radian(p->rotationSpeed);
}

EXPORTFUNC void PARTICLE_setParticleType( int particleType , Ogre::Particle* p )
{
	p->particleType = (Ogre::Particle::ParticleType)particleType;
}

EXPORTFUNC int PARTICLE_getParticleType( Ogre::Particle* p )
{
	return p->particleType;
}








#endif
