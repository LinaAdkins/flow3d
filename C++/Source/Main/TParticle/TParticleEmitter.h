#ifndef H_TPARTICLEEMITTER
#define H_TPARTICLEEMITTER

EXPORTFUNC void PARTICLEEMITTER__initParticle( Ogre::Particle* pParticle , Ogre::ParticleEmitter* ogreParticleEmmitter )
{
	ogreParticleEmmitter->_initParticle( pParticle );
}

EXPORTFUNC void PARTICLEEMITTER_setEnabled( bool enabled , Ogre::ParticleEmitter* p )
{
	p->setEnabled( enabled );
}

EXPORTFUNC bool PARTICLEEMITTER_getEnabled( Ogre::ParticleEmitter* p )
{
	return p->getEnabled();
}

EXPORTFUNC void PARTICLEEMITTER_setColour( Ogre::ColourValue* colour , Ogre::ParticleEmitter* p )
{
	p->setColour( *colour );
}



#endif
