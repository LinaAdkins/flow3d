#ifndef H_OGREVERTEXDATA
#define H_OGREVERTEXDATA

EXPORTFUNC Ogre::VertexData* VERTEXDATA_create()
{
	return new Ogre::VertexData();
}

EXPORTFUNC void VERTEXDATA_delete( Ogre::VertexData* v )
{
	delete v;
}

//Public Attributes

EXPORTFUNC Ogre::VertexDeclaration* VERTEXDATA_getVertexDeclaration( Ogre::VertexData* v )
{
	return v->vertexDeclaration;
}

EXPORTFUNC void VERTEXDATA_setVertexDeclaration( Ogre::VertexDeclaration* vertexDeclaration , Ogre::VertexData* v )
{
	v->vertexDeclaration = vertexDeclaration;
}

EXPORTFUNC Ogre::VertexBufferBinding* VERTEXDATA_getVertexBufferBinding( Ogre::VertexData* v )
{
	return v->vertexBufferBinding;
}

EXPORTFUNC void VERTEXDATA_setVertexBufferBinding( Ogre::VertexBufferBinding* vertexBufferBinding , Ogre::VertexData* v )
{
	v->vertexBufferBinding = vertexBufferBinding;
}

EXPORTFUNC int VERTEXDATA_getVertexStart( Ogre::VertexData* v )
{
	return v->vertexStart;
}

EXPORTFUNC void VERTEXDATA_setVertexStart(  int vertexStart , Ogre::VertexData* v )
{
	v->vertexStart = vertexStart;
}

EXPORTFUNC int VERTEXDATA_getVertexCount( Ogre::VertexData* v )
{
	return v->vertexCount;
}

EXPORTFUNC void VERTEXDATA_setVertexCount( int vertexCount , Ogre::VertexData* v )
{
	v->vertexCount = vertexCount;
}

EXPORTFUNC Ogre::VertexData::HardwareAnimationDataList* VERTEXDATA_getHwAnimationDataList( Ogre::VertexData* v )
{
	return &v->hwAnimationDataList;
}

EXPORTFUNC void VERTEXDATA_setHwAnimationDataList( Ogre::VertexData::HardwareAnimationDataList* hwAnimationDataList , Ogre::VertexData* v )
{
	v->hwAnimationDataList = *hwAnimationDataList;
}

EXPORTFUNC int VERTEXDATA_getHwAnimDataItemsUsed( Ogre::VertexData* v )
{
	return v->hwAnimDataItemsUsed;
}

EXPORTFUNC void VERTEXDATA_setHwAnimDataItemsUsed( int hwAnimDataItemsUsed , Ogre::VertexData* v )
{
	v->hwAnimDataItemsUsed = hwAnimDataItemsUsed;
}

EXPORTFUNC Ogre::HardwareVertexBuffer* VERTEXDATA_getHardwareShadowVolWBuffer( Ogre::VertexData* v )
{
	return v->hardwareShadowVolWBuffer.getPointer();
}

EXPORTFUNC void VERTEXDATA_setHardwareShadowVolWBuffer( Ogre::HardwareVertexBuffer* hardwareShadowVolWBuffer , Ogre::VertexData* v )
{
	v->hardwareShadowVolWBuffer.bind(hardwareShadowVolWBuffer);
}








#endif
