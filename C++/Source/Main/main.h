#ifndef H_MAIN
#define H_MAIN

#if defined(WIN32)
#define WIN32_LEAN_AND_MEAN
#endif

//#define OGRE_THREAD_SUPPORT 1

#include <windows.h>
#include <iostream>

// Our Ogre3D inclues-
#include "Ogre.h"
#include "OgreTerrainSceneManager.h"




int g_tempFlag;
float g_tempFloat;
Ogre::String tempString;
Ogre::Ray tempRay;

// Global vars and Stamps for each build (Demo , Internal , FlowEd )
#ifdef DEMO
Ogre::Timer* protectionTimer;
unsigned long timeLimit;
static std::string stamp = "05d3d167a965514fa8574132da0798cdd6d9a4dc";
#elif defined FLOWED
bool flowedCheck;
static std::string stamp = "b551b678470e8254d8f5d4269a0243a592c4b7bd";
#else
static char* stamp = "c2682629b71e3a6996f5f3aa158417d1527cf112";
#endif

#define EXPORTFUNC extern "C" __declspec(dllexport)



//CEGUI-
#include <CEGUI.h>
#include <OgreCEGUIRenderer.h>
#include <OgreCEGUIResourceProvider.h>
#include <OgreCEGUITexture.h>
#include "_CEGUI.h"

//Ogre-
#include "_flow.h"
#include "ogreRoot.h"
#include "_ogreRenderable.h"
#include "_ogreAnimableObject.h"
#include "ogreRenderWindow.h"
#include "ogreSceneNode.h"
#include "ogreViewport.h"
#include "ogreFlow.h"
#include "ogreSceneManager_.h"
#include "ogreRenderTarget.h"
#include "ogreRenderSystem.h"
#include "ogreHardwareOcclusionQuery.h"
#include "ogreShadowCaster.h"
#include "_dataTypes.h"
#include "_singletonManagers.h"
#include "_ogreStringInterface.h"
#include "_ogreSceneQuery.h"
#include "ogreRenderQueue.h"
#include "ogreRenderQueueGroup.h"
#include "ogreSubMesh.h"
#include "ogreTechnique.h"
#include "ogreTimer.h"
#include "ogreHardwarePixelBuffer.h"
#include "TOverlay.h"
#include "ogreOverlayContainer.h"
#include "TOverlayElement.h"
#include "TParticle/_TParticle.h"
#include "TManualObject.h"
#include "TNodeAnimationTrack.h"
#include  "TOgreKeyFrame.h"
#include "_Iterators.h"
#include "TBillboard.h"
#include "TAnimationStateSet.h"
#include "TOgreImage.h"
#include "TPass.h"
#include "TGpuProgramParameters.h"
#include "TTextManager.h"
#include "TVertexElement.h"
#include "THardwareVertexBuffer.h"
#include "THardwareIndexBuffer.h"
#include "TVertexBufferBinding.h"
#include "TIndexData.h"
#include "TOgreLog.h"
#include "TTextureUnitState.h"
#include "TSubEntity.h"

// Ofusion
#include "tinyxml.h"
#include "OgreOSMScene.h"
#include "IOSMSceneCallbacks.h"
#include "oFusion.h"

// OgreMax
#include "OgreMax.h"



#endif
