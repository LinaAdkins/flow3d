#ifndef H_OGRERENDERQUEUEGROUP
#define H_OGRERENDERQUEUEGROUP

EXPORTFUNC void RQG_setShadowsEnabled( bool enabled , Ogre::RenderQueueGroup* rqg )
{
	rqg->setShadowsEnabled( enabled );
}

EXPORTFUNC int RQG_getShadowsEnabled( Ogre::RenderQueueGroup* rqg )
{
	return rqg->getShadowsEnabled();
}

#endif
