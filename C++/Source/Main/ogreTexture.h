#ifndef H_OGRETEXTURE
#define H_OGRETEXTURE

EXPORTFUNC Ogre::HardwarePixelBuffer* TEXTURE_getBuffer( int face , int mipmap , Ogre::Texture* t )
{
	return t->getBuffer( face , mipmap ).getPointer();
}

#endif
