#ifndef H_TBILLBOARD
#define H_TBILLBOARD

EXPORTFUNC Ogre::Radian* BILLBOARD_getRotation( Ogre::Billboard* b )
{
	return new Ogre::Radian( b->getRotation()  );
}

EXPORTFUNC void BILLBOARD_setRotation( Ogre::Radian* rotation , Ogre::Billboard* b )
{
	b->setRotation( *rotation );
}

EXPORTFUNC void BILLBOARD_setPosition( Ogre::Vector3* position , Ogre::Billboard* b )
{
	b->setPosition( *position );
}

EXPORTFUNC void BILLBOARD_setDimensions( float width , float height , Ogre::Billboard* b )
{
	b->setDimensions( width , height );
}

EXPORTFUNC void BILLBOARD_setColour( Ogre::ColourValue* colour , Ogre::Billboard* b )
{
	b->setColour( *colour );
}

EXPORTFUNC Ogre::ColourValue* BILLBOARD_getColour( Ogre::Billboard* b )
{
	return new Ogre::ColourValue( b->getColour() );
}

EXPORTFUNC void BILLBOARD_setTexcoordRect( float u0 , float v0 , float u1 , float v1 , Ogre::Billboard* b )
{
	b->setTexcoordRect( u0 , v0 , u1 , v1 );
}

#endif
