#ifndef H_TMANUALOBJECTSECTION
#define H_TMANUALOBJECTSECTION

//ManualObjectSection (ManualObject *parent, const String ..   ManualObjectSections are created and destroyed thru
//virtual 	~ManualObjectSection ()                            the scene manager.

EXPORTFUNC Ogre::RenderOperation* MOS_getRenderOperationRef( Ogre::ManualObject::ManualObjectSection* s )
{
	return s->getRenderOperation();
}

EXPORTFUNC const char* MOS_getMaterialName(  Ogre::ManualObject::ManualObjectSection* s  )
{
	tempString = s->getMaterialName();
	return tempString.c_str();
}

EXPORTFUNC void MOS_setMaterialName( const char* name , Ogre::ManualObject::ManualObjectSection* s )
{
	s->setMaterialName( name );
}

EXPORTFUNC void MOS_set32BitIndices( bool n32 , Ogre::ManualObject::ManualObjectSection* s )
{
	s->set32BitIndices(n32);
}

EXPORTFUNC bool MOS_get32BitIndices( Ogre::ManualObject::ManualObjectSection* s )
{
	return s->get32BitIndices();
}

#endif
