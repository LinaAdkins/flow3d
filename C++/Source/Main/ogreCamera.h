#ifndef H_OGRECAMERA
#define H_OGRECAMERA

EXPORTFUNC void CAM_setFOVy( Ogre::Radian* fovy , Ogre::Camera* c )
{
	c->setFOVy( *fovy );
}

EXPORTFUNC Ogre::Radian* CAM_getFOVy( Ogre::Camera* c )
{
	return new Ogre::Radian( c->getFOVy() );
}


EXPORTFUNC Ogre::SceneManager* CAM_getSceneManager( Ogre::Camera* ogreCamera )
{
	return ogreCamera->getSceneManager();
}

EXPORTFUNC void CAM_setPolygonMode( Ogre::PolygonMode sd , Ogre::Camera* camera )
{
	camera->setPolygonMode( sd );
}
EXPORTFUNC int CAM_getPolygonMode( Ogre::Camera* ogreCamera )
{
	return ogreCamera->getPolygonMode();
}

EXPORTFUNC void CAM_setPosition( float x , float y , float z , Ogre::Camera* ogreCamera )
{
	ogreCamera->setPosition( Ogre::Vector3( x , y , z) );
}

EXPORTFUNC Ogre::Vector3* CAM_getPosition( Ogre::Camera* ogreCamera )
{
	return new Ogre::Vector3( ogreCamera->getPosition() );
}


EXPORTFUNC void CAM_move( Ogre::Vector3* ogreVector , Ogre::Camera* ogreCamera )
{
	ogreCamera->move( (const Ogre::Vector3 &)ogreVector );
}

EXPORTFUNC void CAM_setDirection( Ogre::Vector3* vec , Ogre::Camera* c )
{
	c->setDirection( *vec );
}

EXPORTFUNC Ogre::Vector3* CAM_getDirection( Ogre::Camera* c )
{
	return new Ogre::Vector3( c->getDirection() );
}

EXPORTFUNC Ogre::Vector3* CAM_getUp( Ogre::Camera* c )
{
	return new Ogre::Vector3( c->getUp() );
}

EXPORTFUNC Ogre::Vector3* CAM_getRight( Ogre::Camera* c )
{
	return new Ogre::Vector3( c->getRight() );
}

EXPORTFUNC void CAM_lookAt( float x , float y , float z , Ogre::Camera* c )
{
	c->lookAt( x , y , z );
}

EXPORTFUNC void CAM_lookAtWithVector3( Ogre::Vector3* targetPoint , Ogre::Camera* c )
{
	c->lookAt( *targetPoint );
}


EXPORTFUNC void CAM_setNearClipDistance( float NearClipDistance , Ogre::Camera* ogreCamera )
{
	ogreCamera->setNearClipDistance( NearClipDistance );
}

EXPORTFUNC float CAM_getNearClipDistance( Ogre::Camera* c )
{
	return c->getNearClipDistance();
}


EXPORTFUNC void CAM_setFarClipDistance( float FarClipDistance , Ogre::Camera* ogreCamera )
{
	ogreCamera->setFarClipDistance( FarClipDistance );
}

EXPORTFUNC float CAM_getFarClipDistance( Ogre::Camera* c )
{
	return c->getFarClipDistance();
}

EXPORTFUNC float CAM_getAspectRatio( Ogre::Camera* c )
{
	return c->getAspectRatio();
}


EXPORTFUNC void CAM_setAspectRatio( float AspectRatio , Ogre::Camera* ogreCamera )
{
	ogreCamera->setAspectRatio( (Ogre::Real) AspectRatio );
}
EXPORTFUNC void CAM_yaw( float degree , Ogre::Camera* ogreCamera )
{
	ogreCamera->yaw( Ogre::Degree( degree ) );
}

EXPORTFUNC void CAM_pitch( float degree , Ogre::Camera* ogreCamera )
{
	ogreCamera->pitch( Ogre::Degree( degree ) );
}

EXPORTFUNC void CAM_roll( float degree , Ogre::Camera* ogreCamera )
{
	ogreCamera->roll( Ogre::Degree( degree ) );
}

EXPORTFUNC void CAM_getCameraToViewportRay( float posx , float posy , Ogre::Ray* ogreRay , Ogre::Camera* ogreCamera )
{
	*ogreRay = ogreCamera->getCameraToViewportRay( posx ,  posy );
}

EXPORTFUNC void CAM_setAutoTracking( bool enabled , Ogre::SceneNode* ogreSceneNode , float x , float y , float z , Ogre::Camera* ogreCamera )
{
	ogreCamera->setAutoTracking( enabled , ogreSceneNode , Ogre::Vector3( x ,  y , z ) );
}

EXPORTFUNC void CAM_setFixedYawAxis( bool useFixed , Ogre::Vector3* fixedAxis , Ogre::Camera* c )
{
	c->setFixedYawAxis( useFixed , *fixedAxis );
}

EXPORTFUNC Ogre::Quaternion* CAM_getOrientation( Ogre::Camera* c )
{
	return new Ogre::Quaternion( c->getOrientation() );
}

EXPORTFUNC void CAM_setOrientation( Ogre::Quaternion* ogreQuaternion , Ogre::Camera* ogreCamera )
{
	ogreCamera->setOrientation( *ogreQuaternion );
}
EXPORTFUNC Ogre::Quaternion* CAM_getDerivedOrientation( Ogre::Camera* c )
{
	return new Ogre::Quaternion( c->getDerivedOrientation() );
}

EXPORTFUNC Ogre::Vector3* CAM_getDerivedDirection( Ogre::Camera* c )
{
	return new Ogre::Vector3( c->getDerivedDirection() );
}

EXPORTFUNC Ogre::Vector3* CAM_getDerivedUp( Ogre::Camera* c )
{
	return new Ogre::Vector3( c->getDerivedUp() );
}

EXPORTFUNC Ogre::Vector3* CAM_getDerivedRight( Ogre::Camera* c )
{
	return new Ogre::Vector3( c->getDerivedRight() );
}

EXPORTFUNC Ogre::Quaternion* CAM_getRealOrientation( Ogre::Camera* c )
{
	return new Ogre::Quaternion( c->getRealOrientation() );
}

EXPORTFUNC Ogre::Vector3* CAM_getRealDirection( Ogre::Camera* c )
{
	return new Ogre::Vector3( c->getRealDirection() );
}

EXPORTFUNC Ogre::Vector3* CAM_getRealUp( Ogre::Camera* c )
{
	return new Ogre::Vector3( c->getDerivedUp() );
}

EXPORTFUNC Ogre::Vector3* CAM_getRealRight( Ogre::Camera* c )
{
	return new Ogre::Vector3( c->getDerivedRight() );
}


EXPORTFUNC Ogre::Matrix4* CAM_getViewMatrix( Ogre::Camera* ogreCamera )
{
	return new Ogre::Matrix4( ogreCamera->getViewMatrix() );
}

EXPORTFUNC void CAM_setWindow( float x , float y , float width , float height , Ogre::Camera* cam )
{
	cam->setWindow( x , y  , width , height );
}

EXPORTFUNC Ogre::Viewport* CAM_getViewport( Ogre::Camera* cam )
{
	return cam->getViewport();
}


#endif
