#ifndef H_OGRERENDERTARGET
#define H_OGRERENDERTARGET

EXPORTFUNC void RT_delete( Ogre::RenderTarget* ogreRenderTarget )
{
	delete ogreRenderTarget;
}

EXPORTFUNC const char* RT_getName( Ogre::RenderTarget* ogreRenderTarget )
{
	return ogreRenderTarget->getName().c_str();
}

EXPORTFUNC void RT_getMetrics( unsigned int* width , unsigned int* height , unsigned int* colourDepth , Ogre::RenderTarget* ogreRenderTarget )
{
	ogreRenderTarget->getMetrics( *width , *height , *colourDepth );
}

EXPORTFUNC unsigned int RT_getWidth( Ogre::RenderTarget* ogreRenderTarget )
{
	return ogreRenderTarget->getWidth();
}

EXPORTFUNC unsigned int RT_getHeight( Ogre::RenderTarget* ogreRenderTarget )
{
	return ogreRenderTarget->getHeight();
}

EXPORTFUNC unsigned int RT_getColourDepth( Ogre::RenderTarget* ogreRenderTarget )
{
	return ogreRenderTarget->getColourDepth();
}

EXPORTFUNC void RT_update( bool swapBuffers , Ogre::RenderTarget* t )
{
	t->update(swapBuffers);
}


EXPORTFUNC Ogre::Viewport* RT_addViewport( Ogre::Camera* ogreCamera , int ZOrder, float left, float top, float width, float height, Ogre::RenderTarget* ogreRenderTarget)
{
	Ogre::Viewport* ogreViewport = ogreRenderTarget->addViewport( ogreCamera , ZOrder , left, top, width, height );

	return ogreViewport;
}

EXPORTFUNC Ogre::Viewport* RT_getViewport( int index , Ogre::RenderTarget* r )
{
	return r->getViewport( index );
}


EXPORTFUNC void RT_getCustomAttribute( const char* name , void* pData , Ogre::RenderTarget* ogreRenderTarget )
{
	ogreRenderTarget->getCustomAttribute( name , pData );
}

EXPORTFUNC const char* RT_writeContentsToTimestampedFile( const char* FileName , const char* FileExtension , Ogre::RenderTarget* ogreRenderWindow)
{
	return ogreRenderWindow->writeContentsToTimestampedFile( FileName , FileExtension ).c_str();
}

EXPORTFUNC void RT_writeContentsToFile( const char* filename , Ogre::RenderTarget* ogreRenderTarget )
{
	ogreRenderTarget->writeContentsToFile( filename );
}

EXPORTFUNC int RT_getTriangleCount( Ogre::RenderTarget* rt )
{
	return rt->getTriangleCount();
}

EXPORTFUNC float RT_getLastFPS( Ogre::RenderTarget* rt )
{
	return rt->getLastFPS();
}

EXPORTFUNC float RT_getAverageFPS( Ogre::RenderTarget* rt )
{
	return rt->getAverageFPS();
}

EXPORTFUNC Ogre::ColourValue* RT_getPixelColour( int x , int y , Ogre::RenderTarget* t )
{
	// First we create our box for selection
	Ogre::Box box;
	box.left = x;
	box.right = x + 1;
	box.top = y;
	box.bottom = y + 1;
	box.front = 0;
	box.back = 1;

	// Then we create a pixel box to hold our data
	const Ogre::PixelBox& pBox = Ogre::PixelBox();
	t->copyContentsToMemory( pBox ); 

	Ogre::ColourValue color = Ogre::ColourValue();
	Ogre::PixelUtil::unpackColour( &color , pBox.format , pBox.getSubVolume(box).data );


	return new Ogre::ColourValue( color );
}

#endif
