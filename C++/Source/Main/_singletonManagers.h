#ifndef H__SINGLETONMANAGERS
#define H__SINGLETONMANAGERS

#include "ogreMaterialManager.h"
#include "ogreTextureManager.h"
#include "ogreMeshManager.h"
#include "ogreResourceGroupManager.h"
#include "ogreLogManager.h"
#include "ogreCompositorManager.h"
#include "ogreScriptLoader.h"
#include "ogreResourceManager.h"
#include "TOverlayManager.h"
#include "TSkeletonManager.h"
#include "THardwareBufferManager.h"

#endif
