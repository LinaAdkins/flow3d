#ifndef H_TANIMATIONSTATESET
#define H_TANIMATIONSTATESET

/*
Public Member Functions
*/

EXPORTFUNC Ogre::AnimationStateSet* ANIMSTATESET_create()
{
	return new Ogre::AnimationStateSet();
}

EXPORTFUNC Ogre::AnimationStateSet* ANIMSTATESET_createWithAnimationStateSet( Ogre::AnimationStateSet* rhs )
{
	return new Ogre::AnimationStateSet( *rhs );
}

EXPORTFUNC void ANIMSTATESET_delete( Ogre::AnimationStateSet* s )
{
	 delete s;
}

EXPORTFUNC Ogre::AnimationState* ANIMSTATESET_createAnimationState( const char* animName , float timePos , float length , float weight , bool enabled , Ogre::AnimationStateSet* s )
{
	return s->createAnimationState( animName , timePos , length , weight , enabled );
}

EXPORTFUNC Ogre::AnimationState* ANIMSTATESET_getAnimationState( const char* name , Ogre::AnimationStateSet* s )
{
	return s->getAnimationState( name );
}

EXPORTFUNC bool ANIMSTATESET_hasAnimationState( const char* name , Ogre::AnimationStateSet* s )
{
	return s->hasAnimationState( name );
}

EXPORTFUNC void ANIMSTATESET_removeAnimationState( const char* name , Ogre::AnimationStateSet* s )
{
	s->removeAnimationState( name );
}

EXPORTFUNC void ANIMSTATESET_removeAllAnimationStates( Ogre::AnimationStateSet* s )
{
	s->removeAllAnimationStates();
}

EXPORTFUNC Ogre::AnimationStateIterator* ANIMSTATESET_getAnimationStateIterator( Ogre::AnimationStateSet* s )
{
	return new Ogre::AnimationStateIterator( s->getAnimationStateIterator() );
}

/*
Const Iterator Unnecessary
ConstAnimationStateIterator 	getAnimationStateIterator (void) const
*/

EXPORTFUNC void ANIMSTATESET_copyMatchingState( Ogre::AnimationStateSet* target , Ogre::AnimationStateSet* s )
{
	s->copyMatchingState( target );
}

EXPORTFUNC void ANIMSTATESET__notifyDirty( Ogre::AnimationStateSet* s )
{
	s->_notifyDirty();
}

EXPORTFUNC unsigned long ANIMSTATESET_getDirtyFrameNumber( Ogre::AnimationStateSet* s )
{
	return s->getDirtyFrameNumber();
}

EXPORTFUNC void ANIMSTATESET__notifyAnimationStateEnabled( Ogre::AnimationState* target , bool enabled , Ogre::AnimationStateSet* s )
{
	s->_notifyAnimationStateEnabled( target, enabled );
}

EXPORTFUNC bool ANIMSTATESET_hasEnabledAnimationState( Ogre::AnimationStateSet* s )
{
	return s->hasEnabledAnimationState();
}

/*
Unecessary
ConstEnabledAnimationStateIterator 	getEnabledAnimationStateIterator (void) const

Alloc overloads unnecessary
void * 	operator new (size_t sz, const char *file, int line, const char *func)
void * 	operator new (size_t sz)
void * 	operator new (size_t sz, void *ptr)
void * 	operator new[] (size_t sz, const char *file, int line, const char *func)
void * 	operator new[] (size_t sz)
void 	operator delete (void *ptr)
void 	operator delete (void *ptr, const char *, int, const char *)
void 	operator delete[] (void *ptr)
void 	operator delete[] (void *ptr, const char *, int, const char *)
*/


#endif
