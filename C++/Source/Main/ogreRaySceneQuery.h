#ifndef H_OGRERAYSCENEQUERY
#define H_OGRERAYSCENEQUERY

EXPORTFUNC void RSQUERY_setRay( Ogre::Ray* ogreRay , Ogre::RaySceneQuery* ogreRaySceneQuery )
{
	ogreRaySceneQuery->setRay( *ogreRay );
}

EXPORTFUNC Ogre::Ray* RSQUERY_getRay( Ogre::RaySceneQuery* rsq )
{
	return new Ogre::Ray(rsq->getRay());
}

EXPORTFUNC void RSQUERY_setSortByDistance( bool sort , int maxResults , Ogre::RaySceneQuery* ogreRaySceneQuery )
{
	ogreRaySceneQuery->setSortByDistance( sort , maxResults );
}

EXPORTFUNC int RSQUERY_getSortByDistance( Ogre::RaySceneQuery* rsq )
{
	return rsq->getSortByDistance();
}

EXPORTFUNC int RSQUERY_getMaxResults( Ogre::RaySceneQuery* rsq )
{
	return rsq->getMaxResults();
}


EXPORTFUNC Ogre::RaySceneQueryResult* RSQUERY_execute( Ogre::RaySceneQuery* ogreRaySceneQuery )
{
	Ogre::RaySceneQueryResult* result = new Ogre::RaySceneQueryResult( ogreRaySceneQuery->execute() );
	return result;
	
	
	/*Ogre::RaySceneQueryResult::iterator it = result.begin();

	if( it!= result.end() )
	{
		*ogreVector = ogreRaySceneQuery->getRay().getOrigin()+( ogreRaySceneQuery->getRay().getDirection() * it->distance );
		return it->movable;
	}

	return 0;*/
}

EXPORTFUNC void RSQUERY_executeWithListener( Ogre::RaySceneQueryListener* listener , Ogre::RaySceneQuery* rsq )
{
	rsq->execute( listener );
}

EXPORTFUNC Ogre::RaySceneQueryResult* RSQUERY_getLastResults( Ogre::RaySceneQuery* rsq )
{
	Ogre::RaySceneQueryResult* result = new Ogre::RaySceneQueryResult( rsq->getLastResults() );
	return result;
}

EXPORTFUNC void RSQUERY_clearResults( Ogre::RaySceneQuery* ogreRaySceneQuery )
{
	ogreRaySceneQuery->clearResults();
}

EXPORTFUNC int RSQUERY_queryResult( Ogre::MovableObject* obj , float distance , Ogre::RaySceneQuery* rsq )
{
	return rsq->queryResult( obj , distance );
}

EXPORTFUNC int RSQUERY_queryResultWithWorldFragment( Ogre::SceneQuery::WorldFragment* fragment , float distance , Ogre::RaySceneQuery* rsq )
{
	return rsq->queryResult( fragment , distance );
}



#endif
