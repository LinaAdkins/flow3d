#ifndef H_OGRERENDERQUEUE
#define H_OGRERENDERQUEUE

EXPORTFUNC Ogre::RenderQueueGroup* RQ_getQueueGroup( Ogre::uint8 quid , Ogre::RenderQueue* rq )
{
	return rq->getQueueGroup( quid );
}

#endif
