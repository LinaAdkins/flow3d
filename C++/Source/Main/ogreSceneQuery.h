#ifndef H_OGRESCENEQUERY
#define H_OGRESCENEQUERY

EXPORTFUNC void SQ_setQueryMask( Ogre::uint32 mask , Ogre::SceneQuery* sq )
{
	sq->setQueryMask( mask );
}

EXPORTFUNC Ogre::uint32 SQ_getQueryMask( Ogre::SceneQuery* sq )
{
	return sq->getQueryMask();
}

EXPORTFUNC void SQ_setQueryTypeMask( Ogre::uint32 mask , Ogre::SceneQuery* sq )
{
	sq->setQueryTypeMask( mask );
}

EXPORTFUNC Ogre::uint32 SQ_getQueryTypeMask( Ogre::SceneQuery* sq )
{
	return sq->getQueryTypeMask();
}

EXPORTFUNC void SQ_setWorldFragmentType( Ogre::SceneQuery::WorldFragmentType wft , Ogre::SceneQuery* sq )
{
	sq->setWorldFragmentType( wft );
}

EXPORTFUNC int SQ_getWorldFragmentType( Ogre::SceneQuery* sq )
{
	return sq->getWorldFragmentType();
}

EXPORTFUNC const std::set< Ogre::SceneQuery::WorldFragmentType >* SQ_getSupportedWorldFragmentTypes( Ogre::SceneQuery* sq )
{
	return sq->getSupportedWorldFragmentTypes();
}

#endif
