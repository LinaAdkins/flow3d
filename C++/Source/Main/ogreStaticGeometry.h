#ifndef H_OGRESTATICGEOMETRY
#define H_OGRESTATICGEOMETRY

//StaticGeometry (SceneManager *owner, const String &name)		// Static Geometry is managed through the
//virtual 	~StaticGeometry ()									// Scene manager-

EXPORTFUNC const char* SGEOMETRY_getName( Ogre::StaticGeometry* g )
{
	return g->getName().c_str();
}

EXPORTFUNC void SGEOMETRY_addEntity( Ogre::Entity* ent , Ogre::Vector3* position , Ogre::Quaternion* orientation , Ogre::Vector3* scale , Ogre::StaticGeometry* g )
{
	g->addEntity( ent , *position , *orientation , *scale );
}

EXPORTFUNC void SGEOMETRY_addSceneNode( Ogre::SceneNode* node , Ogre::StaticGeometry* g )
{
	g->addSceneNode( node );
}

EXPORTFUNC void SGEOMETRY_build( Ogre::StaticGeometry* g )
{
	g->build();
}

EXPORTFUNC void SGEOMETRY_destroy( Ogre::StaticGeometry* g )
{
	g->destroy();
}

EXPORTFUNC void SGEOMETRY_reset( Ogre::StaticGeometry* g )
{
	g->reset();
}

EXPORTFUNC void SGEOMETRY_setRenderingDistance( float dist , Ogre::StaticGeometry* g )
{
	g->setRenderingDistance( dist );
}

EXPORTFUNC float SGEOMETRY_getRenderingDistance( Ogre::StaticGeometry* g )
{
	return g->getRenderingDistance();
}

EXPORTFUNC float SGEOMETRY_getSquaredRenderingDistance( Ogre::StaticGeometry* g )
{
	return g->getSquaredRenderingDistance();
}

EXPORTFUNC void SGEOMETRY_setVisible( bool visible , Ogre::StaticGeometry* g )
{
	g->setVisible( visible );
}

EXPORTFUNC bool SGEOMETRY_isVisible( Ogre::StaticGeometry* g )
{
	return g->isVisible();
}

EXPORTFUNC void SGEOMETRY_setCastShadows( bool castShadows , Ogre::StaticGeometry* g )
{
	g->setCastShadows( castShadows );
}

EXPORTFUNC bool SGEOMETRY_getCastShadows( Ogre::StaticGeometry* g )
{
	return g->getCastShadows();
}

EXPORTFUNC void SGEOMETRY_setRegionDimensions( Ogre::Vector3* size , Ogre::StaticGeometry* g )
{
	g->setRegionDimensions( *size );
}

EXPORTFUNC Ogre::Vector3* SGEOMETRY_getRegionDimensions( Ogre::StaticGeometry* g )
{
	return new Ogre::Vector3( g->getRegionDimensions() );
}

EXPORTFUNC void SGEOMETRY_setOrigin( Ogre::Vector3* origin , Ogre::StaticGeometry* g )
{
	g->setOrigin( *origin );
}

EXPORTFUNC Ogre::Vector3* SGEOMETRY_getOrigin( Ogre::StaticGeometry* g )
{
	return new Ogre::Vector3( g->getOrigin() );
}

EXPORTFUNC void SGEOMETRY_setRenderQueueGroup( int queueID , Ogre::StaticGeometry* g )
{
	g->setRenderQueueGroup( queueID );
}

EXPORTFUNC int SGEOMETRY_getRenderQueueGroup( Ogre::StaticGeometry* g )
{
	return g->getRenderQueueGroup();
}

// RegionIterator 	getRegionIterator (void) - Not implemented

EXPORTFUNC void SGEOMETRY_dump( const char* filename , Ogre::StaticGeometry* g )
{
	g->dump( filename );
}












#endif
