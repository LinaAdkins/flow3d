#ifndef H_OGREAXISALIGNEDBOX
#define H_OGREAXISALIGNEDBOX

// Public Member Functions
EXPORTFUNC Ogre::AxisAlignedBox* AAB_createEmpty()
{
	return new Ogre::AxisAlignedBox();
}

EXPORTFUNC Ogre::AxisAlignedBox* AAB_createWithExtent( int e )
{
	return new Ogre::AxisAlignedBox( Ogre::AxisAlignedBox::Extent( e ) );
}

EXPORTFUNC Ogre::AxisAlignedBox* AAB_createWithAAB( Ogre::AxisAlignedBox* rkBox )
{
	return new Ogre::AxisAlignedBox( *rkBox );

}

EXPORTFUNC Ogre::AxisAlignedBox* AAB_createWithMinMax( Ogre::Vector3* min , Ogre::Vector3* max )
{
	return new Ogre::AxisAlignedBox( *min , *max );
		
}

EXPORTFUNC Ogre::AxisAlignedBox* AAB_create( float mx , float my , float mz , float Mx , float My , float Mz )
{
	return new Ogre::AxisAlignedBox( mx , my , mz , Mx , My , Mz );
}

EXPORTFUNC void AAB_eq( Ogre::AxisAlignedBox* rhs , Ogre::AxisAlignedBox* b )
{
	*b=*rhs;
}

EXPORTFUNC void AAB_delete( Ogre::AxisAlignedBox* b )
{
	delete b;
}

// const Vector3 & 	getMinimum (void) const - Const uneccessary

EXPORTFUNC Ogre::Vector3* AAB_getMinimum( Ogre::AxisAlignedBox* b )
{
	return new Ogre::Vector3( b->getMinimum());
}

// const Vector3 & 	getMaximum (void) const - Const uneccessary

EXPORTFUNC Ogre::Vector3* AAB_getMaximum( Ogre::AxisAlignedBox* b )
{
	return new Ogre::Vector3( b->getMaximum());
}

EXPORTFUNC void AAB_setMinimumWithVector( Ogre::Vector3* vec , Ogre::AxisAlignedBox* b )
{
	b->setMinimum( *vec );
}

EXPORTFUNC void AAB_setMinimum( float x , float y , float z , Ogre::AxisAlignedBox* b )
{
	b->setMinimum( x , y , z );
}

EXPORTFUNC void AAB_setMinimumX( float x , Ogre::AxisAlignedBox* b )
{
	b->setMinimumX( x );
}

EXPORTFUNC void AAB_setMinimumY( float y , Ogre::AxisAlignedBox* b )
{
	b->setMinimumY( y );
}

EXPORTFUNC void AAB_setMinimumZ( float z , Ogre::AxisAlignedBox* b )
{
	b->setMinimumZ( z );
}

EXPORTFUNC void AAB_setMaximumWithVector( Ogre::Vector3* vec , Ogre::AxisAlignedBox* b )
{
	b->setMaximum( *vec );
}

EXPORTFUNC void AAB_setMaximum( float x , float y , float z , Ogre::AxisAlignedBox* b )
{
	b->setMaximum( x , y , z );
}

EXPORTFUNC void AAB_setMaximumX( float x , Ogre::AxisAlignedBox* b )
{
	b->setMaximumX( x );
}

EXPORTFUNC void AAB_setMaximumY( float y , Ogre::AxisAlignedBox* b )
{
	b->setMaximumY( y );
}

EXPORTFUNC void AAB_setMaximumZ( float z , Ogre::AxisAlignedBox* b )
{
	b->setMaximumZ( z );
}

EXPORTFUNC void AAB_setExtents( Ogre::Vector3* min , Ogre::Vector3* max , Ogre::AxisAlignedBox* b )
{
	b->setExtents( *min , *max );
}

EXPORTFUNC void AAB_setExtentsWithFloats( float mx , float my, float mz , float Mx , float My , float Mz , Ogre::AxisAlignedBox* b )
{
	b->setExtents( mx , my , mz , Mx , My , Mz );
}

// const Vector3 * 	getAllCorners (void) const - BlitzMax has no support for typed arrays

EXPORTFUNC Ogre::Vector3* AAB_getCorner( int cornerToGet , Ogre::AxisAlignedBox* b )
{
	return new Ogre::Vector3( b->getCorner( Ogre::AxisAlignedBox::CornerEnum( cornerToGet ) ) );
}

EXPORTFUNC void AAB_merge( Ogre::AxisAlignedBox* rhs , Ogre::AxisAlignedBox* b )
{
	b->merge( *rhs );
}

EXPORTFUNC void AAB_mergeWithPoint( Ogre::Vector3* point , Ogre::AxisAlignedBox* b )
{
	b->merge( *point );
}

EXPORTFUNC void AAB_transform( Ogre::Matrix4* matrix , Ogre::AxisAlignedBox* b )
{
	b->transform( *matrix );
}

EXPORTFUNC void AAB_transformAffine( Ogre::Matrix4* matrix , Ogre::AxisAlignedBox* b )
{
	b->transformAffine( *matrix );
}

// Named differently to avoid function confusion
EXPORTFUNC void AAB_setBoxNull( Ogre::AxisAlignedBox* b )
{
	b->setNull();
}

// Named differently to avoid function confusion 
EXPORTFUNC bool AAB_isBoxNull( Ogre::AxisAlignedBox* b )
{
	return b->isNull();
}

EXPORTFUNC bool AAB_isFinite( Ogre::AxisAlignedBox* b )
{
	return b->isFinite();
}

EXPORTFUNC void AAB_setInfinite( Ogre::AxisAlignedBox* b )
{
	b->setInfinite();
}

EXPORTFUNC bool AAB_isInfinite( Ogre::AxisAlignedBox* b )
{
	return b->isInfinite();
}

EXPORTFUNC bool AAB_intersects( Ogre::AxisAlignedBox* b2 , Ogre::AxisAlignedBox* b )
{
	return b->intersects( *b2 );
}

EXPORTFUNC Ogre::AxisAlignedBox* AAB_intersection( Ogre::AxisAlignedBox* b2 , Ogre::AxisAlignedBox* b )
{
	return new Ogre::AxisAlignedBox( b->intersection( *b2 ) );
}

EXPORTFUNC float AAB_volume( Ogre::AxisAlignedBox* b )
{
	return b->volume();
}

EXPORTFUNC void AAB_scale( Ogre::Vector3* s , Ogre::AxisAlignedBox* b )
{
	b->scale( *s );
}

EXPORTFUNC bool AAB_intersectsSphere( Ogre::Sphere* s , Ogre::AxisAlignedBox* b )
{
	return b->intersects( *s );
}

EXPORTFUNC bool AAB_intersectsPlane( Ogre::Plane* p , Ogre::AxisAlignedBox* b )
{
	return b->intersects( *p );
}

EXPORTFUNC bool AAB_intersectsVector( Ogre::Vector3* v , Ogre::AxisAlignedBox* b )
{
	return b->intersects( *v );
}

EXPORTFUNC Ogre::Vector3* AAB_getCenter( Ogre::AxisAlignedBox* b )
{
	return new Ogre::Vector3( b->getCenter() );
}

EXPORTFUNC Ogre::Vector3* AAB_getSize( Ogre::AxisAlignedBox* b )
{
	return new Ogre::Vector3( b->getSize() );
}

EXPORTFUNC Ogre::Vector3* AAB_getHalfSize( Ogre::AxisAlignedBox* b )
{
	return new Ogre::Vector3( b->getHalfSize() );
}

EXPORTFUNC bool AAB_containsVector( Ogre::Vector3* v , Ogre::AxisAlignedBox* b )
{
	return b->contains( *v );
}

EXPORTFUNC bool AAB_contains( Ogre::AxisAlignedBox* other , Ogre::AxisAlignedBox* b )
{
	return b->contains( *other );
}

EXPORTFUNC bool AAB_isEqualTo( Ogre::AxisAlignedBox* rhs , Ogre::AxisAlignedBox* b )
{
	return *b == *rhs;
}

EXPORTFUNC bool AAB_isNotEqualTo( Ogre::AxisAlignedBox* rhs , Ogre::AxisAlignedBox* b )
{
	return *b != *rhs;
}

//Static Public
EXPORTFUNC Ogre::AxisAlignedBox* AAB_BOX_NULL()
{
	return (Ogre::AxisAlignedBox*)&Ogre::AxisAlignedBox::BOX_NULL;
}

EXPORTFUNC Ogre::AxisAlignedBox* AAB_BOX_INFINITE()
{
	return (Ogre::AxisAlignedBox*)&Ogre::AxisAlignedBox::BOX_INFINITE;
}

//Friends 
//For output to a string
EXPORTFUNC const char* AAB_toStr( Ogre::AxisAlignedBox* b )
{
	std::ostringstream os;
	os << *b;
	tempString.clear();
	tempString = os.str();
	return tempString.c_str();
}

#endif
