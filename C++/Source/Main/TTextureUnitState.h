#ifndef H_TTEXTUREUNITSTATE
#define H_TTEXTUREUNITSTATE

/*

Public Types
enum  	TextureEffectType {
  ET_ENVIRONMENT_MAP, ET_PROJECTIVE_TEXTURE, ET_UVSCROLL, ET_USCROLL,
  ET_VSCROLL, ET_ROTATE, ET_TRANSFORM
}
 	Definition of the broad types of texture effect you can apply to a texture unit. More...
enum  	EnvMapType { ENV_PLANAR, ENV_CURVED, ENV_REFLECTION, ENV_NORMAL }
 	Enumeration to specify type of envmap. More...
enum  	TextureTransformType {
  TT_TRANSLATE_U, TT_TRANSLATE_V, TT_SCALE_U, TT_SCALE_V,
  TT_ROTATE
}
 	Useful enumeration when dealing with procedural transforms. More...
enum  	TextureAddressingMode { TAM_WRAP, TAM_MIRROR, TAM_CLAMP, TAM_BORDER }
 	Texture addressing modes - default is TAM_WRAP. More...
enum  	TextureCubeFace {
  CUBE_FRONT = 0, CUBE_BACK = 1, CUBE_LEFT = 2, CUBE_RIGHT = 3,
  CUBE_UP = 4, CUBE_DOWN = 5
}
 	Enum identifying the frame indexes for faces of a cube map (not the composite 3D type. More...
enum  	BindingType { BT_FRAGMENT = 0, BT_VERTEX = 1 }
 	The type of unit to bind the texture settings to. More...
enum  	ContentType { CONTENT_NAMED = 0, CONTENT_SHADOW = 1 }
 	Enum identifying the type of content this texture unit contains. More...
typedef
std::multimap
< TextureEffectType,
TextureEffect > 	EffectMap
 	Texture effects in a multimap paired array.

Public Member Functions
TextureUnitState (Pass *parent)
TextureUnitState (Pass *parent, const TextureUnitState &oth)
TextureUnitState & 	operator= (const TextureUnitState &oth)
~TextureUnitState ()
TextureUnitState (Pass *parent, const String &texName, unsigned int texCoordSet=0)
*/

EXPORTFUNC const char* TUS_getTextureName( Ogre::TextureUnitState* s )
{
	tempString = s->getTextureName();
	return tempString.c_str();
}

EXPORTFUNC void TUS_setTextureName( const char* name , int ttype , Ogre::TextureUnitState* s )
{
	s->setTextureName( name , Ogre::TextureType( ttype ) );
}

/*
void 	setCubicTextureName (const String &name, bool forUVW=false)
void 	setCubicTextureName (const String *const names, bool forUVW=false)
void 	setAnimatedTextureName (const String &name, unsigned int numFrames, Real duration=0)
void 	setAnimatedTextureName (const String *const names, unsigned int numFrames, Real duration=0)
std::pair< size_t,size_t > 	getTextureDimensions (unsigned int frame=0) const
void 	setCurrentFrame (unsigned int frameNumber)
unsigned int 	getCurrentFrame (void) const
const String & 	getFrameTextureName (unsigned int frameNumber) const
void 	setFrameTextureName (const String &name, unsigned int frameNumber)e.
void 	addFrameTextureName (const String &name)
void 	deleteFrameTextureName (const size_t frameNumber)
unsigned int 	getNumFrames (void) const
void 	setBindingType (BindingType bt)
BindingType 	getBindingType (void) const
void 	setContentType (ContentType ct)
ContentType 	getContentType (void) const
bool 	isCubic (void) const
bool 	is3D (void) const
TextureType 	getTextureType (void) const
void 	setDesiredFormat (PixelFormat desiredFormat)
PixelFormat 	getDesiredFormat (void) const
void 	setNumMipmaps (int numMipmaps)
int 	getNumMipmaps (void) const
void 	setIsAlpha (bool isAlpha)
bool 	getIsAlpha (void) const
void 	setHardwareGammaEnabled (bool enabled)
bool 	isHardwareGammaEnabled () const
unsigned int 	getTextureCoordSet (void) const
void 	setTextureCoordSet (unsigned int set)
void 	setTextureTransform (const Matrix4 &xform)
const Matrix4 & 	getTextureTransform (void) const
void 	setTextureScroll (Real u, Real v)
void 	setTextureUScroll (Real value)
Real 	getTextureUScroll (void) const
void 	setTextureVScroll (Real value)
Real 	getTextureVScroll (void) const
*/

EXPORTFUNC void TUS_setTextureUScale( float value  , Ogre::TextureUnitState* s )
{
	s->setTextureUScale( value );
}

EXPORTFUNC float TUS_getTextureUScale( Ogre::TextureUnitState* s )
{
	return s->getTextureUScale();
}

EXPORTFUNC void TUS_setTextureVScale( float value , Ogre::TextureUnitState* s )
{
	s->setTextureVScale( value );
}

EXPORTFUNC float TUS_getTextureVScale( Ogre::TextureUnitState* s )
{
	return s->getTextureVScale();
}

EXPORTFUNC void TUS_setTextureScale( float uScale , float vScale , Ogre::TextureUnitState* s )
{
	s->setTextureScale( uScale , vScale );
}

/*
void 	setTextureRotate (const Radian &angle)
const Radian & 	getTextureRotate (void) const
const UVWAddressingMode & 	getTextureAddressingMode (void) const
void 	setTextureAddressingMode (TextureAddressingMode tam)
void 	setTextureAddressingMode (TextureAddressingMode u, TextureAddressingMode v, TextureAddressingMode w)
void 	setTextureAddressingMode (const UVWAddressingMode &uvw)
void 	setTextureBorderColour (const ColourValue &colour)
const ColourValue & 	getTextureBorderColour (void) const
void 	setColourOperationEx (LayerBlendOperationEx op, LayerBlendSource source1=LBS_TEXTURE, LayerBlendSource source2=LBS_CURRENT, const ColourValue &arg1=ColourValue::White, const ColourValue &arg2=ColourValue::White, Real manualBlend=0.0)
void 	setColourOperation (const LayerBlendOperation op)
void 	setColourOpMultipassFallback (const SceneBlendFactor sourceFactor, const SceneBlendFactor destFactor)
const LayerBlendModeEx & 	getColourBlendMode (void) const
const LayerBlendModeEx & 	getAlphaBlendMode (void) const
SceneBlendFactor 	getColourBlendFallbackSrc (void) const
SceneBlendFactor 	getColourBlendFallbackDest (void) const
void 	setAlphaOperation (LayerBlendOperationEx op, LayerBlendSource source1=LBS_TEXTURE, LayerBlendSource source2=LBS_CURRENT, Real arg1=1.0, Real arg2=1.0, Real manualBlend=0.0)
void 	addEffect (TextureEffect &effect)
void 	setEnvironmentMap (bool enable, EnvMapType envMapType=ENV_CURVED)
void 	setScrollAnimation (Real uSpeed, Real vSpeed)
void 	setRotateAnimation (Real speed)
void 	setTransformAnimation (const TextureTransformType ttype, const WaveformType waveType, Real base=0, Real frequency=1, Real phase=0, Real amplitude=1)
void 	setProjectiveTexturing (bool enabled, const Frustum *projectionSettings=0)
void 	removeAllEffects (void)
void 	removeEffect (const TextureEffectType type)
bool 	isBlank (void) const
void 	setBlank (void)
bool 	isTextureLoadFailing () const
void 	retryTextureLoad ()
const EffectMap & 	getEffects (void) const
Real 	getAnimationDuration (void) const
void 	setTextureFiltering (TextureFilterOptions filterType)
void 	setTextureFiltering (FilterType ftype, FilterOptions opts)
void 	setTextureFiltering (FilterOptions minFilter, FilterOptions magFilter, FilterOptions mipFilter)
FilterOptions 	getTextureFiltering (FilterType ftpye) const
void 	setTextureAnisotropy (unsigned int maxAniso)
unsigned int 	getTextureAnisotropy () const
void 	setTextureMipmapBias (float bias)
float 	getTextureMipmapBias (void) const
Pass * 	getParent (void) const
void 	_prepare (void)
void 	_unprepare (void)
void 	_load (void)
void 	_unload (void)
bool 	hasViewRelativeTextureCoordinateGeneration (void) const
bool 	isLoaded (void) const
void 	_notifyNeedsRecompile (void)
void 	setName (const String &name)
const String & 	getName (void) const
void 	setTextureNameAlias (const String &name)
const String & 	getTextureNameAlias (void) const
bool 	applyTextureAliases (const AliasTextureNamePairList &aliasList, const bool apply=true)
void 	_notifyParent (Pass *parent)
const TexturePtr & 	_getTexturePtr (void) const
const TexturePtr & 	_getTexturePtr (size_t frame) const
void 	_setTexturePtr (const TexturePtr &texptr)
void 	_setTexturePtr (const TexturePtr &texptr, size_t frame)
Controller< Real > * 	_getAnimController () const
void * 	operator new (size_t sz, const char *file, int line, const char *func)
void * 	operator new (size_t sz)
void * 	operator new (size_t sz, void *ptr)
void * 	operator new[] (size_t sz, const char *file, int line, const char *func)
void * 	operator new[] (size_t sz)
void 	operator delete (void *ptr)
void 	operator delete (void *ptr, const char *, int, const char *)
void 	operator delete[] (void *ptr)
void 	operator delete[] (void *ptr, const char *, int, const char *)

*/
#endif
