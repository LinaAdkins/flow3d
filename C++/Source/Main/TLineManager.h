#ifndef H_TLINEMANAGER
#define H_TLINEMANAGER

/*
** This helper class is responsible for manipulating a set of managed lines created
** by an Ogre ManualObject.
** Todo:
** startline continueline endline _get2dmanualobject _get3dmanualobject _get2node _get3dnode _drawthickline2 and 3 , updateWithPoints2/3d( arrayofpoints[] )
*/
class TLineManager
{
public:

	// You must call initlines to ensure line objects are allocated before using line objects
	TLineManager::TLineManager( Ogre::SceneManager* s)
	{

		if(!s)
		{
			OGRE_EXCEPT( Ogre::Exception::ERR_INVALIDPARAMS , "TLineManager::TLineManager(...) -> Scene manager passed in is NULL!" , "TLineManager.h");
		}

		mSceneManager = s;
	
		mNode2d = mSceneManager->getRootSceneNode()->createChildSceneNode();
		mNode3d = mSceneManager->getRootSceneNode()->createChildSceneNode();
		
		mManualObject2d = mSceneManager->createManualObject( Ogre::String( "mo2d_" + mNode2d->getName()) );
		mNode2d->attachObject( mManualObject2d );
		mManualObject2d->setUseIdentityProjection(true);
		mManualObject2d->setUseIdentityView(true);

		mManualObject3d = mSceneManager->createManualObject( Ogre::String( "mo3d_" + mNode3d->getName()) );
		mNode3d->attachObject( mManualObject3d );

		// Create base material name strings
		m2dMaterial = new std::string("BaseWhiteNoLighting");
		m3dMaterial = new std::string("BaseWhiteNoLighting");

		return;
	}

	// Clean up all line references
	TLineManager::~TLineManager()
	{
		if( !mManualObject2d || !mManualObject3d || !mNode2d || !mNode3d || !mSceneManager )
		{
			OGRE_EXCEPT( Ogre::Exception::ERR_INVALID_STATE , "TLineManager::~TLineManager(...) -> Null references when trying to cleanup!" , "TLineManager.h" );
		}

		// Clear out 2d & 3d nodes and MOs
		mNode2d->detachAllObjects();
		mSceneManager->destroySceneNode(mNode2d);
		mSceneManager->destroyManualObject( mManualObject2d );
		mNode2d = NULL;
		mManualObject2d = NULL;

		mNode3d->detachAllObjects();
		mSceneManager->destroySceneNode(mNode3d);
		mSceneManager->destroyManualObject( mManualObject3d );
		mNode3d = NULL;
		mManualObject3d = NULL;

		// Destroy our material names.
		delete m2dMaterial;
		delete m3dMaterial;
		m2dMaterial = NULL;
		m3dMaterial = NULL;

		// Clear scene manager reference
		mSceneManager = NULL;

		return;
	}

	void TLineManager::Set2DMaterialName( const char* name )
	{
		*m2dMaterial = name;
	}

	void TLineManager::Draw2DLine( float x1 , float y1 , float x2 , float y2 )
	{
		mManualObject2d->begin(*m2dMaterial , Ogre::RenderOperation::OT_LINE_STRIP);
		mManualObject2d->position( x1 , y1 , 0 );
		mManualObject2d->position( x2 , y2 , 0 );

		mManualObject2d->index(0);
		mManualObject2d->index(1);
		mManualObject2d->index(0);

		mManualObject2d->end();

		return;
	}

	void TLineManager::Clear2DLines()
	{
		mManualObject2d->clear();
	} 

	void TLineManager::Set3DMaterialName( const char* name )
	{
		*m3dMaterial = name;
	}

	void TLineManager::Draw3DLine( float x1 , float y1 , float z1 , float x2 , float y2, float z2 )
	{
		mManualObject3d->begin(*m3dMaterial , Ogre::RenderOperation::OT_LINE_STRIP );
		mManualObject3d->position( x1 , y1 , z1 );
		mManualObject3d->position( x2 , y2 , z2 );

		mManualObject3d->index(0);
		mManualObject3d->index(1);
		mManualObject3d->index(0);

		mManualObject3d->end();

		return;
	}

	void TLineManager::Clear3DLines()
	{
		mManualObject3d->clear();
	}

private:

	Ogre::SceneNode* mNode2d;
	Ogre::SceneNode* mNode3d;
	Ogre::ManualObject* mManualObject2d;
	Ogre::ManualObject* mManualObject3d;
	Ogre::SceneManager* mSceneManager;
	std::string* m2dMaterial;
	std::string* m3dMaterial;

};

EXPORTFUNC TLineManager* LINEMANAGER_create( Ogre::SceneManager* s )
{
	return new TLineManager( s );
}

EXPORTFUNC void LINEMANAGER_delete( TLineManager* m )
{
	delete m;
}

EXPORTFUNC void LINEMANAGER_Draw2DLine( float x1, float y1 , float x2 , float y2 , TLineManager* m  )
{
	m->Draw2DLine( x1 , y1 , x2 , y2 );
}

EXPORTFUNC void LINEMANAGER_Set2DMaterialName( const char* name , TLineManager* m )
{
	m->Set2DMaterialName( name );
}

EXPORTFUNC void LINEMANAGER_Clear2DLines( TLineManager* m )
{
	m->Clear2DLines();
}

EXPORTFUNC void LINEMANAGER_Set3DMaterialName( const char* name , TLineManager* m )
{
	m->Set3DMaterialName( name );
}

EXPORTFUNC void LINEMANAGER_Draw3DLine( float x1 , float y1, float z1 , float x2 , float y2, float z2 , TLineManager* m )
{
	m->Draw3DLine( x1 , y1 , z1 , x2 , y2 , z2 );
}

EXPORTFUNC void LINEMANAGER_Clear3DLines( TLineManager* m )
{
	m->Clear3DLines();
}

#endif
