#ifndef H_OGRERESOURCE
#define H_OGRERESOURCE

EXPORTFUNC void RESOURCE_load( bool backgroundThread ,  Ogre::Resource* ogreResource )
{
	ogreResource->load( backgroundThread );
}

EXPORTFUNC void RESOURCE_reload( Ogre::Resource* ogreResource )
{
	ogreResource->reload();
}

EXPORTFUNC int RESOURCE_isReloadable( Ogre::Resource* ogreResource )
{
	return ogreResource->isReloadable();
}

EXPORTFUNC int RESOURCE_isManuallyLoaded( Ogre::Resource* ogreResource )
{
	return ogreResource->isManuallyLoaded();
}

EXPORTFUNC void RESOURCE_unload( Ogre::Resource* ogreResource )
{
	ogreResource->unload();
}

EXPORTFUNC unsigned int RESOURCE_getSize( Ogre::Resource* ogreResource )
{
	return ogreResource->getSize();
}

EXPORTFUNC void RESOURCE_touch( Ogre::Resource* ogreResource )
{
	ogreResource->touch();
}

EXPORTFUNC const char* RESOURCE_getName( Ogre::Resource* ogreResource )
{
	return ogreResource->getName().c_str();
}
EXPORTFUNC unsigned long RESOURCE_getHandle( Ogre::Resource* ogreResource )
{
	return ogreResource->getHandle();
}
EXPORTFUNC int RESOURCE_isLoaded( Ogre::Resource* ogreResource )
{
	return ogreResource->isLoaded();
}

EXPORTFUNC int RESOURCE_isLoading( Ogre::Resource* ogreResource )
{
	return ogreResource->isLoading();
}
EXPORTFUNC int RESOURCE_getLoadingState( Ogre::Resource* ogreResource )
{
	return ogreResource->getLoadingState();
}
EXPORTFUNC int RESOURCE_isBackgroundLoaded( Ogre::Resource* ogreResource )
{
	return ogreResource->isBackgroundLoaded();
}
EXPORTFUNC void RESOURCE_setBackgroundLoaded( bool bl , Ogre::Resource* ogreResource )
{
	ogreResource->setBackgroundLoaded( bl );
}

EXPORTFUNC void RESOURCE_escalateLoading( Ogre::Resource* ogreResource )
{
	ogreResource->escalateLoading();
}
EXPORTFUNC void RESOURCE_addListener( Ogre::Resource::Listener* lis , Ogre::Resource* ogreResource )
{
	ogreResource->addListener( lis );
}
EXPORTFUNC void RESOURCE_removeListener( Ogre::Resource::Listener* lis , Ogre::Resource* ogreResource )
{
	ogreResource->removeListener( lis );
}
EXPORTFUNC const char* RESOURCE_getGroup( Ogre::Resource* ogreResource )
{
	return ogreResource->getGroup().c_str();
}

EXPORTFUNC void RESOURCE_changeGroupOwnership( const char* newGroup , Ogre::Resource* ogreResource )
{
	ogreResource->changeGroupOwnership( newGroup );
}


EXPORTFUNC Ogre::ResourceManager* RESOURCE_getCreator( Ogre::Resource* ogreResource )
{
	return ogreResource->getCreator();
}

EXPORTFUNC const char* RESOURCE_getOrigin( Ogre::Resource* ogreResource )
{
	tempString = ogreResource->getOrigin();
	return tempString.c_str();
}
EXPORTFUNC void RESOURCE__notifyOrigin( const char* origin , Ogre::Resource* ogreResource )
{
	ogreResource->_notifyOrigin( origin );
}

#endif
