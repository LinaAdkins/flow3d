EXPORTFUNC Ogre::VertexBoneAssignment* VBA_create()
{
	return new Ogre::VertexBoneAssignment();
}

EXPORTFUNC void VBA_delete( Ogre::VertexBoneAssignment* a )
{
	delete a;
}

EXPORTFUNC int VBA_getVertexIndex( Ogre::VertexBoneAssignment* a )
{
	return a->vertexIndex;
}

EXPORTFUNC void VBA_setVertexIndex( int vertexIndex , Ogre::VertexBoneAssignment* a )
{
	a->vertexIndex = vertexIndex;
}

EXPORTFUNC int VBA_getBoneIndex( Ogre::VertexBoneAssignment* a )
{
	return a->boneIndex;
}

EXPORTFUNC void VBA_setBoneIndex( int boneIndex , Ogre::VertexBoneAssignment* a )
{
	a->boneIndex = boneIndex;
}

EXPORTFUNC float VBA_getWeight( Ogre::VertexBoneAssignment* a )
{
	return a->weight;
}

EXPORTFUNC void VBA_setWeight( float weight , Ogre::VertexBoneAssignment* a )
{
	a->weight = weight;
}