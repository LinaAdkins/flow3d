#ifndef H_OGRERADIAN
#define H_OGRERADIAN

EXPORTFUNC Ogre::Radian* RAD_create( float r )
{
	return new Ogre::Radian( r );
}

EXPORTFUNC Ogre::Radian* RAD_createWithDegrees( float d )
{
	return new Ogre::Radian( Ogre::Degree( d ) );
}


EXPORTFUNC float RAD_valueAngleUnits( Ogre::Radian* ogreRadian )
{
	return ogreRadian->valueAngleUnits();
}


EXPORTFUNC float RAD_valueDegrees( Ogre::Radian* ogreRadian )
{
	return ogreRadian->valueDegrees();
}


EXPORTFUNC float RAD_valueRadians( Ogre::Radian* ogreRadian )
{
	return ogreRadian->valueRadians();
}
EXPORTFUNC void RAD_delete( Ogre::Radian* r )
{
	delete r;
}


#endif
