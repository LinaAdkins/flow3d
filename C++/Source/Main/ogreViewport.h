#ifndef H_OGREVIEWPORT
#define H_OGREVIEWPORT

EXPORTFUNC Ogre::Viewport* VP_create( Ogre::Camera* camera , Ogre::RenderTarget* target , float left, float top , float width , float height , int ZOrder )
{
	return new Ogre::Viewport( camera , target , left , top , width , height , ZOrder );

}

EXPORTFUNC void VP_destroy( Ogre::Viewport* v )
{
	delete v;
}

EXPORTFUNC void VP__updateDimensions( Ogre::Viewport* ogreViewport )
{
	ogreViewport->_updateDimensions();
}

EXPORTFUNC void VP_update( Ogre::Viewport* ogreViewport )
{
	ogreViewport->update();
}

EXPORTFUNC Ogre::RenderTarget* VP_getTarget( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getTarget();
}

EXPORTFUNC Ogre::Camera* VP_getCamera( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getCamera();
}

EXPORTFUNC void VP_setCamera( Ogre::Camera* ogreCamera , Ogre::Viewport* ogreViewport )
{
	ogreViewport->setCamera( ogreCamera );
}

EXPORTFUNC int VP_getZOrder( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getZOrder();
}

EXPORTFUNC float VP_getLeft( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getLeft();
}

EXPORTFUNC float VP_getTop( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getTop();
}

EXPORTFUNC float VP_getWidth( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getWidth();
}

EXPORTFUNC float VP_getHeight( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getHeight();
}

EXPORTFUNC int VP_getActualLeft( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getActualLeft();
}

EXPORTFUNC int VP_getActualTop( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getActualTop();
}

EXPORTFUNC int VP_getActualWidth( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getActualWidth();
}

EXPORTFUNC int VP_getActualHeight( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getActualHeight();
}

EXPORTFUNC void VP_setDimensions(float  offsetLeft , float offsetTop,  float width ,float height , Ogre::Viewport* ogreViewport )
{
	ogreViewport->setDimensions(offsetLeft , offsetTop , width, height );
}

EXPORTFUNC void VP_setBackgroundColour( Ogre::ColourValue* colour , Ogre::Viewport* ogreViewPort )
{
	ogreViewPort->setBackgroundColour( *colour );
}

EXPORTFUNC Ogre::ColourValue* VP_getBackgroundColour( Ogre::Viewport* ogreViewport )
{
	return new Ogre::ColourValue( ogreViewport->getBackgroundColour() );
}

EXPORTFUNC void VP_setClearEveryFrame( bool clear , unsigned int buffers , Ogre::Viewport* ogreViewport )
{
	ogreViewport->setClearEveryFrame( clear , buffers );
}

EXPORTFUNC int VP_getClearEveryFrame( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getClearEveryFrame();
}

EXPORTFUNC unsigned int VP_getClearBuffers( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getClearBuffers();
}

EXPORTFUNC void VP_setMaterialScheme( const char* schemeName , Ogre::Viewport* ogreViewport )
{
	ogreViewport->setMaterialScheme( schemeName );
}


EXPORTFUNC const char* VP_getMaterialScheme( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getMaterialScheme().c_str();
}

EXPORTFUNC void VP_getActualDimensions( int *left, int *top, int *width , int *height , Ogre::Viewport* ogreViewport ) 
{
	ogreViewport->getActualDimensions( *left, *top, *width, *height );
}


EXPORTFUNC int VP__isUpdated( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->_isUpdated();
}

EXPORTFUNC void VP__clearUpdatedFlag( Ogre::Viewport* ogreViewport )
{
	ogreViewport->_clearUpdatedFlag();
}

EXPORTFUNC unsigned int VP__getNumRenderedFaces( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->_getNumRenderedFaces();
}

EXPORTFUNC unsigned int VP__getNumRenderedBatches( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->_getNumRenderedBatches();
}

EXPORTFUNC void VP_setOverlaysEnabled( bool enabled , Ogre::Viewport* ogreViewport )
{
	ogreViewport->setOverlaysEnabled( enabled );
}

EXPORTFUNC int VP_getOverlaysEnabled( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getOverlaysEnabled();
}

EXPORTFUNC void VP_setSkiesEnabled( bool enabled,  Ogre::Viewport* ogreViewport )
{
	ogreViewport->setSkiesEnabled( enabled );
}

EXPORTFUNC int VP_getSkiesEnabled( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getSkiesEnabled();
}

EXPORTFUNC void VP_setShadowsEnabled( bool enabled , Ogre::Viewport* ogreViewport )
{
	ogreViewport->setShadowsEnabled( enabled );
}

EXPORTFUNC int VP_getShadowsEnabled( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getShadowsEnabled();
}

EXPORTFUNC void VP_setVisibilityMask( int mask , Ogre::Viewport* v )
{
	v->setVisibilityMask( mask );
}

EXPORTFUNC int VP_getVisibilityMask( Ogre::Viewport* v )
{
	return v->getVisibilityMask();
}


EXPORTFUNC void VP_setRenderQueueInvocationSequenceName( const char* sequenceName , Ogre::Viewport* ogreViewport )
{
	ogreViewport->setRenderQueueInvocationSequenceName( sequenceName );
}

EXPORTFUNC const char* VP_getRenderQueueInvocationSequenceName( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->getRenderQueueInvocationSequenceName().c_str();
}

EXPORTFUNC Ogre::RenderQueueInvocationSequence* VP__getRenderQueueInvocationSequence( Ogre::Viewport* ogreViewport )
{
	return ogreViewport->_getRenderQueueInvocationSequence();
}

#endif
