#ifndef H_TANIMATIONSTATEITERATOR
#define H_TANIMATIONSTATEITERATOR

/*
	Manual Construction Not Allowed
 	MapIterator (typename T::iterator start, typename T::iterator end)
 	MapIterator (T &c)
*/

EXPORTFUNC void ANIMSTATEITER_delete( Ogre::AnimationStateIterator* i )
{
	delete i;
}

EXPORTFUNC bool ANIMSTATEITER_hasMoreElements( Ogre::AnimationStateIterator* i )
{
	return i->hasMoreElements();
}

EXPORTFUNC Ogre::AnimationState* ANIMSTATEITER_getNext( Ogre::AnimationStateIterator* i )
{
	return i->getNext();
}

/* 
	Peek, etc Skipped
	T::mapped_type 	peekNextValue (void)
 		Returns the next value element in the collection, without advancing to the next.
	T::key_type 	peekNextKey (void)
 		Returns the next key element in the collection, without advancing to the next.
	MapIterator< T > & 	operator= (MapIterator< T > &rhs)
 		Required to overcome intermittent bug.
	T::mapped_type * 	peekNextValuePtr (void)
 		Returns a pointer to the next value element in the collection, without advancing to the next afterwards.
	void 	moveNext (void)
 		Moves the iterator on one element. 
*/

#endif
