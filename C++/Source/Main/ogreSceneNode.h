#ifndef H_OGRESCENENODE
#define H_OGRESCENENODE


EXPORTFUNC void SNODE_attachObject( Ogre::MovableObject* ogreMovableObject , Ogre::SceneNode* ogreSceneNode )
{
	ogreSceneNode->attachObject( ogreMovableObject );
}

EXPORTFUNC void SNODE_attachParticleSystem( Ogre::ParticleSystem* ogreParticleSystem , Ogre::SceneNode* ogreSceneNode )
{
	ogreSceneNode->attachObject( ogreParticleSystem );
}

EXPORTFUNC void SNODE_attachManualObject( Ogre::ManualObject* o , Ogre::SceneNode* n )
{
	n->attachObject( o );
}

EXPORTFUNC void SNODE_detachObject( Ogre::MovableObject* ogreMovableObject , Ogre::SceneNode* ogreSceneNode )
{
	ogreSceneNode->detachObject( ogreMovableObject );
}

EXPORTFUNC void SNODE_detachParticleSystem( Ogre::ParticleSystem* ogreParticleSystem , Ogre::SceneNode* ogreSceneNode )
{
	ogreSceneNode->detachObject( ogreParticleSystem );
}

EXPORTFUNC void SNODE_detachManualObject( Ogre::ManualObject* o , Ogre::SceneNode* n )
{
	n->detachObject( o );
}

EXPORTFUNC Ogre::SceneNode* SNODE_createChildSceneNode( const char* sceneNodeName , float x , float y , float z , Ogre::SceneNode* ogreSceneNode )
{
	return ogreSceneNode->createChildSceneNode( sceneNodeName , Ogre::Vector3( x , y , z ) );
}

EXPORTFUNC void SNODE_removeAndDestroyAllChildren( Ogre::SceneNode* ogreSceneNode )
{
	ogreSceneNode->removeAndDestroyAllChildren();
}

EXPORTFUNC void SNODE_showBoundingBox( bool Visible , Ogre::SceneNode* ogreSceneNode )
{
	ogreSceneNode->showBoundingBox( Visible );
}

EXPORTFUNC Ogre::SceneNode* SNODE_getParentSceneNode( Ogre::SceneNode* ogreSceneNode )
{
	return ogreSceneNode->getParentSceneNode();
}

EXPORTFUNC void SNODE_setVisible( bool visible , Ogre::SceneNode* ogreSceneNode , bool cascade = true)
{
	// If demo check the timer-
	#ifdef DEMO
	updateRender();
	#endif

	ogreSceneNode->setVisible( visible , cascade );
}

EXPORTFUNC void SNODE_setFixedYawAxis( bool setUsed , float x , float y , float z , Ogre::SceneNode* ogreSceneNode )
{
	ogreSceneNode->setFixedYawAxis( setUsed , Ogre::Vector3( x , y , z ) );
}

EXPORTFUNC int SNODE_getShowBoundingBox( Ogre::SceneNode* ogreSceneNode )
{
	return ogreSceneNode->getShowBoundingBox();
}

EXPORTFUNC void SNODE_lookAt( Ogre::Vector3* targetPoint , Ogre::Node::TransformSpace relativeTo, Ogre::Vector3* localDirectionVector, Ogre::SceneNode* ogreSceneNode )
{
	ogreSceneNode->lookAt( *targetPoint  , relativeTo , *localDirectionVector );
}

EXPORTFUNC void SNODE_setAutoTracking( bool enabled , Ogre::SceneNode* target , Ogre::Vector3* localDirectionVector , Ogre::Vector3* offset , Ogre::SceneNode* n )
{
	n->setAutoTracking( enabled , target , *localDirectionVector , *offset );
}

EXPORTFUNC unsigned short SNODE_numAttachedObjects( Ogre::SceneNode* ogreSceneNode )
{
	return ogreSceneNode->numAttachedObjects();
}

EXPORTFUNC Ogre::MovableObject* SNODE_getAttachedObject( unsigned short index , Ogre::SceneNode* ogreSceneNode )
{
	// If demo check the timer-
	#ifdef DEMO
	updateRender();
	#endif

	return MovableObjectChildCast( ogreSceneNode->getAttachedObject( index ) );
}

EXPORTFUNC Ogre::MovableObject* SNODE_getAttachedObjectWithName( const char* name , Ogre::SceneNode* ogreSceneNode )
{
	return MovableObjectChildCast( ogreSceneNode->getAttachedObject( name ) );
}

#endif
