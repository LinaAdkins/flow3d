#ifndef H_TTEXTMANAGER
#define H_TTEXTMANAGER

#include <OgreTextAreaOverlayElement.h>

class CTextManager : public Ogre::Singleton<CTextManager>
{

public:

	/// Here we initialise our basic overlays used for text creation.
	CTextManager()
	{
		// Get overlay manager
		_overlayMgr = Ogre::OverlayManager::getSingletonPtr();

		// Create our overlay and container
		_overlay = _overlayMgr->create("CTextManager_Overlay");
		_panel = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "CTextManager_Container"));
		
		// Set dimensions for the container to full screen.
		_panel->setDimensions( 1.0f , 1.0f );
		_panel->setPosition(0, 0);

		// Add the container to the overlay and make it visible.
		_overlay->add2D(_panel);
		_overlay->show();


		// Setup the default font
		mDefaultFont = "BlueHighway";

		// Set the default colour
		mDefaultColour = Ogre::ColourValue();

		// Default metrics mode is pixels
		mDefaultMetricsMode = Ogre::GMM_PIXELS;

		// Default character height in pixels
		mDefaultCharacterHeight = 16;

	}

	/// Here we remove the overlay and its container from the list of overlays and cleanup.
	~CTextManager()
	{
		// Destroy the overlay if it exists
		if( _overlayMgr->getByName( _overlay->getName() ) != 0 )
		{
			_overlayMgr->destroy(_overlay);
		}

		// Destroy the container if it exists
		if( _overlayMgr->getOverlayElement( _panel->getName() ) != 0 )
		{
			_overlayMgr->destroyOverlayElement( _panel );
		}
	}

	/// Add a managed text box and specify its ID, text, dimensions and colour.
	void addTextBox(
		const std::string& ID,
		const std::string& text,
		Ogre::Real x, Ogre::Real y,
		Ogre::Real width, Ogre::Real height,
		const Ogre::ColourValue& color)
	{
		// Create new text box
		Ogre::OverlayElement* textBox = _overlayMgr->createOverlayElement("TextArea", ID);

		// Set text attributes
		textBox->setDimensions(width, height);
		textBox->setMetricsMode(mDefaultMetricsMode);
		textBox->setPosition(x, y);
		textBox->setWidth(width);
		textBox->setHeight(height);
		textBox->setParameter("font_name", mDefaultFont.c_str() );
		textBox->setParameter("char_height", Ogre::StringConverter::toString( mDefaultCharacterHeight ) );
 		textBox->setColour(mDefaultColour);
		textBox->setCaption(text);

		// Add text to the container
		_panel->addChild(textBox);
	}


	/// Remove a managed text box.
	void removeTextBox(const std::string& ID)
	{
		// Remove and destroy the textbox
		_panel->removeChild(ID);
		_overlayMgr->destroyOverlayElement(ID);
	}

	/// Set the text of a text box being managed by its ID.
	void setText(const std::string& ID, const std::string& Text)
	{
		Ogre::OverlayElement* textBox = _overlayMgr->getOverlayElement(ID);
		textBox->setCaption(Text);
	}

	/// Clear all text boxes.
	void clear()
	{
		// Destroy all elements of this overlay
		Ogre::Overlay::Overlay2DElementsIterator Iter = _overlay->get2DElementsIterator();
		while (Iter.hasMoreElements())
		{
			Ogre::OverlayContainer* pContainer = Iter.getNext();

			Ogre::OverlayContainer::ChildIterator IterCont = pContainer->getChildIterator();
			while (IterCont.hasMoreElements())
			{
				Ogre::OverlayElement* pElement = IterCont.getNext();

				Ogre::OverlayManager::getSingleton().destroyOverlayElement(pElement);
			}

			_overlay->remove2D(pContainer);
			Ogre::OverlayManager::getSingleton().destroyOverlayElement((Ogre::OverlayElement*)pContainer);
		}

		// Recreate the container
		_panel = static_cast<Ogre::OverlayContainer*>(_overlayMgr->createOverlayElement("Panel", "CTextManager_Container"));
		
		// Set dimensions for the container to full screen.
		_panel->setDimensions( 1.0f , 1.0f );
		_panel->setPosition(0, 0);

		// Add the container to the overlay and make it visible.
		_overlay->add2D(_panel);
	}

	/// Set text colour using ID and colourvalue
	void setTextColour( const std::string& ID , const Ogre::ColourValue& colour )
	{
		_overlayMgr->getOverlayElement(ID)->setColour( colour );
	}

	/// Get text colour using ID
	const Ogre::ColourValue& getTextColour( const std::string& ID )
	{
		return _overlayMgr->getOverlayElement(ID)->getColour();
	}

	/// Set the default text colour used when addTextBox(...) is called.
	void setDefaultTextColour( const Ogre::ColourValue& colour )
	{
		mDefaultColour = colour;
	}

	/// Get the default text colour used when addTextBox(...) is called.
	const Ogre::ColourValue& getDefaultTextColour()
	{
		return mDefaultColour;
	}

	/// Set the font used on a specific text box by ID.
	void setTextFont( const std::string& ID , const std::string& fontName )
	{
		Ogre::OverlayElement* textBox = _overlayMgr->getOverlayElement(ID);
		textBox->setParameter("font_name", fontName.c_str() );
	}

	/// Get the name of the font used on a specific text box by ID.
	const std::string getTextFont( const std::string& ID )
	{
		Ogre::OverlayElement* textBox = _overlayMgr->getOverlayElement(ID);
		return textBox->getParameter("font_name");
	}

	/// Set the font used during addTextBox.
	void setDefaultFont( const std::string& fontName )
	{
		mDefaultFont = fontName;
	}

	/// Get the default font used during addTextBox.
	const std::string& getDefaultFont()
	{
		return mDefaultFont;
	}

	/// Set the text colour for the top of the text by text ID.
	void setTextColourTop( const std::string& ID ,  const Ogre::ColourValue& colour )
	{
		Ogre::TextAreaOverlayElement* textBox = static_cast<Ogre::TextAreaOverlayElement*>(_overlayMgr->getOverlayElement(ID));
		textBox->setColourTop( colour );
	}

	/// Get the text colour for the top of the text by text ID.
	const Ogre::ColourValue& getTextColourTop( const std::string& ID )
	{
		Ogre::TextAreaOverlayElement* textBox = static_cast<Ogre::TextAreaOverlayElement*>(_overlayMgr->getOverlayElement(ID));
		return textBox->getColourTop();
	}

	/// Set the text colour for the bottom of the text by text ID.
	void setTextColourBottom( const std::string& ID ,  const Ogre::ColourValue& colour )
	{
		Ogre::TextAreaOverlayElement* textBox = static_cast<Ogre::TextAreaOverlayElement*>(_overlayMgr->getOverlayElement(ID));
		textBox->setColourBottom( colour );
	}

	/// Get the text colour for the bottom of the text by text ID.
	const Ogre::ColourValue& getTextColourBottom( const std::string& ID )
	{
		Ogre::TextAreaOverlayElement* textBox = static_cast<Ogre::TextAreaOverlayElement*>(_overlayMgr->getOverlayElement(ID));
		return textBox->getColourBottom();
	}

	/// Set the default metrics mode for every created text box. Default is Ogre::GMM_PIXELS.
	void setDefaultMetricsMode( Ogre::GuiMetricsMode mm )
	{
		mDefaultMetricsMode = mm;
	}

	/// Get the default metrics mode for every created text box. Default is Ogre::GMM_PIXELS.
	Ogre::GuiMetricsMode getDefaultMetricsMode()
	{
		return mDefaultMetricsMode;
	}

	/// Rotate all text created with CTextManager by the given number of degrees.
	void rotateAllText( float degrees )
	{
		_overlay->rotate( Ogre::Degree( degrees ));
	}

	/// Get the angle at which all text in this CTextManager is currently rotated in degrees.
	float getTextAngle()
	{
		return _overlay->getRotate().valueDegrees();
	}

	/// Set the default character height (in pixels) for created text boxes. Default is 16.
	void setDefaultCharacterHeight( int height )
	{
		mDefaultCharacterHeight = height;
	}

	/// Set the default character height (in pixels) for created text boxes. Default is 16.
	int getDefaultCharacterHeight( )
	{
		return mDefaultCharacterHeight;
	}

	/// Set whether or not a text box is visible.
	void setTextVisible( const std::string& ID , bool visible )
	{
		Ogre::TextAreaOverlayElement* textBox = static_cast<Ogre::TextAreaOverlayElement*>(_overlayMgr->getOverlayElement(ID));
		
		if( visible )
		{
			textBox->show();
		}
		else
		{
			textBox->hide();
		}
	}

	/// Get whether or not this text box is visible.
	bool getTextVisible( const std::string& ID )
	{
		Ogre::TextAreaOverlayElement* textBox = static_cast<Ogre::TextAreaOverlayElement*>(_overlayMgr->getOverlayElement(ID));
		return textBox->isVisible();
	}

	/// Set whether or not all text in this TTextManager is visible.
	void setAllTextVisible( bool visible )
	{
		if( visible )
		{
			_overlay->show();
		}
		else
		{
			_overlay->hide();
		}
	}

	/// Get whether or not this entire TTextManager text is visible. Note that this has no bearing on individual text boxes.
	bool getAllTextVisible()
	{
		return _overlay->isVisible();
	}

	/// Get the width of this text element in the specified metrics mode.
	float getTextWidth( const std::string& ID )
	{
		Ogre::TextAreaOverlayElement* textBox = static_cast<Ogre::TextAreaOverlayElement*>(_overlayMgr->getOverlayElement(ID));
		return textBox->getWidth();
	}

	/// Get the height of this text element in the specified metrics mode.
	float getTextHeight( const std::string& ID )
	{
		Ogre::TextAreaOverlayElement* textBox = static_cast<Ogre::TextAreaOverlayElement*>(_overlayMgr->getOverlayElement(ID));
		return textBox->getHeight();
	}

	/// Set the dimensions of this text element in the specified metrics mode.
	void setTextDimensions( const std::string& ID , float width  , float height )
	{
		Ogre::TextAreaOverlayElement* textBox = static_cast<Ogre::TextAreaOverlayElement*>(_overlayMgr->getOverlayElement(ID));
		return textBox->setDimensions( width , height );
	}

	/// Get the position of a given text box from the left hand side of the screen. Position will be in the specified metrics mode.
	float getTextLeft( const std::string& ID )
	{
		Ogre::TextAreaOverlayElement* textBox = static_cast<Ogre::TextAreaOverlayElement*>(_overlayMgr->getOverlayElement(ID));
		return textBox->getLeft();
	}

	/// Get the position of a given text box from the top of the screen. Position will be in the specified metrics mode.
	float getTextTop( const std::string& ID )
	{
		Ogre::TextAreaOverlayElement* textBox = static_cast<Ogre::TextAreaOverlayElement*>(_overlayMgr->getOverlayElement(ID));
		return textBox->getTop();
	}

	/// Set the position of a given text box by specifying it's left and top attributes. Position will be in the specified metrics mode.
	void setTextPosition( const std::string& ID , float left , float top )
	{
		Ogre::TextAreaOverlayElement* textBox = static_cast<Ogre::TextAreaOverlayElement*>(_overlayMgr->getOverlayElement(ID));
		return textBox->setPosition( left , top );
	}

	/// Set the width of the spaces for characters in a specified text box.
	void setTextSpaceWidth( const std::string& ID , float width )
	{
		Ogre::TextAreaOverlayElement* textBox = static_cast<Ogre::TextAreaOverlayElement*>(_overlayMgr->getOverlayElement(ID));
		textBox->setSpaceWidth( width );
	}

	float getTextSpaceWidth( const std::string& ID )
	{
		Ogre::TextAreaOverlayElement* textBox = static_cast<Ogre::TextAreaOverlayElement*>(_overlayMgr->getOverlayElement(ID));
		return textBox->getSpaceWidth();
	}

private:

	Ogre::OverlayManager*    _overlayMgr;
	Ogre::Overlay*           _overlay;
	Ogre::OverlayContainer*  _panel;
	std::string mDefaultFont;
	Ogre::ColourValue mDefaultColour;
	Ogre::GuiMetricsMode mDefaultMetricsMode;
	int mDefaultCharacterHeight;

};

template<> CTextManager* Ogre::Singleton<CTextManager>::ms_Singleton = 0;


EXPORTFUNC void TEXTMANAGER_init()
{
	new CTextManager();
}

EXPORTFUNC void TEXTMANAGER_uninit()
{
	delete CTextManager::getSingletonPtr();
}

EXPORTFUNC void TEXTMANAGER_addTextBox( const char* ID,
										const char* text,
										float x, float y,
										float width, float height,
										Ogre::ColourValue* colour )
{
	CTextManager::getSingleton().addTextBox( ID , text , x , y , width , height , *colour );
}

EXPORTFUNC void TEXTMANAGER_removeTextBox( const char* ID )
{
	CTextManager::getSingleton().removeTextBox(ID);
}

EXPORTFUNC void TEXTMANAGER_setText( const char* ID , const char* Text )
{
	CTextManager::getSingleton().setText( ID , Text );
}

EXPORTFUNC void TEXTMANAGER_clear()
{
	CTextManager::getSingleton().clear();
}

EXPORTFUNC void TEXTMANAGER_setTextColour( const char* ID , Ogre::ColourValue* colour )
{
	CTextManager::getSingleton().setTextColour( ID , *colour );
}

EXPORTFUNC Ogre::ColourValue* TEXTMANAGER_getTextColour( const char* ID )
{
	return new Ogre::ColourValue( CTextManager::getSingleton().getTextColour( ID ) );
}

EXPORTFUNC void TEXTMANAGER_setDefaultTextColour( Ogre::ColourValue* colour )
{
	CTextManager::getSingleton().setDefaultTextColour( *colour );
}

EXPORTFUNC Ogre::ColourValue* TEXTMANAGER_getDefaultTextColour()
{
	return new Ogre::ColourValue( CTextManager::getSingleton().getDefaultTextColour() );
}

EXPORTFUNC void TEXTMANAGER_setTextFont( const char* ID , const char* fontName )
{
	CTextManager::getSingleton().setTextFont( ID , fontName );
}

EXPORTFUNC const char* TEXTMANAGER_getTextFont( const char* ID )
{
	tempString = CTextManager::getSingleton().getTextFont( ID );
	return tempString.c_str();
}

EXPORTFUNC void TEXTMANAGER_setDefaultFont( const char* fontName )
{
	CTextManager::getSingleton().setDefaultFont( fontName );
}

EXPORTFUNC const char* TEXTMANAGER_getDefaultFont()
{
	tempString = CTextManager::getSingleton().getDefaultFont();
	return tempString.c_str();

}

EXPORTFUNC void TEXTMANAGER_setTextColourTop( const char* ID , Ogre::ColourValue* col )
{
	CTextManager::getSingleton().setTextColourTop( ID , *col );
}

EXPORTFUNC Ogre::ColourValue* TEXTMANAGER_getTextColourTop( const char* ID )
{
	return new Ogre::ColourValue( CTextManager::getSingleton().getTextColourTop(ID) );
}

EXPORTFUNC void TEXTMANAGER_setTextColourBottom( const char* ID , Ogre::ColourValue* col )
{
	CTextManager::getSingleton().setTextColourBottom( ID , *col );
}

EXPORTFUNC Ogre::ColourValue* TEXTMANAGER_getTextColourBottom( const char* ID )
{
	return new Ogre::ColourValue( CTextManager::getSingleton().getTextColourBottom(ID) );
}

EXPORTFUNC void TEXTMANAGER_setDefaultMetricsMode( int mm )
{
	CTextManager::getSingleton().setDefaultMetricsMode( Ogre::GuiMetricsMode(mm) );
}

EXPORTFUNC int TEXTMANAGER_getDefaultMetricsMode()
{
	return CTextManager::getSingleton().getDefaultMetricsMode();
}

EXPORTFUNC void TEXTMANAGER_rotateAllText( float degrees )
{
	CTextManager::getSingleton().rotateAllText( degrees );
}

EXPORTFUNC float TEXTMANAGER_getTextAngle()
{
	return CTextManager::getSingleton().getTextAngle();
}

EXPORTFUNC void TEXTMANAGER_setDefaultCharacterHeight( int height )
{
	CTextManager::getSingleton().setDefaultCharacterHeight( height );
}

EXPORTFUNC int TEXTMANAGER_getDefaultCharacterHeight( )
{
	return CTextManager::getSingleton().getDefaultCharacterHeight();
}

EXPORTFUNC void TEXTMANAGER_setTextVisible( const char* ID , bool visible )
{
	CTextManager::getSingleton().setTextVisible( ID  , visible );
}

EXPORTFUNC bool TEXTMANAGER_getTextVisible( const char* ID )
{
	return CTextManager::getSingleton().getTextVisible( ID );
}

EXPORTFUNC void TEXTMANAGER_setAllTextVisible( bool visible )
{
	CTextManager::getSingleton().setAllTextVisible( visible );
}

EXPORTFUNC bool TEXTMANAGER_getAllTextVisible()
{
	return CTextManager::getSingleton().getAllTextVisible();
}

EXPORTFUNC float TEXTMANAGER_getTextWidth( const char* ID )
{
	return CTextManager::getSingleton().getTextWidth( ID );
}

EXPORTFUNC float TEXTMANAGER_getTextHeight( const char* ID )
{
	return CTextManager::getSingleton().getTextHeight( ID );
}

EXPORTFUNC void TEXTMANAGER_setTextDimensions( const char* ID , float width  , float height )
{
	CTextManager::getSingleton().setTextDimensions( ID , width , height );
}

EXPORTFUNC float TEXTMANAGER_getTextLeft( const char* ID )
{
	return CTextManager::getSingleton().getTextLeft( ID );
}

EXPORTFUNC float TEXTMANAGER_getTextTop( const char* ID )
{
	return CTextManager::getSingleton().getTextTop( ID );
}

EXPORTFUNC void TEXTMANAGER_setTextPosition( const char* ID , float left , float top )
{
	CTextManager::getSingleton().setTextPosition( ID , left , top );
}

EXPORTFUNC void TEXTMANAGER_setTextSpaceWidth( const char* ID , float width )
{
	CTextManager::getSingleton().setTextSpaceWidth( ID , width );
}

EXPORTFUNC float TEXTMANAGER_getTextSpaceWidth( const char* ID )
{
	return CTextManager::getSingleton().getTextSpaceWidth( ID );
}

#endif
