#ifndef H_OGREVECTOR2
#define H_OGREVECTOR2

EXPORTFUNC Ogre::Vector2* VECTOR2_createEmpty()
{
	return new Ogre::Vector2( 0 , 0 );
}

EXPORTFUNC Ogre::Vector2* VECTOR2_create( float fX , float fY  )
{
	return new Ogre::Vector2( fX , fY );
}

EXPORTFUNC Ogre::Vector2* VECTOR2_createWithScalar( float scalar )
{
	return new Ogre::Vector2( scalar );
}

// 	Vector2 (const Real afCoordinate[2]) - BlitzMax Collections Not Compatible with C Arrays
//	Vector2 (const int afCoordinate[2])  - BlitzMax Collections Not Compatible with C Arrays

EXPORTFUNC Ogre::Vector2* VECTOR2_createWithFloatPtr( float* r )
{
	return new Ogre::Vector2( r );
}

EXPORTFUNC Ogre::Vector2* VECTOR2_createWithVector2( Ogre::Vector2* rkVector )
{
	return new Ogre::Vector2( *rkVector );
}

//Real 	operator[] (const size_t i) const - Array operator functions supplanted by
//Real & 	operator[] (const size_t i)   - setValue and getValue.

EXPORTFUNC float VECTOR2_getValue( int i , Ogre::Vector2* v )
{
	return (*v)[i];
}

EXPORTFUNC void VECTOR2_setValue( int i , float value , Ogre::Vector2* v )
{
	(*v)[i] = value;
}

//Real * 	ptr ()						- Pointer accessor functions are unnecessary
//const Real * 	ptr () const			- they are more easilly accessible elsewhere.

EXPORTFUNC void VECTOR2_eq( Ogre::Vector2* rkVector , Ogre::Vector2* v )
{
	*v=*rkVector;
}

EXPORTFUNC void VECTOR2_eqWithScalar( float fScalar , Ogre::Vector2* v )
{
	*v=fScalar;
}

EXPORTFUNC int VECTOR2_isEqualTo( Ogre::Vector2* rkVector , Ogre::Vector2* v )
{
	return *v==*rkVector;
}

EXPORTFUNC int VECTOR2_isNotEqualTo( Ogre::Vector2* rkVector , Ogre::Vector2* v )
{
	return *v!=*rkVector;
}

EXPORTFUNC Ogre::Vector2* VECTOR2_addWithScalar( float fScalar , Ogre::Vector2* v )
{
	return new Ogre::Vector2( *v+fScalar );
}

EXPORTFUNC Ogre::Vector2* VECTOR2_add( Ogre::Vector2* rkVector , Ogre::Vector2* v )
{
	return new Ogre::Vector2( *v+*rkVector );
}

EXPORTFUNC Ogre::Vector2* VECTOR2_subWithScalar( float fScalar , Ogre::Vector2* v )
{
	return new Ogre::Vector2( *v-fScalar );
}


EXPORTFUNC Ogre::Vector2* VECTOR2_sub( Ogre::Vector2* rkVector , Ogre::Vector2* v )
{
	return new Ogre::Vector2( *v-*rkVector );
}

EXPORTFUNC Ogre::Vector2* VECTOR2_mulWithScalar( float fScalar , Ogre::Vector2* v )
{
	return new Ogre::Vector2( *v * fScalar );
}

EXPORTFUNC Ogre::Vector2* VECTOR2_mul( Ogre::Vector2* rhs , Ogre::Vector2* v )
{
	return new Ogre::Vector2((*v)*(*rhs));
}

EXPORTFUNC Ogre::Vector2* VECTOR2_divWithScalar( float fScalar , Ogre::Vector2* v )
{
	return new Ogre::Vector2( *v/fScalar );
}

EXPORTFUNC Ogre::Vector2* VECTOR2_div( Ogre::Vector2* rhs , Ogre::Vector2* v )
{
	return new Ogre::Vector2( *v/(*rhs) );
}

EXPORTFUNC Ogre::Vector2* VECTOR2_positive( Ogre::Vector2* v )
{
	return new Ogre::Vector2( +(*v) );
}

EXPORTFUNC Ogre::Vector2* VECTOR2_negative( Ogre::Vector2* v )
{
	return new Ogre::Vector2( -(*v) );
}

EXPORTFUNC void VECTOR2_addEq( Ogre::Vector2* rkVector , Ogre::Vector2* v )
{
	*v+=*rkVector;
}

EXPORTFUNC void VECTOR2_addEqWithScalar( float fScalar , Ogre::Vector2* v )
{
	*v+=fScalar;
}

EXPORTFUNC void VECTOR2_subEq( Ogre::Vector2* rkVector , Ogre::Vector2* v )
{
	*v-=*rkVector;
}

EXPORTFUNC void VECTOR2_subEqWithScalar( float fScalar , Ogre::Vector2* v )
{
	*v-=fScalar;
}

EXPORTFUNC void VECTOR2_mulEqWithScalar( float fScalar , Ogre::Vector2* v )
{
	*v *= fScalar;
}

EXPORTFUNC void VECTOR2_mulEq( Ogre::Vector2* rkVector , Ogre::Vector2* v )
{
	*v *= *rkVector;
}

EXPORTFUNC void VECTOR2_divEqWithScalar( float fScalar , Ogre::Vector2* v )
{
	*v /= fScalar;
}

EXPORTFUNC void VECTOR2_divEq( Ogre::Vector2* rkVector , Ogre::Vector2* v )
{
	*v /= *rkVector;
}

EXPORTFUNC float VECTOR2_length( Ogre::Vector2* v )
{
	return v->length();
}

EXPORTFUNC float VECTOR2_squaredLength( Ogre::Vector2* v )
{
	return v->squaredLength();
}

EXPORTFUNC float VECTOR2_dotProduct( Ogre::Vector2* vec , Ogre::Vector2* v )
{
	return v->dotProduct( *vec );
}

EXPORTFUNC float VECTOR2_normalise( Ogre::Vector2* v )
{
	return v->normalise();
}

EXPORTFUNC Ogre::Vector2* VECTOR2_midPoint( Ogre::Vector2* vec , Ogre::Vector2* v )
{
	return new Ogre::Vector2( v->midPoint( *vec ) );
}

EXPORTFUNC int VECTOR2_isLessThan( Ogre::Vector2* rhs , Ogre::Vector2* v )
{
	return (*v)<(*rhs);
}

EXPORTFUNC int VECTOR2_isGreaterThan( Ogre::Vector2* rhs , Ogre::Vector2* v )
{
	return (*v)>(*rhs);
}

EXPORTFUNC void VECTOR2_makeFloor( Ogre::Vector2* cmp , Ogre::Vector2* v )
{
	v->makeFloor( *cmp );
}

EXPORTFUNC void VECTOR2_makeCeil( Ogre::Vector2* cmp , Ogre::Vector2* v )
{
	v->makeCeil( *cmp );
}

EXPORTFUNC Ogre::Vector2* VECTOR2_perpendicular( Ogre::Vector2* v )
{
	return new Ogre::Vector2( v->perpendicular() );
}

EXPORTFUNC float VECTOR2_crossProduct( Ogre::Vector2* rkVector,  Ogre::Vector2* v )
{
	return v->crossProduct(*rkVector);
}

EXPORTFUNC Ogre::Vector2* VECTOR2_randomDeviant( float angle , Ogre::Vector2* v )
{
	return new Ogre::Vector2( v->randomDeviant( angle ) );
}

EXPORTFUNC int VECTOR2_isZeroLength( Ogre::Vector2* v )
{
	return v->isZeroLength();
}

EXPORTFUNC Ogre::Vector2* VECTOR2_normalisedCopy( Ogre::Vector2* v )
{
	return new Ogre::Vector2( v->normalisedCopy() );
}

EXPORTFUNC Ogre::Vector2* VECTOR2_reflect( Ogre::Vector2* normal , Ogre::Vector2* v )
{
	return new Ogre::Vector2( v->reflect( *normal ) );
}

EXPORTFUNC const char* VECTOR2_toStr( Ogre::Vector2* v )
{
	std::ostringstream os;
	os << *v;
	tempString.clear();
	tempString = os.str();
	return tempString.c_str();
}

EXPORTFUNC void VECTOR2_delete( Ogre::Vector2* v )
{
	delete v;
}

EXPORTFUNC float VECTOR2_getX( Ogre::Vector2* v )
{
	return v->x;
}

EXPORTFUNC float VECTOR2_getY( Ogre::Vector2* v )
{
	return v->y;
}

EXPORTFUNC void VECTOR2_setX(  float x , Ogre::Vector2* v )
{
	v->x = x;
}

EXPORTFUNC void VECTOR2_setY( float y , Ogre::Vector2* v )
{
	v->y = y;
}

EXPORTFUNC void VECTOR2_setAll( float x , float y , Ogre::Vector2* v )
{
	v->x = x;
	v->y = y;
}



// Static Attributes-
EXPORTFUNC Ogre::Vector2* VECTOR2_ZERO()
{
	return (Ogre::Vector2*)&Ogre::Vector2::ZERO;
}

EXPORTFUNC Ogre::Vector2* VECTOR2_UNIT_X()
{
	return (Ogre::Vector2*)&Ogre::Vector2::UNIT_X;
}

EXPORTFUNC Ogre::Vector2* VECTOR2_UNIT_Y()
{
	return (Ogre::Vector2*)&Ogre::Vector2::UNIT_Y;
}

EXPORTFUNC Ogre::Vector2* VECTOR2_NEGATIVE_UNIT_X()
{
	return (Ogre::Vector2*)&Ogre::Vector2::NEGATIVE_UNIT_X;
}

EXPORTFUNC Ogre::Vector2* VECTOR2_NEGATIVE_UNIT_Y()
{
	return (Ogre::Vector2*)&Ogre::Vector2::NEGATIVE_UNIT_Y;
}

EXPORTFUNC Ogre::Vector2* VECTOR2_UNIT_SCALE()
{
	return (Ogre::Vector2*)&Ogre::Vector2::UNIT_SCALE;
}




#endif
