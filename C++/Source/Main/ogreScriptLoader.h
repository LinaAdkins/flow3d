#ifndef H_OGRESCRIPTLOADER
#define H_OGRESCRIPTLOADER

EXPORTFUNC void SL_destroy( Ogre::ScriptLoader* sl )
{
	delete sl;
}

EXPORTFUNC const Ogre::StringVector* SL_getScriptPatterns( Ogre::ScriptLoader* sl )
{
	Ogre::StringVector* sv = new Ogre::StringVector( sl->getScriptPatterns() );
	return sv;
}

EXPORTFUNC void SL_parseScript( Ogre::DataStream* stream , const char* groupName , Ogre::ScriptLoader* sl )
{
	sl->parseScript( Ogre::DataStreamPtr( stream ) , groupName );
}

EXPORTFUNC float SL_getLoadingOrder( Ogre::ScriptLoader* sl )
{
	return sl->getLoadingOrder();
}

#endif
