#ifndef H_OGREANIMATIONSTATE
#define H_OGREANIMATIONSTATE

EXPORTFUNC void AS_delete( Ogre::AnimationState* ogreAnimationState )
{
	delete ogreAnimationState;
}

EXPORTFUNC const char* AS_getAnimationName( Ogre::AnimationState* ogreAnimationState )
{
	return ogreAnimationState->getAnimationName().c_str();
}
EXPORTFUNC void AS_setEnabled( bool enabled , Ogre::AnimationState* ogreAnimationState )
{
	ogreAnimationState->setEnabled( enabled );
}
EXPORTFUNC void AS_addTime( float offset , Ogre::AnimationState* ogreAnimationState )
{
	ogreAnimationState->addTime( offset );
}
EXPORTFUNC void AS_setLoop( bool loop , Ogre::AnimationState* ogreAnimationState )
{
	ogreAnimationState->setLoop( loop );
}

EXPORTFUNC bool AS_getLoop( Ogre::AnimationState* s )
{
	return s->getLoop();
}

EXPORTFUNC void AS_setTimePosition( float timePos , Ogre::AnimationState* ogreAnimationState )
{
	ogreAnimationState->setTimePosition( timePos );
}

EXPORTFUNC float AS_getTimePosition( Ogre::AnimationState* as )
{
	return as->getTimePosition();
}

EXPORTFUNC float AS_getLength( Ogre::AnimationState* as )
{
	return as->getLength();
}

EXPORTFUNC float AS_getWeight( Ogre::AnimationState* s )
{
	return s->getWeight();
}

EXPORTFUNC void AS_setWeight( float weight , Ogre::AnimationState* s )
{
	s->setWeight( weight );
}

EXPORTFUNC void AS_createBlendMask( int blendMaskSizeHint , float initialWeight , Ogre::AnimationState* s )
{
	s->createBlendMask( blendMaskSizeHint , initialWeight );
}

EXPORTFUNC void AS_destroyBlendMask( Ogre::AnimationState* s )
{
	s->destroyBlendMask();
}

EXPORTFUNC bool AS_hasBlendMask( Ogre::AnimationState* s )
{
	return s->hasBlendMask();
}

EXPORTFUNC void AS_setBlendMaskEntry( int boneHandle , float weight , Ogre::AnimationState* s )
{
	s->setBlendMaskEntry( boneHandle , weight );
}

EXPORTFUNC float AS_getBlendMaskEntry( int boneHandle , Ogre::AnimationState* s )
{
	return s->getBlendMaskEntry( boneHandle );
}






#endif
