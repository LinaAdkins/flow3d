#ifndef H_TOVERLAY
#define H_TOVERLAY

EXPORTFUNC Ogre::OverlayContainer* OVERLAY_getChild( const char* name , Ogre::Overlay* o )
{
	return o->getChild( name );
}

EXPORTFUNC const char* OVERLAY_getName( Ogre::Overlay* o )
{
	tempString = o->getName();
	return tempString.c_str();
}

EXPORTFUNC void OVERLAY_setZOrder( int zorder , Ogre::Overlay* o )
{
	o->setZOrder( zorder );
}

EXPORTFUNC int OVERLAY_getZOrder( Ogre::Overlay* o )
{
	return o->getZOrder();
}

EXPORTFUNC bool OVERLAY_isVisible( Ogre::Overlay* o )
{
	return o->isVisible();
}

EXPORTFUNC bool OVERLAY_isInitialised( Ogre::Overlay* o )
{
	return o->isInitialised();
}

EXPORTFUNC void OVERLAY_show( Ogre::Overlay* o )
{
	o->show();
}

EXPORTFUNC void OVERLAY_hide( Ogre::Overlay* o )
{
	o->hide();
}

EXPORTFUNC void OVERLAY_add2D( Ogre::OverlayContainer* cont , Ogre::Overlay* o )
{
	o->add2D( cont );
}

EXPORTFUNC void OVERLAY_remove2D( Ogre::OverlayContainer* cont , Ogre::Overlay* o )
{
	o->remove2D( cont );
}

EXPORTFUNC void OVERLAY_add3D( Ogre::SceneNode* node, Ogre::Overlay* o )
{
	o->add3D( node );
}

EXPORTFUNC void OVERLAY_remove3D( Ogre::SceneNode* node , Ogre::Overlay* o )
{
	o->remove3D( node );
}

EXPORTFUNC void OVERLAY_clear( Ogre::Overlay* o )
{
	o->clear();
}

EXPORTFUNC void OVERLAY_setScroll( float x , float y , Ogre::Overlay* o )
{
	o->setScroll( x , y );
}

EXPORTFUNC float OVERLAY_getScrollX( Ogre::Overlay* o )
{
	return o->getScrollX();
}

EXPORTFUNC float OVERLAY_getScrollY( Ogre::Overlay* o )
{
	return o->getScrollY();
}

EXPORTFUNC void OVERLAY_scroll( float xoff , float yoff , Ogre::Overlay* o )
{
	o->scroll( xoff , yoff );
}

EXPORTFUNC void OVERLAY_setRotate( Ogre::Radian* angle , Ogre::Overlay* o )
{
	o->setRotate( *angle );
}

EXPORTFUNC Ogre::Radian* OVERLAY_getRotate( Ogre::Overlay* o )
{
	return new Ogre::Radian( o->getRotate() );
}

EXPORTFUNC void OVERLAY_rotate( Ogre::Radian* angle , Ogre::Overlay* o )
{
	o->rotate( *angle );
}

EXPORTFUNC void OVERLAY_setScale( float x , float y , Ogre::Overlay* o )
{
	o->setScale( x , y );
}

EXPORTFUNC float OVERLAY_getScaleX( Ogre::Overlay* o )
{
	return o->getScaleX();
}

EXPORTFUNC float OVERLAY_getScaleY( Ogre::Overlay* o )
{
	return o->getScaleY();
}

EXPORTFUNC void OVERLAY__getWorldTransforms( Ogre::Matrix4* xform , Ogre::Overlay* o )
{
	o->_getWorldTransforms( xform );
}

EXPORTFUNC void OVERLAY__findVisibleObjects( Ogre::Camera* cam , Ogre::RenderQueue* queue , Ogre::Overlay* o )
{
	o->_findVisibleObjects( cam , queue );
}

EXPORTFUNC Ogre::OverlayElement* OVERLAY_findElementAt( float x , float y , Ogre::Overlay* o )
{
	return o->findElementAt( x , y );
}

EXPORTFUNC Ogre::Overlay::Overlay2DElementsIterator* OVERLAY_get2DElementsIterator( Ogre::Overlay* o )
{
	return new Ogre::Overlay::Overlay2DElementsIterator( o->get2DElementsIterator() );
}

EXPORTFUNC const char* OVERLAY_getOrigin( Ogre::Overlay* o )
{
	tempString = o->getOrigin();
	return tempString.c_str();
}

EXPORTFUNC void OVERLAY__notifyOrigin( const char* origin , Ogre::Overlay* o )
{
	o->_notifyOrigin( origin );
}









#endif
