#ifndef H__OGRESPHERE
#define H__OGRESPHERE

EXPORTFUNC Ogre::Sphere* SPHERE_create()
{
	return new Ogre::Sphere();
}

EXPORTFUNC Ogre::Sphere* SPHERE_createWithAttributes( Ogre::Vector3* center , float radius )
{
	Ogre::Sphere* ogreSphere = new Ogre::Sphere( *center , radius );
	return ogreSphere;
}

EXPORTFUNC void SPHERE_delete( Ogre::Sphere* ogreSphere )
{
	delete ogreSphere;
}

EXPORTFUNC float SPHERE_getRadius( Ogre::Sphere* ogreSphere )
{
	return ogreSphere->getRadius();
}



#endif
