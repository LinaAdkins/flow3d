#ifndef H_OGRESUBMESH
#define H_OGRESUBMESH

EXPORTFUNC void SUBMESH_setMaterialName( const char* matName , Ogre::SubMesh* s )
{
	s->setMaterialName( matName );
}

EXPORTFUNC const char* SUBMESH_getMaterialName( Ogre::SubMesh* s )
{
	tempString = s->getMaterialName();
	return tempString.c_str();
}

EXPORTFUNC void SUBMESH_addBoneAssignment( Ogre::VertexBoneAssignment* vertBoneAssign , Ogre::SubMesh* s )
{
	s->addBoneAssignment( *vertBoneAssign );
}

EXPORTFUNC void SUBMESH_clearBoneAssignments( Ogre::SubMesh* s )
{
	s->clearBoneAssignments();
}


// Public Attribute Accessors
EXPORTFUNC void SUBMESH_setUseSharedVertices( bool useSharedVertices , Ogre::SubMesh* s )
{
	s->useSharedVertices = useSharedVertices;
}

EXPORTFUNC bool SUBMESH_getUseSharedVertices( Ogre::SubMesh* s )
{
	return s->useSharedVertices;
}

EXPORTFUNC void SUBMESH_setOperationType( int operationType , Ogre::SubMesh* s )
{
	s->operationType = (Ogre::RenderOperation::OperationType)operationType;
}

EXPORTFUNC int SUBMESH_getOperationType( Ogre::SubMesh* s )
{
	return s->operationType;
}

EXPORTFUNC void SUBMESH_setVertexData( Ogre::VertexData* v , Ogre::SubMesh* s )
{
	s->vertexData = v;
}

EXPORTFUNC Ogre::VertexData* SUBMESH_getVertexData( Ogre::SubMesh* s )
{
	return s->vertexData;
}

EXPORTFUNC void SUBMESH_setIndexData( Ogre::IndexData* indexData , Ogre::SubMesh* s )
{
	s->indexData = indexData;
}

EXPORTFUNC Ogre::IndexData* SUBMESH_getIndexData( Ogre::SubMesh* s )
{
	return s->indexData;
}

EXPORTFUNC void SUBMESH_setBlendIndexToBoneIndexMap( Ogre::SubMesh::IndexMap* map , Ogre::SubMesh* s )
{
	s->blendIndexToBoneIndexMap = *map;
}

EXPORTFUNC Ogre::SubMesh::IndexMap* SUBMESH_getBlendIndexToBoneIndexMap( Ogre::SubMesh* s )
{
	return &s->blendIndexToBoneIndexMap;
}

EXPORTFUNC void SUBMESH_setLodFaceList( Ogre::ProgressiveMesh::LODFaceList* mLodFaceList , Ogre::SubMesh* s )
{
	s->mLodFaceList = *mLodFaceList;
}

EXPORTFUNC Ogre::ProgressiveMesh::LODFaceList* SUBMESH_getLodFaceList( Ogre::SubMesh* s )
{
	return &s->mLodFaceList;
}

EXPORTFUNC void SUBMESH_setExtremityPoints( std::vector< Ogre::Vector3 >* extremityPoints , Ogre::SubMesh* s )
{
	s->extremityPoints = *extremityPoints;
}

EXPORTFUNC std::vector<Ogre::Vector3>* SUBMESH_getExtremityPoints( Ogre::SubMesh* s )
{
	return &s->extremityPoints;
}

EXPORTFUNC void SUBMESH_setParent( Ogre::Mesh* parent , Ogre::SubMesh* s )
{
	s->parent = parent;
}

EXPORTFUNC Ogre::Mesh* SUBMESH_getParent( Ogre::SubMesh* s )
{
	return s->parent;
}

#endif
