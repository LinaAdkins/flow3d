#ifndef H_OGREVERTEXDECLARATION
#define H_OGREVERTEXDECLARATION

EXPORTFUNC int VERTEXDECLARATION_getElementCount( Ogre::VertexDeclaration* d )
{
	return d->getElementCount();
}

EXPORTFUNC Ogre::VertexElement* VERTEXDECLARATION_addElement( int source , int offset , int theType , int semantic , int index , Ogre::VertexDeclaration* d )
{
	return new Ogre::VertexElement( d->addElement( source , offset , Ogre::VertexElementType(theType) , Ogre::VertexElementSemantic(semantic) , index ));
}
#endif
