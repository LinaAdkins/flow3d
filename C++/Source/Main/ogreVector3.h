#ifndef H_OGREVECTOR3
#define H_OGREVECTOR3

EXPORTFUNC Ogre::Vector3* VECTOR3_createEmpty()
{
	return  new Ogre::Vector3( 0, 0, 0 );
}

EXPORTFUNC Ogre::Vector3* VECTOR3_create( float x , float y , float z )
{
	return new Ogre::Vector3( x , y , z );
}

//Vector3 (const Real afCoordinate[3]) - Blitz collections not compatible with c arrays
//Vector3 (const int afCoordinate[3]) - Blitz collections not compatible with c arrays
EXPORTFUNC Ogre::Vector3* VECTOR3_createWithFloatPtr( float* r )
{
	return new Ogre::Vector3( r );
}

EXPORTFUNC Ogre::Vector3* VECTOR3_createWithScalar( float scalar )
{
	return new Ogre::Vector3( scalar );
}

EXPORTFUNC Ogre::Vector3* VECTOR3_createWithVector3( Ogre::Vector3* rkVector )
{
	return new Ogre::Vector3( *rkVector );
}

//Real 	operator[] (const size_t i) const ********  Array operator functions supplanted by
//Real & 	operator[] (const size_t i)   ********  setValue and getValue.
EXPORTFUNC float VECTOR3_getValue( int i , Ogre::Vector3* v )
{
	return (*v)[i];
}

EXPORTFUNC void VECTOR3_setValue( int i , float value , Ogre::Vector3* v )
{
	(*v)[i] = value;
}

//Real * 	ptr ()  ************** Pointer accessor functions are unnecessary
//const Real * 	ptr () const ***** because they are more easilly accessible elsewhere.
EXPORTFUNC void VECTOR3_eq( Ogre::Vector3* rkVector , Ogre::Vector3* v )
{
	*v=*rkVector;
}

EXPORTFUNC void VECTOR3_eqWithScalar( float fScalar , Ogre::Vector3* v )
{
	*v=fScalar;
}

EXPORTFUNC int VECTOR3_isEqualTo( Ogre::Vector3* rkVector , Ogre::Vector3* v )
{
	return *v==*rkVector;
}

EXPORTFUNC int VECTOR3_isNotEqualTo( Ogre::Vector3* rkVector , Ogre::Vector3* v )
{
	return *v!=*rkVector;
}

EXPORTFUNC Ogre::Vector3* VECTOR3_addWithScalar( float fScalar , Ogre::Vector3* v )
{
	return new Ogre::Vector3( *v+fScalar );
}

EXPORTFUNC Ogre::Vector3* VECTOR3_add( Ogre::Vector3* rkVector , Ogre::Vector3* v )
{
	return new Ogre::Vector3( *v+*rkVector );
}

EXPORTFUNC Ogre::Vector3* VECTOR3_subWithScalar( float fScalar , Ogre::Vector3* v )
{
	return new Ogre::Vector3( *v-fScalar );
}

EXPORTFUNC Ogre::Vector3* VECTOR3_sub( Ogre::Vector3* rkVector , Ogre::Vector3* v )
{
	return new Ogre::Vector3( *v-*rkVector );
}

EXPORTFUNC Ogre::Vector3* VECTOR3_mulWithScalar( float fScalar , Ogre::Vector3* v )
{
	return new Ogre::Vector3( *v * fScalar );
}


EXPORTFUNC Ogre::Vector3* VECTOR3_mul( Ogre::Vector3* rhs , Ogre::Vector3* v )
{
	return new Ogre::Vector3((*v)*(*rhs));
}

EXPORTFUNC Ogre::Vector3* VECTOR3_divWithScalar( float fScalar , Ogre::Vector3* v )
{
	return new Ogre::Vector3( *v/fScalar );
}

EXPORTFUNC Ogre::Vector3* VECTOR3_div( Ogre::Vector3* rhs , Ogre::Vector3* v )
{
	return new Ogre::Vector3( *v/(*rhs) );
}

EXPORTFUNC Ogre::Vector3* VECTOR3_positive( Ogre::Vector3* v )
{
	return new Ogre::Vector3( +(*v) );
}

EXPORTFUNC Ogre::Vector3* VECTOR3_negative( Ogre::Vector3* v )
{
	return new Ogre::Vector3( -(*v) );
}


EXPORTFUNC void VECTOR3_addEq( Ogre::Vector3* rkVector , Ogre::Vector3* v )
{
	*v+=*rkVector;
}

EXPORTFUNC void VECTOR3_addEqWithScalar( float fScalar , Ogre::Vector3* v )
{
	*v+=fScalar;
}

EXPORTFUNC void VECTOR3_subEq( Ogre::Vector3* rkVector , Ogre::Vector3* v )
{
	*v-=*rkVector;
}

EXPORTFUNC void VECTOR3_subEqWithScalar( float fScalar , Ogre::Vector3* v )
{
	*v-=fScalar;
}

EXPORTFUNC void VECTOR3_mulEqWithScalar( float fScalar , Ogre::Vector3* v )
{
	*v *= fScalar;
}

EXPORTFUNC void VECTOR3_mulEq( Ogre::Vector3* rkVector , Ogre::Vector3* v )
{
	*v *= *rkVector;
}

EXPORTFUNC void VECTOR3_divEqWithScalar( float fScalar , Ogre::Vector3* v )
{
	*v /= fScalar;
}

EXPORTFUNC void VECTOR3_divEq( Ogre::Vector3* rkVector , Ogre::Vector3* v )
{
	*v /= *rkVector;
}

EXPORTFUNC float VECTOR3_length( Ogre::Vector3* v )
{
	return v->length();
}

EXPORTFUNC float VECTOR3_squaredLength( Ogre::Vector3* v )
{
	return v->squaredLength();
}

EXPORTFUNC float VECTOR3_distance( Ogre::Vector3* rhs , Ogre::Vector3* v )
{
	return v->distance( *rhs );
}

EXPORTFUNC float VECTOR3_squaredDistance( Ogre::Vector3* rhs , Ogre::Vector3* v )
{
	return v->squaredDistance( *rhs );
}

EXPORTFUNC float VECTOR3_dotProduct( Ogre::Vector3* vec , Ogre::Vector3* v )
{
	return v->dotProduct( *vec );
}

EXPORTFUNC float VECTOR3_absDotProduct( Ogre::Vector3* vec , Ogre::Vector3* v )
{
	return v->absDotProduct( *vec );
}

EXPORTFUNC float VECTOR3_normalise( Ogre::Vector3* v )
{
	return v->normalise();
}

EXPORTFUNC Ogre::Vector3* VECTOR3_crossProduct( Ogre::Vector3* rkVector,  Ogre::Vector3* v )
{
	return new Ogre::Vector3( v->crossProduct( *rkVector ) );
}

EXPORTFUNC Ogre::Vector3* VECTOR3_midPoint( Ogre::Vector3* vec , Ogre::Vector3* v )
{
	return new Ogre::Vector3( v->midPoint( *vec ) );
}

EXPORTFUNC int VECTOR3_isLessThan( Ogre::Vector3* rhs , Ogre::Vector3* v )
{
	return (*v)<(*rhs);
}

EXPORTFUNC int VECTOR3_isGreaterThan( Ogre::Vector3* rhs , Ogre::Vector3* v )
{
	return (*v)>(*rhs);
}

EXPORTFUNC void VECTOR3_makeFloor( Ogre::Vector3* cmp , Ogre::Vector3* v )
{
	v->makeFloor( *cmp );
}

EXPORTFUNC void VECTOR3_makeCeil( Ogre::Vector3* cmp , Ogre::Vector3* v )
{
	v->makeCeil( *cmp );
}

EXPORTFUNC Ogre::Vector3* VECTOR3_perpendicular( Ogre::Vector3* v )
{
	return new Ogre::Vector3( v->perpendicular() );
}

EXPORTFUNC Ogre::Vector3* VECTOR3_randomDeviant( Ogre::Radian* angle , Ogre::Vector3* up , Ogre::Vector3* v )
{
	return new Ogre::Vector3( v->randomDeviant(*angle,*up) );
}

EXPORTFUNC Ogre::Quaternion* VECTOR3_getRotationTo( Ogre::Vector3* dest, Ogre::Vector3* fallbackAxis , Ogre::Vector3* v )
{
	return new Ogre::Quaternion( v->getRotationTo( *dest,*fallbackAxis ) );
}

EXPORTFUNC int VECTOR3_isZeroLength( Ogre::Vector3* v )
{
	return v->isZeroLength();
}

EXPORTFUNC Ogre::Vector3* VECTOR3_normalisedCopy( Ogre::Vector3* v )
{
	return new Ogre::Vector3( v->normalisedCopy() );
}

EXPORTFUNC Ogre::Vector3* VECTOR3_reflect( Ogre::Vector3* normal , Ogre::Vector3* v )
{
	return new Ogre::Vector3( v->reflect( *normal ) );
}

EXPORTFUNC int VECTOR3_positionEquals( Ogre::Vector3* rhs , float tolerance , Ogre::Vector3* v )
{
	return v->positionEquals( *rhs , tolerance );
}

EXPORTFUNC int VECTOR3_positionCloses( Ogre::Vector3* rhs , float tolerance , Ogre::Vector3* v )
{
	return v->positionCloses( *rhs , tolerance );
}

EXPORTFUNC int VECTOR3_directionEquals( Ogre::Vector3* rhs , Ogre::Radian* tolerance , Ogre::Vector3* v )
{
	return v->directionEquals( *rhs , *tolerance );
}

EXPORTFUNC const char* VECTOR3_toStr( Ogre::Vector3* v )
{
	std::ostringstream os;
	os << *v;
	tempString.clear();
	tempString = os.str();
	return tempString.c_str();
}

EXPORTFUNC void VECTOR3_delete( Ogre::Vector3* v )
{
	delete v;
}

EXPORTFUNC float VECTOR3_getX( Ogre::Vector3* v )
{
	return v->x;
}

EXPORTFUNC float VECTOR3_getY( Ogre::Vector3* v )
{
	return v->y;
}

EXPORTFUNC float VECTOR3_getZ( Ogre::Vector3* v )
{
	return v->z;
}

EXPORTFUNC void VECTOR3_setX( float x , Ogre::Vector3* v )
{
	v->x = x;
}

EXPORTFUNC void VECTOR3_setY( float y , Ogre::Vector3* v )
{
	v->y = y;
}

EXPORTFUNC void VECTOR3_setZ( float z , Ogre::Vector3* v )
{
	v->z = z;
}

EXPORTFUNC void VECTOR3_setAll( float x , float y , float z , Ogre::Vector3* v )
{
	v->x = x;
	v->y = y;
	v->z = z;
}


// Static Attributes-
EXPORTFUNC Ogre::Vector3* VECTOR3_ZERO()
{
	return (Ogre::Vector3*)&Ogre::Vector3::ZERO;
}

EXPORTFUNC Ogre::Vector3* VECTOR3_UNIT_X()
{
	return (Ogre::Vector3*)&Ogre::Vector3::UNIT_X;
}

EXPORTFUNC Ogre::Vector3* VECTOR3_UNIT_Y()
{
	return (Ogre::Vector3*)&Ogre::Vector3::UNIT_Y;
}

EXPORTFUNC Ogre::Vector3* VECTOR3_UNIT_Z()
{
	return (Ogre::Vector3*)&Ogre::Vector3::UNIT_Z;
}

EXPORTFUNC Ogre::Vector3* VECTOR3_NEGATIVE_UNIT_X()
{
	return (Ogre::Vector3*)&Ogre::Vector3::NEGATIVE_UNIT_X;
}

EXPORTFUNC Ogre::Vector3* VECTOR3_NEGATIVE_UNIT_Y()
{
	return (Ogre::Vector3*)&Ogre::Vector3::NEGATIVE_UNIT_Y;
}

EXPORTFUNC Ogre::Vector3* VECTOR3_NEGATIVE_UNIT_Z()
{
	return (Ogre::Vector3*)&Ogre::Vector3::NEGATIVE_UNIT_Z;
}

EXPORTFUNC Ogre::Vector3* VECTOR3_UNIT_SCALE()
{
	return (Ogre::Vector3*)&Ogre::Vector3::UNIT_SCALE;
}






#endif
