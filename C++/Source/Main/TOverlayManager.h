#ifndef H_TOVERLAYMANAGER
#define H_TOVERLAYMANAGER

EXPORTFUNC Ogre::Overlay* OM_create( const char* name )
{
	return Ogre::OverlayManager::getSingleton().create( name );
}

EXPORTFUNC Ogre::Overlay* OM_getByName( const char* name )
{
	return Ogre::OverlayManager::getSingleton().getByName( name );
}

EXPORTFUNC void OM_destroyWithName( const char* name )
{
	Ogre::OverlayManager::getSingleton().destroy( name );
}

EXPORTFUNC void OM_destroyWithInstance( Ogre::Overlay* overlay )
{
	Ogre::OverlayManager::getSingleton().destroy( overlay );
}

EXPORTFUNC void OM_destroyAll()
{
	Ogre::OverlayManager::getSingleton().destroyAll();
}

EXPORTFUNC Ogre::OverlayManager::OverlayMapIterator* OM_getOverlayIterator()
{
	return new Ogre::OverlayManager::OverlayMapIterator( Ogre::OverlayManager::getSingleton().getOverlayIterator() );
}

EXPORTFUNC void OM__queueOverlaysForRendering( Ogre::Camera* cam , Ogre::RenderQueue* pQueue , Ogre::Viewport* vp )
{
	Ogre::OverlayManager::getSingleton()._queueOverlaysForRendering( cam , pQueue , vp );
}

EXPORTFUNC bool OM_hasViewportChanged()
{
	return Ogre::OverlayManager::getSingleton().hasViewportChanged();
}

EXPORTFUNC int OM_getViewportHeight()
{
	return Ogre::OverlayManager::getSingleton().getViewportHeight();
}

EXPORTFUNC int OM_getVeiwportWidth()
{
	return Ogre::OverlayManager::getSingleton().getViewportWidth();
}

EXPORTFUNC float OM_getViewportAspectRatio()
{
	return Ogre::OverlayManager::getSingleton().getViewportAspectRatio();
}

EXPORTFUNC Ogre::OverlayElement* OM_createOverlayElement( const char* typeName , const char* instanceName , bool isTemplate )
{
	return Ogre::OverlayManager::getSingleton().createOverlayElement( typeName , instanceName , isTemplate );
}

EXPORTFUNC Ogre::OverlayElement* OM_getOverlayElement( const char* name , bool isTemplate )
{
	return Ogre::OverlayManager::getSingleton().getOverlayElement( name , isTemplate );
}

EXPORTFUNC void OM_destroyOverlayElementWithName( const char* instanceName , bool isTemplate )
{
	Ogre::OverlayManager::getSingleton().destroyOverlayElement( instanceName , isTemplate );
}

EXPORTFUNC void OM_destroyOverlayElement( Ogre::OverlayElement* pInstance,  bool isTemplate )
{
	Ogre::OverlayManager::getSingleton().destroyOverlayElement( pInstance , isTemplate );
}

EXPORTFUNC void OM_destroyAllOverlayElements( bool isTemplate )
{
	Ogre::OverlayManager::getSingleton().destroyAllOverlayElements( isTemplate );
}

EXPORTFUNC void OM_addOverlayElementFactory( Ogre::OverlayElementFactory* elemFactory )
{
	Ogre::OverlayManager::getSingleton().addOverlayElementFactory( elemFactory );
}

EXPORTFUNC Ogre::OverlayElement* OM_createOverlayElementFromTemplate( const char* templateName , const char* typeName , const char* instanceName , bool isTemplate )
{
	return Ogre::OverlayManager::getSingleton().createOverlayElementFromTemplate( templateName , typeName , instanceName , isTemplate );
}

EXPORTFUNC Ogre::OverlayElement* OM_cloneOverlayElementFromTemplate( const char* templateName , const char* instanceName )
{
	return Ogre::OverlayManager::getSingleton().cloneOverlayElementFromTemplate( templateName , instanceName );
}

EXPORTFUNC Ogre::OverlayElement* OM_createOverlayElementFromFactory( const char* typeName , const char* instanceName )
{
	return Ogre::OverlayManager::getSingleton().createOverlayElementFromFactory( typeName , instanceName );
}

EXPORTFUNC Ogre::OverlayManager::TemplateIterator* OM_getTemplateIterator()
{
	return new Ogre::OverlayManager::TemplateIterator( Ogre::OverlayManager::getSingleton().getTemplateIterator() );
}

EXPORTFUNC bool OM_isTemplate( const char* strName  )
{
	return Ogre::OverlayManager::getSingleton().isTemplate( strName );
}

// ScriptLoader Methods
EXPORTFUNC Ogre::StringVector* OM_getScriptPatterns()
{
	return new Ogre::StringVector( Ogre::OverlayManager::getSingleton().getScriptPatterns() );
}

EXPORTFUNC void OM_parseScript( Ogre::DataStream* stream , const char* groupName )
{
	Ogre::OverlayManager::getSingleton().parseScript( Ogre::DataStreamPtr( stream ) , groupName );
}

EXPORTFUNC float OM_getLoadingOrder()
{
	return Ogre::OverlayManager::getSingleton().getLoadingOrder();
}






#endif
