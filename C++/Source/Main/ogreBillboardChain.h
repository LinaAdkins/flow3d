#ifndef H_OGREBILLBOARDCHAIN
#define H_OGREBILLBOARDCHAIN

//- Constructor unneeded, use factory - BillboardChain (const String &name, size_t maxElements=20, size_t numberOfChains=1, ...
//destructor for managed object unneeded-

EXPORTFUNC void BC_setMaxChainElements( int maxElements , Ogre::BillboardChain* bc )
{
	bc->setMaxChainElements( maxElements );
}

EXPORTFUNC int BC_getMaxChainElements( Ogre::BillboardChain* bc )
{
	return bc->getMaxChainElements();
}

EXPORTFUNC void BC_setNumberOfChains( int numChains , Ogre::BillboardChain* bc )
{
	bc->setNumberOfChains( numChains );
}

EXPORTFUNC int BC_getNumberOfChains( Ogre::BillboardChain* bc )
{
	return bc->getNumberOfChains();
}




EXPORTFUNC void BC_setUseTextureCoords( bool use , Ogre::BillboardChain* bc )
{
	bc->setUseTextureCoords( use );
}

EXPORTFUNC int BC_getUseTextureCoords( Ogre::BillboardChain* bc )
{
	return bc->getUseTextureCoords();
}
EXPORTFUNC void BC_setTextureCoordDirection( int dir , Ogre::BillboardChain* bc )
{
	bc->setTextureCoordDirection( ( Ogre::BillboardChain::TexCoordDirection)dir );
}

EXPORTFUNC int BC_getTextureCoordDirection( Ogre::BillboardChain* bc )
{
	return bc->getTextureCoordDirection();
}
EXPORTFUNC void BC_setOtherTextureCoordRange( float start , float end , Ogre::BillboardChain* bc )
{
	bc->setOtherTextureCoordRange( start , end );
}

EXPORTFUNC const float* BC_getOtherTextureCoordRange( Ogre::BillboardChain* bc )
{
	return bc->getOtherTextureCoordRange();
}


EXPORTFUNC void BC_setUseVertexColours( bool use , Ogre::BillboardChain* bc )
{
	bc->setUseVertexColours( use );
}

EXPORTFUNC int BC_getUseVertexColours( Ogre::BillboardChain* bc )
{
	return bc->getUseVertexColours();
}

EXPORTFUNC void BC_setDynamic( bool dyn , Ogre::BillboardChain* bc )
{
	bc->setDynamic( dyn );
}

EXPORTFUNC int BC_getDynamic( Ogre::BillboardChain* bc )
{
	return bc->getDynamic();
}

EXPORTFUNC void BC_addChainElement( int chainIndex , Ogre::BillboardChain::Element* billboardChainElement , Ogre::BillboardChain* bc )
{
	bc->addChainElement( chainIndex , *billboardChainElement );
}

EXPORTFUNC void BC_removeChainElement( int chainIndex , Ogre::BillboardChain* bc )
{
	bc->removeChainElement( chainIndex );
}

EXPORTFUNC void BC_updateChainElement( int chainIndex , int elementIndex , Ogre::BillboardChain::Element* billboardChainElement , Ogre::BillboardChain* bc )
{
	bc->updateChainElement( chainIndex , elementIndex , *billboardChainElement );
}

EXPORTFUNC const Ogre::BillboardChain::Element* BC_getChainElement( int chainIndex , int elementIndex , Ogre::BillboardChain* bc )
{
	return &bc->getChainElement( chainIndex , elementIndex );
}

EXPORTFUNC void BC_clearChain( int chainIndex , Ogre::BillboardChain* bc )
{
	bc->clearChain( chainIndex );
}

EXPORTFUNC void BC_clearAllChains( Ogre::BillboardChain* bc )
{
	bc->clearAllChains();
}

EXPORTFUNC const char* BC_getMaterialName( Ogre::BillboardChain* bc )
{
	return bc->getMaterialName().c_str();
}

EXPORTFUNC void BC_setMaterialName( const char* name , Ogre::BillboardChain* bc )
{
	bc->setMaterialName( name );
}



//BillboardChain::Element Structure
EXPORTFUNC Ogre::BillboardChain::Element* BCE_createEmpty()
{
	return new Ogre::BillboardChain::Element();
}

EXPORTFUNC Ogre::BillboardChain::Element* BCE_create(Ogre::Vector3* position , float width , float texCoord , Ogre::ColourValue* colour )
{
	return new Ogre::BillboardChain::Element(*position , width , texCoord , *colour );
}

EXPORTFUNC void BCE_delete( Ogre::BillboardChain::Element* elem )
{
	delete elem;
}

//Renderable Methods that will not work with void pointers-
EXPORTFUNC void BC_setCustomParameter( int index , Ogre::Vector4* value , Ogre::BillboardChain* bc )
{
	bc->setCustomParameter( index , *value );
}

EXPORTFUNC Ogre::Vector4* BC_getCustomParameter( int index , Ogre::BillboardChain* bc )
{
	return new Ogre::Vector4( bc->getCustomParameter( index ) );
}

// Not in shoggoth
/*int BC_getNormaliseNormals( Ogre::BillboardChain* bc )
{
	return bc->getNormaliseNormals();
}

Ogre::Vector3* BC_getWorldPosition( Ogre::BillboardChain* bc )
{
	return new Ogre::Vector3( bc->getWorldPosition() );
}*/

EXPORTFUNC Ogre::LightList* BC_getLights( Ogre::BillboardChain* bc )
{
	Ogre::LightList* li = new Ogre::LightList( bc->getLights() );
	return li;
}

EXPORTFUNC int BC_getNumWorldTransforms( Ogre::BillboardChain* bc )
{
	return bc->getNumWorldTransforms();
}

// Not in shoggoth
/*Ogre::PlaneList* BC_getClipPlanes( Ogre::BillboardChain* bc )
{
	Ogre::PlaneList *pl = new Ogre::PlaneList( bc->getClipPlanes() );
	return pl;
}*/

EXPORTFUNC int BC_getCastsShadows( Ogre::BillboardChain* bc )
{
	return bc->getCastsShadows();
}









#endif
