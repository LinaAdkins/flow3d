#ifndef H_OGRERAYSCENEQUERYRESULT
#define H_OGRERAYSCENEQUERYRESULT

//RaySceneQueryResult-------------------------------------------------------
EXPORTFUNC Ogre::RaySceneQueryResult* RSQR_create()
{
	Ogre::RaySceneQueryResult* rsqr = new Ogre::RaySceneQueryResult();
	return rsqr;
}

EXPORTFUNC void RSQR_delete( Ogre::RaySceneQueryResult* rsqr )
{
	delete rsqr;
}

EXPORTFUNC Ogre::RaySceneQueryResult::iterator* RSQR_begin( Ogre::RaySceneQueryResult* rsqr )
{
	Ogre::RaySceneQueryResult::iterator* rsqri = new Ogre::RaySceneQueryResult::iterator();
	*rsqri = rsqr->begin();
	return rsqri;
}

EXPORTFUNC Ogre::RaySceneQueryResult::iterator* RSQR_end( Ogre::RaySceneQueryResult* rsqr )
{
	Ogre::RaySceneQueryResult::iterator* rsqri = new Ogre::RaySceneQueryResult::iterator();
	*rsqri = rsqr->end();
	return rsqri;
}

EXPORTFUNC Ogre::RaySceneQueryResultEntry* RSQR_at( int index , Ogre::RaySceneQueryResult* rsqr )
{
	Ogre::RaySceneQueryResultEntry* entry = new Ogre::RaySceneQueryResultEntry( rsqr->at( index ) );
	return entry;
}
//RaySceneQueryResult::Iterator------------------------------------------------
EXPORTFUNC Ogre::RaySceneQueryResultEntry* RSQRI_getEntry( Ogre::RaySceneQueryResult::iterator* iter )
{
	Ogre::RaySceneQueryResultEntry* entry = new Ogre::RaySceneQueryResultEntry( *(*iter) );
	return entry;
}
EXPORTFUNC int RSQRI_isNotEqualTo( Ogre::RaySceneQueryResult::iterator* a , Ogre::RaySceneQueryResult::iterator* b )
{
	return *a!=*b;
}

EXPORTFUNC int RSQRI_isEqualTo( Ogre::RaySceneQueryResult::iterator* a , Ogre::RaySceneQueryResult::iterator* b )
{
	return *a==*b;
}

EXPORTFUNC void RSQRI_delete( Ogre::RaySceneQueryResult::iterator* i )
{
	delete i;
}

//RaySceneQueryResultEntry
EXPORTFUNC Ogre::MovableObject* RSQRENTRY_getMovable( Ogre::RaySceneQueryResultEntry* entry )
{
	return MovableObjectChildCast(entry->movable);
}

EXPORTFUNC float RSQRENTRY_getDistance( Ogre::RaySceneQueryResultEntry* entry )
{
	return entry->distance;
}

EXPORTFUNC Ogre::SceneQuery::WorldFragment* RSQRENTRY_getWorldFragment( Ogre::RaySceneQueryResultEntry* entry )
{
	return entry->worldFragment;
}

EXPORTFUNC int RSQRENTRY_isLessThan( Ogre::RaySceneQueryResultEntry* a , Ogre::RaySceneQueryResultEntry* b )
{
	return a<b;
}

EXPORTFUNC void RSQRENTRY_delete( Ogre::RaySceneQueryResultEntry* r )
{
	delete r;
}


#endif
