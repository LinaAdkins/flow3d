#ifndef H_OGREVECTOR4
#define H_OGREVECTOR4

EXPORTFUNC Ogre::Vector4* VECTOR4_createEmpty()
{
	return new Ogre::Vector4(Ogre::Real(0));
}

EXPORTFUNC Ogre::Vector4* VECTOR4_create( float fX , float fY , float fZ , float fW )
{
	return new Ogre::Vector4( fX , fY , fZ , fW );
}

//Vector4 (const Real afCoordinate[4]) - Blitz Collections are not compatible with C arrays
//Vector4 (const int afCoordinate[4]) - Blitz Collections are not compatible with C arrays

EXPORTFUNC Ogre::Vector4* VECTOR4_createWithFloatPtr( float* r )
{
	return new Ogre::Vector4( r );
}

EXPORTFUNC Ogre::Vector4* VECTOR4_createWithScalar( float scalar )
{
	return new Ogre::Vector4( scalar );
}

EXPORTFUNC Ogre::Vector4* VECTOR4_createWithVector3( Ogre::Vector3* rhs )
{
	return new Ogre::Vector4( *rhs );
}

EXPORTFUNC Ogre::Vector4* VECTOR4_createWithVector4( Ogre::Vector4* rkVector )
{
	return new Ogre::Vector4( *rkVector );
}

//Real 	operator[] (const size_t i) const ********  Array operator functions supplanted by
//Real & 	operator[] (const size_t i)   ********  setValue and getValue.

EXPORTFUNC float VECTOR4_getValue( int i , Ogre::Vector4* v )
{
	return (*v)[i];
}

EXPORTFUNC void VECTOR4_setValue( int i , float value , Ogre::Vector4* v )
{
	(*v)[i] = value;
}

//Real * 	ptr ()  ************** Pointer accessor functions are unnecessary
//const Real * 	ptr () const ***** because they are more easilly accessible elsewhere.
EXPORTFUNC void VECTOR4_eq( Ogre::Vector4* rkVector , Ogre::Vector4* v )
{
	*v=*rkVector;
}

EXPORTFUNC void VECTOR4_eqWithScalar( float fScalar , Ogre::Vector4* v )
{
	*v=fScalar;
}

EXPORTFUNC int VECTOR4_isEqualTo( Ogre::Vector4* rkVector , Ogre::Vector4* v )
{
	return *v==*rkVector;
}

EXPORTFUNC int VECTOR4_isNotEqualTo( Ogre::Vector4* rkVector , Ogre::Vector4* v )
{
	return *v!=*rkVector;
}

EXPORTFUNC void VECTOR4_eqWithVector3( Ogre::Vector3* rhs , Ogre::Vector4* v )
{
	*v=*rhs;
}

EXPORTFUNC Ogre::Vector4* VECTOR4_addWithScalar( float fScalar , Ogre::Vector4* v )
{
	return new Ogre::Vector4( *v+fScalar );
}

EXPORTFUNC Ogre::Vector4* VECTOR4_add( Ogre::Vector4* rkVector , Ogre::Vector4* v )
{
	return new Ogre::Vector4( *v+*rkVector );
}

EXPORTFUNC Ogre::Vector4* VECTOR4_subWithScalar( float fScalar , Ogre::Vector4* v )
{
	return new Ogre::Vector4( *v-fScalar );
}

EXPORTFUNC Ogre::Vector4* VECTOR4_sub( Ogre::Vector4* rkVector , Ogre::Vector4* v )
{
	return new Ogre::Vector4( *v-*rkVector );
}

EXPORTFUNC Ogre::Vector4* VECTOR4_mulWithScalar( float fScalar , Ogre::Vector4* v )
{
	return new Ogre::Vector4( *v * fScalar );
}

EXPORTFUNC Ogre::Vector4* VECTOR4_mul( Ogre::Vector4* rhs , Ogre::Vector4* v )
{
	return new Ogre::Vector4((*v)*(*rhs));
}

EXPORTFUNC Ogre::Vector4* VECTOR4_divWithScalar( float fScalar , Ogre::Vector4* v )
{
	return new Ogre::Vector4( *v/fScalar );
}

EXPORTFUNC Ogre::Vector4* VECTOR4_div( Ogre::Vector4* rhs , Ogre::Vector4* v )
{
	return new Ogre::Vector4( *v/(*rhs) );
}

EXPORTFUNC Ogre::Vector4* VECTOR4_positive( Ogre::Vector4* v )
{
	return new Ogre::Vector4( +(*v) );
}

EXPORTFUNC Ogre::Vector4* VECTOR4_negative( Ogre::Vector4* v )
{
	return new Ogre::Vector4( -(*v) );
}

EXPORTFUNC void VECTOR4_addEq( Ogre::Vector4* rkVector , Ogre::Vector4* v )
{
	*v+=*rkVector;
}



EXPORTFUNC void VECTOR4_subEq( Ogre::Vector4* rkVector , Ogre::Vector4* v )
{
	*v-=*rkVector;
}

EXPORTFUNC void VECTOR4_mulEqWithScalar( float fScalar , Ogre::Vector4* v )
{
	*v *= fScalar;
}

EXPORTFUNC void VECTOR4_addEqWithScalar( float fScalar , Ogre::Vector4* v )
{
	*v+=fScalar;
}

EXPORTFUNC void VECTOR4_subEqWithScalar( float fScalar , Ogre::Vector4* v )
{
	*v-=fScalar;
}

EXPORTFUNC void VECTOR4_mulEq( Ogre::Vector4* rkVector , Ogre::Vector4* v )
{
	*v *= *rkVector;
}

EXPORTFUNC void VECTOR4_divEqWithScalar( float fScalar , Ogre::Vector4* v )
{
	*v /= fScalar;
}

EXPORTFUNC void VECTOR4_divEq( Ogre::Vector4* rkVector , Ogre::Vector4* v )
{
	*v /= *rkVector;
}

EXPORTFUNC float VECTOR4_dotProduct( Ogre::Vector4* vec , Ogre::Vector4* v )
{
	return v->dotProduct( *vec );
}

EXPORTFUNC const char* VECTOR4_toStr( Ogre::Vector4* v )
{
	std::ostringstream os;
	os << *v;
	tempString.clear();
	tempString = os.str();
	return tempString.c_str();
}

EXPORTFUNC void VECTOR4_delete( Ogre::Vector4* v )
{
	delete v;
}

EXPORTFUNC float VECTOR4_getW( Ogre::Vector4* v )
{
	return v->w;
}

EXPORTFUNC float VECTOR4_getX( Ogre::Vector4* v )
{
	return v->x;
}

EXPORTFUNC float VECTOR4_getY( Ogre::Vector4* v )
{
	return v->y;
}

EXPORTFUNC float VECTOR4_getZ( Ogre::Vector4* v )
{
	return v->z;
}

EXPORTFUNC void VECTOR4_setW( float w , Ogre::Vector4* v )
{
	v->w = w;
}

EXPORTFUNC void VECTOR4_setX( float x , Ogre::Vector4* v )
{
	v->x = x;
}

EXPORTFUNC void VECTOR4_setY( float y , Ogre::Vector4* v )
{
	v->y = y;
}

EXPORTFUNC void VECTOR4_setZ( float z , Ogre::Vector4* v )
{
	v->z = z;
}

EXPORTFUNC void VECTOR4_setAll( float x , float y , float z , float w, Ogre::Vector4* v )
{
	v->x = x;
	v->y = y;
	v->z = z;
	v->w = w;
}

// Static Attributes-
EXPORTFUNC Ogre::Vector4* VECTOR4_ZERO()
{
	return (Ogre::Vector4*)&Ogre::Vector4::ZERO;
}




#endif
