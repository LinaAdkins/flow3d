#ifndef H_OGREMESHMANAGER
#define H_OGREMESHMANAGER

EXPORTFUNC void MM_createPlane( const char* name , const char* groupName , float width , float height , int xSegments , int ySegments )
{
	Ogre::MeshManager::getSingleton().createPlane( name , groupName , Ogre::Plane( Ogre::Vector3::UNIT_Z , Ogre::Vector3::ZERO) , width , height , xSegments, ySegments );
}

EXPORTFUNC Ogre::Mesh* MM_load( const char* filename , const char* groupName, int vertexBufferUsage , int indexBufferUsage , bool vertexBufferShadowed , bool indexBufferShadowed )
{
	return Ogre::MeshManager::getSingleton().load( filename , groupName , (Ogre::HardwareBuffer::Usage)vertexBufferUsage , (Ogre::HardwareBuffer::Usage)indexBufferUsage , (bool)vertexBufferShadowed , (bool)indexBufferShadowed ).getPointer();
}

EXPORTFUNC Ogre::Mesh* MM_createManual( const char* name , const char* groupName , Ogre::ManualResourceLoader* loader )
{
	return Ogre::MeshManager::getSingleton().createManual( name, groupName , loader ).getPointer();
}

EXPORTFUNC Ogre::Resource* MM_getByName( const char* name )
{
	return Ogre::MeshManager::getSingleton().getByName(name).getPointer();
}

EXPORTFUNC bool MM_resourceExists( const char* name )
{
	return Ogre::MeshManager::getSingleton().resourceExists( name );
}

#endif
