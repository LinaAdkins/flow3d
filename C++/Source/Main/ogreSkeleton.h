#ifndef H_OGRESKELETON
#define H_OGRESKELETON

EXPORTFUNC Ogre::Bone* SKELETON_createBone( Ogre::Skeleton* s )
{
	return s->createBone();
}

EXPORTFUNC Ogre::Bone* SKELETON_createBoneWithHandle( int handle , Ogre::Skeleton* s )
{
	return s->createBone( handle );
}

EXPORTFUNC Ogre::Bone* SKELETON_createBoneWithName( const char* name , Ogre::Skeleton* s )
{
	return s->createBone( name );
}

EXPORTFUNC Ogre::Bone* SKELETON_createBoneWithNameHandle( const char* name , int handle , Ogre::Skeleton* s )
{
	return s->createBone( name , handle );
}

EXPORTFUNC int SKELETON_getNumBones( Ogre::Skeleton* s )
{
	return s->getNumBones();
}

/*  ---  Not supported ---
	virtual Bone * 	getRootBone (void) const
*/

EXPORTFUNC Ogre::Skeleton::BoneIterator* SKELETON_getRootBoneIterator( Ogre::Skeleton* s )
{
	return new Ogre::Skeleton::BoneIterator( s->getRootBoneIterator() );
}

EXPORTFUNC Ogre::Skeleton::BoneIterator* SKELETON_getBoneIterator( Ogre::Skeleton* s )
{
	return new Ogre::Skeleton::BoneIterator( s->getBoneIterator() );
}

EXPORTFUNC Ogre::Bone* SKELETON_getBone( int handle , Ogre::Skeleton* s )
{
	return s->getBone( handle );
}

EXPORTFUNC Ogre::Bone* SKELETON_getBoneWithName( const char* name, Ogre::Skeleton* s )
{
	return s->getBone( name );
}

EXPORTFUNC bool SKELETON_hasBone( const char* name , Ogre::Skeleton* s )
{
	return s->hasBone( name );
}

EXPORTFUNC void SKELETON_setBindingPose( Ogre::Skeleton* s )
{
	s->setBindingPose();
}

EXPORTFUNC void SKELETON_reset( bool resetManualBones , Ogre::Skeleton* s )
{
	s->reset( resetManualBones );
}

EXPORTFUNC Ogre::Animation* SKELETON_createAnimation( const char* name , float length , Ogre::Skeleton* s )
{
	return s->createAnimation( name , length );
}

EXPORTFUNC Ogre::Animation* SKELETON_getAnimation( const char* name , Ogre::Skeleton* s )
{
	return s->getAnimation( name );
}

EXPORTFUNC bool SKELETON_hasAnimation( const char* name , Ogre::Skeleton* s )
{
	return s->hasAnimation( name );
}

EXPORTFUNC void SKELETON_removeAnimation( const char*  name , Ogre::Skeleton* s )
{
	s->removeAnimation( name );
}

EXPORTFUNC int SKELETON_getNumAnimations( Ogre::Skeleton* s )
{
	return s->getNumAnimations();
}

EXPORTFUNC void SKELETON_optimiseAllAnimations( bool preservingIdentityNodeTracks , Ogre::Skeleton* s )
{
	s->optimiseAllAnimations();
}

EXPORTFUNC int SKELETON_getBlendMode( Ogre::Skeleton* s )
{
	return s->getBlendMode();
}

EXPORTFUNC void SKELETON_setBlendMode( int state , Ogre::Skeleton* s )
{
	s->setBlendMode(  Ogre::SkeletonAnimationBlendMode( state ) );
}

EXPORTFUNC void SKELETON__notifyManualBonesDirty( Ogre::Skeleton* s )
{
	s->_notifyManualBonesDirty();
}

EXPORTFUNC void SKELETON__notifyManualBoneStateChange( Ogre::Bone* bone , Ogre::Skeleton* s )
{
	s->_notifyManualBoneStateChange( bone );
}

EXPORTFUNC bool SKELETON_getManualBonesDirty( Ogre::Skeleton* s )
{
	return s->getManualBonesDirty();
}

EXPORTFUNC bool SKELETON_hasManualBones( Ogre::Skeleton* s )
{
	return s->hasManualBones();
}

EXPORTFUNC const char* SKELETON_getName( Ogre::Skeleton* s )
{
	tempString = s->getName();
	return tempString.c_str();
}

#endif
