#ifndef H_TOGRELOG
#define H_TOGRELOG

EXPORTFUNC void OGRELOG_setDebugOutputEnabled( bool debugOutput , Ogre::Log* l )
{
	l->setDebugOutputEnabled( debugOutput );
}

#endif