#ifndef H__THARDWAREVERTEXBUFFER
#define H__THARDWAREVERTEXBUFFER

EXPORTFUNC int HVB_getSizeInBytes( Ogre::HardwareVertexBufferSharedPtr* b )
{
	return (*b)->getSizeInBytes();
}

EXPORTFUNC void HVB_writeDataWithFloatList( int offset , int length , std::vector<float>* pSource , bool discardWholeBuffer , Ogre::HardwareVertexBufferSharedPtr* b )
{
	(*b)->writeData( offset , length , &(*pSource)[0] , discardWholeBuffer );
}

EXPORTFUNC void HVB_delete( Ogre::HardwareVertexBufferSharedPtr* b )
{
	delete b;
}

#endif
