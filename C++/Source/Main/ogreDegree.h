#ifndef H_OGREDEGREE
#define H_OGREDEGREE

EXPORTFUNC Ogre::Degree* DEGREE_create( float d )
{
	return new Ogre::Degree( d );
}

EXPORTFUNC float DEGREE_valueDegrees( Ogre::Degree* degree )
{
	return degree->valueDegrees();
}

EXPORTFUNC float DEGREE_valueRadians( Ogre::Degree* degree )
{
	return degree->valueRadians();
}

EXPORTFUNC float DEGREE_valueAngleUnits( Ogre::Degree* degree )
{
	return degree->valueAngleUnits();
}

EXPORTFUNC void DEGREE_delete( Ogre::Degree* d )
{
	delete d;
}


#endif
