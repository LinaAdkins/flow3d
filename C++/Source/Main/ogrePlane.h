#ifndef H_OGREPLANE
#define H_OGREPLANE

EXPORTFUNC Ogre::Plane* PLANE_createEmpty()
{
	return new Ogre::Plane();
}

EXPORTFUNC Ogre::Plane* PLANE_createWithPlane( Ogre::Plane* rhs )
{
	return new Ogre::Plane( *rhs );
}

EXPORTFUNC Ogre::Plane* PLANE_create( Ogre::Vector3* rkNormal , float fConstant )
{
	return new Ogre::Plane( *rkNormal , fConstant );
}

EXPORTFUNC Ogre::Plane* PLANE_createWithPoint( Ogre::Vector3* rkNormal , Ogre::Vector3* rkPoint )
{
	return new Ogre::Plane( *rkNormal ,  *rkPoint );
}

EXPORTFUNC Ogre::Plane* PLANE_createWith3Points( Ogre::Vector3* rkPoint0 , Ogre::Vector3* rkPoint1 , Ogre::Vector3* rkPoint2 )
{
	return new Ogre::Plane( *rkPoint0  , *rkPoint1 , *rkPoint2 );
}

EXPORTFUNC int PLANE_getSide( Ogre::Vector3* rkPoint , Ogre::Plane* p )
{
	return p->getSide( *rkPoint );
}

EXPORTFUNC int PLANE_getSideWithAAB( Ogre::AxisAlignedBox* rkBox , Ogre::Plane* p )
{
	return p->getSide( *rkBox );
}

EXPORTFUNC int PLANE_getSideWithBox( Ogre::Vector3* centre , Ogre::Vector3* halfsize , Ogre::Plane* p )
{
	return p->getSide( *centre , *halfsize );
}

EXPORTFUNC float PLANE_getDistance( Ogre::Vector3* rkPoint , Ogre::Plane* p )
{
	return p->getDistance( *rkPoint );
}

EXPORTFUNC void PLANE_redefine( Ogre::Vector3* rkPoint0 , Ogre::Vector3* rkPoint1 , Ogre::Vector3* rkPoint2 , Ogre::Plane* p )
{
	p->redefine( *rkPoint0 , *rkPoint1 , *rkPoint2 );
}

EXPORTFUNC void PLANE_redefineWithNormal( Ogre::Vector3* rkNormal , Ogre::Vector3* rkPoint , Ogre::Plane* p )
{
	p->redefine( *rkNormal , *rkPoint );
}

EXPORTFUNC Ogre::Vector3* PLANE_projectVector( Ogre::Vector3* v , Ogre::Plane* p )
{
	return new Ogre::Vector3( p->projectVector(*v) );
}

EXPORTFUNC float PLANE_normalise( Ogre::Plane* p )
{
	return p->normalise();
}

EXPORTFUNC int PLANE_isEqualTo( Ogre::Plane* rhs , Ogre::Plane* p )
{
	return *p == *rhs;
}

EXPORTFUNC int PLANE_isNotEqualTo( Ogre::Plane* rhs , Ogre::Plane* p )
{
	return *p != *rhs;
}

EXPORTFUNC const char* PLANE_toStr( Ogre::Plane* p )
{
	std::ostringstream os;
	os << *p;
	tempString.clear();
	tempString = os.str();
	return tempString.c_str();
}

EXPORTFUNC void PLANE_delete( Ogre::Plane* p )
{
	delete p;
}

EXPORTFUNC Ogre::Vector3* PLANE_getNormal( Ogre::Plane* p )
{
	return new Ogre::Vector3(p->normal);
}

EXPORTFUNC float PLANE_getD( Ogre::Plane* p )
{
	return p->d;
}

EXPORTFUNC void PLANE_setNormal( Ogre::Vector3* normal , Ogre::Plane* p )
{
	p->normal = *normal;
}

EXPORTFUNC void PLANE_setD( float d , Ogre::Plane* p )
{
	p->d = d;
}

#endif
