#ifndef H_OGREPARTICLESYSTEM
#define H_OGREPARTICLESYSTEM

//Class openParticleIterator - This is to get around the protected constructor in Ogre::ParticleIterator
class openParticleIterator : public Ogre::ParticleIterator
{
public:
	openParticleIterator(std::list<Ogre::Particle*>::iterator start, std::list<Ogre::Particle*>::iterator end):Ogre::ParticleIterator( start , end ){}
};

class openParticleSystem : public Ogre::ParticleSystem
{	
public:
	openParticleIterator* _getOpenIterator();
};

openParticleIterator* openParticleSystem::_getOpenIterator(void)
{
	openParticleIterator* pi = new openParticleIterator(mActiveParticles.begin(), mActiveParticles.end());
	return pi;
}



//ParticleSystem ()							// Managed - constructor not needed
//ParticleSystem (const String &name, const String &resourceGroupName)	// Managed - constructor not needed
//~ParticleSystem ()							// Managed - constructor not needed

EXPORTFUNC void PS_setRenderer( const char* typeName , Ogre::ParticleSystem* ps )
{
	ps->setRenderer( typeName );
}

EXPORTFUNC Ogre::ParticleSystemRenderer* PS_getRenderer( Ogre::ParticleSystem* ps )
{
	return ps->getRenderer();
}

EXPORTFUNC const char* PS_getRendererName( Ogre::ParticleSystem* ps )
{
	tempString = ps->getRendererName();
	return tempString.c_str();
}

EXPORTFUNC Ogre::ParticleEmitter* PS_addEmitter( const char* emitterType , Ogre::ParticleSystem* ps )
{
	return ps->addEmitter( emitterType );
}

EXPORTFUNC Ogre::ParticleEmitter* PS_getEmitter( int index , Ogre::ParticleSystem* ps )
{
	return ps->getEmitter( index );
}

EXPORTFUNC int PS_getNumEmitters( Ogre::ParticleSystem* ps )
{
	return ps->getNumEmitters();
}

EXPORTFUNC void PS_removeEmitter( int index , Ogre::ParticleSystem* ps )
{
	ps->removeEmitter( index );
}

EXPORTFUNC void PS_removeAllEmitters( Ogre::ParticleSystem* ps )
{
	ps->removeAllEmitters();
}

EXPORTFUNC Ogre::ParticleAffector* PS_addAffector( const char* affectorType , Ogre::ParticleSystem* ps )
{
	return ps->addAffector( affectorType );
}

EXPORTFUNC Ogre::ParticleAffector* PS_getAffector( int index , Ogre::ParticleSystem* ps )
{
	return ps->getAffector( index );
}

EXPORTFUNC int PS_getNumAffectors( Ogre::ParticleSystem* ps )
{
	return ps->getNumAffectors();
}

EXPORTFUNC void PS_removeAffector( int index , Ogre::ParticleSystem* ps )
{
	ps->removeAffector( index );
}

EXPORTFUNC void PS_removeAllAffectors( Ogre::ParticleSystem* ps )
{
	ps->removeAllAffectors();
}

EXPORTFUNC void PS_clear( Ogre::ParticleSystem* ps )
{
	ps->clear();
}

EXPORTFUNC unsigned long PS_getNumParticles( Ogre::ParticleSystem* ps )
{
	return ps->getNumParticles();
}

EXPORTFUNC Ogre::Particle* PS_createParticle( Ogre::ParticleSystem* ps )
{
	return ps->createParticle();
}

EXPORTFUNC Ogre::Particle* PS_createEmitterParticle( const char* emitterName , Ogre::ParticleSystem* ps )
{
	return ps->createEmitterParticle( emitterName );
}

EXPORTFUNC Ogre::Particle* PS_getParticle( unsigned long index , Ogre::ParticleSystem* ps )
{
	return ps->getParticle( index );
}
EXPORTFUNC unsigned long PS_getParticleQuota( Ogre::ParticleSystem* ps )
{
	return ps->getParticleQuota();
}

EXPORTFUNC void PS_setParticleQuota( unsigned long quota , Ogre::ParticleSystem* ps )
{
	ps->setParticleQuota( quota );
}

EXPORTFUNC unsigned long PS_getEmittedEmitterQuota( Ogre::ParticleSystem* ps )
{
	return ps->getEmittedEmitterQuota();
}

EXPORTFUNC void PS_setEmittedEmitterQuota( unsigned long quota , Ogre::ParticleSystem* ps )
{
	ps->setEmittedEmitterQuota( quota );
}

EXPORTFUNC void PS_eq( Ogre::ParticleSystem* a , Ogre::ParticleSystem* b )
{
	*a=*b;
}

EXPORTFUNC void PS__update( float timeElapsed , Ogre::ParticleSystem* ps )
{
	ps->_update( timeElapsed );
}

EXPORTFUNC ::openParticleIterator* PS__getIterator( openParticleSystem* ps )
{
	return ps->_getOpenIterator();
}

EXPORTFUNC void PS_setMaterialName( const char* name , Ogre::ParticleSystem* ps )
{
	ps->setMaterialName( name );
}

EXPORTFUNC const char* PS_getMaterialName( Ogre::ParticleSystem* ps )
{
	return ps->getMaterialName().c_str();
}

EXPORTFUNC void PS__notifyCurrentCamera( Ogre::Camera* cam , Ogre::ParticleSystem* ps )
{
	ps->_notifyCurrentCamera( cam );
}

EXPORTFUNC void PS__notifyAttached( Ogre::Node* parent , bool isTagPoint , Ogre::ParticleSystem* ps )
{
	ps->_notifyAttached( parent , isTagPoint );
}

EXPORTFUNC Ogre::AxisAlignedBox* PS_getBoundingBox( Ogre::ParticleSystem* ps )
{
	return new Ogre::AxisAlignedBox( ps->getBoundingBox() );
}

EXPORTFUNC float PS_getBoundingRadius( Ogre::ParticleSystem* ps )
{
	return ps->getBoundingRadius();
}

EXPORTFUNC void PS__updateRenderQueue( Ogre::RenderQueue* queue , Ogre::ParticleSystem* ps )
{
	ps->_updateRenderQueue( queue );
}

//void 	visitRenderables (Renderable::Visitor *vi...                    // 1.6.0 method

EXPORTFUNC void PS_fastForward( float time , float interval , Ogre::ParticleSystem* ps )
{
	ps->fastForward( time , interval );
}

EXPORTFUNC void PS_setSpeedFactor( float speedFactor , Ogre::ParticleSystem* ps )
{
	ps->setSpeedFactor( speedFactor );
}

EXPORTFUNC float PS_getSpeedFactor( Ogre::ParticleSystem* ps )
{
	return ps->getSpeedFactor();
}

EXPORTFUNC void PS_setIterationInterval( float iterationInterval , Ogre::ParticleSystem* ps )
{
	ps->setIterationInterval( iterationInterval );
}

EXPORTFUNC float PS_getIterationInterval( Ogre::ParticleSystem* ps )
{
	return ps->getIterationInterval();
}

EXPORTFUNC void PS_setNonVisibleUpdateTimeout( float timeout , Ogre::ParticleSystem* ps )
{
	ps->setNonVisibleUpdateTimeout( timeout );
}

EXPORTFUNC float PS_getNonVisibleUpdateTimeout( Ogre::ParticleSystem* ps )
{
	return ps->getNonVisibleUpdateTimeout();
}


EXPORTFUNC const char* PS_getMovableType( Ogre::ParticleSystem* ps )
{
	return ps->getMovableType().c_str();
}

EXPORTFUNC void PS__notifyParticleResized( Ogre::ParticleSystem* ps )
{
	ps->_notifyParticleResized();
}

EXPORTFUNC void PS__notifyParticleRotated( Ogre::ParticleSystem* ps )
{
	ps->_notifyParticleRotated();
}

EXPORTFUNC void PS_setDefaultDimensions( float width , float height , Ogre::ParticleSystem* ps )
{
	ps->setDefaultDimensions( width , height );
}

EXPORTFUNC void PS_setDefaultWidth( float width , Ogre::ParticleSystem* ps )
{
	ps->setDefaultWidth( width );
}

EXPORTFUNC float PS_getDefaultWidth( Ogre::ParticleSystem* ps )
{
	return ps->getDefaultWidth();
}

EXPORTFUNC void PS_setDefaultHeight( float height , Ogre::ParticleSystem* ps )
{
	ps->setDefaultHeight( height );
}

EXPORTFUNC float PS_getDefaultHeight( Ogre::ParticleSystem* ps )
{
	return ps->getDefaultHeight();
}

EXPORTFUNC int PS_getCullIndividually( Ogre::ParticleSystem* ps )
{
	return ps->getCullIndividually();
}

EXPORTFUNC void PS_setCullIndividually( bool cullIndividual , Ogre::ParticleSystem* ps )
{
	ps->setCullIndividually( cullIndividual );
}

EXPORTFUNC const char* PS_getResourceGroupName( Ogre::ParticleSystem* ps )
{
	return ps->getResourceGroupName().c_str();
}

EXPORTFUNC const char* PS_getOrigin( Ogre::ParticleSystem* ps )
{
	return ps->getOrigin().c_str();
}

EXPORTFUNC void PS__notifyOrigin( const char* origin , Ogre::ParticleSystem* ps )
{
	ps->_notifyOrigin( origin );
}

EXPORTFUNC void PS_setRenderQueueGroup( unsigned int queueID , Ogre::ParticleSystem* ps )
{
	ps->setRenderQueueGroup( queueID );
}


EXPORTFUNC void PS_setSortingEnabled( bool enabled , Ogre::ParticleSystem* ps )
{
	ps->setSortingEnabled( enabled );
}

EXPORTFUNC int PS_getSortingEnabled( Ogre::ParticleSystem* ps )
{
	return ps->getSortingEnabled();
}

EXPORTFUNC void PS_setBounds( Ogre::AxisAlignedBox* aabb , Ogre::ParticleSystem* ps )
{
	ps->setBounds( *aabb );
}

EXPORTFUNC void PS_setBoundsAutoUpdated( bool autoUpdate , float stopIn , Ogre::ParticleSystem* ps )
{
	ps->setBoundsAutoUpdated( autoUpdate , stopIn );
}

EXPORTFUNC void PS_setKeepParticlesInLocalSpace( bool keepLocal , Ogre::ParticleSystem* ps )
{
	ps->setKeepParticlesInLocalSpace( keepLocal );
}

EXPORTFUNC int PS_getKeepParticlesInLocalSpace( Ogre::ParticleSystem* ps )
{
	return ps->getKeepParticlesInLocalSpace();
}

EXPORTFUNC void PS__updateBounds( Ogre::ParticleSystem* ps )
{
	ps->_updateBounds();
}

EXPORTFUNC unsigned int PS_getTypeFlags( Ogre::ParticleSystem* ps )
{
	return ps->getTypeFlags();
}

// MovableObject Reimplementation

EXPORTFUNC void PS__notifyCreator( Ogre::MovableObjectFactory* fact , Ogre::ParticleSystem* p )
{
	p->_notifyCreator( fact );
}

EXPORTFUNC Ogre::MovableObjectFactory* PS__getCreator( Ogre::ParticleSystem* p )
{
	return p->_getCreator();
}

EXPORTFUNC void PS__notifyManager( Ogre::SceneManager* man , Ogre::ParticleSystem* p )
{
	p->_notifyManager( man );
}

EXPORTFUNC Ogre::SceneManager* PS__getManager( Ogre::ParticleSystem* p )
{
	return p->_getManager();
}

EXPORTFUNC const char* PS_getName( Ogre::ParticleSystem* ps )
{
	return ps->getName().c_str();
}

EXPORTFUNC Ogre::Node* PS_getParentNode( Ogre::ParticleSystem* ps )
{
	return ps->getParentNode();
}

EXPORTFUNC Ogre::SceneNode* PS_getParentSceneNode( Ogre::ParticleSystem* ps )
{
	return ps->getParentSceneNode();
}

EXPORTFUNC bool PS_isAttached( Ogre::ParticleSystem* ps )
{
	return ps->isAttached();
}

//void PS_detachFromParent( Ogre::ParticleSystem* ps ) - 1.6.0 method
//{
//	ps->detachFromParent();
//}

EXPORTFUNC bool PS_isInScene( Ogre::ParticleSystem* ps )
{
	return ps->isInScene();
}

EXPORTFUNC void PS__notifyMoved( Ogre::ParticleSystem* ps )
{
	ps->_notifyMoved();
}

EXPORTFUNC Ogre::AxisAlignedBox* PS_getWorldBoundingBox( bool derive , Ogre::ParticleSystem* p )
{
	return new Ogre::AxisAlignedBox( p->getWorldBoundingBox( derive ) );
}

EXPORTFUNC Ogre::Sphere* PS_getWorldBoundingSphere( bool derive , Ogre::ParticleSystem* p )
{
	return new Ogre::Sphere( p->getWorldBoundingSphere( derive ) );
}

EXPORTFUNC void PS_setVisible( bool visible , Ogre::ParticleSystem* p )
{
	p->setVisible( visible );
}

EXPORTFUNC int PS_getVisible( Ogre::ParticleSystem* p )
{
	return p->getVisible();
}

EXPORTFUNC int PS_isVisible( Ogre::ParticleSystem* p )
{
	return p->isVisible();
}

EXPORTFUNC void PS_setRenderingDistance( Ogre::Real dist , Ogre::ParticleSystem* p )
{
	p->setRenderingDistance( dist );
}

EXPORTFUNC float PS_getRenderingDistance( Ogre::ParticleSystem* p )
{
	return p->getRenderingDistance();
}

EXPORTFUNC void PS_setUserObject( Ogre::UserDefinedObject* obj , Ogre::ParticleSystem* p )
{
	p->setUserObject( obj );
}

EXPORTFUNC Ogre::UserDefinedObject* PS_getUserObject(  Ogre::ParticleSystem* p )
{
	return p->getUserObject();
}

EXPORTFUNC void PS_setUserAny( Ogre::Any* anything , Ogre::ParticleSystem* p )
{
	p->setUserAny( *anything );
}

EXPORTFUNC Ogre::Any* PS_getUserAny( Ogre::ParticleSystem* p )
{
	return new Ogre::Any( p->getUserAny() );
}

EXPORTFUNC unsigned int PS_getRenderQueueGroup( Ogre::ParticleSystem* p)
{
	return p->getRenderQueueGroup();
}

EXPORTFUNC Ogre::Matrix4* PS__getParentNodeFullTransform( Ogre::ParticleSystem* p )
{
	return new Ogre::Matrix4( p->_getParentNodeFullTransform() );
}

EXPORTFUNC void PS_setQueryFlags( unsigned int flags , Ogre::ParticleSystem* p )
{
	p->setQueryFlags( flags );
}

EXPORTFUNC void PS_addQueryFlags( unsigned int flags , Ogre::ParticleSystem* p )
{
	p->addQueryFlags( flags );
}

EXPORTFUNC void PS_removeQueryFlags( unsigned int flags , Ogre::ParticleSystem* p )
{
	p->removeQueryFlags( flags );
}

EXPORTFUNC unsigned int PS_getQueryFlags( Ogre::ParticleSystem* p )
{
	return p->getQueryFlags();
}

EXPORTFUNC void PS_setVisibilityFlags( unsigned int flags , Ogre::ParticleSystem* p )
{
	p->setVisibilityFlags( flags );
}

EXPORTFUNC void PS_addVisibilityFlags( unsigned int flags , Ogre::ParticleSystem* p )
{
	p->addVisibilityFlags( flags );
}

EXPORTFUNC void PS_removeVisibilityFlags( unsigned int flags , Ogre::ParticleSystem* p )
{
	p->removeVisibilityFlags( flags );
}

EXPORTFUNC unsigned int PS_getVisibilityFlags( Ogre::ParticleSystem* p )
{
	return p->getVisibilityFlags();
}

EXPORTFUNC void PS_setListener( Ogre::ParticleSystem::Listener* listener , Ogre::ParticleSystem* p )
{
	p->setListener( listener );
}

EXPORTFUNC Ogre::ParticleSystem::Listener* PS_getListener( Ogre::ParticleSystem* p )
{
	return p->getListener();
}

EXPORTFUNC Ogre::LightList* PS_queryLights( Ogre::ParticleSystem* p )
{
	return new Ogre::LightList( p->queryLights() );
}

//void PS_setDebugDisplayEnabled( bool enabled , Ogre::ParticleSystem* p ) 1.6.0 Method
//{
//	p->setDebugDisplayEnabled( enabled );
//}

//bool PS_isDebugDisplayEnabled( Ogre::ParticleSystem* p )  1.6.0 Method
//{
//	return p->isDebugDisplayEnabled();
//}

// Ogre::ShadowCaster
EXPORTFUNC void PS_setCastShadows( bool enabled , Ogre::ParticleSystem* p )
{
	p->setCastShadows( enabled );
}


EXPORTFUNC int PS_getCastShadows( Ogre::ParticleSystem* p )
{
	return p->getCastShadows();
}

EXPORTFUNC Ogre::EdgeData* PS_getEdgeList( Ogre::ParticleSystem* p )
{
	return p->getEdgeList();
}

EXPORTFUNC int PS_hasEdgeList( Ogre::ParticleSystem* p )
{
	return p->hasEdgeList();
}


EXPORTFUNC Ogre::AxisAlignedBox* PS_getLightCapBounds( Ogre::ParticleSystem* p )
{
	return new Ogre::AxisAlignedBox( p->getLightCapBounds() );
}

EXPORTFUNC Ogre::AxisAlignedBox* PS_getDarkCapBounds( Ogre::Light* light , float dirLightExtrusionDist , Ogre::ParticleSystem* p )
{
	return new Ogre::AxisAlignedBox( p->getDarkCapBounds( *light , dirLightExtrusionDist ) );
}

EXPORTFUNC Ogre::ShadowCaster::ShadowRenderableListIterator* PS_getShadowVolumeRenderableIterator( int shadowTechnique , Ogre::Light* light , Ogre::HardwareIndexBuffer* indexBuffer, bool extrudeVertices , float extrusionDistance , int flags , Ogre::ParticleSystem* p )
{
	return new Ogre::ShadowCaster::ShadowRenderableListIterator(  p->getShadowVolumeRenderableIterator( (Ogre::ShadowTechnique)shadowTechnique , light , &Ogre::HardwareIndexBufferSharedPtr(indexBuffer), extrudeVertices , extrusionDistance, flags ) );
}

EXPORTFUNC float PS_getPointExtrusionDistance( Ogre::Light* l , Ogre::ParticleSystem* p )
{
	return p->getPointExtrusionDistance( l );
}


//Static Public Member Functions
EXPORTFUNC void PS_setDefaultIterationInterval( float iterationInterval )
{
	Ogre::ParticleSystem ps;
	ps.setDefaultIterationInterval( iterationInterval );
}

EXPORTFUNC float PS_getDefaultIterationInterval()
{
	Ogre::ParticleSystem ps;
	return ps.getDefaultIterationInterval();
}

EXPORTFUNC void PS_setDefaultNonVisibleUpdateTimeout(float timeout)
{
	Ogre::ParticleSystem ps;
	ps.setDefaultNonVisibleUpdateTimeout( timeout );
}

EXPORTFUNC float PS_getDefaultNonVisibleUpdateTimeout()
{
	Ogre::ParticleSystem ps;
	return ps.getDefaultNonVisibleUpdateTimeout();
}



#endif
