#ifndef H_TMANUALOBJECT
#define H_TMANUALOBJECT

EXPORTFUNC void MANUALOBJECT_clear( Ogre::ManualObject* o )
{
	o->clear();
}

EXPORTFUNC void MANUALOBJECT_estimateVertexCount( int vcount , Ogre::ManualObject* o )
{
	o->estimateVertexCount(vcount);
}

EXPORTFUNC void MANUALOBJECT_estimateIndexCount( int icount , Ogre::ManualObject* o )
{
	o->estimateIndexCount(icount);
}

EXPORTFUNC void MANUALOBJECT_begin( const char* materialName , int opType , Ogre::ManualObject* o )
{
	o->begin( materialName , Ogre::RenderOperation::OperationType(opType) );
}

EXPORTFUNC void MANUALOBJECT_setDynamic( bool dyn , Ogre::ManualObject* o )
{
	o->setDynamic( dyn );
}

EXPORTFUNC bool MANUALOBJECT_getDynamic( Ogre::ManualObject* o )
{
	return o->getDynamic();
}

EXPORTFUNC void MANUALOBJECT_beginUpdate( int sectionIndex , Ogre::ManualObject* o )
{
	o->beginUpdate( sectionIndex );
}

EXPORTFUNC void MANUALOBJECT_positionWithVector3( Ogre::Vector3* pos , Ogre::ManualObject* o )
{
	o->position( *pos );
}

EXPORTFUNC void MANUALOBJECT_position( float x , float y , float z , Ogre::ManualObject* o )
{
	o->position( x ,y ,z );
}

EXPORTFUNC void MANUALOBJECT_normalWithVector3( Ogre::Vector3* norm , Ogre::ManualObject* o )
{
	o->normal( *norm );
}

EXPORTFUNC void MANUALOBJECT_normal( float x , float y , float z , Ogre::ManualObject* o )
{
	o->normal( x , y , z );
}

EXPORTFUNC void MANUALOBJECT_textureCoord1D(float u , Ogre::ManualObject* o )
{
	o->textureCoord(u);
}

EXPORTFUNC void MANUALOBJECT_textureCoord(float u , float v,  Ogre::ManualObject* o )
{
	o->textureCoord(u,v);
}

EXPORTFUNC void MANUALOBJECT_textureCoord3D( float u , float v , float w ,Ogre::ManualObject* o )
{
	o->textureCoord(u,v,w);
}

EXPORTFUNC void MANUALOBJECT_textureCoord4D( float x , float y , float z , float w , Ogre::ManualObject* o )
{
	o->textureCoord(x,y,z,w);
}

EXPORTFUNC void MANUALOBJECT_textureCoordWithVector2( Ogre::Vector2* uv , Ogre::ManualObject* o )
{
	o->textureCoord(*uv);
}

EXPORTFUNC void MANUALOBJECT_textureCoordWithVector3( Ogre::Vector3* uvw , Ogre::ManualObject* o )
{
	o->textureCoord(*uvw);
}

EXPORTFUNC void MANUALOBJECT_textureCoordWithVector4( Ogre::Vector4* xyzw , Ogre::ManualObject* o )
{
	o->textureCoord(*xyzw);
}
EXPORTFUNC void MANUALOBJECT_colourWithColourValue( Ogre::ColourValue* col , Ogre::ManualObject* o )
{
	o->colour(*col);
}
EXPORTFUNC void MANUALOBJECT_colour( float r , float g, float b , float a , Ogre::ManualObject* o )
{
	o->colour(r,g,b,a);
}

EXPORTFUNC void MANUALOBJECT_index( unsigned int idx , Ogre::ManualObject* o )
{
	o->index(idx);
}

EXPORTFUNC void MANUALOBJECT_triangle( unsigned int i1 , unsigned int i2 , unsigned int i3 , Ogre::ManualObject* o )
{
	o->triangle(i1,i2,i3);
}

EXPORTFUNC void MANUALOBJECT_quad( unsigned int i1 , unsigned int i2 , unsigned int i3 , unsigned int i4 ,Ogre::ManualObject* o )
{
	o->quad(i1,i2,i3,i4);
}

EXPORTFUNC Ogre::ManualObject::ManualObjectSection* MANUALOBJECT_end(Ogre::ManualObject* o )
{
	return o->end();
}

EXPORTFUNC void MANUALOBJECT_setMaterialName( int subindex , const char* name , Ogre::ManualObject* o )
{
	o->setMaterialName( subindex , name );
}

EXPORTFUNC Ogre::Mesh* MANUALOBJECT_convertToMesh( const char* meshName , const char* groupName , Ogre::ManualObject* o )
{
	return o->convertToMesh( meshName , groupName ).getPointer();
}

EXPORTFUNC void MANUALOBJECT_setUseIdentityProjection( bool useIdentityProjection , Ogre::ManualObject* o )
{
	o->setUseIdentityProjection( useIdentityProjection );
}

EXPORTFUNC bool MANUALOBJECT_getUseIdentityProjection( Ogre::ManualObject* o )
{
	return o->getUseIdentityProjection();
}

EXPORTFUNC void MANUALOBJECT_setUseIdentityView( bool useIdentityView , Ogre::ManualObject* o )
{
	o->setUseIdentityView( useIdentityView );
}

EXPORTFUNC bool MANUALOBJECT_getUseIdentityView( Ogre::ManualObject* o )
{
	return o->getUseIdentityView();
}

EXPORTFUNC void MANUALOBJECT_setBoundingBox( Ogre::AxisAlignedBox* box , Ogre::ManualObject* o )
{
	o->setBoundingBox( *box );
}

EXPORTFUNC Ogre::ManualObject::ManualObjectSection* MANUALOBJECT_getSection( unsigned int index , Ogre::ManualObject* o )
{
	return o->getSection( index );
}

EXPORTFUNC unsigned int MANUALOBJECT_getNumSections( Ogre::ManualObject* o )
{
	return o->getNumSections();
}

EXPORTFUNC void MANUALOBJECT_setKeepDeclarationOrder( bool keepOrder , Ogre::ManualObject* o )
{
	o->setKeepDeclarationOrder( keepOrder );
}

EXPORTFUNC bool MANUALOBJECT_getKeepDeclarationOrder( Ogre::ManualObject* o )
{
	return o->getKeepDeclarationOrder();
}

// MovableObject

EXPORTFUNC const char* MANUALOBJECT_getMovableType( Ogre::ManualObject* o )
{
	return o->getMovableType().c_str();
}

EXPORTFUNC Ogre::AxisAlignedBox* MANUALOBJECT_getBoundingBox( Ogre::ManualObject* o )
{
	return new Ogre::AxisAlignedBox( o->getBoundingBox() );
}

EXPORTFUNC float MANUALOBJECT_getBoundingRadius( Ogre::ManualObject* o )
{
	return o->getBoundingRadius();
}

EXPORTFUNC void MANUALOBJECT__updateRenderQueue( Ogre::RenderQueue* queue , Ogre::ManualObject* o )
{
	o->_updateRenderQueue( queue );
}

EXPORTFUNC Ogre::EdgeData* MANUALOBJECT_getEdgeList( Ogre::ManualObject* o )
{
	return o->getEdgeList();
}

EXPORTFUNC int MANUALOBJECT_hasEdgeList( Ogre::ManualObject* o )
{
	return o->hasEdgeList();
}

EXPORTFUNC Ogre::ShadowCaster::ShadowRenderableListIterator* MANUALOBJECT_getShadowVolumeRenderableIterator( int shadowTechnique , Ogre::Light* light , Ogre::HardwareIndexBuffer* indexBuffer, bool extrudeVertices , float extrusionDistance , int flags , Ogre::ManualObject* o )
{
	return new Ogre::ShadowCaster::ShadowRenderableListIterator(  o->getShadowVolumeRenderableIterator( (Ogre::ShadowTechnique)shadowTechnique , light , &Ogre::HardwareIndexBufferSharedPtr(indexBuffer), extrudeVertices , extrusionDistance, flags ) );
}

EXPORTFUNC void MANUALOBJECT_visitRenderables( Ogre::Renderable::Visitor* visitor , bool debugRenderables , Ogre::ManualObject* o )
{
	o->visitRenderables( visitor , debugRenderables );
}

EXPORTFUNC void MANUALOBJECT__notifyCreator( Ogre::MovableObjectFactory* fact , Ogre::ManualObject* o )
{
	o->_notifyCreator( fact );
}

EXPORTFUNC Ogre::MovableObjectFactory* MANUALOBJECT__getCreator( Ogre::ManualObject* o )
{
	return o->_getCreator();
}

EXPORTFUNC void MANUALOBJECT__notifyManager( Ogre::SceneManager* man , Ogre::ManualObject* o )
{
	o->_notifyManager( man );
}

EXPORTFUNC Ogre::SceneManager* MANUALOBJECT__getManager( Ogre::ManualObject* o )
{
	return o->_getManager();
}

EXPORTFUNC const char* MANUALOBJECT_getName( Ogre::ManualObject* o )
{
	return o->getName().c_str();
}

EXPORTFUNC Ogre::Node* MANUALOBJECT_getParentNode( Ogre::ManualObject* o )
{
	return o->getParentNode();
}

EXPORTFUNC Ogre::SceneNode* MANUALOBJECT_getParentSceneNode( Ogre::ManualObject* o )
{
	return o->getParentSceneNode();
}

EXPORTFUNC void MANUALOBJECT__notifyAttached( Ogre::Node* parent , bool isTagPoint , Ogre::ManualObject* o )
{
	o->_notifyAttached( parent , isTagPoint );
}

EXPORTFUNC bool MANUALOBJECT_isAttached( Ogre::ManualObject* o )
{
	return o->isAttached();
}

EXPORTFUNC void MANUALOBJECT_detatchFromParent( Ogre::ManualObject* o )
{
	o->detatchFromParent();
}

EXPORTFUNC bool MANUALOBJECT_isInScene( Ogre::ManualObject* o )
{
	return o->isInScene();
}

EXPORTFUNC void MANUALOBJECT__notifyMoved( Ogre::ManualObject* o )
{
	o->_notifyMoved();
}

EXPORTFUNC void MANUALOBJECT__notifyCurrentCamera( Ogre::Camera* cam , Ogre::ManualObject* o )
{
	o->_notifyCurrentCamera( cam );
}

EXPORTFUNC Ogre::AxisAlignedBox* MANUALOBJECT_getWorldBoundingBox( bool derive , Ogre::ManualObject* o )
{
	return new Ogre::AxisAlignedBox( o->getWorldBoundingBox( derive ) );
}


EXPORTFUNC Ogre::Sphere* MANUALOBJECT_getWorldBoundingSphere( bool derive , Ogre::ManualObject* o )
{
	return new Ogre::Sphere( o->getWorldBoundingSphere( derive ));
}


EXPORTFUNC void MANUALOBJECT_setVisible( bool visible , Ogre::ManualObject* o )
{
	o->setVisible( visible );
}

EXPORTFUNC bool MANUALOBJECT_getVisible( Ogre::ManualObject* o )
{
	return o->getVisible();
}

EXPORTFUNC bool MANUALOBJECT_isVisible( Ogre::ManualObject* o )
{
	return o->isVisible();
}

EXPORTFUNC void MANUALOBJECT_setRenderingDistance( float dist , Ogre::ManualObject* o )
{
	o->setRenderingDistance( dist );
}

EXPORTFUNC float MANUALOBJECT_getRenderingDistance( Ogre::ManualObject* o )
{
	return o->getRenderingDistance();
}

EXPORTFUNC void MANUALOBJECT_setUserObject( Ogre::UserDefinedObject* obj , Ogre::ManualObject* o )
{
	o->setUserObject( obj );
}

EXPORTFUNC Ogre::UserDefinedObject* MANUALOBJECT_getUserObject(  Ogre::ManualObject* o )
{
	return o->getUserObject();
}

// Ogre::Any Unsupported
/*
void MANUALOBJECT_setUserAny( Ogre::Any* anything , Ogre::ManualObject* o )
{
	o->setUserAny( *anything );
}

Ogre::Any* MANUALOBJECT_getUserAny( Ogre::ManualObject* o )
{
	return new Ogre::Any( o->getUserAny() );
}*/

EXPORTFUNC void MANUALOBJECT_setRenderQueueGroup( int queueID , Ogre::ManualObject* o )
{
	o->setRenderQueueGroup( queueID );
}

EXPORTFUNC unsigned int MANUALOBJECT_getRenderQueueGroup( Ogre::ManualObject* o )
{
	return o->getRenderQueueGroup();
}

EXPORTFUNC Ogre::Matrix4* MANUALOBJECT__getParentNodeFullTransform( Ogre::ManualObject* o )
{
	return new Ogre::Matrix4( o->_getParentNodeFullTransform() );
}

EXPORTFUNC void MANUALOBJECT_setQueryFlags( unsigned int flags , Ogre::ManualObject* o )
{
	o->setQueryFlags( flags );
}

EXPORTFUNC void MANUALOBJECT_addQueryFlags( unsigned int flags , Ogre::ManualObject* o )
{
	o->addQueryFlags( flags );
}

EXPORTFUNC void MANUALOBJECT_removeQueryFlags( unsigned int flags , Ogre::ManualObject* o )
{
	o->removeQueryFlags( flags );
}

EXPORTFUNC unsigned int MANUALOBJECT_getQueryFlags( Ogre::ManualObject* o )
{
	return o->getQueryFlags();
}

EXPORTFUNC void MANUALOBJECT_setVisibilityFlags( unsigned int flags , Ogre::ManualObject* o )
{
	o->setVisibilityFlags( flags );
}

EXPORTFUNC void MANUALOBJECT_addVisibilityFlags( unsigned int flags , Ogre::ManualObject* o )
{
	o->addVisibilityFlags( flags );
}

EXPORTFUNC void MANUALOBJECT_removeVisibilityFlags( unsigned int flags , Ogre::ManualObject* o )
{
	o->removeVisibilityFlags( flags );
}

EXPORTFUNC unsigned int MANUALOBJECT_getVisibilityFlags( Ogre::ManualObject* o )
{
	return o->getVisibilityFlags();
}

EXPORTFUNC void MANUALOBJECT_setListener( Ogre::MovableObject::Listener* listener , Ogre::ManualObject* o )
{
	o->setListener( listener );
}

EXPORTFUNC Ogre::MovableObject::Listener* MANUALOBJECT_getListener( Ogre::ManualObject* o )
{
	return o->getListener();
}

EXPORTFUNC Ogre::LightList* MANUALOBJECT_queryLights( Ogre::ManualObject* o )
{
	return new Ogre::LightList( o->queryLights() );
}

// virtual LightList * 	_getLightList ()			// Not neccessary, querylights is fine.


//Shadow Caster

EXPORTFUNC Ogre::AxisAlignedBox* MANUALOBJECT_getLightCapBounds( Ogre::ManualObject* o )
{
	return new Ogre::AxisAlignedBox( o->getLightCapBounds() );
}

EXPORTFUNC Ogre::AxisAlignedBox* MANUALOBJECT_getDarkCapBounds( Ogre::Light* light , float dirLightExtrusionDist , Ogre::ManualObject* o )
{
	return new Ogre::AxisAlignedBox( o->getDarkCapBounds( *light , dirLightExtrusionDist ) );
}

EXPORTFUNC void MANUALOBJECT_setCastShadows( bool enabled , Ogre::ManualObject* o )
{
	o->setCastShadows( enabled );
}

EXPORTFUNC int MANUALOBJECT_getCastShadows( Ogre::ManualObject* o )
{
	return o->getCastShadows();
}

EXPORTFUNC float MANUALOBJECT_getPointExtrusionDistance( Ogre::Light* l , Ogre::ManualObject* o )
{
	return o->getPointExtrusionDistance( l );
}

// MovableObject

EXPORTFUNC unsigned int MANUALOBJECT_getTypeFlags( Ogre::ManualObject* o )
{
	return o->getTypeFlags();
}

EXPORTFUNC void MANUALOBJECT_setDebugDisplayEnabled( bool enabled , Ogre::ManualObject* o )
{
	o->setDebugDisplayEnabled( enabled );
}

EXPORTFUNC bool MANUALOBJECT_isDebugDisplayEnabled( Ogre::ManualObject* o )
{
	return o->isDebugDisplayEnabled();
}



// Static MovableObject

EXPORTFUNC void MANUALOBJECT_setDefaultQueryFlags( unsigned int flags )
{
	Ogre::ManualObject::setDefaultQueryFlags( flags );
}

EXPORTFUNC unsigned int MANUALOBJECT_getDefaultQueryFlags( )
{
	return Ogre::ManualObject::getDefaultQueryFlags();
}

EXPORTFUNC void MANUALOBJECT_setDefaultVisibilityFlags( unsigned int flags )
{
	Ogre::ManualObject::setDefaultVisibilityFlags( flags );
}

EXPORTFUNC unsigned int MANUALOBJECT_getDefaultVisibilityFlags()
{
	return Ogre::ManualObject::getDefaultVisibilityFlags();
}

//Static ShadowCaster
EXPORTFUNC void MANUALOBJECT_extrudeVertices( Ogre::HardwareVertexBufferSharedPtr* vertexBuffer , long originalVertexCount , Ogre::Vector4* lightPos , float extrudeDist )
{
	Ogre::ManualObject::extrudeVertices( *vertexBuffer , originalVertexCount , *lightPos , extrudeDist );
}




#endif
