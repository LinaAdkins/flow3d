#ifndef H_TOGREKEYFRAME
#define H_TOGREKEYFRAME

// Include our children classes
#include "TTransformKeyFrame.h"


/* 
Manual constructor/destructor not supported, only maanged insances allowed

KeyFrame (const AnimationTrack *parent, Real time) 
virtual 	~KeyFrame ()
*/

EXPORTFUNC float OGREKEYFRAME_getTime( Ogre::KeyFrame* f )
{
	return f->getTime();
}

EXPORTFUNC Ogre::KeyFrame* OGREKEYFRAME__clone( Ogre::AnimationTrack* newParent , Ogre::KeyFrame* f )
{
	return f->_clone( newParent );
}


/*

	Ogre Alloc overloads not necessary

void * 	operator new (size_t sz, const char *file, int line, const char *func)
void * 	operator new (size_t sz)
void * 	operator new (size_t sz, void *ptr)
void * 	operator new[] (size_t sz, const char *file, int line, const char *func)
void * 	operator new[] (size_t sz)
void 	operator delete (void *ptr)
void 	operator delete (void *ptr, const char *, int, const char *)
void 	operator delete[] (void *ptr)
void 	operator delete[] (void *ptr, const char *, int, const char *)
*/

#endif
