#ifndef H_PASS
#define H_PASS

/*
TextureUnitState * 	createTextureUnitState (void)
*/

EXPORTFUNC Ogre::TextureUnitState* PASS_createTextureUnitState( const char* textureName , int texCoordSet , Ogre::Pass* p )
{
	return p->createTextureUnitState( textureName , texCoordSet );
}
/*
void 	addTextureUnitState (TextureUnitState *state)
*/

EXPORTFUNC  Ogre::TextureUnitState* PASS_getTextureUnitState( int index , Ogre::Pass* p )
{
	return p->getTextureUnitState( index );
}

EXPORTFUNC Ogre::TextureUnitState* PASS_getTextureUnitStateWithName( const char* name , Ogre::Pass* p )
{
	return p->getTextureUnitState( name );
}

/*
// Const versions aren't necessary
const TextureUnitState * 	getTextureUnitState (unsigned short index) const
const TextureUnitState * 	getTextureUnitState (const String &name) const
*/

EXPORTFUNC int PASS_getTextureUnitStateIndex( Ogre::TextureUnitState* state , Ogre::Pass* p )
{
	return p->getTextureUnitStateIndex( state );
}

/*
TextureUnitStateIterator 	getTextureUnitStateIterator (void)
ConstTextureUnitStateIterator 	getTextureUnitStateIterator (void) const
*/

EXPORTFUNC void PASS_removeTextureUnitState( int index , Ogre::Pass* p )
{
	p->removeTextureUnitState( index );
}
/*
void 	removeAllTextureUnitStates (void)
*/

EXPORTFUNC int PASS_getNumTextureUnitStates( Ogre::Pass* p )
{
	return p->getNumTextureUnitStates();
}

EXPORTFUNC void PASS_setSceneBlending( int sbt , Ogre::Pass* p )
{
	p->setSceneBlending( Ogre::SceneBlendType( sbt ) );
}

EXPORTFUNC void PASS_setAlphaRejectSettings( int func , int value , bool alphaToCoverageEnabled , Ogre::Pass* p )
{
	p->setAlphaRejectSettings( Ogre::CompareFunction(func) , value , alphaToCoverageEnabled );
}

EXPORTFUNC void PASS_setCullingMode( int mode , Ogre::Pass* p )
{
	p->setCullingMode( Ogre::CullingMode( mode ) );
}

EXPORTFUNC void PASS_setManualCullingMode( int mode , Ogre::Pass* p )
{
	p->setManualCullingMode( Ogre::ManualCullingMode( mode ) );
}

EXPORTFUNC void PASS_setVertexProgram( const char* name , bool resetParams , Ogre::Pass* p )
{
	p->setVertexProgram( name , resetParams );
}

EXPORTFUNC Ogre::GpuProgramParameters* PASS_getVertexProgramParameters( Ogre::Pass* p )
{
	return p->getVertexProgramParameters().getPointer();
}

#endif