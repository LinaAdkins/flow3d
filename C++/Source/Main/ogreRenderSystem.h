#ifndef H_OGRERENDERSYSTEM
#define H_OGRERENDERSYSTEM

EXPORTFUNC const char* RS_getName( Ogre::RenderSystem* ogreRenderSystem )
{
	return ogreRenderSystem->getName().c_str();
}

//getConfigOptions - returns list

EXPORTFUNC void RS_setConfigOption( const char* name , const char* value , Ogre::RenderSystem* ogreRenderSystem )
{
	ogreRenderSystem->setConfigOption( name , value );
}

EXPORTFUNC Ogre::HardwareOcclusionQuery* RS_createHardwareOcclusionQuery( Ogre::RenderSystem* ogreRenderSystem )
{
	return ogreRenderSystem->createHardwareOcclusionQuery();
}

EXPORTFUNC void RS_destroyHardwareOcclusionQuery( Ogre::HardwareOcclusionQuery* ogreHOQ , Ogre::RenderSystem* ogreRenderSystem )
{
	ogreRenderSystem->destroyHardwareOcclusionQuery( ogreHOQ );
}

EXPORTFUNC const char* RS_validateConfigOptions( Ogre::RenderSystem* ogreRenderSystem )
{
	return ogreRenderSystem->validateConfigOptions().c_str();
}

EXPORTFUNC Ogre::RenderWindow* RS__initialise( bool autoCreateWindow , const char* windowTitle , Ogre::RenderSystem* ogreRenderSystem )
{
	return ogreRenderSystem->_initialise( autoCreateWindow , windowTitle );
}

EXPORTFUNC void RS__setCullingMode( int mode , Ogre::RenderSystem* ogreRenderSystem )
{
	ogreRenderSystem->_setCullingMode( (Ogre::CullingMode)mode );
}

EXPORTFUNC void RS__setViewport( Ogre::Viewport* vp , Ogre::RenderSystem* s )
{
	s->_setViewport( vp );
}

EXPORTFUNC void RS_clearFrameBuffer( int buffers, Ogre::ColourValue* colour , float depth, int stencil , Ogre::RenderSystem* s )
{
	s->clearFrameBuffer( buffers , *colour , depth , stencil );
}




#endif
