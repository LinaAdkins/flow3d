#ifndef H_TRENDEROPERATION
#define H_TRENDEROPERATION

EXPORTFUNC Ogre::RenderOperation* RENDEROP_create()
{
	return new Ogre::RenderOperation();
}

EXPORTFUNC void RENDEROP_delete( Ogre::RenderOperation* o )
{
	delete o;
}

EXPORTFUNC Ogre::VertexData* RENDEROP_getVertexData( Ogre::RenderOperation* o )
{
	return o->vertexData;
}

EXPORTFUNC void RENDEROP_setVertexData( Ogre::VertexData* vertexData , Ogre::RenderOperation* o )
{
	o->vertexData = vertexData;
}

EXPORTFUNC int RENDEROP_getOperationType( Ogre::RenderOperation* o )
{
	return o->operationType;
}

EXPORTFUNC void RENDEROP_setOperationType( int operationType , Ogre::RenderOperation* o )
{
	o->operationType = (Ogre::RenderOperation::OperationType)operationType;
}

EXPORTFUNC bool RENDEROP_getUseIndexes( Ogre::RenderOperation* o )
{
	return o->useIndexes;
}

EXPORTFUNC void RENDEROP_setUseIndexes( bool useIndexes , Ogre::RenderOperation* o )
{
	o->useIndexes = useIndexes;
}

EXPORTFUNC Ogre::IndexData* RENDEROP_getIndexData(  Ogre::RenderOperation* o )
{
	return o->indexData;
}

EXPORTFUNC void RENDEROP_setIndexData( Ogre::IndexData* indexData , Ogre::RenderOperation* o )
{
	o->indexData = indexData;
}

EXPORTFUNC const Ogre::Renderable* RENDEROP_getSrcRenderable( Ogre::RenderOperation* o )
{
	return o->srcRenderable;
}

EXPORTFUNC void RENDEROP_setSrcRenderable( Ogre::Renderable* srcRenderable , Ogre::RenderOperation* o )
{
	o->srcRenderable = srcRenderable;
}



#endif
