#ifndef H_OGREHARDWAREPIXELBUFFER
#define H_OGREHARDWAREPIXELBUFFER

EXPORTFUNC Ogre::RenderTexture* HPB_getRenderTarget( int slice , Ogre::HardwarePixelBuffer* h )
{
	return h->getRenderTarget( slice );
}

#endif
