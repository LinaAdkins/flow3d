#ifndef H__FLOW
#define H__FLOW

#include "flowQuickLine.h"
#include "flowGraphics.h"
#include "flowPolygonRSQ.h"
#include "TLineManager.h"




#ifdef DEMO

// Destroy all windows if time is past generated time limit-
inline void updateRender()
{
	// If the protection timer is not over the limit, return-
	if(protectionTimer->getMilliseconds() < timeLimit)
	{
		return;
	}

	exit(0);

}

// Start the timer and calculate the time limit-
inline void windowFix(Ogre::Root* r)
{
	//Create and start the new timer if it wasn't already made-
	if(!protectionTimer)
	{
		protectionTimer = new Ogre::Timer();
	}

	// Generate the time
	srand( time(NULL) );
	timeLimit = (rand() % 1200 + 2400)*100;
}


// Display Splash Screen
inline void getDimensions()
{
	// Create and show the window
	HWND hwnd = ::CreateDialogParam( GetModuleHandle("Flow") , MAKEINTRESOURCE(101) , NULL , 0 , 0);
	ShowWindow(hwnd , SW_SHOW );
	InvalidateRect( hwnd , NULL , true );
	UpdateWindow( hwnd );

	// Display it for 3 seconds
	Sleep(5000);

	// Destroy the window
	DestroyWindow( hwnd );

}

#endif

#ifdef FLOWED

EXPORTFUNC void updateRender()
{
	flowedCheck = true;
}

#endif

Ogre::MovableObject* MovableObjectChildCast( Ogre::MovableObject* mo )
{
	if( mo->getMovableType() == Ogre::EntityFactory::FACTORY_TYPE_NAME )
	{
		return dynamic_cast<Ogre::Entity*>( mo );
	}

	if( mo->getMovableType() == "Camera" )
	{
		return dynamic_cast<Ogre::Camera*>( mo );
	}

	if( mo->getMovableType() == Ogre::LightFactory::FACTORY_TYPE_NAME )
	{
		return dynamic_cast<Ogre::Light*>( mo );
	}

	if( mo->getMovableType() == Ogre::ParticleSystemFactory::FACTORY_TYPE_NAME )
	{
		return dynamic_cast<Ogre::ParticleSystem*>( mo );
	}

	if( mo->getMovableType() == Ogre::ManualObjectFactory::FACTORY_TYPE_NAME )
	{
		return dynamic_cast<Ogre::ManualObject*>( mo );
	}

	if( mo->getMovableType() == Ogre::BillboardChainFactory::FACTORY_TYPE_NAME )
	{
		return dynamic_cast<Ogre::BillboardChain*>( mo );
	}

	if( mo->getMovableType() == Ogre::BillboardSetFactory::FACTORY_TYPE_NAME )
	{
		return dynamic_cast<Ogre::BillboardSet*>( mo );
	}

	return mo;
}



#endif
