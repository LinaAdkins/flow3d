#ifndef H_OGRECOLOURVALUE
#define H_OGRECOLOURVALUE

EXPORTFUNC Ogre::ColourValue* COLOURVALUE_create( float red, float green , float blue , float alpha )
{
	return new Ogre::ColourValue( red , green , blue , alpha );
}

EXPORTFUNC void COLOURVALUE_delete( Ogre::ColourValue* c )
{
	delete c;
}

EXPORTFUNC float COLOURVALUE_getR( Ogre::ColourValue* c )
{
	return c->r;
}

EXPORTFUNC void COLOURVALUE_setR( float r , Ogre::ColourValue* c )
{
	c->r = r;
}

EXPORTFUNC float COLOURVALUE_getG( Ogre::ColourValue* c )
{
	return c->g;
}

EXPORTFUNC void COLOURVALUE_setG( float g , Ogre::ColourValue* c )
{
	c->g = g;
}

EXPORTFUNC float COLOURVALUE_getB( Ogre::ColourValue* c )
{
	return c->b;
}

EXPORTFUNC void COLOURVALUE_setB( float b , Ogre::ColourValue* c )
{
	c->b = b;
}

EXPORTFUNC float COLOURVALUE_getA( Ogre::ColourValue* c )
{
	return c->a;
}

EXPORTFUNC void COLOURVALUE_setA( float a , Ogre::ColourValue* c )
{
	c->a = a;
}

EXPORTFUNC void COLOURVALUE_setAll( float r , float g, float b , float a , Ogre::ColourValue* c )
{
	c->r = r;
	c->g = g;
	c->b = b;
	c->a = a;
}

EXPORTFUNC const char* COLOURVALUE_toStr( Ogre::ColourValue* c )
{
	std::ostringstream os;
	os << *c;
	tempString = os.str();
	return tempString.c_str();
}

#endif
