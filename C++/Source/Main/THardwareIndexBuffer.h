#ifndef H__THARDWAREINDEXBUFFER
#define H__THARDWAREINDEXBUFFER

EXPORTFUNC int HIB_getSizeInBytes( Ogre::HardwareIndexBufferSharedPtr* b )
{
	return (*b)->getSizeInBytes();
}

EXPORTFUNC void HIB_writeDataWithIndexList( int offset , int length , std::vector<int>* pSource , bool discardWholeBuffer , Ogre::HardwareIndexBufferSharedPtr* b )
{
	(*b)->writeData( offset , length , &(*pSource)[0] , discardWholeBuffer );
}

EXPORTFUNC void HIB_delete( Ogre::HardwareIndexBufferSharedPtr* b )
{
	delete b;
}

#endif
