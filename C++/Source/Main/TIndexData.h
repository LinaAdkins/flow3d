#ifndef H_TINDEXDATA
#define H_TINDEXDATA

EXPORTFUNC void INDEXDATA_setIndexBuffer( Ogre::HardwareIndexBufferSharedPtr* buffer , Ogre::IndexData* d )
{
	d->indexBuffer = (*buffer);
}

EXPORTFUNC void INDEXDATA_setIndexStart( int indexStart , Ogre::IndexData* d )
{
	d->indexStart = indexStart;
}

EXPORTFUNC void INDEXDATA_setIndexCount( int indexCount , Ogre::IndexData* d )
{
	d->indexCount = indexCount;
}

#endif
