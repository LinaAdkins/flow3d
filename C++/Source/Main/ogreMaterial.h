#ifndef H_OGREMATERIAL
#define H_OGREMATERIAL

/* Constructor not necessary, materials are managed
Ogre::MaterialPtr* MATERIAL_create( ... )*/

// Used for deleting ptr instances, not actual materials.
EXPORTFUNC void MATERIAL_delete( Ogre::MaterialPtr* m )
{
	delete m;
}

EXPORTFUNC void MATERIAL_eq( Ogre::MaterialPtr* a , Ogre::MaterialPtr* m )
{
	*m=*a;
}

EXPORTFUNC int MATERIAL_isTransparent( Ogre::Material* ogreMaterial )
{
	return ogreMaterial->isTransparent();
}

EXPORTFUNC void MATERIAL_setReceiveShadows( bool enabled , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setReceiveShadows( enabled );
}
EXPORTFUNC int MATERIAL_getReceiveShadows( Ogre::Material* ogreMaterial )
{
	return ogreMaterial->getReceiveShadows();
}

EXPORTFUNC void MATERIAL_setTransparencyCastsShadows( bool enabled , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setTransparencyCastsShadows( enabled );
}

EXPORTFUNC int MATERIAL_getTransparencyCastsShadows( Ogre::Material* ogreMaterial )
{
	return ogreMaterial->getTransparencyCastsShadows();
}

EXPORTFUNC Ogre::Technique* MATERIAL_createTechnique( Ogre::Material* ogreMaterial )
{
	return ogreMaterial->createTechnique();
}

EXPORTFUNC Ogre::Technique* MATERIAL_getTechnique( const char* name , Ogre::Material* ogreMaterial )
{
	return ogreMaterial->getTechnique( name );
}

EXPORTFUNC Ogre::Technique* MATERIAL_getTechniqueWithIndex( int index ,Ogre::MaterialPtr* m )
{
	return (*m)->getTechnique( index );
}

EXPORTFUNC int MATERIAL_getNumTechniques( Ogre::Material* ogreMaterial )
{
	return ogreMaterial->getNumTechniques();
}

EXPORTFUNC void MATERIAL_removeTechnique( int index , Ogre::Material* ogreMaterial )
{
	ogreMaterial->removeTechnique( index );
}

EXPORTFUNC void MATERIAL_removeAllTechniques( Ogre::Material* ogreMaterial )
{
	ogreMaterial->removeAllTechniques();
}

EXPORTFUNC Ogre::Material::TechniqueIterator* MATERIAL_getTechniqueIterator( Ogre::Material* ogreMaterial )
{
	Ogre::Material::TechniqueIterator* ti = new Ogre::Material::TechniqueIterator( ogreMaterial->getTechniqueIterator() );
	return ti;
}

EXPORTFUNC Ogre::Material::TechniqueIterator* MATERIAL_getSupportedTechniqueIterator( Ogre::Material* ogreMaterial )
{
	Ogre::Material::TechniqueIterator* ti = new Ogre::Material::TechniqueIterator( ogreMaterial->getSupportedTechniqueIterator() );
	return ti;
}

EXPORTFUNC Ogre::Technique* MATERIAL_getSupportedTechnique( int index , Ogre::Material* ogreMaterial )
{
	return ogreMaterial->getSupportedTechnique( index );
}

EXPORTFUNC int MATERIAL_getNumSupportedTechniques( Ogre::Material* ogreMaterial )
{
	return ogreMaterial->getNumSupportedTechniques();
}

EXPORTFUNC const char* MATERIAL_getUnsupportedTechniquesExplanation( Ogre::Material* ogreMaterial )
{
	return ogreMaterial->getUnsupportedTechniquesExplanation().c_str();
}

EXPORTFUNC int MATERIAL_getNumLodLevels( const char* schemeName , Ogre::Material* ogreMaterial )
{
	return ogreMaterial->getNumLodLevels( schemeName );
}

EXPORTFUNC int MATERIAL_getNumLodLevelsWithIndex( int schemeIndex , Ogre::Material* ogreMaterial )
{
	return ogreMaterial->getNumLodLevels( schemeIndex );
}

EXPORTFUNC Ogre::Technique* MATERIAL_getBestTechnique( int lodIndex , Ogre::Material* ogreMaterial )
{
	return ogreMaterial->getBestTechnique( lodIndex );
}

EXPORTFUNC Ogre::MaterialPtr* MATERIAL_clone( const char* newName , bool changeGroup , const char* newGroup , Ogre::MaterialPtr* m )
{
	return new Ogre::MaterialPtr( (*m)->clone( newName , changeGroup , newGroup ) );
}

EXPORTFUNC void MATERIAL_copyDetailsTo( Ogre::Material* mat , Ogre::Material* ogreMaterial )
{
	ogreMaterial->copyDetailsTo( Ogre::MaterialPtr(mat) );
}

EXPORTFUNC void MATERIAL_compile( bool autoManageTextureUnits , Ogre::Material* ogreMaterial )
{
	ogreMaterial->compile( autoManageTextureUnits );
}

EXPORTFUNC void MATERIAL_setPointSize( float ps , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setPointSize( ps );
}

EXPORTFUNC void MATERIAL_setAmbient( float red, float green, float blue , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setAmbient( red , green , blue );
}

EXPORTFUNC void MATERIAL_setAmbientWithColourValue( Ogre::ColourValue* ogreColourValue , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setAmbient( *ogreColourValue );
}

EXPORTFUNC void MATERIAL_setDiffuse( float red , float green, float blue, float alpha, Ogre::Material* ogreMaterial )
{
	ogreMaterial->setDiffuse( red, green , blue, alpha );
}

EXPORTFUNC void MATERIAL_setDiffuseWithColourValue( Ogre::ColourValue* ogreColourValue , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setDiffuse( *ogreColourValue );
}
EXPORTFUNC void MATERIAL_setSpecular( float red, float green, float blue, float alpha, Ogre::Material* ogreMaterial )
{
	ogreMaterial->setSpecular( red , green , blue , alpha );
}

EXPORTFUNC void MATERIAL_setSpecularWithColourValue( Ogre::ColourValue* ogreColourValue , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setSpecular( *ogreColourValue );
}

EXPORTFUNC void MATERIAL_setShininess( float val , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setShininess( val );
}

EXPORTFUNC void MATERIAL_setSelfIllumination( float red , float green, float blue , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setSelfIllumination( red, green , blue );
}

EXPORTFUNC void MATERIAL_setSelfIlluminationWithColourValue( Ogre::ColourValue* ogreColourValue , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setSelfIllumination( *ogreColourValue );
}

EXPORTFUNC void MATERIAL_setDepthCheckEnabled( bool enabled , Ogre::MaterialPtr* m )
{
	(*m)->setDepthCheckEnabled( enabled );
}

EXPORTFUNC void MATERIAL_setDepthWriteEnabled( bool enabled , Ogre::MaterialPtr* m )
{
	(*m)->setDepthWriteEnabled( enabled );
}

EXPORTFUNC void MATERIAL_setDepthFunction( int func , Ogre::MaterialPtr* m )
{
	(*m)->setDepthFunction( Ogre::CompareFunction( func ) );
}

EXPORTFUNC void MATERIAL_setColourWriteEnabled( bool enabled , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setColourWriteEnabled( enabled );
}

EXPORTFUNC void MATERIAL_setCullingMode( int mode , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setCullingMode( (Ogre::CullingMode)mode );
}

EXPORTFUNC void MATERIAL_setManualCullingMode( int mode , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setManualCullingMode( (Ogre::ManualCullingMode)mode );
}

EXPORTFUNC void MATERIAL_setLightingEnabled( bool enabled, Ogre::Material* ogreMaterial )
{
	ogreMaterial->setLightingEnabled( enabled );
}

EXPORTFUNC void MATERIAL_setShadingMode( int mode , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setShadingMode( (Ogre::ShadeOptions)mode );
}

EXPORTFUNC void MATERIAL_setFog( bool overrideScene , int mode , Ogre::ColourValue* ogreColourValue , float expDensity , float linearStart , float linearEnd, Ogre::Material* ogreMaterial )
{
	ogreMaterial->setFog( overrideScene , (Ogre::FogMode) mode , *ogreColourValue , expDensity , linearStart , linearEnd );
}

EXPORTFUNC void MATERIAL_setDepthBias( float constantBias , float slopeScaleBias , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setDepthBias( constantBias , slopeScaleBias );
}

EXPORTFUNC void MATERIAL_setTextureFiltering( int filterType , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setTextureFiltering( (Ogre::TextureFilterOptions) filterType );
}

EXPORTFUNC void MATERIAL_setTextureAnisotropy( int maxAniso , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setTextureAnisotropy( maxAniso );
}


EXPORTFUNC void MATERIAL_setSceneBlending( int sbt , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setSceneBlending( (Ogre::SceneBlendType) sbt );
}

EXPORTFUNC void MATERIAL_setSceneBlendingWithFactors( int sourceFactor , int destFactor , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setSceneBlending( (Ogre::SceneBlendFactor) sourceFactor , (Ogre::SceneBlendFactor) destFactor );
}

EXPORTFUNC void MATERIAL__notifyNeedsRecompile( Ogre::Material* ogreMaterial )
{
	ogreMaterial->_notifyNeedsRecompile();
}
EXPORTFUNC void MATERIAL_setLodLevels( Ogre::Material::LodDistanceList* lodDistances , Ogre::Material* ogreMaterial )
{
	ogreMaterial->setLodLevels( *lodDistances );
}

EXPORTFUNC Ogre::Material::LodDistanceIterator* MATERIAL_getLodDistanceIterator( Ogre::Material* ogreMaterial )
{
	Ogre::Material::LodDistanceIterator* ldi = new Ogre::Material::LodDistanceIterator( ogreMaterial->getLodDistanceIterator() );
	return ldi;
}

EXPORTFUNC int MATERIAL_getLodIndex( float d , Ogre::Material* ogreMaterial )
{
	return ogreMaterial->getLodIndex( d );
}

EXPORTFUNC int MATERIAL_getLodIndexSquaredDepth( float squaredDepth , Ogre::Material* ogreMaterial )
{
	return ogreMaterial->getLodIndexSquaredDepth( squaredDepth );
}

EXPORTFUNC int MATERIAL_applyTextureAliases( Ogre::AliasTextureNamePairList* aliasList , bool apply , Ogre::Material* ogreMaterial )
{
	return ogreMaterial->applyTextureAliases( *aliasList , apply );
}

EXPORTFUNC int MATERIAL_getCompilationRequired( Ogre::Material* ogreMaterial )
{
	return ogreMaterial->getCompilationRequired();
}

// TResource

EXPORTFUNC const char* MATERIAL_getName( Ogre::MaterialPtr* m )
{
	tempString = (*m)->getName();
	return tempString.c_str();
}

#endif
