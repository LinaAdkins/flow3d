#ifndef H_OGREANIMABLEOBJECT
#define H_OGREANIMABLEOBJECT

EXPORTFUNC Ogre::AnimableObject* AO_create()
{
	Ogre::AnimableObject* ao = new Ogre::AnimableObject();
	return ao;
}

EXPORTFUNC void AO_delete( Ogre::AnimableObject* ao )
{
	delete ao;
}

EXPORTFUNC Ogre::StringVector* AO_getAnimableValueNames( Ogre::AnimableObject* ao )
{
	Ogre::StringVector* ogreStringVector = new Ogre::StringVector( ao->getAnimableValueNames() );
	return ogreStringVector;
}

EXPORTFUNC Ogre::AnimableValue* AO_createAnimableValue( const char* valueName , Ogre::AnimableObject* ao )
{
	return ao->createAnimableValue( valueName ).getPointer();
}



#endif
