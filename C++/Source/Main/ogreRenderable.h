#ifndef H_OGRERENDERABLE
#define H_OGRERENDERABLE

// 	Renderable () - Not required, class is abstract.
// virtual 	~Renderable () - Not required, class is abstract.

// Adapted to use hard pointer refrences-
EXPORTFUNC Ogre::MaterialPtr* RENDERABLE_getMaterial( Ogre::Renderable* ogreRenderable )
{
	return new Ogre::MaterialPtr( ogreRenderable->getMaterial() );
}

EXPORTFUNC Ogre::Technique* RENDERABLE_getTechnique( Ogre::Renderable* ogreRenderable )
{
	return ogreRenderable->getTechnique();
}

EXPORTFUNC void RENDERABLE_getRenderOperation( Ogre::RenderOperation* op , Ogre::Renderable* ogreRenderable )
{
	ogreRenderable->getRenderOperation( *op );
}

EXPORTFUNC void RENDERABLE_getWorldTransforms( Ogre::Matrix4* xform , Ogre::Renderable* ogreRenderable )
{
	ogreRenderable->getWorldTransforms( xform );
}

EXPORTFUNC int RENDERABLE_getNumWorldTransforms( Ogre::Renderable* ogreRenderable )
{
	return ogreRenderable->getNumWorldTransforms();
}

EXPORTFUNC void RENDERABLE_setUseIdentityProjection( bool useIdentityProjection , Ogre::Renderable* ogreRenderable )
{
	ogreRenderable->setUseIdentityProjection( useIdentityProjection );
}

EXPORTFUNC int RENDERABLE_getUseIdentityProjection( Ogre::Renderable* ogreRenderable )
{
	return ogreRenderable->getUseIdentityProjection();
}

EXPORTFUNC void RENDERABLE_setUseIdentityView( bool useIdentityView , Ogre::Renderable* ogreRenderable )
{
	return ogreRenderable->setUseIdentityView( useIdentityView );
}

EXPORTFUNC int RENDERABLE_getUseIdentityView( Ogre::Renderable* ogreRenderable )
{
	return ogreRenderable->getUseIdentityView();
}

EXPORTFUNC float RENDERABLE_getSquaredViewDepth( Ogre::Camera* ogreCamera , Ogre::Renderable* ogreRenderable )
{
	return ogreRenderable->getSquaredViewDepth( ogreCamera );
}

EXPORTFUNC Ogre::LightList* RENDERABLE_getLights( Ogre::Renderable* ogreRenderable )
{
	Ogre::LightList* li = new Ogre::LightList( ogreRenderable->getLights() );
	return li;
}

EXPORTFUNC int RENDERABLE_getCastsShadows( Ogre::Renderable* ogreRenderable )
{
	return ogreRenderable->getCastsShadows();
}

EXPORTFUNC void RENDERABLE_setCustomParameter( int index , Ogre::Vector4* value , Ogre::Renderable* ogreRenderable )
{
	ogreRenderable->setCustomParameter( 0 , *value );
}

EXPORTFUNC Ogre::Vector4* RENDERABLE_getCustomParameter( int index , Ogre::Renderable* ogreRenderable )
{
	return new Ogre::Vector4( ogreRenderable->getCustomParameter( index ) );
}

EXPORTFUNC void RENDERABLE__updateCustomGpuParameter( Ogre::GpuProgramParameters::AutoConstantEntry* constantEntry , Ogre::GpuProgramParameters* params , Ogre::Renderable* ogreRenderable )
{
	ogreRenderable->_updateCustomGpuParameter( *constantEntry , params );
}

EXPORTFUNC void RENDERABLE_setPolygonModeOverrideable( bool overrideable , Ogre::Renderable* ogreRenderable )
{
	ogreRenderable->setPolygonModeOverrideable( overrideable );
}

EXPORTFUNC int RENDERABLE_getPolygonModeOverrideable( Ogre::Renderable* ogreRenderable )
{
	return ogreRenderable->getPolygonModeOverrideable();
}

















#endif
