#ifndef H_OGRELIGHT
#define H_OGRELIGHT

EXPORTFUNC void LIGHT_setType( int LightType , Ogre::Light* ogreLight )
{
	ogreLight->setType( (Ogre::Light::LightTypes) LightType );
}

EXPORTFUNC int LIGHT_getType( Ogre::Light* ogreLight )
{
	return ogreLight->getType();
}

EXPORTFUNC void LIGHT_setDiffuseColour( float r , float g , float b , Ogre::Light* ogreLight  )
{
	ogreLight->setDiffuseColour( r , g , b );
}

EXPORTFUNC void LIGHT_setDiffuseColourWithColourValue( Ogre::ColourValue* colour , Ogre::Light* ogreLight  )
{
	ogreLight->setDiffuseColour( *colour );
}

EXPORTFUNC Ogre::ColourValue* LIGHT_getDiffuseColour( Ogre::Light* ogreLight )
{
	return new Ogre::ColourValue( ogreLight->getDiffuseColour() );
}

EXPORTFUNC void LIGHT_setSpecularColour( float r , float g , float b , Ogre::Light* ogreLight   )
{
	ogreLight->setSpecularColour( r , g , b );
}

EXPORTFUNC void LIGHT_setSpecularColourWithColourValue( Ogre::ColourValue* colour , Ogre::Light* ogreLight )
{
	ogreLight->setSpecularColour( *colour );
}

EXPORTFUNC Ogre::ColourValue* LIGHT_getSpecularColour( Ogre::Light* ogreLight )
{
	return new Ogre::ColourValue(ogreLight->getSpecularColour());
}

EXPORTFUNC void LIGHT_setAttenuation( float range , float constant , float linear , float quadratic , Ogre::Light* ogreLight )
{
	ogreLight->setAttenuation( range , constant , linear , quadratic );
}

EXPORTFUNC float LIGHT_getAttenuationRange( Ogre::Light* ogreLight )
{
	return ogreLight->getAttenuationRange();
}

EXPORTFUNC float LIGHT_getAttenuationConstant( Ogre::Light* ogreLight )
{
	return ogreLight->getAttenuationConstant();
}

EXPORTFUNC float LIGHT_getAttenuationLinear( Ogre::Light* ogreLight )
{
	return ogreLight->getAttenuationLinear();
}

EXPORTFUNC float LIGHT_getAttenuationQuadric( Ogre::Light* ogreLight )
{
	return ogreLight->getAttenuationQuadric();
}

EXPORTFUNC void LIGHT_setPosition( float x , float y , float z , Ogre::Light* ogreLight )
{
	ogreLight->setPosition( x , y , z );
}

EXPORTFUNC void LIGHT_setPositionWithVector3( Ogre::Vector3* vec , Ogre::Light* ogreLight )
{
	ogreLight->setPosition( *vec );
}

EXPORTFUNC Ogre::Vector3* LIGHT_getPosition( Ogre::Light* ogreLight )
{
	return new Ogre::Vector3( ogreLight->getPosition() );
}

EXPORTFUNC void LIGHT_setDirection( float x , float y , float z , Ogre::Light* ogreLight  )
{
	ogreLight->setDirection( x , y , z );
}

EXPORTFUNC void LIGHT_setDirectionWithVector3( Ogre::Vector3* vec , Ogre::Light* ogreLight )
{
	ogreLight->setDirection( *vec );
}

EXPORTFUNC Ogre::Vector3* LIGHT_getDirection( Ogre::Light* ogreLight )
{
	return new Ogre::Vector3( ogreLight->getDirection() );
}

EXPORTFUNC void LIGHT_setSpotlightRange( Ogre::Radian* innerAngle , Ogre::Radian* outerAngle , float falloff , Ogre::Light* ogreLight )
{
	ogreLight->setSpotlightRange( *innerAngle , *outerAngle , falloff );
}

EXPORTFUNC Ogre::Radian* LIGHT_getSpotlightInnerAngle( Ogre::Light* l )
{
	return new Ogre::Radian(l->getSpotlightInnerAngle());
}

EXPORTFUNC Ogre::Radian* LIGHT_getSpotlightOuterAngle( Ogre::Light* l )
{
	return new Ogre::Radian(l->getSpotlightOuterAngle());
}

EXPORTFUNC float LIGHT_getSpotlightFalloff( Ogre::Light* ogreLight )
{
	return ogreLight->getSpotlightFalloff();
}

EXPORTFUNC void LIGHT_setSpotlightInnerAngle( Ogre::Radian* val , Ogre::Light* ogreLight )
{
	ogreLight->setSpotlightInnerAngle( *val );
}

EXPORTFUNC void LIGHT_setSpotlightOuterAngle( Ogre::Radian* val , Ogre::Light* ogreLight )
{
	ogreLight->setSpotlightOuterAngle( *val );
}

EXPORTFUNC void LIGHT_setSpotlightFalloff( float val , Ogre::Light* ogreLight )
{
	ogreLight->setSpotlightFalloff( val );
}

EXPORTFUNC void LIGHT_setPowerScale( float power , Ogre::Light* ogreLight )
{
	ogreLight->setPowerScale( power );
}

EXPORTFUNC float LIGHT_getPowerScale( Ogre::Light* ogreLight )
{
	return ogreLight->getPowerScale();
}

EXPORTFUNC void LIGHT__notifyAttached( Ogre::Node* parent , bool isTagPoint , Ogre::Light* ogreLight )
{
	ogreLight->_notifyAttached( parent , isTagPoint );
}

EXPORTFUNC void LIGHT__notifyMoved( Ogre::Light* ogreLight )
{
	ogreLight->_notifyMoved();
}

EXPORTFUNC Ogre::AxisAlignedBox* LIGHT_getBoundingBox( Ogre::Light* ogreLight )
{
	return new Ogre::AxisAlignedBox( ogreLight->getBoundingBox() );

}

EXPORTFUNC void LIGHT__updateRenderQueue( Ogre::RenderQueue* queue , Ogre::Light* ogreLight )
{
	ogreLight->_updateRenderQueue( queue );
}

EXPORTFUNC const char* LIGHT_getMovableType( Ogre::Light* ogreLight )
{
	return ogreLight->getMovableType().c_str();
}

EXPORTFUNC Ogre::Vector3* LIGHT_getDerivedPosition( Ogre::Light* ogreLight )
{
	return new Ogre::Vector3( ogreLight->getDerivedPosition() );
}

EXPORTFUNC Ogre::Vector3* LIGHT_getDerivedDirection( Ogre::Light* ogreLight )
{
	return new Ogre::Vector3( ogreLight->getDerivedDirection() );
}

EXPORTFUNC void LIGHT_setVisible( bool visible , Ogre::Light* ogreLight )
{
	ogreLight->setVisible( visible );
}

EXPORTFUNC float LIGHT_getBoundingRadius( Ogre::Light* ogreLight )
{
	return ogreLight->getBoundingRadius();
}

EXPORTFUNC Ogre::Vector4* LIGHT_getAs4DVector( Ogre::Light* ogreLight )
{
	return new Ogre::Vector4( ogreLight->getAs4DVector() );
}

EXPORTFUNC Ogre::PlaneBoundedVolume* LIGHT__getNearClipVolume( Ogre::Camera* cam , Ogre::Light* l )
{
	return new Ogre::PlaneBoundedVolume(l->_getNearClipVolume(cam));
}

EXPORTFUNC Ogre::PlaneBoundedVolumeList* LIGHT__getFrustumClipVolumes( Ogre::Camera* cam , Ogre::Light* ogreLight )
{
	return new Ogre::PlaneBoundedVolumeList( ogreLight->_getFrustumClipVolumes( cam ) );
}

EXPORTFUNC Ogre::uint32 LIGHT_getTypeFlags( Ogre::Light* ogreLight )
{
	return ogreLight->getTypeFlags();
}

EXPORTFUNC Ogre::AnimableValue* LIGHT_createAnimableValue( const char* valueName , Ogre::Light* ogreLight )
{
	return ogreLight->createAnimableValue( valueName ).getPointer();
}

EXPORTFUNC void LIGHT_setCustomShadowCameraSetup( Ogre::ShadowCameraSetup* customShadowSetup , Ogre::Light* ogreLight )
{
	ogreLight->setCustomShadowCameraSetup( Ogre::ShadowCameraSetupPtr(customShadowSetup) );
}

EXPORTFUNC void LIGHT_resetCustomShadowCameraSetup( Ogre::Light* ogreLight )
{
	ogreLight->resetCustomShadowCameraSetup();
}

EXPORTFUNC Ogre::ShadowCameraSetup* LIGHT_getCustomShadowCameraSetup( Ogre::Light* ogreLight )
{
	return ogreLight->getCustomShadowCameraSetup().getPointer();
}





















#endif
