#ifndef H_TSKYPLANEGENPARAMETERS
#define H_TSKYPLANEGENPARAMETERS

EXPORTFUNC float SKYPLANEGENPARAMETERS_getSkyPlaneScale( Ogre::SceneManager::SkyPlaneGenParameters* p )
{
	return p->skyPlaneScale;
}

EXPORTFUNC float SKYPLANEGENPARAMETERS_getSkyPlaneTiling( Ogre::SceneManager::SkyPlaneGenParameters* p )
{
	return p->skyPlaneTiling;
}

EXPORTFUNC float SKYPLANEGENPARAMETERS_getSkyPlaneBow( Ogre::SceneManager::SkyPlaneGenParameters* p )
{
	return p->skyPlaneBow;
}

EXPORTFUNC int SKYPLANEGENPARAMETERS_getSkyPlaneXSegments( Ogre::SceneManager::SkyPlaneGenParameters* p )
{
	return p->skyPlaneXSegments;
}

EXPORTFUNC int SKYPLANEGENPARAMETERS_getSkyPlaneYSegments( Ogre::SceneManager::SkyPlaneGenParameters* p )
{
	return p->skyPlaneYSegments;
}

EXPORTFUNC void SKYPLANEGENPARAMETERS_delete( Ogre::SceneManager::SkyPlaneGenParameters* p )
{
	delete p;
}

#endif
