#ifndef H_FLOWGRAPHICS
#define H_FLOWGRAPHICS

// Note: Most FG stuff will be handled blitzmax side for now, but some functions are much
// easier to setup in c++, such as the full screen functionality.

EXPORTFUNC bool FG_enableFullScreen( int width , int height , int bits )
{
#ifdef WIN32
	DEVMODE dm;
	memset( &dm , 0 , sizeof dm );
	dm.dmSize = sizeof dm;
	dm.dmPelsWidth = width;
	dm.dmPelsHeight = height;
	dm.dmBitsPerPel = bits;
	dm.dmFields = DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;
	if (ChangeDisplaySettings(&dm , CDS_FULLSCREEN)!=DISP_CHANGE_SUCCESSFUL)
	{
		return false;
	}
	else
	{
		return true;
	}
#elif OSX

#elif LINUX

#endif
}



#endif
