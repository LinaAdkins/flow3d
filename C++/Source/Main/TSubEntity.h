#ifndef H_TSUBENTITY
#define H_TSUBENTITY

/*

Public Member Functions
*/

EXPORTFUNC const char* SUBENTITY_getMaterialName( Ogre::SubEntity* s )
{
	tempString = s->getMaterialName();
	return tempString.c_str();
}

EXPORTFUNC void SUBENTITY_setMaterialName( const char* name , Ogre::SubEntity* s )
{
	s->setMaterialName( name );
}

/*
virtual void 	setVisible (bool visible)
virtual bool 	isVisible (void) const
SubMesh * 	getSubMesh (void)
Entity * 	getParent (void) const

--- Overridden Ogre::Renderable ---
*/

EXPORTFUNC Ogre::MaterialPtr* SUBENTITY_getMaterial( Ogre::SubEntity* s )
{
	return new Ogre::MaterialPtr( s->getMaterial() );
}

/*
const MaterialPtr & 	getMaterial (void) const
Technique * 	getTechnique (void) const
void 	getRenderOperation (RenderOperation &op)
void 	getWorldTransforms (Matrix4 *xform) const
unsigned short 	getNumWorldTransforms (void) const
Real 	getSquaredViewDepth (const Camera *cam) const

--- Non Overridden Ogre::Renderable ---
const LightList & 	getLights (void) const
VertexData * 	_getSkelAnimVertexData (void)
VertexData * 	_getSoftwareVertexAnimVertexData (void)
VertexData * 	_getHardwareVertexAnimVertexData (void)
TempBlendedBufferInfo * 	_getSkelAnimTempBufferInfo (void)
TempBlendedBufferInfo * 	_getVertexAnimTempBufferInfo (void)
VertexData * 	getVertexDataForBinding (void)
void 	_markBuffersUnusedForAnimation (void)
void 	_markBuffersUsedForAnimation (void)
bool 	_getBuffersMarkedForAnimation (void) const
void 	_restoreBuffersForUnusedAnimation (bool hardwareAnimation)
void 	_updateCustomGpuParameter (const GpuProgramParameters::AutoConstantEntry &constantEntry, GpuProgramParameters *params) const
void 	_invalidateCameraCache ()
virtual bool 	preRender (SceneManager *sm, RenderSystem *rsys)
virtual void 	postRender (SceneManager *sm, RenderSystem *rsys)
void 	setUseIdentityProjection (bool useIdentityProjection)
bool 	getUseIdentityProjection (void) const
void 	setUseIdentityView (bool useIdentityView)
bool 	getUseIdentityView (void) const
void 	setCustomParameter (size_t index, const Vector4 &value)
const Vector4 & 	getCustomParameter (size_t index) const
virtual void 	setPolygonModeOverrideable (bool override)
virtual bool 	getPolygonModeOverrideable (void) const
virtual void 	setUserAny (const Any &anything)
virtual const Any & 	getUserAny (void) const
virtualRenderSystemData * 	getRenderSystemData () const
virtual void 	setRenderSystemData (RenderSystemData *val) const
*/

/* Alloc not used
void * 	operator new (size_t sz, const char *file, int line, const char *func)
void * 	operator new (size_t sz)
void * 	operator new (size_t sz, void *ptr)
void * 	operator new[] (size_t sz, const char *file, int line, const char *func)
void * 	operator new[] (size_t sz)
void 	operator delete (void *ptr)
void 	operator delete (void *ptr, const char *, int, const char *)
void 	operator delete[] (void *ptr)
void 	operator delete[] (void *ptr, const char *, int, const char *)

*/


#endif