#ifndef H_TNODETRACKLIST
#define H_TNODETRACKLIST

/*
	Manual Construction Not Allowed
	(constructor)	 Construct map (public member function)
*/
EXPORTFUNC void NODETRACKLIST_delete( Ogre::Animation::NodeTrackList* l )
{
	delete l;
}

/*
	Equality Operator Skipped
	operator=	Copy container content (public member function)
*/


EXPORTFUNC Ogre::Animation::NodeTrackList::iterator* NODETRACKLIST_begin( Ogre::Animation::NodeTrackList* l )
{
	return new Ogre::Animation::NodeTrackList::iterator( l->begin() );
}

EXPORTFUNC Ogre::Animation::NodeTrackList::iterator* NODETRACKLIST_end( Ogre::Animation::NodeTrackList* l )
{
	return new Ogre::Animation::NodeTrackList::iterator( l->end() );
}

/*
	Reverse Iterators Skipped
	rbegin	Return reverse iterator to reverse beginning (public member function)
	rend	Return reverse iterator to reverse end (public member function)


	Skipped For now
	empty	Test whether container is empty (public member function)
	size	Return container size (public member function)
	max_size	Return maximum size (public member function)
	operator[]	Access element (public member function)
	insert	Insert element (public member function)
	erase	Erase elements (public member function)
	swap	Swap content (public member function)
	clear	Clear content (public member function)
	key_comp	Return key comparison object (public member function)
	value_comp	Return value comparison object (public member function)
	find	Get iterator to element (public member function)
	count	Count elements with a specific key (public member function)
	lower_bound	Return iterator to lower bound (public member function)
	upper_bound	Return iterator to upper bound (public member function)
	equal_range	Get range of equal elements (public member function)
	get_allocator	Get allocator (public member function)
*/

/*
	AnimationTrackList Iterator
*/

EXPORTFUNC void NODETRACKLISTITER_delete( Ogre::Animation::NodeTrackList::iterator* i )
{
	delete i;
}

EXPORTFUNC bool NODETRACKLISTITER_isNotEqualTo( Ogre::Animation::NodeTrackList::iterator* comparator , Ogre::Animation::NodeTrackList::iterator* i )
{
	return (*i) != (*comparator);
}

EXPORTFUNC void NODETRACKLISTITER_moveNext( Ogre::Animation::NodeTrackList::iterator* i )
{
	++(*i);
}

EXPORTFUNC Ogre::NodeAnimationTrack* NODETRACKLISTITER_getAnimationTrack( Ogre::Animation::NodeTrackList::iterator* i )
{
	return (*i)->second;
}




#endif
