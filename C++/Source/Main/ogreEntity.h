#ifndef H_OGREENTITY
#define H_OGREENTITY

EXPORTFUNC Ogre::Mesh* ENT_getMesh( Ogre::Entity* e )
{
	return e->getMesh().getPointer();
}

EXPORTFUNC Ogre::SubEntity* ENT_getSubEntity( unsigned int index , Ogre::Entity* e )
{
	return e->getSubEntity( index );
}

EXPORTFUNC Ogre::SubEntity* ENT_getSubEntityWithName( const char* name , Ogre::Entity* e )
{
	return e->getSubEntity( name );
}

EXPORTFUNC int ENT_getNumSubEntities( Ogre::Entity* e )
{
	return e->getNumSubEntities();
}

EXPORTFUNC Ogre::Entity* ENT_clone( const char* cloneName , Ogre::Entity* e )
{
	return e->clone( cloneName );
}

EXPORTFUNC void ENT_setMaterialName( const char* materialName , Ogre::Entity* ogreEntity )
{
	ogreEntity->setMaterialName( materialName );
}

EXPORTFUNC void ENT__notifyCurrentCamera( Ogre::Camera* cam , Ogre::Entity* e )
{
	e->_notifyCurrentCamera( cam );
}
EXPORTFUNC void ENT_setRenderQueueGroup( Ogre::uint8 queueID , Ogre::Entity* e )
{
	e->setRenderQueueGroup( queueID );
}

EXPORTFUNC Ogre::AxisAlignedBox* ENT_getBoundingBox( Ogre::Entity* e )
{
	Ogre::AxisAlignedBox* aab = new Ogre::AxisAlignedBox( e->getBoundingBox() );
	return aab;
}

EXPORTFUNC Ogre::AxisAlignedBox* ENT_getChildObjectsBoundingBox( Ogre::Entity* e )
{
	Ogre::AxisAlignedBox* aab = new Ogre::AxisAlignedBox( e->getChildObjectsBoundingBox() );
	return aab;	
}

EXPORTFUNC void ENT__updateRenderQueue( Ogre::RenderQueue* queue , Ogre::Entity* e )
{
	e->_updateRenderQueue( queue );
}

EXPORTFUNC const char* ENT_getMovableType( Ogre::Entity* e )
{
	return e->getMovableType().c_str();
}

EXPORTFUNC Ogre::AnimationState* ENT_getAnimationState( const char* name , Ogre::Entity* ogreEntity )
{	
	return ogreEntity->getAnimationState( name );
}

EXPORTFUNC Ogre::AnimationStateSet* ENT_getAllAnimationStates( Ogre::Entity* e )
{
	return e->getAllAnimationStates();
}

EXPORTFUNC void ENT_setDisplaySkeleton( bool display , Ogre::Entity* e )
{
	e->setDisplaySkeleton( display );
}

EXPORTFUNC int ENT_getDisplaySkeleton( Ogre::Entity* e )
{
	return e->getDisplaySkeleton();
}

EXPORTFUNC Ogre::Entity* ENT_getManualLodLevel( int index , Ogre::Entity* e )
{
	return e->getManualLodLevel( index );
}


EXPORTFUNC int ENT_getNumManualLodLevels( Ogre::Entity* e )
{
	return (int)e->getNumManualLodLevels();
}

EXPORTFUNC void ENT_setMeshLodBias( float factor , int maxDetailIndex , int minDetailIndex , Ogre::Entity* e )
{
	e->setMeshLodBias( factor , maxDetailIndex , minDetailIndex );
}

EXPORTFUNC void ENT_setMaterialLodBias( float factor , int maxDetailIndex , int minDetailIndex , Ogre::Entity* e )
{
	e->setMaterialLodBias( factor , maxDetailIndex , minDetailIndex );
}
EXPORTFUNC void ENT_setPolygonModeOverrideable( bool PolygonModeOverrideable , Ogre::Entity* e )
{
	e->setPolygonModeOverrideable( PolygonModeOverrideable );
}

EXPORTFUNC Ogre::TagPoint* ENT_attachObjectToBone( const char* boneName , Ogre::MovableObject* pMovable , Ogre::Quaternion* offsetOrientation , Ogre::Vector3* offsetPosition , Ogre::Entity* e )
{
	return e->attachObjectToBone( boneName , pMovable , *offsetOrientation , *offsetPosition );
}

EXPORTFUNC void ENT_detachObjectFromBone( Ogre::MovableObject* obj , Ogre::Entity* e )
{
	e->detachObjectFromBone( obj );
}

EXPORTFUNC Ogre::MovableObject* ENT_detachObjectFromBoneWithName( const char* movableName , Ogre::Entity* e )
{
	return MovableObjectChildCast( e->detachObjectFromBone( movableName ));
}

EXPORTFUNC void ENT_detachAllObjectsFromBone( Ogre::Entity* e )
{
	e->detachAllObjectsFromBone();
}

EXPORTFUNC Ogre::Entity::ChildObjectListIterator* ENT_getAttachedObjectIterator( Ogre::Entity* e )
{
	return new Ogre::Entity::ChildObjectListIterator( e->getAttachedObjectIterator() );
}

// Not in shoggoth
/*void ENT_setNormaliseNormals( bool normalise , Ogre::Entity* e )
{
	e->setNormaliseNormals( normalise );
}

int ENT_getNormaliseNormals( Ogre::Entity* e )
{
	return e->getNormaliseNormals();
}*/

EXPORTFUNC const Ogre::Matrix4* ENT__getBoneMatrices( Ogre::Entity* e )
{
	return e->_getBoneMatrices();
}

EXPORTFUNC int ENT__getNumBoneMatrices( Ogre::Entity* e )
{
	return e->_getNumBoneMatrices();
}

EXPORTFUNC int ENT_hasSkeleton( Ogre::Entity* e )
{
	return e->hasSkeleton();
}

EXPORTFUNC Ogre::SkeletonInstance* ENT_getSkeleton( Ogre::Entity* e )
{
	return e->getSkeleton();
}

EXPORTFUNC int ENT_isHardwareAnimationEnabled( Ogre::Entity* e )
{
	return e->isHardwareAnimationEnabled();
}

EXPORTFUNC int ENT_getSoftwareAnimationRequests( Ogre::Entity* e )
{
	return e->getSoftwareAnimationRequests();
}

EXPORTFUNC int ENT_getSoftwareAnimationNormalsRequests( Ogre::Entity* e )
{
	return e->getSoftwareAnimationNormalsRequests();
}

EXPORTFUNC void ENT_addSoftwareAnimationRequest( bool normalsAlso , Ogre::Entity* e )
{
	e->addSoftwareAnimationRequest( normalsAlso );
}

EXPORTFUNC void ENT_removeSoftwareAnimationRequest( bool normalsAlso , Ogre::Entity* e )
{
	e->removeSoftwareAnimationRequest( normalsAlso );
}

EXPORTFUNC void ENT_shareSkeletonInstanceWith( Ogre::Entity* entity , Ogre::Entity* e )
{
	e->shareSkeletonInstanceWith( entity );
}

EXPORTFUNC int ENT_hasVertexAnimation( Ogre::Entity* e )
{
	return e->hasVertexAnimation();
}

EXPORTFUNC void ENT_stopSharingSkeletonInstance( Ogre::Entity* e )
{
	e->stopSharingSkeletonInstance();
}

EXPORTFUNC int ENT_sharesSkeletonInstance( Ogre::Entity* e )
{
	return e->sharesSkeletonInstance();
}

EXPORTFUNC const Ogre::Entity::EntitySet* ENT_getSkeletonInstanceSharingSet( Ogre::Entity* e )
{
	return e->getSkeletonInstanceSharingSet();
}

EXPORTFUNC void ENT_refreshAvailableAnimationState( Ogre::Entity* e )
{
	e->refreshAvailableAnimationState();
}

EXPORTFUNC void ENT__updateAnimation( Ogre::Entity* e )
{
	e->_updateAnimation();
}

EXPORTFUNC int ENT__isAnimated( Ogre::Entity* e )
{
	return e->_isAnimated();
}

EXPORTFUNC int ENT__isSkeletonAnimated( Ogre::Entity* e )
{
	return e->_isSkeletonAnimated();
}

EXPORTFUNC Ogre::VertexData* ENT__getSkelAnimVertexData( Ogre::Entity* e )
{
	return e->_getSkelAnimVertexData();
}

EXPORTFUNC Ogre::VertexData* ENT__getSoftwareVertexAnimVertexData( Ogre::Entity* e )
{
	return e->_getSoftwareVertexAnimVertexData();
}

EXPORTFUNC Ogre::VertexData* ENT__getHardwareVertexAnimVertexData( Ogre::Entity* e )
{
	return e->_getHardwareVertexAnimVertexData();
}

EXPORTFUNC Ogre::TempBlendedBufferInfo* ENT__getSkelAnimTempBufferInfo( Ogre::Entity* e )
{
	return e->_getSkelAnimTempBufferInfo();
}

EXPORTFUNC Ogre::TempBlendedBufferInfo* ENT__getVertexAnimTempBufferInfo( Ogre::Entity* e )
{
	return e->_getVertexAnimTempBufferInfo();
}

EXPORTFUNC Ogre::VertexData* ENT_getVertexDataForBinding( Ogre::Entity* e )
{
	return e->getVertexDataForBinding();
}

EXPORTFUNC int ENT_chooseVertexDataForBinding( bool hasVertexAnim , Ogre::Entity* e )
{
	return e->chooseVertexDataForBinding( hasVertexAnim );
}

EXPORTFUNC int ENT__getBuffersMarkedForAnimation( Ogre::Entity* e )
{
	return e->_getBuffersMarkedForAnimation();
}

EXPORTFUNC void ENT__markBuffersUsedForAnimation( Ogre::Entity* e )
{
	e->_markBuffersUsedForAnimation();
}

EXPORTFUNC int ENT_isInitialised( Ogre::Entity* e )
{
	return e->isInitialised();
}

EXPORTFUNC void ENT__initialise( bool forceReinitialise , Ogre::Entity* e )
{
	e->_initialise( forceReinitialise );
}

EXPORTFUNC void ENT__deinitialise( Ogre::Entity* e )
{
	e->_deinitialise();
}

EXPORTFUNC void ENT_backgroundLoadingComplete( Ogre::Resource* res , Ogre::Entity* e )
{
	e->backgroundLoadingComplete( res );
}

EXPORTFUNC void ENT_delete( Ogre::Entity* e )
{
	delete e;
}



#endif
