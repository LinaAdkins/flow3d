#ifndef H_OGREMATRIX4
#define H_OGREMATRIX4

EXPORTFUNC Ogre::Matrix4* MATRIX4_createEmpty()
{
	Ogre::Matrix4* matrix = new Ogre::Matrix4();
	return matrix;
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_create( float m00 , float m01 , float m02 , float m03 , float m10 , float m11 , float m12 , float m13 , float m20 , float m21 , float m22 , float m23 , float m30 , float m31 , float m32 , float m33 )
{
	Ogre::Matrix4* matrix = new Ogre::Matrix4( m00 , m01 , m02 , m03 , m10 , m11 , m12 , m13 , m20 , m21 , m22 , m23 , m30 , m31 , m32 , m33 );
	return matrix;
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_createWithMatrix3( Ogre::Matrix3* m3x3 )
{
	Ogre::Matrix4* matrix = new Ogre::Matrix4( *m3x3 );
	return matrix;
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_createWithQuaternion( Ogre::Quaternion* rot )
{
	Ogre::Matrix4* matrix = new Ogre::Matrix4( *rot );
	return matrix;
}

// Name changed since [] operators cannot be used-
EXPORTFUNC float MATRIX4_getValue( int iRow , int iColumn , Ogre::Matrix4* matrix )
{
	return (*matrix)[iRow][iColumn];
}

// const Real *const 	operator[] (size_t iRow) const - unneccessary -
EXPORTFUNC Ogre::Matrix4* MATRIX4_concatenate( Ogre::Matrix4* m2 , Ogre::Matrix4* matrix )
{
	Ogre::Matrix4* product = new Ogre::Matrix4( matrix->concatenate( *m2 ) );
	return product;
}

// Matrix4 	operator * (const Matrix4 &m2) const - Just uses concatenate with an operator - unneccessary -
EXPORTFUNC Ogre::Vector3* MATRIX4_mulWithVector3( Ogre::Vector3* v , Ogre::Matrix4* matrix )
{
	return new Ogre::Vector3( (*matrix)*(*v) );
}

EXPORTFUNC Ogre::Vector4* MATRIX4_mulWithVector4( Ogre::Vector4* v , Ogre::Matrix4* matrix )
{
	return new Ogre::Vector4( (*matrix)*(*v) );
}

EXPORTFUNC Ogre::Plane* MATRIX4_mulWithPlane( Ogre::Plane* p , Ogre::Matrix4* matrix )
{
	Ogre::Plane* product = new Ogre::Plane( (*matrix)*(*p) );
	return product;
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_add( Ogre::Matrix4* m2 , Ogre::Matrix4* matrix )
{
	Ogre::Matrix4* sum = new Ogre::Matrix4( (*matrix)+(*m2) );
	return sum;
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_sub( Ogre::Matrix4* m2 , Ogre::Matrix4* matrix )
{
	Ogre::Matrix4* difference = new Ogre::Matrix4( (*matrix) - (*m2) );
	return difference;
}

EXPORTFUNC int MATRIX4_isEqualTo( Ogre::Matrix4* m2 , Ogre::Matrix4* matrix )
{
	return( (*matrix)==(*m2) );
}

EXPORTFUNC int MATRIX4_isNotEqualTo( Ogre::Matrix4* m2 , Ogre::Matrix4* matrix )
{
	return( (*matrix)!=(*m2) );
}

EXPORTFUNC void MATRIX4_eq( Ogre::Matrix4* m2 , Ogre::Matrix4* matrix )
{
	*matrix = *m2;
}

EXPORTFUNC void MATRIX4_eqWithMatrix3( Ogre::Matrix3* mat3 , Ogre::Matrix4* matrix )
{
	*matrix=*mat3;
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_transpose( Ogre::Matrix4* matrix )
{
	Ogre::Matrix4* result = new Ogre::Matrix4( matrix->transpose() );
	return result;
}

EXPORTFUNC void MATRIX4_setTrans( Ogre::Vector3* v , Ogre::Matrix4* matrix )
{
	matrix->setTrans( *v );
}

EXPORTFUNC Ogre::Vector3* MATRIX4_getTrans( Ogre::Matrix4* matrix )
{
	return new Ogre::Vector3( matrix->getTrans() );

	
}

EXPORTFUNC void MATRIX4_makeTrans( Ogre::Vector3* v , Ogre::Matrix4* matrix )
{
	matrix->makeTrans( *v );
}

EXPORTFUNC void MATRIX4_makeTransWithFloat( float tx , float ty , float tz , Ogre::Matrix4* matrix )
{
	matrix->makeTrans( tx , ty , tz );
}

EXPORTFUNC void MATRIX4_setScale( Ogre::Vector3* v , Ogre::Matrix4* matrix )
{
	matrix->setScale( *v );
}

EXPORTFUNC void MATRIX4_extract3x3Matrix( Ogre::Matrix3* m3x3 , Ogre::Matrix4* matrix )
{
	matrix->extract3x3Matrix( *m3x3 );
}

EXPORTFUNC Ogre::Quaternion* MATRIX4_extractQuaternion( Ogre::Matrix4* matrix )
{
	return new Ogre::Quaternion( matrix->extractQuaternion() );
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_mulWithScalar( float scalar , Ogre::Matrix4* matrix )
{
	return new Ogre::Matrix4( (*matrix)*scalar );
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_adjoint( Ogre::Matrix4* matrix )
{
	Ogre::Matrix4* result = new Ogre::Matrix4( matrix->adjoint() );
	return result;
}

EXPORTFUNC float MATRIX4_determinant( Ogre::Matrix4* matrix )
{
	return matrix->determinant();
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_inverse( Ogre::Matrix4* matrix )
{
	Ogre::Matrix4* result = new Ogre::Matrix4( matrix->inverse() );
	return result;
}

EXPORTFUNC void MATRIX4_makeTransform( Ogre::Vector3* position , Ogre::Vector3* scale , Ogre::Quaternion* orientation , Ogre::Matrix4* matrix )
{
	matrix->makeTransform( *position , *scale ,  *orientation );
}

EXPORTFUNC void MATRIX4_makeInverseTransform( Ogre::Vector3* position , Ogre::Vector3* scale , Ogre::Quaternion* orientation , Ogre::Matrix4* matrix )
{
	matrix->makeInverseTransform( *position , *scale , * orientation );
}

EXPORTFUNC int MATRIX4_isAffine( Ogre::Matrix4* matrix )
{
	return matrix->isAffine();
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_inverseAffine( Ogre::Matrix4* matrix )
{
	Ogre::Matrix4* result = new Ogre::Matrix4( matrix->inverseAffine() );
	return result;
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_concatenateAffine( Ogre::Matrix4* m2 ,  Ogre::Matrix4* matrix )
{
	Ogre::Matrix4* result = new Ogre::Matrix4( matrix->concatenateAffine( *m2 ) );
	return result;
}

EXPORTFUNC Ogre::Vector3* MATRIX4_transformAffine( Ogre::Vector3* v , Ogre::Matrix4* matrix )
{
	return new Ogre::Vector3( matrix->transformAffine( *v ) );
}

EXPORTFUNC Ogre::Vector4* MATRIX4_transformAffineWithVector4( Ogre::Vector4* v , Ogre::Matrix4* matrix )
{
	return new Ogre::Vector4( matrix->transformAffine(*v) );
}

// Static Public Member Functions
EXPORTFUNC Ogre::Matrix4* MATRIX4_getStaticTrans( Ogre::Vector3* v )
{
	Ogre::Matrix4* result = new Ogre::Matrix4( Ogre::Matrix4::getTrans( *v ) );
	return result;
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_getStaticTransWithFloat( float t_x , float t_y , float t_z )
{
	Ogre::Matrix4* result = new Ogre::Matrix4( Ogre::Matrix4::getTrans( t_x , t_y , t_z ) );
	return result;
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_getStaticScale( Ogre::Vector3* v )
{
	Ogre::Matrix4* result = new Ogre::Matrix4( Ogre::Matrix4::getScale( *v ) );
	return result;
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_getStaticScaleWithFloat( float s_x , float s_y , float s_z )
{
	Ogre::Matrix4* result = new Ogre::Matrix4( Ogre::Matrix4::getScale( s_x , s_y , s_z ) );
	return result;
}

// Static public attributes
EXPORTFUNC Ogre::Matrix4* MATRIX4_ZERO()
{
	return (Ogre::Matrix4*)&Ogre::Matrix4::ZERO;
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_IDENTITY()
{
	return (Ogre::Matrix4*)&Ogre::Matrix4::IDENTITY;
}

EXPORTFUNC Ogre::Matrix4* MATRIX4_CLIPSPACE2DTOIMAGESPACE()
{
	return (Ogre::Matrix4*)&Ogre::Matrix4::CLIPSPACE2DTOIMAGESPACE;
}

// Friends
// _OgreExport friend std::ostream & 	operator<< (std::ostream &o, const Matrix4 &m)_OgreExport friend std::ostream & 	operator<< (std::ostream &o, const Matrix4 &m)
// Supplanted by a toStr() functions for Ogre::Matrix4
EXPORTFUNC const char* MATRIX4_toStr( Ogre::Matrix4* matrix )
{
	std::ostringstream os;
	os << *matrix;
	tempString.clear();
	tempString = os.str();
	return tempString.c_str();
}
EXPORTFUNC void MATRIX4_delete( Ogre::Matrix4* matrix )
{
	delete matrix;
}

#endif
