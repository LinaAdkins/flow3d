#ifndef H_TBONEITERATOR
#define H_TBONEITERATOR

/*
	Manual Construction Not Allowed
 	VectorIterator (typename T::iterator start, typename T::iterator end)
 	VectorIterator (T &c)
*/

EXPORTFUNC bool BONEITERATOR_hasMoreElements( Ogre::Skeleton::BoneIterator* i )
{
	return i->hasMoreElements();
}

EXPORTFUNC Ogre::Bone* BONEITERATOR_getNext( Ogre::Skeleton::BoneIterator* i )
{
	return i->getNext();
}

EXPORTFUNC void BONEITERATOR_delete( Ogre::Skeleton::BoneIterator* i )
{
	delete i;
}

/*
	Skipped for now
	T::value_type 	peekNext (void)
	T::pointer 	peekNextPtr (void)
	void 	moveNext (void)
*/

#endif
