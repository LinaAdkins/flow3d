#ifndef H_TSCENEMANAGERMETADATA
#define H_TSCENEMANAGERMETADATA

EXPORTFUNC const char* SMMD_getTypeName( Ogre::SceneManagerMetaData* d )
{
	return d->typeName.c_str();
}

EXPORTFUNC const char* SMMD_getDescription( Ogre::SceneManagerMetaData* d )
{
	return d->description.c_str();
}

EXPORTFUNC int SMMD_getSceneTypeMask( Ogre::SceneManagerMetaData* d )
{
	return d->sceneTypeMask;
}

EXPORTFUNC bool SMMD_isWorldGeometrySupported( Ogre::SceneManagerMetaData* d )
{
	return d->worldGeometrySupported;
}

#endif
