#ifndef H_TSKYBOXGENPARAMETERS
#define H_TSKYBOXGENPARAMETERS

EXPORTFUNC float SKYBOXGENPARAMETERS_getSkyBoxDistance( Ogre::SceneManager::SkyBoxGenParameters* p )
{
	return p->skyBoxDistance;
}

EXPORTFUNC void SKYBOXGENPARAMETERS_delete( Ogre::SceneManager::SkyBoxGenParameters* p)
{
	delete p;
}

#endif
