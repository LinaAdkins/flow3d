#ifndef H_OGRETIMER
#define H_OGRETIMER

EXPORTFUNC Ogre::Timer* OGRETIMER_create()
{
	return new Ogre::Timer();
}

EXPORTFUNC void OGRETIMER_delete( Ogre::Timer* t )
{
	delete t;
}

EXPORTFUNC void OGRETIMER_reset( Ogre::Timer* t )
{
	t->reset();
}

EXPORTFUNC float OGRETIMER_getTime( Ogre::Timer* t )
{
	return  static_cast<float>( t->getMicroseconds() ) / 1000000 ; 
}



#endif
