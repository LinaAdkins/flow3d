#ifndef H_TBILLBOARDSET
#define H_TBILLBOARDSET

EXPORTFUNC Ogre::Billboard* BILLBOARDSET_createBillboard( Ogre::Vector3* position , Ogre::ColourValue* colour , Ogre::BillboardSet* s )
{
	return s->createBillboard( *position , *colour );
}

EXPORTFUNC void BILLBOARDSET_setMaterialName( const char* name , Ogre::BillboardSet* s )
{
	s->setMaterialName( name );
}

EXPORTFUNC bool BILLBOARDSET_getSortingEnabled( Ogre::BillboardSet* s )
{
	return s->getSortingEnabled();
}

EXPORTFUNC void BILLBOARDSET_setSortingEnabled( bool sortenable , Ogre::BillboardSet* s )
{
	s->setSortingEnabled( sortenable );
}

#endif
