#ifndef H_OGRERENDERSYSTEMLIST
#define H_OGRERENDERSYSTEMLIST

EXPORTFUNC Ogre::RenderSystemList* RSL_create()
{
	Ogre::RenderSystemList* rsl = new Ogre::RenderSystemList();
	return rsl;
}

EXPORTFUNC void RSL_delete( Ogre::RenderSystemList* rsl )
{
	delete rsl;
}

EXPORTFUNC Ogre::RenderSystemList::iterator* RSL_begin( Ogre::RenderSystemList* rsl )
{
	Ogre::RenderSystemList::iterator* rsli = new Ogre::RenderSystemList::iterator();
	*rsli = rsl->begin();
	return rsli;
}

EXPORTFUNC Ogre::RenderSystemList::iterator* RSL_end( Ogre::RenderSystemList* rsl )
{
	Ogre::RenderSystemList::iterator* rsli = new Ogre::RenderSystemList::iterator();
	*rsli = rsl->end();
	return rsli;
}

EXPORTFUNC int RSL_size( Ogre::RenderSystemList* rsl )
{
	return rsl->size();
}

EXPORTFUNC Ogre::RenderSystem* RSLI_getRenderSystem( Ogre::RenderSystemList::iterator* rsli )
{
	return **rsli;
}

EXPORTFUNC void RSLI_eq( Ogre::RenderSystemList::iterator* a , Ogre::RenderSystemList::iterator* i )
{
	*i = *a;
}

EXPORTFUNC bool RSLI_isEqualTo( Ogre::RenderSystemList::iterator* a , Ogre::RenderSystemList::iterator* i )
{
	return (*i == *a);
}

EXPORTFUNC bool RSLI_isNotEqualTo( Ogre::RenderSystemList::iterator* a , Ogre::RenderSystemList::iterator* i )
{
	return (*i != *a);
}

EXPORTFUNC void RSLI_moveForward( Ogre::RenderSystemList::iterator* i )
{
	++(*i);
}



#endif
