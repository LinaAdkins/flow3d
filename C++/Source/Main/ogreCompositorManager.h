#ifndef H_OGRECOMPOSITORMANAGER
#define H_OGRECOMPOSITORMANAGER

EXPORTFUNC Ogre::CompositorInstance* COMPMANAGER_addCompositor( Ogre::Viewport* vp , const char* compositor , int addPosition )
{
	return Ogre::CompositorManager::getSingleton().addCompositor( vp , compositor , addPosition );
}

EXPORTFUNC void COMPMANAGER_setCompositorEnabled( Ogre::Viewport* vp , const char* compositor , bool value )
{
	Ogre::CompositorManager::getSingleton().setCompositorEnabled( vp , compositor , value );
}

EXPORTFUNC void COMPMANAGER_removeCompositor( Ogre::Viewport* vp , const char* compositor )
{
	Ogre::CompositorManager::getSingleton().removeCompositor( vp , compositor );
}

EXPORTFUNC void COMPMANAGER_removeAll()
{
	Ogre::CompositorManager::getSingleton().removeAll();
}



#endif
