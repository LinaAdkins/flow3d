#ifndef H_OGREMATERIALMANAGER
#define H_OGREMATERIALMANAGER

EXPORTFUNC Ogre::MaterialPtr* MATERIALMANAGER_create( const char* name , const char* group, bool isManual , Ogre::ManualResourceLoader* loader , Ogre::NameValuePairList* createParams )
{
	return new Ogre::MaterialPtr( Ogre::MaterialManager::getSingleton().create( name , group , isManual, loader , createParams ) );
}

EXPORTFUNC Ogre::MaterialPtr* MATERIALMANAGER_getByName( const char* name )
{
	return new Ogre::MaterialPtr( Ogre::MaterialManager::getSingleton().getByName( name ) );
}

class CCubemapGenerator
{
public:
	/// Initialise the camera array used by the cubemap generator.
	CCubemapGenerator::CCubemapGenerator( Ogre::SceneManager* sm )
	{
		/*
		// Get a unique, random name for the camera
		while( bool nameFound = false )
		{
			// We'll generate a random name with the timer
			unsigned long currTime = Ogre::Root::getSingleton().getTimer()->getMilliseconds();
			Ogre::StringUtil::StrStreamType camName;
			
			camName << "CubemapGeneratorCam";//  currTime; 

			// If the name is available, take it and end the loop
			if( !sm->hasCamera( camName.str() ) )
			{
				nameFound = true;
				mCubeCamera = sm->createCamera( camName.str() );
			}
		}*/

		mCubeCamera = sm->createCamera("mCubeCamera");

		// Set aspect ratio to 1x1 and FOV to 90 degrees
		mCubeCamera->setAspectRatio(1.0f);
		mCubeCamera->setFOVy( Ogre::Radian( Ogre::Math::PI / 2 ) );

	}

	/** Set the position of the camera used to generate the cubemap. ( eg. Where you want the cubemap to be rendered from )
		Note that the position is in world coordinates.
	*/
	void CCubemapGenerator::setCameraPosition( const Ogre::Vector3& pos )
	{
		mCubeCamera->setPosition( pos );
	}
	
	/// Get the camera instance used for generating the cubmaps. 
	Ogre::Camera* CCubemapGenerator::getCamera()
	{
		return mCubeCamera;
	}

	/// Generate a new cubemap. Please note that if you want to create a dynamic cubemap, you'll need to use updateCubemap for subsequent calls.
	Ogre::TexturePtr CCubemapGenerator::generateCubemap( Ogre::String name , Ogre::String resgroup = Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME , Ogre::uint size = 512 , Ogre::PixelFormat pf = Ogre::PF_A8R8G8B8 )
	{
		// Create our empty, base texture.
		Ogre::TexturePtr cubeMap = Ogre::TextureManager::getSingleton().createManual(
																		name ,
																		resgroup ,
																		Ogre::TEX_TYPE_CUBE_MAP ,
																		size , size ,
																		0 , 0 ,
																		pf ,
																		Ogre::TU_RENDERTARGET );

		// Start our loop which will be used to generate the texture's different faces
		for ( int i = 0; i < 6; i++ ) 
		{
			// Get the render target for this face
			Ogre::RenderTarget *rt = cubeMap->getBuffer(i)->getRenderTarget();

			// Orient the camera according to which face we're on
			_OrientCamera( i );

			// Setup the viewport on the render target
			Ogre::Viewport* vp = rt->addViewport( mCubeCamera );
			vp->setOverlaysEnabled(FALSE);
			vp->setClearEveryFrame( true );
			vp->setBackgroundColour( Ogre::ColourValue::Black );

		}


		return cubeMap;
	}

	/** Method for attaching a cubemap to a material. Note that the material must have a blank texture 
		defined before the diffuse texture. Returns true if attaching was a sucess and false if it was not.
	*/
	bool CCubemapGenerator::attachCubemapToMaterial( Ogre::String materialName , Ogre::TexturePtr tex )
	{
		Ogre::MaterialPtr material = Ogre::MaterialManager::getSingleton().getByName( materialName );

		// If we have a good material, then attach
		if( !material.isNull() )
		{
			Ogre::TextureUnitState* t = material->getTechnique(0)->getPass(0)->getTextureUnitState("CubeEnvMap");

				   t = material->getTechnique(0)->getPass(0)->getTextureUnitState("CubeEnvMap");

				   // If the CubeEnvMap texture is available, then set it
				   if (!t && material->getTechnique(0)->getPass(0)->getNumTextureUnitStates()){
					  t = material->getTechnique(0)->getPass(0)->createTextureUnitState("CubeEnvMap");
					  t->setEnvironmentMap(true, Ogre::TextureUnitState::ENV_REFLECTION);
					  material->getTechnique(0)->getPass(0)->getTextureUnitState(0)->setCubicTextureName(tex->getName() ,true);
					  return true;
				   }
		}
		return false;

	}

	/** Internal method to orient the camera into front, back , left, right, etc positions for
		grabbing the composite cubemap image from the scene.
	*/	
	void CCubemapGenerator::_OrientCamera( int face )
	{
		switch (face) 
		{
			case 0:
				mCubeCamera->yaw( Ogre::Radian( -Ogre::Math::PI / 2) ); //Right
				break;
			case 1:
				mCubeCamera->yaw( Ogre::Radian( Ogre::Math::PI / 2) ); // Left 
				break; 
			case 2:
				mCubeCamera->pitch( Ogre::Radian( Ogre::Math::PI / 2) ); // Up
				break;
			case 3:
				mCubeCamera->pitch( Ogre::Radian( -Ogre::Math::PI / 2) );  // Down
				break;
			case 4:
				mCubeCamera->yaw( Ogre::Radian( Ogre::Math::PI ) ); // Back
				break;
			case 5:
				mCubeCamera->yaw( Ogre::Radian( -Ogre::Math::PI ) ); // Front
				break;
			default:
				break;
		}
	}

private:
	/// Camera used to render the cubemap.
	Ogre::Camera* mCubeCamera;

protected:
};

EXPORTFUNC CCubemapGenerator* CUBEMAPGEN_create( Ogre::SceneManager* sm )
{
	return new CCubemapGenerator( sm );
}

EXPORTFUNC void CUBEMAPGEN_delete( CCubemapGenerator* g )
{
	delete g;
}

EXPORTFUNC void CUBEMAPGEN_setCameraPosition( Ogre::Vector3* pos  , CCubemapGenerator* g )
{
	g->setCameraPosition( *pos );
}

EXPORTFUNC Ogre::Texture* CUBEMAPGEN_generateCubemap( const char* name , const char* resgroup , int size , int pf , CCubemapGenerator* g )
{
	return g->generateCubemap( name , resgroup , size , Ogre::PixelFormat( pf ) ).getPointer();
}

EXPORTFUNC void CUBEMAPGEN_attachCubemapToMaterial( const char* name , Ogre::Texture* t , CCubemapGenerator* g )
{
	g->attachCubemapToMaterial( name , Ogre::TexturePtr( t ) );
}








#endif
