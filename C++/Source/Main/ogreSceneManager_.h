#ifndef H_OGRESCENEMANAGER_
#define H_OGRESCENEMANAGER_

// SceneManager (const String &instanceName) - Constructor - Unnecessary-
// virtual 	~SceneManager () - Destructor - Not required for managed objects-
EXPORTFUNC const char* SM_getName( Ogre::SceneManager* ogreSceneManager )
{
	tempString.clear();
	tempString = ogreSceneManager->getName();
	return tempString.c_str();
}
EXPORTFUNC const char* SM_getTypeName( Ogre::SceneManager* ogreSceneManager )
{
	tempString.clear();
	tempString = ogreSceneManager->getTypeName();
	return tempString.c_str();
}

EXPORTFUNC Ogre::Camera* SM_createCamera( const char* CameraName , Ogre::SceneManager* ogreSceneManager  )
{
	return ogreSceneManager->createCamera( CameraName );
}

EXPORTFUNC Ogre::Camera* SM_getCamera( const char* name , Ogre::SceneManager* sm )
{
	return sm->getCamera( name );
}

EXPORTFUNC int SM_hasCamera( const char* name , Ogre::SceneManager* sm )
{
	return sm->hasCamera( name );
}

EXPORTFUNC void SM_destroyCamera( Ogre::Camera* ogreCamera, Ogre::SceneManager* sm )
{
	sm->destroyCamera(ogreCamera);
}

EXPORTFUNC void SM_destroyCameraWithName( const char* name , Ogre::SceneManager* sm )
{
	sm->destroyCamera( name );
}

EXPORTFUNC void SM_destroyAllCameras( Ogre::SceneManager* sm )
{
	sm->destroyAllCameras();
}

EXPORTFUNC Ogre::Light* SM_createLight( const char* LightName , Ogre::SceneManager* sm )
{
	return sm->createLight( LightName );
}

EXPORTFUNC Ogre::Light* SM_getLight( const char* name , Ogre::SceneManager* sm )
{
	return sm->getLight( name );
}
EXPORTFUNC int SM_hasLight( const char* name , Ogre::SceneManager* sm )
{
	return sm->hasLight( name );
}

EXPORTFUNC void SM_destroyLight( Ogre::Light* light , Ogre::SceneManager* sm )
{
	sm->destroyLight( light );
}

EXPORTFUNC void SM_destroyLightWithName( const char* name , Ogre::SceneManager* sm )
{
	sm->destroyLight( name );
}

EXPORTFUNC void SM_destroyAllLights( Ogre::SceneManager* sm )
{
	sm->destroyAllLights();
}

EXPORTFUNC void SM__notifyLightsDirty( Ogre::SceneManager* sm )
{
	sm->_notifyLightsDirty();
}

EXPORTFUNC int SM__getLightsDirtyCounter( Ogre::SceneManager* sm )
{
	return sm->_getLightsDirtyCounter();
}

EXPORTFUNC Ogre::LightList* SM__getLightsAffectingFrustum( Ogre::SceneManager* sm )
{
	return new Ogre::LightList( sm->_getLightsAffectingFrustum() );
}

EXPORTFUNC void SM__populateLightList( Ogre::Vector3* position , float radius , Ogre::LightList* destList , Ogre::SceneManager* sm )
{
	sm->_populateLightList( *position , radius , *destList );
}

EXPORTFUNC Ogre::SceneNode* SM_createSceneNode( Ogre::SceneManager* sm )
{
	return sm->createSceneNode();
}

EXPORTFUNC Ogre::SceneNode* SM_createSceneNodeWithName( const char* name , Ogre::SceneManager* sm )
{
	return sm->createSceneNode( name );
}

EXPORTFUNC void SM_destroySceneNode( const char* name , Ogre::SceneManager* ogreSceneManager )
{
	ogreSceneManager->destroySceneNode( name );
}
EXPORTFUNC Ogre::SceneNode* SM_getRootSceneNode( Ogre::SceneManager* ogreSceneManager )
{
	return ogreSceneManager->getRootSceneNode();
}

EXPORTFUNC Ogre::SceneNode* SM_getSceneNode( const char* name , Ogre::SceneManager* sm )
{
	return sm->getSceneNode( name );
}

EXPORTFUNC int SM_hasSceneNode( const char* name , Ogre::SceneManager* sm )
{
	return sm->hasSceneNode( name );
}

EXPORTFUNC Ogre::Entity* SM_createEntity( const char* entityName , const char* entityMesh , Ogre::SceneManager* sm)
{
	return sm->createEntity( entityName , entityMesh );
}

EXPORTFUNC Ogre::Entity* SM_createEntityWithPrefab( const char* entityName , int ptype , Ogre::SceneManager* sm )
{
	return sm->createEntity( entityName , (Ogre::SceneManager::PrefabType)ptype );
}

EXPORTFUNC Ogre::Entity* SM_getEntity( const char* name , Ogre::SceneManager* sm )
{
	return sm->getEntity( name );
}

EXPORTFUNC int SM_hasEntity( const char* name , Ogre::SceneManager* sm )
{
	return sm->hasEntity( name );
}

EXPORTFUNC void SM_destroyEntity( Ogre::Entity* ogreEntity , Ogre::SceneManager* sm )
{
	sm->destroyEntity( ogreEntity );
}

EXPORTFUNC void SM_destroyEntityWithName( const char* name,  Ogre::SceneManager* sm )
{
	sm->destroyEntity( name );
}
EXPORTFUNC void SM_destroyAllEntities( Ogre::SceneManager* sm )
{
	sm->destroyAllEntities();
}

EXPORTFUNC Ogre::ManualObject* SM_createManualObject( const char* name , Ogre::SceneManager* sm )
{
	return sm->createManualObject( name );
}

EXPORTFUNC Ogre::ManualObject* SM_getManualObject( const char* name , Ogre::SceneManager* sm )
{
	return sm->getManualObject( name );
}

EXPORTFUNC int SM_hasManualObject( const char* name , Ogre::SceneManager* sm )
{
	return sm->hasManualObject( name );
}

EXPORTFUNC void SM_destroyManualObject( Ogre::ManualObject* obj , Ogre::SceneManager* sm )
{
	sm->destroyManualObject( obj );
}

EXPORTFUNC void SM_destroyManualObjectWithName( const char* name , Ogre::SceneManager* sm )
{
	sm->destroyManualObject( name );
}

EXPORTFUNC void SM_destroyAllManualObjects( Ogre::SceneManager* sm )
{
	sm->destroyAllManualObjects();
}

EXPORTFUNC Ogre::BillboardChain* SM_createBillboardChain( const char* name , Ogre::SceneManager* sm )
{
	return sm->createBillboardChain( name );
}
EXPORTFUNC Ogre::BillboardChain* SM_getBillboardChain( const char* name , Ogre::SceneManager* sm )
{
	return sm->getBillboardChain( name );
}

EXPORTFUNC int SM_hasBillboardChain( const char* name , Ogre::SceneManager* sm )
{
	return sm->hasBillboardChain( name );
}

EXPORTFUNC void SM_destroyBillboardChain( Ogre::BillboardChain* obj , Ogre::SceneManager* sm )
{
	sm->destroyBillboardChain( obj );
}

EXPORTFUNC void SM_destroyBillboardChainWithName( const char* name , Ogre::SceneManager* sm )
{
	sm->destroyBillboardChain( name );
}

EXPORTFUNC void SM_destroyAllBillboardChains( Ogre::SceneManager* sm )
{
	sm->destroyAllBillboardChains();
}

EXPORTFUNC Ogre::RibbonTrail* SM_createRibbonTrail( const char* name , Ogre::SceneManager* sm )
{
	return sm->createRibbonTrail( name );
}

EXPORTFUNC Ogre::RibbonTrail* SM_getRibbonTrail( const char* name , Ogre::SceneManager* sm )
{
	return sm->getRibbonTrail( name );
}

EXPORTFUNC int SM_hasRibbonTrail( const char* name , Ogre::SceneManager* sm )
{
	return sm->hasRibbonTrail( name );
}

EXPORTFUNC void SM_destroyRibbonTrail( Ogre::RibbonTrail* obj , Ogre::SceneManager* sm )
{
	sm->destroyRibbonTrail( obj );
}

EXPORTFUNC void SM_destroyRibbonTrailWithName( const char* name , Ogre::SceneManager* sm )
{
	sm->destroyRibbonTrail( name );
}

EXPORTFUNC void SM_destroyAllRibbonTrails( Ogre::SceneManager* sm )
{
	sm->destroyAllRibbonTrails();
}

EXPORTFUNC Ogre::ParticleSystem* SM_createParticleSystem( const char* name , int quota, const char* resourceGroup , Ogre::SceneManager* ogreSceneManager )
{
	return ogreSceneManager->createParticleSystem( name , quota , resourceGroup );
}

EXPORTFUNC Ogre::ParticleSystem* SM_createParticleSystemWithTemplate( const char* name , const char* templateName , Ogre::SceneManager* ogreSceneManager )
{
	return ogreSceneManager->createParticleSystem( name , templateName );
}

EXPORTFUNC Ogre::ParticleSystem* SM_getParticleSystem( const char* name , Ogre::SceneManager* sm )
{
	return sm->getParticleSystem( name );
}

EXPORTFUNC int SM_hasParticleSystem( const char* name , Ogre::SceneManager* sm )
{
	return sm->hasParticleSystem( name );
}

EXPORTFUNC void SM_destroyParticleSystem( Ogre::ParticleSystem* obj , Ogre::SceneManager* sm )
{
	sm->destroyParticleSystem( obj );
}

EXPORTFUNC void SM_destroyParticleSystemWithName( const char* name , Ogre::SceneManager* sm )
{
	sm->destroyParticleSystem( name );
}

EXPORTFUNC void SM_destroyAllParticleSystems( Ogre::SceneManager* sm )
{
	sm->destroyAllParticleSystems();
}

EXPORTFUNC void SM_clearScene( Ogre::SceneManager* sm )
{
	sm->clearScene();
}

EXPORTFUNC void SM_setAmbientLight( Ogre::ColourValue* colour , Ogre::SceneManager* ogreSceneManager )
{
	ogreSceneManager->setAmbientLight( *colour );
}

EXPORTFUNC Ogre::ColourValue* SM_getAmbientLight( Ogre::SceneManager* sm )
{
	return new Ogre::ColourValue( sm->getAmbientLight() );

}

EXPORTFUNC void SM_setWorldGeometry( const char* filename , Ogre::SceneManager* ogreSceneManager )
{
	ogreSceneManager->setWorldGeometry( filename );
}

EXPORTFUNC void SM_setWorldGeometryWithDataStream( Ogre::DataStream* stream , const char* typeName , Ogre::SceneManager* sm )
{
	sm->setWorldGeometry( Ogre::DataStreamPtr( stream ) , typeName );
}

EXPORTFUNC int SM_estimateWorldGeometry( const char* filename , Ogre::SceneManager* sm )
{
	return sm->estimateWorldGeometry( filename );
}

EXPORTFUNC int SM_estimateWorldGeometryWithDataStream( Ogre::DataStream* stream , const char* typeName , Ogre::SceneManager* sm )
{
	return sm->estimateWorldGeometry( Ogre::DataStreamPtr( stream ) , typeName );
}

EXPORTFUNC Ogre::ViewPoint* SM_getSuggestedViewpoint( bool random  , Ogre::SceneManager* sm )
{
	return new Ogre::ViewPoint( sm->getSuggestedViewpoint( random ));
}

EXPORTFUNC int SM_setOption( const char* strKey , void* pValue , Ogre::SceneManager* sm )
{
	return sm->setOption( strKey , pValue );
}

EXPORTFUNC int SM_getOption( const char* strKey , void* pDestValue , Ogre::SceneManager* sm )
{
	return sm->getOption( strKey , pDestValue );
}


EXPORTFUNC int SM_hasOption( const char* strKey , Ogre::SceneManager* sm )
{
	return sm->hasOption( strKey );
}

EXPORTFUNC int SM_getOptionValues( const char* strKey , Ogre::StringVector* refValueList , Ogre::SceneManager* sm )
{
	return sm->getOptionValues( strKey , *refValueList );
}

EXPORTFUNC int SM_getOptionKeys( Ogre::StringVector* refKeys , Ogre::SceneManager* sm )
{
	return sm->getOptionKeys( *refKeys );
}

EXPORTFUNC void SM__updateSceneGraph( Ogre::Camera* cam , Ogre::SceneManager* sm )
{
	sm->_updateSceneGraph( cam );
}

EXPORTFUNC void SM__findVisibleObjects( Ogre::Camera* cam , Ogre::VisibleObjectsBoundsInfo* visibleBounds , bool onlyShadowCasters , Ogre::SceneManager* sm )
{
	sm->_findVisibleObjects( cam , visibleBounds , onlyShadowCasters );
}

EXPORTFUNC void SM__applySceneAnimations( Ogre::SceneManager* sm )
{
	sm->_applySceneAnimations();
}

EXPORTFUNC void SM__renderVisibleObjects( Ogre::SceneManager* sm )
{
	sm->_renderVisibleObjects();
}

EXPORTFUNC void SM__renderScene( Ogre::Camera* camera , Ogre::Viewport* vp , bool includeOverlays , Ogre::SceneManager* sm )
{
	sm->_renderScene( camera , vp , includeOverlays );
}

EXPORTFUNC void SM__queueSkiesForRendering( Ogre::Camera* cam , Ogre::SceneManager* sm )
{
	sm->_queueSkiesForRendering( cam );
}

EXPORTFUNC void SM__setDestinationRenderSystem( Ogre::RenderSystem* sys , Ogre::SceneManager* sm )
{
	sm->_setDestinationRenderSystem( sys );
}

EXPORTFUNC void SM_setSkyPlane( bool enable , Ogre::Plane *plane , const char* materialName , Ogre::SceneManager* ogreSceneManager , float scale , float tiling , bool drawFirst , float bow , int xsegments , int ysegments , const char* groupName )
{
	ogreSceneManager->setSkyPlane( enable , *plane , materialName , scale , tiling , drawFirst , bow , xsegments , ysegments , groupName );
}

EXPORTFUNC int SM_isSkyPlaneEnabled( Ogre::SceneManager* sm )
{
	return sm->isSkyPlaneEnabled();
}

EXPORTFUNC Ogre::SceneNode* SM_getSkyPlaneNode( Ogre::SceneManager* sm )
{
	return sm->getSkyPlaneNode();
}

EXPORTFUNC Ogre::SceneManager::SkyPlaneGenParameters* SM_getSkyPlaneGenParameters( Ogre::SceneManager* sm )
{
	return new Ogre::SceneManager::SkyPlaneGenParameters( sm->getSkyPlaneGenParameters() );
}

EXPORTFUNC void SM_setSkyBox( bool enable , const char* materialName , Ogre::SceneManager* ogreSceneManager , float distance, bool drawFirst , Ogre::Quaternion* orientation , const char* groupName )
{
	ogreSceneManager->setSkyBox( enable , materialName , distance , drawFirst , *orientation , groupName );
}

EXPORTFUNC int SM_isSkyBoxEnabled( Ogre::SceneManager* sm )
{
	return sm->isSkyBoxEnabled();
}

EXPORTFUNC Ogre::SceneNode* SM_getSkyBoxNode( Ogre::SceneManager* sm )
{
	return sm->getSkyBoxNode();
}

EXPORTFUNC Ogre::SceneManager::SkyBoxGenParameters* SM_getSkyBoxGenParameters( Ogre::SceneManager* sm )
{
	return new Ogre::SceneManager::SkyBoxGenParameters( sm->getSkyBoxGenParameters() );
}

EXPORTFUNC void SM_setSkyDome( bool enable , const char* materialName , Ogre::SceneManager* ogreSceneManager , float curvature , float tiling , float distance , bool drawfirst, Ogre::Quaternion* orientation , int xsegments, int ysegments , int ysegments_keep , const char* groupName  )
{
	ogreSceneManager->setSkyDome( enable , materialName , curvature , tiling , distance , drawfirst , *orientation , xsegments , ysegments , ysegments_keep , groupName );
}

EXPORTFUNC int SM_isSkyDomeEnabled( Ogre::SceneManager* sm )
{
	return sm->isSkyDomeEnabled();
}

EXPORTFUNC Ogre::SceneNode* SM_getSkyDomeNode( Ogre::SceneManager* sm )
{
	return sm->getSkyDomeNode();
}

EXPORTFUNC Ogre::SceneManager::SkyDomeGenParameters* SM_getSkyDomeGenParameters( Ogre::SceneManager* sm )
{
	return new Ogre::SceneManager::SkyDomeGenParameters( sm->getSkyDomeGenParameters() );
}

EXPORTFUNC void SM_setFog( int mode , Ogre::ColourValue* colour , float expDensity , float linearStart , float linearEnd , Ogre::SceneManager* sm )
{
	sm->setFog( (Ogre::FogMode)mode , *colour , expDensity , linearStart , linearEnd );
}

EXPORTFUNC int SM_getFogMode( Ogre::SceneManager* sm )
{
	return sm->getFogMode();
}

EXPORTFUNC Ogre::ColourValue* SM_getFogColour( Ogre::SceneManager* sm )
{
	return new Ogre::ColourValue( sm->getFogColour() );
}

EXPORTFUNC float SM_getFogStart( Ogre::SceneManager* sm )
{
	return sm->getFogStart();
}

EXPORTFUNC float SM_getFogEnd( Ogre::SceneManager* sm )
{
	return sm->getFogEnd();
}

EXPORTFUNC float SM_getFogDensity( Ogre::SceneManager* sm )
{
	return sm->getFogDensity();
}

EXPORTFUNC Ogre::BillboardSet* SM_createBillboardSet( const char* name , int poolSize , Ogre::SceneManager* sm )
{
	return sm->createBillboardSet( name , poolSize );
}

EXPORTFUNC Ogre::BillboardSet* SM_getBillboardSet( const char* name , Ogre::SceneManager* sm )
{
	return sm->getBillboardSet( name );
}

EXPORTFUNC int SM_hasBillboardSet( const char* name , Ogre::SceneManager* sm )
{
	return sm->hasBillboardSet( name );
}

EXPORTFUNC void SM_destroyBillboardSet( Ogre::BillboardSet* set  , Ogre::SceneManager* sm )
{
	sm->destroyBillboardSet( set );
}

EXPORTFUNC void SM_destroyBillboardSetWithName( const char* name , Ogre::SceneManager* sm )
{
	sm->destroyBillboardSet( name );
}

EXPORTFUNC void SM_destroyAllBillboardSets( Ogre::SceneManager* sm )
{
	sm->destroyAllBillboardSets();
}

EXPORTFUNC void SM_setDisplaySceneNodes( bool display , Ogre::SceneManager* sm )
{
	sm->setDisplaySceneNodes( display );
}

EXPORTFUNC int SM_getDisplaySceneNodes( Ogre::SceneManager* sm )
{
	return sm->getDisplaySceneNodes();
}

EXPORTFUNC Ogre::Animation* SM_createAnimation( const char* name , float length , Ogre::SceneManager* sm )
{
	return sm->createAnimation( name , length );
}

EXPORTFUNC Ogre::Animation* SM_getAnimation( const char* name , Ogre::SceneManager* sm )
{
	return sm->getAnimation( name );
}

EXPORTFUNC int SM_hasAnimation( const char* name , Ogre::SceneManager* sm )
{
	return sm->hasAnimation( name );
}

EXPORTFUNC void SM_destroyAnimation( const char* name , Ogre::SceneManager* sm )
{
	sm->destroyAnimation( name );
}

EXPORTFUNC void SM_destroyAllAnimations( Ogre::SceneManager* sm )
{
	sm->destroyAllAnimations();
}

EXPORTFUNC Ogre::AnimationState* SM_createAnimationState( const char* animName , Ogre::SceneManager* ogreSceneManager )
{
	return ogreSceneManager->createAnimationState( animName );
}

EXPORTFUNC Ogre::AnimationState* SM_getAnimationState( const char* name , Ogre::SceneManager* sm )
{
	return sm->getAnimationState( name );
}

EXPORTFUNC int SM_hasAnimationState( const char* name , Ogre::SceneManager* sm )
{
	return sm->hasAnimationState( name );
}

EXPORTFUNC void SM_destroyAnimationState( const char* name , Ogre::SceneManager* sm )
{
	sm->destroyAnimationState( name );
}

EXPORTFUNC void SM_destroyAllAnimationStates( Ogre::SceneManager* sm )
{
	sm->destroyAllAnimationStates();
}

EXPORTFUNC void SM_manualRender( Ogre::RenderOperation* rend , Ogre::Pass* pass , Ogre::Viewport* vp , Ogre::Matrix4* worldMatrix , Ogre::Matrix4* viewMatrix , Ogre::Matrix4* projMatrix, bool doBeginEndFrame , Ogre::SceneManager* sm )
{
	sm->manualRender( rend , pass , vp , *worldMatrix , *viewMatrix , *projMatrix , doBeginEndFrame );
}

EXPORTFUNC Ogre::RenderQueue* SM_getRenderQueue( Ogre::SceneManager* sm )
{
	return sm->getRenderQueue();
}

EXPORTFUNC void SM_addRenderQueueListener( Ogre::RenderQueueListener* newListener , Ogre::SceneManager* sm )
{
	sm->addRenderQueueListener( newListener );
}

EXPORTFUNC void SM_removeRenderQueueListener( Ogre::RenderQueueListener* delListener , Ogre::SceneManager* sm )
{
	sm->removeRenderQueueListener( delListener );
}

EXPORTFUNC void SM_addSpecialCaseRenderQueue( int quid , Ogre::SceneManager* sm )
{
	sm->addSpecialCaseRenderQueue( quid );
}

EXPORTFUNC void SM_removeSpecialCaseRenderQueue( int quid , Ogre::SceneManager* sm )
{
	sm->removeSpecialCaseRenderQueue( quid );
}

EXPORTFUNC void SM_clearSpecialCaseRenderQueues( Ogre::SceneManager* sm )
{
	sm->clearSpecialCaseRenderQueues();
}

EXPORTFUNC void SM_setSpecialCaseRenderQueueMode( int mode , Ogre::SceneManager* sm )
{
	sm->setSpecialCaseRenderQueueMode( (Ogre::SceneManager::SpecialCaseRenderQueueMode)mode );
}

EXPORTFUNC int SM_getSpecialCaseRenderQueueMode( Ogre::SceneManager* sm )
{
	return sm->getSpecialCaseRenderQueueMode();
}

EXPORTFUNC int SM_isRenderQueueToBeProcessed( int qid , Ogre::SceneManager* sm )
{
	return sm->isRenderQueueToBeProcessed( qid );
}

EXPORTFUNC void SM_setWorldGeometryRenderQueue( int qid , Ogre::SceneManager* sm )
{
	sm->setWorldGeometryRenderQueue( qid );
}

EXPORTFUNC int SM_getWorldGeometryRenderQueue( Ogre::SceneManager* sm )
{
	return sm->getWorldGeometryRenderQueue();
}

EXPORTFUNC void SM_showBoundingBoxes( bool bShow , Ogre::SceneManager* sm )
{
	sm->showBoundingBoxes( bShow );
}

EXPORTFUNC int SM_getShowBoundingBoxes( Ogre::SceneManager* sm )
{
	return sm->getShowBoundingBoxes();
}

EXPORTFUNC void SM__notifyAutotrackingSceneNode( Ogre::SceneNode* node , bool autoTrack , Ogre::SceneManager* sm )
{
	sm->_notifyAutotrackingSceneNode( node , autoTrack );
}

EXPORTFUNC Ogre::AxisAlignedBoxSceneQuery* SM_createAABBQuery( Ogre::AxisAlignedBox* box , int mask , Ogre::SceneManager* sm )
{
	return sm->createAABBQuery( *box , mask );
}

EXPORTFUNC Ogre::SphereSceneQuery* SM_createSphereQuery( Ogre::Sphere* sphere , int mask , Ogre::SceneManager* sm )
{
	return sm->createSphereQuery( *sphere , mask );
}

EXPORTFUNC Ogre::PlaneBoundedVolumeListSceneQuery* SM_createPlaneBoundedVolumeQuery( Ogre::PlaneBoundedVolumeList* volumes , int mask , Ogre::SceneManager* sm )
{
	return sm->createPlaneBoundedVolumeQuery( *volumes , mask );
}


EXPORTFUNC Ogre::RaySceneQuery* SM_createRayQuery ( Ogre::Ray* ogreRay , Ogre::uint32 mask , Ogre::SceneManager* ogreSceneManager )
{
	return ogreSceneManager->createRayQuery( *ogreRay , mask );
}

EXPORTFUNC Ogre::IntersectionSceneQuery* SM_createIntersectionQuery( int mask , Ogre::SceneManager* sm )
{
	return sm->createIntersectionQuery( mask );
}

EXPORTFUNC void SM_destroyQuery( Ogre::SceneQuery* query , Ogre::SceneManager* sm )
{
	sm->destroyQuery( query );
}

EXPORTFUNC Ogre::SceneManager::CameraIterator* SM_getCameraIterator( Ogre::SceneManager* sm )
{
	return new  Ogre::SceneManager::CameraIterator( sm->getCameraIterator() );
}

EXPORTFUNC Ogre::SceneManager::AnimationIterator* SM_getAnimationIterator( Ogre::SceneManager* sm )
{
	return new Ogre::SceneManager::AnimationIterator( sm->getAnimationIterator() );
}

EXPORTFUNC Ogre::AnimationStateIterator* SM_getAnimationStateIterator( Ogre::SceneManager* sm )
{
	return new Ogre::AnimationStateIterator( sm->getAnimationStateIterator() );
}

EXPORTFUNC void SM_setShadowTechnique( int ShadowType , Ogre::SceneManager* ogreSceneManager )
{
	ogreSceneManager->setShadowTechnique( (Ogre::ShadowTechnique) ShadowType );
}

EXPORTFUNC int SM_getShadowTechnique( Ogre::SceneManager* sm )
{
	return sm->getShadowTechnique();
}

EXPORTFUNC void SM_setShowDebugShadows( bool debug , Ogre::SceneManager* sm )
{
	sm->setShowDebugShadows( debug );
}

EXPORTFUNC int SM_getShowDebugShadows( Ogre::SceneManager* sm )
{
	return sm->getShowDebugShadows();
}

EXPORTFUNC void SM_setShadowColour( Ogre::ColourValue* colour , Ogre::SceneManager* sm )
{
	sm->setShadowColour( *colour );
}

EXPORTFUNC Ogre::ColourValue* SM_getShadowColour( Ogre::SceneManager* sm )
{
	return new Ogre::ColourValue( sm->getShadowColour() );
}

EXPORTFUNC void SM_setShadowDirectionalLightExtrusionDistance( float dist , Ogre::SceneManager* sm )
{
	sm->setShadowDirectionalLightExtrusionDistance( dist );
}

EXPORTFUNC float SM_getShadowDirectionalLightExtrusionDistance( Ogre::SceneManager* sm )
{
	return sm->getShadowDirectionalLightExtrusionDistance();
}

EXPORTFUNC void SM_setShadowFarDistance( float distance , Ogre::SceneManager* sm )
{
	sm->setShadowFarDistance( distance );
}

EXPORTFUNC float SM_getShadowFarDistance( Ogre::SceneManager* sm )
{
	return sm->getShadowFarDistance();
}

EXPORTFUNC void SM_setShadowIndexBufferSize( int size , Ogre::SceneManager* sm )
{
	sm->setShadowIndexBufferSize( size );
}

EXPORTFUNC int SM_getShadowIndexBufferSize( Ogre::SceneManager* sm )
{
	return sm->getShadowIndexBufferSize();
}

EXPORTFUNC void SM_setShadowTextureSize( int size , Ogre::SceneManager* sm )
{
	sm->setShadowTextureSize( size );
}

EXPORTFUNC void SM_setShadowTextureConfig( int shadowIndex , int width , int height , int format , Ogre::SceneManager* sm )
{
	sm->setShadowTextureConfig( shadowIndex , width , height , (Ogre::PixelFormat)format );
}

EXPORTFUNC void SM_setShadowTextureConfigWithConfig( int shadowIndex , Ogre::ShadowTextureConfig* config , Ogre::SceneManager* sm )
{
	sm->setShadowTextureConfig( shadowIndex , *config );
}

EXPORTFUNC Ogre::ConstShadowTextureConfigIterator* SM_getShadowTextureConfigIterator( Ogre::SceneManager* sm )
{
	return new Ogre::ConstShadowTextureConfigIterator( sm->getShadowTextureConfigIterator() );
}

EXPORTFUNC void SM_setShadowTexturePixelFormat( int fmt , Ogre::SceneManager* sm )
{
	sm->setShadowTexturePixelFormat( (Ogre::PixelFormat)fmt );
}

EXPORTFUNC void SM_setShadowTextureCount( int count , Ogre::SceneManager* sm )
{
	sm->setShadowTextureCount( count );
}

EXPORTFUNC int SM_getShadowTextureCount( Ogre::SceneManager* sm )
{
	return sm->getShadowTextureCount();
}

EXPORTFUNC void SM_setShadowTextureSettings( int size , int count , int fmt ,  Ogre::SceneManager* sm )
{
	sm->setShadowTextureSettings( size , count , (Ogre::PixelFormat)fmt );
}

EXPORTFUNC Ogre::Texture* SM_getShadowTexture( int shadowIndex , Ogre::SceneManager* sm )
{
	return sm->getShadowTexture( shadowIndex ).getPointer();
}

EXPORTFUNC void SM_setShadowDirLightTextureOffset( float offset , Ogre::SceneManager* sm )
{
	sm->setShadowDirLightTextureOffset( offset );
}

EXPORTFUNC float SM_getShadowDirLightTextureOffset( Ogre::SceneManager* sm )
{
	return sm->getShadowDirLightTextureOffset();
}

EXPORTFUNC void SM_setShadowTextureFadeStart( float fadeStart , Ogre::SceneManager* sm )
{
	sm->setShadowTextureFadeStart( fadeStart );
}

EXPORTFUNC void SM_setShadowTextureFadeEnd( float fadeEnd , Ogre::SceneManager* sm )
{
	sm->setShadowTextureFadeEnd( fadeEnd );
}

EXPORTFUNC void SM_setShadowTextureSelfShadow( bool selfShadow , Ogre::SceneManager* sm )
{
	sm->setShadowTextureSelfShadow( selfShadow );
}

EXPORTFUNC int SM_getShadowTextureSelfShadow( Ogre::SceneManager* sm )
{
	return sm->getShadowTextureSelfShadow();
}

EXPORTFUNC void SM_setShadowTextureCasterMaterial( const char* name , Ogre::SceneManager* sm )
{
	sm->setShadowTextureCasterMaterial( name );
}

EXPORTFUNC void SM_setShadowTextureReceiverMaterial( const char* name , Ogre::SceneManager* sm )
{
	sm->setShadowTextureReceiverMaterial( name );
}

EXPORTFUNC void SM_setShadowCasterRenderBackFaces( bool bf , Ogre::SceneManager* sm )
{
	sm->setShadowCasterRenderBackFaces( bf );
}

EXPORTFUNC int SM_getShadowCasterRenderBackFaces( Ogre::SceneManager* sm )
{
	return sm->getShadowCasterRenderBackFaces();
}

EXPORTFUNC void SM_setShadowCameraSetup( Ogre::ShadowCameraSetup* shadowSetup , Ogre::SceneManager* sm )
{
	sm->setShadowCameraSetup( Ogre::ShadowCameraSetupPtr( shadowSetup ) );
}

EXPORTFUNC Ogre::ShadowCameraSetup* SM_getShadowCameraSetup( Ogre::SceneManager* sm )
{
	return sm->getShadowCameraSetup().getPointer();
}

EXPORTFUNC void SM_setShadowUseInfiniteFarPlane( bool enable , Ogre::SceneManager* sm )
{
	sm->setShadowUseInfiniteFarPlane( enable );
}

EXPORTFUNC int SM_isShadowTechniqueStencilBased( Ogre::SceneManager* sm )
{
	return sm->isShadowTechniqueStencilBased();
}

EXPORTFUNC int SM_isShadowTechniqueTextureBased( Ogre::SceneManager* sm )
{
	return sm->isShadowTechniqueTextureBased();
}

EXPORTFUNC int SM_isShadowTechniqueModulative( Ogre::SceneManager* sm )
{ 
	return sm->isShadowTechniqueModulative();
}

EXPORTFUNC int SM_isShadowTechniqueAdditive( Ogre::SceneManager* sm )
{
	return sm->isShadowTechniqueAdditive();
}

EXPORTFUNC int SM_isShadowTechniqueIntegrated( Ogre::SceneManager* sm )
{
	return sm->isShadowTechniqueIntegrated();
}

EXPORTFUNC int SM_isShadowTechniqueInUse( Ogre::SceneManager* sm )
{
	return sm->isShadowTechniqueInUse();
}

EXPORTFUNC void SM_addListener( Ogre::SceneManager::Listener* s , Ogre::SceneManager* sm )
{
	sm->addListener( s ); 
}

EXPORTFUNC void SM_removeListener( Ogre::SceneManager::Listener* s , Ogre::SceneManager* sm )
{
	sm->removeListener( s );
}

EXPORTFUNC Ogre::StaticGeometry* SM_createStaticGeometry( const char* name , Ogre::SceneManager* ogreSceneManager )
{
	return ogreSceneManager->createStaticGeometry( name );
}

EXPORTFUNC Ogre::StaticGeometry* SM_getStaticGeometry( const char* name , Ogre::SceneManager* sm ) 
{
	return sm->getStaticGeometry( name );
}

EXPORTFUNC int SM_hasStaticGeometry( const char* name , Ogre::SceneManager* sm )
{
	return sm->hasStaticGeometry( name );
}

EXPORTFUNC void SM_destroyStaticGeometry( Ogre::StaticGeometry* geom , Ogre::SceneManager* sm )
{
	sm->destroyStaticGeometry( geom );
}

EXPORTFUNC void SM_destroyStaticGeometryWithName( const char* name , Ogre::SceneManager* sm )
{
	sm->destroyStaticGeometry( name );
}

EXPORTFUNC void SM_destroyAllStaticGeometry( Ogre::SceneManager* sm )
{
	sm->destroyAllStaticGeometry();
}

EXPORTFUNC Ogre::InstancedGeometry* SM_createInstancedGeometry( const char* name , Ogre::SceneManager* sm ) 
{
	return sm->createInstancedGeometry( name );
}

EXPORTFUNC Ogre::InstancedGeometry* SM_getInstancedGeometry( const char* name , Ogre::SceneManager* sm )
{
	return sm->getInstancedGeometry( name );
}

EXPORTFUNC void SM_destroyInstancedGeometry( Ogre::InstancedGeometry* geom , Ogre::SceneManager* sm )
{
	sm->destroyInstancedGeometry( geom );
}

EXPORTFUNC void SM_destroyInstancedGeometryWithName( const char* name , Ogre::SceneManager* sm )
{
	sm->destroyInstancedGeometry( name );
}

EXPORTFUNC void SM_destroyAllInstancedGeometry( Ogre::SceneManager* sm )
{
	sm->destroyAllInstancedGeometry();
}

EXPORTFUNC Ogre::MovableObject* SM_createMovableObject( const char* name , const char* typeName , Ogre::NameValuePairList* params , Ogre::SceneManager* sm )
{
	return MovableObjectChildCast(sm->createMovableObject( name , typeName , params ));
}

EXPORTFUNC void SM_destroyMovableObjectWithName( const char* name , const char* typeName , Ogre::SceneManager* sm )
{
	sm->destroyMovableObject( name , typeName );
}

EXPORTFUNC void SM_destroyMovableObject( Ogre::MovableObject* m , Ogre::SceneManager* sm )
{
	sm->destroyMovableObject( m );
}

EXPORTFUNC void SM_destroyAllMovableObjectsByType( const char* typeName , Ogre::SceneManager* sm )
{
	sm->destroyAllMovableObjectsByType( typeName );
}

EXPORTFUNC void SM_destroyAllMovableObjects( Ogre::SceneManager* sm )
{
	sm->destroyAllMovableObjects();
}

EXPORTFUNC Ogre::MovableObject* SM_getMovableObject( const char* name , const char* typeName , Ogre::SceneManager* sm )
{
	return MovableObjectChildCast(sm->getMovableObject( name , typeName ));
}

EXPORTFUNC int SM_hasMovableObject( const char* name ,const char* typeName , Ogre::SceneManager* sm )
{
	return sm->hasMovableObject( name , typeName );
}

EXPORTFUNC Ogre::SceneManager::MovableObjectIterator* SM_getMovableObjectIterator( const char* typeName , Ogre::SceneManager* sm )
{
	return new Ogre::SceneManager::MovableObjectIterator( sm->getMovableObjectIterator( typeName ) );
}

EXPORTFUNC void SM_injectMovableObject( Ogre::MovableObject* m , Ogre::SceneManager* sm )
{
	sm->injectMovableObject( m );
} 

EXPORTFUNC void SM_extractMovableObjectWithName( const char* name ,const char* typeName , Ogre::SceneManager* sm )
{
	sm->extractMovableObject( name , typeName );
}

EXPORTFUNC void SM_extractMovableObject( Ogre::MovableObject* m , Ogre::SceneManager* sm )
{
	sm->extractMovableObject( m );
}

EXPORTFUNC void SM_extractAllMovableObjectsByType( const char* typeName , Ogre::SceneManager* sm )
{
	sm->extractAllMovableObjectsByType( typeName );
}

EXPORTFUNC void SM_setVisibilityMask( int mask ,Ogre::SceneManager* sm )
{
	sm->setVisibilityMask( mask );
}

EXPORTFUNC int SM_getVisibilityMask( Ogre::SceneManager* sm )
{
	return sm->getVisibilityMask();
}

EXPORTFUNC int SM__getCombinedVisibilityMask( Ogre::SceneManager* sm )
{
	return sm->_getCombinedVisibilityMask();
}

EXPORTFUNC void SM_setFindVisibleObjects( bool find , Ogre::SceneManager* sm )
{
	sm->setFindVisibleObjects( find );
}

EXPORTFUNC int SM_getFindVisibleObjects( Ogre::SceneManager* sm )
{
	return sm->getFindVisibleObjects();
}

EXPORTFUNC void SM__injectRenderWithPass( Ogre::Pass* pass , Ogre::Renderable* rend , bool shadowDerivation , Ogre::SceneManager* sm )
{
	sm->_injectRenderWithPass( pass , rend , shadowDerivation );
}

EXPORTFUNC void SM__suppressRenderStateChanges( bool suppress , Ogre::SceneManager* sm )
{
	sm->_suppressRenderStateChanges( suppress );
}

EXPORTFUNC int SM__areRenderStateChangesSuppressed( Ogre::SceneManager* sm )
{
	return sm->_areRenderStateChangesSuppressed();
}

EXPORTFUNC const Ogre::Pass* SM__setPass( Ogre::Pass* pass , bool evenIfSuppressed , bool shadowDerivation , Ogre::SceneManager* sm )
{
	return sm->_setPass( pass , evenIfSuppressed , shadowDerivation );
}

EXPORTFUNC void SM__suppressShadows( bool suppress , Ogre::SceneManager* sm )
{
	sm->_suppressShadows( suppress );
}

EXPORTFUNC int SM__areShadowsSuppressed( Ogre::SceneManager* sm )
{
	return sm->_areShadowsSuppressed();
}

EXPORTFUNC void SM__renderQueueGroupObjects( Ogre::RenderQueueGroup* group , int om , Ogre::SceneManager* sm )
{
	sm->_renderQueueGroupObjects( group , (Ogre::QueuedRenderableCollection::OrganisationMode)om );
}

//void SM_setQueuedRenderableVisitor( Ogre::SceneManager::SceneMgrQueuedRenderableVisitor* visitor , Ogre::SceneManager* sm ) - Requires abstraction-
//Ogre::SceneManager::SceneMgrQueuedRenderableVisitor* SM_getQueuedRenderableVisitor( Ogre::SceneManager* sm ) - Requires abstraction-

EXPORTFUNC Ogre::RenderSystem* SM_getDestinationRenderSystem( Ogre::SceneManager* sm )
{
	return sm->getDestinationRenderSystem();
}

EXPORTFUNC Ogre::Viewport* SM_getCurrentViewport( Ogre::SceneManager* sm )
{
	return sm->getCurrentViewport();
}

EXPORTFUNC Ogre::VisibleObjectsBoundsInfo* SM_getVisibleObjectsBoundsInfo( Ogre::Camera* cam , Ogre::SceneManager* sm )
{
	return new Ogre::VisibleObjectsBoundsInfo( sm->getVisibleObjectsBoundsInfo( cam ) );
}

EXPORTFUNC Ogre::VisibleObjectsBoundsInfo* SM_getShadowCasterBoundsInfo( Ogre::Light* light, Ogre::SceneManager* sm )
{
	return new Ogre::VisibleObjectsBoundsInfo( sm->getShadowCasterBoundsInfo( light ) );
}

EXPORTFUNC int SM_WORLD_GEOMETRY_TYPE_MASK()
{
	return Ogre::SceneManager::WORLD_GEOMETRY_TYPE_MASK;
}

EXPORTFUNC int SM_ENTITY_TYPE_MASK()
{
	return Ogre::SceneManager::ENTITY_TYPE_MASK;
}

EXPORTFUNC int SM_FX_TYPE_MASK()
{
	return Ogre::SceneManager::FX_TYPE_MASK;
}

EXPORTFUNC int SM_STATICGEOMETRY_TYPE_MASK()
{
	return Ogre::SceneManager::STATICGEOMETRY_TYPE_MASK;
}
EXPORTFUNC int SM_LIGHT_TYPE_MASK()
{
	return Ogre::SceneManager::LIGHT_TYPE_MASK;
}
EXPORTFUNC int SM_USER_TYPE_MASK_LIMIT()
{
	return Ogre::SceneManager::USER_TYPE_MASK_LIMIT;
}

// Simple types defined in the SceneManager namespace-
EXPORTFUNC void VIEWPOINT_delete( Ogre::ViewPoint* vp )
{
	delete vp;
}








#endif
