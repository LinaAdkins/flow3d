#ifndef H_OGRESIMPLESPLINE
#define H_OGRESIMPLESPLINE

EXPORTFUNC Ogre::SimpleSpline* SIMPLESPLINE_create()
{
	Ogre::SimpleSpline* spline = new Ogre::SimpleSpline;
	return spline;
}

EXPORTFUNC void SIMPLESPLINE_delete(Ogre::SimpleSpline* spline)
{
	delete spline;
}

EXPORTFUNC void SIMPLESPLINE_addPoint( Ogre::Vector3* p , Ogre::SimpleSpline* spline )
{
	spline->addPoint( *p );
}

EXPORTFUNC Ogre::Vector3* SIMPLESPLINE_getPoint( unsigned short index , Ogre::SimpleSpline* spline )
{
	return new Ogre::Vector3( spline->getPoint( index ) );
}


EXPORTFUNC unsigned short SIMPLESPLINE_getNumPoints( Ogre::SimpleSpline* spline )
{
	return spline->getNumPoints();
}

EXPORTFUNC void SIMPLESPLINE_clear( Ogre::SimpleSpline* spline )
{
	spline->clear();
}

EXPORTFUNC void SIMPLESPLINE_updatePoint( unsigned short index , Ogre::Vector3* value , Ogre::SimpleSpline* spline )
{
	spline->updatePoint( index , *value );
}

EXPORTFUNC Ogre::Vector3* SIMPLESPLINE_interpolate( float t , Ogre::SimpleSpline* spline )
{
	return new Ogre::Vector3( spline->interpolate(t) );
}

EXPORTFUNC Ogre::Vector3* SIMPLESPLINE_interpolateWithIndex( unsigned int fromIndex , float t , Ogre::SimpleSpline* spline )
{
	return new Ogre::Vector3( spline->interpolate( fromIndex , t ) );
}

EXPORTFUNC void SIMPLESPLINE_setAutoCalculate( bool autoCalc , Ogre::SimpleSpline* spline )
{
	spline->setAutoCalculate( autoCalc );
}

EXPORTFUNC void SIMPLESPLINE_recalcTangents( Ogre::SimpleSpline* spline )
{
	spline->recalcTangents();
}
	
	

#endif
