#ifndef H_OGREANGLE
#define H_OGREANGLE

EXPORTFUNC Ogre::Angle* ANGLE_create( float theangle )
{
	return new Ogre::Angle( theangle );
}

// Not entirely sure what kind of operator overloading this constitutes as-
//Ogre::Angle::operator Radian() const
//Ogre::Angle::operator Degree() const

EXPORTFUNC void ANGLE_delete( Ogre::Angle* a )
{
	delete a;
}


#endif
