#ifndef H_OGRENAMEVALUEPAIRLIST
#define H_OGRENAMEVALUEPAIRLIST

EXPORTFUNC Ogre::NameValuePairList* NVPL_create()
{
	return new Ogre::NameValuePairList();
}

EXPORTFUNC void NVPL_delete( Ogre::NameValuePairList* nvpl )
{
	delete nvpl;
}

EXPORTFUNC void NVPL_insert( const char* key , const char* value , Ogre::NameValuePairList* nvpl )
{
	nvpl->insert( std::pair<std::string , std::string>( key , value ) );
}

EXPORTFUNC Ogre::NameValuePairList::iterator* NVPL_begin( Ogre::NameValuePairList* l )
{
	return new Ogre::NameValuePairList::iterator(l->begin());
}

EXPORTFUNC Ogre::NameValuePairList::iterator* NVPL_end( Ogre::NameValuePairList* l )
{
	return new Ogre::NameValuePairList::iterator(l->end());
}


// Name Value Pair List Iterator

EXPORTFUNC Ogre::NameValuePairList::iterator* NVPLI_create()
{
	return new Ogre::NameValuePairList::iterator();
}

EXPORTFUNC void NVPLI_delete( Ogre::NameValuePairList::iterator* i )
{
	delete i;
}

EXPORTFUNC void NVPLI_eq( Ogre::NameValuePairList::iterator* rkIterator , Ogre::NameValuePairList::iterator* i )
{
	*i=*rkIterator;
}

EXPORTFUNC bool NVPLI_isNotEqualTo( Ogre::NameValuePairList::iterator* rkIterator , Ogre::NameValuePairList::iterator* i )
{
	return *i!=*rkIterator;
}

EXPORTFUNC bool NVPLI_isEqualTo( Ogre::NameValuePairList::iterator* rkIterator , Ogre::NameValuePairList::iterator* i )
{
	return *i==*rkIterator;
}

EXPORTFUNC void NVPLI_increment( Ogre::NameValuePairList::iterator* i )
{
	++*i;
}

EXPORTFUNC void NVPLI_decrement( Ogre::NameValuePairList::iterator* i )
{
	--*i;
}

EXPORTFUNC const char* NVPLI_first( Ogre::NameValuePairList::iterator* i )
{

	tempString = (*i)->first;
	return tempString.c_str();
}

EXPORTFUNC const char* NVPLI_second( Ogre::NameValuePairList::iterator* i )
{
	tempString = (*i)->second;
	return tempString.c_str();
}



#endif
