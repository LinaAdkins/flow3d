#ifndef H_OGRERAY
#define H_OGRERAY


EXPORTFUNC Ogre::Ray* OGRERAY_createEmpty()
{
	return new Ogre::Ray();
}

EXPORTFUNC Ogre::Ray* OGRERAY_create( Ogre::Vector3* origin , Ogre::Vector3* direction )
{
	return new Ogre::Ray( *origin , *direction );
}

EXPORTFUNC void OGRERAY_setOrigin( Ogre::Vector3* origin , Ogre::Ray* r)
{
	r->setOrigin( *origin );
}
EXPORTFUNC Ogre::Vector3* OGRERAY_getOrigin( Ogre::Ray* r )
{
	return new Ogre::Vector3( r->getOrigin() );
}

EXPORTFUNC void OGRERAY_setDirection( Ogre::Vector3* dir , Ogre::Ray* r )
{
	r->setDirection( *dir );
}

EXPORTFUNC Ogre::Vector3* OGRERAY_getDirection( Ogre::Ray* r )
{
	return new Ogre::Vector3( r->getDirection() );
}

EXPORTFUNC Ogre::Vector3* OGRERAY_getPoint( float t , Ogre::Ray* r )
{
	return new Ogre::Vector3( r->getPoint(t) );
}

//Vector3 	operator * (Real t) const - Unnecessary ( getPoint does the same thing )

EXPORTFUNC std::pair<bool,float>* OGRERAY_intersectsPlane( Ogre::Plane* p , Ogre::Ray* r )
{
	return new std::pair<bool, float>( r->intersects( *p ) );
}
EXPORTFUNC std::pair<bool,float>* OGRERAY_intersectsPlaneBoundedVolume( Ogre::PlaneBoundedVolume* p , Ogre::Ray* r )
{
	return new std::pair<bool , float>( r->intersects( *p ) );
}
EXPORTFUNC std::pair<bool,float>* OGRERAY_intersectsSphere( Ogre::Sphere* s, Ogre::Ray* r )
{
	return new std::pair<bool , float>( r->intersects( *s ) );
}

EXPORTFUNC std::pair<bool,float>* OGRERAY_intersectsAxisAlignedBox( Ogre::AxisAlignedBox* box , Ogre::Ray* r )
{
	return new std::pair<bool , float>( r->intersects( *box ) );
}

EXPORTFUNC const char* OGRERAY_toStr( Ogre::Ray* r )
{
	std::ostringstream os;
	os << "origin{" << r->getOrigin() << "} direction{" << r->getDirection() << "}";
	tempString.clear();
	tempString = os.str();
	return tempString.c_str();
}
	
EXPORTFUNC void OGRERAY_delete( Ogre::Ray* r )
{
	delete r;
}



#endif
