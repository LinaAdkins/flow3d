#ifndef H_TVERTEXLIST
#define H_TVERTEXLIST


typedef std::vector<Ogre::Vector3> VertexList;


EXPORTFUNC VertexList* VERTEXLIST_create()
{
	return new VertexList();
}

EXPORTFUNC void VERTEXLIST_delete( VertexList* l )
{
	delete l;
}

EXPORTFUNC void VERTEXLIST_push_back( Ogre::Vector3* vector , VertexList* l )
{
	l->push_back( *vector );
}

EXPORTFUNC Ogre::Vector3* VERTEXLIST_getValue( int index , VertexList* l )
{
	return &(*l)[index];
}

EXPORTFUNC int VERTEXLIST_size( VertexList* l )
{
	return l->size();
}

#endif
