#ifndef H_OGREBYTEFLOATPAIR
#define H_OGREBYTEFLOATPAIR

EXPORTFUNC std::pair<bool,float>* BYTEFLOATPAIR_create(bool first, float second)
{
	return new std::pair<bool,float>( first , second);
}

EXPORTFUNC void BYTEFLOATPAIR_delete( std::pair<bool,float>* p )
{
	delete p;
}

EXPORTFUNC bool BYTEFLOATPAIR_getFirst( std::pair<bool,float>* p )
{
	return p->first;
}

EXPORTFUNC void BYTEFLOATPAIR_setFirst( bool first , std::pair<bool,float>* p )
{
	p->first = first;
}

EXPORTFUNC float BYTEFLOATPAIR_getSecond( std::pair<bool,float>* p )
{
	return p->second;
}

EXPORTFUNC void BYTEFLOATPAIR_setSecond( float second , std::pair<bool,float>* p )
{
	p->second = second;
}

#endif
