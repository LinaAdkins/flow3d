#ifndef H__TSKELETONMANAGER
#define H__TSKELETONMANAGER


EXPORTFUNC Ogre::Skeleton* SKELETONMANAGER_create( const char* name , const char* group , bool isManual , Ogre::ManualResourceLoader* loader , Ogre::NameValuePairList* createParams )
{
	Ogre::SkeletonPtr ptr = Ogre::SkeletonManager::getSingleton().create( name , group , isManual , loader , createParams );
	return ptr.getPointer();
}

EXPORTFUNC void SKELETONMANAGER_remove( const char* name )
{
	Ogre::SkeletonManager::getSingleton().remove( name );
}

#endif