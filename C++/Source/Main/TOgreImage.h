#ifndef H_OGREIMAGE
#define H_OGREIMAGE

/*

Public Member Functions

*/

EXPORTFUNC Ogre::Image* OGREIMAGE_create()
{
	return new Ogre::Image();
}

/*
Image (const Image &img)
*/

EXPORTFUNC void OGREIMAGE_delete( Ogre::Image* i )
{
	delete i;
}

/*
Image & 	operator= (const Image &img)
Image & 	flipAroundY ()
Image & 	flipAroundX ()
Image & 	loadDynamicImage (uchar *pData, size_t uWidth, size_t uHeight, size_t depth, PixelFormat eFormat, bool autoDelete=false, size_t numFaces=1, size_t numMipMaps=0)
Image & 	loadDynamicImage (uchar *pData, size_t uWidth, size_t uHeight, PixelFormat eFormat)
Image & 	loadRawData (DataStreamPtr &stream, size_t uWidth, size_t uHeight, size_t uDepth, PixelFormat eFormat, size_t numFaces=1, size_t numMipMaps=0)
Image & 	loadRawData (DataStreamPtr &stream, size_t uWidth, size_t uHeight, PixelFormat eFormat)
*/

EXPORTFUNC void OGREIMAGE_load( const char* strFileName , const char* groupName , Ogre::Image* i )
{
	i->load( strFileName , groupName );
}

/*
Image & 	load (DataStreamPtr &stream, const String &type=StringUtil::BLANK)
void 	save (const String &filename)
DataStreamPtr 	encode (const String &formatextension)
uchar * 	getData (void)
const uchar * 	getData () const
size_t 	getSize () const
size_t 	getNumMipmaps () const
bool 	hasFlag (const ImageFlags imgFlag) const
*/

EXPORTFUNC int OGREIMAGE_getWidth( Ogre::Image* i )
{
	return i->getWidth();
}

EXPORTFUNC int OGREIMAGE_getHeight( Ogre::Image* i )
{
	return i->getHeight();
}

/*
size_t 	getWidth (void) const
size_t 	getHeight (void) const
size_t 	getDepth (void) const
size_t 	getNumFaces (void) const
size_t 	getRowSpan (void) const
PixelFormat 	getFormat () const
uchar 	getBPP () const
bool 	getHasAlpha () const
ColourValue 	getColourAt (int x, int y, int z) const
PixelBox 	getPixelBox (size_t face=0, size_t mipmap=0) const
void 	resize (ushort width, ushort height, Filter filter=FILTER_BILINEAR)

/* 
Alloc Operators not used
void * 	operator new (size_t sz, const char *file, int line, const char *func)
void * 	operator new (size_t sz)
void * 	operator new (size_t sz, void *ptr)
void * 	operator new[] (size_t sz, const char *file, int line, const char *func)
void * 	operator new[] (size_t sz)
void 	operator delete (void *ptr)
void 	operator delete (void *ptr, const char *, int, const char *)
void 	operator delete[] (void *ptr)
void 	operator delete[] (void *ptr, const char *, int, const char *)
*/

/*
Static Public Member Functions
static void 	applyGamma (uchar *buffer, Real gamma, size_t size, uchar bpp)
static void 	scale (const PixelBox &src, const PixelBox &dst, Filter filter=FILTER_BILINEAR)
static size_t 	calculateSize (size_t mipmaps, size_t faces, size_t width, size_t height, size_t depth, PixelFormat format)
static String 	getFileExtFromMagic (DataStreamPtr stream)

*/

// Function will return TFGImage
// Needs corresponding DrawImage that works with
EXPORTFUNC void FLOW3D_ImageLoadTest( const char* filename)
{
	// Use Ogre::Image to load filename.
	// TextureManager.LoadImage
	// Create a material for the image and place it on that.

}

#endif