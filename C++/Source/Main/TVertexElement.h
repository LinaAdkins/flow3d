#ifndef H_TVERTEXELEMENT
#define H_TVERTEXELEMENT

EXPORTFUNC void VERTEXELEMENT_delete( Ogre::VertexElement* e )
{
	delete e;
}

// Static Public Functions

EXPORTFUNC int VERTEXELEMENT_getTypeSize( int etype )
{
	return Ogre::VertexElement::getTypeSize( Ogre::VertexElementType( etype ) );
}

#endif