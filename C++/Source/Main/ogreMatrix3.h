#ifndef H_OGREMATRIX3
#define H_OGREMATRIX3

EXPORTFUNC Ogre::Matrix3* MATRIX3_createEmpty()
{
	return new Ogre::Matrix3();
}

//Matrix3 (const Real arr[3][3]) - Pointer logic is not compatible, ignored*-

EXPORTFUNC Ogre::Matrix3* MATRIX3_createWithMatrix3( Ogre::Matrix3* mat )
{
	return new Ogre::Matrix3( *mat );
}

EXPORTFUNC Ogre::Matrix3* MATRIX3_create( float fEntry00 , float fEntry01 , float fEntry02 , float fEntry10 , float fEntry11 , float fEntry12 , float fEntry20 , float fEntry21 , float fEntry22 )
{
	return new Ogre::Matrix3( fEntry00 , fEntry01 , fEntry02 , fEntry10 , fEntry11 , fEntry12 , fEntry20, fEntry21 , fEntry22 );
}

// Name changed since [] operators cannot be used-
EXPORTFUNC float MATRIX3_getValue( int iRow , int iColumn , Ogre::Matrix3* matrix )
{
	return (*matrix)[iRow][iColumn];
}

EXPORTFUNC Ogre::Vector3* MATRIX3_GetColumn( int iCol , Ogre::Matrix3* matrix )
{
	return new Ogre::Vector3( matrix->GetColumn(iCol) );
}

EXPORTFUNC void MATRIX3_SetColumn( int iCol , Ogre::Vector3* vec , Ogre::Matrix3* matrix )
{
	matrix->SetColumn( iCol , *vec );
}

EXPORTFUNC void MATRIX3_FromAxes( Ogre::Vector3* xAxis , Ogre::Vector3* yAxis , Ogre::Vector3* zAxis , Ogre::Matrix3* matrix )
{
	matrix->FromAxes( *xAxis , *yAxis , *zAxis );
}

EXPORTFUNC void MATRIX3_eq( Ogre::Matrix3* rkMatrix , Ogre::Matrix3* matrix )
{
	*matrix = *rkMatrix;
}

EXPORTFUNC int MATRIX3_isEqualTo( Ogre::Matrix3* rkMatrix , Ogre::Matrix3* matrix )
{
	return *matrix == *rkMatrix;
}

EXPORTFUNC int MATRIX3_isNotEqualTo( Ogre::Matrix3* rkMatrix , Ogre::Matrix3* matrix )
{
	return *matrix != *rkMatrix;
}

EXPORTFUNC Ogre::Matrix3* MATRIX3_add( Ogre::Matrix3* rkMatrix , Ogre::Matrix3* matrix )
{
	return new Ogre::Matrix3( *rkMatrix+*matrix );
}

EXPORTFUNC Ogre::Matrix3* MATRIX3_sub( Ogre::Matrix3* rkMatrix , Ogre::Matrix3* matrix )
{
	return new Ogre::Matrix3( *rkMatrix-*matrix );
}

EXPORTFUNC Ogre::Matrix3* MATRIX3_mul( Ogre::Matrix3* rkMatrix , Ogre::Matrix3* matrix )
{
	return new Ogre::Matrix3( *rkMatrix * *matrix );
}

EXPORTFUNC Ogre::Matrix3* MATRIX3_neg(  Ogre::Matrix3* matrix )
{
	return new Ogre::Matrix3( -(*matrix) );
}

EXPORTFUNC Ogre::Vector3* MATRIX3_mulWithVector3( Ogre::Vector3* rkVector , Ogre::Matrix3* matrix )
{
	return new Ogre::Vector3( *matrix * *rkVector );
}

EXPORTFUNC Ogre::Matrix3* MATRIX3_mulWithScalar( float fScalar , Ogre::Matrix3* matrix )
{
	return new Ogre::Matrix3( fScalar * *matrix );
}
EXPORTFUNC Ogre::Matrix3* MATRIX3_Transpose( Ogre::Matrix3* matrix )
{
	return new Ogre::Matrix3( matrix->Transpose() );
}

EXPORTFUNC int MATRIX3_InverseWithMatrix3( Ogre::Matrix3* rkInverse , float fTolerance , Ogre::Matrix3* matrix )
{
	return matrix->Inverse( *rkInverse , fTolerance );
}

EXPORTFUNC Ogre::Matrix3* MATRIX3_Inverse( float fTolerance , Ogre::Matrix3* matrix )
{
	return new Ogre::Matrix3( matrix->Inverse( fTolerance ));
}

EXPORTFUNC float MATRIX3_Determinant( Ogre::Matrix3* matrix )
{
	return matrix->Determinant();
}

EXPORTFUNC void MATRIX3_SingularValueDecomposition( Ogre::Matrix3* rkL , Ogre::Vector3* rkS , Ogre::Matrix3* rkR , Ogre::Matrix3* matrix )
{
	matrix->SingularValueDecomposition( *rkL , *rkS , *rkR );
}

//void 	SingularValueComposition (const Matrix3 &rkL, const Vector3 &rkS, const Matrix3 &rkR) - unnecessary

EXPORTFUNC void MATRIX3_Orthonormalize( Ogre::Matrix3* matrix )
{
	matrix->Orthonormalize();
}

EXPORTFUNC void MATRIX3_QDUDecomposition( Ogre::Matrix3* rkQ , Ogre::Vector3* rkD , Ogre::Vector3* rkU , Ogre::Matrix3* matrix )
{
	matrix->QDUDecomposition( *rkQ , *rkD , *rkU );
}

EXPORTFUNC float MATRIX3_SpectralNorm( Ogre::Matrix3* matrix )
{
	return matrix->SpectralNorm();
}

EXPORTFUNC void MATRIX3_ToAxisAngle( Ogre::Vector3* rkAxis , Ogre::Radian* rfAngle , Ogre::Matrix3* matrix )
{
	matrix->ToAxisAngle( *rkAxis , *rfAngle );
}

EXPORTFUNC void MATRIX3_ToAxisAngleWithDegrees( Ogre::Vector3* rkAxis , Ogre::Degree* rfAngle , Ogre::Matrix3* matrix )
{
	matrix->ToAxisAngle( *rkAxis , *rfAngle );
}

EXPORTFUNC void MATRIX3_FromAxisAngle( Ogre::Vector3* rkAxis , Ogre::Radian* fRadians , Ogre::Matrix3* matrix )
{
	matrix->FromAxisAngle( *rkAxis , *fRadians );
}

EXPORTFUNC int MATRIX3_ToEulerAnglesXYZ( Ogre::Radian* rfYAngle , Ogre::Radian* rfPAngle , Ogre::Radian* rfRAngle , Ogre::Matrix3* matrix )
{
	return matrix->ToEulerAnglesXYZ( *rfYAngle , *rfPAngle , *rfRAngle );
}

EXPORTFUNC int MATRIX3_ToEulerAnglesXZY( Ogre::Radian* rfYAngle , Ogre::Radian* rfPAngle , Ogre::Radian* rfRAngle , Ogre::Matrix3* matrix )
{
	return matrix->ToEulerAnglesXZY( *rfYAngle , *rfPAngle , *rfRAngle );
}

EXPORTFUNC int MATRIX3_ToEulerAnglesYXZ( Ogre::Radian* rfYAngle , Ogre::Radian* rfPAngle , Ogre::Radian* rfRAngle , Ogre::Matrix3* matrix )
{
	return matrix->ToEulerAnglesYXZ( *rfYAngle , *rfPAngle , *rfRAngle );
}

EXPORTFUNC int MATRIX3_ToEulerAnglesYZX( Ogre::Radian* rfYAngle , Ogre::Radian* rfPAngle , Ogre::Radian* rfRAngle , Ogre::Matrix3* matrix )
{
	return matrix->ToEulerAnglesYZX( *rfYAngle , *rfPAngle , *rfRAngle );
}

EXPORTFUNC int MATRIX3_ToEulerAnglesZXY( Ogre::Radian* rfYAngle , Ogre::Radian* rfPAngle , Ogre::Radian* rfRAngle , Ogre::Matrix3* matrix )
{
	return matrix->ToEulerAnglesZXY( *rfYAngle , *rfPAngle , *rfRAngle );
}

EXPORTFUNC int MATRIX3_ToEulerAnglesZYX( Ogre::Radian* rfYAngle , Ogre::Radian* rfPAngle , Ogre::Radian* rfRAngle , Ogre::Matrix3* matrix )
{
	return matrix->ToEulerAnglesZYX( *rfYAngle , *rfPAngle , *rfRAngle );
}

EXPORTFUNC void MATRIX3_FromEulerAnglesXYZ( Ogre::Radian* fYAngle , Ogre::Radian* fPAngle , Ogre::Radian* fRAngle , Ogre::Matrix3* matrix )
{
	matrix->FromEulerAnglesXYZ( *fYAngle , *fPAngle , *fRAngle );
}

EXPORTFUNC void MATRIX3_FromEulerAnglesXZY( Ogre::Radian* fYAngle , Ogre::Radian* fPAngle , Ogre::Radian* fRAngle , Ogre::Matrix3* matrix )
{
	matrix->FromEulerAnglesXZY( *fYAngle , *fPAngle , *fRAngle );
}

EXPORTFUNC void MATRIX3_FromEulerAnglesYXZ( Ogre::Radian* fYAngle , Ogre::Radian* fPAngle , Ogre::Radian* fRAngle , Ogre::Matrix3* matrix )
{
	matrix->FromEulerAnglesYXZ( *fYAngle , *fPAngle , *fRAngle );
}

EXPORTFUNC void MATRIX3_FromEulerAnglesYZX( Ogre::Radian* fYAngle , Ogre::Radian* fPAngle , Ogre::Radian* fRAngle , Ogre::Matrix3* matrix )
{
	matrix->FromEulerAnglesYZX( *fYAngle , *fPAngle , *fRAngle );
}

EXPORTFUNC void MATRIX3_FromEulerAnglesZXY( Ogre::Radian* fYAngle , Ogre::Radian* fPAngle , Ogre::Radian* fRAngle , Ogre::Matrix3* matrix )
{
	matrix->FromEulerAnglesZXY( *fYAngle , *fPAngle , *fRAngle );
}

EXPORTFUNC void MATRIX3_FromEulerAnglesZYX( Ogre::Radian* fYAngle , Ogre::Radian* fPAngle , Ogre::Radian* fRAngle , Ogre::Matrix3* matrix )
{
	matrix->FromEulerAnglesZYX( *fYAngle , *fPAngle , *fRAngle );
}

EXPORTFUNC void MATRIX3_EigenSolveSymmetric( float k1 , float k2 , float k3 , Ogre::Vector3* v1 , Ogre::Vector3* v2 , Ogre::Vector3* v3 , Ogre::Matrix3* matrix )
{
	Ogre::Vector3 eigenVectors[3] = { *v1 , *v2 , *v3 };
	float eigenValues[3] = { k1 , k2 , k3 };
	matrix->EigenSolveSymmetric( eigenValues ,  eigenVectors );
}

EXPORTFUNC void MATRIX3_delete( Ogre::Matrix3* mat )
{
	delete mat;
}

EXPORTFUNC const char* MATRIX3_toStr( Ogre::Matrix3* matrix )
{
	std::ostringstream os;

	os << "Matrix3(";
	for (size_t i = 0; i < 3; ++i)
	{
		os << " row" << (unsigned)i << "{";
		for(size_t j = 0; j < 3; ++j)
		{
			os << (*matrix)[i][j] << " ";
		}
		os << "}";
	}
	os << ")"; 

	tempString.clear();
	tempString = os.str();
	return tempString.c_str();
}
// Static Public Member Functions
EXPORTFUNC void MATRIX3_TensorProduct( Ogre::Vector3* rkU , Ogre::Vector3* rkV , Ogre::Matrix3* rkProduct )
{
	Ogre::Matrix3::TensorProduct( *rkU , *rkV , *rkProduct );
}

// Static Public Attributes
EXPORTFUNC float MATRIX3_EPSILON()
{
	return Ogre::Matrix3::EPSILON;
}

EXPORTFUNC Ogre::Matrix3* MATRIX3_ZERO()
{
	return (Ogre::Matrix3*)&Ogre::Matrix3::ZERO;
}

EXPORTFUNC Ogre::Matrix3* MATRIX3_IDENTITY()
{
	return (Ogre::Matrix3*)&Ogre::Matrix3::IDENTITY;
}







#endif
