#ifndef H_TOVERLAYELEMENT
#define H_TOVERLAYELEMENT

//OverlayElement (const String &name)	// Managed
//virtual 	~OverlayElement ()	// Managed

EXPORTFUNC void OVERLAYELEM_initialise( Ogre::OverlayElement* e )
{
	e->initialise();
}

EXPORTFUNC const char* OVERLAYELEM_getName( Ogre::OverlayElement* e )
{
	tempString = e->getName();
	return tempString.c_str();
}

EXPORTFUNC void OVERLAYELEM_show( Ogre::OverlayElement* e )
{
	e->show();
}

EXPORTFUNC void OVERLAYELEM_hide( Ogre::OverlayElement* e )
{
	e->hide();
}

EXPORTFUNC bool OVERLAYELEM_isVisible( Ogre::OverlayElement* e )
{
	return e->isVisible();
}

EXPORTFUNC bool OVERLAYELEM_isEnabled( Ogre::OverlayElement* e )
{
	return e->isEnabled();
}

EXPORTFUNC void OVERLAYELEM_setEnabled( bool b ,  Ogre::OverlayElement* e )
{
	e->setEnabled(b);
}

EXPORTFUNC void OVERLAYELEM_setDimensions( float width , float height , Ogre::OverlayElement* e )
{
	e->setDimensions( width , height );
}

EXPORTFUNC void OVERLAYELEM_setPosition( float left , float top , Ogre::OverlayElement* e )
{
	e->setPosition( left , top );
}

EXPORTFUNC void OVERLAYELEM_setWidth( float width , Ogre::OverlayElement* e )
{
	e->setWidth( width );
}

EXPORTFUNC float OVERLAYELEM_getWidth( Ogre::OverlayElement* e )
{
	return e->getWidth();
}

EXPORTFUNC void OVERLAYELEM_setHeight( float height , Ogre::OverlayElement* e )
{
	e->setHeight( height );
}

EXPORTFUNC float OVERLAYELEM_getHeight( Ogre::OverlayElement* e )
{
	return e->getHeight();
}

EXPORTFUNC void OVERLAYELEM_setLeft( float left , Ogre::OverlayElement* e )
{
	e->setLeft( left );
}

EXPORTFUNC float OVERLAYELEM_getLeft( Ogre::OverlayElement* e )
{
	return e->getLeft();
}

EXPORTFUNC void OVERLAYELEM_setTop( float Top , Ogre::OverlayElement* e )
{
	e->setTop( Top );
}

EXPORTFUNC float OVERLAYELEM_getTop( Ogre::OverlayElement* e )
{
	return e->getTop();
}

EXPORTFUNC const char* OVERLAYELEM_getMaterialName( Ogre::OverlayElement* e )
{
	tempString = e->getMaterialName();
	return tempString.c_str();
}

EXPORTFUNC void OVERLAYELEM_setMaterialName( const char*  matName , Ogre::OverlayElement* e )
{
	e->setMaterialName( matName );
}

/*
// Slightly more efficient, internal accessors not necessary
	Real 	_getLeft (void) const
	Real 	_getTop (void) const
	Real 	_getWidth (void) const
	Real 	_getHeight (void) const
	void 	_setLeft (Real left)
	void 	_setTop (Real top)
	void 	_setWidth (Real width)
	void 	_setHeight (Real height)
	void 	_setPosition (Real left, Real top)
	void 	_setDimensions (Real width, Real height)
*/

// Renderable Methods ---

EXPORTFUNC Ogre::MaterialPtr* OVERLAYELEM_getMaterial( Ogre::OverlayElement* e )
{
	return new Ogre::MaterialPtr( e->getMaterial() );
}

EXPORTFUNC void OVERLAYELEM_getWorldTransforms( Ogre::Matrix4* xform , Ogre::OverlayElement* e )
{
	e->getWorldTransforms( xform );
}

// /Renderable Methods ---

EXPORTFUNC void OVERLAYELEM__positionsOutOfDate( Ogre::OverlayElement* e )
{
	e->_positionsOutOfDate();
}

EXPORTFUNC void OVERLAYELEM__update( Ogre::OverlayElement* e )
{
	e->_update();
}

EXPORTFUNC void OVERLAYELEM__updateFromParent( Ogre::OverlayElement* e )
{
	e->_updateFromParent();
}

EXPORTFUNC void OVERLAYELEM__notifyParent( Ogre::OverlayContainer* parent , Ogre::Overlay* overlay , Ogre::OverlayElement* e )
{
	e->_notifyParent( parent , overlay );
}

EXPORTFUNC float OVERLAYELEM__getDerivedLeft( Ogre::OverlayElement* e )
{
	return e->_getDerivedLeft();
}

EXPORTFUNC float OVERLAYELEM__getDerivedTop( Ogre::OverlayElement* e )
{
	return e->_getDerivedTop();
}

EXPORTFUNC float OVERLAYELEM__getRelativeWidth( Ogre::OverlayElement* e )
{
	return e->_getRelativeWidth();
}

EXPORTFUNC float OVERLAYELEM__getRelativeHeight( Ogre::OverlayElement* e )
{
	return e->_getRelativeHeight();
}


EXPORTFUNC void OVERLAYELEM__getClippingRegion( Ogre::Rectangle* clippingRegion , Ogre::OverlayElement* e )
{
	e->_getClippingRegion( *clippingRegion );
}

EXPORTFUNC int OVERLAYELEM__notifyZOrder( int newZOrder , Ogre::OverlayElement* e )
{
	return e->_notifyZOrder( newZOrder );
}

EXPORTFUNC void OVERLAYELEM__notifyWorldTransforms( Ogre::Matrix4* xform , Ogre::OverlayElement* e )
{
	e->_notifyWorldTransforms( *xform );
}

EXPORTFUNC void OVERLAYELEM__notifyViewport( Ogre::OverlayElement* e )
{
	e->_notifyViewport();
}

EXPORTFUNC void OVERLAYELEM__updateRenderQueue( Ogre::RenderQueue* queue , Ogre::OverlayElement* e )
{
	e->_updateRenderQueue( queue );
}

EXPORTFUNC const char* OVERLAYELEM_getTypeName( Ogre::OverlayElement* e )
{
	tempString = e->getTypeName();
	return tempString.c_str();
}

EXPORTFUNC void OVERLAYELEM_setCaption( const char* text , Ogre::OverlayElement* e )
{
	e->setCaption( text );
}

EXPORTFUNC const char* OVERLAYELEM_getCaption( Ogre::OverlayElement* e )
{
	tempString = e->getCaption();
	return tempString.c_str();
}

EXPORTFUNC void OVERLAYELEM_setColour( Ogre::ColourValue* col , Ogre::OverlayElement* e )
{
	e->setColour( *col );
}

EXPORTFUNC Ogre::ColourValue* OVERLAYELEM_getColour( Ogre::OverlayElement* e )
{
	return new Ogre::ColourValue( e->getColour() );
}

EXPORTFUNC void OVERLAYELEM_setMetricsMode( int gmm , Ogre::OverlayElement* e )
{
	e->setMetricsMode( Ogre::GuiMetricsMode( gmm ) );
}

EXPORTFUNC int OVERLAYELEM_getMetricsMode( Ogre::OverlayElement* e )
{
	return e->getMetricsMode();
}

EXPORTFUNC void OVERLAYELEM_setHorizontalAlignment( int gha , Ogre::OverlayElement* e )
{
	e->setHorizontalAlignment( Ogre::GuiHorizontalAlignment( gha ) );
}

EXPORTFUNC int OVERLAYELEM_getHorizontalAlignment( Ogre::OverlayElement* e )
{
	return e->getHorizontalAlignment();
}

EXPORTFUNC void OVERLAYELEM_setVerticalAlignment( int gva , Ogre::OverlayElement* e )
{
	e->setVerticalAlignment( Ogre::GuiVerticalAlignment( gva ) );
}

EXPORTFUNC int OVERLAYELEM_getVerticalAlignment( Ogre::OverlayElement* e )
{
	return e->getVerticalAlignment();
}

EXPORTFUNC bool OVERLAYELEM_contains( float x , float y , Ogre::OverlayElement* e )
{
	return e->contains( x , y );
}

EXPORTFUNC Ogre::OverlayElement* OVERLAYELEM_findElementAt( float x , float y , Ogre::OverlayElement* e )
{
	return e->findElementAt( x , y );
}

EXPORTFUNC bool OVERLAYELEM_isContainer( Ogre::OverlayElement* e )
{
	return e->isContainer();
}

EXPORTFUNC bool OVERLAYELEM_isKeyEnabled( Ogre::OverlayElement* e )
{
	return e->isKeyEnabled();
}

EXPORTFUNC bool OVERLAYELEM_isCloneable( Ogre::OverlayElement* e )
{
	return e->isCloneable();
}

EXPORTFUNC void OVERLAYELEM_setCloneable( bool c , Ogre::OverlayElement* e )
{
	e->setCloneable( c );
}

EXPORTFUNC Ogre::OverlayContainer* OVERLAYELEM_getParent( Ogre::OverlayElement* e )
{
	return e->getParent();
}

EXPORTFUNC void OVERLAYELEM__setParent( Ogre::OverlayContainer* parent , Ogre::OverlayElement* e )
{
	e->_setParent( parent );
}

EXPORTFUNC int OVERLAYELEM_getZOrder( Ogre::OverlayElement* e )
{
	return e->getZOrder();
}

// Renderable
EXPORTFUNC float OVERLAYELEM_getSquaredViewDepth( Ogre::Camera* cam , Ogre::OverlayElement* e )
{
	return e->getSquaredViewDepth( cam );
}

EXPORTFUNC Ogre::LightList* OVERLAYELEM_getLights( Ogre::OverlayElement* e )
{
	return new Ogre::LightList( e->getLights() );
}

// /Renderable

EXPORTFUNC void OVERLAYELEM_copyFromTemplate( Ogre::OverlayElement* templateOverlay , Ogre::OverlayElement* e )
{
	e->copyFromTemplate( templateOverlay );
}

EXPORTFUNC Ogre::OverlayElement* OVERLAYELEM_clone( const char* instanceName , Ogre::OverlayElement* e )
{
	return e->clone( instanceName );
}

EXPORTFUNC const Ogre::OverlayElement* OVERLAYELEM_getSourceTemplate( Ogre::OverlayElement* e )
{
	return e->getSourceTemplate();
}

// StringInterface

EXPORTFUNC Ogre::ParamDictionary* OVERLAYELEM_getParamDictionary( Ogre::OverlayElement* e )
{
	return e->getParamDictionary();
}

// const ParamDictionary * 	getParamDictionary (void) const - Unnecessary.

EXPORTFUNC Ogre::ParameterList* OVERLAYELEM_getParameters( Ogre::OverlayElement* e )
{
	return new Ogre::ParameterList( e->getParameters() );
}

EXPORTFUNC bool OVERLAYELEM_setParameter( const char* name , const char* value , Ogre::OverlayElement* e )
{
	return e->setParameter( name, value );
}

EXPORTFUNC void OVERLAYELEM_setParameterList( Ogre::NameValuePairList* paramList , Ogre::OverlayElement* e )
{
	e->setParameterList( *paramList );
}

EXPORTFUNC const char* OVERLAYELEM_getParameter( const char* name , Ogre::OverlayElement* e )
{
	tempString = e->getParameter( name );
	return tempString.c_str();
}

EXPORTFUNC void OVERLAYELEM_copyParametersTo( Ogre::StringInterface* dest , Ogre::OverlayElement* e )
{
	e->copyParametersTo( dest );
}

// /StringInterface

// Renderable

EXPORTFUNC Ogre::Technique* OVERLAYELEM_getTechnique( Ogre::OverlayElement* e  )
{
	return e->getTechnique();
}

EXPORTFUNC void OVERLAYELEM_getRenderOperation( Ogre::RenderOperation* op , Ogre::OverlayElement* e  )
{
	e->getRenderOperation( *op );
}


// Shoggoth Methods
/*
void OVERLAYELEM_preRender( Ogre::SceneManager* sm , Ogre::RenderSystem* rs , Ogre::OverlayElement* e )
{
	e->preRender( sm , rs );
}

void OVERLAYELEM_postRender( Ogre::SceneManager* sm , Ogre::RenderSystem* rs , Ogre::OverlayElement* e )
{
	e->postRender( sm , rs );
}
*/

EXPORTFUNC int OVERLAYELEM_getNumWorldTransforms( Ogre::OverlayElement* e )
{
	return e->getNumWorldTransforms();
}

EXPORTFUNC void OVERLAYELEM_setUseIdentityProjection( bool useIdentityProjection , Ogre::OverlayElement* e )
{
	e->setUseIdentityProjection( useIdentityProjection );
}

EXPORTFUNC bool OVERLAYELEM_getUseIdentityProjection( Ogre::OverlayElement* e )
{
	return e->getUseIdentityProjection();
}

EXPORTFUNC void OVERLAYELEM_setUseIdentityView( bool useIdentityView , Ogre::OverlayElement* e )
{
	return e->setUseIdentityView( useIdentityView );
}

EXPORTFUNC bool OVERLAYELEM_getUseIdentityView( Ogre::OverlayElement* e )
{
	return e->getUseIdentityView();
}

EXPORTFUNC bool OVERLAYELEM_getCastsShadows( Ogre::OverlayElement* e  )
{
	return e->getCastsShadows();
}

EXPORTFUNC void OVERLAYELEM_setCustomParameter( int index , Ogre::Vector4* value , Ogre::OverlayElement* e  )
{
	e->setCustomParameter( 0 , *value );
}

EXPORTFUNC Ogre::Vector4* OVERLAYELEM_getCustomParameter( int index , Ogre::OverlayElement* e  )
{
	return new Ogre::Vector4( e->getCustomParameter( index ) );
}

EXPORTFUNC void OVERLAYELEM__updateCustomGpuParameter( Ogre::GpuProgramParameters::AutoConstantEntry* constantEntry , Ogre::GpuProgramParameters* params , Ogre::OverlayElement* e  )
{
	e->_updateCustomGpuParameter( *constantEntry , params );
}

EXPORTFUNC void OVERLAYELEM_setPolygonModeOverrideable( bool overrideable , Ogre::OverlayElement* e  )
{
	e->setPolygonModeOverrideable( overrideable );
}

EXPORTFUNC bool OVERLAYELEM_getPolygonModeOverrideable( Ogre::OverlayElement* e  )
{
	return e->getPolygonModeOverrideable();
}

//virtual void 	setUserAny (const Any &anything) - Not supported
//virtual const Any & 	getUserAny (void) const - Not supported

// Shoggoth Methods
/*
Ogre::renderSystemData* OVERLAYELEM_getRenderSystemData( Ogre::OverlayElement* e )
{
	return e->getRenderSystemData();
}

void OVERLAYELEM_setRenderSystemData( Ogre::RenderSystemData* val , Ogre::OverlayElement* e )
{
	e->setRenderSystemDat();
}
*/

// /Renderable

// Static Methods

// StringInterface
EXPORTFUNC void OVERLAYELEM_cleanupDictionary()
{
	Ogre::OverlayElement::cleanupDictionary();
}
// /StringInterface




#endif
