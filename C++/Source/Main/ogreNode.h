#ifndef H_OGRENODE
#define H_OGRENODE

EXPORTFUNC const char* NODE_getName( Ogre::Node* n )
{
	return n->getName().c_str();	
}

EXPORTFUNC Ogre::Node* NODE_getParent( Ogre::Node* n )
{
	return n->getParent();
}

EXPORTFUNC Ogre::Quaternion* NODE_getOrientation( Ogre::Node* n )
{
	return new Ogre::Quaternion( n->getOrientation() );
}

EXPORTFUNC void NODE_setOrientationWithQuaternion( Ogre::Quaternion* q , Ogre::Node* n )
{
	n->setOrientation( *q );
}

EXPORTFUNC void NODE_setOrientation( float w , float x , float y , float z , Ogre::Node* n )
{
	n->setOrientation( w , x , y , z );
}

EXPORTFUNC void NODE_resetOrientation( Ogre::Node* n )
{
	n->resetOrientation();
}

EXPORTFUNC void NODE_setInitialState( Ogre::Node* n )
{
	n->setInitialState();
}

EXPORTFUNC void NODE_resetToInitialState( Ogre::Node* n )
{
	n->resetToInitialState();
}


EXPORTFUNC void NODE_setPositionWithVector3( Ogre::Vector3* pos , Ogre::Node* n )
{
	n->setPosition( *pos );
}
EXPORTFUNC void NODE_setPosition( float x , float y , float z , Ogre::Node* n)
{
	n->setPosition( x , y , z );
}

EXPORTFUNC Ogre::Vector3* NODE_getPosition( Ogre::Node* n )
{
	return new Ogre::Vector3( n->getPosition() );
}

EXPORTFUNC void NODE_setScaleWithVector3( Ogre::Vector3* scale , Ogre::Node* n )
{
	n->setScale( *scale );
}


EXPORTFUNC void NODE_setScale( float x , float y , float z , Ogre::Node* n )
{
	n->setScale( x , y , z );
}

EXPORTFUNC Ogre::Vector3* NODE_getScale( Ogre::Node* n )
{
	return new Ogre::Vector3( n->getScale() );
}

EXPORTFUNC void NODE_setInheritOrientation( bool inherit , Ogre::Node* n )
{
	n->setInheritOrientation( inherit );
}

EXPORTFUNC bool NODE_getInheritOrientation( Ogre::Node* n )
{
	return n->getInheritOrientation();
}

EXPORTFUNC void NODE_setInheritScale( bool inherit , Ogre::Node* n )
{
	n->setInheritScale( inherit );
}

EXPORTFUNC bool NODE_getInheritScale( Ogre::Node* n )
{
	return n->getInheritScale();
}

EXPORTFUNC void NODE_scaleWithVector3( Ogre::Vector3* scale , Ogre::Node* n )
{
	n->scale( *scale );
}

EXPORTFUNC void NODE_scale ( float x, float y, float z , Ogre::Node* n )
{
	n->scale(  x , y , z );
}

EXPORTFUNC void NODE_translateWithVector3( Ogre::Vector3* d , int relativeTo, Ogre::Node* n )
{
	n->translate( *d , (Ogre::Node::TransformSpace)relativeTo );
}

EXPORTFUNC void NODE_translate(float x, float y, float z, int relativeTo, Ogre::Node* n)
{
	n->translate( x , y , z , (Ogre::Node::TransformSpace) relativeTo );
}

EXPORTFUNC void NODE_translateAlongAxesWithVector3( Ogre::Matrix3* axes ,  Ogre::Vector3* move , int relativeTo , Ogre::Node* n )
{
	n->translate( *axes , *move , (Ogre::Node::TransformSpace) relativeTo );
}

EXPORTFUNC void NODE_translateAlongAxes( Ogre::Matrix3* axes , float x , float y , float z , int relativeTo , Ogre::Node* n )
{
	n->translate( *axes , x, y , z , (Ogre::Node::TransformSpace) relativeTo );
}


EXPORTFUNC void NODE_roll( float degrees , Ogre::Node::TransformSpace relativeTo , Ogre::Node* n )
{
	n->roll( Ogre::Degree( degrees ) , relativeTo );
}

EXPORTFUNC void NODE_rollWithRadians( float radians , Ogre::Node::TransformSpace relativeTo , Ogre::Node* n )
{
	n->roll( Ogre::Radian( radians ) , relativeTo );
}

EXPORTFUNC void NODE_pitch( float degrees , Ogre::Node::TransformSpace relativeTo , Ogre::Node* n )
{
	n->pitch( Ogre::Degree( degrees ), relativeTo );
}

EXPORTFUNC void NODE_pitchWithRadians( float radians , Ogre::Node::TransformSpace relativeTo , Ogre::Node* n )
{
	n->pitch( Ogre::Radian( radians ) , relativeTo );
}


EXPORTFUNC void NODE_yaw( float degrees , Ogre::Node::TransformSpace relativeTo , Ogre::Node* n )
{
	n->yaw( Ogre::Degree( degrees ) , relativeTo );
}

EXPORTFUNC void NODE_yawWithRadians( float radians , Ogre::Node::TransformSpace relativeTo , Ogre::Node* n )
{
	n->yaw( Ogre::Radian( radians ) , relativeTo );
}

EXPORTFUNC void NODE_rotate(Ogre::Vector3* axis , float degrees , Ogre::Node::TransformSpace relativeTo , Ogre::Node* n )
{
	n->rotate(*axis , Ogre::Degree( degrees ) , relativeTo );
}

EXPORTFUNC void NODE_rotateWithRadians( Ogre::Vector3* axis , float radians , Ogre::Node::TransformSpace relativeTo , Ogre::Node* n )
{
	n->rotate( *axis , Ogre::Radian( radians ) , relativeTo );
}

EXPORTFUNC void NODE_rotateWithQuaternion( Ogre::Quaternion* q , Ogre::Node::TransformSpace relativeTo , Ogre::Node* n )
{
	n->rotate( *q , relativeTo );
}

EXPORTFUNC Ogre::Matrix3* NODE_getLocalAxes( Ogre::Node* n )
{
	return new Ogre::Matrix3( n->getLocalAxes() );
}

EXPORTFUNC Ogre::Node* NODE_createChild( Ogre::Vector3* translate , Ogre::Quaternion* rotate , Ogre::Node* n )
{
	return n->createChild( *translate , *rotate );
}

EXPORTFUNC Ogre::Node* NODE_createChildWithName( const char* name , Ogre::Vector3* translate , Ogre::Quaternion* rotate , Ogre::Node* n )
{
	return n->createChild( name , *translate , *rotate );
}

EXPORTFUNC void NODE_addChild( Ogre::Node* child , Ogre::Node* n )
{
	n->addChild( child );
}

EXPORTFUNC int NODE_numChildren( Ogre::Node* n )
{
	return n->numChildren();
}

EXPORTFUNC Ogre::Node* NODE_getChild( int index , Ogre::Node* n )
{
	// If demo, check the timer
	#ifdef DEMO
	updateRender();
	#endif

	return n->getChild( index );
}

EXPORTFUNC Ogre::Node* NODE_getChildWithName( const char* name , Ogre::Node* n )
{
	return n->getChild( name );
}

EXPORTFUNC Ogre::Node::ChildNodeIterator* NODE_getChildIterator( Ogre::Node* n )
{
	return new Ogre::Node::ChildNodeIterator( n->getChildIterator() );
}

EXPORTFUNC Ogre::Node* NODE_removeChildWithIndex( int index , Ogre::Node* n )
{
	return n->removeChild( index );
}

EXPORTFUNC Ogre::Node* NODE_removeChild( Ogre::Node* child , Ogre::Node* n)
{
	return n->removeChild( child );
}

EXPORTFUNC Ogre::Node* NODE_removeChildWithName( const char* name , Ogre::Node* n )
{
	return n->removeChild( name );
}

EXPORTFUNC void NODE_removeAllChildren( Ogre::Node* n )
{
	n->removeAllChildren();
}

EXPORTFUNC Ogre::Quaternion* NODE__getDerivedOrientation( Ogre::Node* n )
{
	return new Ogre::Quaternion( n->_getDerivedOrientation() );
}

EXPORTFUNC Ogre::Vector3* NODE__getDerivedPosition( Ogre::Node* n )
{
	return new Ogre::Vector3( n->_getDerivedPosition() );
}

EXPORTFUNC Ogre::Vector3* NODE__getDerivedScale( Ogre::Node* n )
{
	return new Ogre::Vector3( n->_getDerivedScale() );
}

EXPORTFUNC Ogre::Matrix4* NODE__getFullTransform( Ogre::Node* n )
{
	return new Ogre::Matrix4( n->_getFullTransform() );
}

EXPORTFUNC void NODE__update( bool updateChildren , bool parentHasChanged , Ogre::Node* n )
{
	n->_update( updateChildren , parentHasChanged );
}

EXPORTFUNC void NODE_setListener( Ogre::Node::Listener* listener , Ogre::Node* n )
{
	n->setListener( listener );
}

EXPORTFUNC Ogre::Node::Listener* NODE_getListener( Ogre::Node* n )
{
	return n->getListener();
}






#endif
