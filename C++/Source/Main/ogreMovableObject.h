#ifndef H_OGREMOVABLEOBJECT
#define H_OGREMOVABLEOBJECT

EXPORTFUNC void MO__notifyCreator( Ogre::MovableObjectFactory* fact , Ogre::MovableObject* mo )
{
	mo->_notifyCreator( fact );
}

EXPORTFUNC Ogre::MovableObjectFactory* MO__getCreator( Ogre::MovableObject* mo )
{
	return mo->_getCreator();
}

EXPORTFUNC void MO__notifyManager( Ogre::SceneManager* man , Ogre::MovableObject* mo )
{
	mo->_notifyManager( man );
}

EXPORTFUNC Ogre::SceneManager* MO__getManager( Ogre::MovableObject* mo )
{
	return mo->_getManager();
}

EXPORTFUNC const char* MO_getName( Ogre::MovableObject* mo )
{
	return mo->getName().c_str();
}

EXPORTFUNC const char* MO_getMovableType( Ogre::MovableObject* mo )
{
	return mo->getMovableType().c_str();
}

EXPORTFUNC Ogre::Node* MO_getParentNode( Ogre::MovableObject* mo )
{
	return mo->getParentNode();
}

EXPORTFUNC Ogre::SceneNode* MO_getParentSceneNode( Ogre::MovableObject* mo )
{
	// If demo check the timer-
	#ifdef DEMO
	updateRender();
	#endif

	return mo->getParentSceneNode();
}

EXPORTFUNC void MO__notifyAttached( Ogre::Node* parent , bool isTagPoint , Ogre::MovableObject* mo )
{
	mo->_notifyAttached( parent , isTagPoint );
}

EXPORTFUNC bool MO_isAttached( Ogre::MovableObject* mo )
{
	return mo->isAttached();
}

EXPORTFUNC void MO_detatchFromParent( Ogre::MovableObject* mo )
{
	mo->detatchFromParent();
}

EXPORTFUNC bool MO_isInScene( Ogre::MovableObject* mo )
{
	return mo->isInScene();
}

EXPORTFUNC void MO__notifyMoved( Ogre::MovableObject* mo )
{
	mo->_notifyMoved();
}

EXPORTFUNC void MO__notifyCurrentCamera( Ogre::Camera* cam , Ogre::MovableObject* mo )
{
	mo->_notifyCurrentCamera( cam );
}

EXPORTFUNC Ogre::AxisAlignedBox* MO_getBoundingBox( Ogre::MovableObject* mo )
{
	Ogre::AxisAlignedBox* aab = new Ogre::AxisAlignedBox( mo->getBoundingBox() );
	return aab;
}

EXPORTFUNC float MO_getBoundingRadius( Ogre::MovableObject* mo )
{
	return mo->getBoundingRadius();
}

EXPORTFUNC Ogre::Sphere* MO_getWorldBoundingSphere( bool derive , Ogre::MovableObject* mo )
{
	Ogre::Sphere* ogreSphere = new Ogre::Sphere();
	*ogreSphere = mo->getWorldBoundingSphere( derive );
	return ogreSphere;
}

EXPORTFUNC void MO__updateRenderQueue( Ogre::RenderQueue* queue , Ogre::MovableObject* mo )
{
	mo->_updateRenderQueue( queue );
}

EXPORTFUNC void MO_setVisible( bool visible , Ogre::MovableObject* mo )
{
	mo->setVisible( visible );
}

EXPORTFUNC bool MO_getVisible( Ogre::MovableObject* mo )
{
	return mo->getVisible();
}

EXPORTFUNC bool MO_isVisible( Ogre::MovableObject* mo )
{
	return mo->isVisible();
}

EXPORTFUNC void MO_setRenderingDistance( Ogre::Real dist , Ogre::MovableObject* mo )
{
	mo->setRenderingDistance( dist );
}

EXPORTFUNC float MO_getRenderingDistance( Ogre::MovableObject* mo )
{
	return mo->getRenderingDistance();
}

EXPORTFUNC void MO_setUserObject( Ogre::UserDefinedObject* obj , Ogre::MovableObject* mo )
{
	mo->setUserObject( obj );
}

EXPORTFUNC Ogre::UserDefinedObject* MO_getUserObject(  Ogre::MovableObject* mo )
{
	return mo->getUserObject();
}

EXPORTFUNC void MO_setUserAny( Ogre::Any* anything , Ogre::MovableObject* mo )
{
	mo->setUserAny( *anything );
}

EXPORTFUNC Ogre::Any* MO_getUserAny( Ogre::MovableObject* mo )
{
	return new Ogre::Any( mo->getUserAny() );
}

EXPORTFUNC void MO_setRenderQueueGroup( int queueID , Ogre::MovableObject* mo )
{
	mo->setRenderQueueGroup( queueID );
}

EXPORTFUNC unsigned int MO_getRenderQueueGroup( Ogre::MovableObject* mo )
{
	return mo->getRenderQueueGroup();
}

EXPORTFUNC Ogre::Matrix4* MO__getParentNodeFullTransform( Ogre::MovableObject* mo )
{
	Ogre::Matrix4* ogreMatrix4 = new Ogre::Matrix4( mo->_getParentNodeFullTransform() );
	return ogreMatrix4;
}

EXPORTFUNC void MO_setQueryFlags( unsigned int flags , Ogre::MovableObject* mo )
{
	mo->setQueryFlags( flags );
}

EXPORTFUNC void MO_addQueryFlags( unsigned int flags , Ogre::MovableObject* mo )
{
	mo->addQueryFlags( flags );
}

EXPORTFUNC void MO_removeQueryFlags( unsigned int flags , Ogre::MovableObject* mo )
{
	mo->removeQueryFlags( flags );
}

EXPORTFUNC unsigned int MO_getQueryFlags( Ogre::MovableObject* mo )
{
	return mo->getQueryFlags();
}

EXPORTFUNC void MO_setVisibilityFlags( unsigned int flags , Ogre::MovableObject* mo )
{
	mo->setVisibilityFlags( flags );
}

EXPORTFUNC void MO_addVisibilityFlags( unsigned int flags , Ogre::MovableObject* mo )
{
	mo->addVisibilityFlags( flags );
}

EXPORTFUNC void MO_removeVisibilityFlags( unsigned int flags , Ogre::MovableObject* mo )
{
	mo->removeVisibilityFlags( flags );
}

EXPORTFUNC unsigned int MO_getVisibilityFlags( Ogre::MovableObject* mo )
{
	return mo->getVisibilityFlags();
}

EXPORTFUNC void MO_setListener( Ogre::MovableObject::Listener* listener , Ogre::MovableObject* mo )
{
	mo->setListener( listener );
}

EXPORTFUNC Ogre::MovableObject::Listener* MO_getListener( Ogre::MovableObject* mo )
{
	return mo->getListener();
}

EXPORTFUNC Ogre::LightList* MO_queryLights( Ogre::MovableObject* mo )
{
	return new Ogre::LightList( mo->queryLights() );
}

EXPORTFUNC void MO_setCastShadows( bool enabled , Ogre::MovableObject* mo )
{
	mo->setCastShadows( enabled );
}

EXPORTFUNC int MO_getCastShadows( Ogre::MovableObject* mo )
{
	return mo->getCastShadows();
}

EXPORTFUNC unsigned int MO_getTypeFlags( Ogre::MovableObject* mo )
{
	return mo->getTypeFlags();
}

EXPORTFUNC void MO_visitRenderables( Ogre::Renderable::Visitor* visitor , bool debugRenderables , Ogre::MovableObject* o )
{
	o->visitRenderables( visitor , debugRenderables );
}

EXPORTFUNC void MO_setDebugDisplayEnabled( bool enabled , Ogre::MovableObject* o )
{
	o->setDebugDisplayEnabled( enabled );
}

EXPORTFUNC bool MO_isDebugDisplayEnabled( Ogre::MovableObject* o )
{
	return o->isDebugDisplayEnabled();
}

EXPORTFUNC void MO_setDefaultQueryFlags( unsigned int flags )
{
	Ogre::MovableObject::setDefaultQueryFlags( flags );
}

EXPORTFUNC unsigned int MO_getDefaultQueryFlags( )
{
	return Ogre::MovableObject::getDefaultQueryFlags();
}

EXPORTFUNC void MO_setDefaultVisibilityFlags( unsigned int flags )
{
	Ogre::MovableObject::setDefaultVisibilityFlags( flags );
}

EXPORTFUNC unsigned int MO_getDefaultVisibilityFlags()
{
	return Ogre::MovableObject::getDefaultVisibilityFlags();
}










#endif
