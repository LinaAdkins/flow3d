#ifndef H_OGRESHADOWCASTER
#define H_OGRESHADOWCASTER

//virtual 	~ShadowCaster () - Unnecessary virtual destructor -
EXPORTFUNC int SHADOWCASTER_getCastShadows( Ogre::ShadowCaster* sc )
{
	return sc->getCastShadows();
}

EXPORTFUNC Ogre::EdgeData* SHADOWCASTER_getEdgeList( Ogre::ShadowCaster* sc )
{
	return sc->getEdgeList();
}

EXPORTFUNC int SHADOWCASTER_hasEdgeList( Ogre::ShadowCaster* sc )
{
	return sc->hasEdgeList();
}

EXPORTFUNC Ogre::AxisAlignedBox* SHADOWCASTER_getWorldBoundingBox( bool derive , Ogre::ShadowCaster* ogreShadowCaster )
{
	Ogre::AxisAlignedBox* AAB = new Ogre::AxisAlignedBox( ogreShadowCaster->getWorldBoundingBox( derive ) );
	return AAB;
}

EXPORTFUNC Ogre::AxisAlignedBox* SHADOWCASTER_getLightCapBounds( Ogre::ShadowCaster* ogreShadowCaster )
{
	Ogre::AxisAlignedBox* AAB = new Ogre::AxisAlignedBox( ogreShadowCaster->getLightCapBounds() );
	return AAB;
}

EXPORTFUNC Ogre::AxisAlignedBox* SHADOWCASTER_getDarkCapBounds( Ogre::Light* light , float dirLightExtrusionDist , Ogre::ShadowCaster* ogreShadowCaster )
{
	Ogre::AxisAlignedBox* AAB = new Ogre::AxisAlignedBox( ogreShadowCaster->getDarkCapBounds( *light , dirLightExtrusionDist ) );
	return AAB;
}

EXPORTFUNC Ogre::ShadowCaster::ShadowRenderableListIterator* SHADOWCASTER_getShadowVolumeRenderableIterator( int shadowTechnique , Ogre::Light* light , Ogre::HardwareIndexBuffer* indexBuffer, bool extrudeVertices , float extrusionDistance , int flags , Ogre::ShadowCaster* sc )
{
	return new Ogre::ShadowCaster::ShadowRenderableListIterator(  sc->getShadowVolumeRenderableIterator( (Ogre::ShadowTechnique)shadowTechnique , light , &Ogre::HardwareIndexBufferSharedPtr(indexBuffer), extrudeVertices , extrusionDistance, flags ) );
}

EXPORTFUNC float SHADOWCASTER_getPointExtrusionDistance( Ogre::Light* l , Ogre::ShadowCaster* ogreShadowCaster )
{
	return ogreShadowCaster->getPointExtrusionDistance( l );
}

//Static Public Member Functions-
EXPORTFUNC void SHADOWCASTER_extrudeVertices( Ogre::HardwareVertexBufferSharedPtr* vertexBuffer , long originalVertexCount , Ogre::Vector4* lightPos , float extrudeDist )
{
	Ogre::ShadowCaster::extrudeVertices( *vertexBuffer , originalVertexCount , *lightPos , extrudeDist );
}






#endif
