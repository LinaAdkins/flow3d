#ifndef H_TTRANSFORMKEYFRAME
#define H_TTRANSFORMKEYFRAME

/*
Managed instances only, manual construction / destruction not allowed

TransformKeyFrame (const AnimationTrack *parent, Real time)
~TransformKeyFrame ()
*/

EXPORTFUNC void TFORMKEYFRAME_setTranslate( Ogre::Vector3* trans , Ogre::TransformKeyFrame* f )
{
	f->setTranslate( *trans );
}

EXPORTFUNC Ogre::Vector3* TFORMKEYFRAME_getTranslate( Ogre::TransformKeyFrame* f )
{
	return new Ogre::Vector3( f->getTranslate() );
}

EXPORTFUNC void TFORMKEYFRAME_setScale( Ogre::Vector3* scale , Ogre::TransformKeyFrame* f )
{
	f->setScale( *scale );
}

EXPORTFUNC Ogre::Vector3* TFORMKEYFRAME_getScale( Ogre::TransformKeyFrame* f )
{
	return new Ogre::Vector3( f->getScale() );
}

EXPORTFUNC void TFORMKEYFRAME_setRotation( Ogre::Quaternion* rot , Ogre::TransformKeyFrame* f )
{
	f->setRotation( *rot );
}

EXPORTFUNC Ogre::Quaternion* TFORMKEYFRAME_getRotation( Ogre::TransformKeyFrame* f )
{
	return new Ogre::Quaternion( f->getRotation() );
}

/*
Already defined in parent TOgreKeyFrame and not overloaded

KeyFrame * 	_clone (AnimationTrack *newParent) const
Real 	getTime (void) const  - Already defined in parent TOgreKeyFrame
*/

/*

	Ogre Alloc overloads not necessary

void * 	operator new (size_t sz, const char *file, int line, const char *func)
void * 	operator new (size_t sz)
void * 	operator new (size_t sz, void *ptr)
void * 	operator new[] (size_t sz, const char *file, int line, const char *func)
void * 	operator new[] (size_t sz)
void 	operator delete (void *ptr)
void 	operator delete (void *ptr, const char *, int, const char *)
void 	operator delete[] (void *ptr)
void 	operator delete[] (void *ptr, const char *, int, const char *)
*/

#endif
