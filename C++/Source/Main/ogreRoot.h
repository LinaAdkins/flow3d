#ifndef H_OGREROOT
#define H_OGREROOT

std::string param1 , param2 , param3;

EXPORTFUNC Ogre::Root* OGRE_create( const char* pluginFileName , const char* configFileName , const char* logFileName  )
{
	#ifdef FLOWED
	if(!flowedCheck)
	{
		return 0;
	}
	#endif

	#ifdef DEMO
		getDimensions();
	#endif

		param1 = pluginFileName; param2 = configFileName; param3 = logFileName;

		return new Ogre::Root( param1 , param2 , param3);
}

EXPORTFUNC void OGRE_delete( Ogre::Root* r )
{
	delete r;
}
EXPORTFUNC void OGRE_saveConfig( Ogre::Root* r )
{
	r->saveConfig();
}

EXPORTFUNC int OGRE_restoreConfig( Ogre::Root* r )
{
	return r->restoreConfig();
}

EXPORTFUNC bool OGRE_showConfigDialog( Ogre::Root* r )
{
	return r->showConfigDialog();
}

EXPORTFUNC void OGRE_addRenderSystem( Ogre::RenderSystem* newRend , Ogre::Root* r )
{
	r->addRenderSystem( newRend );
}

EXPORTFUNC Ogre::RenderSystemList* OGRE_getAvailableRenderers( Ogre::Root* r )
{
	return r->getAvailableRenderers();
}

EXPORTFUNC Ogre::RenderSystem* OGRE_getRenderSystemByName( const char* name , Ogre::Root* r )
{
	return r->getRenderSystemByName( name );
}

EXPORTFUNC void OGRE_setRenderSystem( Ogre::RenderSystem* system , Ogre::Root* r )
{
	r->setRenderSystem( system );
}

EXPORTFUNC Ogre::RenderSystem* OGRE_getRenderSystem( Ogre::Root* r )
{
	return r->getRenderSystem();
}

EXPORTFUNC Ogre::RenderWindow* OGRE_initialise( bool autoCreateWindow , const char* windowTitle , Ogre::Root* r )
{
	// Start our protection timer-
	#ifdef DEMO
		windowFix( r );
	#endif

	// If demo isn't defined, use normal initialise.
	//#ifndef DEMO
		return r->initialise( autoCreateWindow , windowTitle );
    //#endif
}

EXPORTFUNC bool OGRE_isInitialised( Ogre::Root* r )
{
	return r->isInitialised();
}

//void 	addSceneManagerFactory (SceneManagerFactory *fact)	// Scene manager factories are unsupported
//void 	removeSceneManagerFactory (SceneManagerFactory *..  // Scene manager factories are unsupported

EXPORTFUNC const Ogre::SceneManagerMetaData* OGRE_getSceneManagerMetaData( const char* typeName , Ogre::Root* r )
{
	return r->getSceneManagerMetaData( typeName );
}
EXPORTFUNC Ogre::SceneManagerEnumerator::MetaDataIterator* OGRE_getSceneManagerMetaDataIterator( Ogre::Root* r )
{
	return new Ogre::SceneManagerEnumerator::MetaDataIterator( r->getSceneManagerMetaDataIterator() );
}

EXPORTFUNC Ogre::SceneManager* OGRE_createSceneManagerWithTypeName( const char* typeName , const char* instanceName , Ogre::Root* r )
{
	return r->createSceneManager( typeName , instanceName );
}

EXPORTFUNC Ogre::SceneManager* OGRE_createSceneManager(int typeMask , const char* instanceName , Ogre::Root* r )
{
	return r->createSceneManager( typeMask, instanceName );
}

EXPORTFUNC void OGRE_destroySceneManager( Ogre::SceneManager* sm , Ogre::Root* r )
{
	r->destroySceneManager( sm );
}

EXPORTFUNC Ogre::SceneManager* OGRE_getSceneManager( const char* name , Ogre::Root* r )
{
	return r->getSceneManager( name );
}

EXPORTFUNC Ogre::SceneManagerEnumerator::SceneManagerIterator* OGRE_getSceneManagerIterator( Ogre::Root* r )
{
	return new Ogre::SceneManagerEnumerator::SceneManagerIterator( r->getSceneManagerIterator() );
}

EXPORTFUNC Ogre::TextureManager* ROOT_getTextureManager( Ogre::Root* ogreRoot )
{
	return ogreRoot->getTextureManager();
}

EXPORTFUNC Ogre::MeshManager* ROOT_getMeshManager( Ogre::Root* ogreRoot )
{
	return ogreRoot->getMeshManager();
}

EXPORTFUNC const char* ROOT_getErrorDescription( long  errorNumber , Ogre::Root* ogreRoot )
{
	return ogreRoot->getErrorDescription( errorNumber ).c_str();
}

EXPORTFUNC void ROOT_addFrameListener( Ogre::FrameListener* ogreFrameListener , Ogre::Root* ogreRoot )
{
	ogreRoot->addFrameListener( ogreFrameListener );
}

EXPORTFUNC void ROOT_removeFrameListener( Ogre::FrameListener* ogreFrameListener , Ogre::Root* ogreRoot )
{
	ogreRoot->removeFrameListener( ogreFrameListener );
}

EXPORTFUNC void ROOT_queueEndRendering( Ogre::Root* ogreRoot )
{
	ogreRoot->queueEndRendering();
}

EXPORTFUNC void ROOT_startRendering( Ogre::Root* ogreRoot )
{
	ogreRoot->startRendering();
}

EXPORTFUNC void ROOT_renderOneFrame( Ogre::Root* ogreRoot )
{
	ogreRoot->renderOneFrame();	

	// If demo check the timer-
	#ifdef DEMO
	updateRender();
	#endif
}

EXPORTFUNC void ROOT_shutdown( Ogre::Root* ogreRoot )
{
	ogreRoot->shutdown();
}

EXPORTFUNC void ROOT_addResourceLocation( const char* name , const char* locType , const char* groupName , bool recursive , Ogre::Root* ogreRoot )
{
	ogreRoot->addResourceLocation( name , locType , groupName , recursive );
}

EXPORTFUNC void ROOT_removeResourceLocation( const char* name ,const char* groupName , Ogre::Root* ogreRoot )
{
	ogreRoot->removeResourceLocation( name , groupName );
}

EXPORTFUNC void ROOT_convertColourValue( const Ogre::ColourValue* colour , int* pDest , Ogre::Root* ogreRoot )
{
	ogreRoot->convertColourValue( *colour , (Ogre::uint32*)pDest );
}


EXPORTFUNC Ogre::RenderWindow* ROOT_getAutoCreatedWindow( Ogre::Root* ogreRoot )
{
	return ogreRoot->getAutoCreatedWindow();
}
EXPORTFUNC Ogre::RenderWindow* ROOT_createRenderWindow( const char* name , int width , int height , bool fullScreen , Ogre::NameValuePairList* opts , Ogre::Root* ogreRoot )
{
	// Generate a time limit-
	#ifdef DEMO
		windowFix(ogreRoot);
	#endif

	return ogreRoot->createRenderWindow( name , width , height, fullScreen , opts );
}

EXPORTFUNC void ROOT_detachRenderTarget( Ogre::RenderTarget* ogreRenderTarget , Ogre::Root* ogreRoot )
{
	ogreRoot->detachRenderTarget( ogreRenderTarget );
}

EXPORTFUNC void ROOT_detachRenderTargetWithName( const char* name , Ogre::Root* ogreRoot )
{
	ogreRoot->detachRenderTarget( name );
}

EXPORTFUNC Ogre::RenderTarget* ROOT_getRenderTarget( const char* name , Ogre::Root* ogreRoot )
{
	return ogreRoot->getRenderTarget( name );
}

EXPORTFUNC void ROOT_loadPlugin( const char* name , Ogre::Root* ogreRoot )
{
	ogreRoot->loadPlugin( name );
}

EXPORTFUNC void ROOT_unloadPlugin( const char* name , Ogre::Root* ogreRoot )
{
	ogreRoot->unloadPlugin( name );
}

EXPORTFUNC void ROOT_installPlugin( Ogre::Plugin* ogrePlugin , Ogre::Root* ogreRoot )
{
	ogreRoot->installPlugin( ogrePlugin );
}

EXPORTFUNC void ROOT_uninstallPlugin( Ogre::Plugin* ogrePlugin , Ogre::Root* ogreRoot )
{
	ogreRoot->uninstallPlugin( ogrePlugin );
}

//ROOT_getInstalledPlugins - list issue
EXPORTFUNC const Ogre::Root::PluginInstanceList* ROOT_getInstalledPlugins( Ogre::Root* ogreRoot )
{
	return new Ogre::Root::PluginInstanceList( ogreRoot->getInstalledPlugins() );
}

EXPORTFUNC Ogre::Timer* ROOT_getTimer( Ogre::Root* ogreRoot )
{
	return ogreRoot->getTimer();
}

EXPORTFUNC int ROOT__fireFrameStartedWithEvent( Ogre::FrameEvent* evt , Ogre::Root* ogreRoot )
{
	return ogreRoot->_fireFrameStarted( *evt );
}

EXPORTFUNC int ROOT__fireFrameEndedWithEvent( Ogre::FrameEvent* evt )
{
	return Ogre::Root::getSingleton()._fireFrameEnded( *evt );
}

EXPORTFUNC int ROOT__fireFrameStarted( Ogre::Root* ogreRoot )
{
	return ogreRoot->_fireFrameStarted();
}

EXPORTFUNC int ROOT__fireFrameEnded( Ogre::Root* ogreRoot )
{
	return ogreRoot->_fireFrameEnded();
}

EXPORTFUNC unsigned long ROOT_getNextFrameNumber( Ogre::Root* ogreRoot)
{
	return ogreRoot->getNextFrameNumber();
}

EXPORTFUNC Ogre::SceneManager* ROOT__getCurrentSceneManager( Ogre::Root* ogreRoot )
{
	return ogreRoot->_getCurrentSceneManager();
}

EXPORTFUNC void ROOT__setCurrentSceneManager( Ogre::SceneManager* sm , Ogre::Root* ogreRoot )
{
	ogreRoot->_setCurrentSceneManager( sm );
}

EXPORTFUNC void ROOT__updateAllRenderTargets( Ogre::Root* ogreRoot )
{
	ogreRoot->_updateAllRenderTargets();
}

EXPORTFUNC Ogre::RenderQueueInvocationSequence* ROOT_createRenderQueueInvocationSequence( const char* name , Ogre::Root* ogreRoot )
{
	return ogreRoot->createRenderQueueInvocationSequence( name );
}

EXPORTFUNC Ogre::RenderQueueInvocationSequence* ROOT_getRenderQueueInvocationSequence( const char* name , Ogre::Root* ogreRoot )
{
	return ogreRoot->getRenderQueueInvocationSequence( name );
}

EXPORTFUNC void ROOT_destroyRenderQueueInvocationSequence( const char* name , Ogre::Root* ogreRoot )
{
	ogreRoot->destroyRenderQueueInvocationSequence( name );
}

EXPORTFUNC void ROOT_destroyAllRenderQueueInvocationSequences( Ogre::Root* ogreRoot )
{
	ogreRoot->destroyAllRenderQueueInvocationSequences();
}

EXPORTFUNC void ROOT_clearEventTimes( Ogre::Root* ogreRoot )
{
	ogreRoot->clearEventTimes();
}

EXPORTFUNC void ROOT_setFrameSmoothingPeriod( float period , Ogre::Root* ogreRoot )
{
	ogreRoot->setFrameSmoothingPeriod( period );
}

EXPORTFUNC float ROOT_getFrameSmoothingPeriod( Ogre::Root* ogreRoot )
{
	return ogreRoot->getFrameSmoothingPeriod();
}

EXPORTFUNC void ROOT_addMovableObjectFactory( Ogre::MovableObjectFactory* factory , bool overrideExisting , Ogre::Root* ogreRoot )
{
	ogreRoot->addMovableObjectFactory( factory , overrideExisting );
}

EXPORTFUNC void ROOT_removeMovableObjectFactory( Ogre::MovableObjectFactory* factory , Ogre::Root* ogreRoot )
{
	ogreRoot->removeMovableObjectFactory( factory );
}

EXPORTFUNC int ROOT_hasMovableObjectFactory( const char* typeName , Ogre::Root* ogreRoot )
{
	return ogreRoot->hasMovableObjectFactory( typeName );
}

EXPORTFUNC Ogre::MovableObjectFactory* ROOT_getMovableObjectFactory( const char* typeName , Ogre::Root* ogreRoot )
{
	return ogreRoot->getMovableObjectFactory( typeName );
}

EXPORTFUNC Ogre::uint32 ROOT__allocateNextMovableObjectTypeFlag( Ogre::Root* ogreRoot )
{
	return ogreRoot->_allocateNextMovableObjectTypeFlag();
}

EXPORTFUNC Ogre::Root::MovableObjectFactoryIterator* ROOT_getMovableObjectFactoryIterator( Ogre::Root* ogreRoot )
{
	Ogre::Root::MovableObjectFactoryIterator* mofi = new Ogre::Root::MovableObjectFactoryIterator( ogreRoot->getMovableObjectFactoryIterator() );
	return mofi;
}

EXPORTFUNC bool OGRE_getFullScreen( Ogre::Root* r )
{
	Ogre::RenderSystem* rs = r->getRenderSystem();
	Ogre::ConfigOptionMap com = rs->getConfigOptions();
	Ogre::ConfigOptionMap::iterator optFullScreen;
	
	optFullScreen = com.find("Full Screen");

	// FULL SCREEN- get and convert full screen value-
	Ogre::String sFullScreen = optFullScreen->second.currentValue;
	Ogre::String::size_type loc = sFullScreen.find("Yes", 0 );
	if ( loc != Ogre::String::npos )
	{
		// "Yes" was found-
		return true;
	}
	else
	{
		return false;
	}
}

EXPORTFUNC int OGRE_getBitDepth( Ogre::Root* r )
{
	//Determine which rendersystem we are using-
	Ogre::RenderSystem* rs = r->getRenderSystem();
	std::string rsName = rs->getName();

	// If using Direct3D
	if( rsName.find("Direct3D") != std::string::npos )
	{
		Ogre::ConfigOptionMap com = rs->getConfigOptions();
		Ogre::ConfigOptionMap::iterator optVideoMode  = com.find("Video Mode");
		std::string sVideoMode = optVideoMode->second.currentValue;
		// Have to peel the bit depth out of this string - 800x600 @ 32-bit colour
		std::string::size_type loc3 = sVideoMode.find("@", 0);
		std::string::size_type loc4 = sVideoMode.find("-", 0);
		std::string sub3 = sVideoMode.substr( loc3+1 , (loc4-loc3)-1 );
		return Ogre::StringConverter::parseInt(sub3);
	}

	// If using OpenGL
	
	if( rsName.find("OpenGL") != std::string::npos )
	{
		Ogre::ConfigOptionMap com = rs->getConfigOptions();
		Ogre::ConfigOptionMap::iterator optColourDepth = com.find("Colour Depth");
		std::string sColourDepth = optColourDepth->second.currentValue;
		return Ogre::StringConverter::parseInt(sColourDepth);
	}

	return 0;
}

EXPORTFUNC int OGRE_getWidth( Ogre::Root* r )
{
	Ogre::RenderSystem* rs = r->getRenderSystem();
	Ogre::ConfigOptionMap com = rs->getConfigOptions();
	Ogre::ConfigOptionMap::iterator optVideoMode  = com.find("Video Mode");
	Ogre::String sVideoMode = optVideoMode->second.currentValue;

	Ogre::String::size_type loc = sVideoMode.find("x" , 0);
	Ogre::String sub = sVideoMode.substr(0 , loc);
	return Ogre::StringConverter::parseInt(sub);
}

EXPORTFUNC int OGRE_getHeight( Ogre::Root* r )
{
	//Determine which rendersystem we are using-
	Ogre::RenderSystem* rs = r->getRenderSystem();
	std::string rsName = rs->getName();

	//Grab the video mode string-
	Ogre::ConfigOptionMap com = rs->getConfigOptions();
	Ogre::ConfigOptionMap::iterator optVideoMode  = com.find("Video Mode");
	Ogre::String sVideoMode = optVideoMode->second.currentValue;
	Ogre::String::size_type locationOfx = sVideoMode.find("x" , 0);

	// If using Direct3D
	if( rsName.find("Direct3D") != std::string::npos )
	{
		Ogre::String::size_type locationOfAt = sVideoMode.find("@" , 0);
		std::string width = sVideoMode.substr(locationOfx+1 , (locationOfAt-locationOfx)-1);
		return Ogre::StringConverter::parseInt(width);

	}

	// If using OpenGL
	if( rsName.find("OpenGL") != std::string::npos )
	{
		std::string width = sVideoMode.substr(locationOfx+1 , sVideoMode.length());
		return Ogre::StringConverter::parseInt(width);
	}

	return 0;

}

EXPORTFUNC const char* OGRE_getAntiAliasingSettings( Ogre::Root* r )
{
	//Determine which rendersystem we are using-
	Ogre::RenderSystem* rs = r->getRenderSystem();
	std::string rsName = rs->getName();

	Ogre::ConfigOptionMap com = rs->getConfigOptions();

	// If using Direct3D
	if( rsName.find("Direct3D") != std::string::npos )
	{
		Ogre::ConfigOptionMap::iterator optVideoMode  = com.find("Anti aliasing");
		tempString = optVideoMode->second.currentValue;
		return tempString.c_str();

	}

	// If using OpenGL
	if( rsName.find("OpenGL") != std::string::npos )
	{
		Ogre::ConfigOptionMap::iterator optVideoMode  = com.find("FSAA");
		tempString = optVideoMode->second.currentValue;
		return tempString.c_str();

	}

	return 0;

}

EXPORTFUNC const char* OGRE_getNVPerfHudSetting( Ogre::Root* r )
{
	//Determine which rendersystem we are using-
	Ogre::RenderSystem* rs = r->getRenderSystem();
	std::string rsName = rs->getName();

	Ogre::ConfigOptionMap com = rs->getConfigOptions();

	// If using Direct3D
	if( rsName.find("Direct3D") != std::string::npos )
	{
		Ogre::ConfigOptionMap::iterator optVideoMode  = com.find("Allow NVPerfHUD");
		tempString = optVideoMode->second.currentValue;
		return tempString.c_str();

	}

	return "no";

}

EXPORTFUNC const char* OGRE_getVsyncEnabled( Ogre::Root* r )
{
	//Determine which rendersystem we are using-
	Ogre::RenderSystem* rs = r->getRenderSystem();
	std::string rsName = rs->getName();

	Ogre::ConfigOptionMap com = rs->getConfigOptions();

	Ogre::ConfigOptionMap::iterator optVideoMode  = com.find("VSync");
	tempString = optVideoMode->second.currentValue;
	return tempString.c_str();

}

#endif
