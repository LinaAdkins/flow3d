#ifndef H_FLOWQUICKLINE
#define H_FLOWQUICKLINE

/* What the quickline interface needs-
// Quickline is just a derivation of ManualObject
o Fast Init
o addPoint
o setColor
o clear
o Single Interface = Single line = Single manual object
o Global line:TQuickLine = TQuickLine.Create()
o line.addPoint( x , y , z )
o line.setColour( r , g , b , a )
o line.clear()*/

class Wire
{
private:
	Ogre::ManualObject* mo;
	Ogre::SceneNode* sn;
	Ogre::MaterialPtr material;
	Ogre::ColourValue color;
	std::vector<Ogre::Vector3> points;
	std::string mname;
public:
	Wire::Wire( const char* name , Ogre::SceneManager* sm ) : 
	  mo( sm->createManualObject( name ) ) , 
	  sn( sm->getRootSceneNode()->createChildSceneNode( name ) ),
	  material( Ogre::MaterialManager::getSingleton().create( name , "wire" ) ),
	  color( 0.0 , 1.0 , 0.0 , 1.0 ),
	  mname( name )
	  { 
		  // ManualObject Properties-
		  mo->setDynamic( true );
		  
		  // Set an infinite AABB so the line never gets culled from view-
		  Ogre::AxisAlignedBox aabInf;	
		  aabInf.setInfinite();
		  mo->setBoundingBox( aabInf );

		  // Material Properties-
		  material->setReceiveShadows( false );
		  material->setLightingEnabled( true );
		  material->getTechnique(0)->getPass(0)->setDiffuse(0,0,1,0);
		  material->getTechnique(0)->getPass(0)->setAmbient(0,0,1);
		  material->getTechnique(0)->getPass(0)->setSelfIllumination(0,0,1);


		  // Attach the ManualObject to our scene-
		  sn->attachObject( mo );
	  }
	

	Wire::~Wire();
	// Functions for manipulating points-
	void Wire::addPoint( float x , float y , float z );
	void Wire::setPoint( int index , float x, float y , float z );
	Ogre::Vector3* Wire::getPoint( int index );
	void Wire::removePoint( int index );
	void Wire::resetPoints();
	void Wire::setColour( Ogre::ColourValue* colur );
	Ogre::ColourValue* Wire::getColour();
};


EXPORTFUNC void QUICKLINE_create( Ogre::SceneManager* sm )
{
	//Ogre::ManualObject* mo = sm->createManualObject("line1");
	//Ogre::SceneNode* sn = sm->getRootSceneNode()->createChildSceneNode("line1_node");

	//Ogre::MaterialPtr momat = Ogre::MaterialManager::getSingleton().create("line1_material","debugger");
	//momat->setReceiveShadows(false);
	//momat->getTechnique(0)->setLightingEnabled(true);
//	momat->getTechnique(0)->getPass(0)->setDiffuse(0,0,1,0);
//	momat->getTechnique(0)->getPass(0)->setAmbient(0,0,1);
//	momat->getTechnique(0)->getPass(0)->setSelfIllumination(0,0,1);
//	mo->setDynamic( true );
//	mo->begin("line1_material" , Ogre::RenderOperation::OT_LINE_LIST );
//	mo->position( 3 , 2 , 1 );
//	mo->end();
	//Ogre::AxisAlignedBox aabInf;
	//aabInf.setInfinite();
	//mo->setBoundingBox( aabInf );
	//sn->attachObject( mo );
	//return mo;
}

EXPORTFUNC void QUICKLINE_addPoint( float x , float y , float z , Ogre::ManualObject* mo )
{
	mo->begin("line1_material" , Ogre::RenderOperation::OT_LINE_LIST );
	mo->position( x , y , z );
	mo->position( x+12 , y+12 , z+12 );
	//mo->end();
}



#endif
