#ifndef H_OGRESTRINGINTERFACE
#define H_OGRESTRINGINTERFACE

EXPORTFUNC void STRINT_delete( Ogre::StringInterface* ogreStringInterface )
{
	delete ogreStringInterface;
}

EXPORTFUNC Ogre::ParamDictionary* STRINT_getParamDictionary( Ogre::StringInterface* ogreStringInterface )
{
	return ogreStringInterface->getParamDictionary();
}

EXPORTFUNC Ogre::ParameterList* STRINT_getParameters( Ogre::StringInterface* ogreStringInterface )
{
	Ogre::ParameterList* opl = new Ogre::ParameterList( ogreStringInterface->getParameters() ); 
	return opl;
}

EXPORTFUNC int STRINT_setParameter( const char* name , const char* value , Ogre::StringInterface* ogreStringInterface )
{
	return ogreStringInterface->setParameter( name , value );
}

EXPORTFUNC void STRINT_setParameterList( Ogre::NameValuePairList* paramList , Ogre::StringInterface* ogreStringInterface )
{
	ogreStringInterface->setParameterList( *paramList );
}

EXPORTFUNC const char* STRINT_getParameter( const char* name , Ogre::StringInterface* ogreStringInterface )
{
	tempString = ogreStringInterface->getParameter( name );
	return tempString.c_str();
}

EXPORTFUNC void STRINT_copyParametersTo( Ogre::StringInterface* dest , Ogre::StringInterface* ogreStringInterface )
{
	ogreStringInterface->copyParametersTo( dest );
}

EXPORTFUNC void STRINT_cleanupDictionary()
{
	Ogre::StringInterface::cleanupDictionary();
} 

#endif
