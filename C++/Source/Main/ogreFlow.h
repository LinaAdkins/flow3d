#ifndef H_OGREFLOW
#define H_OGREFLOW

Ogre::MovableObject* FLOW_abPick( Ogre::Ray* ogreRay , Ogre::Vector3* collisionPoint , Ogre::uint32 mask )
{
	Ogre::MovableObject* pickedObject;
	return MovableObjectChildCast( pickedObject );
}

Ogre::String ogretemp;

EXPORTFUNC void FLOW_loadResourceFile( const char* resourceFileName )
{
	ogretemp = Ogre::String( resourceFileName );
	Ogre::ConfigFile cf;
	cf.load( ogretemp );

	Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();

	Ogre::String secName, typeName, archName;
	while (seci.hasMoreElements())
	{
		secName = seci.peekNextKey();
		Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
		Ogre::ConfigFile::SettingsMultiMap::iterator i;
		for( i=settings->begin(); i != settings->end(); ++i)
		{
			typeName = i->first;
			archName = i->second;
			Ogre::ResourceGroupManager::getSingleton().addResourceLocation( archName, typeName, secName );
		}
	}
}




#endif
