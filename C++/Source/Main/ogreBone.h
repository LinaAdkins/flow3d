#ifndef H_OGREBONE
#define H_OGREBONE

EXPORTFUNC Ogre::Bone* BONE_createChild( int handle , Ogre::Vector3* translate , Ogre::Quaternion* rotate , Ogre::Bone* b )
{
	return b->createChild( handle , *translate , *rotate );
}

EXPORTFUNC int BONE_getHandle( Ogre::Bone* b )
{
	return b->getHandle();
}

EXPORTFUNC void BONE_setBindingPose( Ogre::Bone* b )
{
	b->setBindingPose();
}

EXPORTFUNC void BONE_reset( Ogre::Bone* b )
{
	b->reset();
}

EXPORTFUNC void BONE_setManuallyControlled( bool manuallyControlled , Ogre::Bone* b )
{
	b->setManuallyControlled( manuallyControlled );
}

EXPORTFUNC bool BONE_isManuallyControlled( Ogre::Bone* b )
{
	return b->isManuallyControlled();
}

EXPORTFUNC void BONE__getOffsetTransform( Ogre::Matrix4* m , Ogre::Bone* b )
{
	b->_getOffsetTransform( *m );
}

EXPORTFUNC Ogre::Vector3* BONE__getBindingPoseInverseScale ( Ogre::Bone* b )
{
	return new Ogre::Vector3( b->_getBindingPoseInverseScale() );
}

EXPORTFUNC Ogre::Vector3* BONE__getBindingPoseInversePosition( Ogre::Bone* b )
{
	return new Ogre::Vector3( b->_getBindingPoseInversePosition() );
}

EXPORTFUNC Ogre::Quaternion* BONE__getBindingPoseInverseOrientation( Ogre::Bone* b )
{
	return new Ogre::Quaternion( b->_getBindingPoseInverseOrientation() );
}

EXPORTFUNC void BONE_needUpdate( bool forceParentUpdate , Ogre::Bone* b )
{
	b->needUpdate();
}

// Node Functions

EXPORTFUNC const char* BONE_getName( Ogre::Bone* b )
{
	return b->getName().c_str();	
}

EXPORTFUNC Ogre::Node* BONE_getParent( Ogre::Bone* b )
{
	return b->getParent();
}

EXPORTFUNC Ogre::Quaternion* BONE_getOrientation( Ogre::Bone* b )
{
	return new Ogre::Quaternion( b->getOrientation() );
}

EXPORTFUNC void BONE_setOrientationWithQuaternion( Ogre::Quaternion* q , Ogre::Bone* b )
{
	b->setOrientation( *q );
}

EXPORTFUNC void BONE_setOrientation( float w , float x , float y , float z , Ogre::Bone* b )
{
	b->setOrientation( w , x , y , z );
}

EXPORTFUNC void BONE_resetOrientation( Ogre::Bone* b )
{
	b->resetOrientation();
}

EXPORTFUNC void BONE_setInitialState( Ogre::Bone* b )
{
	b->setInitialState();
}

EXPORTFUNC void BONE_resetToInitialState( Ogre::Bone* b )
{
	b->resetToInitialState();
}


EXPORTFUNC void BONE_setPositionWithVector3( Ogre::Vector3* pos , Ogre::Bone* b )
{
	b->setPosition( *pos );
}
EXPORTFUNC void BONE_setPosition( float x , float y , float z , Ogre::Bone* b)
{
	b->setPosition( x , y , z );
}

EXPORTFUNC Ogre::Vector3* BONE_getPosition( Ogre::Bone* b )
{
	return new Ogre::Vector3( b->getPosition() );
}

EXPORTFUNC void BONE_setScaleWithVector3( Ogre::Vector3* scale , Ogre::Bone* b )
{
	b->setScale( *scale );
}


EXPORTFUNC void BONE_setScale( float x , float y , float z , Ogre::Bone* b )
{
	b->setScale( x , y , z );
}

EXPORTFUNC Ogre::Vector3* BONE_getScale( Ogre::Bone* b )
{
	return new Ogre::Vector3( b->getScale() );
}

EXPORTFUNC void BONE_setInheritOrientation( bool inherit , Ogre::Bone* b )
{
	b->setInheritOrientation( inherit );
}

EXPORTFUNC bool BONE_getInheritOrientation( Ogre::Bone* b )
{
	return b->getInheritOrientation();
}

EXPORTFUNC void BONE_setInheritScale( bool inherit , Ogre::Bone* b )
{
	b->setInheritScale( inherit );
}

EXPORTFUNC bool BONE_getInheritScale( Ogre::Bone* b )
{
	return b->getInheritScale();
}

EXPORTFUNC void BONE_scaleWithVector3( Ogre::Vector3* scale , Ogre::Bone* b )
{
	b->scale( *scale );
}

EXPORTFUNC void BONE_scale ( float x, float y, float z , Ogre::Bone* b )
{
	b->scale(  x , y , z );
}

EXPORTFUNC void BONE_translateWithVector3( Ogre::Vector3* d , int relativeTo, Ogre::Bone* b )
{
	b->translate( *d , (Ogre::Node::TransformSpace)relativeTo );
}

EXPORTFUNC void BONE_translate(float x, float y, float z, int relativeTo, Ogre::Bone* b)
{
	b->translate( x , y , z , (Ogre::Node::TransformSpace) relativeTo );
}

EXPORTFUNC void BONE_translateAlongAxesWithVector3( Ogre::Matrix3* axes ,  Ogre::Vector3* move , int relativeTo , Ogre::Bone* b )
{
	b->translate( *axes , *move , (Ogre::Node::TransformSpace) relativeTo );
}

EXPORTFUNC void BONE_translateAlongAxes( Ogre::Matrix3* axes , float x , float y , float z , int relativeTo , Ogre::Bone* b )
{
	b->translate( *axes , x, y , z , (Ogre::Node::TransformSpace) relativeTo );
}


EXPORTFUNC void BONE_roll( float degrees , Ogre::Node::TransformSpace relativeTo , Ogre::Bone* b )
{
	b->roll( Ogre::Degree( degrees ) , relativeTo );
}

EXPORTFUNC void BONE_rollWithRadians( float radians , Ogre::Node::TransformSpace relativeTo , Ogre::Bone* b )
{
	b->roll( Ogre::Radian( radians ) , relativeTo );
}

EXPORTFUNC void BONE_pitch( float degrees , Ogre::Node::TransformSpace relativeTo , Ogre::Bone* b )
{
	b->pitch( Ogre::Degree( degrees ), relativeTo );
}

EXPORTFUNC void BONE_pitchWithRadians( float radians , Ogre::Node::TransformSpace relativeTo , Ogre::Bone* b )
{
	b->pitch( Ogre::Radian( radians ) , relativeTo );
}


EXPORTFUNC void BONE_yaw( float degrees , Ogre::Node::TransformSpace relativeTo , Ogre::Bone* b )
{
	b->yaw( Ogre::Degree( degrees ) , relativeTo );
}

EXPORTFUNC void BONE_yawWithRadians( float radians , Ogre::Node::TransformSpace relativeTo , Ogre::Bone* b )
{
	b->yaw( Ogre::Radian( radians ) , relativeTo );
}

EXPORTFUNC void BONE_rotate(Ogre::Vector3* axis , float degrees , Ogre::Node::TransformSpace relativeTo , Ogre::Bone* b )
{
	b->rotate(*axis , Ogre::Degree( degrees ) , relativeTo );
}

EXPORTFUNC void BONE_rotateWithRadians( Ogre::Vector3* axis , float radians , Ogre::Node::TransformSpace relativeTo , Ogre::Bone* b )
{
	b->rotate( *axis , Ogre::Radian( radians ) , relativeTo );
}

EXPORTFUNC void BONE_rotateWithQuaternion( Ogre::Quaternion* q , Ogre::Node::TransformSpace relativeTo , Ogre::Bone* b )
{
	b->rotate( *q , relativeTo );
}

EXPORTFUNC Ogre::Matrix3* BONE_getLocalAxes( Ogre::Bone* b )
{
	return new Ogre::Matrix3( b->getLocalAxes() );
}

EXPORTFUNC void BONE_addChild( Ogre::Node* child , Ogre::Bone* b )
{
	b->addChild( child );
}

EXPORTFUNC int BONE_numChildren( Ogre::Bone* b )
{
	return b->numChildren();
}

EXPORTFUNC Ogre::Node* BONE_getChild( int index , Ogre::Bone* b )
{
	return b->getChild( index );
}

EXPORTFUNC Ogre::Node* BONE_getChildWithName( const char* name , Ogre::Bone* b )
{
	return b->getChild( name );
}

EXPORTFUNC Ogre::Node::ChildNodeIterator* BONE_getChildIterator( Ogre::Bone* b )
{
	return new Ogre::Node::ChildNodeIterator( b->getChildIterator() );
}

EXPORTFUNC Ogre::Node* BONE_removeChildWithIndex( int index , Ogre::Bone* b )
{
	return b->removeChild( index );
}

EXPORTFUNC Ogre::Node* BONE_removeChild( Ogre::Node* child , Ogre::Bone* b)
{
	return b->removeChild( child );
}

EXPORTFUNC Ogre::Node* BONE_removeChildWithName( const char* name , Ogre::Bone* b )
{
	return b->removeChild( name );
}

EXPORTFUNC void BONE_removeAllChildren( Ogre::Bone* b )
{
	b->removeAllChildren();
}

EXPORTFUNC Ogre::Quaternion* BONE__getDerivedOrientation( Ogre::Bone* b )
{
	return new Ogre::Quaternion( b->_getDerivedOrientation() );
}

EXPORTFUNC Ogre::Vector3* BONE__getDerivedPosition( Ogre::Bone* b )
{
	return new Ogre::Vector3( b->_getDerivedPosition() );
}

EXPORTFUNC Ogre::Vector3* BONE__getDerivedScale( Ogre::Bone* b )
{
	return new Ogre::Vector3( b->_getDerivedScale() );
}

EXPORTFUNC Ogre::Matrix4* BONE__getFullTransform( Ogre::Bone* b )
{
	return new Ogre::Matrix4( b->_getFullTransform() );
}

EXPORTFUNC void BONE__update( bool updateChildren , bool parentHasChanged , Ogre::Bone* b )
{
	b->_update( updateChildren , parentHasChanged );
}

EXPORTFUNC void BONE_setListener( Ogre::Node::Listener* listener , Ogre::Bone* b )
{
	b->setListener( listener );
}

EXPORTFUNC Ogre::Node::Listener* BONE_getListener( Ogre::Bone* b )
{
	return b->getListener();
}


#endif
