#ifndef H_OGRERENDERWINDOW
#define H_OGRERENDERWINDOW

EXPORTFUNC void RW_swapBuffers( bool waitForVSync , Ogre::RenderWindow* rw )
{
	rw->swapBuffers( waitForVSync );
}

#endif
