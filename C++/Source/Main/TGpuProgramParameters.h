#ifndef H_TGPUPROGRAMPARAMETERS
#define H_TGPUPROGRAMPARAMETERS

/*
Public Types
enum  	AutoConstantType {...} - AutoConstantType wrapped in blitz.
enum  	ACDataType { ACDT_NONE, ACDT_INT, ACDT_REAL }
enum  	ElementType { ET_INT, ET_REAL }
typedef std::vector< AutoConstantEntry > 	AutoConstantList
typedef std::vector< float > 	FloatConstantList
typedef std::vector < int > 	IntConstantList
typedef ConstVectorIterator < AutoConstantList > 	AutoConstantIterator

Public Member Functions

Managed so we don't use these
GpuProgramParameters ()
~GpuProgramParameters ()
GpuProgramParameters (const GpuProgramParameters &oth)

GpuProgramParameters & 	operator= (const GpuProgramParameters &oth)
void 	_setNamedConstants (const GpuNamedConstants *constantmap)
void 	_setLogicalIndexes (GpuLogicalBufferStruct *floatIndexMap, GpuLogicalBufferStruct *intIndexMap)
bool 	hasNamedParameters () const
bool 	hasLogicalIndexedParameters () const
void 	setConstant (size_t index, const Vector4 &vec)
void 	setConstant (size_t index, Real val)
void 	setConstant (size_t index, const Vector3 &vec)
void 	setConstant (size_t index, const Matrix4 &m)
void 	setConstant (size_t index, const Matrix4 *m, size_t numEntries)
void 	setConstant (size_t index, const float *val, size_t count)
void 	setConstant (size_t index, const double *val, size_t count)
void 	setConstant (size_t index, const ColourValue &colour)
void 	setConstant (size_t index, const int *val, size_t count)
void 	_writeRawConstants (size_t physicalIndex, const float *val, size_t count)
void 	_writeRawConstants (size_t physicalIndex, const double *val, size_t count)
void 	_writeRawConstants (size_t physicalIndex, const int *val, size_t count)
void 	_readRawConstants (size_t physicalIndex, size_t count, float *dest)
void 	_readRawConstants (size_t physicalIndex, size_t count, int *dest)
void 	_writeRawConstant (size_t physicalIndex, const Vector4 &vec, size_t count=4)
void 	_writeRawConstant (size_t physicalIndex, Real val)
void 	_writeRawConstant (size_t physicalIndex, int val)
void 	_writeRawConstant (size_t physicalIndex, const Vector3 &vec)
void 	_writeRawConstant (size_t physicalIndex, const Matrix4 &m)
void 	_writeRawConstant (size_t physicalIndex, const Matrix4 *m, size_t numEntries)
void 	_writeRawConstant (size_t physicalIndex, const ColourValue &colour, size_t count=4)
GpuConstantDefinitionIterator 	getConstantDefinitionIterator (void) const
const GpuConstantDefinition & 	getConstantDefinition (const String &name) const
const GpuNamedConstants & 	getConstantDefinitions () const
const GpuLogicalBufferStruct * 	getFloatLogicalBufferStruct () const
size_t 	getFloatLogicalIndexForPhysicalIndex (size_t physicalIndex)
size_t 	getIntLogicalIndexForPhysicalIndex (size_t physicalIndex)
const GpuLogicalBufferStruct * 	getIntLogicalBufferStruct () const
const FloatConstantList & 	getFloatConstantList () const
float * 	getFloatPointer (size_t pos)
const float * 	getFloatPointer (size_t pos) const
const IntConstantList & 	getIntConstantList () const
int * 	getIntPointer (size_t pos)
const int * 	getIntPointer (size_t pos) const
const AutoConstantList & 	getAutoConstantList () const
void 	setAutoConstant (size_t index, AutoConstantType acType, size_t extraInfo=0)
void 	setAutoConstantReal (size_t index, AutoConstantType acType, Real rData)
void 	_setRawAutoConstant (size_t physicalIndex, AutoConstantType acType, size_t extraInfo, size_t elementSize=4)
void 	_setRawAutoConstantReal (size_t physicalIndex, AutoConstantType acType, Real rData, size_t elementSize=4)
void 	clearAutoConstant (size_t index)
void 	setConstantFromTime (size_t index, Real factor)
void 	clearAutoConstants (void)
AutoConstantIterator 	getAutoConstantIterator (void) const
*/
EXPORTFUNC int GPUPP_getAutoConstantCount( Ogre::GpuProgramParameters* p )
{
	return p->getAutoConstantCount();
}

/*
AutoConstantEntry * 	getAutoConstantEntry (const size_t index)
bool 	hasAutoConstants (void) const
const AutoConstantEntry * 	findFloatAutoConstantEntry (size_t logicalIndex)
const AutoConstantEntry * 	findIntAutoConstantEntry (size_t logicalIndex)
const AutoConstantEntry * 	findAutoConstantEntry (const String &paramName)
const AutoConstantEntry * 	_findRawAutoConstantEntryFloat (size_t physicalIndex)
const AutoConstantEntry * 	_findRawAutoConstantEntryInt (size_t physicalIndex)
void 	_updateAutoParamsNoLights (const AutoParamDataSource *source)
void 	_updateAutoParamsLightsOnly (const AutoParamDataSource *source)
void 	setIgnoreMissingParams (bool state)
*/

EXPORTFUNC void GPUPP_setNamedConstantWithFloat(const char* name, float val , Ogre::GpuProgramParameters* p)
{
	p->setNamedConstant( name , val );
}

EXPORTFUNC void GPUPP_setNamedConstantWithInt(const char* name, int val , Ogre::GpuProgramParameters* p)
{
	p->setNamedConstant( name , val );
}

EXPORTFUNC void GPUPP_setNamedConstantWithVec4(const char* name, Ogre::Vector4* vec , Ogre::GpuProgramParameters* p)
{
	p->setNamedConstant( name , *vec );
}

EXPORTFUNC void GPUPP_setNamedConstantWithVec3(const char* name, Ogre::Vector3* vec, Ogre::GpuProgramParameters* p)
{
	p->setNamedConstant( name , *vec );
}

EXPORTFUNC void GPUPP_setNamedConstantWithMatrix4(const char* name, Ogre::Matrix4* m , Ogre::GpuProgramParameters* p)
{
	p->setNamedConstant( name , *m );
}

/* No support for pointer iteration
setNamedConstantWith(const char* name, const Matrix4 *m, size_t numEntries)
setNamedConstant(const char* name, const float *val, size_t count, size_t multiple=4)
setNamedConstant(const char* name, const double *val, size_t count, size_t multiple=4)
*/


EXPORTFUNC void GPUPP_setNamedConstantWithColourValue(const char* name,  Ogre::ColourValue* colour , Ogre::GpuProgramParameters* p)
{
	p->setNamedConstant( name , *colour );
}

/* No support for pointer iterator
setNamedConstant(const char* name, const int *val, size_t count, size_t multiple=4)
*/

EXPORTFUNC void GPUPP_setNamedAutoConstant(const char* name, int acType, int extraInfo , Ogre::GpuProgramParameters* p)
{
	p->setNamedAutoConstant( name , Ogre::GpuProgramParameters::AutoConstantType( acType ) , extraInfo );
}

EXPORTFUNC void GPUPP_setNamedAutoConstantReal(const char* name, int acType, float rData , Ogre::GpuProgramParameters* p)
{
	p->setNamedAutoConstantReal( name , Ogre::GpuProgramParameters::AutoConstantType( acType ) , rData );
}

EXPORTFUNC void GPUPP_setNamedConstantFromTime(const char* name, float factor , Ogre::GpuProgramParameters* p)
{
	p->setNamedConstantFromTime( name , factor );
}

/*
void 	clearNamedAutoConstant (const String &name)
const GpuConstantDefinition * 	_findNamedConstantDefinition (const String &name, bool throwExceptionIfMissing=false) const
size_t 	_getFloatConstantPhysicalIndex (size_t logicalIndex, size_t requestedSize)
size_t 	_getIntConstantPhysicalIndex (size_t logicalIndex, size_t requestedSize)
void 	setTransposeMatrices (bool val)
bool 	getTransposeMatrices (void) const
void 	copyConstantsFrom (const GpuProgramParameters &source)
void 	incPassIterationNumber (void)
bool 	hasPassIterationNumber () const
size_t 	getPassIterationNumberIndex () const

No need for alloc
void * 	operator new (size_t sz, const char *file, int line, const char *func)
void * 	operator new (size_t sz)
void * 	operator new (size_t sz, void *ptr)
void * 	operator new[] (size_t sz, const char *file, int line, const char *func)
void * 	operator new[] (size_t sz)
void 	operator delete (void *ptr)
void 	operator delete (void *ptr, const char *, int, const char *)
void 	operator delete[] (void *ptr)
void 	operator delete[] (void *ptr, const char *, int, const char *)

Static Public Member Functions
static const AutoConstantDefinition * 	getAutoConstantDefinition (const String &name)
static const AutoConstantDefinition * 	getAutoConstantDefinition (const size_t idx)
static size_t 	getNumAutoConstantDefinitions (void)

*/

#endif