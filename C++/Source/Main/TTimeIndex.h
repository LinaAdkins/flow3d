#ifndef H__TTIMEINDEX
#define H__TTIMEINDEX

EXPORTFUNC Ogre::TimeIndex* TIMEINDEX_create( float timePos )
{
	return new Ogre::TimeIndex( timePos );
}

EXPORTFUNC Ogre::TimeIndex* TIMEINDEX_createWithIndex( float timePos , int keyIndex )
{
	return new Ogre::TimeIndex( timePos , keyIndex );
}

EXPORTFUNC bool TIMEINDEX_hasKeyIndex( Ogre::TimeIndex* t )
{
	return t->hasKeyIndex();
}

EXPORTFUNC float TIMEINDEX_getTimePos( Ogre::TimeIndex* t )
{
	return t->getTimePos();
}

EXPORTFUNC int TIMEINDEX_getKeyIndex( Ogre::TimeIndex* t )
{
	return t->getKeyIndex();
}

EXPORTFUNC void TIMEINDEX_delete( Ogre::TimeIndex* t )
{
	delete t;
}

#endif
