#ifndef H_TNODEANIMATIONTRACK
#define H_TNODEANIMATIONTRACK

EXPORTFUNC Ogre::TransformKeyFrame* NODEANIMTRACK_createNodeKeyFrame( float timePos , Ogre::NodeAnimationTrack* t )
{
	return t->createNodeKeyFrame( timePos );
}

EXPORTFUNC Ogre::Node* NODEANIMTRACK_getAssociatedNode( Ogre::NodeAnimationTrack* t )
{
	return t->getAssociatedNode();
}

EXPORTFUNC void NODEANIMTRACK_setAssociatedNode( Ogre::Node* node , Ogre::NodeAnimationTrack* t )
{
	t->setAssociatedNode( node );
}

EXPORTFUNC void NODEANIMTRACK_apply( float timeIndex , float weight , float scale , Ogre::NodeAnimationTrack* t )
{
	t->apply( timeIndex , weight , scale  );
}

EXPORTFUNC void NODEANIMTRACK_applyWithTimeIndex( Ogre::TimeIndex* timeIndex , float weight , float scale , Ogre::NodeAnimationTrack* t )
{
	t->apply( *timeIndex , weight , scale );
}

#endif
