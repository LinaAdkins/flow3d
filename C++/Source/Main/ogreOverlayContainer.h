#ifndef H_OGREOVERLAYCONTAINER
#define H_OGREOVERLAYCONTAINER

EXPORTFUNC Ogre::OverlayElement* OVERLAYCONTAINER_getChild( const char* name , Ogre::OverlayContainer* c )
{
	return c->getChild(name);
}

#endif
