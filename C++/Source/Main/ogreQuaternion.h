#ifndef H_OGREQUATERNION
#define H_OGREQUATERNION

EXPORTFUNC Ogre::Quaternion* QUAT_createEmpty()
{
	return new Ogre::Quaternion();
}

EXPORTFUNC Ogre::Quaternion* QUAT_create( float fW , float fX , float fY , float fZ )
{
	return new Ogre::Quaternion( fW , fX , fY , fZ );
}

EXPORTFUNC Ogre::Quaternion* QUAT_createWithQuaternion( Ogre::Quaternion* rkQ )
{
	return new Ogre::Quaternion( *rkQ );
}

EXPORTFUNC Ogre::Quaternion* QUAT_createWithMatrix3( Ogre::Matrix3* rot )
{
	return new Ogre::Quaternion( *rot );
}

EXPORTFUNC Ogre::Quaternion* QUAT_createWithAngleAxis( Ogre::Radian* rfAngle , Ogre::Vector3* rkAxis )
{
	return new Ogre::Quaternion( *rfAngle , *rkAxis );
}

EXPORTFUNC Ogre::Quaternion* QUAT_createWith3OrthoAxes( Ogre::Vector3* xaxis , Ogre::Vector3* yaxis , Ogre::Vector3* zaxis )
{
	return new Ogre::Quaternion( *xaxis , *yaxis , *zaxis );

}

// BlitzMax custom type arrays are not compatible, users will have to use createWith3OrthoAxes
//void* QUAT_createWith3OrthoAxesPtr( Ogre::Vector3* akAxis )
EXPORTFUNC Ogre::Quaternion* QUAT_createWithFloatPtr( float* valptr )
{
	return new Ogre::Quaternion( valptr );

}
// Name changed since [] operators cannot be used-
EXPORTFUNC float QUAT_getValue( int i , Ogre::Quaternion* q )
{
	return (*q)[i];
}

// Name changed since [] operators cannot be used-
EXPORTFUNC void QUAT_setValue( int i, float value , Ogre::Quaternion* q )
{
	(*q)[i]=value;
}

//Real &operator[] (const size_t i) - Unnecessary
// Uneccessary, pointer to first element is in the type itself-
//float* QUAT_ptr( Ogre::Quaternion* q )
//const Real* ptr() const - Unnecessary
EXPORTFUNC void QUAT_FromRotationMatrix( Ogre::Matrix3* kRot , Ogre::Quaternion* q )
{
	q->FromRotationMatrix( *kRot );
}
EXPORTFUNC void QUAT_ToRotationMatrix( Ogre::Matrix3* kRot , Ogre::Quaternion* q )
{
	q->ToRotationMatrix( *kRot );
}
EXPORTFUNC void QUAT_FromAngleAxis( Ogre::Radian* rfAngle , Ogre::Vector3* rkAxis , Ogre::Quaternion* q )
{
	q->FromAngleAxis( *rfAngle , *rkAxis );
}

EXPORTFUNC void QUAT_ToAngleAxis( Ogre::Radian* rfAngle , Ogre::Vector3* rkAxis , Ogre::Quaternion* q )
{
	q->ToAngleAxis( *rfAngle , *rkAxis );
}

EXPORTFUNC void QUAT_ToAngleAxisWithDegree( Ogre::Degree* dAngle , Ogre::Vector3* rkAxis , Ogre::Quaternion* q )
{
	q->ToAngleAxis( *dAngle , *rkAxis );
}

//Custom type arrays are not allowed-
//void QUAT_FromAxesPtr( Ogre::Vector3* akAxis , Ogre::Quaternion* q )
EXPORTFUNC void QUAT_FromAxes( Ogre::Vector3* xAxis , Ogre::Vector3* yAxis , Ogre::Vector3* zAxis , Ogre::Quaternion* q )
{
	q->FromAxes( *xAxis , *yAxis , *zAxis );
}
//Custom type arrays are not allowed-
//void QUAT_ToAxesPtr( Ogre::Vector3* akAxis , Ogre::Quaternion* q )
EXPORTFUNC void QUAT_ToAxes( Ogre::Vector3* xAxis , Ogre::Vector3* yAxis , Ogre::Vector3* zAxis , Ogre::Quaternion* q )
{
	q->ToAxes( *xAxis , *yAxis , *zAxis );
}

EXPORTFUNC Ogre::Vector3* QUAT_xAxis( Ogre::Quaternion* q )
{
	return new Ogre::Vector3(q->xAxis());
}

EXPORTFUNC Ogre::Vector3* QUAT_yAxis( Ogre::Quaternion* q )
{
	return new Ogre::Vector3(q->yAxis());
}

EXPORTFUNC Ogre::Vector3* QUAT_zAxis( Ogre::Quaternion* q )
{
	return new Ogre::Vector3(q->zAxis());
}



EXPORTFUNC void QUAT_eq( Ogre::Quaternion* rkQ , Ogre::Quaternion* q )
{
	*q=*rkQ;
}

EXPORTFUNC Ogre::Quaternion* QUAT_add( Ogre::Quaternion* rkQ , Ogre::Quaternion* q )
{
	return new Ogre::Quaternion( *q+*q );
}



EXPORTFUNC Ogre::Quaternion* QUAT_sub( Ogre::Quaternion* rkQ , Ogre::Quaternion* q )
{
	return new Ogre::Quaternion( *q-*rkQ );
}

EXPORTFUNC Ogre::Quaternion* QUAT_mul( Ogre::Quaternion* rkQ , Ogre::Quaternion* q )
{
	return new Ogre::Quaternion( (*q) * (*rkQ) );
}

EXPORTFUNC Ogre::Quaternion* QUAT_mulWithScalar( float fScalar , Ogre::Quaternion* q )
{
	return new Ogre::Quaternion( (*q) * fScalar );
}

EXPORTFUNC Ogre::Quaternion* QUAT_negative( Ogre::Quaternion* q )
{
	return new Ogre::Quaternion( -(*q) );
}




EXPORTFUNC int QUAT_isEqualTo( Ogre::Quaternion* rhs , Ogre::Quaternion* q )
{
	return *q==*rhs;
}

EXPORTFUNC int QUAT_isNotEqualTo( Ogre::Quaternion* rhs , Ogre::Quaternion* q )
{
	return *q!=*rhs;
}
EXPORTFUNC float QUAT_Dot( Ogre::Quaternion* rkQ , Ogre::Quaternion* q)
{
	return q->Dot(*rkQ);
}

EXPORTFUNC float QUAT_Norm( Ogre::Quaternion* q )
{
	return q->Norm();
}

EXPORTFUNC float QUAT_normalise( Ogre::Quaternion* q )
{
	return q->normalise();
}
EXPORTFUNC Ogre::Quaternion* QUAT_Inverse( Ogre::Quaternion* q )
{
	return new Ogre::Quaternion( q->Inverse() );
}


EXPORTFUNC Ogre::Quaternion* QUAT_UnitInverse( Ogre::Quaternion* q )
{
	return new Ogre::Quaternion( q->UnitInverse() );

}
EXPORTFUNC Ogre::Quaternion* QUAT_Exp( Ogre::Quaternion* q )
{
	return new Ogre::Quaternion( q->Exp() );
}

EXPORTFUNC Ogre::Quaternion* QUAT_Log( Ogre::Quaternion* q )
{
	return new Ogre::Quaternion( q->Log() );
}
EXPORTFUNC Ogre::Vector3* QUAT_mulWithVector3( Ogre::Vector3* rkVector , Ogre::Quaternion* q )
{
	return new Ogre::Vector3( (*q) * (*rkVector) );
}

EXPORTFUNC Ogre::Radian* QUAT_getRoll( bool reprojectAxis , Ogre::Quaternion* q )
{
	return new Ogre::Radian( q->getRoll( reprojectAxis ) );
}

EXPORTFUNC Ogre::Radian* QUAT_getPitch( bool reprojectAxis , Ogre::Quaternion* q )
{
	return new Ogre::Radian( q->getPitch( reprojectAxis) );
}

EXPORTFUNC Ogre::Radian* QUAT_getYaw( bool reprojectAxis , Ogre::Quaternion* q )
{
	return new Ogre::Radian( q->getYaw( reprojectAxis ) );
}

EXPORTFUNC int QUAT_isEqualToWithTolerance( Ogre::Quaternion* rhs , Ogre::Radian* tolerance , Ogre::Quaternion* q )
{
	return q->equals( *rhs , *tolerance );
}

EXPORTFUNC void QUAT_delete( Ogre::Quaternion* ogreQuaternion )
{
	delete ogreQuaternion;
}

// Additional Accessors
EXPORTFUNC float QUAT_getW( Ogre::Quaternion* q )
{
	return q->w;
}

EXPORTFUNC float QUAT_getX( Ogre::Quaternion* q )
{
	return q->x;
}

EXPORTFUNC float QUAT_getY( Ogre::Quaternion* q )
{
	return q->y;
}

EXPORTFUNC float QUAT_getZ( Ogre::Quaternion* q )
{
	return q->z;
}

EXPORTFUNC void QUAT_setW( float w , Ogre::Quaternion* q )
{
	q->w = w;
}

EXPORTFUNC void QUAT_setX( float x , Ogre::Quaternion* q )
{
	q->x = x;
}

EXPORTFUNC void QUAT_setY( float y , Ogre::Quaternion* q )
{
	q->y = y;
}

EXPORTFUNC void QUAT_setZ( float z , Ogre::Quaternion* q )
{
	q->z = z;
}

EXPORTFUNC void QUAT_setAll( float w , float x , float y , float z , Ogre::Quaternion* q )
{
	q->w = w;
	q->x = x;
	q->y = y;
	q->z = z;
}


//Static Member Functions
EXPORTFUNC Ogre::Quaternion* QUAT_Slerp( float fT , Ogre::Quaternion* rkP , Ogre::Quaternion* rkQ , bool shortestPath )
{
	return new Ogre::Quaternion( Ogre::Quaternion::Slerp( fT , *rkP , *rkQ , shortestPath ) );
}

EXPORTFUNC Ogre::Quaternion* QUAT_SlerpExtraSpins( float fT , Ogre::Quaternion* rkP , Ogre::Quaternion* rkQ , int iExtraSpins )
{
	return new Ogre::Quaternion( Ogre::Quaternion::SlerpExtraSpins( fT , *rkP , *rkQ , iExtraSpins ) );
}

EXPORTFUNC void QUAT_Intermediate( Ogre::Quaternion* rkQ0 , Ogre::Quaternion* rkQ1 , Ogre::Quaternion* rkQ2 , Ogre::Quaternion* rka , Ogre::Quaternion* rkB )
{
	Ogre::Quaternion::Intermediate( *rkQ0 , *rkQ1 , *rkQ2 , *rka , *rkB );
}
EXPORTFUNC Ogre::Quaternion* QUAT_Squad( float fT , Ogre::Quaternion* rkP , Ogre::Quaternion* rkA , Ogre::Quaternion* rkB , Ogre::Quaternion* rkQ , bool shortestPath )
{
	return new Ogre::Quaternion( Ogre::Quaternion::Squad( fT , *rkP , *rkA , *rkB , *rkQ , shortestPath ) );
}

EXPORTFUNC Ogre::Quaternion* QUAT_nlerp( float fT , Ogre::Quaternion* rkP , Ogre::Quaternion* rkQ , bool shortestPath )
{
	return new Ogre::Quaternion( Ogre::Quaternion::nlerp( fT , *rkP , *rkQ , shortestPath ) );
}

//Static Attributes
EXPORTFUNC float* QUAT_ms_fEpsilon()
{
	return (float*)&Ogre::Quaternion::ms_fEpsilon;
}

EXPORTFUNC Ogre::Quaternion* QUAT_ZERO()
{
	return (Ogre::Quaternion*)&Ogre::Quaternion::ZERO;
}

EXPORTFUNC Ogre::Quaternion* QUAT_IDENTITY()
{
	return (Ogre::Quaternion*)&Ogre::Quaternion::IDENTITY;
}
//Friends
//_OgreExport friend Quaternion 	operator * (Real fScalar, const Quaternion &rkQ)
// Supplanted by mulScalar above

//_OgreExport friend std::ostream & 	operator<< (std::ostream &o, const Quaternion &q)
// Supplanted by a toStr() functions for Ogre::Quaternion
EXPORTFUNC const char* QUAT_toStr( Ogre::Quaternion* q )
{
	std::ostringstream os;
	os << *q;
	tempString.clear();
	tempString = os.str();
	return tempString.c_str();
}



#endif
