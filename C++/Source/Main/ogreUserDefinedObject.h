#ifndef H_OGREUSERDEFINEDOBJECT
#define H_OGREUSERDEFINEDOBJECT

// As we cannot actually subclass from the blitzmax interface, here we have
// created a generic user defined object that has a vector to hold object 
// references as well as a variable for holding OSM list properties.

class FlowUserObject : public Ogre::UserDefinedObject
{

public:

Ogre::UserDefinedObject* getCustomObject() const
{
	return customObject.isEmpty() ? 0 : Ogre::any_cast<UserDefinedObject*>(customObject); 
}
void setCustomObject( Ogre::UserDefinedObject* object )
{
	customObject = Ogre::Any( object );
}

Ogre::NameValuePairList* getOSMPropertyList()
{
	return &m_osmPropertyList;
}

void setOSMPropertyList( Ogre::NameValuePairList* list )
{
	m_osmPropertyList = *list;
}


private:
	Ogre::NameValuePairList m_osmPropertyList;
	Ogre::Any customObject;

};

EXPORTFUNC FlowUserObject* UO_create()
{
	return new FlowUserObject();
}

EXPORTFUNC void UO_delete( FlowUserObject* u )
{		
	delete u;
}

EXPORTFUNC Ogre::UserDefinedObject* UO_getCustomObject( FlowUserObject* u )
{
	return u->getCustomObject();
}

EXPORTFUNC void UO_setCustomObject( Ogre::UserDefinedObject* object , FlowUserObject* u )
{
	u->setCustomObject( object );
}


EXPORTFUNC Ogre::NameValuePairList* UO_getOSMPropertyList( FlowUserObject* u )
{
	return u->getOSMPropertyList();
}

EXPORTFUNC void UO_setOSMPropertyList( Ogre::NameValuePairList* list , FlowUserObject* u )
{
	u->setOSMPropertyList( list );
}

#endif
