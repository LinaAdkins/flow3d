#ifndef H_TANIMATIONITERATOR
#define H_TANIMATIONITERATOR

/*
	Manual Construction Not Allowed
 	MapIterator (typename T::iterator start, typename T::iterator end)
 	MapIterator (T &c)
*/

EXPORTFUNC void ANIMITER_delete( Ogre::SceneManager::AnimationIterator* i )
{
	delete i;
}

EXPORTFUNC bool ANIMITER_hasMoreElements( Ogre::SceneManager::AnimationIterator* i )
{
	return i->hasMoreElements();
}

EXPORTFUNC Ogre::Animation* ANIMITER_getNext( Ogre::SceneManager::AnimationIterator* i )
{
	return i->getNext();
}

/* 
	Peek, etc Skipped
	T::mapped_type 	peekNextValue (void)
 		Returns the next value element in the collection, without advancing to the next.
	T::key_type 	peekNextKey (void)
 		Returns the next key element in the collection, without advancing to the next.
	MapIterator< T > & 	operator= (MapIterator< T > &rhs)
 		Required to overcome intermittent bug.
	T::mapped_type * 	peekNextValuePtr (void)
 		Returns a pointer to the next value element in the collection, without advancing to the next afterwards.
	void 	moveNext (void)
 		Moves the iterator on one element. 
*/

#endif
