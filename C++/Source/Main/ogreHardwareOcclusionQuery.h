#ifndef H_OGREHARDWAREOCCLUSIONQUERY
#define H_OGREHARDWAREOCCLUSIONQUERY

EXPORTFUNC void HOQ_delete( Ogre::HardwareOcclusionQuery* ogreHOQ )
{
	delete ogreHOQ;
}

EXPORTFUNC void HOQ_beginOcclusionQuery( Ogre::HardwareOcclusionQuery* ogreHOQ )
{
	ogreHOQ->beginOcclusionQuery();
}

EXPORTFUNC void HOQ_endOcclusionQuery( Ogre::HardwareOcclusionQuery* ogreHOQ )
{
	ogreHOQ->endOcclusionQuery();
}

EXPORTFUNC int HOQ_pullOcclusionQuery( unsigned int* NumOfFragments , Ogre::HardwareOcclusionQuery* ogreHOQ )
{
	return ogreHOQ->pullOcclusionQuery( NumOfFragments );
}

EXPORTFUNC unsigned int HOQ_getLastQuerysPixelcount( Ogre::HardwareOcclusionQuery* ogreHOQ )
{
	return ogreHOQ->getLastQuerysPixelcount();
}

EXPORTFUNC int HOQ_isStillOutstanding( Ogre::HardwareOcclusionQuery* ogreHOQ )
{
	return ogreHOQ->isStillOutstanding();
}


#endif
