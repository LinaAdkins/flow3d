#ifndef H_OGRELOGMANAGER
#define H_OGRELOGMANAGER

EXPORTFUNC Ogre::LogManager* LM_create()
{
	return new Ogre::LogManager();
}

EXPORTFUNC Ogre::Log* LM_createLog( const char* name , bool defaultLog , bool debuggerOutput , bool suppressFileOutput )
{
	return Ogre::LogManager::getSingleton().createLog( name , defaultLog , debuggerOutput , suppressFileOutput );
}

EXPORTFUNC void LM_logMessage( int loggingMessageLevel , const char* message, bool debug )
{
	Ogre::LogManager::getSingleton().logMessage( (Ogre::LogMessageLevel) loggingMessageLevel , message , debug );
}

EXPORTFUNC void LM_setLogDetail( int loggingLevel )
{
	Ogre::LogManager::getSingleton().setLogDetail( (Ogre::LoggingLevel) loggingLevel );
}

EXPORTFUNC Ogre::Log* LM_getDefaultLog()
{
	return Ogre::LogManager::getSingleton().getDefaultLog();
}

#endif
