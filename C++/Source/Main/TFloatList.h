#ifndef H__TFLOATLIST
#define H__TFLOATLIST

typedef std::vector<float> FloatList;

EXPORTFUNC FloatList* FLOATLIST_create()
{
	return new FloatList();
}

EXPORTFUNC void FLOATLIST_delete( FloatList* l )
{
	delete l;
}

EXPORTFUNC int FLOATLIST_size( FloatList* l )
{
	return l->size();
}

EXPORTFUNC void FLOATLIST_reserve( int size , FloatList* l )
{
	l->reserve( size );
}

EXPORTFUNC void FLOATLIST_push_back( float value , FloatList* l )
{
	l->push_back( value );
}

EXPORTFUNC void FLOATLIST_add3DPoint( float p1 , float p2 , float p3 , FloatList* l )
{
	l->push_back( p1 ); l->push_back( p2 ); l->push_back(p3);
}

EXPORTFUNC void FLOATLIST_add2DPoint( float u , float v , FloatList* l )
{
	l->push_back( u ); l->push_back( v );
}

EXPORTFUNC void FLOATLIST_clear( FloatList* l )
{
	l->clear();
}

EXPORTFUNC float FLOATLIST_getValue( int index , FloatList* l )
{
	return (*l)[index];
}

#endif