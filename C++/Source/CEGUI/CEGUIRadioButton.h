#ifndef H_CEGUIRADIOBUTTON
#define H_CEGUIRADIOBUTTON

EXPORTFUNC int CEGUIRADIO_isSelected( CEGUI::RadioButton* radiobutton )
{
	return radiobutton->isSelected();
}

EXPORTFUNC long CEGUIRADIO_getGroupID( CEGUI::RadioButton* radiobutton )
{
	return  radiobutton->getGroupID();
}

EXPORTFUNC CEGUI::RadioButton* CEGUIRADIO_getSelectedButtonInGroup( CEGUI::RadioButton* radiobutton )
{
	return radiobutton->getSelectedButtonInGroup();
}

EXPORTFUNC void CEGUIRADIO_setSelected( bool select , CEGUI::RadioButton* radiobutton )
{
	radiobutton->setSelected( select );
}

EXPORTFUNC void CEGUIRADIO_setGroupID( long group , CEGUI::RadioButton* radiobutton )
{
	radiobutton->setGroupID( group );
}

#endif
