#ifndef H_CEGUIITEMLISTBOX
#define H_CEGUIITEMLISTBOX

EXPORTFUNC int CEGUIILBOX_getSelectedCount( CEGUI::ItemListbox* ilb )
{
	return (int)ilb->getSelectedCount();
}

EXPORTFUNC CEGUI::ItemEntry* CEGUIILBOX_getLastSelectedItem( CEGUI::ItemListbox* ilb )
{
	return ilb->getLastSelectedItem();
}

EXPORTFUNC CEGUI::ItemEntry* CEGUIILBOX_getFirstSelectedItem( size_t start_index , CEGUI::ItemListbox* ilb )
{
	return ilb->getFirstSelectedItem( start_index );
}

EXPORTFUNC CEGUI::ItemEntry* CEGUIILBOX_getNextSelectedItem( CEGUI::ItemListbox* ilb )
{
	return ilb->getNextSelectedItem();
}

EXPORTFUNC CEGUI::ItemEntry* CEGUIILBOX_getNextSelectedItemAfter( CEGUI::ItemEntry* start_item , CEGUI::ItemListbox* ilb )
{
	return ilb->getNextSelectedItemAfter( start_item );
}

EXPORTFUNC int CEGUIILBOX_isMultiSelectEnabled( CEGUI::ItemListbox* ilb )
{
	return ilb->isMultiSelectEnabled();
}

EXPORTFUNC int CEGUIILBOX_isItemSelected( size_t index , CEGUI::ItemListbox* ilb )
{
	return ilb->isItemSelected( index );
}

EXPORTFUNC void CEGUIILBOX_setMultiSelectEnabled( bool state , CEGUI::ItemListbox* ilb )
{
	ilb->setMultiSelectEnabled( state );
}

EXPORTFUNC void CEGUIILBOX_clearAllSelections( CEGUI::ItemListbox* ilb )
{
	ilb->clearAllSelections();
}

EXPORTFUNC void CEGUIILBOX_selectRange( size_t a , size_t z , CEGUI::ItemListbox* ilb )
{
	ilb->selectRange( a , z );
}

EXPORTFUNC void CEGUIILBOX_selectAllItems( CEGUI::ItemListbox* ilb )
{
	ilb->selectAllItems();
}





#endif
