#ifndef H_CEGUIMENUBASE
#define H_CEGUIMENUBASE

EXPORTFUNC float CEGUIMENUBASE_getItemSpacing( CEGUI::MenuBase* MenuBase )
{
	return MenuBase->getItemSpacing();
}

EXPORTFUNC int CEGUIMENUBASE_isMultiplePopupsAllowed( CEGUI::MenuBase* MenuBase )
{
	return MenuBase->isMultiplePopupsAllowed();
}

EXPORTFUNC CEGUI::MenuItem* CEGUIMENUBASE_getPopupMenuItem( CEGUI::MenuBase* MenuBase )
{
	return MenuBase->getPopupMenuItem();
}

EXPORTFUNC void CEGUIMENUBASE_setItemSpacing( float spacing , CEGUI::MenuBase* MenuBase )
{
	MenuBase->setItemSpacing( spacing );
}

EXPORTFUNC void CEGUIMENUBASE_changePopupMenuItem( CEGUI::MenuItem* item , CEGUI::MenuBase* MenuBase )
{
	MenuBase->changePopupMenuItem( item );
}

EXPORTFUNC void CEGUIMENUBASE_setAllowMultiplePopups( bool setting , CEGUI::MenuBase* MenuBase )
{
	MenuBase->setAllowMultiplePopups( setting );
}

#endif
