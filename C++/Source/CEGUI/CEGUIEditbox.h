#ifndef H_CEGUIEDITBOX
#define H_CEGUIEDITBOX

EXPORTFUNC int CEGUIEDITBOX_hasInputFocus( CEGUI::Editbox* editbox )
{
	return editbox->hasInputFocus();
}

EXPORTFUNC int CEGUIEDITBOX_isReadOnly( CEGUI::Editbox* editbox  )
{
	return editbox->isReadOnly();
}

EXPORTFUNC int CEGUIEDITBOX_isTextMasked( CEGUI::Editbox* editbox )
{
	return editbox->isTextMasked();
}

EXPORTFUNC int CEGUIEDITBOX_isTextValid( CEGUI::Editbox* editbox )
{
	return editbox->isTextValid();
}

EXPORTFUNC const char* CEGUIEDITBOX_getValidationString( CEGUI::Editbox* editbox )
{
	return editbox->getValidationString().c_str();
}


EXPORTFUNC int CEGUIEDITBOX_getCaratIndex( CEGUI::Editbox* editbox )
{
	return (int)editbox->getCaratIndex();
}

EXPORTFUNC int CEGUIEDITBOX_getSelectionStartIndex( CEGUI::Editbox* editbox )
{
	return (int)editbox->getSelectionStartIndex();
}

EXPORTFUNC int CEGUIEDITBOX_getSelectionEndIndex( CEGUI::Editbox* editbox )
{
	return (int)editbox->getSelectionEndIndex();
}


EXPORTFUNC int CEGUIEDITBOX_getSelectionLength( CEGUI::Editbox* editbox )
{
	return (int)editbox->getSelectionLength();
}
EXPORTFUNC int CEGUIEDITBOX_getMaskCodePoint( CEGUI::Editbox* editbox )
{
	return editbox->getMaskCodePoint();
}

EXPORTFUNC int CEGUIEDITBOX_getMaxTextLength( CEGUI::Editbox* editbox )
{
	return (int)editbox->getMaxTextLength();
}
EXPORTFUNC void CEGUIEDITBOX_setReadOnly( bool setting , CEGUI::Editbox* editbox )
{
	editbox->setReadOnly( setting );
}
EXPORTFUNC void CEGUIEDITBOX_setTextMasked( bool setting , CEGUI::Editbox* editbox )
{
	editbox->setTextMasked( setting );
}
EXPORTFUNC void CEGUIEDITBOX_setValidationString( const char* validation_string , CEGUI::Editbox* editbox )
{
	editbox->setValidationString( validation_string );
}
EXPORTFUNC void CEGUIEDITBOX_setCaratIndex( int carat_pos , CEGUI::Editbox* editbox )
{
	editbox->setCaratIndex( carat_pos );
}
EXPORTFUNC void CEGUIEDITBOX_setSelection( int start_pos , int end_pos , CEGUI::Editbox* editbox )
{
	editbox->setSelection( start_pos , end_pos );
}
EXPORTFUNC void CEGUIEDITBOX_setMaskCodePoint( int code_point , CEGUI::Editbox* editbox )
{
	editbox->setMaskCodePoint( code_point );
}

EXPORTFUNC void CEGUIEDITBOX_setMaxTextLength( int max_len , CEGUI::Editbox* editbox )
{
	editbox->setMaxTextLength( max_len );
}

#endif
