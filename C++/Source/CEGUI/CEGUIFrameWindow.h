#ifndef H_CEGUIFRAMEWINDOW
#define H_CEGUIFRAMEWINDOW

// Commonly needed subscriber events until event subscribing is worked out-
bool closeWindow( const CEGUI::EventArgs& e)
{
	static_cast<const CEGUI::WindowEventArgs&>(e).window->hide();
	return true;
}

EXPORTFUNC void CEGUIFW_setSizingEnabled( bool setting , CEGUI::FrameWindow* ceguiFrameWindow )
{
	ceguiFrameWindow->setSizingEnabled( setting );
}

EXPORTFUNC void CEGUIFW_setRollupEnabled( bool setting , CEGUI::FrameWindow* ceguiFrameWindow )
{
	ceguiFrameWindow->setRollupEnabled( setting );
}

EXPORTFUNC void CEGUIFW_setCloseButtonEnabled( bool setting , CEGUI::FrameWindow* ceguiFrameWindow )
{
	ceguiFrameWindow->setCloseButtonEnabled( setting );
}

EXPORTFUNC void CEGUIFW_setFrameEnabled( bool setting , CEGUI::FrameWindow* ceguiFrameWindow )
{
	ceguiFrameWindow->setFrameEnabled( setting );
}

EXPORTFUNC void CEGUIFW_setTitleBarEnabled( bool setting , CEGUI::FrameWindow* ceguiFrameWindow )
{
	ceguiFrameWindow->setTitleBarEnabled( setting );
}



EXPORTFUNC void CEGUIFW_hideOnClose( CEGUI::FrameWindow* ceguiFrameWindow )
{
	ceguiFrameWindow->subscribeEvent( CEGUI::FrameWindow::EventCloseClicked ,
		CEGUI::Event::Subscriber::SubscriberSlot(&closeWindow) );
}







#endif
