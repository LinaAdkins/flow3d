#ifndef H_CEGUISPINNER
#define H_CEGUISPINNER

EXPORTFUNC float CEGUISPINNER_getCurrentValue( CEGUI::Spinner* spinner )
{
	return spinner->getCurrentValue();
}

EXPORTFUNC float CEGUISPINNER_getStepSize( CEGUI::Spinner* spinner )
{
	return spinner->getStepSize();
}

EXPORTFUNC float CEGUISPINNER_getMaximumValue( CEGUI::Spinner* spinner )
{
	return spinner->getMaximumValue();
}

EXPORTFUNC float CEGUISPINNER_getMinimumValue( CEGUI::Spinner* spinner )
{
	return spinner->getMinimumValue();
}

EXPORTFUNC int CEGUISPINNER_getTextInputMode( CEGUI::Spinner* spinner )
{
	return spinner->getTextInputMode();
}
EXPORTFUNC void CEGUISPINNER_setCurrentValue( float value , CEGUI::Spinner* spinner )
{
	spinner->setCurrentValue( value );
}

EXPORTFUNC void CEGUISPINNER_setStepSize( float step , CEGUI::Spinner* spinner )
{
	spinner->setStepSize( step );
}

EXPORTFUNC void CEGUISPINNER_setMaximumValue( float maxValue , CEGUI::Spinner* spinner )
{
	spinner->setMaximumValue( maxValue );
}

EXPORTFUNC void CEGUISPINNER_setMinimumValue( float minValue , CEGUI::Spinner* spinner )
{
	spinner->setMinimumValue( minValue );
}

EXPORTFUNC void CEGUISPINNER_setTextInputMode( int mode , CEGUI::Spinner* spinner )
{
	spinner->setTextInputMode( (CEGUI::Spinner::TextInputMode) mode );
}

#endif
