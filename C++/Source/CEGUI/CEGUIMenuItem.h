#ifndef H_CEGUIMENUITEM
#define H_CEGUIMENUITEM

EXPORTFUNC int CEGUIMI_isHovering( CEGUI::MenuItem* mi )
{
	return mi->isHovering();
}

EXPORTFUNC int CEGUIMI_isPushed( CEGUI::MenuItem* mi )
{
	return mi->isPushed();
}

EXPORTFUNC int CEGUIMI_isOpened( CEGUI::MenuItem* mi )
{
	return mi->isOpened();
}

#endif
