#ifndef H_CEGUISYSTEM
#define H_CEGUISYSTEM

//Convert from BlitzMax keycodes to ASCII Keycodes
int MaxToASCII( int c )
{
	int cs = 0;
	switch(c)
	{
	case 8:
		cs = CEGUI::Key::Backspace;
		break;
	case 9:
		cs = CEGUI::Key::Tab;
		break;

	case 12:
		cs = CEGUI::Key::NumLock;
		break;

	case 13:
		cs = CEGUI::Key::Return;
		break;
	
	case 27:
		cs = CEGUI::Key::Escape;
		break;

	case 32:
		cs = CEGUI::Key::Space;
		break;

	case 33:
		cs = CEGUI::Key::PageUp;
		break;

	case 34:
		cs = CEGUI::Key::PageDown;
		break;

	case 35:
		cs = CEGUI::Key::End;
		break;

	case 36:
		cs = CEGUI::Key::Home;
		break;

	case 37:
		cs = CEGUI::Key::ArrowLeft;
		break;

	case 38:
		cs = CEGUI::Key::ArrowUp;
		break;

	case 39:
		cs = CEGUI::Key::ArrowRight;
		break;

	case 40:
		cs = CEGUI::Key::ArrowDown;
		break;

	//KEY_SELECT
	case 41:
		break;

	//KEY_PRINT
	case 42:
		break;

	//KEY_EXECUTE
	case 43:
		break;

	//KEY_SCREEN
	case 44:
		break;

	case 45:
		cs = CEGUI::Key::Insert;
		break;

	case 46:
		cs = CEGUI::Key::Delete;
		break;

	case 48:
		cs = CEGUI::Key::Zero;
		break;

	case 49:
		cs = CEGUI::Key::One;
		break;

	case 50:
		cs = CEGUI::Key::Two;
		break;

	case 51:
		cs = CEGUI::Key::Three;
		break;

	case 52:
		cs = CEGUI::Key::Four;
		break;

	case 53:
		cs = CEGUI::Key::Five;
		break;

	case 54:
		cs = CEGUI::Key::Six;
		break;
	
	case 55:
		cs = CEGUI::Key::Seven;
		break;
	
	case 56:
		cs = CEGUI::Key::Eight;
		break;

	case 57:
		cs = CEGUI::Key::Nine;
		break;

	case 65:
		cs = CEGUI::Key::A;
		break; 

	case 66:
		cs = CEGUI::Key::B;
		break;

	case 67:
		cs = CEGUI::Key::C;
		break;

	case 68:
		cs = CEGUI::Key::D;
		break;

	case 69:
		cs = CEGUI::Key::E;
		break;

	case 70:
		cs = CEGUI::Key::F;
		break;

	case 71:
		cs = CEGUI::Key::G;
		break;

	case 72:
		cs = CEGUI::Key::H;
		break;

	case 73:
		cs = CEGUI::Key::I;
		break;

	case 74:
		cs = CEGUI::Key::J;
		break;

	case 75:
		cs = CEGUI::Key::K;
		break;

	case 76:
		cs = CEGUI::Key::L;
		break;

	case 77:
		cs = CEGUI::Key::M;
		break;

	case 78:
		cs = CEGUI::Key::N;
		break;

	case 79:
		cs = CEGUI::Key::O;
		break;

	case 80:
		cs = CEGUI::Key::P;
		break;

	case 81:
		cs = CEGUI::Key::Q;
		break;

	case 82:
		cs = CEGUI::Key::R;
		break;

	case 83:
		cs = CEGUI::Key::S;
		break;

	case 84:
		cs = CEGUI::Key::T;
		break;

	case 85:
		cs = CEGUI::Key::U;
		break;

	case 86:
		cs = CEGUI::Key::V;
		break;

	case 87:
		cs = CEGUI::Key::W;
		break;

	case 88:
		cs = CEGUI::Key::X;
		break;

	case 89:
		cs = CEGUI::Key::Y;
		break;

	case 90:
		cs = CEGUI::Key::Z;
		break;

	case 96:
		cs = CEGUI::Key::Numpad0;
		break;

	case 97:
		cs = CEGUI::Key::Numpad1;
		break;

	case 98:
		cs = CEGUI::Key::Numpad2;
		break;

	case 99:
		cs = CEGUI::Key::Numpad3;
		break;

	case 100:
		cs = CEGUI::Key::Numpad4;
		break;

	case 101:
		cs = CEGUI::Key::Numpad5;
		break;

	case 102:
		cs = CEGUI::Key::Numpad6;
		break;

	case 103:
		cs = CEGUI::Key::Numpad7;
		break;

	case 104:
		cs = CEGUI::Key::Numpad8;
		break;

	case 105:
		cs = CEGUI::Key::Numpad9;
		break;

	case 106:
		cs = CEGUI::Key::Multiply;
		break;

	case 107:
		cs = CEGUI::Key::Add;
		break;

	case 109:
		cs = CEGUI::Key::Subtract;
		break;

	case 110:
		cs = CEGUI::Key::Decimal;
		break;

	case 111:
		cs = CEGUI::Key::Divide;
		break;

	case 112:
		cs = CEGUI::Key::F1;
		break;

	case 113:
		cs = CEGUI::Key::F2;
		break;

	case 114:
		cs = CEGUI::Key::F3;
		break;

	case 115:
		cs = CEGUI::Key::F4;
		break;

	case 116:
		cs = CEGUI::Key::F5;
		break;

	case 117:
		cs = CEGUI::Key::F6;
		break;

	case 118:
		cs = CEGUI::Key::F7;
		break;

	case 119:
		cs = CEGUI::Key::F8;
		break;
			
	case 120:
		cs = CEGUI::Key::F9;
		break;

	case 121:
		cs = CEGUI::Key::F10;
		break;

	case 122:
		cs = CEGUI::Key::F11;
		break;

	case 123:
		cs = CEGUI::Key::F12;
		break;

	case 192:
		cs = CEGUI::Key::Grave;
		break;

	case 189:
		cs = CEGUI::Key::Minus;
		break;

	case 187:
		cs = CEGUI::Key::Equals;
		break;

	case 219:
		cs = CEGUI::Key::LeftBracket;
		break;

	case 221:
		cs = CEGUI::Key::RightBracket;
		break;

	case 226:
		cs = CEGUI::Key::Backslash;
		break;

	case 186:
		cs = CEGUI::Key::Semicolon;
		break;

	case 222:
		cs = CEGUI::Key::Apostrophe;
		break;

	case 188:
		cs = CEGUI::Key::Comma;
		break;

	case 190:
		cs = CEGUI::Key::Period;
		break;

	case 191:
		cs = CEGUI::Key::Slash;
		break;

	case 160:
		cs = CEGUI::Key::LeftShift;
		break;

	case 161:
		cs = CEGUI::Key::RightShift;
		break;

	case 162:
		cs = CEGUI::Key::LeftControl;
		break;

	case 163:
		cs = CEGUI::Key::RightControl;
		break;

	case 164:
		cs = CEGUI::Key::LeftAlt;
		break;

	case 165:
		cs = CEGUI::Key::RightAlt;
		break;

	case 91:
		cs = CEGUI::Key::LeftWindows;
		break;

	case 92:
		cs = CEGUI::Key::RightWindows;
		break;

	default:
           cs = 0;
		   break;

	}
	
	return cs;
}



EXPORTFUNC CEGUI::System* CEGUISYSTEM_create( CEGUI::OgreCEGUIRenderer* renderer,
								   CEGUI::ResourceProvider* resourceProvider,
								   CEGUI::XMLParser* xmlParser,
								   CEGUI::ScriptModule* scriptModule,
								   const char* configFile )
{
	CEGUI::System *system = new CEGUI::System(renderer);
	return system;

}

EXPORTFUNC void CEGUISYSTEM_delete( CEGUI::System* system )
{
	delete system;
}

//CEGUI->System->setDefaultMouseCursor
EXPORTFUNC void CEGUISYSTEM_setDefaultMouseCursor( const char* imageset , const char* image_name , CEGUI::System* system )
{
	system->setDefaultMouseCursor( imageset , image_name );
}

//CEGUI->System->setDefaultFont(...)
EXPORTFUNC void CEGUISYSTEM_setDefaultFont(const char* name , CEGUI::System* system)
{
	system->setDefaultFont((CEGUI::utf8*) name );
}

//CEGUI->System->setGUISheet(...)
EXPORTFUNC void CEGUISYSTEM_setGUISheet( CEGUI::Window* sheet , CEGUI::System* system )
{
	system->setGUISheet( sheet );
}

//CEGUI->System->getDefault
EXPORTFUNC const CEGUI::Image* CEGUISYSTEM_getDefaultMouseCursor( CEGUI::System* system )
{
	return system->getDefaultMouseCursor();
}

//CEGUI->System->getDefaultTooltip()
EXPORTFUNC CEGUI::Tooltip* CEGUISYSTEM_getDefaultTooltip( CEGUI::System* system )
{
	return system->getDefaultTooltip();
}

//CEGUI->System->injectMouseMove(...)
EXPORTFUNC void CEGUISYSTEM_injectMouseMove( float x , float y , CEGUI::System* system )
{
	system->injectMouseMove( x , y );
}

//CEGUI->System->injectMouseButtonDown(...)
EXPORTFUNC void CEGUISYSTEM_injectMouseButtonDown( int mouseButton , CEGUI::System* system )
{
	switch(mouseButton)
	{
	case 1:
		system->injectMouseButtonDown( CEGUI::LeftButton );
		return;
	case 2:
		system->injectMouseButtonDown( CEGUI::RightButton );
		return;
	case 3:
		system->injectMouseButtonDown( CEGUI::MiddleButton );
		return;
	default:
		return;
	}

}
//CEGUI->System->injectMouseButtonUp(...)
EXPORTFUNC void CEGUISYSTEM_injectMouseButtonUp( int mouseButton , CEGUI::System* system )
{
	switch(mouseButton)
	{
	case 1:
		system->injectMouseButtonUp( CEGUI::LeftButton );
		return;
	case 2:
		system->injectMouseButtonUp( CEGUI::RightButton );
		return;
	case 3:
		system->injectMouseButtonUp( CEGUI::MiddleButton );
		return;
	default:
		return;
	}
}

//
EXPORTFUNC int CEGUISYSTEM_injectTimePulse( float timeElapsed , CEGUI::System* system )
{
	return system->injectTimePulse( timeElapsed );
}

EXPORTFUNC void CEGUISYSTEM_renderGUI( CEGUI::System* s )
{
	s->renderGUI();
}

EXPORTFUNC int CEGUISYSTEM_injectKeyUp( int key_code , CEGUI::System* system )
{
	return system->injectKeyUp( MaxToASCII(key_code) );
}

EXPORTFUNC int CEGUISYSTEM_injectKeyDown( int key_code , CEGUI::System* system )
{
	return system->injectKeyDown( MaxToASCII( key_code ) );
}

EXPORTFUNC int CEGUISYSTEM_injectChar( int code_point , CEGUI::System* system )
{
	return system->injectChar( code_point );
}

EXPORTFUNC void CEGUISYSTEM_setDefaultTooltip( const char* tooltipType , CEGUI::System* system )
{
	system->setDefaultTooltip( tooltipType );
}


#endif
