#ifndef H_CEGUITOOLTIP
#define H_CEGUITOOLTIP

EXPORTFUNC CEGUI::Tooltip* CEGUITT_create( const char* type , const char* name )
{
	CEGUI::Tooltip* tooltip = new CEGUI::Tooltip( type , name );
	return tooltip;
}

EXPORTFUNC void CEGUITT_delete( CEGUI::Tooltip* tooltip )
{
	if(tooltip){
		delete tooltip;
	}
}

EXPORTFUNC void CEGUITT_setHoverTime( float hoverTime , CEGUI::Tooltip* tooltip )
{
	tooltip->setHoverTime( hoverTime );
}

EXPORTFUNC void CEGUITT_setDisplayTime( float displayTime , CEGUI::Tooltip* tooltip )
{
	tooltip->setDisplayTime( displayTime );
}

EXPORTFUNC void CEGUITT_setFadeTime( float fadeTime , CEGUI::Tooltip* tooltip )
{
	tooltip->setFadeTime( fadeTime );
}







#endif
