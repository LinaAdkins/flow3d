#ifndef H_CEGUIPOPUPMENU
#define H_CEGUIPOPUPMENU

EXPORTFUNC float CEGUIPOPUP_getFadeInTime( CEGUI::PopupMenu* PopupMenu )
{
	return PopupMenu->getFadeInTime();
}

EXPORTFUNC float CEGUIPOPUP_getFadeOutTime( CEGUI::PopupMenu* PopupMenu )
{
	return PopupMenu->getFadeOutTime();
}

EXPORTFUNC int CEGUIPOPUP_isPopupMenuOpen( CEGUI::PopupMenu* PopupMenu )
{
	return PopupMenu->isPopupMenuOpen();
}

EXPORTFUNC void CEGUIPOPUP_setFadeInTime( float fadetime , CEGUI::PopupMenu* PopupMenu )
{
	PopupMenu->setFadeInTime( fadetime );
}

EXPORTFUNC void CEGUIPOPUP_setFadeOutTime( float fadetime , CEGUI::PopupMenu* PopupMenu )
{
	PopupMenu->setFadeOutTime( fadetime );
}

EXPORTFUNC void CEGUIPOPUP_openPopupMenu( bool notify , CEGUI::PopupMenu* PopupMenu )
{
	PopupMenu->openPopupMenu( notify );
}

EXPORTFUNC void CEGUIPOPUP_closePopupMenu( bool notify , CEGUI::PopupMenu* PopupMenu )
{
	PopupMenu->closePopupMenu( notify );
}

#endif
