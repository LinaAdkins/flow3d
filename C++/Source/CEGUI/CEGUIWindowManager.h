#ifndef H_CEGUIWINDOWMANAGER
#define H_CEGUIWINDOWMANAGER

//CEGUI->WindowManager->createWindow(...)
EXPORTFUNC CEGUI::Window* CEGUIWM_createWindow( const char* type , const char* name )
{
	return CEGUI::WindowManager::getSingleton().createWindow( type , name );
}

//CEGUI->WindowManager->destroyWindow(...)
EXPORTFUNC void CEGUIWM_destroyWindow( CEGUI::Window* window )
{
	CEGUI::WindowManager::getSingleton().destroyWindow( window );
}
//CEGUI->WindowManager->createFrameWindow(...)
EXPORTFUNC CEGUI::FrameWindow* CEGUIWM_createFrameWindow( const char* type , const char* name )
{
	return (CEGUI::FrameWindow*)CEGUI::WindowManager::getSingleton().createWindow( type , name );

}
//CEGUI->WindowManager->loadWindowLayout(...)
EXPORTFUNC CEGUI::Window* CEGUIWM_loadWindowLayout( const char* layoutFile )
{
	return CEGUI::WindowManager::getSingleton().loadWindowLayout( layoutFile );
}

EXPORTFUNC CEGUI::Combobox* CEGUIWM_createCombobox( const char* type , const char* name )
{
	return (CEGUI::Combobox*) CEGUI::WindowManager::getSingleton().createWindow( type , name );
}

//Buttons
EXPORTFUNC CEGUI::RadioButton* CEGUIWM_createRadioButton( const char* type , const char* name )
{
	return (CEGUI::RadioButton*) CEGUI::WindowManager::getSingleton().createWindow( type , name );
}
EXPORTFUNC CEGUI::PushButton* CEGUIWM_createPushButton( const char* type , const char* name )
{
	return (CEGUI::PushButton*) CEGUI::WindowManager::getSingleton().createWindow( type , name );
}
EXPORTFUNC CEGUI::Checkbox* CEGUIWM_createCheckbox( const char* type  , const char* name )
{
	return (CEGUI::Checkbox*) CEGUI::WindowManager::getSingleton().createWindow( type , name );
}
EXPORTFUNC CEGUI::TabButton* CEGUIWM_createTabButton( const char* type , const char* name )
{
	return (CEGUI::TabButton*) CEGUI::WindowManager::getSingleton().createWindow( type , name );
}
//CEGUI::Slider*
EXPORTFUNC CEGUI::Slider* CEGUIWM_createSlider( const char* type , const char* name )
{
	return (CEGUI::Slider*) CEGUI::WindowManager::getSingleton().createWindow( type , name );
}
//CEGUI::Editbox*
EXPORTFUNC CEGUI::Editbox* CEGUIWM_createEditbox( const char* type , const char* name )
{
	return (CEGUI::Editbox* ) CEGUI::WindowManager::getSingleton().createWindow( type , name );
}

EXPORTFUNC CEGUI::Listbox* CEGUIWM_createListbox( const char* type , const char* name )
{
	return (CEGUI::Listbox*) CEGUI::WindowManager::getSingleton().createWindow( type , name );
}

EXPORTFUNC CEGUI::Spinner* CEGUIWM_createSpinner( const char* type , const char* name )
{
	return (CEGUI::Spinner*) CEGUI::WindowManager::getSingleton().createWindow( type , name );
}

EXPORTFUNC CEGUI::ItemListBase* CEGUIWM_createItemListBase( const char* type , const char* name )
{
	return (CEGUI::ItemListBase*) CEGUI::WindowManager::getSingleton().createWindow( type , name );
}

EXPORTFUNC CEGUI::MenuBase* CEGUIWM_createMenuBase( const char * type , const char* name )
{
	return (CEGUI::MenuBase*) CEGUI::WindowManager::getSingleton().createWindow( type , name );
}


EXPORTFUNC CEGUI::Menubar* CEGUIWM_createMenuBar( const char* type , const char* name )
{
	return (CEGUI::Menubar*) CEGUI::WindowManager::getSingleton().createWindow( type , name );
}

EXPORTFUNC CEGUI::PopupMenu* CEGUIWM_createPopupMenu( const char* type , const char* name )
{
	return (CEGUI::PopupMenu*) CEGUI::WindowManager::getSingleton().createWindow( type , name );
}

// Begin the different getWindow Functions. Changed: January 1st, 2008
// Set this way because casting max-side caused memory leak issues.

EXPORTFUNC CEGUI::Window* CEGUIWM_getWindow( const char* name )
{
	return CEGUI::WindowManager::getSingleton().getWindow( name );
}
	
EXPORTFUNC CEGUI::PushButton* CEGUIWM_getPushButton( const char* name )
{
	return static_cast<CEGUI::PushButton*>(CEGUI::WindowManager::getSingleton().getWindow( name ));
}

EXPORTFUNC CEGUI::PopupMenu* CEGUIWM_getPopupMenu( const char* name )
{
	return static_cast<CEGUI::PopupMenu*>(CEGUI::WindowManager::getSingleton().getWindow( name ));
}

EXPORTFUNC CEGUI::FrameWindow* CEGUIWM_getFrameWindow( const char* name )
{
	return static_cast<CEGUI::FrameWindow*>(CEGUI::WindowManager::getSingleton().getWindow( name ));
}

EXPORTFUNC CEGUI::Slider* CEGUIWM_getSlider( const char* name )
{
	return static_cast<CEGUI::Slider*>(CEGUI::WindowManager::getSingleton().getWindow( name ));
}

EXPORTFUNC CEGUI::Spinner* CEGUIWM_getSpinner( const char* name )
{
	return static_cast<CEGUI::Spinner*>(CEGUI::WindowManager::getSingleton().getWindow( name ));
}

EXPORTFUNC CEGUI::Combobox* CEGUIWM_getCombobox( const char* name )
{
	return static_cast<CEGUI::Combobox*>(CEGUI::WindowManager::getSingleton().getWindow( name ));
}

EXPORTFUNC CEGUI::RadioButton* CEGUIWM_getRadioButton( const char* name )
{
	return static_cast<CEGUI::RadioButton*>(CEGUI::WindowManager::getSingleton().getWindow( name ));
}

EXPORTFUNC CEGUI::Checkbox* CEGUIWM_getCheckbox( const char* name )
{
	return static_cast<CEGUI::Checkbox*>(CEGUI::WindowManager::getSingleton().getWindow( name ));
}

EXPORTFUNC CEGUI::Editbox* CEGUIWM_getEditbox( const char* name )
{
	return static_cast<CEGUI::Editbox*>(CEGUI::WindowManager::getSingleton().getWindow( name ));
}

EXPORTFUNC CEGUI::ItemListbox* CEGUIWM_getItemListbox( const char* name )
{
	return static_cast<CEGUI::ItemListbox*>(CEGUI::WindowManager::getSingleton().getWindow( name ));
}

EXPORTFUNC CEGUI::Listbox* CEGUIWM_getListbox( const char* name )
{
	return static_cast<CEGUI::Listbox*>(CEGUI::WindowManager::getSingleton().getWindow( name ));
}

EXPORTFUNC CEGUI::Menubar* CEGUIWM_getMenubar( const char* name )
{
	return static_cast<CEGUI::Menubar*>(CEGUI::WindowManager::getSingleton().getWindow( name ));
}

EXPORTFUNC CEGUI::MenuItem* CEGUIWM_getMenuItem( const char* name )
{
	return static_cast<CEGUI::MenuItem*>(CEGUI::WindowManager::getSingleton().getWindow( name ));
}

EXPORTFUNC CEGUI::TabControl* CEGUIWM_getTabControl( const char* name )
{
	return static_cast<CEGUI::TabControl*>(CEGUI::WindowManager::getSingleton().getWindow( name ) );
}


#endif
