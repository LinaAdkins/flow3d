#ifndef H_CEGUILISTBOXTEXTITEM
#define H_CEGUILISTBOXTEXTITEM

EXPORTFUNC CEGUI::ListboxTextItem* CEGUILBTI_create( const char* text )
{
	return new CEGUI::ListboxTextItem( text );
}

#endif
