#ifndef H_CEGUIVECTOR2
#define H_CEGUIVECTOR2

EXPORTFUNC CEGUI::Vector2* CEGUIVECTOR2_create( float d_x , float d_y )
{
	return new CEGUI::Vector2( d_x , d_y );
}

EXPORTFUNC void CEGUIVECTOR2_delete( CEGUI::Vector2* v )
{
	delete v;
}

EXPORTFUNC float CEGUIVECTOR2_getX( CEGUI::Vector2* v )
{
	return v->d_x;
}

EXPORTFUNC void CEGUIVECTOR2_setX(float d_x , CEGUI::Vector2* v )
{
	v->d_x = d_x;
}

EXPORTFUNC float CEGUIVECTOR2_getY( CEGUI::Vector2* v )
{
	return v->d_y;
}

EXPORTFUNC void CEGUIVECTOR2_setY( float d_y , CEGUI::Vector2* v )
{
	v->d_y = d_y;
}

EXPORTFUNC void CEGUIVECTOR2_setAll( float d_x , float d_y , CEGUI::Vector2* v )
{
	v->d_x = d_x;
	v->d_y = d_y;
}



#endif
