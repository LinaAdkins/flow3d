#ifndef H_CEGUIITEMLISTBASE
#define H_CEGUIITEMLISTBASE

EXPORTFUNC int CEGUIILB_getItemCount( CEGUI::ItemListBase* ItemListBase )
{
	return (int)ItemListBase->getItemCount();
}

EXPORTFUNC CEGUI::ItemEntry* CEGUIILB_getItemFromIndex( int index , CEGUI::ItemListBase* ItemListBase )
{
	return ItemListBase->getItemFromIndex( index );
}

EXPORTFUNC int CEGUIILB_getItemIndex( CEGUI::ItemEntry* item , CEGUI::ItemListBase* ItemListBase )
{
	return (int)ItemListBase->getItemIndex( item );
}

EXPORTFUNC CEGUI::ItemEntry* CEGUIILB_findItemWithText( const char* text , CEGUI::ItemEntry* start_item , CEGUI::ItemListBase* ItemListBase )
{
	return ItemListBase->findItemWithText( text , start_item );
}

EXPORTFUNC int CEGUIILB_isItemInList( CEGUI::ItemEntry* item , CEGUI::ItemListBase* ItemListBase )
{
	return ItemListBase->isItemInList( item );
}

EXPORTFUNC int CEGUIILB_isAutoResizeEnabled( CEGUI::ItemListBase* ItemListBase )
{
	return ItemListBase->isAutoResizeEnabled();
}

EXPORTFUNC int CEGUIILB_isSortEnabled( CEGUI::ItemListBase* ItemListBase )
{
	return ItemListBase->isSortEnabled();
}

EXPORTFUNC int CEGUIILB_getSortMode( CEGUI::ItemListBase* ItemListBase )
{
	return ItemListBase->getSortMode();
}

EXPORTFUNC CEGUI::ItemListBase::SortCallback CEGUIILB_getSortCallback( CEGUI::ItemListBase* ItemListBase )
{
	return ItemListBase->getSortCallback();
}

EXPORTFUNC void CEGUIILB_initialiseComponents( CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->initialiseComponents();
}

EXPORTFUNC void CEGUIILB_resetList( CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->resetList();
}

EXPORTFUNC void CEGUIILB_addItem( CEGUI::ItemEntry* item , CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->addItem( item );
}

EXPORTFUNC void CEGUIILB_insertItem( CEGUI::ItemEntry* item , CEGUI::ItemEntry* position , CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->insertItem( item  , position );
}

EXPORTFUNC void CEGUIILB_removeItem( CEGUI::ItemEntry* item , CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->removeItem( item );
}

EXPORTFUNC void CEGUIILB_handleUpdatedItemData( bool resort , CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->handleUpdatedItemData( resort );
}

EXPORTFUNC void CEGUIILB_setAutoResizeEnabled( bool setting , CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->setAutoResizeEnabled( setting );
}

EXPORTFUNC void CEGUIILB_sizeToContent( CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->sizeToContent();
}

EXPORTFUNC void CEGUIILB_endInitialisation( CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->endInitialisation();
}

EXPORTFUNC void CEGUIILB_performChildWindowLayout( CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->performChildWindowLayout();
}

EXPORTFUNC CEGUI::Rect* CEGUIILB_getItemRenderArea( CEGUI::ItemListBase* ItemListBase )
{
	CEGUI::Rect* ceguiRect = new CEGUI::Rect( ItemListBase->getItemRenderArea() );
	return ceguiRect;
}

EXPORTFUNC CEGUI::Window* CEGUIILB_getContentPane( CEGUI::ItemListBase* ItemListBase )
{
	return ItemListBase->getContentPane();
}

EXPORTFUNC void CEGUIILB_notifyItemClicked( CEGUI::ItemEntry* li , CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->notifyItemClicked( li );
}

EXPORTFUNC void CEGUIILB_notifyItemSelectState( CEGUI::ItemEntry* li , bool state , CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->notifyItemSelectState( li , state );
}

EXPORTFUNC void CEGUIILB_setSortEnabled( bool setting , CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->setSortEnabled( setting );
}

EXPORTFUNC void CEGUIILB_setSortMode( int mode , CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->setSortMode( (CEGUI::ItemListBase::SortMode) mode );
}

EXPORTFUNC void CEGUIILB_setSortCallback( CEGUI::ItemListBase::SortCallback cb , CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->setSortCallback( cb );
}

EXPORTFUNC void CEGUIILB_sortList( bool relayout , CEGUI::ItemListBase* ItemListBase )
{
	ItemListBase->sortList( relayout );
}










#endif
