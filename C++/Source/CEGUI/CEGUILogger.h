#ifndef H_CEGUILOGGER
#define H_CEGUILOGGER

EXPORTFUNC void CEGUILOGGER_setLoggingLevel( CEGUI::LoggingLevel level )
{
	CEGUI::Logger::getSingleton().setLoggingLevel( level );
}

EXPORTFUNC int CEGUILOGGER_getLoggingLevel()
{
	return CEGUI::Logger::getSingleton().getLoggingLevel();
}

EXPORTFUNC void CEGUILOGGER_logEvent( const char* message , CEGUI::LoggingLevel level )
{
	CEGUI::Logger::getSingleton().logEvent( message , level );
}

EXPORTFUNC void CEGUILOGGER_setLogFileName( const char* filename , bool append )
{
	CEGUI::Logger::getSingleton().setLogFilename( filename , append );
}


#endif
