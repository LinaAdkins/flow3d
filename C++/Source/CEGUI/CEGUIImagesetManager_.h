#ifndef H_CEGUIIMAGESETMANAGER_
#define H_CEGUIIMAGESETMANAGER_

EXPORTFUNC CEGUI::Imageset* CEGUIISM_createImageset( const char* name )
{
	return CEGUI::ImagesetManager::getSingleton().createImageset( name );
}

#endif
