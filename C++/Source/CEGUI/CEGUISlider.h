#ifndef H_CEGUISLIDER
#define H_CEGUISLIDER

EXPORTFUNC float CEGUISLIDER_getCurrentValue( CEGUI::Slider* slider )
{
	return slider->getCurrentValue();
}

EXPORTFUNC float CEGUISLIDER_getMaxValue( CEGUI::Slider* slider )
{
	return slider->getMaxValue();
}

EXPORTFUNC float CEGUISLIDER_getClickStep( CEGUI::Slider* slider )
{
	return slider->getClickStep();
}
EXPORTFUNC void CEGUISLIDER_setMaxValue( float maxVal , CEGUI::Slider* slider )
{
	slider->setMaxValue( maxVal );
}
EXPORTFUNC void CEGUISLIDER_setCurrentValue( float value , CEGUI::Slider* slider )
{
	slider->setCurrentValue( value );
}
EXPORTFUNC void CEGUISLIDER_setClickStep( float step , CEGUI::Slider* slider )
{
	slider->setClickStep( step );
}

#endif
