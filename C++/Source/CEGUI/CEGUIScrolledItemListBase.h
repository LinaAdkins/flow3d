#ifndef H_CEGUISCROLLEDITEMLISTBASE
#define H_CEGUISCROLLEDITEMLISTBASE

EXPORTFUNC int CEGUISILB_isVertScrollbarAlwaysShown( CEGUI::ScrolledItemListBase* silb )
{
	return silb->isVertScrollbarAlwaysShown();
}

EXPORTFUNC int CEGUISILB_isHorzScrollbarAlwaysShown( CEGUI::ScrolledItemListBase* silb )
{
	return silb->isHorzScrollbarAlwaysShown();
}

EXPORTFUNC CEGUI::Scrollbar* CEGUISILB_getVertScrollbar( CEGUI::ScrolledItemListBase* silb )
{
	return silb->getVertScrollbar();
}

EXPORTFUNC CEGUI::Scrollbar* CEGUISILB_getHorzScrollbar( CEGUI::ScrolledItemListBase* silb )
{
	return silb->getHorzScrollbar();
}

EXPORTFUNC void CEGUISILB_setShowVertScrollbar( bool mode , CEGUI::ScrolledItemListBase* silb )
{
	silb->setShowVertScrollbar( mode );
}

EXPORTFUNC void CEGUISILB_setShowHorzScrollbar( bool mode , CEGUI::ScrolledItemListBase* silb )
{
	silb->setShowHorzScrollbar( mode );
}





#endif
