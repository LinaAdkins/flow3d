#ifndef H_CEGUISCHEMEMANAGER
#define H_CEGUISCHEMEMANAGER

EXPORTFUNC void CEGUISM_loadScheme( const char* scheme )
{
	CEGUI::SchemeManager::getSingleton().loadScheme((CEGUI::utf8*) scheme );
}

#endif
