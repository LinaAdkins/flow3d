#ifndef H_CEGUIUVECTOR2
#define H_CEGUIUVECTOR2

EXPORTFUNC CEGUI::UVector2* CEGUIUVECTOR2_create( CEGUI::UDim* d_x , CEGUI::UDim* d_y )
{
	return new CEGUI::UVector2( *d_x , *d_y );
}

EXPORTFUNC void CEGUIUVECTOR2_delete( CEGUI::UVector2* v )
{
	delete v;
}

EXPORTFUNC CEGUI::UDim* CEGUIUVECTOR2_getX( CEGUI::UVector2* v )
{
	return &v->d_x;
}

EXPORTFUNC void CEGUIUVECTOR2_setX( CEGUI::UDim* d_x , CEGUI::UVector2* v )
{
	v->d_x = *d_x;
}

EXPORTFUNC CEGUI::UDim* CEGUIUVECTOR2_getY( CEGUI::UVector2* v )
{
	return &v->d_y;
}

EXPORTFUNC void CEGUIUVECTOR2_setY( CEGUI::UDim* d_y , CEGUI::UVector2* v )
{
	v->d_y = *d_y;
}

EXPORTFUNC void CEGUIUVECTOR2_setAll( CEGUI::UDim* d_x , CEGUI::UDim* d_y , CEGUI::UVector2* v )
{
	v->d_x = *d_x;
	v->d_y = *d_y;
}

#endif
