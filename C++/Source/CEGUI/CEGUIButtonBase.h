#ifndef H_CEGUIBUTTONBASE
#define H_CEGUIBUTTONBASE

#include "CEGUICheckbox.h"
#include "CEGUIPushButton.h"
#include "CEGUIRadioButton.h"
#include "CEGUITabButton.h"

EXPORTFUNC int CEGUIBB_isHovering( CEGUI::ButtonBase* button )
{
	return button->isHovering();
}

EXPORTFUNC int CEGUIBB_isPushed( CEGUI::ButtonBase* button )
{
	return button->isPushed();
}



#endif
