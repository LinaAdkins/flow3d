#ifndef H_CEGUILISTBOX
#define H_CEGUILISTBOX

EXPORTFUNC CEGUI::ListboxItem* CEGUILB_getFirstSelectedItem( CEGUI::Listbox* b )
{
	return b->getFirstSelectedItem();
}

EXPORTFUNC void CEGUILB_resetList( CEGUI::Listbox* lb )
{
	lb->resetList();
}

EXPORTFUNC void CEGUILB_addItem( CEGUI::ListboxItem* item , CEGUI::Listbox* lb )
{
	lb->addItem( item );
}

EXPORTFUNC void CEGUILB_removeItem( CEGUI::ListboxItem* item , CEGUI::Listbox* lb )
{
	lb->removeItem( item );
}

#endif
