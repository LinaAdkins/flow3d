#ifndef H_TTABCONTROL
#define H_TTABCONTROL

EXPORTFUNC void CEGUITABCONTROL_setTabHeight( CEGUI::UDim* height , CEGUI::TabControl* c )
{
	c->setTabHeight( *height );
}

#endif
