#ifndef H_CEGUILISTBOXITEM
#define H_CEGUILISTBOXITEM

//CEGUI::ListboxItem* CEGUILBI_create( const char* text ) - Can't create due to abstract methods - not intended functionality

EXPORTFUNC const char* CEGUILBI_getText( CEGUI::ListboxItem* i )
{
	tempString = i->getText().c_str();
	return tempString.c_str();
}

EXPORTFUNC void CEGUILBI_setText( const char* text , CEGUI::ListboxItem* i )
{
	i->setText( text );
}

#endif
