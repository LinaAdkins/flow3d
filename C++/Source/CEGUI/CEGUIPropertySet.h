#ifndef H_CEGUIPROPERTYSET
#define H_CEGUIPROPERTYSET

EXPORTFUNC void CEGUIPROPERTYSET_setProperty( const char* name , const char* value , CEGUI::PropertySet* ps )
{
	ps->setProperty( name , value );
}

#endif
