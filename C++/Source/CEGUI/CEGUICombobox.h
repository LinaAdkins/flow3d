#ifndef H_CEGUICOMBOBOX
#define H_CEGUICOMBOBOX

EXPORTFUNC void CEGUICOMBO_resetList( CEGUI::Combobox* combobox )
{
	combobox->resetList();
}

EXPORTFUNC int CEGUICOMBO_getItemCount( CEGUI::Combobox* c )
{
	return c->getItemCount();
}

EXPORTFUNC CEGUI::ListboxItem* CEGUICOMBO_getSelectedItem( CEGUI::Combobox* combobox )
{
	return combobox->getSelectedItem();
}

EXPORTFUNC CEGUI::ListboxItem* CEGUICOMBO_getListboxItemFromIndex( int index , CEGUI::Combobox* c )
{
	return c->getListboxItemFromIndex( index );
}

EXPORTFUNC int CEGUICOMBO_getItemIndex( CEGUI::ListboxItem* item , CEGUI::Combobox* c )
{
	return c->getItemIndex( item );
}

EXPORTFUNC void CEGUICOMBO_addItem( CEGUI::ListboxItem* item , CEGUI::Combobox* combobox )
{
	combobox->addItem( item );
}

EXPORTFUNC void CEGUICOMBO_setItemSelectState( CEGUI::ListboxItem* item , bool state , CEGUI::Combobox* combobox )
{
	combobox->setItemSelectState( item , state );
}


EXPORTFUNC CEGUI::ListboxItem* CEGUICOMBO_findItemWithText( const char* text , CEGUI::ListboxItem* start_item , CEGUI::Combobox* combobox )
{
	return combobox->findItemWithText( text , start_item );

}

// Helper function - Exists to allow users an easy way to set comboboxes-
EXPORTFUNC void CEGUICOMBO_setSelectedItem( CEGUI::ListboxItem* item , CEGUI::Combobox* combobox )
{
	combobox->clearAllSelections();
	combobox->setItemSelectState( item , true );
	combobox->setText( item->getText() );
}

#endif
