#ifndef H_CEGUIFONTMANAGER
#define H_CEGUIFONTMANAGER

EXPORTFUNC void CEGUIFONTMANAGER_createFont( const char* font )
{
	CEGUI::FontManager::getSingleton().createFont( font );
}

#endif
