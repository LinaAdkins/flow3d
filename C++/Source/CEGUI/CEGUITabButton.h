#ifndef H_CEGUITABBUTTON
#define H_CEGUITABBUTTON

EXPORTFUNC void CEGUITAB_setSelected( bool selected , CEGUI::TabButton* tabbutton )
{
	tabbutton->setSelected( selected );
}

EXPORTFUNC int CEGUITAB_isSelected( CEGUI::TabButton* tabbutton )
{
	return tabbutton->isSelected();
}

EXPORTFUNC void CEGUITAB_setTargetWindow( CEGUI::Window* window , CEGUI::TabButton* tabbutton )
{
	tabbutton->setTargetWindow( window );
}

EXPORTFUNC CEGUI::Window* CEGUITAB_getTargetWindow( CEGUI::TabButton* tabbutton )
{
	return tabbutton->getTargetWindow();
}

#endif
