#ifndef H_CEGUIUDIM
#define H_CEGUIUDIM

EXPORTFUNC CEGUI::UDim* CEGUIUDIM_create( float d_scale , float d_offset )
{
	return new CEGUI::UDim( d_scale , d_offset );
}

EXPORTFUNC void CEGUIUDIM_delete( CEGUI::UDim* u )
{
	delete u;
}

EXPORTFUNC float CEGUIUDIM_getScale( CEGUI::UDim* u )
{
	return u->d_scale;
}

EXPORTFUNC void CEGUIUDIM_setScale( float d_scale , CEGUI::UDim* u )
{
	u->d_scale = d_scale;
}

EXPORTFUNC float CEGUIUDIM_getOffset( CEGUI::UDim* u )
{
	return u->d_offset;
}

EXPORTFUNC void CEGUIUDIM_setOffset( float d_offset , CEGUI::UDim* u )
{
	u->d_offset = d_offset;
}

EXPORTFUNC void CEGUIUDIM_setAll( float d_scale , float d_offset , CEGUI::UDim* u )
{
	u->d_scale = d_scale;
	u->d_offset = d_offset;
}



#endif
