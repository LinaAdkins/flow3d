#ifndef H_CEGUIMOUSECURSOR
#define H_CEGUIMOUSECURSOR

EXPORTFUNC CEGUI::MouseCursor* CEGUIMC_create()
{
	CEGUI::MouseCursor* mouseCursor = new CEGUI::MouseCursor();
	return mouseCursor;
}

EXPORTFUNC void CEGUIMC_delete( CEGUI::MouseCursor* mc )
{
	if(mc)
	delete mc;
}

EXPORTFUNC void CEGUIMC_setImage( CEGUI::Image* image , CEGUI::MouseCursor *mc )
{
	mc->setImage( image );
}



EXPORTFUNC CEGUI::Point* CEGUIMC_getPosition()
{
	return new CEGUI::Point( CEGUI::MouseCursor::getSingleton().getPosition() );
}

EXPORTFUNC void CEGUIMC_show( CEGUI::MouseCursor* mc )
{
	mc->show();
}

EXPORTFUNC void CEGUIMC_hide( CEGUI::MouseCursor* mc )
{
	mc->hide();
}

EXPORTFUNC void CEGUIMC_setVisible( bool visible , CEGUI::MouseCursor* mc )
{
	mc->setVisible( visible );
}

EXPORTFUNC bool CEGUIMC_isVisible( CEGUI::MouseCursor* mc )
{
	return mc->isVisible();
}

EXPORTFUNC CEGUI::MouseCursor* CEGUIMC_getSingleton()
{
	return CEGUI::MouseCursor::getSingletonPtr();
}

#endif
