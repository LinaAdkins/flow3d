#ifndef H_CEGUIRENDERER
#define H_CEGUIRENDERER

EXPORTFUNC CEGUI::OgreCEGUIRenderer* CEGUIRENDERER_create( Ogre::RenderWindow* window ,
											    Ogre::uint8 queue_id,
												bool post_queue,
												CEGUI::uint max_quads,
												Ogre::SceneManager* ogreSceneManager )
{
	CEGUI::OgreCEGUIRenderer* renderer = new CEGUI::OgreCEGUIRenderer(window , queue_id , post_queue , max_quads , ogreSceneManager );
	return renderer;
}


EXPORTFUNC void CEGUIRENDERER_delete( CEGUI::OgreCEGUIRenderer* renderer )
{
	delete renderer;
}

EXPORTFUNC void CEGUIRENDERER_setTargetSceneManager( Ogre::SceneManager* ogreSceneManager , CEGUI::OgreCEGUIRenderer* renderer )
{
	renderer->setTargetSceneManager( ogreSceneManager );
}





#endif
