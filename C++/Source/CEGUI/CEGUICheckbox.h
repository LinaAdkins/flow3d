#ifndef H_CEGUICHECKBOX
#define H_CEGUICHECKBOX

EXPORTFUNC int CEGUICHECKBOX_isSelected( CEGUI::Checkbox* checkbox )
{
	return checkbox->isSelected();
}
EXPORTFUNC void CEGUICHECKBOX_setSelected( bool select , CEGUI::Checkbox* checkbox )
{
	checkbox->setSelected( select );
}

#endif
