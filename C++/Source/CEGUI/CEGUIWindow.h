#ifndef H_CEGUIWINDOW
#define H_CEGUIWINDOW

EXPORTFUNC const char* CEGUIWINDOW_getType( CEGUI::Window* w )
{
	tempString = w->getType().c_str();
	return tempString.c_str();
}

EXPORTFUNC const char* CEGUIWINDOW_getName( CEGUI::Window* w )
{
	tempString = w->getName().c_str();
	return tempString.c_str();
}

EXPORTFUNC bool CEGUIWINDOW_isActive( CEGUI::Window* w )
{
	return w->isActive();
}

EXPORTFUNC void CEGUIWINDOW_addChildWindow( CEGUI::Window* window , CEGUI::Window* parentWindow )
{
	parentWindow->addChildWindow( window );
}

EXPORTFUNC void CEGUIWINDOW_setPosition( CEGUI::UVector2* pos  , CEGUI::Window* w)
{
	w->setPosition( *pos );
}

EXPORTFUNC CEGUI::UVector2* CEGUIWINDOW_getPosition( CEGUI::Window* w )
{
	 return new CEGUI::UVector2( w->getPosition() );
}
EXPORTFUNC void CEGUIWINDOW_setSize( CEGUI::UDim* width , CEGUI::UDim* height , CEGUI::Window* window )
{
	window->setSize( CEGUI::UVector2( *width , *height ) );	
}

EXPORTFUNC void CEGUIWINDOW_setText( const char* text , CEGUI::Window* window )
{
	window->setText( text );
}

EXPORTFUNC void CEGUIWINDOW_setVisible( bool visible , CEGUI::Window* window )
{
	window->setVisible( visible );
}

EXPORTFUNC CEGUI::Window* CEGUIWINDOW_getActiveChild( CEGUI::Window* w )
{
	return w->getActiveChild();
}

EXPORTFUNC void CEGUIWINDOW_setAlpha( float alpha , CEGUI::Window* window )
{
	window->setAlpha( alpha );
}
EXPORTFUNC const char* CEGUIWINDOW_getText( CEGUI::Window* window )
{
	return window->getText().c_str();
}

EXPORTFUNC bool CEGUIWINDOW_isCapturedByThis( CEGUI::Window* w )
{
	return w->isCapturedByThis();
}

EXPORTFUNC bool CEGUIWINDOW_isCapturedByAncestor( CEGUI::Window* w )
{
	return w->isCapturedByAncestor();
}

EXPORTFUNC bool CEGUIWINDOW_isCapturedByChild( CEGUI::Window* w )
{
	return w->isCapturedByChild();
}

EXPORTFUNC void CEGUIWINDOW_setEnabled( bool enabled , CEGUI::Window* window )
{
	window->setEnabled( enabled );
}
EXPORTFUNC void CEGUIWINDOW_setTooltipText( const char* tooltipText , CEGUI::Window* window )
{
	window->setTooltipText( tooltipText );
}
//CEGUI->System->setTooltip
EXPORTFUNC void CEGUIWINDOW_setTooltip( CEGUI::Tooltip* tooltip , CEGUI::Window* window )
{
	window->setTooltip( tooltip );
}
//CEGUI->Window->getRect
//CEGUI->Window->isHit
EXPORTFUNC int CEGUIWINDOW_isHit( CEGUI::Vector2* position , CEGUI::Window* window )
{
	return window->isHit( *position );
}

EXPORTFUNC int CEGUIWINDOW_isVisible( int localOnly , CEGUI::Window* w )
{
	return w->isVisible();
}

EXPORTFUNC void CEGUIWINDOW_show( CEGUI::Window* w )
{
	w->show();
}

EXPORTFUNC void CEGUIWINDOW_hide( CEGUI::Window* w )
{
	w->hide();
}

EXPORTFUNC void CEGUIWINDOW_activate( CEGUI::Window* w )
{
	w->activate();
}

EXPORTFUNC void CEGUIWINDOW_deactivate( CEGUI::Window* w )
{
	w->deactivate();
}


// *Avaialable helper functions for disabling and hiding windows easilly-
EXPORTFUNC void CEGUIWINDOW_toggleAvailable( CEGUI::Window* w )
{
	if(w->isVisible())
	{
		w->setVisible( false );
		w->setEnabled( false );
		return;
	}
	else
	{
		w->setVisible( true );
		w->setEnabled( false );
		return;
	}
}

EXPORTFUNC void CEGUIWINDOW_setAvailable( bool available , CEGUI::Window* w )
{
	w->setVisible( available );
	w->setEnabled( available );
}

EXPORTFUNC int CEGUIWINDOW_getAvailable( CEGUI::Window* w )
{
	if( w->isVisible() == true && w->isDisabled() == false )
	{
		return true;
	}
	else
	{
		return false;
	}
}







#endif
