// CustomRayCastCar.cpp: implementation of the CustomRayCastCar class.
//
//////////////////////////////////////////////////////////////////////
#include "CustomJointLibraryStdAfx.h"
#include "CustomRayCastCar.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


#if 0
#include "dxstdafx.h"
#include "s_raycastvehicle.h"



void S_RaycastVehicle::CreateVehicle(IDirect3DDevice9* pd3dDevice , NewtonWorld* nWorld ,  float x, float y , float z , float mass , ID3DXMesh* carmesh)  
{          
	lapCount = 0 ; 

	//It's used mainly for cloning, must move it to S_Types.h as well
	struct CarVertex
	{
		D3DXVECTOR3 p ;
		D3DXVECTOR3 n ;
		FLOAT tu, tv ;
	} ;

#define CLONE_FVF (D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1)

	world = nWorld ;
	globalDampCoef = 1.0 ;
	forceForward = 16.0f ;  //8
	carAI= false ;
	bleft = false ;
	bright = false ;
	bforward = false ;
	bbackward = false ;
	ang = 0.0 ;
	vehicleMass = mass ;  
	COM_offset = dVector(0.0 , +0.1 , 0.0 ); //Original COM_offset(0.0 , +0.06 , 0.0 );
	ForceK = 20 ; //20
	wayindex = 0 ;
	NewtonCollision* collision; 
	if(carmesh == NULL)
	{

		collision = NewtonCreateBox (nWorld, x, y, z, NULL); 
		//create the rigid body
		vehicleBody = NewtonCreateBody (nWorld, collision);
		// set the force and torque call back function
		NewtonBodySetForceAndTorqueCallback (vehicleBody, PhysicsApplyForceAndTorqueRayCar000);

		NewtonBodySetCentreOfMass(vehicleBody , &COM_offset.m_x) ;
		// set the mass matrix

		float Ixx,Iyy,Izz ;
		Ixx =  0.3 * mass * (y * y + z * z) / 12.0f;
		Iyy =  0.3 * mass * (x * x + z * z) / 12.0f;
		Izz =  0.3 * mass * (x * x + y * y) / 12.0f;
		NewtonBodySetMassMatrix (vehicleBody, mass, Ixx , Iyy  , Izz );
		NewtonBodySetAutoFreeze (vehicleBody, 0 );

	} 

	else
	{


		ID3DXMesh* arbMesh = NULL;
		HRESULT hr = carmesh->CloneMeshFVF(0 , CLONE_FVF , pd3dDevice , &arbMesh ) ;

		if(FAILED(hr))
		{
			MessageBoxA(0,"Clone mesh failed", 0,0); 

		}
		//DWORD dwNumVertices = Form1->testMesh->GetLocalMesh()->GetNumVertices();
		DWORD dwNumVertices = arbMesh->GetNumVertices();

		float *vert = new float [dwNumVertices * 3] ;
		D3DXVECTOR3 vertices[3] ;

		WORD*      pIndices;
		CarVertex*    pVertices;

		arbMesh->LockVertexBuffer(D3DLOCK_READONLY , (VOID**)&pVertices) ;
		arbMesh->LockIndexBuffer(D3DLOCK_READONLY , (VOID**)&pIndices) ;


		for( DWORD i=0; i < dwNumVertices ; i++ )
		{
			vertices[0] = pVertices[pIndices[3*i+0]].p;
			vertices[1] = pVertices[pIndices[3*i+1]].p;
			vertices[2] = pVertices[pIndices[3*i+2]].p;

			vert[i*3+0 ] = vertices[0].x  * 2.5 ;
			vert[i*3+1 ] = vertices[0].y  * 2.5;
			vert[i*3+2 ] = vertices[0].z  *2.5 ;

		}



		//Don't forget to unlock the buffers
		arbMesh->UnlockVertexBuffer() ;
		arbMesh->UnlockIndexBuffer() ;
		//Release the mesh
		arbMesh->Release() ;
		collision = NewtonCreateConvexHull(nWorld ,
			dwNumVertices ,
			vert , //vertex cloud
			sizeof(float) * 3 ,
			NULL);
		vehicleBody = NewtonCreateBody(nWorld, collision);
		NewtonBodySetForceAndTorqueCallback (vehicleBody, PhysicsApplyForceAndTorqueRayCar000);
		NewtonBodySetCentreOfMass(vehicleBody , &COM_offset.m_x) ;
		float Ixx,Iyy,Izz ;
		Ixx =  0.3 * mass * (y * y + z * z) / 12.0f;
		Iyy =  0.3 * mass * (x * x + z * z) / 12.0f;
		Izz =  0.3 * mass * (x * x + y * y) / 12.0f;
		dVector inertia ;
		//NewtonConvexCollisionCalculateInertialMatrix(collision ,&inertia[0] ,&COM_offset[0]) ;

		NewtonBodySetMassMatrix (vehicleBody, mass, Ixx , Iyy  , Izz );
		NewtonBodySetAutoFreeze (vehicleBody, 0 );

	}



	// release the collision geometry when not need it
	NewtonReleaseCollision (nWorld, collision);
	NewtonBodySetContinuousCollisionMode (vehicleBody , 1) ;
	NewtonBodySetUserData(vehicleBody , this) ;

	CarJoint = NewtonConstraintCreateUserJoint(nWorld , 6 ,PhysicsSubmitCarConstraints , vehicleBody ,NULL) ;
	NewtonJointSetUserData(CarJoint, this); 

	dMatrix m = GetIdentityMatrix(); 
	//m.m_posit.m_y -= 30 ;
	//m.m_posit.m_x -= 20 ;

	NewtonBodySetMatrix(vehicleBody, &m[0][0]);

#undef CLONE_FVF

}

D3DXVECTOR3 GetRelativeVector(D3DXMATRIX mat , D3DXVECTOR3 vRel )
{
	D3DXVECTOR3 res ;
	D3DXVec3TransformCoord(&res ,&vRel , &mat) ;
	return res ;
}



void _cdecl S_RaycastVehicle::PhysicsApplyForceAndTorqueRayCar000 (const NewtonBody* body)
{
	S_RaycastVehicle* vehi = (S_RaycastVehicle*)NewtonBodyGetUserData(body) ;
	vehi->PhysicsApplyForceAndTorqueRayCarHandler(body) ;
}

void S_RaycastVehicle::PhysicsApplyForceAndTorqueRayCarHandler (const NewtonBody* body)
{

	D3DXMATRIX mat ;
	NewtonBodyGetMatrix(body, &mat[0]); 
	memcpy(&CarMat[0][0] ,&mat._11, sizeof(D3DXMATRIX)); 
	memcpy(&matrix._11 ,&mat._11, sizeof(D3DXMATRIX)); 
	//gravity
	dFloat Ixx;
	dFloat Iyy;
	dFloat Izz;
	dFloat mass;

	NewtonBodyGetMassMatrix (body, &mass, &Ixx, &Iyy, &Izz);
	dVector grav_force ;



	float dist[4] ;

	bool hits[4] ;
	D3DXVECTOR3 pivotStart[4];
	D3DXVECTOR3 pivotEnd[4];

	D3DXVECTOR3 rel ;
	D3DXVECTOR3 normals[4] ;

	float kc = 1.5 ;

	// This should be outside with a config file
	pivotStart[0] = GetRelativeVector(mat,VECTOR3(5/kc, 0.0, -2.5/kc));
	pivotEnd[0] = GetRelativeVector(mat,VECTOR3(5/kc,  -4.0, -2.5/kc));

	pivotStart[1] = GetRelativeVector(mat,VECTOR3(5/kc, 0.0, 2.5/kc));
	pivotEnd[1] = GetRelativeVector(mat,VECTOR3(5/kc,  -4.0, 2.5/kc));

	pivotStart[2] = GetRelativeVector(mat,VECTOR3(-5/kc, 0.0  ,2.5/kc));
	pivotEnd[2] = GetRelativeVector(mat,VECTOR3(-5/kc,   -4.0 ,2.5/kc));

	pivotStart[3] = GetRelativeVector(mat,VECTOR3(-5/kc, 0.0, -2.5/kc));
	pivotEnd[3] = GetRelativeVector(mat,VECTOR3(-5/kc,  -4.0, -2.5/kc));


	int ID ;
	dist[0]= NewtonCollisionRayCast (newtonCollision0 , &pivotStart[0].x , &pivotEnd[0].x , &normals[0].x , &ID) ;
	dist[1]= NewtonCollisionRayCast (newtonCollision0 , &pivotStart[1].x , &pivotEnd[1].x , &normals[1].x , &ID) ;
	dist[2]= NewtonCollisionRayCast (newtonCollision0 , &pivotStart[2].x , &pivotEnd[2].x , &normals[2].x , &ID) ;
	dist[3]= NewtonCollisionRayCast (newtonCollision0 , &pivotStart[3].x , &pivotEnd[3].x , &normals[3].x , &ID) ;



	//NOTICE < 0.8 instead of 1.0
	for(int x=0 ; x < 4 ; ++x) {
		if(dist[x] > 1.0f) {

			dist[x] = 1.0f ;

			hits[x] = false ;
			vwheels[x].contact = false ;
		}
		else {

			hits[x] = true ;
			vwheels[x].contactPos =  pivotStart[x] + dist[x] *(pivotEnd[x]-pivotStart[x]) ;
			//fix 
			//vwheels[x].contactPos.y -= 0.5f; 
			vwheels[x].contactNormal = normals[x] ;
			vwheels[x].contact = true ; 
			vwheels[x].distance = dist[x] ;

		}
	}


	grav_force = dVector (0.0f, -mass * 100, 0.0f);

	NewtonBodyAddForce (body, &grav_force.m_x);
	NewtonBodySetMassMatrix (body, vehicleMass, Ixx , Iyy  , Izz );          


	vwheels[0].distance = dist[0] ;
	vwheels[1].distance = dist[1] ;
	vwheels[2].distance = dist[2] ;
	vwheels[3].distance = dist[3] ;

	D3DXVECTOR3 pivot[4] ;

	float diameter = 8.0 ;



	//FIX THE PIVOT START REDUCTION  (koeficient kc = 1.5 above )
	// This should be outside with a config file
	pivotStart[0] = GetRelativeVector(mat,VECTOR3(5, 0.0, -2.5));
	pivotEnd[0] = GetRelativeVector(mat,VECTOR3(5,  -5.0, -2.5));

	pivotStart[1] = GetRelativeVector(mat,VECTOR3(5, 0.0, 2.5));
	pivotEnd[1] = GetRelativeVector(mat,VECTOR3(5,  -5.0, 2.5));

	pivotStart[2] = GetRelativeVector(mat,VECTOR3(-5, 0.0  ,2.5));
	pivotEnd[2] = GetRelativeVector(mat,VECTOR3(-5,   -5.0 ,2.5));

	pivotStart[3] = GetRelativeVector(mat,VECTOR3(-5, 0.0, -2.5));
	pivotEnd[3] = GetRelativeVector(mat,VECTOR3(-5,  -5.0, -2.5));


	float kk = 1.0 ;
	pivot[0] = (pivotStart[0] - pivotEnd[0])/ (dist [0]  * kk );
	pivot[1] = (pivotStart[1] - pivotEnd[1])/ (dist [1]  * kk );
	pivot[2] = (pivotStart[2] - pivotEnd[2])/ (dist [2]  * kk );
	pivot[3] = (pivotStart[3] - pivotEnd[3])/ (dist [3]  * kk );


	float forceK = ForceK ;


	D3DXVECTOR3 f[4];

	float neutralSus = 0.7 ;
	float xx[4];
	float kx = 200 ;
	float fwheelRad = 0.7 ;
	xx[0] = dist[0] - neutralSus ;
	xx[1] = dist[1] - neutralSus ;
	xx[2] = dist[2] - neutralSus ;
	xx[3] = dist[3] - neutralSus ;

	VECTOR3 vSus = pivotStart[0] - pivotEnd[0] ;
	D3DXVec3Normalize(&vSus,&vSus);
	vSus *= 5.0 ;

	if(dist[0] < neutralSus )
	{
		f[0] = -vSus * xx[0] * kx;
	}
	else 
	{
		// f[0] = vSus * xx[0] * kx;
		f[0] = VECTOR3(0,0,0); 
	}

	if(dist[1] < neutralSus )
	{
		f[1] = -vSus * xx[1] * kx;
	}
	else
	{
		//f[1] = vSus * xx[1] * kx;
		f[1] = VECTOR3(0,0,0); 
	}


	if(dist[2] < neutralSus )
	{
		f[2] = -vSus * xx[2] * kx;
	}
	else
	{
		//f[2] = vSus * xx[2] * kx;
		f[2] = VECTOR3(0,0,0); 
	}

	if(dist[3] < neutralSus )
	{
		f[3] = -vSus * xx[3] * kx ;
	}
	else
	{
		//f[3] = vSus * xx[3] * kx ;
		f[3] = VECTOR3(0,0,0); 
	}




	float x = 20 ;
	D3DXVECTOR3 torques[4] ;
	D3DXVECTOR3 pos = VECTOR3(mat._41 , mat._42 , mat._43) ;

	D3DXVECTOR3 tmp[4] ;
	tmp[0] = pivotStart[0] ;
	tmp[1] =  pivotStart[1]  ;
	tmp[2] =  pivotStart[2]  ;
	tmp[3] =  pivotStart[3] ;

	float c = 1.0 ;      

	tmp[0] = GetRelativeVector(mat,VECTOR3(5.0 / c, 0 ,-2.0 / c ));


	tmp[1] = GetRelativeVector(mat,VECTOR3(5.0 / c, 0 ,2.0 / c  ));


	tmp[2] = GetRelativeVector(mat,VECTOR3(-5 / c , 0 ,2.0 / c ));


	tmp[3] = GetRelativeVector(mat,VECTOR3(-5 / c, 0 ,-2.0 / c ));


	//D3DXVec3Cross(&torques[0] , &tmp[0] , &f[0]) ;
	//D3DXVec3Cross(&torques[1] , &tmp[1] , &f[1]) ;
	//D3DXVec3Cross(&torques[2] , &tmp[2] , &f[2]) ;
	//D3DXVec3Cross(&torques[3] , &tmp[3] , &f[3]) ;
	vwheels[0].contact = false ;
	vwheels[1].contact = false ;
	vwheels[2].contact = false ;
	vwheels[3].contact = false ;




	dVector center(mat._41 , mat._42 , mat._43) ;
	center.m_x -= COM_offset.m_x ;
	center.m_y += COM_offset.m_y ;

	dVector r[4]; 
	VECTOR3 frontCent = GetRelativeVector(mat,VECTOR3(5,0,0));
	VECTOR3 backCent = GetRelativeVector(mat,VECTOR3(-5,0,0));

	FrontCenter = frontCent ;

	r[0] = dVector(tmp[0].x , tmp[0].y , tmp[0].z) - center ; 
	r[1] = dVector(tmp[1].x , tmp[1].y , tmp[1].z) - center ; 
	r[2] = dVector(tmp[2].x , tmp[2].y , tmp[2].z) - center ; 
	r[3] = dVector(tmp[3].x , tmp[3].y , tmp[3].z) - center ; 




	dVector omega ; 
	dVector lin_velo ;
	NewtonBodyGetOmega(body , &omega.m_x);
	NewtonBodyGetVelocity(body, &lin_velo.m_x) ;
	dVector contactVelos[4] ;

	lin_Velocity_length = sqrt(lin_velo.m_x*lin_velo.m_x + lin_velo.m_y*lin_velo.m_y + lin_velo.m_z*lin_velo.m_z);



	/*
	//HACK 
	if( (lin_velo.m_x < 0.1) && (lin_velo.m_y < 0.1) && (lin_velo.m_z < 0.1) )
	{
	lin_velo = globalCarMat.m_right.Scale (2.0)  ;

	}
	*/

	contactVelos[0] = lin_velo + (omega * r[0]) ;
	contactVelos[1] = lin_velo + (omega * r[1])  ;
	contactVelos[2] = lin_velo + (omega * r[2]) ;
	contactVelos[3] = lin_velo + (omega * r[3]) ;


	/*
	//TEST
	contactVelos[0] =  dVector(tmp[0].x , tmp[0].y , tmp[0].z)-previousContactPos[0]   ;
	contactVelos[1] = dVector(tmp[1].x , tmp[1].y , tmp[1].z)-previousContactPos[1]    ;
	contactVelos[2] = dVector(tmp[2].x , tmp[2].y , tmp[2].z) -previousContactPos[2]   ;
	contactVelos[3] = dVector(tmp[3].x , tmp[3].y , tmp[3].z)-previousContactPos[3]    ;

	previousContactPos[0] = dVector(tmp[0].x , tmp[0].y , tmp[0].z) ;
	previousContactPos[1] = dVector(tmp[1].x , tmp[1].y , tmp[1].z) ;
	previousContactPos[2] = dVector(tmp[2].x , tmp[2].y , tmp[2].z) ;
	previousContactPos[3] = dVector(tmp[3].x , tmp[3].y , tmp[3].z) ;
	*/


	D3DXVECTOR3 lateral_velos[4] ;
	D3DXVECTOR3 binormals[4] ;

	binormals[0] = GetRelativeVector(mat,VECTOR3(5,0,-6));

	binormals[1] = GetRelativeVector(mat,VECTOR3(5,0,6));

	binormals[2] = GetRelativeVector(mat,VECTOR3(-5,0,6));

	binormals[3] = GetRelativeVector(mat,VECTOR3(-5,0,-6));

	binormals[0] = binormals[0] - tmp[0] ;
	binormals[1] = binormals[1] - tmp[1] ;
	binormals[2] = binormals[2] - tmp[2] ;
	binormals[3] = binormals[3] - tmp[3] ;




	D3DXVECTOR3 contVel[4]; 
	memcpy(&contVel[0].x ,  &contactVelos[0].m_x , sizeof(D3DXVECTOR3) ) ;
	memcpy(&contVel[1].x ,  &contactVelos[1].m_x , sizeof(D3DXVECTOR3) ) ;
	memcpy(&contVel[2].x ,  &contactVelos[2].m_x , sizeof(D3DXVECTOR3) ) ;
	memcpy(&contVel[3].x ,  &contactVelos[3].m_x , sizeof(D3DXVECTOR3) ) ;




	/*
	contVel[0] *=20 ;
	contVel[1] *=20 ;
	contVel[2] *=20 ; 
	contVel[3] *=20;
	*/
	//frontCent = frontCent - VECTOR3(mat._41,mat._42, mat._43) ;
	//D3DXVec3Normalize(&frontCent , &frontCent ) ;
	//VECTOR3 v = frontCent * D3DXVec3Dot(&frontCent , &contVel[0]);

	D3DXVECTOR3 vecVel = VECTOR3(lin_velo.m_x , lin_velo.m_y , lin_velo.m_z) ;
	lin_Velocity_length = D3DXVec3Length(&vecVel); 

	D3DXVec3Normalize(&binormals[0] , &binormals[0] ) ;
	D3DXVec3Normalize(&binormals[1] , &binormals[1] ) ;
	D3DXVec3Normalize(&binormals[2] , &binormals[2] ) ;
	D3DXVec3Normalize(&binormals[3] , &binormals[3] ) ;
	float K = 1.0 ;
	lateral_velos[0] = binormals[0] * D3DXVec3Dot(&binormals[0] , &contVel[0]);
	lateral_velos[1] = binormals[1] * D3DXVec3Dot(&binormals[1] , &contVel[1]);
	lateral_velos[2] = binormals[2] * D3DXVec3Dot(&binormals[2] , &contVel[2]);
	lateral_velos[3] = binormals[3] * D3DXVec3Dot(&binormals[3] , &contVel[3]);


	//lateral_velos[0] *= 1.6;//(0.1/dist[0]) + 1;  // * 5.5f ;
	//lateral_velos[1] *= 1.6;//(0.1/dist[1]) + 1;  // * 5.5f ; 
	//lateral_velos[2] *= 1.6;//(0.1/dist[2]) + 1;  // * 5.5f ; 
	//lateral_velos[3] *= 1.6;//(0.1/dist[3]) + 1;  // * 5.5f ; 




	/*
	D3DXVec3Normalize(&lateral_velos[0] , &lateral_velos[0] ) ;
	D3DXVec3Normalize(&lateral_velos[1] , &lateral_velos[1] ) ;
	D3DXVec3Normalize(&lateral_velos[2] , &lateral_velos[2] ) ;
	D3DXVec3Normalize(&lateral_velos[3] , &lateral_velos[3] ) ;
	*/

	for(int i = 0 ; i < 4 ; i++)
	{
		vwheels[i].binormal = binormals[i] ;
		vwheels[i].latAccel = lateral_velos[i];
		vwheels[i].pivot = tmp[i] ;
		vwheels[i].distance = dist[i] ;

		vwheels[i].lateralAcceleration = D3DXVec3Length(&lateral_velos[i]) - vwheels[i].prevVelocity ;

		vwheels[i].prevVelocity = D3DXVec3Length(&lateral_velos[i]);
	}

	if( (D3DXVec3Length(&vwheels[0].latAccel) > 20 ) && (D3DXVec3Length(&vwheels[3].latAccel) > 20 )) 
	{
		slide = true ;
	}
	else
	{
		slide = false ; 

	}

	//USE UserJoint so null this vectors
	lateral_velos[0] = lateral_velos[1] = lateral_velos[2] = lateral_velos[3] = VECTOR3(0,0,0) ;

	/*
	if(dist[0] < 0.29f) lateral_velos[0] =  VECTOR3(0,0,0) ;
	if(dist[1] < 0.29f) lateral_velos[1] =  VECTOR3(0,0,0) ;
	if(dist[2] < 0.29f) lateral_velos[2] =  VECTOR3(0,0,0) ;
	if(dist[3] < 0.29f) lateral_velos[3] =  VECTOR3(0,0,0) ;
	*/


	vwheels[0].pivot2 = GetRelativeVector(mat,VECTOR3(2,-0,-1.0));


	vwheels[1].pivot2 = GetRelativeVector(mat,VECTOR3(2,-0,1.0));


	vwheels[2].pivot2 = GetRelativeVector(mat,VECTOR3(-2,-0,1.0));


	vwheels[3].pivot2 = GetRelativeVector(mat,VECTOR3(-2,-0,-1.0));


	float koef =  updateInterval/2 ;
	float maxAngle = 5 ;
	float a = 0 ;

	//maxAngle = (lin_Velocity_length ) / 20 ;
	//
	//if(maxAngle > 5 ) maxAngle = 5 ;
	//
	//if(bforward)
	//{
	//  maxAngle = 5; 
	//}

	//NOTICE maxAngle is modified bellow 

	maxAngle = 30 ;

	//Angle and step for external config !
	if(bleft == true)
	{ 
		ang += koef  ;
		if(ang > maxAngle) ang = maxAngle ;
		//ang = 30 ; 
	}

	if(bright == true)
	{ 
		ang -= koef  ;
		if(ang < -maxAngle) ang = -maxAngle ; 
		//ang = -30 ;
	}




	if(bleft == false && bright == false )
	{
		if(ang > 0.0) ang = ang - (koef  )  ;
		if(ang < 0.0) ang = ang + (koef  ) ;

	}

	//if(bforward == false)
	//{
	// ang = ang / 1.05 ;
	//}

	//if(ang > 0) f[2] *= fabs(ang/10);
	//if(ang < 0) f[3] *= fabs(ang/10) ;



	// Drive force( 36 ) for external config !
	VECTOR3 vforward = VECTOR3(0,0,0) ;

	//if(slide == true )ang =  ang /1.1 ;
	float upforce = 30 / (lin_Velocity_length / 8)  ;
	if(upforce > 15) upforce = 15 ; 

	if(bforward)
	{  
		vforward = GetRelativeVector(mat,VECTOR3(50,upforce  , ang / 3));
	}
	else
	{
		vforward = GetRelativeVector(mat,VECTOR3(0,0,ang / 3 ));

	}






	if(bbackward)
	{
		vforward = GetRelativeVector(mat,VECTOR3(-50,-upforce,0)); 
		bforward = true ;
	}


	if(carAI == false)
	{
		if(GetDistance3D_(&Points[numAIpoints-1] , &VECTOR3(center.m_x , center.m_y  , center.m_z )) < 50 )
		{
			finishReached = true ; 
		}

		if(GetDistance3D_(&Points[0] , &VECTOR3(center.m_x , center.m_y  , center.m_z )) < 50 &&  (finishReached) )
		{
			lapCount ++ ;  
			finishReached = false ; 
		}

	}



	if(carAI > 0)
	{
		// ======================================================================================
		//AI car // comment it out


		/*       
		VECTOR3 getWayPt( VECTOR3& pos)
		{
		if(GetDistance3D_(&Points[wayindex] , &pos) < 20 )
		{
		wayindex++;
		if(wayindex > (sizeof(Points) / sizeof(D3DXVECTOR3)) -1 ) wayindex = 0 ;
		return Points[wayindex] ;
		}
		return Points[wayindex] ;
		};
		*/


		if(carAI > 0)
		{



			vforward = getWayPt(VECTOR3(center.m_x , center.m_y  , center.m_z )) ;
			if (lin_Velocity_length < 0.1)
			{
				dMatrix m = GetIdentityMatrix() ; 
				m.m_posit = dVector(vforward.x , vforward.y ,  vforward.z ) ;
				m.m_posit.m_y += 15; 
				NewtonBodySetMatrix(vehicleBody , &m.m_front.m_x ) ;
				NewtonWorldUnfreezeBody(world,vehicleBody);  
			}

			//vforward =    GetVecAlongThePath(Animator , 20 , 2 ) ;
			//vforward.y += 2 ;
			//float dis = GetDistance3D_(&VECTOR3(mat._41 , mat._42 , mat._43) , &vforward ) ;
			//if( GetDistance3D_(&VECTOR3(mat._41 , mat._42 , mat._43) , &vforward ) > 200 ) vforward = VECTOR3(mat._41 , mat._42 , mat._43) ;

			D3DXVECTOR3 vfront ; 
			vfront = GetRelativeVector(mat,VECTOR3(0,0 ,20) ) - VECTOR3(center.m_x , center.m_y , center.m_z ) ;
			vforward = vforward - VECTOR3(center.m_x , center.m_y , center.m_z ) ;
			float d  = AngleFromVectors_( vforward , vfront ) ;
			//vfront = GetRelativeVector(mat,VECTOR3(20,0 ,0) ) - VECTOR3(center.m_x , center.m_y , center.m_z );

			ang = D3DXToDegree(d); 

			if(ang > 40 ) ang = 40 ;
			if(ang < -40) ang = -40 ; 

			if(ang > 0)
			{
				vang += koef * 3 ; 
			}
			else
			{
				vang -= koef * 3 ;
			}

			if(vang > (ang * 1.5) ) vang = ang * 1.5 ;
			if(vang < (-ang * 1.5)) vang = -ang * 1.5 ; 


			//if(d > 0 ) 
			//{
			//   ang = 30 ;
			//}
			//else
			//{
			// ang = -30 ;
			//}



			//if(d > 0 )
			//{
			//   ang = D3DXToDegree(AngleFromVectors_( vforward , vfront )) ;
			//}
			//else
			//{
			//   ang = -D3DXToDegree(AngleFromVectors_( vforward , vfront )) ;
			//}


			vforward = GetRelativeVector(mat,VECTOR3(23,0 ,ang/5 ) );


		}
		else
		{
			vforward = VECTOR3(0,0,0) ;
		}




		//D3DXVec3Normalize(&forward,&forward);

		bforward = true ;


		// END AI CARS 
		// ========================================================================================

	}




	VECTOR3 forwardEnd = vforward - VECTOR3(center.m_x , center.m_y , center.m_z ) ;

	if(carAI>0)
	{  
		forwardEnd *= 1.0; 
	}
	nextwaypt = forwardEnd ;

	forwardEnd *= forceForward ;



	dVector COM_offset ; 
	if( (hits[0] == false) || (hits[1] == false) || (hits[2] == false) || (hits[3] == false)  )
	{
		//lateral_velos[0] = lateral_velos[1] = lateral_velos[2] = lateral_velos[3] = VECTOR3(0,0,0) ;
		//forwardEnd = VECTOR3(0,0,0) ;
		//COM_offset = dVector(0.0 , -0.01 , 0.0 ); //Original COM_offset(0.0 , +0.06 , 0.0 );
		//NewtonBodySetCentreOfMass(vehicleBody , &COM_offset.m_x) ;

	}
	else
	{
		COM_offset = dVector(0.0 , +0.1 , 0.0 ); //Original COM_offset(0.0 , +0.06 , 0.0 );
		NewtonBodySetCentreOfMass(vehicleBody , &COM_offset.m_x) ;

	}
	//DAMP
	dVector nozzleDir (CarMat.m_up);
	float nozzleDampCoef = this->globalDampCoef ; // you need to tweak this value for gentle soft veheaviour 
	dVector forces[4] ;
	float nozzleDampForce = - (contactVelos[0] % nozzleDir) * mass * nozzleDampCoef; 

	forces[0] = dVector(nozzleDir.Scale (D3DXVec3Length(&f[0]) + nozzleDampForce));

	nozzleDampForce = - (contactVelos[1] % nozzleDir) * mass * nozzleDampCoef; 
	forces[1]  = dVector(nozzleDir.Scale (D3DXVec3Length(&f[1]) + nozzleDampForce));

	nozzleDampForce = - (contactVelos[2] % nozzleDir) * mass * nozzleDampCoef; 
	forces[2]  = dVector(nozzleDir.Scale (D3DXVec3Length(&f[2]) + nozzleDampForce));

	nozzleDampForce = - (contactVelos[3] % nozzleDir) * mass * nozzleDampCoef; 
	forces[3]  = dVector (nozzleDir.Scale (D3DXVec3Length(&f[3]) + nozzleDampForce));

	memcpy(&f[0].x , &forces[0].m_x , sizeof(D3DXVECTOR3)); 
	memcpy(&f[1].x , &forces[1].m_x , sizeof(D3DXVECTOR3)); 
	memcpy(&f[2].x , &forces[2].m_x , sizeof(D3DXVECTOR3)); 
	memcpy(&f[3].x , &forces[3].m_x , sizeof(D3DXVECTOR3)); 

	//END DAMP

	//apply force with a little bit shorter arms

	float cc = 1.5 ;

	//float turn = ang ;
	//if(turn < 0.0f ) turn *= -1 ;

	// float tilt = turn / 60.0f ;


	D3DXVECTOR3 off_tmp[4];

	off_tmp[0] = GetRelativeVector(mat,VECTOR3(5/cc ,-0 , -2/cc  ));


	off_tmp[1] = GetRelativeVector(mat,VECTOR3(5/cc ,-0   ,2/cc   ));


	off_tmp[2] = GetRelativeVector(mat,VECTOR3(-5/cc ,-0 , 2/cc   ));


	off_tmp[3] = GetRelativeVector(mat,VECTOR3(-5/cc ,-0 , -2/cc  ));


	//
	// POWER BACK RISING
	//dVector vec = (lin_velo - dVector(forwardEnd.x , forwardEnd.y , forwardEnd.z )) ; 
	//f[2] *= sqrt(vec.m_x*vec.m_x + vec.m_y*vec.m_y + vec.m_z*vec.m_z)/100 ;
	//f[3] *= sqrt(vec.m_x*vec.m_x + vec.m_y*vec.m_y + vec.m_z*vec.m_z)/100 ;



	if(hits[0])
	{
		if(bforward || bleft || bright)
		{

			BodyAddGlobalForce(body,&forwardEnd.x,&off_tmp[0].x);
		}

		/* if(bforward)
		{
		f[0] *= (1 + ( 1 / (lin_Velocity_length/10))) ; 
		}*/

		BodyAddGlobalForce(body,&f[0].x,&off_tmp[0].x);

		vwheels[0].contact = true ;
		vwheels[0].tireLoad = D3DXVec3Length(&f[0]);

	} 


	if(hits[1])
	{
		if(bforward || bleft || bright)
		{
			BodyAddGlobalForce(body,&forwardEnd.x,&off_tmp [1].x);
		}

		/* if(bforward)
		{
		f[1] *= (1 + (1 / (lin_Velocity_length/10))) ; 
		}         */

		BodyAddGlobalForce(body,&f[1].x,&off_tmp[1].x);

		vwheels[1].contact = true ;
		vwheels[1].tireLoad = D3DXVec3Length(&f[1]);
	}



	if(hits[2])
	{


		BodyAddGlobalForce(body,&f[2].x,&off_tmp[2].x);

		vwheels[2].contact = true ;

		vwheels[2].tireLoad = D3DXVec3Length(&f[2]);
	}



	if(hits[3])
	{



		BodyAddGlobalForce(body,&f[3].x,&off_tmp[3].x);

		vwheels[3].tireLoad = D3DXVec3Length(&f[3]);

		vwheels[3].contact = true ;
	}


	dVector angularDamp (0.5f, 0.5f, 0.5f);
	NewtonBodySetAngularDamping (body, &angularDamp.m_x);
	//  NewtonBodySetLinearDamping (body, 0.2f);
}




//==============================================================================
D3DXVECTOR3 GetVecAlongThePath(CSplineAnimator* Anim , unsigned timePos , unsigned msAhead)
{
	D3DXVECTOR3 vlook ;
	return Anim->update(GetTickCount()+ timePos) ;

	//update the lookat point to some point
	//which is 10 ms ahead the path
	//return Anim->update(GetTickCount() +  msAhead + timePos) ;


}


float AngleFromVectors_(D3DXVECTOR3 v1 , D3DXVECTOR3 v2)
{

	D3DXVec3Normalize(&v1,&v1);
	D3DXVec3Normalize(&v2,&v2);
	//float dot = D3DXVec3Dot(&v1, &v2) ;
	// return acos(dot);
	return D3DXVec3Dot(&v1, &v2) ;
}




float GetDistance3D_(D3DXVECTOR3 * v1, D3DXVECTOR3 * v2)
{
	return sqrt(pow(v1->x - v2->x,2)+pow(v1->y - v2->y,2)+pow(v1->z - v2->z,2)) ;
};


void BodyAddGlobalForce(const NewtonBody *a_newtonBody, dFloat a_force[3], dFloat a_posit[3] )
{
	dMatrix m;
	NewtonBodyGetMatrix( a_newtonBody, (float*)&m[0] );

	dVector tmp( a_posit );
	dVector r = tmp - m.m_posit;

	dVector torque;
	torque = r * dVector(a_force);

	NewtonBodyAddForce( a_newtonBody, &a_force[0] );
	NewtonBodyAddTorque( a_newtonBody, &torque[0]);
} 



D3DXMATRIX GetRelativeMattrix(D3DXMATRIX mat , D3DXVECTOR3 vRel )
{

	D3DXMATRIX trans ;
	D3DXMatrixTranslation(&trans , vRel.x , vRel.y ,vRel.z) ;
	trans = trans * mat ;
	return trans ;

}

S_RaycastVehicle::S_RaycastVehicle(void)
{
}

S_RaycastVehicle::~S_RaycastVehicle(void)
{
}

VECTOR3 S_RaycastVehicle::getWayPt( VECTOR3& pos)
{
	if(GetDistance3D_(&Points[wayindex] , &pos) < 40 )
	{
		wayindex++;
		if(wayindex > numAIpoints -1 )
		{
			wayindex = 0 ;
			lapCount ++ ; 
		}
		return Points[wayindex] ;
	}
	return Points[wayindex] ;
};




//===============================================================================
void S_RaycastVehicle::PhysicsSubmitCarConstraintsHandler(const NewtonJoint* joint)
{
	//return ;

	//for(int i = 0 ; i < 4 ; i++)
	//{
	//if(wheels[i].contact )
	//{
	//if( (i==2) || (i== 3) )
	//{

	//AudioEnv->SourceSetVolume(Sound_skid,0);
	float dist = 0.0 ; 
	if( (vwheels[2].contact) && (vwheels[3].contact) )
	{
		////VECTOR3 pivot2 = (vwheels[2].pivot +  vwheels[3].pivot + vwheels[0].pivot +  vwheels[1].pivot)  / 4 ; 
		//
		//VECTOR3 pivot2 = (vwheels[2].pivot2 +  vwheels[3].pivot2 )  / 2 ; 
		//
		//float cent = (D3DXVec3Length(&vwheels[3].latAccel) + D3DXVec3Length(&vwheels[2].latAccel)) / 2 ;
		//               cent = 6 /cent;
		//
		//
		////D3DXMATRIX turn,h ;
		////   dMatrix m; 
		////   memcpy(&h._11,&CarMat[0][0],sizeof(D3DXMATRIX)) ;
		////                   D3DXMatrixRotationY(&turn,D3DXToRadian(ang));
		////                   h = h * turn ;
		////                   memcpy(&m[0][0],&h._11,sizeof(D3DXMATRIX)) ;
		//                  


		//cent = lin_Velocity_length / 130 ;

		//if(cent > 0.2) cent = 0.2;
		//
		//pivot2 = GetRelativeVector(matrix,VECTOR3(-cent,0,0));

		//
		//
		////if(slide == false )
		////{
		//
		////dVector cross = CarMat.m_right + dVector(pivot2.x , pivot2.y , pivot2.z);

		//NewtonUserJointAddLinearRow( CarJoint, &pivot2.x , &pivot2.x, &CarMat.m_right[0]  );
		//
		//
		////Estimate the "angle" between the vehicle right vector and the rear wheel velocity, and apply 
		//// the acceleration to the joint with opposite sign.
		//dist = (vwheels[2].distance + vwheels[3].distance ) / 2 ;
		//dist = 0.8 / dist ;
		//  
		//float k2 = 1.0 ;
		//float acc = D3DXVec3Dot(&vwheels[2].latAccel , &vwheels[2].binormal) * dist * k2 ; 

		//
		//NewtonUserJointSetRowAcceleration( CarJoint, -acc ) ;
		//                       
		//NewtonUserJointSetRowMinimumFriction( CarJoint,-600 );
		//NewtonUserJointSetRowMaximumFriction( CarJoint, 600 );
		//
		//
		////} //end if slide  == false


		//   VECTOR3 pivot2 = (vwheels[2].pivot +  vwheels[3].pivot) / 2 ; 
		//
		//NewtonUserJointAddLinearRow( CarJoint, &pivot2.x, &pivot2.x, &CarMat.m_right[0]  );
		//
		//
		////Estimate the "angle" between the vehicle right vector and the rear wheel velocity, and apply 
		//// the acceleration to the joint with opposite sign.
		//dist = (vwheels[2].distance + vwheels[3].distance) / 2 ;
		//dist = 0.8 / dist ;
		//  
		//float k2 = 1.0 ;
		//float acc = D3DXVec3Dot(&vwheels[2].latAccel , &vwheels[2].binormal) * dist * k2 ; 
		//NewtonUserJointSetRowAcceleration( CarJoint, -acc ) ;
		//                       
		//NewtonUserJointSetRowMinimumFriction( CarJoint,-600  );
		//NewtonUserJointSetRowMaximumFriction( CarJoint, 600  );

	}

	//}
	//}


	if( (vwheels[0].contact) && (vwheels[1].contact) )
	{



		//VECTOR3 pivot2 = (vwheels[0].pivot2 +  vwheels[1].pivot2) / 2 ; 
		//D3DXMATRIX turn,h ;
		//dMatrix m = GetIdentityMatrix(); 
		//memcpy(&h,&CarMat,sizeof(D3DXMATRIX)) ;
		//                  D3DXMatrixRotationY(&turn,D3DXToRadian(-ang*10));
		//                  
		//h = h * turn ;
		//                  
		//memcpy(&m,&h , sizeof(D3DXMATRIX)) ;
		//               dVector cross = CarMat.m_right + dVector(pivot2.x , pivot2.y , pivot2.z);
		//NewtonUserJointAddLinearRow( CarJoint, &pivot2.x, &pivot2.x, &cross[0]  );
		//
		//
		////Estimate the "angle" between the vehicle right vector and the rear wheel velocity, and apply 
		//// the acceleration to the joint with opposite sign.
		//dist = (vwheels[0].distance + vwheels[1].distance) / 2 ;
		//dist = 0.8 / dist ;
		//
		//float k2 = 1.0 ;
		//float acc = D3DXVec3Dot(&vwheels[0].latAccel , &vwheels[0].binormal) * dist * k2 ; 
		//

		//
		//NewtonUserJointSetRowAcceleration( CarJoint, -acc ) ;
		//                       
		//NewtonUserJointSetRowMinimumFriction( CarJoint,-250 );
		//NewtonUserJointSetRowMaximumFriction( CarJoint, 250 );




		//      }
	}


	for(int  i = 0 ; i < 4 ; i++)
	{
		if(vwheels[i].contact)
		{

			D3DXMATRIX turn,h ;
			dMatrix m; 
			memcpy(&h._11,&CarMat[0][0],sizeof(D3DXMATRIX)) ;
			D3DXMatrixRotationY(&turn,D3DXToRadian(-(ang) ));
			h = h * turn ;
			memcpy(&m[0][0],&h._11,sizeof(D3DXMATRIX)) ;



			if( (i == 0) || (i == 1))    
			{
				NewtonUserJointAddLinearRow( CarJoint, &vwheels[i].pivot.x , &vwheels[i].pivot.x, &m.m_right[0]  );
			}
			else
			{

				NewtonUserJointAddLinearRow( CarJoint, &vwheels[i].pivot.x , &vwheels[i].pivot.x, &CarMat.m_right[0]  );
			}



			float dist = vwheels[i].distance ;
			float lateralFriction = 100;

			dist = ( 1.0 / (dist) ) * lateralFriction ;

			float k2 = 1.0 ;
			float acc = D3DXVec3Dot(&vwheels[i].latAccel , &vwheels[i].binormal) * dist * k2 ; 
			//
			//NewtonUserJointSetRowAcceleration( CarJoint, 0.5 ) ;

			NewtonUserJointSetRowMinimumFriction( CarJoint,-dist );
			NewtonUserJointSetRowMaximumFriction( CarJoint, dist );

		}


	}




}


//===============================================================================
void S_RaycastVehicle::getTireMatricesDirect(D3DXMATRIX* pMatrices ) 
{
	memcpy(pMatrices , &tireMatrices[0]._11 , sizeof(D3DXMATRIX) * 4 ) ;

}
void S_RaycastVehicle::getTireMatrices(D3DXMATRIX* pMatrices , float timestep )
{

	D3DXMATRIX scale ;
	D3DXMatrixScaling(&scale,2.5,2.5,2.5);
	//OPTIMIZE THIS !!
	NewtonBodyGetMatrix(vehicleBody, &matrix._11);  
	matrix = scale * matrix ;


	/*if(carAI>0)
	{
	VECTOR3 cent = VECTOR3(matrix._41 , matrix._42 ,matrix._43);
	VECTOR3 carV,wayV ;
	carV = FrontCenter - cent ;


	ang = AngleFromVectors_(carV , nextwaypt ) ;   



	ang = D3DXToDegree(ang);

	VECTOR3 right = VECTOR3(CarMat.m_right.m_x , CarMat.m_right.m_y , CarMat.m_right.m_z ) ;
	float dot = D3DXVec3Dot(&nextwaypt , &right) ;

	if (dot<0.0) ang = -ang ;


	if(ang > 30) ang = 30; 
	if(ang < -30) ang = -30 ;

	}*/


	D3DXMATRIX tireMat ;
	D3DXMATRIX rot ;
	//float an = 0.0 ;
	//if(driveStuff.left == true) an = -45 ;
	//if(driveStuff.right == true) an = 45 ;
	if(carAI > 0)
	{

		D3DXMatrixRotationY(&rot , D3DXToRadian(-ang* (1.3f) )) ;
	}
	else
	{
		D3DXMatrixRotationY(&rot , D3DXToRadian(-ang )) ;
	}


	for(int i = 0 ; i < 4 ; i++)
	{
		if( (vwheels[i].distance > 0.7) ||  (vwheels[i].contact == false) ) vwheels[i].distance = 0.7;


	}




	float ammount = lin_Velocity_length / 100.0f;
	if((lin_Velocity_length < 1)&&( bforward || bbackward))
	{
		ammount = 0.3; 
	}
	if(ammount > 0.56) ammount = 0.56; 
	if( ! bbackward )
	{   
		wheel_rollangle -= (ammount) ;
		if(wheel_rollangle < 0.0) wheel_rollangle = 360.0 ;
	}
	else
	{
		wheel_rollangle += (ammount) ;
		if(wheel_rollangle > 360.0) wheel_rollangle = 0.0 ;
	}


	//wheel_rollangle = fmod(wheel_rollangle + lin_Velocity_length * (timestep /8) , 3.1416f * 2.0f );


	D3DXMATRIX rotZ ;
	D3DXMatrixRotationZ(&rotZ , wheel_rollangle ) ;  
	tireMat = GetRelativeMattrix(matrix,VECTOR3(1.5, (-vwheels[0].distance  )  ,-0.75));
	tireMat = rotZ * rot * tireMat;     
	pMatrices[0] = tireMat ;
	tireMatrices[0] = tireMat ;

	tireMat = GetRelativeMattrix(matrix,VECTOR3(1.5, (-vwheels[1].distance  )  ,0.75));
	tireMat = rotZ *  rot * tireMat ; 

	pMatrices[1] = tireMat ;  
	tireMatrices[1] = tireMat ;

	tireMat = GetRelativeMattrix(matrix,VECTOR3(-1.3, (-vwheels[2].distance )  ,0.75));
	tireMat = rotZ *  tireMat ;

	pMatrices[2] = tireMat ;  
	tireMatrices[2] = tireMat ;

	tireMat = GetRelativeMattrix(matrix,VECTOR3(-1.3, (-vwheels[3].distance )    ,-0.75));
	tireMat = rotZ *  tireMat ;

	pMatrices[3] = tireMat ;      
	tireMatrices[3] = tireMat ;



}




void _cdecl S_RaycastVehicle::PhysicsSubmitCarConstraints (const NewtonJoint* joint) 
{
	S_RaycastVehicle* vehi = (S_RaycastVehicle*)NewtonJointGetUserData(joint) ;
	vehi->PhysicsSubmitCarConstraintsHandler(joint) ;



}







#endif



















CustomRayCastCar::CustomRayCastCar(int maxTireCount, const dMatrix& cordenateSytem, NewtonBody* carBody, const dVector& gravity)
	:NewtonCustomJoint(3 * maxTireCount, carBody, NULL), m_gravity (gravity)
{
	dVector com;
	dMatrix tmp;

	// set the chassis matrix at the center of mass
	NewtonBodyGetCentreOfMass(m_body0, &com[0]);
	com.m_w = 1.0f;

	// set the joint reference point at the center of mass of the body
	dMatrix chassisMatrix (cordenateSytem);
	chassisMatrix.m_posit += chassisMatrix.RotateVector(com);

	CalculateLocalMatrix (chassisMatrix, m_localFrame, tmp);

	// allocate space for the tires;
	m_tiresCount = 0;
	m_tires = new Tire[maxTireCount];

	m_curSpeed = 0.0f;
	m_aerodynamicDrag = 0.1f; 
	m_aerodynamicDownForce = 0.1f; 

	dFloat Ixx; 
	dFloat Iyy; 
	dFloat Izz; 
	NewtonBodyGetMassMatrix(m_body0, &m_mass, &Ixx, &Iyy, &Izz);

	// register the callback for tire integration
//	NewtonUserJointSetFeedbackCollectorCallback (m_joint, IntegrateTires);
}


CustomRayCastCar::~CustomRayCastCar()
{
	NewtonWorld *world;

	world = NewtonBodyGetWorld (m_body0);
	for (int i = 0; i < m_tiresCount; i ++) {
		NewtonReleaseCollision (world, m_tires[i].m_shape);
	}

	if(m_tires) {
		delete[] m_tires;
	}
}


int CustomRayCastCar::GetTiresCount() const
{
	return m_tiresCount;
}




void CustomRayCastCar::GetInfo (NewtonJointRecord* info) const
{
}

//this function is to be overloaded by a derive class
void CustomRayCastCar::SetSteering (dFloat angle)
{
}

//this function is to be overloaded by a derive class
void CustomRayCastCar::SetBrake (dFloat torque)
{
}

//this function is to be overloaded by a derive class
void CustomRayCastCar::SetTorque (dFloat torque)
{
}

dFloat CustomRayCastCar::GetSpeed() const
{
	return m_curSpeed;
}


void CustomRayCastCar::SetTireMaxRPS (int tireIndex, dFloat maxTireRPS)
{
	m_tires[tireIndex].m_maxTireRPS = maxTireRPS;
}

CustomRayCastCar::Tire& CustomRayCastCar::GetTire (int index) const
{
	return m_tires[index];
}

dFloat CustomRayCastCar::GetParametricPosition (int index) const
{
	return m_tires[index].m_posit / m_tires[index].m_suspensionLength;
}

void CustomRayCastCar::SetTireSteerAngle (int index, dFloat angle)
{
	m_tires[index].m_steerAngle = angle;
	m_tires[index].m_localAxis.m_z = dCos (angle);
	m_tires[index].m_localAxis.m_x = dSin (angle);
}

void CustomRayCastCar::SetTireTorque (int index, dFloat torque)
{
//torque=-600.0f;
	m_tires[index].m_torque = torque;
}

void CustomRayCastCar::SetTireBrake (int index, dFloat torque)
{
	m_tires[index].m_breakTorque = torque;
}


void CustomRayCastCar::AddSingleSuspensionTire (
	void *userData,
	const dVector& localPosition, 
	dFloat mass,
	dFloat radius, 
	dFloat width,
	dFloat suspensionLength,
	dFloat springConst,
	dFloat springDamper,
	int castMode)
{
	// calculate the tire local base pose matrix
	dMatrix bodyMatrix;
	m_tires[m_tiresCount].m_torque = 0.0f;
	m_tires[m_tiresCount].m_harpoint = m_localFrame.UntransformVector(localPosition);              
	m_tires[m_tiresCount].m_localAxis = m_localFrame.UnrotateVector(dVector (0.0f, 0.0f, 1.0f, 0.0f));
	m_tires[m_tiresCount].m_localAxis.m_w = 0.0f;
	m_tires[m_tiresCount].m_userData = userData;
	m_tires[m_tiresCount].m_angularVelocity = 0.0f;
	m_tires[m_tiresCount].m_spinAngle = 0.0f;
	m_tires[m_tiresCount].m_steerAngle = 0.0f;
	
	m_tires[m_tiresCount].m_posit = suspensionLength;
	m_tires[m_tiresCount].m_suspensionLength = suspensionLength;
	m_tires[m_tiresCount].m_tireLoad = 0.0f;
	m_tires[m_tiresCount].m_breakTorque = 0.0f;
	m_tires[m_tiresCount].m_localSuspentionSpeed = 0.0f;
	
	m_tires[m_tiresCount].m_springConst = springConst;
	m_tires[m_tiresCount].m_springDamper = springDamper;
	m_tires[m_tiresCount].m_groundFriction = 1.0f;

	m_tires[m_tiresCount].m_tireUseConvexCastMode = castMode; 
//	m_tires[m_tiresCount].m_tireJacobianRowIndex = -1;

	// make a convex shape to represent the tire collision
	#define TIRE_SHAPE_SIZE 12
	dVector shapePoints[TIRE_SHAPE_SIZE * 2];
	for (int i = 0; i < TIRE_SHAPE_SIZE; i ++) {
		shapePoints[i].m_x = -width * 0.5f;	
		shapePoints[i].m_y = radius * dCos (2.0f * 3.1416 * dFloat(i)/ dFloat(TIRE_SHAPE_SIZE));
		shapePoints[i].m_z = radius * dSin (2.0f * 3.1416 * dFloat(i)/ dFloat(TIRE_SHAPE_SIZE));
		shapePoints[i + TIRE_SHAPE_SIZE].m_x = -shapePoints[i].m_x;
		shapePoints[i + TIRE_SHAPE_SIZE].m_y = shapePoints[i].m_y;
		shapePoints[i + TIRE_SHAPE_SIZE].m_z = shapePoints[i].m_z;
	}
	m_tires[m_tiresCount].m_shape = NewtonCreateConvexHull (m_world, TIRE_SHAPE_SIZE * 2, &shapePoints[0].m_x, sizeof (dVector), 0.0f, NULL);

	// calculate the tire geometrical parameters
	m_tires[m_tiresCount].m_radius = radius;
//	m_tires[m_tiresCount].m_radiusInv  = 1.0f / m_tires[m_tiresCount].m_radius;
	m_tires[m_tiresCount].m_mass = mass;	
	m_tires[m_tiresCount].m_massInv = 1.0f / m_tires[m_tiresCount].m_mass;	
	m_tires[m_tiresCount].m_Ixx = mass * radius * radius / 2.0f;
	m_tires[m_tiresCount].m_IxxInv = 1.0f / m_tires[m_tiresCount].m_Ixx;
	SetTireMaxRPS (m_tiresCount, 150.0f / radius);

	m_tiresCount ++;
}


const dMatrix& CustomRayCastCar::GetChassisMatrixLocal () const
{
	return m_localFrame;
}

dMatrix CustomRayCastCar::CalculateSuspensionMatrix (int tireIndex, dFloat distance) const
{
	const Tire& tire = m_tires[tireIndex];

	dMatrix matrix;
	// calculate the steering angle matrix for the axis of rotation
	matrix.m_front = tire.m_localAxis;
	matrix.m_up    = dVector (0.0f, 1.0f, 0.0f, 0.0f);
	matrix.m_right = dVector (-tire.m_localAxis.m_z, 0.0f, tire.m_localAxis.m_x, 0.0f);
	matrix.m_posit = tire.m_harpoint - m_localFrame.m_up.Scale (distance);
	return matrix;
}

dMatrix CustomRayCastCar::CalculateTireMatrix (int tireIndex) const
{
	const Tire& tire = m_tires[tireIndex];

	// calculate the rotation angle matrix
	dMatrix angleMatrix (dPitchMatrix(tire.m_spinAngle));

	// get the tire body matrix
	dMatrix bodyMatrix;
	NewtonBodyGetMatrix(m_body0, &bodyMatrix[0][0]);
	return angleMatrix * CalculateSuspensionMatrix (tireIndex, tire.m_posit) * m_localFrame * bodyMatrix;
}


unsigned CustomRayCastCar::ConvexCastPrefilter(const NewtonBody* body, const NewtonCollision* collision, void* userData)
{
	NewtonBody* me;

	// for now just collide with static data.m_bodies
	dFloat mass; 
	dFloat Ixx; 
	dFloat Iyy; 
	dFloat Izz; 
	NewtonBodyGetMassMatrix(body, &mass, &Ixx, &Iyy, &Izz);
	if (mass > 0.0f) {
		return 0;
	}


	me = (NewtonBody*) userData;
	// do no cast myself
	return (me != body);
}


void CustomRayCastCar::CalculateTireCollision (Tire& tire, const dMatrix& suspensionMatrixInGlobalSpace) const
{
	if (tire.m_tireUseConvexCastMode) { 
		NewtonWorldConvexCastReturnInfo info;
		dVector destination (suspensionMatrixInGlobalSpace.TransformVector(m_localFrame.m_up.Scale (-tire.m_suspensionLength)));

		// cast the convex shape along the suspension length
		dFloat hitParam;
		if (NewtonWorldConvexCast (m_world, &suspensionMatrixInGlobalSpace[0][0], &destination[0], tire.m_shape, &hitParam, (void*)m_body0, ConvexCastPrefilter, &info, 1)) {
			// the ray hit something
			tire.m_posit = info.m_intersectionParam * tire.m_suspensionLength;
			tire.m_contactPoint = info.m_point;
			tire.m_contactNormal = info.m_normal;
			// TO DO: get the material properties for tire frictions on different roads 
			//dTrace (("%f\n", info.m_intersectionParam));

			switch (info.m_contactID)
			{
				case 0:
				{
					// normal ground friction
					tire.m_groundFriction = 1.0f;
					break;
				}

				default:
				{
					// default ground friction
					tire.m_groundFriction = 1.0f;
					break;
				}
			}

		} else {
			// if the ray do not hit anything the tire in airborne
			tire.m_posit = tire.m_suspensionLength;
			tire.m_groundFriction = 0.0f;
		}

	} else {
		// make a data structure to collect the information returned by the ray cast
		struct RayCastInfo
		{
			RayCastInfo(const NewtonBody* body)
			{
				m_param = 1.0f;
				m_me = body;
				m_hitBody = NULL;
			}


			static dFloat RayCast (const NewtonBody* body, const dFloat* normal, int collisionID, void* userData, dFloat intersetParam)
			{
				RayCastInfo& caster = *((RayCastInfo*) userData); 

				// if this body is not the vehicle, see if a close hit
				if (body != caster.m_me) {
					if (intersetParam < caster.m_param) {
						// this is a close hit, record the information. 
						caster.m_param = intersetParam;
						caster.m_hitBody = body;
						caster.m_contactID = collisionID;
						caster.m_normal = dVector (normal[0], normal[1], normal[2], 1.0f);
					}
				}
				return intersetParam;
			}

			dFloat m_param;
			dVector m_normal;
			const NewtonBody* m_me;
			const NewtonBody* m_hitBody;
			int m_contactID;
		};

		RayCastInfo info (m_body0);

		// extend the ray by the radius of the tire
		dFloat dist (tire.m_suspensionLength + tire.m_radius);
		dVector destination (suspensionMatrixInGlobalSpace.TransformVector(m_localFrame.m_up.Scale (-dist)));	

		// cast a ray to the world
		NewtonWorldRayCast(m_world, &suspensionMatrixInGlobalSpace.m_posit[0], &destination[0], RayCastInfo::RayCast, &info, ConvexCastPrefilter);

		// if the ray hit something, it means the tire has some traction
		if (info.m_hitBody) {
			dFloat intesectionDist;


			tire.m_contactPoint = suspensionMatrixInGlobalSpace.m_posit + (destination - suspensionMatrixInGlobalSpace.m_posit).Scale (info.m_param); 
			tire.m_contactNormal = info.m_normal;
			// TO DO: get the material properties for tire frictions on different roads 

			intesectionDist = dist * info.m_param - tire.m_radius;
			if (intesectionDist > tire.m_suspensionLength) {
				intesectionDist = tire.m_suspensionLength;
			} else if (intesectionDist < dFloat (0.0f)) {
				intesectionDist = dFloat (0.0f);
			}
			tire.m_posit = intesectionDist;
			switch (info.m_contactID)
			{
				case 0:
				{
					// normal ground friction
					tire.m_groundFriction = 1.0f;
					break;
				}

				default:
				{
					// default ground friction
					tire.m_groundFriction = 1.0f;
					break;
				}
			}

		} else {
			tire.m_posit = tire.m_suspensionLength;
			tire.m_groundFriction = 0.0f;
		}
	}
}

#if 0

void CustomRayCastCar::SubmitConstrainst(dFloat timestep, int threadIndex)
{
//	dFloat invTimestep;
	dMatrix bodyMatrix;
	dMatrix suspensionMatrices[VEHICLE_MAX_TIRE_COUNT];  

	// get the simulation time
//	invTimestep = 1.0f / timestep ;

	// get the vehicle global matrix, and use it in several calculations
	NewtonBodyGetMatrix(m_body0, &bodyMatrix[0][0]);
	dMatrix chassisMatrix (m_localFrame * bodyMatrix);  

	// calculate all suspension matrices in global space and tire collision
	for (int i = 0; i < m_tiresCount; i ++) {
		Tire& tire = m_tires[i];
		dMatrix suspensionMatrix = suspensionMatrices[i];

		// calculate this suspension matrix and save it for future used
		suspensionMatrix = CalculateSuspensionMatrix (i, 0.0f) * chassisMatrix;

		// calculate the tire collision
		CalculateTireCollision (tire, suspensionMatrix);
	}

	// force and torque accumulators

	// calculate and accumulate the external forces
	ApplySuspensionForces (chassisMatrix, timestep);

	// calculate and accumulate the internals forces
	ApplyTireForces (chassisMatrix, timestep);

	// set the current vehicle speed
//	m_curSpeed = bodyMatrix[0] % globalVeloc;
}



#define SOLVER_ERROR_2					 (1.0e-2f) 
#define TIRE_FRICTION					 (1.2f)
//#define TIRE_FRICTION					 (1.0f)

#define AXEL_RELAXATION					(1.0e-3f)
#define CONTACT_PATCH_RELAXATION		(1.0e-2f)

#define VEHICLE_MAX_DOF					 (VEHICLE_MAX_TIRE_COUNT * 8)


class RayCastCarPhysicsData
{
	public:

	class BodyProxy
	{
		public:
		dMatrix m_invInertia;
		dVector m_force;
		dVector m_torque;
		dVector m_com;
		dFloat m_invMass;
		dFloat m_padd[3];
	};

	class Jacobian
	{
		public:
		dVector m_linear;
		dVector m_angular;
	};

	class JacobianPair
	{
		public:
		Jacobian m_jacobian_IM0;
		Jacobian m_jacobian_IM1;
	};

	BodyProxy m_bodies[VEHICLE_MAX_TIRE_COUNT + 2];
	Jacobian m_y[VEHICLE_MAX_DOF];
	JacobianPair m_Jt[VEHICLE_MAX_DOF];
	JacobianPair m_JinvMass[VEHICLE_MAX_DOF];

	dFloat m_accel[VEHICLE_MAX_DOF];
	dFloat m_force[VEHICLE_MAX_DOF];
	dFloat m_deltaForce[VEHICLE_MAX_DOF];
	dFloat m_deltaAccel[VEHICLE_MAX_DOF];

	dFloat m_diagDamp[VEHICLE_MAX_DOF];
	dFloat m_activeRow[VEHICLE_MAX_DOF];
	dFloat m_invDJMinvJt[VEHICLE_MAX_DOF];
	dFloat m_relaxation[VEHICLE_MAX_DOF];
	dFloat m_minForceLimit[VEHICLE_MAX_DOF];
	dFloat m_maxForceLimit[VEHICLE_MAX_DOF];

	int m_body0[VEHICLE_MAX_DOF];
	int m_body1[VEHICLE_MAX_DOF];
};

void CustomRayCastCar::ApplySuspensionForces (const dMatrix& chassisMatrix, dFloat timestep) const
{
	dVector globalVeloc;
	dVector globalOmega;
	dVector torque (0.0f, 0.0f, 0.0f, 0.0f);
	dVector force (0.0f, 0.0f, 0.0f, 0.0f);
	
	// get the chassis instantaneous linear and angular velocity in the local space of the chassis
	NewtonBodyGetOmega(m_body0, &globalOmega[0]);
	NewtonBodyGetVelocity(m_body0, &globalVeloc[0]);

	// add aerodynamic forces
	dVector verticalVeloc (chassisMatrix.m_up.Scale (chassisMatrix.m_up % globalVeloc));
	dVector horizontalVeloc (globalVeloc - verticalVeloc);

	force -= horizontalVeloc.Scale (m_aerodynamicDrag * m_mass);
	force -= chassisMatrix.m_up.Scale (m_aerodynamicDownForce * m_mass * dSqrt (horizontalVeloc % horizontalVeloc));

	// calculate all suspension forces due to spring and damper
	for (int i = 0; i < m_tiresCount; i ++) {
		
		Tire& tire = m_tires[i];

		tire.m_tireLoad = dFloat (0.0f);
		if (tire.m_posit < tire.m_suspensionLength) {

			// calculate the linear velocity of the tire at the ground contact
			dVector tireAxelPosit (chassisMatrix.TransformVector(tire.m_harpoint - m_localFrame.m_up.Scale (tire.m_posit)));
			dVector localAxelPosit (tireAxelPosit - chassisMatrix.m_posit);
			dVector tireAxelVeloc (globalVeloc + globalOmega * localAxelPosit); 		

			// TO DO: need to calculate the velocity if the other body at the point
			// for now assume the ground is a static body
			dVector hitBodyVeloc (0, 0, 0, 0);

			// calculate the relative velocity
			dVector relVeloc (tireAxelVeloc - hitBodyVeloc);
			tire.m_localSuspentionSpeed = -(relVeloc % chassisMatrix.m_up);

			// now calculate the tire load at the contact patch
			tire.m_tireLoad = - NewtonCalculateSpringDamperAcceleration (timestep, tire.m_springConst, tire.m_suspensionLength - tire.m_posit, tire.m_springDamper, tire.m_localSuspentionSpeed)  * m_mass;
			if (tire.m_tireLoad < 0.0f) {
				tire.m_tireLoad = 0.0f;
			}

			// convert the tire load force magnitude to a torque and force.
			dVector tireForce (chassisMatrix.m_up.Scale (tire.m_tireLoad));

			// accumulate the force and torque form this suspension
			force += tireForce;
			torque += localAxelPosit * tireForce;
		}
	}

	// apply the suspention force to body
	NewtonBodyAddForce(m_body0, &force[0]);
	NewtonBodyAddTorque(m_body0, &torque[0]);
}



void CustomRayCastCar::ApplyTireForces (const dMatrix& chassisMatrix, dFloat timestep) const
{
	int rows;
	int activeTiresRows;
	dFloat invTimestep;
	dVector bodyVeloc;
	dVector bodyOmega;
	RayCastCarPhysicsData data;

	// initialize workld bod physics data
	data.m_bodies[0].m_force = dVector (0.0f, 0.0f, 0.0f, 0.0f); 
	data.m_bodies[0].m_torque = dVector (0.0f, 0.0f, 0.0f, 0.0f); 
	data.m_bodies[0].m_invInertia = GetZeroMatrix();
	data.m_bodies[0].m_invMass = 0.0f;

	// initialize chassis physics data
	dMatrix tmpMatrix (GetIdentityMatrix());;
	NewtonBodyGetForceAcc (m_body0, &data.m_bodies[1].m_force.m_x);
	NewtonBodyGetTorqueAcc (m_body0, &data.m_bodies[1].m_torque.m_x);
	NewtonBodyGetInvMass(GetBody0(), &data.m_bodies[1].m_invMass, &tmpMatrix[0][0], &tmpMatrix[1][1], &tmpMatrix[2][2]);
	data.m_bodies[1].m_invInertia = chassisMatrix.Transpose() * tmpMatrix;


	NewtonBodyGetOmega(m_body0, &bodyOmega.m_x);
	NewtonBodyGetVelocity(m_body0, &bodyVeloc.m_x);

	// add Jacobian between chassis and tires
	rows = 0;
	for (int i = 0; i < m_tiresCount; i ++) {
		Tire& tire = m_tires[i];

		// calculate the tire pin directions 
		dVector rightDir (chassisMatrix.RotateVector (tire.m_localAxis));
		dVector frontDir (chassisMatrix.m_up * rightDir);
		tire.m_longitudinalDir = frontDir;
		tire.m_lateralDir = rightDir; 

		// save tire dynamic data			
		data.m_bodies[i + 2].m_com = chassisMatrix.TransformVector (tire.m_harpoint - m_localFrame.m_up.Scale (tire.m_posit));
		data.m_bodies[i + 2].m_invMass = tire.m_massInv;
		data.m_bodies[i + 2].m_force = m_gravity.Scale(tire.m_mass);
		data.m_bodies[i + 2].m_torque = tire.m_lateralDir.Scale (tire.m_torque);
		data.m_bodies[i + 2].m_invInertia = GetIdentityMatrix();
		data.m_bodies[i + 2].m_invInertia[0][0] *= tire.m_IxxInv;
		data.m_bodies[i + 2].m_invInertia[1][1] *= tire.m_IxxInv;
		data.m_bodies[i + 2].m_invInertia[2][2] *= tire.m_IxxInv;  // assume for now spherical inerrtial fo tires

		// set the tire longitudinal jacobian
		{
			data.m_body0[rows] = 1;
			data.m_body1[rows] = i + 2;

			RayCastCarPhysicsData::Jacobian &jacobian0 = data.m_Jt[rows].m_jacobian_IM0; 
			jacobian0.m_linear = frontDir;
			jacobian0.m_angular = (data.m_bodies[i + 2].m_com - chassisMatrix.m_posit) * jacobian0.m_linear;

			RayCastCarPhysicsData::Jacobian &jacobian1 = data.m_Jt[rows].m_jacobian_IM1; 
			jacobian1.m_linear = frontDir.Scale (-1.0f);
			jacobian1.m_angular = dVector (0.0f, 0.0f, 0.0f, 0.0f);

			data.m_accel[rows] = 0.0f;
			data.m_relaxation[rows] = AXEL_RELAXATION;
			data.m_minForceLimit[rows] = -1.0e10f;
			data.m_maxForceLimit[rows] =  1.0e10f;
			rows ++;
		}

		// set the tire lateral jacobian
		{
			data.m_body0[rows] = 1;
			data.m_body1[rows] = i + 2;

			RayCastCarPhysicsData::Jacobian &jacobian0 = data.m_Jt[rows].m_jacobian_IM0; 
			jacobian0.m_linear = rightDir;
			jacobian0.m_angular = (data.m_bodies[i + 2].m_com - chassisMatrix.m_posit) * jacobian0.m_linear;

			RayCastCarPhysicsData::Jacobian &jacobian1 = data.m_Jt[rows].m_jacobian_IM1; 
			jacobian1.m_linear = rightDir.Scale (-1.0f);
			jacobian1.m_angular = dVector (0.0f, 0.0f, 0.0f, 0.0f);

			data.m_accel[rows] = 0.0f;
			data.m_relaxation[rows] = AXEL_RELAXATION;
			data.m_minForceLimit[rows] = -1.0e10f;
			data.m_maxForceLimit[rows] =  1.0e10f;
			rows ++;
		}


		// longitudinal twist tire jacobian
		{
			data.m_body0[rows] = 1;
			data.m_body1[rows] = i + 2;

			RayCastCarPhysicsData::Jacobian &jacobian0 = data.m_Jt[rows].m_jacobian_IM0; 
			jacobian0.m_linear = dVector (0.0f, 0.0f, 0.0f, 0.0f);
			jacobian0.m_angular = frontDir;

			RayCastCarPhysicsData::Jacobian &jacobian1 = data.m_Jt[rows].m_jacobian_IM1; 
			jacobian1.m_linear = dVector (0.0f, 0.0f, 0.0f, 0.0f);
			jacobian1.m_angular = frontDir.Scale (-1.0f);

			data.m_accel[rows] = 0.0f;
			data.m_relaxation[rows] = AXEL_RELAXATION;
			data.m_minForceLimit[rows] = -1.0e10f;
			data.m_maxForceLimit[rows] =  1.0e10f;
			rows ++;
		}

		// Lateral twist tire jacobian
		{
			data.m_body0[rows] = 1;
			data.m_body1[rows] = i + 2;

			RayCastCarPhysicsData::Jacobian &jacobian0 = data.m_Jt[rows].m_jacobian_IM0; 
			jacobian0.m_linear = dVector (0.0f, 0.0f, 0.0f, 0.0f);
			jacobian0.m_angular = chassisMatrix.m_up;

			RayCastCarPhysicsData::Jacobian &jacobian1 = data.m_Jt[rows].m_jacobian_IM1; 
			jacobian1.m_linear = dVector (0.0f, 0.0f, 0.0f, 0.0f);
			jacobian1.m_angular = chassisMatrix.m_up.Scale (-1.0f);

			data.m_accel[rows] = 0.0f;
			data.m_relaxation[rows] = AXEL_RELAXATION;
			data.m_minForceLimit[rows] = -1.0e10f;
			data.m_maxForceLimit[rows] =  1.0e10f;
			rows ++;
		}
		_ASSERTE (rows < VEHICLE_MAX_DOF);
	}


	// add Jacobian between body tires and floor or other data.m_bodies
	activeTiresRows = rows;
	invTimestep = 1.0f / timestep;
	for (int i = 0; i < m_tiresCount; i ++) {
		Tire& tire = m_tires[i];

		dVector tireOmega (bodyOmega + tire.m_lateralDir.Scale (tire.m_angularVelocity));
		dVector tireVeloc (bodyVeloc + bodyOmega * (data.m_bodies[i + 2].m_com - chassisMatrix.m_posit));
		dVector tireContactVeloc (tireVeloc + tireOmega * (tire.m_contactPoint - data.m_bodies[i + 2].m_com));

		if (tire.m_tireLoad > 0.0f) {

			dVector surfaceVeloc (0.0f, 0.0f, 0.0f, 0.0f);

			dVector relativeVeloc (surfaceVeloc - tireContactVeloc);

			// calculate the tire longitudinal forces derivatives
			{
				data.m_body0[rows] = 0;
				data.m_body1[rows] = i + 2;

				RayCastCarPhysicsData::Jacobian &jacobian0 = data.m_Jt[rows].m_jacobian_IM0; 
				jacobian0.m_linear = dVector (0.0f, 0.0f, 0.0f, 0.0f);
				jacobian0.m_angular = dVector (0.0f, 0.0f, 0.0f, 0.0f);

				RayCastCarPhysicsData::Jacobian &jacobian1 = data.m_Jt[rows].m_jacobian_IM1; 
				jacobian1.m_linear = tire.m_longitudinalDir;
				jacobian1.m_angular = (tire.m_contactPoint - data.m_bodies[i + 2].m_com) * jacobian1.m_linear;

				data.m_relaxation[rows] = CONTACT_PATCH_RELAXATION;
				data.m_accel[rows] = (relativeVeloc % tire.m_longitudinalDir) * invTimestep;
				data.m_minForceLimit[rows] = - tire.m_tireLoad * TIRE_FRICTION;
				data.m_maxForceLimit[rows] =   tire.m_tireLoad * TIRE_FRICTION;
				rows ++;
			}

			// calculate the tire side forces derivatives
			{
				data.m_body0[rows] = 0;
				data.m_body1[rows] = i + 2;

				RayCastCarPhysicsData::Jacobian &jacobian0 = data.m_Jt[rows].m_jacobian_IM0; 
				jacobian0.m_linear = dVector (0.0f, 0.0f, 0.0f, 0.0f);
				jacobian0.m_angular = dVector (0.0f, 0.0f, 0.0f, 0.0f);

				RayCastCarPhysicsData::Jacobian &jacobian1 = data.m_Jt[rows].m_jacobian_IM1; 
				jacobian1.m_linear = tire.m_lateralDir;
				jacobian1.m_angular = (tire.m_contactPoint - data.m_bodies[i + 2].m_com) * jacobian1.m_linear;

				data.m_relaxation[rows] = CONTACT_PATCH_RELAXATION;
				data.m_accel[rows] = (relativeVeloc % tire.m_lateralDir) * invTimestep;
				data.m_minForceLimit[rows] = - tire.m_tireLoad * TIRE_FRICTION;
				data.m_maxForceLimit[rows] =   tire.m_tireLoad * TIRE_FRICTION;
				rows ++;
			}
		} 


		if ((tire.m_breakTorque > 0.0f) || (dAbs (tire.m_angularVelocity) > tire.m_maxTireRPS)) {

			if (tire.m_breakTorque > 0.0f) {
				data.m_accel[rows] = tire.m_angularVelocity * invTimestep;
				data.m_minForceLimit[rows] = -tire.m_breakTorque;
				data.m_maxForceLimit[rows] =  tire.m_breakTorque;
			} else {
				if (tire.m_angularVelocity > 0.0f) {
					data.m_accel[rows] = (tire.m_angularVelocity - tire.m_maxTireRPS) * invTimestep;
				} else {
					data.m_accel[rows] = (tire.m_angularVelocity + tire.m_maxTireRPS) * invTimestep;
				}
				data.m_minForceLimit[rows] = -1.0e10f;
				data.m_maxForceLimit[rows] =  1.0e10f;;
			}
				
		} else {
			dFloat val;
			data.m_accel[rows] = tire.m_angularVelocity * invTimestep * 0.3f;
			val = dAbs (tire.m_angularVelocity);
			data.m_minForceLimit[rows] = -10.0f * tire.m_Ixx;
			data.m_maxForceLimit[rows] =  10.0f * tire.m_Ixx;
		}
		data.m_body0[rows] = 1;
		data.m_body1[rows] = i + 2;
		RayCastCarPhysicsData::Jacobian &jacobian0 = data.m_Jt[rows].m_jacobian_IM0; 
		RayCastCarPhysicsData::Jacobian &jacobian1 = data.m_Jt[rows].m_jacobian_IM1; 
		jacobian0.m_linear = dVector (0.0f, 0.0f, 0.0f, 0.0f);
		jacobian0.m_angular = tire.m_lateralDir;
		jacobian1.m_linear = dVector (0.0f, 0.0f, 0.0f, 0.0f);
		jacobian1.m_angular = tire.m_lateralDir.Scale (-1.0f);
		data.m_relaxation[rows] = AXEL_RELAXATION;
		rows ++;
	}

	// calculate the relative accelerations
	for (int i = 0; i < rows; i ++) {
		int j0;
		int j1;
		dFloat accel;
		dFloat diagonal;

		j0 = data.m_body0[i];
		j1 = data.m_body1[i];

		RayCastCarPhysicsData::Jacobian &jacobian0 = data.m_Jt[i].m_jacobian_IM0; 
		RayCastCarPhysicsData::Jacobian &jacobian1 = data.m_Jt[i].m_jacobian_IM1; 
		RayCastCarPhysicsData::Jacobian &jacobianInvM0 = data.m_JinvMass[i].m_jacobian_IM0; 
		RayCastCarPhysicsData::Jacobian &jacobianInvM1 = data.m_JinvMass[i].m_jacobian_IM1; 

		jacobianInvM0.m_linear = jacobian0.m_linear.Scale (data.m_bodies[j0].m_invMass);
		jacobianInvM0.m_angular = data.m_bodies[j0].m_invInertia.UnrotateVector (jacobian0.m_angular);
		jacobianInvM1.m_linear = jacobian1.m_linear.Scale (data.m_bodies[j1].m_invMass);
		jacobianInvM1.m_angular = data.m_bodies[j1].m_invInertia.UnrotateVector (jacobian1.m_angular);

		accel  = jacobianInvM0.m_linear  % data.m_bodies[j0].m_force;
		accel += jacobianInvM0.m_angular % data.m_bodies[j0].m_torque;
		accel += jacobianInvM1.m_linear  % data.m_bodies[j1].m_force;
		accel += jacobianInvM1.m_angular % data.m_bodies[j1].m_torque;
		// remembet adding centripletal force to teh accelration here
//			accel += ceneteripelts()

		diagonal  = jacobianInvM0.m_linear % jacobian0.m_linear;
		diagonal += jacobianInvM0.m_angular % jacobianInvM0.m_angular;
		diagonal += jacobianInvM1.m_linear % jacobian1.m_linear;
		diagonal += jacobianInvM1.m_angular % jacobianInvM1.m_angular;

		data.m_force[i] = dFloat (0.0f);
		data.m_accel[i] -= accel;
		data.m_diagDamp[i] = diagonal * data.m_relaxation[i];
		data.m_invDJMinvJt[i] = 1.0f / (diagonal + data.m_diagDamp[i]);
	}

	{
		dFloat akNum;
		dFloat accNorm2;

		akNum = dFloat (0.0f);
		accNorm2 = 0.0f;
		for (int i = 0; i < rows; i ++) {
			dFloat bk;
			dFloat val;
			val = data.m_accel[i];
			bk = val * data.m_invDJMinvJt[i];
			akNum += val * bk;
			data.m_deltaForce[i] = bk;
			data.m_activeRow[i] = 1.0f;
			val = val * val;
			if (val > accNorm2) accNorm2 = val;
		}

		for (int i = 0; (i < rows) && (accNorm2 > SOLVER_ERROR_2); i ++) {
			int clampedForce;
			dFloat ak;
			dFloat akDen;
			dFloat clipVal;
			for (int j = 0; j < rows; j ++) {
				data.m_y[j].m_linear[0] = 0.0f;
				data.m_y[j].m_linear[1] = 0.0f;
				data.m_y[j].m_linear[2] = 0.0f;
				data.m_y[j].m_linear[3] = 0.0f;
				data.m_y[j].m_angular[0] = 0.0f;
				data.m_y[j].m_angular[1] = 0.0f;
				data.m_y[j].m_angular[2] = 0.0f;
				data.m_y[j].m_angular[3] = 0.0f;
			}

			for (int j = 0; j < rows; j ++) {
				int i0;
				int i1;
				dFloat val;

				i0 = data.m_body0[j];
				i1 = data.m_body1[j];

				val = data.m_deltaForce[j]; 
				data.m_y[i0].m_linear  += data.m_Jt[j].m_jacobian_IM0.m_linear.Scale (val);
				data.m_y[i0].m_angular += data.m_Jt[j].m_jacobian_IM0.m_angular.Scale (val);

				data.m_y[i1].m_linear  += data.m_Jt[j].m_jacobian_IM1.m_linear.Scale (val);
				data.m_y[i1].m_angular += data.m_Jt[j].m_jacobian_IM1.m_angular.Scale (val);
			}

			akDen = dFloat (0.0f);
			for (int j = 0; j < rows; j ++) {
				int i0;
				int i1;
				dFloat acc;

				i0 = data.m_body0[j];
				i1 = data.m_body1[j];
				
				acc  = ((data.m_JinvMass[j].m_jacobian_IM0.m_linear % data.m_y[i0].m_linear) + (data.m_JinvMass[j].m_jacobian_IM0.m_angular % data.m_y[i0].m_angular));
				acc += ((data.m_JinvMass[j].m_jacobian_IM1.m_linear % data.m_y[i1].m_linear) + (data.m_JinvMass[j].m_jacobian_IM1.m_angular % data.m_y[i1].m_angular));
				acc += data.m_deltaForce[j] * data.m_diagDamp[j];

				data.m_deltaAccel[j] = acc;
				akDen += (acc * data.m_deltaForce[j]);
			}
			

//			_ASSERTE (akDen >= dFloat (0.0f));
			akDen = (akDen > 1.0e-16f) ? akDen : 1.0e-16f;
			_ASSERTE (dAbs (akDen) >= 1.0e-16f);
			ak = akNum / akDen;

			clipVal = 0.0f;
			clampedForce = -1;
			for (int j = activeTiresRows; j < rows; j ++) {

				if (data.m_activeRow[j]) {
					dFloat force;
					force = data.m_force[j] + ak * data.m_deltaForce[j];
					if (force < data.m_minForceLimit[j]) {
						_ASSERTE (((data.m_minForceLimit[j] - data.m_force[j]) / data.m_deltaForce[j]) <= ak);
						ak = (data.m_minForceLimit[j] - data.m_force[j]) / data.m_deltaForce[j];
						clampedForce = j;
						clipVal = data.m_minForceLimit[j];

					} else if (force > data.m_maxForceLimit[j]) {
						_ASSERTE (((data.m_maxForceLimit[j] - data.m_force[j]) / data.m_deltaForce[j]) <= ak);
						ak = (data.m_maxForceLimit[j] - data.m_force[j]) / data.m_deltaForce[j];
						clampedForce = j;
						clipVal = data.m_maxForceLimit[j];
					}
				}
			}

			if (clampedForce != -1) {

				akNum = dFloat (0.0f);
				accNorm2 = dFloat (0.0f);
				data.m_activeRow[clampedForce] = dFloat (0.0f);
				for (int j = 0; j < rows; j ++) {
					dFloat val;
					data.m_force[j] += ak * data.m_deltaForce[j];
					data.m_accel[j] -= ak * data.m_deltaAccel[j];

					val = data.m_accel[j] * data.m_invDJMinvJt[j] * data.m_activeRow[j];
					data.m_deltaForce[j] = val;
					akNum += val * data.m_accel[j];
					val = data.m_accel[j] * data.m_accel[j] * data.m_activeRow[j];
					if (val > accNorm2) accNorm2 = val;
				}
				data.m_force[clampedForce] = clipVal;
				i = -1;

			} else {
				accNorm2 = dFloat (0.0f);
				for (int j = 0; j < rows; j ++) {
					dFloat val;
					data.m_force[j] += ak * data.m_deltaForce[j];
					data.m_accel[j] -= ak * data.m_deltaAccel[j];
					val = data.m_accel[j] * data.m_accel[j] * data.m_activeRow[j];
					if (val > accNorm2) accNorm2 = val;
				}

				if (accNorm2 > SOLVER_ERROR_2) {
					dFloat akDen;

					akDen = (akNum > 1.0e-17f) ? akNum : 1.0e-17f;;
					akNum = 0.0f;
					for (int j = 0; j < rows; j ++) {
						dFloat val;
						val = data.m_accel[j] * data.m_invDJMinvJt[j] * data.m_activeRow[j];
						data.m_deltaAccel[j] = val;
						akNum += data.m_accel[j] * val;
					}

					ak = akNum / akDen;
					for (int j = 0; j < rows; j ++) {
						data.m_deltaForce[j] = data.m_deltaAccel[j] + ak * data.m_deltaForce[j];
					}
				}
			}
		}
	}

	for (int i = 0; i < rows; i ++) {
		int i0;
		int i1;
		dFloat force;

		i0 = data.m_body0[i];
		i1 = data.m_body1[i];
		force = data.m_force[i]; 

		RayCastCarPhysicsData::Jacobian &jacobian0 = data.m_Jt[i].m_jacobian_IM0; 
		RayCastCarPhysicsData::Jacobian &jacobian1 = data.m_Jt[i].m_jacobian_IM1; 

		data.m_bodies[i0].m_force += jacobian0.m_linear.Scale (force);
		data.m_bodies[i0].m_torque += jacobian0.m_angular.Scale (force);

		data.m_bodies[i1].m_force += jacobian1.m_linear.Scale (force);
		data.m_bodies[i1].m_torque += jacobian1.m_angular.Scale (force);
	}

	for (int i = 0; i < m_tiresCount; i ++) {
		Tire& tire = m_tires[i];

		tire.m_torque = 0.0f;
		tire.m_breakTorque = 0.0f;
		tire.m_angularVelocity += (data.m_bodies[i + 2].m_torque % tire.m_lateralDir) * tire.m_IxxInv * timestep;

//if (tire.m_angularVelocity >  100.0) tire.m_angularVelocity = 100.0f;
//if (tire.m_angularVelocity < -100.0) tire.m_angularVelocity = -100.0f;

		tire.m_spinAngle = dMod (tire.m_spinAngle + tire.m_angularVelocity * timestep, 3.1416f * 2.0f); 
	}

	// apply the net force and torque to the car chassis.
	NewtonBodySetForce(m_body0, &data.m_bodies[1].m_force.m_x);
	NewtonBodySetTorque(m_body0, &data.m_bodies[1].m_torque.m_x);
}


#else

void CustomRayCastCar::SubmitConstrainst(dFloat timestep, int threadIndex)
{
	dFloat invTimestep;
	dMatrix bodyMatrix;
	dMatrix suspensionMatrices[VEHICLE_MAX_TIRE_COUNT];  

	// get the simulation time
	invTimestep = 1.0f / timestep ;

	// get the vehicle global matrix, and use it in several calculations
	NewtonBodyGetMatrix(m_body0, &bodyMatrix[0][0]);
	dMatrix chassisMatrix (m_localFrame * bodyMatrix);  

	// calculate all suspension matrices in global space and tire collision
	for (int i = 0; i < m_tiresCount; i ++) {
		Tire& tire = m_tires[i];
		dMatrix suspensionMatrix = suspensionMatrices[i];

		// calculate this suspension matrix and save it for future used
		suspensionMatrix = CalculateSuspensionMatrix (i, 0.0f) * chassisMatrix;

		// calculate the tire collision
		CalculateTireCollision (tire, suspensionMatrix);
	}


	// calculate all suspension forces due to spring and damper
	dVector globalVeloc;
	dVector globalOmega;
	dVector force (0.0f, 0.0f, 0.0f, 0.0f);
	dVector torque (0.0f, 0.0f, 0.0f, 0.0f);

	// get the chassis instantaneous linear and angular velocity in the local space of the chassis
	int longitidunalForceIndex;
	longitidunalForceIndex = 0;
	NewtonBodyGetVelocity(m_body0, &globalVeloc[0]);
	NewtonBodyGetOmega(m_body0, &globalOmega[0]);
	for (int i = 0; i < m_tiresCount; i ++) {
		Tire& tire = m_tires[i];

		if (tire.m_posit < tire.m_suspensionLength) {
			dFloat speed;

			// calculate the linear velocity of the tire at the ground contact
			dVector tireAxelPosit (chassisMatrix.TransformVector(tire.m_harpoint - m_localFrame.m_up.Scale (tire.m_posit)));
			dVector localAxelPosit (tireAxelPosit - chassisMatrix.m_posit);
			dVector tireAxelVeloc (globalVeloc + globalOmega * localAxelPosit); 		

			// TO DO: need to calculate the velocity if the other body at the point
			// for now assume the ground is a static body
			dVector hitBodyVeloc (0, 0, 0, 0);

			// calculate the relative velocity
			dVector relVeloc (tireAxelVeloc - hitBodyVeloc);
			speed = -(relVeloc % chassisMatrix.m_up);

			// now calculate the tire load at the contact point
			tire.m_tireLoad = - NewtonCalculateSpringDamperAcceleration (timestep, tire.m_springConst, tire.m_suspensionLength - tire.m_posit, tire.m_springDamper, speed);

			if (tire.m_tireLoad < 0.0f) {
				// since the tire is not a body with real mass it can only push the chassis.
				tire.m_tireLoad = 0.0f;
			} else {
				//this suspension is applying a normalize force to the car chassis, need to scales by the mass of the car
				tire.m_tireLoad *= m_mass;

				// apply the tire model to these wheel
				ApplyTireFrictionModel(tire, chassisMatrix, tireAxelVeloc, tireAxelPosit, timestep, invTimestep, longitidunalForceIndex);
			}

			// convert the tire load force magnitude to a torque and force.
			dVector tireForce (chassisMatrix.m_up.Scale (tire.m_tireLoad));

			// accumulate the force and torque form this suspension
			force += tireForce;
			torque += localAxelPosit * tireForce;
		} else {
			//tire is on the air  not force applied to the vehicle.
			tire.m_tireLoad = dFloat (0.0f);
//			tire.m_tireJacobianRowIndex = -1;
		}
	}


	// add aerodynamic forces
	dVector verticalVeloc (chassisMatrix.m_up.Scale (chassisMatrix.m_up % globalVeloc));
	dVector horizontalVeloc (globalVeloc - verticalVeloc);
	force -= horizontalVeloc.Scale (m_aerodynamicDrag * m_mass);
	force -= chassisMatrix.m_up.Scale (m_aerodynamicDownForce * m_mass * dSqrt (horizontalVeloc % horizontalVeloc));

	// apply the net suspension force and torque to the car chassis.
	NewtonBodyAddForce(m_body0, &force[0]);
	NewtonBodyAddTorque(m_body0, &torque[0]);

	// set the current vehicle speed
	m_curSpeed = bodyMatrix[0] % globalVeloc;
}


void CustomRayCastCar::ApplyTireFrictionModel(
	Tire& tire, 
	const dMatrix& chassisMatrix,
	const dVector& tireAxelVeloc,
	const dVector& tireAxelPosit,
	dFloat timestep,
	dFloat invTimestep,
	int& longitudinalForceIndex)
{
	// calculate the point of side ping vector
	dVector lateralPin (chassisMatrix.RotateVector (tire.m_localAxis));
	dVector longitudinalPin (chassisMatrix.m_up * lateralPin);
	tire.m_longitudinalDir = longitudinalPin;
	tire.m_lateralDir = lateralPin; 

	// TO DO: need to subtract the velocity at the contact point of the hit body
	// for now assume the ground is a static body
	dVector hitBodyContactVeloc (0, 0, 0, 0);

	// calculate relative velocity at the tire center
	dVector tireAxelRelativeVelocity (tireAxelVeloc - hitBodyContactVeloc); 

	// now calculate relative velocity a velocity at contact point

	dVector tireAngularVelocity (lateralPin.Scale (tire.m_angularVelocity));
	dVector tireRadius (tire.m_contactPoint - tireAxelPosit);
	dVector tireContactVelocity (tireAngularVelocity * tireRadius);	
	dVector tireContactRelativeVelocity (tireAxelRelativeVelocity + tireContactVelocity); 
	tire.m_tireRadius = tireRadius;


	if (1) {
		// the tire is in coasting mode
		//		dFloat tireContactSpeed;
		//		dFloat tireRelativeSpeed;
		//		dFloat lateralForceMagnitud;

		//these tire is coasting, so the lateral friction dominates the behaviors  

		dFloat invMag2;
		dFloat frictionCircleMag;
		dFloat lateralFrictionForceMag;
		dFloat longitudinalAcceleration;
		dFloat longitudinalFrictionForceMag;

		frictionCircleMag = tire.m_tireLoad * tire.m_groundFriction;
		lateralFrictionForceMag = frictionCircleMag;
//		longitudinalFrictionForceMag = tire.m_tireLoad * tire.m_fixedRollingFriction;
		longitudinalFrictionForceMag = tire.m_tireLoad * 1.0f;

		invMag2 = frictionCircleMag / dSqrt (lateralFrictionForceMag * lateralFrictionForceMag + longitudinalFrictionForceMag * longitudinalFrictionForceMag);
		lateralFrictionForceMag *= invMag2;
		longitudinalFrictionForceMag = invMag2;

		//longitudinalFrictionForceMag = 1000;
		NewtonUserJointAddLinearRow (m_joint, &tireAxelPosit[0], &tireAxelPosit[0], &lateralPin[0]);
		NewtonUserJointSetRowMaximumFriction(m_joint,  lateralFrictionForceMag);
		NewtonUserJointSetRowMinimumFriction(m_joint, -lateralFrictionForceMag);
		longitudinalForceIndex ++; 

		// apply longitudinal friction force and acceleration
		longitudinalAcceleration = -(tireContactRelativeVelocity % longitudinalPin) * invTimestep;
		NewtonUserJointAddLinearRow (m_joint, &tireAxelPosit[0], &tireAxelPosit[0], &longitudinalPin[0]);
		NewtonUserJointSetRowMaximumFriction (m_joint,  longitudinalFrictionForceMag);
		NewtonUserJointSetRowMinimumFriction (m_joint, -longitudinalFrictionForceMag);
		NewtonUserJointSetRowAcceleration (m_joint, longitudinalAcceleration);
//		tire.m_tireJacobianRowIndex = longitudinalForceIndex;
		longitudinalForceIndex ++;



		// save the tire contact longitudinal velocity for integration after the solver
		tire.m_currentSlipVeloc = tireAxelRelativeVelocity % longitudinalPin;
	} else {
		_ASSERTE (0);
	}
}


/*
void CustomRayCastCar::IntegrateTires (dFloat timestep, int threadIndex)
{
	dFloat timestepInv;

	// get the simulation time
	timestepInv = 1.0f / timestep;

	dMatrix bodyMatrix;
	NewtonBodyGetMatrix(m_body0, &bodyMatrix[0][0]);
	dMatrix chassisMatrix (m_localFrame * bodyMatrix);  

	dVector globalVeloc;
	dVector globalOmega;
	NewtonBodyGetVelocity(m_body0, &globalVeloc[0]);
	NewtonBodyGetOmega(m_body0, &globalOmega[0]);

	// get the tire longitudinal to apply free force diagram for tire integration
	for (int i = 0; i < m_tiresCount; i ++) {
		Tire& tire = m_tires[i];

		if (tire.m_tireJacobianRowIndex == -1){
			dFloat torque;
			//this is free rolling tire 
			//just apply just integrate the torque and apply some angular damp
			torque = tire.m_torque - tire.m_angularVelocity * tire.m_Ixx * 0.1f;
			tire.m_angularVelocity  += torque * tire.m_IxxInv * timestep;


		} else {
			//check if any engine torque or brake torque is applied to the tire
			if (dAbs(tire.m_torque) < 1.0e-3f){
				//tire is coasting, calculate the tire zero slip angular velocity
				// this is the velocity that satisfy the constraint equation
				// V % dir + W * R % dir = 0
				// where V is the tire Axel velocity
				// W is the tire local angular velocity
				// R is the tire radius
				// dir is the longitudinal direction of of the tire.

				dVector tireAxelPosit (chassisMatrix.TransformVector(tire.m_harpoint - chassisMatrix.m_up.Scale (tire.m_posit)));
				dVector localAxelPosit (tireAxelPosit - chassisMatrix.m_posit);
				dVector tireAxelVeloc (globalVeloc + globalOmega * localAxelPosit); 		

				dFloat tireLinearSpeed;
				dFloat tireContactSpeed;
				tireLinearSpeed = tireAxelVeloc % tire.m_longitudinalDir;
				tireContactSpeed = (tire.m_lateralDir * tire.m_tireRadius) % tire.m_longitudinalDir;
				tire.m_angularVelocity = - tireLinearSpeed / tireContactSpeed ;
			} else {
				// tire is under some power, need to do the free body integration to apply the net torque
				int tireforceIndex;
				dFloat netTorque;
				dFloat tireFrictionTorque;
				tireforceIndex = tire.m_tireJacobianRowIndex;
				tireFrictionTorque = tire.m_radius * NewtonUserJointGetRowForce (m_joint, tireforceIndex);
				netTorque = tire.m_torque - tireFrictionTorque - tire.m_angularVelocity * tire.m_Ixx * 0.1f;


				tire.m_angularVelocity  += netTorque * tire.m_IxxInv * timestep;

			}
		}

		// integrate tire angular velocity and rotation
		tire.m_spinAngle = dMod (tire.m_spinAngle + tire.m_angularVelocity * timestep, 3.1416f * 2.0f); 

		// reset tire torque to zero after integration; 
		tire.m_torque = 0.0f;
	}
}
*/


#endif



