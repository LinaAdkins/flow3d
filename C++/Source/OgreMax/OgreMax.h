#ifndef H_OGREMAX
#define H_OGREMAX

#include "OgreMaxScene.hpp"


EXPORTFUNC OgreMax::OgreMaxScene* OGREMAXSCENE_create()
{
	return new OgreMax::OgreMaxScene();
}

EXPORTFUNC void OGREMAXSCENE_delete( OgreMax::OgreMaxScene* s )
{
	delete s;
}

EXPORTFUNC Ogre::SceneManager* OGREMAXSCENE_GetSceneManager( OgreMax::OgreMaxScene* s )
{
	return s->GetSceneManager();
}

EXPORTFUNC void OGREMAXSCENE_SetNamePrefix( const char* name , int prefixes , OgreMax::OgreMaxScene* s  )
{
	s->SetNamePrefix( name , prefixes );
}

EXPORTFUNC void OGREMAXSCENE_load( const char* fileNameOrContent , 
					   Ogre::RenderWindow* renderWindow , 
					   int loadOptions , 
					   Ogre::SceneManager* sceneManager , 
					   Ogre::SceneNode* rootNode , 
					   OgreMax::OgreMaxSceneCallback* callback , 
					   const char* defaultResourceGroupName , 
					   OgreMax::OgreMaxScene* s )
{
	s->Load( fileNameOrContent , renderWindow , loadOptions , sceneManager , rootNode , callback , defaultResourceGroupName );
}

/*  OgreMax Scene Callback Class */
//This is our class extension for handling callback events for scene loading-
class OgreMaxCallback : public OgreMax::OgreMaxSceneCallback 
{

public:
	void initialize( void (*blitzNodeFunc)( const OgreMax::OgreMaxScene*, Ogre::SceneNode*),
					 void (*blitzCamFunc)(const OgreMax::OgreMaxScene* scene, Ogre::Camera* camera),
					 void (*blitzEntityFunc)(const OgreMax::OgreMaxScene* scene, Ogre::Entity* entity),
					 void (*blitzLightFunc)(const OgreMax::OgreMaxScene*, Ogre::Light*),
					 void (*blitzMovableObjectFunc)(const OgreMax::OgreMaxScene*, Ogre::MovableObject*),
					 void (*blitzLoadProgressFunc)( const OgreMax::OgreMaxScene*, float progress )
				   )
	{

		this->loadNodeFunc = blitzNodeFunc;
		this->loadCamFunc  = blitzCamFunc;
		this->loadEntityFunc = blitzEntityFunc;
		this->loadLightFunc = blitzLightFunc;
		this->loadMovableObjectFunc = blitzMovableObjectFunc;
		this->loadProgressFunc = blitzLoadProgressFunc;
	}

	// Called before a node is saved in the scene file
	void FinishedCreatingNode(const OgreMax::OgreMaxScene* scene, Ogre::SceneNode* node)
	{
		loadNodeFunc( scene , node );
	}

	// Called before a camera is saved in the scene file
	void CreatedCamera(const OgreMax::OgreMaxScene* scene, Ogre::Camera* camera) { 
		loadCamFunc( scene , camera );
	}
	// Called before an entity is saved in the scene file
	void CreatedEntity(const OgreMax::OgreMaxScene* scene, Ogre::Entity* entity) {
		loadEntityFunc( scene , entity );
	}

	// Called before a light is saved in the scene file
	void CreatedLight(const OgreMax::OgreMaxScene* scene, Ogre::Light* light) { 
		loadLightFunc( scene , light );
	}

	// Called after a movable object is created
	void CreatedMovableObject (const OgreMax::OgreMaxScene *scene, Ogre::MovableObject *object){
		loadMovableObjectFunc( scene , object );
	}

	// Called each time load progress is updated
	void UpdatedLoadProgress (const OgreMax::OgreMaxScene *scene, Ogre::Real progress){
		loadProgressFunc( scene , progress );
	}

	// Function pointers to blitz callback functions.
	void(*loadNodeFunc)( const OgreMax::OgreMaxScene*  , Ogre::SceneNode* );
	void(*loadCamFunc)(const OgreMax::OgreMaxScene*, Ogre::Camera* );
	void(*loadEntityFunc)(const OgreMax::OgreMaxScene* , Ogre::Entity* );
	void(*loadLightFunc)(const OgreMax::OgreMaxScene* , Ogre::Light* );
	void(*loadMovableObjectFunc)(const OgreMax::OgreMaxScene* , Ogre::MovableObject*);
	void(*loadProgressFunc)( const OgreMax::OgreMaxScene* , float );


};

EXPORTFUNC OgreMaxCallback* OGREMAXCALLBACK_create()
{
	return new OgreMaxCallback();
}

EXPORTFUNC void OGREMAXCALLBACK_delete( OgreMaxCallback* c )
{
	delete c;
}

EXPORTFUNC void OGREMAXCALLBACK_initialize( void(*blitzNodeFunc)( const OgreMax::OgreMaxScene* , Ogre::SceneNode*  ) , 
								 void(*blitzCamFunc)( const OgreMax::OgreMaxScene* , Ogre::Camera* ) , 
								 void(*blitzEntityFunc)( const OgreMax::OgreMaxScene* , Ogre::Entity* ) , 
								 void(*blitzLightFunc)( const OgreMax::OgreMaxScene* , Ogre::Light* ) , 
								 void(*blitzMovableObjectFunc)( const OgreMax::OgreMaxScene* , Ogre::MovableObject* ) ,
								 void(*blitzProgressFunc)( const OgreMax::OgreMaxScene* , float  ) , 
								 OgreMaxCallback* c )

{
	c->initialize( blitzNodeFunc, blitzCamFunc , blitzEntityFunc , blitzLightFunc , blitzMovableObjectFunc , blitzProgressFunc );
}

#endif
